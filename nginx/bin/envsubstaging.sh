#!/usr/bin/env bash

for f in /etc/nginx/sites-template/*; do envsubst '\$STAGING_VERSION' < $f > /etc/nginx/sites-available/$(basename $f); done
