<?php

trait Checkout {

  public function checkout($data = []) {

    $api = $this->get('cart', IntegrationTestCase::$storage['header']);

    $response_get_cart = json_decode($api->response->getContent(), true)['data'];

    $api->seeStatusCode(200);

    $api = $this->json('POST', 'cart/checkout', [
      'data' => array_merge($data, [
        'signature' => \Odeo\Domains\Constant\Platform::generateCartSignature([
          'platform_id' => $data['platform_id'],
          'gateway_id' => $data['gateway_id'],
          'cart_id' => $response_get_cart['cart_id']
        ])])
    ], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(201);

    return json_decode($api->response->getContent(), true)['data'];

  }

}