<?php

class manageStoreTest extends AndroidTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }
  
  public function getStore() {
    $this->loginAsSeller();
    
    $api = $this->get('user/stores', IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'stores' => [
          "*" => [
            'store_id'
          ]
        ]
      ]
    ]);
    
    $data = json_decode($api->response->getContent(), true)['data'];
    
    return $data['stores'][0]['store_id'];
  }

  public function testUpdateStoreInformation() {
    
    $storeId = $this->getStore();
    
    $api = $this->get('store/' . $storeId, IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'store_id'
      ]
    ]);
    
    $api = $this->get('store/' . $storeId . '/settings', IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'store_id'
      ]
    ]);
    
    $api = $this->get('store/' . $storeId . '/accounts', IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'store_id', 'accounts'
      ]
    ]);
    
    $api = $this->json('POST', 'store/updateSettings', [
      'data' => [
        "store_id" => $storeId,
        "store_name" => "New Store Name",
        "logo_image" => "path/to/logo",
        "favicon" => "path/to/favicon",
        "facebook" => "facebook.com/AccName",
        "twitter" => "twitter.com/AccName",
        "instagram" => "instagram.com/AccName",
        "path" => "path.com/AccName",
        "google_plus" => "plus.google.com/AccName",
        "linkedin" => "linkedin.com/AccName",
        "phone_number" => "685267878088"
      ]
    ], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
  }
  
  public function testUpdateStoreInventories() {
    $storeId = $this->getStore();
    
    $api = $this->get('store/' . $storeId . '/inventories', IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'store_id', 'inventory' => [
          '*' => [
            'type', 'service_type'
          ]
        ]
      ]
    ]);
    
    $data = json_decode($api->response->getContent(), true)['data'];
    $provider = $data['inventory'][0]['providers'][0];
    
    $api = $this->json('POST', 'store/markup', [
      'data' => [
        "store_id" => $storeId,
        "service_detail_id" => $provider["service_detail_id"],
        "discount" => ($provider["markup_type"] == "percentage") ? 1 : 1000,
        "active" => 1,
        "disable" => 1
      ]
    ], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
  }
}
