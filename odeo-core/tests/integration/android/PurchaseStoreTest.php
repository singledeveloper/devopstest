<?php

class purchaseStoreTest extends AndroidTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function testPurchaseStore() {
    
    $this->loginAsSeller();
    
    $api = $this->get('plans');

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'plans' => [
          '*' => [
            "plan_id"
          ]
        ]
      ]
    ]);
    
    $data = json_decode($api->response->getContent(), true)['data'];
    
    $api = $this->json('POST', 'store/create', [
      'data' => [
        'plan_id' => $data['plans'][0]["plan_id"],
        'name' => 'Store Test Android',
        'subdomain_name' => str_random(16)
      ]
    ], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(201);
    
    $api->seeJsonStructure([
      'data' => [
        'store_id', 'cart_id'
      ]
    ]);
    
    $this->checkoutAndPay(\Odeo\Domains\Constant\Payment::ANDROID_PLAN, 19);
  }
  
}
