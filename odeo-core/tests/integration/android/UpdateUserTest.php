<?php

class updateUserTest extends AndroidTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function testUpdateUser() {
    
    $this->loginAsSeller();
    
    $api = $this->get('user/account', IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'user_id', 'name', 'type'
      ]
    ]);
    
    $api = $this->json('POST', 'user/account', [
      'data' => [
        'name' => 'Brian Japutra',
        'email' => 'brian@odeo.co.id'
      ]
    ], IntegrationTestCase::$storage['header']);
    
    $api->seeStatusCode(200);
  }
  
  public function testChangePassword() {
    
    $this->loginAsSeller();
    
    $api = $this->json('POST', 'user/account/password', [
      'data' => [
        'old_password' => '12345678',
        'new_password' => '123456'
      ]
    ], IntegrationTestCase::$storage['header']);
    
    $api->seeStatusCode(200);
  }
}
