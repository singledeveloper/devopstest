<?php

class manageBankAccountTest extends AndroidTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function testManageBankAccount() {
    
    $this->loginAsSeller();
    
    $api = $this->get('banks');

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'banks' => [
          '*' => [
            "id"
          ]
        ]
      ]
    ]);
    
    $data = json_decode($api->response->getContent(), true)['data'];
    
    $api = $this->json('POST', 'user/bankAccount/create', [
      'data' => [
        'name' => 'Bank Saya',
        'bank_id' => $data['banks'][0]["id"],
        'account_number' => '5270282612',
        'account_name' => 'Brian Japutra'
      ]
    ], IntegrationTestCase::$storage['header']);
    
    $api->seeStatusCode(201);
    
    $api->seeJsonStructure([
      'data' => [
        'id'
      ]
    ]);
    
    $api = $this->get('user/bankAccount', IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'bank_accounts' => [
          '*' => [
            "id"
          ]
        ]
      ]
    ]);
    
    $data = json_decode($api->response->getContent(), true)['data'];
    
    $api = $this->json('POST', 'user/bankAccount/delete', [
      'data' => [
        'bank_account_id' => $data['bank_accounts'][0]["id"]
      ]
    ], IntegrationTestCase::$storage['header']);
    
    $api->seeStatusCode(200);
  }
  
}
