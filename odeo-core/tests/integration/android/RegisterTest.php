<?php

class registerTest extends AndroidTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function testRegisterByTelephone() {
    
    $telephone = '6285267878089';
    
    $api = $this->json('POST', 'user/sendVerification', [
      'data' => [
        'phone_number' => $telephone,
        'scenario' => 'sign_up'
      ]
    ]);

    $api->seeStatusCode(201);
    
    $api->seeJsonStructure([
      'data' => [
        'ttl'
      ]
    ]);
    
    $api = $this->json('POST', 'user/verify', [
      'data' => [
        'phone_number' => $telephone,
        'verification_code' => 1234
      ]
    ]);
    
    $api->seeStatusCode(200);

    $api = $this->json('POST', 'user/register', [
      'data' => [
        'phone_number' => $telephone,
        'password' => '123456',
        'confirm_password' => '123456',
        'type' => \Odeo\Domains\Constant\UserType::SELLER
      ]
    ]);
    
    $api->seeStatusCode(201);
    
    $api->seeJsonStructure([
      'data' => [
        'user_id', 'token', 'token_refresh'
      ]
    ]);
  }
}
