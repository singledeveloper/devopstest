<?php

abstract class WebStoreTestCase extends IntegrationTestCase {



  public function __construct() {
    parent::__construct();
  }


  public function checkoutAndPay() {

    $response_checkout = $this->checkout([
      'platform_id' => \Odeo\Domains\Constant\Platform::WEB_STORE,
      'gateway_id' => \Odeo\Domains\Constant\Payment::WEB_STORE,
      'telephone' => '081315556285',
      'email' => 'vincent.wu.vt@gmail.com'
    ]);

    $this->payWithBankTransfer($response_checkout['order_id'], 42);
  }





}