<?php


class purchasePulsaTest extends WebStoreTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function getNominal($serviceDetail, $storeId = 1) {

    $query = http_build_query([
      'prefix' => '0813',
      'service_detail_id' => $serviceDetail,
      'store_id' => $storeId
    ]);

    $api = $this->get('pulsa?' . $query, IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api->seeJsonStructure([
      'data' => [
        'inventory' => [
          '*' => [
            'category', 'details'
          ]
        ]
      ]
    ]);

    return json_decode($api->response->getContent(), true)['data'];

  }


  public function testAddToCartSpektrum() {
    
    $api = $this->get('store/testong/informations');

    $api->seeStatusCode(200);

    $api->seeJsonStructure([
      'data' => [
        'store_id'
      ]
    ]);
    
    $data = json_decode($api->response->getContent(), true)['data'];

    $this->loginAsGuest();

    $response_nominal = $this->getNominal(\Odeo\Domains\Constant\ServiceDetail::PULSA_ODEO, $data['store_id']);

    $this->addCart([
      'item_id' => $response_nominal['inventory'][0]['details'][0]['item_id'],
      'item_detail' => [
        'operator' => $response_nominal['operator'],
        'number' => '0813' . mt_rand(100000, 999999)
      ],
      'store_id' => $data['store_id'],
      'service_detail_id' => \Odeo\Domains\Constant\ServiceDetail::PULSA_ODEO,
    ]);

    $this->checkoutAndPay();
  }


}
