<?php


class purchaseBoltTest extends WebStoreTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function getNominal($serviceDetail) {

    $query = http_build_query([
      'service_detail_id' => $serviceDetail,
      'store_id' => 1
    ]);

    $api = $this->get('bolt?' . $query, IntegrationTestCase::$storage['header']);


    $api->seeStatusCode(200);

    $api->seeJsonStructure([
      'data' => [
        'inventory' => [
          '*' => [
            'category', 'details'
          ]
        ]
      ]
    ]);

    return json_decode($api->response->getContent(), true)['data'];

  }


  public function testAddToCartSpektrum() {

    $this->loginAsGuest();

    $response_nominal = $this->getNominal(\Odeo\Domains\Constant\ServiceDetail::BOLT_ODEO);

    $this->addCart([
      'item_id' => $response_nominal['inventory'][0]['details'][0]['item_id'],
      'item_detail' => [
        'operator' => $response_nominal['operator'],
        'number' => mt_rand(100000000, 999999999)
      ],
      'store_id' => 1,
      'service_detail_id' => \Odeo\Domains\Constant\ServiceDetail::BOLT_ODEO,
    ]);

    $this->checkoutAndPay();
  }


}
