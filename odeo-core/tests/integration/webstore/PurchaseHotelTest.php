<?php


class purchaseHotelTest extends WebStoreTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function testPurchaseHotel() {

    $serviceDetail = \Odeo\Domains\Constant\ServiceDetail::HOTEL_TIKET;

    $this->loginAsGuest();

    $this->searchAutoComplete($serviceDetail);
    $response_search_hotel = $this->search($serviceDetail);
    $response_choose_room = $this->chooseRoom($serviceDetail, $response_search_hotel['results'][0]);
//    $this->addCart([
//      'item_id' => 0,
//      'item_detail' => [
//        'book_uri' => $response_choose_room['rooms'][0]['bookUri'],
//      ],
//      'store_id' => 1,
//      'service_detail_id' => $serviceDetail,
//    ]);
//
//    $this->checkoutAndPay();

  }

  public function searchAutoComplete($serviceDetail) {

    $query = http_build_query([
      'q' => 'jakarta',
      'service_detail_id' => $serviceDetail,
      'store_id' => 1
    ]);

    $api = $this->get('hotel/search/autocomplete?' . $query, IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api->seeJsonStructure([
      'data' => [
        'results' => [
          '*' => [
            'business_uri'
          ]
        ]
      ]
    ]);
  }

  public function search($serviceDetail) {

    $api = $this->json('POST', 'hotel/search', [
      'startdate' => \Carbon\Carbon::tomorrow()->toDateString(),
      'enddate' => \Carbon\Carbon::tomorrow()->addDay(1)->toDateString(),
      'night' => 1,
      'service_detail_id' => $serviceDetail,
      'room' => 1,
      'offset' => 0,
      'q' => 'jakarta',
      'store_id' => 1
    ], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api->seeJsonStructure([
      'data' => [
        'metadata' => ['offset', 'limit', 'count'],
        'results' => [
          '*' => ['name', 'price']
        ]
      ]
    ]);

    return json_decode($api->response->getContent(), true)['data'];

  }

  public function chooseRoom($serviceDetail, $hotel) {

    $query = http_build_query([
      'business_uri' => $hotel['business_uri'],
      'service_detail_id' => $serviceDetail,
      'store_id' => 1
    ]);

    $api = $this->get('hotel/view?' . $query, IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    $api->seeJsonStructure([
      'data' => [
        'rooms' => [
          '*' => ['price']
        ]
      ]
    ]);
    return json_decode($api->response->getContent(), true)['data'];
  }
}
