<?php


class purchaseFlightTest extends WebStoreTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function testPurchaseTiket() {

    $serviceDetail = \Odeo\Domains\Constant\ServiceDetail::FLIGHT_TIKET;

    $this->loginAsGuest();

    $this->getAirport($serviceDetail);

    $response_search_flight = $this->searchFlight($serviceDetail);

    $response_get_flight = $this->getFlight($response_search_flight['departure'][0], $serviceDetail);
    // TODO passportnationalitya id from getFlight might return 0, so whatever you do, when it's happened, this flow will always fail.

    /*$this->addCart([
      'item_id' => $response_get_flight['flight_id'],
      'item_detail' => $response_get_flight,
      'store_id' => 1,
      'service_detail_id' => $serviceDetail
    ]);

    $this->checkoutAndPay();*/

  }


  public function getFlight($flight, $serviceDetail) {

    $query = http_build_query([
      'date' => \Carbon\Carbon::tomorrow()->toDateString(),
      'flight_id' => $flight['flight_id'],
      'service_detail_id' => $serviceDetail,
      'store_id' => 1
    ]);

    $api = $this->get('flight/order?' . $query, IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api->seeJsonStructure([
      'data' => [
        'adult', 'contact'
      ]
    ]);

    $data = json_decode($api->response->getContent(), true)['data'];

    $response['firstnamea1'] = 'vincent' . substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), -5);
    $response['titlea1'] = 'Mr';

    if (isset($data['adult']['dcheckinbaggagea1'])) $response['dheckinbaggagea11'] = $data['adult']['dcheckinbaggagea1']['resource'][0]['id'];
    if (isset($data['adult']['birthdatea'])) $response['birthdatea1'] = '2004-10-22';
    if (isset($data['adult']['passportnationalitya'])) $response['passportnationalitya1'] = $data['adult']['passportnationalitya']['resource'][0]['id'];
    
    $response['conEmailAddress'] = 'test@gmail.com';
    $response['conPhone'] = '081315556285';
    $response['conSalutation'] = 'Mr';
    $response['conFirstName'] = 'Vincent';
    $response['flight_id'] = $flight['flight_id'];


    return $response;
  }


  public function getAirport($serviceDetail) {

    $this->loginAsGuest();

    $query = http_build_query([
      'service_detail_id' => $serviceDetail,
      'store_id' => 1
    ]);

    $api = $this->get('flight?' . $query, IntegrationTestCase::$storage['header']);


    $api->seeStatusCode(200);

    $api->seeJsonStructure([
      'data' => [
        'airports' => [
          '*' => ['code', 'name']
        ]
      ]
    ]);

  }

  public function searchFlight($serviceDetail) {

    $this->loginAsGuest();

    $api = $this->json('POST', 'flight/search', [
      'data' => [
        'adult' => 1,
        'child' => 0,
        'infant' => 0,
        'date' => \Carbon\Carbon::tomorrow()->toDateString(),
        'origin' => 'CGK',
        'destination' => 'DPS',
        'service_detail_id' => $serviceDetail,
        'store_id' => 1
      ]
    ], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api->seeJsonStructure([
      'data' => [
        'airlines' => [
          '*' => ['code', 'name']
        ],
        'departure' => [
          '*' => ['flight_id', 'price_value']
        ]
      ]
    ]);

    return json_decode($api->response->getContent(), true)['data'];

  }

}
