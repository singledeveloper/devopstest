#!/bin/sh
#
# An example hook script to prepare a packed repository for use over
# dumb transports.
#
# To enable this hook, rename this file to "post-update".
export COMPOSE_INTERACTIVE_NO_CLI=1
dockerFile=/var/www/odeo-docker/docker-compose.yml
compose=/usr/local/bin/docker-compose

$compose -f $dockerFile exec php-fpm sh -c '(kill -USR2 1)' > /dev/null 2>&1 &
#$compose -f $dockerFile exec workspace bash -c '(cd /var/www/odeo-core && php artisan migrate --force)' > /dev/null 2>&1 &
$compose -f $dockerFile exec workspace bash -c '(cd /var/www/odeo-core && php artisan queue:restart)' > /dev/null 2>&1 &
