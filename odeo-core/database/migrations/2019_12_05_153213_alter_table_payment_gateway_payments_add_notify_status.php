<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentGatewayPaymentsAddNotifyStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::transaction(function () {
        Schema::table('payment_gateway_payments', function(Blueprint $table){
          $table->integer('notify_status')->nullable();
        });
        \DB::statement("update payment_gateway_payments pgp set notify_status = status");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
