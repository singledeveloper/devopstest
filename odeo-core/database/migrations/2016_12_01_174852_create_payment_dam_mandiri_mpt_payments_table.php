<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDamMandiriMptPaymentsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('payment_dam_mandiri_mpt_payments', function (Blueprint $table) {
      $table->increments('id');
      $table->string('payment_account_number');
      $table->string('payment_account_name');
      $table->decimal('payment_amount', 17, 2);
      $table->timestamp('payment_time_limit_info');
      $table->string('payment_ticket');
      $table->string('trx_ref_no')->nullable();
      $table->integer('status_init');
      $table->string('success_init')->nullable();
      $table->integer('status_validate')->nullable();
      $table->string('success_validate')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('payment_dam_mandiri_mpt_payments');
  }
}
