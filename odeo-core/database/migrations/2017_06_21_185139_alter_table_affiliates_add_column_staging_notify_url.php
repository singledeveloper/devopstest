<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAffiliatesAddColumnStagingNotifyUrl extends Migration {

  public function up() {
    Schema::table('affiliates', function (Blueprint $table) {
      $table->string('staging_notify_url')->nullable();
    });
  }

  public function down() {
    Schema::table('affiliates', function (Blueprint $table) {
      $table->dropColumn('staging_notify_url');
    });
  }
}
