<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiDisbursements extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('api_disbursements', function (Blueprint $table) {

      $table->increments('id');
      
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->integer('bank_id');
      $table->foreign('bank_id')->references('id')->on('banks')->onDelete('set null');
      
      $table->decimal('amount', 17, 2);
      $table->decimal('fee', 8, 2)->default(0);
      $table->string('account_number');
      $table->string('description')->default('');
      $table->string('status');
      
      $table->datetime('cancelled_at')->nullable();
      $table->datetime('verified_at')->nullable();

      $table->bigInteger('auto_disbursement_reference_id')->unsigned()->nullable();
      $table->string('auto_disbursement_vendor')->nullable();

      $table->timestamps();

    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('api_disbursements');
  }
}
