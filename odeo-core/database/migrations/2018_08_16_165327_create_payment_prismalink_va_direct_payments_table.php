<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentPrismalinkVaDirectPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_prismalink_va_direct_payments', function (Blueprint $table) {
        $table->increments('id');
        $table->string('code', 2);
        $table->string('partner_id');
        $table->decimal('amount', 15, 2);
        $table->string('signature');
        $table->string('payment_code', 2)->nullable();
        $table->decimal('payment_fee', 15, 2)->nullable();
        $table->decimal('payment_net_amount', 15, 2)->nullable();
        $table->integer('currency_code')->nullable();
        $table->string('reference_number')->nullable();
        $table->string('payment_signature')->nullable();
        $table->timestamp('paid_at')->nullable();
        $table->string('void_code', 2)->nullable();
        $table->timestamp('void_at')->nullable();
        $table->string('void_reference_number')->nullable();
        $table->string('void_signature')->nullable();
        $table->timestamps();
      });
    }

    public function down()
    {
        //
    }
}
