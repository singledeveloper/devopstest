<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStoreRevenuesAddTotalProfitColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('store_revenues', function(Blueprint $table) {
        $table->decimal('amount_free', 20, 2)->default(0);
        $table->decimal('total_free', 20, 2)->default(0);
        $table->decimal('amount_profit', 20, 2)->default(0);
        $table->decimal('total_profit', 20, 2)->default(0);
        $table->decimal('amount_profit_free', 20, 2)->default(0);
        $table->decimal('total_profit_free', 20, 2)->default(0);
        $table->decimal('amount_rush', 20, 2)->default(0);
        $table->decimal('total_rush', 20, 2)->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('store_revenues', function(Blueprint $table) {
        $table->dropColumn('amount_free');
        $table->dropColumn('total_free');
        $table->dropColumn('amount_profit');
        $table->dropColumn('total_profit');
        $table->dropColumn('amount_profit_free');
        $table->dropColumn('total_profit_free');
        $table->dropColumn('amount_rush');
        $table->dropColumn('total_rush');
      });
    }
}
