<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDisbursementArtajasaStatusInquiries extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('disbursement_artajasa_status_inquiries', function (Blueprint $table) {
      $table->increments('id');

      $table->bigInteger('disbursement_artajasa_disbursement_id')->unsigned();
      $table->foreign('disbursement_artajasa_disbursement_id')
        ->references('id')->on('disbursement_artajasa_disbursements')
        ->onDelete('set null');

      $table->integer('transaction_id')->nullable();
      $table->dateTime('transaction_datetime')->nullable();
      $table->integer('query_transaction_id')->nullable();
      $table->dateTime('query_transaction_datetime')->nullable();
      $table->string('response_code')->nullable();
      $table->text('response_description')->nullable();

      $table->text('response_json')->nullable();
      $table->dateTime('response_datetime')->nullable();
      $table->text('error_exception_message')->nullable();
      $table->integer('error_exception_code')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('disbursement_artajasa_status_inquiries');
  }
}
