<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentUrlsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('payment_urls', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->bigInteger('order_id')->unsigned();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');

      $table->tinyInteger('opc')->unsigned()->nullable();
      $table->foreign('opc')->references('code')->on('payment_odeo_payment_channels')->onDelete('set null');

      $table->text('body');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('payment_urls');
  }
}
