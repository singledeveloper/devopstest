<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZenzivaSmsLogsTable extends Migration {

  public function up() {
    Schema::create('zenziva_sms_logs', function(Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('to')->nullable();
      $table->string('sms_text')->nullable();
      $table->text('xml_response')->nullable();
      $table->timestamp('message_timestamp')->nullable();
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('zenziva_sms_logs');
  }
}
