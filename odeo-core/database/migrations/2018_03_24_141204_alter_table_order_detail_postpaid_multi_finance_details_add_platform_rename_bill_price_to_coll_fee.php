<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailPostpaidMultiFinanceDetailsAddPlatformRenameBillPriceToCollFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_postpaid_multi_finance_details', function(Blueprint $table){
        $table->string('platform', 20)->nullable();
        $table->integer('bill_price')->nullable()->change();
        $table->renameColumn('bill_price', 'coll_fee');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
