<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePostpaidInquiryHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postpaid_inquiry_histories', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('seller_store_id')->nullable();
          $table->foreign('seller_store_id')->references('id')->on('stores')->onDelete('set null');
          $table->bigInteger('user_id');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
          $table->integer('inventory_id');
          $table->foreign('inventory_id')->references('id')->on('pulsa_odeo_inventories')->onDelete('set null');
          $table->string('reference_type', 50);
          $table->bigInteger('reference_id')->nullable();
          $table->string('number', 50);
          $table->text('request')->nullable();
          $table->text('response')->nullable();
          $table->text('notify')->nullable();
          $table->text('result')->nullable();
          $table->string('error_message', 255)->nullable();
          $table->char('status', 5);
          $table->boolean('is_processed')->default(false);
          $table->timestamps();
          $table->index(['seller_store_id']);
          $table->index(['user_id']);
          $table->index(['inventory_id']);
          $table->index(['reference_type']);
          $table->index(['reference_id']);
          $table->index(['number']);
          $table->index(['status']);
          $table->index(['is_processed']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
