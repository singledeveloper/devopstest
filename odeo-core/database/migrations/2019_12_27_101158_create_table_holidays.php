<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHolidays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holidays', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->char('event_date', 10);
          $table->string('description', 255);
          $table->text('translation_data')->nullable();
          $table->integer('settlement_increment')->default(1);
          $table->boolean('is_active')->default(true);
          $table->timestamps();
          $table->index(['event_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
