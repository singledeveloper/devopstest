<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentCimbVaPaymentsAddColumnReferenceNumberTransaction extends Migration
{
  public function up() {
    Schema::table('payment_cimb_va_payments', function (Blueprint $table) {
      $table->text('reference_number_transaction')->nullable();
    });
  }


  public function down() {
    Schema::table('payment_cimb_va_payments', function (Blueprint $table) {
      $table->dropColumn('reference_number_transaction');
    });
  }
}
