<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAffiliateUserInvoicesChangeBilledUserIdType extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('affiliate_user_invoices', function (Blueprint $table) {
      $table->string('billed_user_id')->change();
      $table->index('billed_user_id');
      $table->index('period');
      $table->index('biller_id');
      $table->index('status');
      $table->index('invoice_number');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  
  }
}
