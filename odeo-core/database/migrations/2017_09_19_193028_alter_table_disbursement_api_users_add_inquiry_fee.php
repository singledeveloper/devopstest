<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTableDisbursementApiUsersAddInquiryFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('disbursement_api_users', function (Blueprint $table) {
        $table->decimal('inquiry_fee', 17, 2)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('disbursement_api_users', function (Blueprint $table) {
        $table->dropColumn('inquiry_fee');
      });
    }
}
