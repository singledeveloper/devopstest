<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiket_airports', function(Blueprint $table) {
            $table->char('id', 3);
            $table->primary('id');
            $table->string('name', 50);
            $table->string('location', 50);
            $table->char('country_id', 2);
            $table->string('country_name', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tiket_airports');
    }
}
