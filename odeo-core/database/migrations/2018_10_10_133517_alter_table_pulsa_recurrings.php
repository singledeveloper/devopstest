<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaRecurrings extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('pulsa_recurrings', function (Blueprint $table) {
      $table->bigInteger('store_id')->unsigned()->nullable();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
      $table->date('expired_at')->nullable();
      $table->date('next_payment')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
