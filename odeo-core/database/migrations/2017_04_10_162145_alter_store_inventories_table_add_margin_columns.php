<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStoreInventoriesTableAddMarginColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('store_inventories', function (Blueprint $table) {
        $table->string('margin_type', 10)->nullable();
        $table->decimal('margin', 10, 2)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('store_inventories', function (Blueprint $table) {
        $table->dropColumn('margin_type');
        $table->dropColumn('margin');
      });
    }
}
