<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankAccountInquiriesAddColumnUserId extends Migration {

  public function up() {
    Schema::table('bank_account_inquiries', function (Blueprint $table) {
      $table->bigInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users');
    });
  }

  public function down() {
    Schema::table('bank_account_inquiries', function (Blueprint $table) {
      //
    });
  }
}
