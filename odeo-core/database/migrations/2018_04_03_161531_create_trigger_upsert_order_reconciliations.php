<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUpsertOrderReconciliations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::beginTransaction();

      Schema::table('order_reconciliations', function(Blueprint $table) {
        $table->bigInteger('order_id')->unique()->change();
      });

      DB::unprepared('
        CREATE OR REPLACE FUNCTION upsert_order_reconciliations()
            RETURNS trigger AS
        $BODY$
        BEGIN
          IF NEW.order_id IS NOT NULL THEN 
            INSERT INTO order_reconciliations (
              order_id,
              total_cash_other,
              total_cash,
              total_deposit,
              total_marketing,
              trx_count,
              transaction_date,
              created_at,
              updated_at,
              trx_month,
              trx_year,
              is_reconciled
            )
            VALUES (
              NEW.order_id,
              (case when NEW.user_id is null and NEW.store_id is null then NEW.amount else 0 end),
              (case when NEW.user_id is not null and NEW.cash_type = \'cash\' then NEW.amount else 0 end),
              (case when NEW.cash_type = \'deposit\' then NEW.amount else 0 end),
              (case when NEW.cash_type = \'ads\' or NEW.cash_type = \'rush\' or NEW.cash_type = \'cashback\' then NEW.amount else 0 end),
              1,
              NEW.created_at,
              now(),
              now(),
              date_part(\'month\', NEW.created_at),
              date_part(\'year\', NEW.created_at),
              false
            ) ON CONFLICT (order_id) DO UPDATE 
              SET
                payment_name = NULL,
                is_reconciled = false,
                total_diff = 0,
                diff_reason = NULL,
                total_cash_other = order_reconciliations.total_cash_other + (case when NEW.user_id is null and NEW.store_id is null then NEW.amount else 0 end),
                total_cash = order_reconciliations.total_cash + (case when NEW.user_id is not null and NEW.cash_type = \'cash\' then NEW.amount else 0 end),
                total_deposit = order_reconciliations.total_deposit + (case when NEW.cash_type = \'deposit\' then NEW.amount else 0 end),
                total_marketing = order_reconciliations.total_marketing + (case when NEW.cash_type = \'ads\' or NEW.cash_type = \'rush\' or NEW.cash_type = \'cashback\' then NEW.amount else 0 end),
                trx_count = order_reconciliations.trx_count + 1,
                updated_at = now()
            ;
          END IF;
          RETURN NEW;
        END;
        $BODY$
        LANGUAGE plpgsql;
      ');

      DB::unprepared("
        CREATE TRIGGER upsert_order_reconciliations
        AFTER INSERT
        ON cash_transactions
        FOR EACH ROW
        EXECUTE PROCEDURE upsert_order_reconciliations();
      ");

      DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
