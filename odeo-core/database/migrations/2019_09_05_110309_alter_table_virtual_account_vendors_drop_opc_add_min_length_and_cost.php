<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVirtualAccountVendorsDropOpcAddMinLengthAndCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('virtual_account_vendors', function (Blueprint $table) {
        $table->dropColumn('opc');
        $table->integer('min_length')->nullable();
        $table->decimal('cost', 15, 2)->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
