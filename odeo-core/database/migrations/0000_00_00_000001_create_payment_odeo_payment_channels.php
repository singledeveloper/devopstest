<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentOdeoPaymentChannels extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('payment_odeo_payment_channels', function (Blueprint $table) {
      $table->integer('code')->unsigned();
      $table->primary('code');

      $table->tinyInteger('gateway_id');
      $table->unsignedInteger('info_id');
      $table->foreign('info_id')->references('id')->on('payment_odeo_payment_channel_informations')->onDelete('set null');

      $table->tinyInteger('active')->nullable();

      $table->timestamps();


    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('payment_odeo_payment_channels');
  }
}
