<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreAgentMasterSettingsTable extends Migration {

  public function up() {
    Schema::create('store_agent_master_settings', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('store_id')->unsigned();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
      $table->decimal('bonus', 10, 2);
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('store_agent_master_settings');
  }
}
