<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailPulsaSwitchersAddSerialNumberColumn extends Migration {

  public function up() {
    Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
      $table->string("serial_number", 255)->nullable()->before('status');
    });
  }


  public function down() {
    Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
      $table->dropColumn("serial_number");
    });
  }
}
