<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementApiUsersSecretAccessKeyLength extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('disbursement_api_users', function (Blueprint $table) {
      $table->string('secret_access_key', 1024)->change();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('disbursement_api_users', function (Blueprint $table) {
      $table->string('secret_access_key')->change();
    });
  }
}
