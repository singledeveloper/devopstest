<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementArtajasaDisbursementsAddBankName extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    DB::beginTransaction();
    Schema::table('disbursement_artajasa_disbursements', function (Blueprint $table) {
      $table->string('transfer_to_bank')->nullable();
    });

    DB::unprepared('update disbursement_artajasa_disbursements
      set transfer_to_bank=b.name
      from disbursement_artajasa_disbursements a join banks b on a.bank_code=b.aj_bank_code
      where a.bank_code is not null');

    DB::commit();
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('disbursement_artajasa_disbursements', function (Blueprint $table) {
      $table->dropColumn('transfer_to_bank');
    });
  }
}
