<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorSwitchersTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('vendor_switchers', function (Blueprint $table) {
      $table->integer('id')->unsigned()->primary();
      $table->integer('vendor_id')->unsigned();
      $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
      $table->string('name', 50)->default('');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('vendor_switchers');
  }
}
