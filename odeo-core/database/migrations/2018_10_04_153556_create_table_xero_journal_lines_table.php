<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableXeroJournalLinesTable extends Migration
{
    public function up()
    {
      Schema::create('xero_journal_lines', function(Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('manual_journal_id')->nullable();
        $table->string('tax_type');
        $table->decimal('tax_amount', 15, 2);
        $table->decimal('debit_amount', 15, 2);
        $table->decimal('credit_amount', 15, 2);
        $table->string('account_code');
        $table->text('tracking');
        $table->string('account_id');
        $table->boolean('is_blank');
        $table->index('manual_journal_id');
        $table->index('account_code');
      }); 
    }

    public function down()
    {
        //
    }
}
