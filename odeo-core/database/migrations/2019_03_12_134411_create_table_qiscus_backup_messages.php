<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableQiscusBackupMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qiscus_backup_messages', function(Blueprint $table){
            $table->string('id');
            $table->bigInteger('qiscus_backup_id')->unsigned();
            $table->foreign('qiscus_backup_id')->references('id')->on('qiscus_backups')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('message');
            $table->string('type');
            $table->string('unique_id');
            $table->string('room_unique_id');
            $table->datetime('created_at');
            $table->string('url')->nullable();
            $table->string('replied_comment_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
