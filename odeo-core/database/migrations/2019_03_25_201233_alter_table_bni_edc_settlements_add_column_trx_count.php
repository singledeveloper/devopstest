<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBniEdcSettlementsAddColumnTrxCount extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('bni_edc_settlements', function (Blueprint $table) {
      $table->integer('trx_count');
      $table->dropColumn('trx_fee');
      $table->index('trx_date');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
