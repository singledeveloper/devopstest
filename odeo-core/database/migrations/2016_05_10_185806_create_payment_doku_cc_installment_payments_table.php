<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDokuCcInstallmentPaymentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('payment_doku_cc_installment_payments', function (Blueprint $table) {
      $table->increments('id');

      $table->string('purchase_currency')->nullable();
      $table->string('payment_date_time')->nullable();
      $table->string('liability')->nullable();
      $table->string('payment_channel')->nullable();
      $table->string('amount')->nullable();
      $table->string('payment_code')->nullable();
      $table->string('mcn')->nullable();
      $table->string('result_msg')->nullable();
      $table->string('verify_id')->nullable();
      $table->string('bank')->nullable();
      $table->string('status_type')->nullable();
      $table->string('approval_code')->nullable();
      $table->string('edu_status')->nullable();
      $table->string('secured_3ds_status')->nullable();
      $table->string('verify_score')->nullable();
      $table->string('response_code')->nullable();
      $table->string('ch_name')->nullable();
      $table->string('brand')->nullable();
      $table->string('verify_status')->nullable();
      $table->string('session_id')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('payment_doku_cc_installment_payments');
  }
}
