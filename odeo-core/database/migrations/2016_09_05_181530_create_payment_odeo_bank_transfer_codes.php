<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentOdeoBankTransferCodes extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('payment_odeo_bank_transfer_codes', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('code');
      $table->decimal('amount', 17, 2);
      $table->decimal('total', 17, 2);
      $table->string('bank');

      $table->unsignedBigInteger('config_id');
      $table->foreign('config_id')
        ->references('id')->on('payment_odeo_bank_transfer_code_configs')->onDelete('set null');

      $table->unsignedBigInteger('order_id');
      $table->foreign('order_id')
        ->references('id')->on('orders')
        ->onDelete('cascade');

      $table->index(['code', 'total', 'amount', 'bank']);

      $table->timestamps();

      $table->timestamp('expired_at');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('payment_odeo_bank_transfer_codes');
  }
}
