<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankAccountInquiriesAddVendorDisbursementReferenceId extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('bank_account_inquiries', function (Blueprint $table) {
      $table->bigInteger('vendor_disbursement_reference_id')->unsigned()->nullable();
      $table->decimal('cost', 17, 2)->default(0);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}
