<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReportsAddSalesValue extends Migration {

  public function up() {
    Schema::table('sales_by_payment_reports', function (Blueprint $table) {
      $table->decimal('sales_value', 17, 2)->unsigned()->default(0);
    });

    Schema::table('sales_platform_reports', function (Blueprint $table) {
      $table->decimal('sales_value', 17, 2)->unsigned()->default(0);
    });
  }

  public function down() {
    Schema::table('sales_by_payment_reports', function (Blueprint $table) {
      $table->dropColumn('sales_value');
    });

    Schema::table('sales_platform_reports', function (Blueprint $table) {
      $table->dropColumn('sales_value');
    });
  }
}
