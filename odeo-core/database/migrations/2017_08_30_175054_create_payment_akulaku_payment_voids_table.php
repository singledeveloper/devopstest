<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentAkulakuPaymentVoidsTable extends Migration
{
    public function up()
    {
        Schema::create('payment_akulaku_payment_voids', function(Blueprint $table) {
          $table->increments('id');
          $table->integer('akulaku_payment_id')->unsigned();
          $table->foreign('akulaku_payment_id')->references('id')->on('payment_akulaku_payments');
          $table->bigInteger('order_id')->nullable();
          $table->text('log_request')->nullable();
          $table->text('log_response')->nullable();
          $table->timestamps();
        });
    }

    public function down()
    {
      Schema::drop('payment_akulaku_payment_voids');
    }
}
