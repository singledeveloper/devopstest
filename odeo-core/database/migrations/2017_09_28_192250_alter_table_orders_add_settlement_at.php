<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrdersAddSettlementAt extends Migration {

  public function up() {
    Schema::table('orders', function (Blueprint $table) {
      $table->timestamp('settlement_at')->nullable();
    });
  }


  public function down() {
    Schema::table('orders', function (Blueprint $table) {
      $table->dropColumn('settlement_at');
    });
  }
}
