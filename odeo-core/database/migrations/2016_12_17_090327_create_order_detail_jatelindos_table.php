<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailJatelindosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail_jatelindos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_detail_pulsa_switcher_id');
            $table->foreign('order_detail_pulsa_switcher_id')->references('id')->on('order_detail_pulsa_switchers')->onDelete('cascade');
            $table->char('transaction_id', 6);
            $table->char('status', 5)->default('10000');
            $table->text('log_purchase_request')->nullable();
            $table->text('log_purchase_response')->nullable();
            $table->text('log_advice_request')->nullable();
            $table->text('log_advice_response')->nullable();
            $table->timestamps();

        });
    
        DB::update("ALTER SEQUENCE order_detail_jatelindos_id_seq RESTART WITH 1000000000");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_detail_jatelindos');
    }
}
