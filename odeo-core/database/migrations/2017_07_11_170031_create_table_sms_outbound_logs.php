<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSmsOutboundLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_outbound_logs', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('sms_switcher_id')->nullable();
          $table->foreign('sms_switcher_id')->references('id')->on('sms_switchers')->onDelete('cascade');
          $table->string('outbound_to', 255);
          $table->string('number', 20);
          $table->text('sms_text');
          $table->text('log_response')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
