<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBannersReconstructV1 extends Migration {

  public function up() {
    Schema::table('banners', function (Blueprint $table) {
      $table->dropColumn('name');
      $table->dropColumn('description');
      $table->dropColumn('url');
      $table->unsignedBigInteger('owner_id');
      $table->string('owner_type');
      $table->string('trigger');
      $table->boolean('active')->default(false);
      $table->unsignedInteger('priority')->default(0);
      $table->string('desktop_url')->nullable();
      $table->string('mobile_url')->nullable();
    });
  }


  public function down() {
    Schema::table('banners', function (Blueprint $table) {
      $table->string('name');
      $table->string('description');
    });
  }
}
