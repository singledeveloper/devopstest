<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCashTransactionsAddTransactionKeyReferenceIdReferenceType extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('cash_transactions', function (Blueprint $table) {
      $table->string('transaction_key', 60)->nullable();
      $table->string('reference_type', 60)->nullable();
      $table->bigInteger('reference_id')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('cash_transactions', function (Blueprint $table) {
      //
    });
  }
}
