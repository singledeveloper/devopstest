<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTempCashesTable extends Migration {

  public function up() {
    Schema::create('user_temp_cashes', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->bigInteger('cash_transaction_id')->unsigned();
      $table->foreign('cash_transaction_id')->references('id')->on('cash_transactions')->onDelete('set null');
      $table->decimal('amount', 17, 2)->default(0);
      $table->timestamp('created_at');
      $table->timestamp('settlement_at');
    });
  }


  public function down() {
    Schema::drop('user_temp_cashes');
  }
}
