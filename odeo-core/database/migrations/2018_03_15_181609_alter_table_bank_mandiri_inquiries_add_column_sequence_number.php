<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankMandiriInquiriesAddColumnSequenceNumber extends Migration {

  public function up() {
    Schema::table('bank_mandiri_inquiries', function (Blueprint $table) {
      $table->bigInteger('sequence_number', false, true)->nullable();
    });
  }

  public function down() {
    Schema::table('bank_mandiri_inquiries', function (Blueprint $table) {
      //
    });
  }
}
