<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDartmediaLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sms_dartmedia_logs', function(Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('sms_switcher_id')->nullable();
        $table->foreign('sms_switcher_id')->references('id')->on('sms_switchers')->onDelete('cascade');
        $table->string('to')->nullable();
        $table->string('sms_text')->nullable();
        $table->text('xml_response')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
