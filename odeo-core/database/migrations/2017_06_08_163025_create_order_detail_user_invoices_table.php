<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailUserInvoicesTable extends Migration {

  public function up() {
    Schema::create('order_detail_user_invoices', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('order_detail_id');
      $table->foreign('order_detail_id')->references('id')->on('order_details')->onDelete('cascade');
      $table->string('invoice_number', 16)->unique();
      $table->integer('biller')->unsigned();
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('order_detail_user_invoices');
  }
}
