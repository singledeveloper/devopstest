<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVirtualAccountMaps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('virtual_account_maps')) {
            return;
        }

        Schema::create('virtual_account_maps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('channel_type')->nullable();
            $table->string('prefix')->nullable();
            $table->bigInteger('va_vendor_id')->nullable();
            $table->foreign('va_vendor_id')->references('id')->on('virtual_account_vendors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virtual_account_maps');
    }
}
