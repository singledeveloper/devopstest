<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentMandiriVaPaymentsAddIsReconciled extends Migration
{
    public function up()
    {
      Schema::table('payment_mandiri_va_payments', function(Blueprint $table){
        $table->boolean('is_reconciled')->default(false);
      });
    }

    public function down()
    {
        //
    }
}
