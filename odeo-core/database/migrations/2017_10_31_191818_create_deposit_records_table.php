<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositRecordsTable extends Migration {

  public function up() {
    Schema::create('deposit_records', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->decimal('amount', 17, 2);

      $table->bigInteger('store_id');
      $table->foreign('store_id')
        ->references('id')
        ->on('stores')
        ->onDelete('cascade');

      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('deposit_records');
  }
}
