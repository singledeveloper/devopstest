<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePostpaidOdeos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('postpaid_odeos', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('vendor_switcher_id')->unsigned();
        $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('cascade');
        $table->integer('service_detail_id')->unsigned();
        $table->foreign('service_detail_id')->references('id')->on('service_details')->onDelete('cascade');
        $table->string('fee_type', 20);
        $table->string('code', 20);
        $table->integer('fee');
        $table->boolean("is_active")->default(true);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('postpaid_odeos');
    }
}
