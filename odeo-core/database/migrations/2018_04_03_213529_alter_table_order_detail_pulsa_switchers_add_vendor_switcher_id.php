<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailPulsaSwitchersAddVendorSwitcherId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_detail_pulsa_switchers', function(Blueprint $table) {
          $table->integer('vendor_switcher_id')->nullable();
          $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('set null');
          $table->index(['vendor_switcher_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
