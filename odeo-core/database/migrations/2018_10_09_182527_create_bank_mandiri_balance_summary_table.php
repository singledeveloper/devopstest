<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankMandiriBalanceSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('bank_mandiri_inquiry_balance_summaries', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->decimal('balance_change', 17, 2);
        $table->decimal('previous_balance', 17, 2);
        $table->decimal('balance', 17, 2);
        $table->decimal('scrape_current_balance', 17, 2);
        $table->date('scrape_start_date');
        $table->date('scrape_end_date');
        $table->char('status', 5);
        $table->bigInteger('first_distinct_id')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
