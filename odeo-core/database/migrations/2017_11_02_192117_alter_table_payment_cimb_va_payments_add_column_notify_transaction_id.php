<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentCimbVaPaymentsAddColumnNotifyTransactionId extends Migration {

  public function up() {
    Schema::table('payment_cimb_va_payments', function (Blueprint $table) {
      $table->string('notify_transaction_id')->nullable();
    });
  }


  public function down() {
    Schema::table('payment_cimb_va_payments', function (Blueprint $table) {
      //
    });
  }
}
