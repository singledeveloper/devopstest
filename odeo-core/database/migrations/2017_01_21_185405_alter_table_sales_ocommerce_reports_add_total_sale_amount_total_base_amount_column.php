<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSalesOcommerceReportsAddTotalSaleAmountTotalBaseAmountColumn extends Migration {

  public function up() {
    Schema::table('sales_ocommerce_reports', function (Blueprint $table) {
      $table->decimal('total_sale_amount', 17, 2)->unsigned()->default(0);
      $table->decimal('total_base_amount', 17, 2)->unsigned()->default(0);
    });
  }


  public function down() {
    Schema::table('sales_ocommerce_reports', function (Blueprint $table) {
      $table->dropColumn('total_sale_amount');
      $table->dropColumn('total_base_amount');
    });
  }
}
