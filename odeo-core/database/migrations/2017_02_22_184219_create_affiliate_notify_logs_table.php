<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateNotifyLogsTable extends Migration {

  public function up() {
    Schema::create('affiliate_notify_logs', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->bigInteger('order_id')->unsigned();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
      $table->string('status')->nullable();
      $table->integer('response_status_code')->nullable();
      $table->text('response_reason')->nullable();
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('affiliate_notify_logs');
  }
}
