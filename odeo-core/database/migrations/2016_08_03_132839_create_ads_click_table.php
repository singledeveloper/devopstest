<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsClickTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_clicks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ads_id');    
            $table->foreign('ads_id')->references('id')->on('ads')->onDelete('set null');
            $table->bigInteger('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
            $table->string('ip');
            $table->string('user_agent');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads_clicks');
    }
}
