<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePulsaOdeoInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulsa_odeo_inventories', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('operator_id')->unsigned();
            $table->foreign('operator_id')->references('id')->on('pulsa_operators')->onDelete('cascade');
            $table->integer('vendor_switcher_id')->unsigned();
            $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('cascade');
            $table->string('code', 20);
            $table->string("category", 50)->default('');
            $table->string('name', 50)->default('');
            $table->integer('nominal');
            $table->integer('base_price');
            $table->boolean("is_active")->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pulsa_odeo_inventories');
    }
}
