<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserLoginHistoriesAddClientInfoAndRoute extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('user_login_histories', function (Blueprint $table) {
      $table->integer('platform_id')->nullable();
      $table->renameColumn('use_android_version', 'client_version');
      $table->string('route')->nullable()->default('login');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('user_login_histories', function (Blueprint $table) {
      $table->dropColumn('platform_id');
      $table->renameColumn('client_version', 'use_android_version');
      $table->dropColumn('route');
    });
  }
}
