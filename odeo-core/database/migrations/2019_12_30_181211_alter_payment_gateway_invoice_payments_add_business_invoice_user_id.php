<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentGatewayInvoicePaymentsAddBusinessInvoiceUserId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payment_gateway_invoice_payments', function(Blueprint $table){
        $table->bigInteger('business_invoice_user_id')->nullable();
        $table->foreign('business_invoice_user_id')->references('id')->on('business_invoice_users')->onDelete('set null');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
