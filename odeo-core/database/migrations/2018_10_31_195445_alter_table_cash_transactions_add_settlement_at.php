<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCashTransactionsAddSettlementAt extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('cash_transactions', function (Blueprint $table) {
      $table->timestamp('settlement_at')->nullable();
    });

    DB::statement('UPDATE cash_transactions SET settlement_at = to_timestamp(data :: JSON ->> \'settlement_at\', \'YYYY-MM-DD HH24:MI:SS\') WHERE data IS NOT NULL AND data <> \'\' AND data :: JSON ->> \'settlement_at\' IS NOT NULL');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {

  }
}
