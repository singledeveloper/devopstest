<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRegisteredUserReportsAddUserTypeColumn extends Migration {

  public function up() {
    Schema::table('registered_user_reports', function (Blueprint $table) {
      $table->string('user_type')->nullable();
    });
  }

  public function down() {
    Schema::table('registered_user_reports', function (Blueprint $table) {
      $table->dropColumn("user_type");
    });
  }
}
