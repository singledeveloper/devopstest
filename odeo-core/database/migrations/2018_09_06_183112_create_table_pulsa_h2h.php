<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePulsaH2h extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulsa_h2h_groups', function(Blueprint $table){
          $table->increments('id');
          $table->string('name', 50);
          $table->text('description')->nullable();
          $table->boolean('is_active')->default(true);
          $table->timestamps();
        });

        Schema::create('pulsa_h2h_users', function(Blueprint $table){
          $table->increments('id');
          $table->bigInteger('user_id');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
          $table->integer('pulsa_h2h_group_id');
          $table->foreign('pulsa_h2h_group_id')->references('id')->on('pulsa_h2h_groups')->onDelete('cascade');
          $table->timestamps();
          $table->index(['user_id']);
          $table->index(['pulsa_h2h_group_id']);
        });

        Schema::create('pulsa_h2h_group_details', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->integer('pulsa_h2h_group_id');
          $table->foreign('pulsa_h2h_group_id')->references('id')->on('pulsa_h2h_groups')->onDelete('cascade');
          $table->integer('pulsa_odeo_id');
          $table->foreign('pulsa_odeo_id')->references('id')->on('pulsa_odeos')->onDelete('cascade');
          $table->integer('price');
          $table->timestamps();
          $table->index(['pulsa_odeo_id']);
          $table->index(['pulsa_h2h_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
