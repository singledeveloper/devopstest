<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentKredivoPaymentVoidsTable extends Migration {

  public function up() {
    Schema::create('payment_kredivo_payment_voids', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('kredivo_payment_id')->unsigned();
      $table->foreign('kredivo_payment_id')->references('id')->on('payment_kredivo_payments');
      $table->string('status')->nullable();
      $table->string('fraud_status')->nullable();
      $table->bigInteger('order_id')->nullable();
      $table->string('transaction_time')->nullable();
      $table->decimal('amount', 17, 2)->nullable();
      $table->string('payment_type')->nullable();
      $table->string('transaction_status')->nullable();
      $table->string('message')->nullable();
      $table->string('transaction_id')->nullable();
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('payment_kredivo_payment_voids');
  }
}
