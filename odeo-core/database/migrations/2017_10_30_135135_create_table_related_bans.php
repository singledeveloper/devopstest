<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRelatedBans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_banned_bank_accounts', function(Blueprint $table){
          $table->increments('id');
          $table->integer('bank_id');
          $table->foreign('bank_id')->references('id')->on('banks')->onDelete('set null');
          $table->string('account_number', 255);
          $table->boolean('is_banned')->default(true);
          $table->text('banned_reason')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
