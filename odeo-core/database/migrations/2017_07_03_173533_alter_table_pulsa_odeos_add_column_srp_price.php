<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaOdeosAddColumnSrpPrice extends Migration {

  public function up() {
    Schema::table('pulsa_odeos', function (Blueprint $table) {
      $table->integer('srp_price')->default(0);
    });
  }

  public function down() {
    Schema::table('pulsa_odeos', function (Blueprint $table) {
      $table->dropColumn('srp_price');
    });
  }
}
