<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomeOutcomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('income_outcomes', function (Blueprint $table) {
        $table->date('date')->primary();
        $table->integer('income')->unsigned()->default(0);
        $table->integer('outcome')->unsigned()->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('income_outcomes');
    }
}
