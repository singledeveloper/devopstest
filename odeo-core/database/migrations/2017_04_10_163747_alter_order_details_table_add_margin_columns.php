<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderDetailsTableAddMarginColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_details', function (Blueprint $table) {
        $table->decimal('merchant_price', 17, 2)->default(0);
        $table->decimal('mentor_margin', 10, 2)->default(0);
        $table->decimal('leader_margin', 10, 2)->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('order_details', function (Blueprint $table) {
        $table->dropColumn('merchant_price');
        $table->dropColumn('mentor_margin');
        $table->dropColumn('leader_margin');
      });
    }
}
