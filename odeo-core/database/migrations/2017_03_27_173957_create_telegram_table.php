<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelegramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_users', function(Blueprint $table){
          $table->increments('id');
          $table->integer('chat_id');
          $table->bigInteger('user_id')->nullable();
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
          $table->string('name', 50)->nullable();
          $table->timestamps();
        });

        Schema::create('telegram_histories', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->integer('chat_id');
          $table->integer('update_id');
          $table->integer('timestamp');
          $table->text('message');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telegram_users');
        Schema::drop('telegram_histories');
    }
}
