<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlnInquiryUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('pln_inquiry_users', function(Blueprint $table){
        $table->increments('id');
        $table->integer('pln_inquiry_id');
        $table->foreign('pln_inquiry_id')->references('id')->on('pln_inquiries');
        $table->bigInteger('user_id')->unsigned();
        $table->foreign('user_id')->references('id')->on('users');
        $table->timestamps();
        $table->index(['pln_inquiry_id', 'user_id']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
