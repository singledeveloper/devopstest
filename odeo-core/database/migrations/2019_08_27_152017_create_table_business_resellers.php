<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBusinessResellers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_resellers', function(Blueprint $table){
            $table->increments('id');
            $table->bigInteger('reseller_user_id');
            $table->bigInteger('user_id');
            $table->string('fee_type', 50);
            $table->integer('fee_value');
            $table->string('scope', 255);
            $table->timestamps();
            $table->index(['user_id']);
            $table->index(['reseller_user_id']);
          });
  
          Schema::create('business_reseller_transactions', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->char('transaction_date', 10);
            $table->string('type', 50);
            $table->integer('qty');
            $table->bigInteger('total_amount');
            $table->bigInteger('total_fee');
            $table->bigInteger('total_cost');
            $table->boolean('is_processed')->default(false);
            $table->timestamps();
            $table->index(['transaction_date']);
            $table->index(['type']);
          });
  
          Schema::create('business_reseller_bonuses', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('reseller_user_id');
            $table->integer('business_reseller_id')->nullable();
            $table->foreign('business_reseller_id')->references('id')->on('business_resellers');
            $table->integer('business_reseller_transaction_id');
            $table->foreign('business_reseller_transaction_id')->references('id')->on('business_reseller_transactions');
            $table->bigInteger('reseller_fee');
            $table->boolean('is_sent')->default(false);
            $table->timestamps();
            $table->index(['business_reseller_id']);
            $table->index(['business_reseller_transaction_id']);
          });
  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
