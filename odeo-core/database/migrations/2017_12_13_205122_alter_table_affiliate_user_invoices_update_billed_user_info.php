<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAffiliateUserInvoicesUpdateBilledUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('affiliate_user_invoices', function(Blueprint $table){
        $table->renameColumn('billed_qluster_user_id', 'billed_user_id');
        $table->renameColumn('billed_qluster_user_name', 'billed_user_name');
        $table->string('billed_user_email', 50)->nullable();
        $table->string('billed_user_telephone', 20)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('affiliate_user_invoices', function(Blueprint $table){
        $table->renameColumn('billed_user_id', 'billed_qluster_user_id');
        $table->renameColumn('billed_user_name', 'billed_qluster_user_name');
        $table->dropColumn('billed_user_email');
        $table->dropColumn('billed_user_telephone');
      });
    }
}
