<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCashRecurrings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_recurrings', function(Blueprint $table){
          $table->increments('id');
          $table->bigInteger('user_id');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
          $table->string('transaction_type', 50);
          $table->integer('bank_id');
          $table->string('destination_number', 50);
          $table->string('type', 50);
          $table->bigInteger('amount');
          $table->text('template_notes')->nullable();
          $table->bigInteger('approval_user_id')->nullable();
          $table->boolean('is_enabled')->default(false);
          $table->boolean('is_active')->default(true);
          $table->timestamp('last_executed_at')->nullable();
          $table->timestamps();
          $table->index(['user_id']);
          $table->index(['transaction_type']);
          $table->index(['bank_id']);
          $table->index(['type']);
        });

        Schema::create('cash_recurring_histories', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->integer('cash_recurring_id');
          $table->foreign('cash_recurring_id')->references('id')->on('cash_recurrings')->onDelete('set null');
          $table->bigInteger('reference_id')->nullable();
          $table->string('reference_type', 50);
          $table->char('status', 5);
          $table->text('error_message')->nullable();
          $table->timestamps();
          $table->index(['cash_recurring_id']);
          $table->index(['status']);
          $table->index(['reference_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
