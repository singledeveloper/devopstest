<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailCreditBillsTable extends Migration {

  public function up() {
    Schema::create('order_detail_credit_bills', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('order_detail_id');
      $table->foreign('order_detail_id')->references('id')->on('order_details')->onDelete('cascade');
      $table->integer('credit_bill_id')->unsigned();
      $table->tinyInteger('biller');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('order_detail_credit_bills');
  }
}
