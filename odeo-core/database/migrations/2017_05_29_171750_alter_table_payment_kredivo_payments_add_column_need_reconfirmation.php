<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentKredivoPaymentsAddColumnNeedReconfirmation extends Migration
{

    public function up()
    {
        Schema::table('payment_kredivo_payments', function (Blueprint $table) {
          $table->boolean('need_reconfirmation')->default(false);
        });
    }


    public function down()
    {
        Schema::table('payment_kredivo_payments', function (Blueprint $table) {
          $table->dropColumn('need_reconfirmation');
        });
    }
}
