<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentArtajasaCallbackLogs extends Migration {

  public function up() {
    Schema::create('payment_artajasa_callback_logs', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->text('body');
      $table->string('type');
      $table->timestamps();
    });
  }


  public function down() {
    Schema::dropIfExists('payment_artajasa_callback_logs');
  }
}
