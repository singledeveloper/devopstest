<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePulsaOdeoReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulsa_odeo_reports', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->char('report_date', 10);
          $table->string('report_type', 50);
          $table->integer('total');
          $table->text('additional_data')->nullable();
          $table->timestamps();
          $table->index(['report_date']);
          $table->index(['report_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
