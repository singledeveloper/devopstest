<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBniEdcTransactions extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('bni_edc_transactions', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('bni_edc_settlement_id')->nullable(true);
      $table->foreign('bni_edc_settlement_id')->references('id')->on('bni_edc_settlements')->onDelete('cascade');
      $table->date('proc_date');
      $table->string('mid');
      $table->string('ob');
      $table->string('gb');
      $table->string('seq');
      $table->string('type');
      $table->date('trx_date');
      $table->string('approval_code');
      $table->string('card_no');
      $table->decimal('amount', 17, 2);
      $table->string('tid');
      $table->string('trx_type');
      $table->string('ptr');
      $table->decimal('mdr_pct', 5, 2);
      $table->decimal('mdr', 17, 2);
      $table->decimal('air_fare', 17, 2);
      $table->string('plan');
      $table->decimal('ss_amount', 17, 2);
      $table->string('ss_fee_type');
      $table->string('flag');
      $table->decimal('net_amount', 17, 2);
      $table->string('merchant_account');
      $table->string('merchant_name');
      $table->decimal('trx_fee', 17, 2);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
//    Schema::dropIfExists('bni_edc_transactions');
  }
}
