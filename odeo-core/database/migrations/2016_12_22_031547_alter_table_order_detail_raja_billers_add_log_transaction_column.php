<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailRajaBillersAddLogTransactionColumn extends Migration {

  public function up() {
    Schema::table('order_detail_raja_billers', function (Blueprint $table) {
      $table->text('log_transaction')->nullable()->after('log_response');
    });
  }

  public function down() {
    Schema::table('order_detail_raja_billers', function (Blueprint $table) {
      $table->dropColumn('log_transaction');
    });
  }
}
