<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersAddPurchasePreferredStoreIdColumn extends Migration {

  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->bigInteger('purchase_preferred_store_id')->unsigned()->nullable();
      $table->foreign('purchase_preferred_store_id')->references('id')->on('stores')->onDelete('cascade');
    });
  }

  public function down() {
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn('store_referred_id');
    });
  }
}
