<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePulsaSupplies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulsa_supplies', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->string('supply_code', 10);
          $table->integer('pulsa_odeo_inventory_id');
          $table->foreign('pulsa_odeo_inventory_id')->references('id')->on('pulsa_odeo_inventories');
          $table->integer('store_base_price');
          $table->timestamps();
          $table->index(['supply_code']);
          $table->index(['pulsa_odeo_inventory_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
