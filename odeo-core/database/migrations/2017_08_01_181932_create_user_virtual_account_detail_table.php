<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVirtualAccountDetailTable extends Migration {

  public function up() {
    Schema::create('user_virtual_account_details', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('user_virtual_account_id');
      $table->foreign('user_virtual_account_id')->references('id')->on('user_virtual_accounts')->onDelete('cascade');
      $table->string('virtual_account_number', 16)->unique();
      $table->string('vendor');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('user_virtual_account_details');
  }
}
