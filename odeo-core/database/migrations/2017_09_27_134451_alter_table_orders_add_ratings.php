<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrdersAddRatings extends Migration {

  public function up() {
    Schema::table('orders', function (Blueprint $table) {
      $table->boolean('confirm_receipt')->default(false);
      $table->tinyInteger('rating')->nullable();
      $table->string('feedback')->default('');
      $table->timestamp('confirmed_at')->nullable();
    });
  }


  public function down() {
    Schema::table('orders', function (Blueprint $table) {
      $table->dropColumn('confirm_receipt');
      $table->dropColumn('rating');
      $table->dropColumn('feedback');
      $table->dropColumn('confirmed_at');
    });
  }
}
