<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVendorSwitcherReplenishments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('vendor_switcher_replenishments', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->integer('vendor_switcher_id');
        $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('set null');
        $table->bigInteger('amount');
        $table->bigInteger('balance_before');
        $table->bigInteger('auto_disbursement_reference_id')->unsigned()->nullable();
        $table->string('auto_disbursement_vendor')->nullable();
        $table->char('status', 5)->default('10000');
        $table->string('cancel_reason', 255)->nullable();
        $table->timestamp('verified_at')->nullable();
        $table->timestamp('cancelled_at')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
