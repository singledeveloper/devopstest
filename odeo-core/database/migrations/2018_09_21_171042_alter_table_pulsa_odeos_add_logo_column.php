<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaOdeosAddLogoColumn extends Migration {

  public function up() {
    Schema::table('pulsa_odeos', function (Blueprint $table) {
      $table->string('logo')->nullable();
    });

    DB::table('pulsa_odeos')->where('category', 'like', 'Axis%')->update(["logo" => "axis.png"]);
    DB::table('pulsa_odeos')->where('category', 'like', 'Bolt%')->update(["logo" => "bolt.png"]);
    DB::table('pulsa_odeos')->where('category', 'BPJS Kesehatan')->update(["logo" => "bpjs-kesehatan.png"]);
    DB::table('pulsa_odeos')->where('category', 'BPJS Ketenagakerjaan')->update(["logo" => "bpjs-ketenagakerjaan.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game Digicash')->update(["logo" => "digicash.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game Faveo')->update(["logo" => "faveo.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game GArena')->update(["logo" => "garena.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game Gemscool')->update(["logo" => "gemscool.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game Lyto')->update(["logo" => "lyto.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game Megaxus')->update(["logo" => "megaxus.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game Mobile Legends')->update(["logo" => "mobile-legend.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game Netmarble')->update(["logo" => "netmarble.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game OLEH4U')->update(["logo" => "olleh4u.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game Orange Game')->update(["logo" => "orange-game.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game Qeon')->update(["logo" => "qeon.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game VTC Online')->update(["logo" => "vtc.png"]);
    DB::table('pulsa_odeos')->where('category', 'Game Zynga')->update(["logo" => "zynga.png"]);
    DB::table('pulsa_odeos')->where('category', 'like', 'Indosat%')->update(["logo" => "indosat.png"]);
    DB::table('pulsa_odeos')->where('category', 'Matrix Indosat')->update(["logo" => "indosat.png"]);
    DB::table('pulsa_odeos')->where('category', 'Kode Voucher Google Play')->update(["logo" => "google-play.png"]);
    DB::table('pulsa_odeos')->where('category', 'PDAM')->update(["logo" => "pdam.png"]);
    DB::table('pulsa_odeos')->where('category', 'like', 'PLN%')->update(["logo" => "pln.png"]);
    DB::table('pulsa_odeos')->where('category', 'like', 'SmartFren%')->update(["logo" => "smartfren.png"]);
    DB::table('pulsa_odeos')->where('category', 'like', 'Smartfren%')->update(["logo" => "smartfren.png"]);
    DB::table('pulsa_odeos')->where('category', 'Telkom Flexi')->update(["logo" => "telkom.png"]);
    DB::table('pulsa_odeos')->where('category', 'like', 'Telkomsel%')->update(["logo" => "telkomsel.png"]);
    DB::table('pulsa_odeos')->where('category', 'Telkom Speedy')->update(["logo" => "speedy.png"]);
    DB::table('pulsa_odeos')->where('category', 'Telkom Telepon Rumah')->update(["logo" => "telkom.png"]);
    DB::table('pulsa_odeos')->where('category', 'like', 'Three%')->update(["logo" => "three.png"]);
    DB::table('pulsa_odeos')->where('category', 'Transportasi e-Toll')->update(["logo" => "e-toll.png"]);
    DB::table('pulsa_odeos')->where('category', 'Transportasi Gojek')->update(["logo" => "gojek.png"]);
    DB::table('pulsa_odeos')->where('category', 'Transportasi Grab')->update(["logo" => "grab.png"]);
    DB::table('pulsa_odeos')->where('category', 'Transportasi Grab Driver')->update(["logo" => "grab.png"]);
    DB::table('pulsa_odeos')->where('category', 'like', 'XL%')->update(["logo" => "xl.png"]);

  }

  public function down() {
    //
  }
}
