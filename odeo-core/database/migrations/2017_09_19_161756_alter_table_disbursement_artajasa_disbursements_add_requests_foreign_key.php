<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTableDisbursementArtajasaDisbursementsAddRequestsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('disbursement_artajasa_disbursements', function (Blueprint $table) {
        $table->bigInteger('disbursement_artajasa_transfer_id')
          ->nullable()->unsigned();
        $table->foreign('disbursement_artajasa_transfer_id', 'transfer_id')
          ->references('id')->on('disbursement_artajasa_transfers')
          ->onDelete('set null');

        $table->bigInteger('disbursement_artajasa_transfer_inquiry_id')
          ->nullable()->unsigned();
        $table->foreign('disbursement_artajasa_transfer_inquiry_id', 'transfer_inquiry_id')
          ->references('id')->on('disbursement_artajasa_transfer_inquiries')
          ->onDelete('set null');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('disbursement_artajasa_disbursements', function (Blueprint $table) {
        $table->dropForeign('transfer_id');
        $table->dropForeign('transfer_inquiry_id');
        $table->dropColumn('disbursement_artajasa_transfer_id');
        $table->dropColumn('disbursement_artajasa_transfer_inquiry_id');
      });
    }
}
