<?php

use Illuminate\Database\Migrations\Migration;

class UpdateBanksAjBankCode extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    $sql = <<<SQL
update banks set aj_bank_code='041', aj_regency_code='JKT' where id=19;
update banks set aj_bank_code='774', aj_regency_code='JKT' where id=23;
update banks set aj_bank_code='011', aj_regency_code='JKT' where id=26;
update banks set aj_bank_code='114', aj_regency_code='SBY' where id=75;
update banks set aj_bank_code='028', aj_regency_code='JKT' where id=85;
update banks set aj_bank_code='484', aj_regency_code='JKT' where id=87;
update banks set aj_bank_code='023', aj_regency_code='JKT' where id=88;
update banks set aj_bank_code='009', aj_regency_code='JKT' where id=97;
update banks set aj_bank_code='213', aj_regency_code='SBY' where id=117;
update banks set aj_bank_code='061', aj_regency_code='JKT' where id=118;
update banks set aj_bank_code='777', aj_regency_code='JKT' where id=125;      
SQL;

    DB::unprepared($sql);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}
