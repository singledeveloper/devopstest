<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserOtpAttemptsChangeColumnOtpTo6Char extends Migration {

  public function up() {
    Schema::table('user_otp_attempts', function (Blueprint $table) {
      $table->string('otp', 6)->change();
    });
  }

  public function down() {
    Schema::table('user_otp_attempts', function (Blueprint $table) {
      //
    });
  }
}
