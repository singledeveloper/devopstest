<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCashNotifyLogs extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('cash_notify_logs', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id');
      $table->string('reference_type');
      $table->bigInteger('reference_id');
      $table->integer('status_code');
      $table->string('url');
      $table->boolean('is_success');
      $table->text('request_dump')->nullable();
      $table->text('response_dump')->nullable();
      $table->text('error_log')->nullable();
      $table->timestamp('notified_at')->nullable();
      $table->timestamp('created_at');
      $table->timestamp('updated_at');

      $table->index(['reference_type', 'reference_id', 'is_success']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
//    Schema::dropIfExists('cash_webhooks_logs');
  }
}
