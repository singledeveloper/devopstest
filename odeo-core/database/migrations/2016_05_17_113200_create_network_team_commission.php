<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworkTeamCommission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('network_team_commissions', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
            $table->integer('first_amount')->unsigned();
            $table->timestamp('first_amount_claimed_at')->nullable();
            $table->integer('second_amount')->unsigned();
            $table->timestamp('second_amount_claimed_at')->nullable();
            $table->char('day_limit', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('network_team_commissions');
    }
}
