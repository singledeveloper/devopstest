<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTcashUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tcash_users', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('store_id')->nullable();
        $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
        $table->string('telephone', 50);
        $table->string('imei', 255)->nullable();
        $table->string('imsi', 255)->nullable();
        $table->string('customer_id', 255)->nullable();
        $table->integer('current_balance')->default(0);
        $table->string('pin', 255)->nullable();
        $table->string('enc_token_key', 255)->nullable();
        $table->string('session_key', 255)->nullable();
        $table->text('cookie_jar')->nullable();
        $table->text('last_error_message')->nullable();
        $table->char('status', 5);
        $table->boolean('is_active')->default(true);
        $table->timestamps();
        $table->index(['store_id']);
        $table->index(['telephone']);
        $table->index(['status']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
