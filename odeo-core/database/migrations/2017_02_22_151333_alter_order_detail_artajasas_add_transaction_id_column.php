<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderDetailArtajasasAddTransactionIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_artajasas', function (Blueprint $table) {
        $table->char('transaction_id', 6)->default('000000')->after('switcher_reference_id');
        $table->text('log_reversal')->nullable()->after('log_response');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('order_detail_artajasas', function (Blueprint $table) {
        $table->dropColumn('transaction_id');
        $table->dropColumn('log_reversal');
      });
    }
}
