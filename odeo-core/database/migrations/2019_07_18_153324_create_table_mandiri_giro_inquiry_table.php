<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMandiriGiroInquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('bank_mandiri_giro_inquiries', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->integer('bank_scrape_account_number_id')->unsigned();
        $table->foreign('bank_scrape_account_number_id')->references('id')->on('users');
        $table->string('date')->nullable();
        $table->string('date_value')->nullable();
        $table->string('description')->nullable();
        $table->string('reference_number')->nullable();
        $table->string('reference')->nullable();
        $table->string('reference_type')->nullable();
        $table->decimal('credit', 17, 2)->nullable();
        $table->decimal('debit', 17, 2)->nullable();
        $table->decimal('balance', 17, 2)->nullable();
        $table->unsignedBigInteger('sequence_number')->nullable();

        $table->timestamps();
        $table->softDeletes();

        $table->index(['date', 'date_value', 'reference_type', 'reference_number', 'reference', 'balance', 'credit', 'debit', 'sequence_number']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
