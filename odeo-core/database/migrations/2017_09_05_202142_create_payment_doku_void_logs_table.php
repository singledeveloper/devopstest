<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDokuVoidLogsTable extends Migration
{
  public function up() {
    Schema::create('payment_doku_void_logs', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedBigInteger('reference_id');
      $table->string('reference_type');
      $table->text('log_request')->nullable();
      $table->text('log_response')->nullable();
      $table->boolean('voided')->default(true);
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('payment_doku_void_logs');
  }
}
