<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCashApiUsers extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('cash_api_users', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users');
      $table->decimal('withdraw_fee', 8, 2);
      $table->string('callback_url');
      $table->string('signing_key');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
