<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDokuBcaClickpayPaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_doku_bca_clickpay_payment_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('reference_id')->unsigned();
            $table->foreign('reference_id')->references('id')->on('payment_doku_bca_clickpay_payments')->onDelete('set null');

            $table->string('status_type')->nullable();
            $table->string('response_code')->nullable();
            $table->string('payment_channel')->nullable();
            $table->string('words')->nullable();
            $table->string('session_id')->nullable();
            $table->string('process_step')->nullable();
            $table->string('approval_code')->nullable();
            $table->string('result_msg')->nullable();
            $table->string('bank')->nullable();
            $table->string('mcn')->nullable();
            $table->timestamp('payment_date_time')->nullable();
            $table->string('verify_id')->nullable();
            $table->string('verify_score')->nullable();
            $table->string('verify_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_doku_bca_clickpay_payment_details');
    }
}
