<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDokuVaDirectPaymentTable extends Migration {

  public function up() {
    Schema::create('payment_doku_va_direct_payments', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('mall_id');
      $table->integer('chain_merchant')->nullable();
      $table->integer('payment_channel');
      $table->string('payment_code', 16);
      $table->string('inquiry_words');
      $table->decimal('amount', 15, 2);
      $table->integer('order_id');
      $table->foreign('order_id')->references('id')->on('orders');
      $table->string('inquiry_response_words');
      $table->string('session_id');
      $table->text('additional_data');
      $table->string('payment_date_time')->nullable();
      $table->string('liability')->nullable();
      $table->string('notify_words')->nullable();
      $table->string('result_msg')->nullable();
      $table->string('verify_id')->nullable();
      $table->string('bank')->nullable();
      $table->string('status_type')->nullable();
      $table->string('approval_code')->nullable();
      $table->string('edu_status')->nullable();
      $table->string('secured_3ds_status')->nullable();
      $table->string('verify_score')->nullable();
      $table->string('notify_response_code')->nullable();
      $table->string('verify_status')->nullable();
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('payment_doku_va_direct_payments');
  }
}
