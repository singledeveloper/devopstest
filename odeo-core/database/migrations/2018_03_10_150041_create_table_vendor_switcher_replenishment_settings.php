<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVendorSwitcherReplenishmentSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('vendor_switcher_replenishment_settings', function(Blueprint $table){
        $table->increments('id');
        $table->integer('vendor_switcher_id');
        $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('cascade');
        $table->string('route_type', 10); //beforesend, oncomplete
        $table->string('channel_type', 20); //telegram, email, jabber, constant_value
        $table->text('data', 255);
        $table->boolean('is_active')->default(true);
        $table->timestamps();
        $table->index(['vendor_switcher_id']);
      });

      Schema::create('vendor_switcher_replenishment_jabber_histories', function(Blueprint $table){
        $table->increments('id');
        $table->integer('vendor_switcher_id');
        $table->foreign('vendor_switcher_id')->references('id')->on('vendor_switchers')->onDelete('cascade');
        $table->text('log_request');
        $table->text('log_reply')->nullable();
        $table->char('status', 5);
        $table->timestamps();
        $table->index(['vendor_switcher_id']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
