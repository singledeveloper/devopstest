<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorDisbursementsTable extends Migration {

  public function up() {
    Schema::create('vendor_disbursements', function (Blueprint $table) {
      $table->integer('id')->unsigned()->primary();
      $table->string('name')->nullable();
      $table->decimal('balance', 17, 2);
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('vendor_disbursements');
  }
}
