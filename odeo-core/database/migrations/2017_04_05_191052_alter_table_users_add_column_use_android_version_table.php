<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersAddColumnUseAndroidVersionTable extends Migration {

  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->string('use_android_version')->nullable();
    });
  }

  public function down() {
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn('use_android_version');
    });
  }
}
