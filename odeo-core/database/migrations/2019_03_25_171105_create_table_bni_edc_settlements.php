<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBniEdcSettlements extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('bni_edc_settlements', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->date('trx_date');
      $table->decimal('total_amount', 17, 2);
      $table->decimal('trx_fee', 17, 2);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
//    Schema::dropIfExists('bni_edc_settlements');
  }
}
