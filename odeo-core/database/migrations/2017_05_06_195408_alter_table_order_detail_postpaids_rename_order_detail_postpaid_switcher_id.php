<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailPostpaidsRenameOrderDetailPostpaidSwitcherId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_postpaid_bpjs_kes_details', function (Blueprint $table){
        $table->dropForeign('order_detail_postpaid_bpjs_details_order_detail_postpaid_switcher_id_foreign');
        $table->renameColumn('order_detail_postpaid_switcher_id', 'order_detail_pulsa_switcher_id');
      });

      Schema::table('order_detail_postpaid_pln_details', function (Blueprint $table){
        $table->dropForeign('order_detail_postpaid_details_order_detail_postpaid_switcher_id_foreign');
        $table->renameColumn('order_detail_postpaid_switcher_id', 'order_detail_pulsa_switcher_id');
      });

      Schema::table('order_detail_postpaid_pulsa_details', function (Blueprint $table){
        $table->dropForeign('order_detail_postpaid_pulsa_details_order_detail_postpaid_switcher_id_foreign');
        $table->renameColumn('order_detail_postpaid_switcher_id', 'order_detail_pulsa_switcher_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
