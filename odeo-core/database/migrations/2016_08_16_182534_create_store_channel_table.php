<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('store_channels', function (Blueprint $table) {
        $table->increments('id');
        $table->string('code', 40)->nullable();
        $table->string('hash', 40)->nullable();
        $table->bigInteger('user_id')->unsigned();
        $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        $table->bigInteger('store_id')->unsigned();
        $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
        $table->string('channel_data');
        $table->string('channel_settings');
        $table->integer('daily_payment_limit')->default(80000000);
        $table->char('status', 5);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('store_channels');
    }
}
