<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTableDisbursementArtajasaDisbursementsDropUnusedColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('disbursement_artajasa_disbursements', function (Blueprint $table) {
        $table->dropColumn('transaction_id');
        $table->dropColumn('transaction_datetime');
        $table->dropColumn('inquiry_transaction_id');
        $table->dropColumn('inquiry_transaction_datetime');

        $table->dropColumn('ref_number');

        $table->dropColumn('response_code');
        $table->dropColumn('response_description');
        $table->dropColumn('inquiry_response_code');
        $table->dropColumn('inquiry_response_description');

        $table->dropColumn('response_datetime');
        $table->dropColumn('inquiry_response_datetime');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('disbursement_artajasa_disbursements', function (Blueprint $table) {
        $table->integer('transaction_id')->nullable();
        $table->dateTime('transaction_datetime')->nullable();
        $table->integer('inquiry_transaction_id')->nullable();
        $table->dateTime('inquiry_transaction_datetime')->nullable();

        $table->string('ref_number')->nullable();

        $table->string('response_code')->nullable();
        $table->text('response_description')->nullable();
        $table->string('inquiry_response_code')->nullable();
        $table->text('inquiry_response_description')->nullable();

        $table->dateTime('response_datetime')->nullable();
        $table->dateTime('inquiry_response_datetime')->nullable();
      });
    }
}
