<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentAkulakuPaymentsAddIsConfirmedColumn extends Migration {

  public function up() {
    Schema::table('payment_akulaku_payments', function (Blueprint $table) {
      $table->text('log_confirm')->nullable();
      $table->boolean('is_confirmed')->default(false);
    });
  }


  public function down() {
    Schema::table('payment_akulaku_payments', function (Blueprint $table) {
      $table->dropColumn('is_confirmed');
      $table->dropColumn('log_confirm');
    });
  }
}
