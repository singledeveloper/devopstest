<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailPostpaidBpjsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('order_detail_postpaid_bpjs_details', function (Blueprint $table) {
        $table->increments('id');
        $table->bigInteger('order_detail_postpaid_switcher_id');
        $table->foreign('order_detail_postpaid_switcher_id')->references('id')->on('order_detail_postpaid_switchers')->onDelete('cascade');
        $table->string('branch_code', 10);
        $table->string('branch_name', 50);
        $table->integer('month_counts');
        $table->integer('participant_counts');
        $table->integer('base_price');
        $table->integer('bill_rest');
        $table->integer('admin');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('order_detail_postpaid_bpjs_details');
    }
}
