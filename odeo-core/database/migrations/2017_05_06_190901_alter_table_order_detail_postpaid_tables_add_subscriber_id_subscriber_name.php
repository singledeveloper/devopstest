<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailPostpaidTablesAddSubscriberIdSubscriberName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_detail_postpaid_bpjs_kes_details', function (Blueprint $table){
          $table->string('subscriber_id', 50)->nullable();
          $table->string('subscriber_name', 50)->nullable();
        });

      Schema::table('order_detail_postpaid_pln_details', function (Blueprint $table){
        $table->string('subscriber_id', 50)->nullable();
        $table->string('subscriber_name', 50)->nullable();
      });

      Schema::table('order_detail_postpaid_pulsa_details', function (Blueprint $table){
        $table->string('subscriber_id', 50)->nullable();
        $table->string('subscriber_name', 50)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
