<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

  public function up() {
    Schema::create('users', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name', 50)->nullable();
      $table->string('email', 50)->unique()->nullable();
      $table->char('password', 60)->nullable();
      $table->string('telephone', 20)->unique()->nullable();
      $table->string('type', 10);
      $table->char('status', 5);
      $table->integer('login_counts')->default(0);
      $table->timestamps();
      $table->softDeletes();
    });
  }


  public function down() {
    Schema::drop('users');
  }
}
