<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserForgotsAddSecurityType extends Migration {

  public function up() {
    Schema::table('user_forgots', function (Blueprint $table) {
      $table->string('security_type')->nullable();
    });
  }


  public function down() {
    Schema::table('user_forgots', function (Blueprint $table) {

    });
  }
}
