<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTableUserRateAppsAddConfirmationNameColumn extends Migration {

  public function up() {
    Schema::table('user_rate_apps', function (Blueprint $table) {
      $table->string('confirmation_name');
    });
  }

  public function down() {
    Schema::table('user_rate_apps', function (Blueprint $table) {
      $table->dropColumn('confirmation_name');
    });
  }
}
