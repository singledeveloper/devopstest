<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTableDisbursementArtajasaStatusInquiriesAddTransferDataColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('disbursement_artajasa_status_inquiries', function (Blueprint $table) {
        $table->string('query_response_code')->nullable();
        $table->text('query_response_description')->nullable();
        $table->dropColumn('response_json');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('disbursement_artajasa_status_inquiries', function (Blueprint $table) {
        $table->text('response_json')->nullable();
        $table->dropColumn('query_response_code');
        $table->dropColumn('query_response_description');
      });
    }
}
