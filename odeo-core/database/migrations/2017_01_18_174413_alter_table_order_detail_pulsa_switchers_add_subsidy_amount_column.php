<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailPulsaSwitchersAddSubsidyAmountColumn extends Migration {

  public function up() {
    Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
      $table->integer("subsidy_amount")->default(0)->after('first_base_price');
    });
  }


  public function down() {
    Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
      $table->dropColumn("subsidy_amount");
    });
  }
}
