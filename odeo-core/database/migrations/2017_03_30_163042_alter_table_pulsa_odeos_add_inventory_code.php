<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaOdeosAddInventoryCode extends Migration {

  public function up() {
    Schema::table('pulsa_odeos', function (Blueprint $table) {
      $table->string('inventory_code', 10)->nullable();
    });
  }


  public function down() {
    Schema::table('pulsa_odeos', function (Blueprint $table) {
      $table->dropColumn('inventory_code');
    });
  }
}
