<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCompanyTransactionAdditionalInformationsAddReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_transaction_additional_informations', function(Blueprint $table){
          $table->bigInteger('cash_transaction_id')->nullable()->change();
          $table->string('reference_type', 255)->nullable();
          $table->bigInteger('reference_id')->nullable();
          $table->index(['reference_type']);
          $table->index(['reference_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
