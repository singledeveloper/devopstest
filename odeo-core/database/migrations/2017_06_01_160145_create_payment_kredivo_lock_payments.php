<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentKredivoLockPayments extends Migration {

  public function up() {
    Schema::create('payment_kredivo_lock_payments', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->bigInteger('order_id')->unsigned()->nullable();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
      $table->decimal('locked_amount', 17, 2);
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('payment_kredivo_lock_payments');
  }
}
