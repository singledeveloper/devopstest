<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailGameVoucherDetailsChangeTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('order_detail_voucher_game_details', 'order_detail_game_voucher_details');

        Schema::table('order_detail_game_voucher_details', function(Blueprint $table){
          $table->string('email', 50)->nullable()->change();
          $table->string('pin', 50)->nullable()->change();
          $table->integer('admin_fee', 50)->default(0)->change();
        });

        Schema::table('order_detail_postpaid_bpjs_kes_details', function(Blueprint $table){
          $table->renameColumn('admin', 'admin_fee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
