<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentPrioritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        Schema::create('agent_priorities', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('agent_id')->unsigned();
            $table->bigInteger('service_id')->unsigned();
            $table->smallInteger('priority');
            $table->boolean('is_selected')->default(false);
            $table->timestamps();

            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
        });

        foreach(Odeo\Domains\Constant\Service::getServicesForAgents() as $serviceId) {
            DB::unprepared("
                insert into agent_priorities(agent_id, service_id, priority, created_at, updated_at) 
                select id as agent_id, '".$serviceId."' as service_id, '1' as priority, now() as created_at, now() as updated_at
                from agents
            ");
        }
        
        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
