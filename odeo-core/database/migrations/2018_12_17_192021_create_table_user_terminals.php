<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserTerminals extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (Schema::hasTable('user_terminals')) {
      return;
    }

    Schema::create('user_terminals', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users');
      $table->integer('user_invoice_biller_id')->unsigned();
      $table->foreign('user_invoice_biller_id')->references('id')->on('user_invoice_billers');
      $table->string('terminal_id');
      $table->boolean('is_active');
      $table->timestamps();
      $table->index(['terminal_id', 'is_active']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}
