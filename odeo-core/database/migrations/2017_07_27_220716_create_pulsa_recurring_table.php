<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePulsaRecurringTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('pulsa_recurrings', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->integer('pulsa_odeo_id');
      $table->foreign('pulsa_odeo_id')->references('id')->on('pulsa_odeos')->onDelete('cascade');
      $table->string('destination_number', 20);
      $table->string('type', 20);
      $table->boolean('is_enabled')->default(true);
      $table->boolean('is_active')->default(true);
      $table->timestamps();
    });

    Schema::create('pulsa_recurring_histories', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('pulsa_recurring_id');
      $table->foreign('pulsa_recurring_id')->references('id')->on('pulsa_recurrings')->onDelete('cascade');
      $table->bigInteger('order_id')->nullable();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
      $table->char('status', 5)->default('10000');
      $table->text('error_message')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}
