<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentGatewayNotifyLogsAddPgChannelId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::transaction(function () {
        Schema::table('payment_gateway_notify_logs', function(Blueprint $table){
          $table->bigInteger('pg_user_channel_id')->nullable();
          $table->foreign('pg_user_channel_id')->references('id')->on('payment_gateway_user_payment_channels');
        });
        \DB::statement('update payment_gateway_notify_logs
          set pg_user_channel_id = payment_gateway_user_payment_channels.id
          from payment_gateway_user_payment_channels 
          where payment_gateway_user_payment_channels.pg_user_id = payment_gateway_notify_logs.pg_user_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
