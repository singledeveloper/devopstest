<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAffiliateUserInvoicesAddCreatedBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::transaction(function () {
        Schema::table('affiliate_user_invoices', function (Blueprint $table) {
          $table->bigInteger('created_by')->nullable();
          $table->foreign('created_by')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
        });
        \DB::statement("update affiliate_user_invoices set created_by = user_id");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
