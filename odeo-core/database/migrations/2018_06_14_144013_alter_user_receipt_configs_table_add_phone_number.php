<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserReceiptConfigsTableAddPhoneNumber extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('user_receipt_configs', function (Blueprint $table) {
      $table->string('phone_number', 20)->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('user_receipt_configs', function (Blueprint $table) {
      //
    });
  }
}
