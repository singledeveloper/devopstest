<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentPrismalinkVaDirectPaymentAddOrderId extends Migration
{
    public function up()
    {
      Schema::table('payment_prismalink_va_direct_payments', function(Blueprint $table){
        $table->bigInteger('order_id')->nullable();
        $table->string('virtual_account_number', 32)->nullable();
      });

      \DB::statement('UPDATE payment_prismalink_va_direct_payments 
        SET order_id = t.order_id 
        from (select id, reference_id, info_id, order_id 
          from payments) t
        where t.reference_id = payment_prismalink_va_direct_payments.id
        and t.info_id = 31
        and t.id > 4741770');
      
      \DB::statement('UPDATE payment_prismalink_va_direct_payments 
        SET virtual_account_number = t.va_number 
        from (select trim(both \'"\' from (body::json->\'accountNo\')::text) as va_number, 
          trim(both \'"\' from (body::json->\'signature\')::text) as signature, type
          from payment_prismalink_callback_logs) t
        where t.signature::text = payment_prismalink_va_direct_payments.signature
        and t.type = \'va_inquiry\'');
    }

    public function down()
    {
        //
    }
}
