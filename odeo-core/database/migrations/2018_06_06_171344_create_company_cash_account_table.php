<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyCashAccountTable extends Migration
{
    public function up()
    {
      Schema::create('company_cash_accounts', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('user_id');
        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        $table->string('company_name');
        $table->timestamp('last_transaction_update')->nullable();
        $table->timestamps();

        $table->index(['user_id']);
      });
    }

    public function down()
    {
        //
    }
}
