<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalPendingRequestsTable extends Migration
{
    public function up()
    {
      Schema::create('approval_pending_requests', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->text('raw_data');
        $table->string('path');
        $table->string('status');
        $table->bigInteger('requested_by');
        $table->bigInteger('approved_by')->nullable();
        $table->timestamp('approved_at')->nullable();
        $table->timestamp('processed_at')->nullable();
        $table->timestamps();
        $table->foreign('requested_by')->references('id')->on('users')->onDelete('set null');
        $table->foreign('approved_by')->references('id')->on('users')->onDelete('set null');
        $table->index(['path']);
      });
    }

    public function down()
    {
        //
    }
}
