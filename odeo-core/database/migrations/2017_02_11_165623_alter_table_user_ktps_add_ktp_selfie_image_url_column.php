<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserKtpsAddKtpSelfieImageUrlColumn extends Migration {

  public function up() {
    Schema::table('user_ktps', function (Blueprint $table) {
      $table->string('ktp_selfie_image_url')->nullable();
    });
  }

  public function down() {
    Schema::table('user_ktps', function (Blueprint $table) {
      $table->dropColumn('ktp_selfie_image_url');
    });
  }
}
