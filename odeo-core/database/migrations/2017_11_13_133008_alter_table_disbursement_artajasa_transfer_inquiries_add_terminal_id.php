<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementArtajasaTransferInquiriesAddTerminalId extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('disbursement_artajasa_transfer_inquiries', function (Blueprint $table) {
      $table->string('terminal_id')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('disbursement_artajasa_transfer_inquiries', function (Blueprint $table) {
      $table->dropColumn('terminal_id');
    });
  }
}
