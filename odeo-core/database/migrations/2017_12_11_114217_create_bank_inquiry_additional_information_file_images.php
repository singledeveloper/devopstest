<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankInquiryAdditionalInformationFileImages extends Migration
{
    public function up()
    {
      Schema::create('bank_inquiry_additional_information_file_images', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->unsignedBigInteger('bank_inquiry_additional_information_id')
          ->references('id')
          ->on('bank_inquiry_additional_informations')
          ->onDelete('cascade');

        $table->text('image_url');
        $table->string('type');

        $table->index(['bank_inquiry_additional_information_id', 'type']);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
