<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaSuppliesAddPulsaOdeoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('pulsa_supplies', function (Blueprint $table) {
        $table->integer('pulsa_odeo_id')->unsigned()->nullable();
        $table->foreign('pulsa_odeo_id')->references('id')->on('pulsa_odeos')->onDelete('set null');
        $table->boolean('is_active')->default(true);
        $table->index(['pulsa_odeo_id']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
