<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderDetailArtajasas extends Migration {

  public function up() {
    Schema::create('order_detail_artajasas', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('order_detail_pulsa_switcher_id');
      $table->foreign('order_detail_pulsa_switcher_id')->references('id')->on('order_detail_pulsa_switchers')->onDelete('cascade');
      $table->char('status', 5)->default('10000');
      $table->text('log_request')->nullable();
      $table->text('log_response')->nullable();
      $table->timestamps();
    });

    DB::update("ALTER SEQUENCE order_detail_artajasas_id_seq RESTART WITH 1000000000");
  }

  public function down() {
    Schema::drop('order_detail_artajasas');
  }
}
