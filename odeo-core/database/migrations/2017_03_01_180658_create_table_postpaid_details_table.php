<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePostpaidDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('order_detail_postpaid_details', function (Blueprint $table) {
        $table->increments('id');
        $table->bigInteger('order_detail_postpaid_switcher_id');
        $table->foreign('order_detail_postpaid_switcher_id')->references('id')->on('order_detail_postpaid_switchers')->onDelete('cascade');
        $table->char('period', 7);
        $table->integer('base_price');
        $table->integer('fine');
        $table->integer('admin_fee');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_detail_postpaid_details');
    }
}
