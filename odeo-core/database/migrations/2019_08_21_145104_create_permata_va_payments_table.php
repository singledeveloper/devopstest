<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermataVaPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_permata_va_payments', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('inquiry_id');
        $table->foreign('inquiry_id')->references('id')->on('payment_permata_va_inquiries');
        $table->string('instcode');
        $table->string('va_number');
        $table->string('trace_no');
        $table->string('trn_date');
        $table->decimal('bill_amount', 17, 2);
        $table->string('del_channel');
        $table->text('ref_info')->nullable();
        $table->string('status')->nullable();
        $table->bigInteger('payment_id')->nullable();
        $table->foreign('payment_id')->references('id')->on('payments');

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
