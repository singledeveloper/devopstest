<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailBlackhawksRemoveVoidThingsAddTransactionType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_detail_blackhawks', function(Blueprint $table){
          $table->string('transaction_type', 20)->default('normal');
          $table->removeColumn('log_void_request');
          $table->removeColumn('log_void_response');
          $table->removeColumn('log_reversal_void_request');
          $table->removeColumn('log_reversal_void_response');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
