<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailDutaPulsaTable extends Migration
{
  public function up()
  {
    Schema::create('order_detail_duta_pulsas', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('switcher_reference_id');
      $table->foreign('switcher_reference_id')->references('id')->on('order_detail_pulsa_switchers')->onDelete('cascade');
      $table->char('status', 5)->default('10000');
      $table->text('log_request')->nullable();
      $table->text('log_response')->nullable();
      $table->text('log_notify')->nullable();
      $table->string('route', 50);
      $table->timestamps();
    });

    DB::update("ALTER SEQUENCE order_detail_duta_pulsas_id_seq RESTART WITH 1000000000");
  }

  public function down()
  {
    Schema::drop('order_detail_duta_pulsas');
  }
}
