<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentUserDirectPaymentsTable extends Migration {

  public function up() {
    Schema::create('payment_user_direct_payments', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->unsignedInteger('reference_id')->nullable();
      $table->unsignedInteger('info_id');
      $table->foreign('info_id')->references('id')->on('payment_odeo_payment_channel_informations')->onDelete('set null');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('payment_user_direct_payments');
  }
}
