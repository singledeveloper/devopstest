<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworkReferralBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('network_referral_bonuses', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('referred_store_id')->unsigned();
            $table->foreign('referred_store_id')->references('id')->on('stores')->onDelete('set null');
            $table->integer('amount');
            $table->enum('is_claimed', array('0', '1'));
            $table->timestamp('created_at');
        });
        
        Schema::create('network_referral_bonus_details', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('referred_store_id')->unsigned();
            $table->foreign('referred_store_id')->references('id')->on('stores')->onDelete('set null');
            $table->bigInteger('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
            $table->integer('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('set null');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('network_referral_bonuses');
        Schema::drop('network_referral_bonus_details');
    }
}
