<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDoublePurchaseSuspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('double_purchase_suspects', function (Blueprint $table) {
          $table->increments('id');
          $table->bigInteger('switcher_reference_id');
          $table->foreign('switcher_reference_id')->references('id')->on('order_detail_pulsa_switchers')->onDelete('cascade');
          $table->string('reference_type', 20);
          $table->integer('loss_amount');
          $table->boolean('is_lost')->default(false);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('double_purchase_suspects');
    }
}
