<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisbursementBriDisbursementsTable extends Migration {

  public function up() {
    Schema::create('disbursement_bri_disbursements', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('remark')->nullable();
      $table->string('transfer_from')->nullable();
      $table->string('transfer_to')->nullable();
      $table->dateTime('transfer_datetime')->nullable();
      $table->dateTime('response_datetime')->nullable();
      $table->decimal('amount', 17, 0)->nullable();
      $table->string('disbursement_type')->nullable();
      $table->string('status')->nullable();
      $table->string('response_code')->nullable();
      $table->text('response_description')->nullable();
      $table->text('error_description')->nullable();
      $table->boolean('status_bri')->default(true);
      $table->string('referral_number')->nullable();
      $table->string('trx_name')->nullable();

      $table->unsignedBigInteger('disbursement_reference_id')->nullable();
      $table->index(['disbursement_reference_id', 'disbursement_type']);
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('disbursement_bni_disbursements');
  }
}
