<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailTiketsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('order_detail_tikets', function (Blueprint $table) {
      $table->increments('id');

      $table->bigInteger('order_detail_id')->unsigned();
      $table->foreign('order_detail_id')->references('id')->on('order_details')->onDelete('cascade');
      $table->bigInteger('user_id')->unsigned();
      $table->string('tiket_order_id', 10);
      $table->string('tiket_order_detail_id', 10);
      $table->string('name');
      $table->text('data');
      $table->string('status', 5);
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('order_detail_tikets');
  }
}
