<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersAddColumnIsEmailVerified extends Migration {

  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->boolean('is_email_verified')->default(false);
    });
  }


  public function down() {
    Schema::table('users', function (Blueprint $table) {
      //
    });
  }
}
