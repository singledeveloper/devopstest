<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSmsCenters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sms_centers', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('number', 50);
        $table->string('operator_name', 50);
        $table->string('jabber_account', 50)->nullable();
        $table->char('status', 5)->default('50000');
        $table->timestamp('last_pinged_at');
        $table->timestamp('last_error_reported_at')->nullable();
        $table->timestamps();
      });

      Schema::table('sms_outbound_logs', function (Blueprint $table){
        $table->dropColumn('outbound_to');
        $table->bigInteger('sms_center_id')->unsigned()->default(1);
        $table->foreign('sms_center_id')->references('id')->on('sms_centers')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
