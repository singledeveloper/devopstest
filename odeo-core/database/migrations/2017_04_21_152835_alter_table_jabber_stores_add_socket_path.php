<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableJabberStoresAddSocketPath extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('jabber_stores', function(Blueprint $table){
        $table->string('socket_path', 255)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('jabber_stores', function(Blueprint $table){
        $table->dropColumn('socket_path');
      });
    }
}
