<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailMkmsAddSessionIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_detail_mkms', function (Blueprint $table) {
            //$table->char("session_id", 32)->nullable()->after("order_detail_pulsa_switcher_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_detail_mkms', function (Blueprint $table) {
            //$table->dropColumn("session_id");
        });
    }
}
