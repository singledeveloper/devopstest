<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ScrapeInformationTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $now = Carbon::now();


    DB::table('bank_scrape_informations')->insert([
      [
        'target' => 'mandiri',
        'url' => 'https://mcm.bankmandiri.co.id/corp/common/login.do?action=logout',
        'created_at' => $now,
        'updated_at' => $now,
      ],
      [
        'target' => 'bca',
        'url' => 'https://vpn.tarumanagara.com/+CSCOE+/logon.html',
        'created_at' => $now,
        'updated_at' => $now,
      ]
    ]);
  }
}
