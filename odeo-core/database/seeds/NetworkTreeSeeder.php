<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class NetworkTreeSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('network_trees')->insert([
            'store_id'          => 1,
            'is_network_enable' => 1
        ]);
        DB::table('network_trees')->insert([
            'store_id'          => 2,
            'referred_store_id' => 1,
            'parent_store_id'   => 1,
            'position'          => 0,
            'is_network_enable' => 0
        ]);
        DB::table('network_trees')->insert([
            'store_id'          => 3,
            'referred_store_id' => 1,
            'parent_store_id'   => 1,
            'position'          => 1,
            'is_network_enable' => 1
        ]);
    }
}
