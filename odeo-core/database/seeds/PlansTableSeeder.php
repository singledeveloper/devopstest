<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PlansTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    DB::table('plans')->insert([
      'name' => 'LITE',
      'color' => '#FF3700',
      'cost_per_month' => 130000,
      'minimal_months' => 6,
      'is_free_domain' => 0,
      'sales_volume' => 20,
      'max_assign_sponsor_bonus' => 78000,
      'max_upgrade_sponsor_bonus' => 39000,
      'max_pairing' => 0,
      'max_pairing_potential' => 0,
      'warranty' => 52000
    ]);

    DB::table('plans')->insert([
      'name' => 'STARTER',
      'color' => '#F87D0B',
      'cost_per_month' => 650000,
      'minimal_months' => 6,
      'is_free_domain' => 1,
      'sales_volume' => 100,
      'max_assign_sponsor_bonus' => 390000,
      'max_upgrade_sponsor_bonus' => 195000,
      'max_pairing' => 1,
      'max_pairing_potential' => 500000,
      'warranty' => 260000
    ]);

    DB::table('plans')->insert([
      'name' => 'BUSINESS',
      'color' => '#F5B80E',
      'cost_per_month' => 1950000,
      'minimal_months' => 6,
      'is_free_domain' => 1,
      'sales_volume' => 300,
      'max_assign_sponsor_bonus' => 1170000,
      'max_upgrade_sponsor_bonus' => 585000,
      'max_pairing' => 4,
      'max_pairing_potential' => 2000000,
      'warranty' => 780000
    ]);

    DB::table('plans')->insert([
      'name' => 'PRO',
      'color' => '#7CE020',
      'cost_per_month' => 3900000,
      'minimal_months' => 6,
      'is_free_domain' => 1,
      'sales_volume' => 600,
      'max_assign_sponsor_bonus' => 2340000,
      'max_upgrade_sponsor_bonus' => 1170000,
      'max_pairing' => 8,
      'max_pairing_potential' => 5000000,
      'warranty' => 1560000
    ]);

    DB::table('plans')->insert([
      'name' => 'ENTERPRISE',
      'color' => '#13CBAB',
      'cost_per_month' => 6500000,
      'minimal_months' => 6,
      'is_free_domain' => 1,
      'sales_volume' => 1000,
      'max_assign_sponsor_bonus' => 3900000,
      'max_upgrade_sponsor_bonus' => 1950000,
      'max_pairing' => 12,
      'max_pairing_potential' => 10000000,
      'warranty' => 2600000
    ]);

    DB::table('plans')->insert([
      'id' => 99,
      'name' => 'FREE',
      'color' => '#FFFFFF',
      'cost_per_month' => 0,
      'minimal_months' => 6,
      'is_free_domain' => 0,
      'sales_volume' => 0,
      'max_assign_sponsor_bonus' => 0,
      'max_upgrade_sponsor_bonus' => 0,
      'max_pairing' => 0,
      'max_pairing_potential' => 0,
      'warranty' => 0
    ]);
  }
}
