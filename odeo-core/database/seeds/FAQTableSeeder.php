<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FAQTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faqs')->insert([
            'question'      => 'Mengapa saya harus berlangganan plan 6 bulan sekaligus?',
            'answer'        => 'Karena ketika kita melakukan pembelian nama domain (dan server), jangka waktu nama domain tersebut aktif minimal 1 tahun. Maka dari itu, agar hasil yang diperoleh lebih optimal, maka ODEO menganjurkan untuk berlangganan minimal 6 bulan.'
        ]);

        DB::table('faqs')->insert([
            'question'      => 'Ketika saya membeli plan ODEO, apakah saya bisa langsung berjualan?',
            'answer'        => 'Ya, jika Anda menggunakan Mentor ID dari mentor Anda, maka Anda akan mendapatkan FREE oDeposit senilai Rp200,000 yang dapat digunakan sebagai inventory toko Anda.'
        ]);

        DB::table('faqs')->insert([
            'question'      => 'Apakah saya boleh membeli plan ODEO lebih dari satu?',
            'answer'        => 'Boleh, setiap plan memiliki system management masing-masing, namun sudah terintegrasi sempurna terhadap satu account Anda, sehingga tidak perlu membuat banyak account untuk dapat memiliki banyak store.'
        ]);
        
        DB::table('faqs')->insert([
            'question'      => 'Apakah saya dapat menarik (withdraw) oCash saya setiap harinya?',
            'answer'        => 'Untuk permintaan penarikan oCash (withdraw) dapat dilakukan setiap harinya, namun biasanya transaksi akan dijalankan oleh bank pada hari dan jam kerja berikutnya (Senin - Jumat pukul 08:00 - 17:00 dan tidak termasuk hari libur nasional).'
        ]);
        
        DB::table('faqs')->insert([
            'question'      => 'Mengapa di store saya terdapat pengumuman "maaf, persediaan kami sedang habis"?',
            'answer'        => 'Hal tersebut terjadi ketika inventory / saldo oDeposit Anda sudah habis / berada di bawah minimum harga produk termurah. Inventory / saldo oDeposit minimum untuk setiap layanan agar toko Anda tetap dapat berjualan adalah: Pulsa = 10k, PLN = 20k, Hotel = 200k, Tiket = 400k.'
        ]);
        
        DB::table('faqs')->insert([
            'question'      => 'Kapan saya dapat menambah inventory / saldo oDeposit saya agar toko saya dapat tetap buka dan dapat berjualan tanpa terjadi stock habis (out of stock)?',
            'answer'        => 'Untuk penambahan oDeposit dapat dilakukan setiap harinya, namun ODEO baru akan selesai menambahkan deposit ke setiap merchant yang ada pada hari kerja berikutnya (Senin - Jumat dan tidak termasuk hari libur nasional) pada pukul 12:00.'
        ]);
    }
}
