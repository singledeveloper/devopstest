<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;

class UsersTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    DB::table('users')->insert(
      [
        'name' => 'Web Tester',
        'password' => Hash::make('12345678'),
        'type' => UserType::SELLER,
        'status' => UserStatus::OK,
      ]);

    DB::table('users')->insert(
      [
        'name' => 'Android Tester',
        'telephone' => '628997170052',
        'password' => Hash::make('12345678'),
        'type' => UserType::SELLER,
        'status' => UserStatus::OK,
      ]
    );

    DB::table('users')->insert(
      [
        'name' => 'Admin Panel Tester',
        'email' => 'admin@odeo.co.id',
        'password' => Hash::make('12345678'),
        'type' => 'admin',
        'status' => UserStatus::OK,
      ]
    );
  }
}
