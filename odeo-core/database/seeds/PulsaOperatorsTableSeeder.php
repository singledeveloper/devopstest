<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PulsaOperatorsTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('pulsa_operators')->insert([
      'code'      => 'TS',
      'name'      => 'Telkomsel',
      'status'    => 1
    ]);
    
    DB::table('pulsa_operators')->insert([
      'code'      => 'IS',
      'name'      => 'Indosat',
      'status'    => 1
    ]);
    
    DB::table('pulsa_operators')->insert([
      'code'      => 'XL',
      'name'      => 'XL',
      'status'    => 1
    ]);
    
    DB::table('pulsa_operators')->insert([
      'code'      => 'TH',
      'name'      => 'Three',
      'status'    => 1
    ]);
    
    DB::table('pulsa_operators')->insert([
      'code'      => 'AX',
      'name'      => 'Axis',
      'status'    => 1
    ]);
    
    DB::table('pulsa_operators')->insert([
      'code'      => 'SF',
      'name'      => 'SmartFren',
      'status'    => 1
    ]);
    
    DB::table('pulsa_operators')->insert([
      'code'      => 'ES',
      'name'      => 'Esia',
      'status'    => 1
    ]);
    
    DB::table('pulsa_operators')->insert([
      'code'      => 'PLN',
      'name'      => 'PLN Token',
      'status'    => 1
    ]);
    
    DB::table('pulsa_operators')->insert([
      'code'      => 'BO',
      'name'      => 'Bolt',
      'status'    => 1
    ]);
  }
}
