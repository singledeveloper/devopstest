<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2018-12-28
 * Time: 22:47
 */

namespace Odeo\Console;

use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Subscription\StoreVerificator;
use Odeo\Domains\Transaction\DepositRequester;
use Odeo\Domains\Transaction\Helper\CashManager;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Transaction\Repository\StoreDepositRepository;
use Odeo\Domains\Transaction\TempDepositRequester;

class AutoTopupDeposit {

  protected $userStore = [
    [
      'user_id' => 1,
      'store_id' => 1,
      'deposit_threshold' => 1000000,
      'topup_amount' => 2000000,
      'topup_below_preferred_amount' => true,
    ],
    [
      'user_id' => 19,
      'store_id' => 923,
      'deposit_threshold' => 2000000,
      'topup_amount' => 5000000,
      'topup_below_preferred_amount' => false,
    ],
  ];
  private $storeDeposits;
  private $cashManager;

  public function __construct() {
    $this->storeDeposits = app()->make(StoreDepositRepository::class);
    $this->cashManager = app()->make(CashManager::class);
  }

  public function run() {
    foreach ($this->userStore as $us) {
      if (!$this->checkDepositIsBelowTreshhold($us)) {
        continue;
      }

      $this->doTopup($us);
    }
  }

  private function doTopup($data) {
    $amount = $this->getTopupAmount($data);
    if (!$amount) {
      return;
    }

    $pipeline = new Pipeline;

    $pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true,
      'not_available_for_free_store' => true,
      'store_id' => $data['store_id'],
    ]));
    $pipeline->add(new Task(CartRemover::class, 'clear', ['store_id' => $data['store_id'],]));
    $pipeline->add(new Task(DepositRequester::class, 'request', ['store_id' => $data['store_id'],]));
    $pipeline->add(new Task(CartInserter::class, 'addToCart', ['store_id' => $data['store_id'],]));
    $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
    $pipeline->add(new Task(TempDepositRequester::class, 'tempDeposit'));
    $pipeline->add(new Task(PaymentRequester::class, 'request'));
    $pipeline->add(new Task(PaymentOpenRequester::class, 'open', [
      'with_password' => true,
    ]));

    $pipeline->enableTransaction();
    $pipeline->execute([
      'auth' => [
        'user_id' => $data['user_id'],
        'platform_id' => Platform::SYSTEM,
        'type' => UserType::SELLER,
      ],
      'deposit' => [
        'amount' => $amount,
        'currency' => Currency::IDR,
      ],
      'info_id' => Payment::OPC_GROUP_OCASH,
      'opc' => 531,
      'gateway_id' => Payment::AUTO_RECURRING_ODEPOSIT,
      'service_detail_id' => ServiceDetail::DEPOSIT_ODEO,
    ]);
  }

  private function checkDepositIsBelowTreshhold($data) {
    $tempDeposits = [];
    $deposit = $this->cashManager->getStoreDepositBalance($data['store_id'], Currency::IDR, $tempDeposits);
    return $deposit[$data['store_id']]['amount'] < $data['deposit_threshold'];
  }

  private function getTopupAmount($data) {
    $locked = [];
    $cash = $this->cashManager->getCashBalance($data['user_id'], Currency::IDR, $locked);
    $currentOCashAmount = $cash[$data['user_id']]['amount'];

    if ($data['topup_below_preferred_amount']) {
      return min($currentOCashAmount, $data['topup_amount']);
    }

    if ($currentOCashAmount >= $data['topup_amount']) {
      return $data['topup_amount'];
    }

    return 0;
  }

}
