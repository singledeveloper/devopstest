<?php
namespace Odeo\Domains\VirtualAccount\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\VirtualAccount\Model\UserVirtualAccount;

class UserVirtualAccountRepository extends Repository {

  public function __construct(UserVirtualAccount $userVirtualAccount) {
    $this->model = $userVirtualAccount;
  }

  public function findByUserIdAndCreatedBy($userId, $createdBy) {
    return $this->model
      ->where('user_id', $userId)
      ->where('created_by', $createdBy)
      ->first();
  }

}
