<?php

namespace Odeo\Domains\Reminder\Scheduler;

use Odeo\Domains\Reminder\Repository\BillReminderRepository;

class RemoveExpiredBillReminderScheduler {

  private $billReminders;

  public function __construct() {
    $this->billReminders = app()->make(BillReminderRepository::class);
  }

  public function run() {
    $this->billReminders->removeExpired();
  }

}
