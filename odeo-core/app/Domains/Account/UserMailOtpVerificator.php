<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 23-Jul-18
 * Time: 6:31 PM
 */

namespace Odeo\Domains\Account;


use Carbon\Carbon;
use Odeo\Domains\Account\Helper\OtpGenerator;
use Odeo\Domains\Account\Jobs\SendOtpEmail;
use Odeo\Domains\Account\Repository\UserEmailVerificationOtpAttemptRepository;
use Odeo\Domains\Account\Repository\UserEmailVerificationOtpRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\Email;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Exceptions\FailException;

class UserMailOtpVerificator {

  private $userRepo, $userEmailVerificationOtpRepo, $otpGeneratorHelper, $userOtpAttemptRecorder, $userHistoryCreator;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
    $this->userEmailVerificationOtpRepo = app()->make(UserEmailVerificationOtpRepository::class);
    $this->userEmailVerificationOtpAttemptRepo = app()->make(UserEmailVerificationOtpAttemptRepository::class);
    $this->otpGeneratorHelper = app()->make(OtpGenerator::class);
    $this->userSelector = app()->make(UserSelector::class);
    $this->userOtpAttemptRecorder = app()->make(UserOtpAttemptRecorder::class);
    $this->userHistoryCreator = app()->make(UserHistoryCreator::class);
  }

  public function sendVerifyEmailOtp(PipelineListener $listener, $data) {
    $email = $data['email'];

    try {
      $user = $this->userRepo->findById($data['auth']['user_id']);

      $this->validateEmail($user, $email);

      $this->checkUserStatus($user);

      $userOtpBefore = $this->userEmailVerificationOtpRepo->findAllEmail($email);
      list ($send, $token) = $this->generateToken($userOtpBefore, $email, $data);

      if ($send) {
//        isProduction() &&
        $listener->pushQueue(new SendOtpEmail($email, $this->produceSendEmailData($token)));
      }
    } catch (FailException $e) {
      if ($e->getErrorStatus()) {
        return $listener->responseWithErrorStatus($e->getErrorStatus(), $e->getMessage());
      } else {
        return $listener->response(400, $e->getMessage());
      }
    }
    return $listener->response(201, $this->produceSuccessData($userOtpBefore));
  }

  public function verifyEmailOtp(PipelineListener $listener, $data) {
    $email = $data['email'];
    $userId = $data['auth']['user_id'];
    list($isValid, $message) = $this->userOtpAttemptRecorder->recordEmailOtp($email, $data['verification_code']);

    if (!$isValid) {
      return $listener->response(400, $message);
    }

    // OTP valid
    $now = Carbon::now();
    $user = $this->userRepo->findById($userId);
    $user->email = $email;
    $user->is_email_verified = true;
    $user->mail_verified_at = $now->toDateTimeString();

    $this->userHistoryCreator->create($user);
    
    $this->userRepo->save($user);

    return $listener->response(200, $this->userSelector->_transforms($user, $this->userRepo));
  }

  private function validateEmail($user, $email) {
    if (Email::isBlacklisted($email)) {
      throw new FailException(trans('user.email_not_allowed'));
    }

    if ($user->email !== $email && $this->userRepo->findByEmail($email)) {
      throw new FailException(trans('user.email_is_taken'));
    }

    if ($user->is_email_verified && $user->email === $email) {
      throw new FailException(trans('user.email_is_verified'));
    }

  }

  private function generateToken($userOtpBefore, $email, $data) {
    $lastUserOtp = $userOtpBefore->first();

    $now = Carbon::now();
    $lastUserOtpCreatedAt = $lastUserOtp ? Carbon::parse($lastUserOtp->created_at) : null;

    if (count($userOtpBefore) >= 3) {
      if ($lastUserOtpCreatedAt && $lastUserOtpCreatedAt->diffInHours($now) == 0) {
        throw new FailException(trans('user.max_otp_request_reached'));
      } else {
        $this->userEmailVerificationOtpRepo->deleteByEmail($email);
      }
    }

    $sendOtp = !$lastUserOtpCreatedAt || $lastUserOtpCreatedAt->diffInMinutes($now) >= 1;

    if ($sendOtp) {
      $force4DigitsOtp = isset($data['force_4_digits_otp']) && $data['force_4_digits_otp'];
      $token = $this->newOtp($email, $force4DigitsOtp);

      return [true, $token];
    }

    return [false, null];
  }

  private function newOtp($email, $force4DigitsOtp = false) {
    $token = $this->otpGeneratorHelper->generateOtp($force4DigitsOtp);

    $userOtp = $this->userEmailVerificationOtpRepo->getNew();

    $userOtp->email = $email;
    $userOtp->otp = $token;
    $userOtp->is_used = 0;
    $userOtp->created_at = date('Y-m-d H:i:s');

    $this->userEmailVerificationOtpRepo->save($userOtp);

    return $token;
  }

  private function checkUserStatus($user) {
    if (!$user) {
      throw new FailException('User does not exist in our data.');

    } else if (in_array($user->status, UserStatus::groupBanned())) {
      throw new FailException(trans('errors.account_banned'), ErrorStatus::ACCOUNT_BANNED);

    } else if ($user->status == UserStatus::DEACTIVATED) {
      throw new FailException(trans('errors.account_deactivated'));
    }
  }

  private function produceSuccessData($userOtpBefore) {
    return [
      "ttl" => 60000,
      "remains" => 3 - count($userOtpBefore)
    ];
  }

  private function produceSendEmailData($token) {
    return [
      'text' => trans('user.email_otp_instruction', [
        'otp' => ''
      ]),
      'otp' => $token
    ];
  }

}
