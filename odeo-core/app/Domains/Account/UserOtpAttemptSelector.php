<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class UserOtpAttemptSelector implements SelectorListener {

  public function __construct() {
    $this->userOtpAttempt = app()->make(\Odeo\Domains\Account\Repository\UserOtpAttemptRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $userOtpAttempts=[];
    $userOtpAttempts["otp_attempt_id"] = $item->id;
    $userOtpAttempts["telephone"] = $item->telephone;
    $userOtpAttempts["otp"] = $item->otp;
    $userOtpAttempts["ip_address"] = $item->ip_address;
    $userOtpAttempts["is_attempted"] = $item->is_attempted;
    if(isset($item->created_at)) $userOtpAttempts["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if(isset($item->updated_at)) $userOtpAttempts["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");
    return $userOtpAttempts;
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->userOtpAttempt->normalizeFilters($data);
    $this->userOtpAttempt->setSimplePaginate(true);
    $userOtpAttempts = [];
    foreach ($this->userOtpAttempt->gets() as $item) {
      $userOtpAttempts[] = $this->_transforms($item, $this->userOtpAttempt);
    }

    if (sizeof($userOtpAttempts) > 0) {
      return $listener->response(200, array_merge(
        ["user_otp_attempts" => $this->_extends($userOtpAttempts, $this->userOtpAttempt)],
        $this->userOtpAttempt->getPagination()
      ));
    }
    return $listener->response(204, ["user_otp_attempts" => []]);
  }

}
