<?php

namespace Odeo\Domains\Account\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendNewBusinessUser extends Job  {
  protected $email;
  protected $name;
  protected $telephone;
  protected $password;
  protected $pin;
  protected $ccEmails;

  public function __construct($email, $name, $telephone, $password, $pin, $ccEmails = []) {
    parent::__construct();
    $this->email = $email;
    $this->name = $name;
    $this->telephone = $telephone;
    $this->password = $password;
    $this->pin = $pin;
    $this->ccEmails = $ccEmails;
  }

  public function handle() {
    Mail::send(isProduction() ? 'emails.email_new_business_user' : 'emails.email_new_business_user_dev', [
      'name' => $this->name,
      'telephone' => revertTelephone($this->telephone),
      'password' => $this->password,
      'pin' => $this->pin
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id');
      $m->to($this->email, $this->name)
        ->subject(isProduction() ? '[ODEO] Welcome to ODEO Business' : '[ODEO] Development');
      $m->cc($this->ccEmails)
        ->subject(isProduction() ? '[ODEO] Welcome to ODEO Business' : '[ODEO] Development');
    });
  }
}
