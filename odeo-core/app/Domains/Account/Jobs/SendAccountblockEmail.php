<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 14/09/18
 * Time: 13.25
 */

namespace Odeo\Domains\Account\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendAccountblockEmail extends Job  {

  private $userEmail, $hourBlocked;

  public function __construct($userEmail, $hourBlocked) {
    parent::__construct();
    $this->userEmail = $userEmail;
    $this->hourBlocked = $hourBlocked;
  }

  public function handle() {
    Mail::send('emails.account_blocked', [
      'hour' => $this->hourBlocked
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id');
      $m->to($this->userEmail)->subject('Account');
    });
  }
}
