<?php

namespace Odeo\Domains\Account\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendEmailConfirmation extends Job  {
  protected $url;
  protected $user;


  public function __construct($url, $user) {
    parent::__construct();
    $this->url = $url;
    $this->user = $user;
  }

  public function handle() {
    Mail::send('emails.email_confirmation', [
      'url' => $this->url
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id');
      $m->to($this->user->email, $this->user->name)->subject('[ODEO] Email Confirmation');
    });
  }
}
