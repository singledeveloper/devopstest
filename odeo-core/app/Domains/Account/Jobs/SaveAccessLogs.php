<?php

namespace Odeo\Domains\Account\Jobs;

use Odeo\Domains\Account\Repository\UserAccessLogRepository;
use Odeo\Jobs\Job;

class SaveAccessLogs extends Job {

  protected $data;

  private $accessLogs;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    $this->accessLogs = app()->make(UserAccessLogRepository::class);

    $accessLog = $this->accessLogs->getNew();
    $accessLog->user_id = $this->data['user_id'];
    $accessLog->url_path = isset($this->data['path']) ? $this->data['path'] : null;
    $accessLog->description = $this->data['description'];
    $accessLog->created_at = date('Y-m-d H:i:s');

    $this->accessLogs->save($accessLog);
  }
}
