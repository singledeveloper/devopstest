<?php

namespace Odeo\Domains\Account;


use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class BankSelector implements SelectorListener {

  private $banks;

  public function __construct() {
    $this->banks = app()->make(\Odeo\Domains\Account\Repository\BankRepository::class);
  }

  public function get(PipelineListener $listener, $data) {
    $banks = [];
    foreach ($this->banks->getATMBersamaBankList() as $bank) {
      $banks[] = $this->_transforms($bank, $this->banks);
    }

    if (sizeof($banks) > 0)
      return $listener->response(200, array_merge(
        ["banks" => $this->_extends($banks, $this->banks)]
      ));

    return $listener->response(204, ["banks" => []]);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    return [
      'id' => $item->id,
      'name' => $item->name,
      'logo' => AssetAws::getAssetPath(AssetAws::BANK, $item->logo)
    ];
  }
}
