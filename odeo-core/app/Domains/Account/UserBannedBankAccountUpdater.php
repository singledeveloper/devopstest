<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Core\PipelineListener;

class UserBannedBankAccountUpdater {

  private $userBannedBankAccounts;

  public function __construct() {
    $this->userBannedBankAccounts = app()->make(\Odeo\Domains\Account\Repository\UserBannedBankAccountRepository::class);
  }

  public function update(PipelineListener $listener, $data){
    if ($userBannedBankAccounts = $this->userBannedBankAccounts->findById($data["banned_bank_account_id"])) {
      $userBannedBankAccounts->is_banned = $data["is_banned"];
      $userBannedBankAccounts->banned_reason = $data["banned_reason"];
      $this->userBannedBankAccounts->save($userBannedBankAccounts);
      return $listener->response(200);
    }
    return $listener->response(400);
  }

}
