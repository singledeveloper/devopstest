<?php

namespace Odeo\Domains\Account;

use Carbon\Carbon;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Core\PipelineListener;

class ZendeskAuthRequester {

  private $userRepo;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
  }


  public function zendeskJwtAuthentication(PipelineListener $listener, $data) {

    if (!isset($data['user_token'])) {
      return $listener->response(401);
    }

    $user = $this->userRepo->findById($data["user_token"]);

    if (!$user) {
      return $listener->response(401);
    }

    $header = json_encode(["alg" => "HS256", "typ" => "JWT"]);

    $payload = json_encode([
      "name" => $user->name,
      "email" => $user->email,
      "jti" => $data["user_token"],
      "iat" => (int)Carbon::now()->format('U')
    ]);

    $jwtSecretKey = env('ZENDESK_JWT_KEY');
    $encodedHeader = rtrim(strtr(base64_encode($header), '+/', '-_'), '=');
    $encodedPayload = rtrim(strtr(base64_encode($payload), '+/', '-_'), '=');
    $unsignedToken = $encodedHeader . '.' . $encodedPayload;
    $signature = hash_hmac('sha256', $unsignedToken, $jwtSecretKey, true);
    $encodedSignature = rtrim(strtr(base64_encode($signature), '+/', '-_'), '=');
    $token = $unsignedToken . "." . $encodedSignature;

    return $listener->response(200, ["jwt" => $token]);

  }

}
