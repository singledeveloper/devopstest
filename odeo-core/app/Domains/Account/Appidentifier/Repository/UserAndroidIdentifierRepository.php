<?php

namespace Odeo\Domains\Account\Appidentifier\Repository;

use Odeo\Domains\Account\Appidentifier\Model\UserAndroidIdentifier;
use Odeo\Domains\Core\Repository;

class UserAndroidIdentifierRepository extends Repository {

  public function __construct(UserAndroidIdentifier $androidIdentifier) {
    $this->model = $androidIdentifier;
  }

  public function findByUserId($userId) {
    return $this->model->where('user_id', $userId)->first();
  }

  public function getSSAIDCount($ssaid) {
    return $this->model->where('ssaid', $ssaid)->count();
  }

}
