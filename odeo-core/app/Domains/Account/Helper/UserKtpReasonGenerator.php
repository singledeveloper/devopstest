<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 11-Apr-18
 * Time: 3:26 PM
 */

namespace Odeo\Domains\Account\Helper;


use Odeo\Domains\Constant\UserKtpStatus;

class UserKtpReasonGenerator {

  public function getNikReason($nikStatus) {
    switch ($nikStatus) {
      case UserKtpStatus::NIK_MISMATCH:
        return trans('user.nik_mismatch');
      default:
        return "";
    }
  }

  public function getNameReason($nameStatus) {
    switch ($nameStatus) {
      case UserKtpStatus::NAME_MISMATCH:
        return trans('user.name_mismatch');
      default:
        return "";
    }
  }

  public function getKtpReason($ktpStatus) {
    switch ($ktpStatus) {
      case UserKtpStatus::KTP_MISMATCH:
        return trans('user.ktp_mismatch');
      case UserKtpStatus::KTP_UNCLEAR:
        return trans('user.ktp_unclear');
      default:
        return "";
    }
  }

  public function getKtpSelfieReason($ktpSelfieStatus) {
    switch ($ktpSelfieStatus) {
      case UserKtpStatus::KTP_SELFIE_MISMATCH:
        return trans('user.ktp_selfie_mismatch');
      case UserKtpStatus::KTP_SELFIE_UNCLEAR:
        return trans('user.ktp_selfie_unclear');
      default:
        return "";
    }
  }

  public function getLastOcashBalanceReason($lastOcashBalanceStatus) {
    switch ($lastOcashBalanceStatus) {
      case UserKtpStatus::LAST_OCASH_BALANCE_MISMATCH:
        return trans('user.last_ocash_balance_mismatch');
      default:
        return "";
    }
  }

  public function getLastTransactionDescriptionReason($lastTransactionDescriptionStatus) {
    switch ($lastTransactionDescriptionStatus) {
      case UserKtpStatus::LAST_TRANSACTION_DESCRIPTION_MISMATCH:
        return trans('user.last_transaction_description_mismatch');
      default:
        return "";
    }
  }

  public function getReasonsArray($nikStatus, $nameStatus, $ktpStatus, $ktpSelfieStatus, $lastOcashBalanceStatus, $lastTransactionDescriptionAmountStatus, $fillBlank = false) {
    $reasons = [];
    array_push($reasons, $this->getNikReason($nikStatus));
    array_push($reasons, $this->getNameReason($nameStatus));
    array_push($reasons, $this->getKtpReason($ktpStatus));
    array_push($reasons, $this->getKtpSelfieReason($ktpSelfieStatus));
    array_push($reasons, $this->getLastOcashBalanceReason($lastOcashBalanceStatus));
    array_push($reasons, $this->getLastTransactionDescriptionReason($lastTransactionDescriptionAmountStatus));

    if (!$fillBlank) {
      $reasons = array_filter($reasons, function($reason) {
        return $reason !== '';
      });
    }

    return $reasons;
  }

  public function getReasons(
    $nikStatus = UserKtpStatus::NIK_SUCCESS, 
    $nameStatus = UserKtpStatus::NAME_SUCCESS, 
    $ktpStatus = UserKtpStatus::KTP_SUCCESS, 
    $ktpSelfieStatus = UserKtpStatus::KTP_SELFIE_SUCCESS, 
    $lastOcashBalanceStatus = UserKtpStatus::LAST_OCASH_BALANCE_SUCCESS, 
    $lastTransactionDescriptionAmountStatus = UserKtpStatus::LAST_TRANSACTION_DESCRIPTION_SUCCESS
  ) {
    $reasons = join("\n", $this->getReasonsArray($nikStatus, $nameStatus, $ktpStatus, $ktpSelfieStatus, $lastOcashBalanceStatus, $lastTransactionDescriptionAmountStatus));

    return $reasons;
  }

}