<?php

namespace Odeo\Domains\Account;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Account\Helper\OtpGenerator;

use Odeo\Domains\Account\Model\UserForgot;
use Odeo\Domains\Account\Model\UserOtp;
use Odeo\Domains\Account\Repository\UserForgotRepository;
use Odeo\Domains\Account\Repository\UserOtpRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Exceptions\FailException;

class Resetter {

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
    $this->userOtpRepo = app()->make(UserOtpRepository::class);
    $this->userForgotRepo = app()->make(UserForgotRepository::class);
    $this->otpGeneratorHelper = app()->make(OtpGenerator::class);
    $this->userHistoryCreator = app()->make(UserHistoryCreator::class);
  }

  public function resetPin(PipelineListener $listener, $data) {
    try {
      $user = $this->userRepo->findById($data['auth']['user_id']);
      $this->verify($user, 'pin', $data);

      list($isValid, $message) = checkPinSecure($data['pin']);
      if (!$isValid) return $listener->response(400, $message);

      $user->transaction_pin = Hash::make($data['pin']);
      $user->invalid_pin_attempt_count = 0;

      $this->userRepo->save($user);

    } catch (FailException $e) {
      return $listener->response(400, $e->getMessage());
    }

    return $listener->response(200);

  }

  public function resetPassword(PipelineListener $listener, $data) {
    $phone = $data['telephone'] ?? $data['phone_number'];
    try {
      $user = $this->userRepo->findByTelephone($phone);
      $this->verify($user, 'password', $data);

      $newPassword = Hash::make($data['password']);

      if ($user->password == $user->transaction_pin) {
        $user->transaction_pin = $newPassword;
        $user->invalid_pin_attempt_count = 0;
      }

      $user->password = $newPassword;
      $user->login_counts = 0;

      if ($user->status == UserStatus::RESET_PIN) {
        $user->status = UserStatus::OK;
        $this->userHistoryCreator->create($user, $user->id);
      }

      $this->userRepo->save($user);

    } catch (FailException $e) {
      return $listener->response(400, $e->getMessage());
    }

    return $listener->response(200);
  }


  private function verify($user, $securityType, $data) {
    $userOtp = $this->userOtpRepo->findByTelephone($user->telephone);
    $userForgot = $this->userForgotRepo->findByUserId($user->id, $securityType);

    if (!$userForgot) {
      throw new FailException(trans('errors.invalid_reset_token'));
    }

    $this->validateExpiry($userForgot, $userOtp, $data['token']);

    $this->userOtpRepo->archiveOtp($user->telephone);
    $this->userForgotRepo->delete($user->forgot);
  }


  private function validateExpiry(UserForgot $userForgot, UserOtp $userOtp, $token) {

    if (!$userForgot) {
      throw new FailException(trans('errors.invalid_reset_token'));
    }

    if ($userForgot->token != $token) {
      throw new FailException(trans('errors.invalid_reset_token'));
    }

    $userForgotCreatedAt = strtotime($userForgot->created_at);
    $userOtpCreatedAt = strtotime($userOtp->created_at);
    $limit = strtotime(date('Y-m-d H:i:s', strtotime('-20 minutes')));

    if ($userForgotCreatedAt < $limit) {
      throw new FailException(trans('errors.invalid_reset_token'));
    }

    if ($userOtpCreatedAt < $limit) {
      throw new FailException(trans('errors.invalid_reset_token'));
    }
  }


}
