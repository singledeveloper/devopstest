<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\Bank;

class BankRepository extends Repository {

  public function __construct(Bank $bank) {
    $this->model = $bank;
  }

  public function gets() {
    \Log::info('abank');
    $query = $this->model;

    return $this->getResult($query->orderBy('priority', 'asc'));
  }

  public function getATMBersamaBankList() {
    return $this->getCloneModel()
      ->whereNotNull('aj_bank_code')
      ->whereNotNull('aj_regency_code')
      ->orderBy('priority', 'asc')
      ->get();
  }

  public function listByIds($ids) {
    return $this->getCloneModel()
      ->whereIn('id', $ids)
      ->get();
  }
}
