<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\UserProxy;
use Odeo\Domains\Core\Repository;

class UserProxyRepository extends Repository {

  public function __construct(UserProxy $userProxy) {
    $this->model = $userProxy;
  }

  public function findIpByStoreId($storeId) {
    return $this->getCloneModel()
      ->join('user_stores', 'user_stores.user_id', '=', 'user_proxies.user_id')
      ->where('user_stores.store_id', $storeId)
      ->where('user_stores.type', 'owner')
      ->select('user_proxies.ip')->first();
  }
}
