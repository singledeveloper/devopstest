<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\Faq;

class FaqRepository extends Repository {

  public function __construct(Faq $faq) {
    $this->model = $faq;
  }

  public function get($data = []) {
    $query = $this->model->orderBy('priority', 'asc');
    if (isset($data['group'])) $query = $query->where('group', $data['group']);

    return $query->get();
  }
}
