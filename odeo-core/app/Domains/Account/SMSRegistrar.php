<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Agent\AgentRequester;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Vendor\SMS\CenterManager;

class SMSRegistrar extends CenterManager {

  public function __construct() {
    parent::__construct();
  }

  public function parse(PipelineListener $listener, $data) {

    list($command, $masterCode, $userName, $userAddress, $transactionPin) = explode('.', $data['sms_message']);

    $data['sms_message'] = $command . '.' . $masterCode. '.' . $userName. '.' . $userAddress . '.XXXXXX';
    $this->saveHistory($data);

    $pipeline = new Pipeline();
    $pipeline->add(new Task(Registrar::class, 'autoSign', [
      'telephone' => purifyTelephone($data['sms_to']),
      'name' => $userName,
      'street' => $userAddress,
      'status' => UserStatus::OK
    ]));
    $pipeline->add(new Task(AgentRequester::class, 'addMaster', [
      'master_code' => $masterCode
    ]));
    $pipeline->add(new Task(\Odeo\Domains\Vendor\SMS\Registrar::class, 'register', [
      'telephone' => $data['sms_to'],
      'transaction_pin' => $transactionPin,
      'master_code' => $masterCode
    ]));
    $pipeline->enableTransaction();
    $pipeline->execute([]);

    if ($pipeline->fail()) $this->reply($pipeline->errorMessage, $data['sms_to']);

    return $listener->response(200);
  }
}
