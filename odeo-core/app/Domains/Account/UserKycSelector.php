<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class UserKycSelector implements SelectorListener {

  private $userKycRepo, $userKycOptionRepo, $userKycDetailRepo;

  public function __construct() {
    $this->userKycRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycRepository::class);
    $this->userKycOptionRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycOptionRepository::class);
    $this->userKycDetailRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycDetailRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $userKyc = [];
    $details = [];
    if ($kyc = $this->userKycRepo->checkCurrentKyc($data['auth']['user_id'])) {
      $details = $this->userKycDetailRepo->getByKycId($kyc->id);
    }

    foreach ($this->userKycOptionRepo->getByBusinessType($data['business_type']) as $item) {
      $detail = isset($details[$item->id]) ? $details[$item->id] : false;
      $userKyc[] = ForBusiness::createDetail($detail, $item);
    }

    return $listener->response(200, ["business_kyc" => $userKyc]);
  }

  public function getUsers(PipelineListener $listener, $data) {
    $userKyc = [];
    $this->userKycRepo->normalizeFilters($data);
    $this->userKycRepo->setSimplePaginate(true);
    foreach ($this->userKycRepo->gets() as $item) {
      if ($item->status == '50000') $status = 'APPROVED';
      else if ($item->status == '40000') $status = 'REJECTED';
      else if ($item->status == '30000') $status = 'WAITING VERIFICATION';
      else $status = 'PENDING';

      $userKyc[] = [
        'id' => $item->id,
        'user_id' => $item->user_id,
        'user_name' => $item->user_name,
        'business_type' => strtoupper($item->business_type),
        'status' => $status,
        'verified_at' => $item->verified_at,
        'rejected_at' => $item->rejected_at
      ];
    }

    if (count($userKyc) > 0)
      return $listener->response(200, array_merge(
        ["user_kyc" => $userKyc],
        $this->userKycRepo->getPagination()
      ));

    return $listener->response(204, ["user_kyc" => []]);
  }

  public function getById(PipelineListener $listener, $data) {
    $userKyc = [];
    if ($kyc = $this->userKycRepo->findById($data['kyc_id'])) {
      $details = $this->userKycDetailRepo->getByKycId($kyc->id);
      foreach ($this->userKycOptionRepo->getByBusinessType($kyc->business_type) as $item) {
        $detail = isset($details[$item->id]) ? $details[$item->id] : false;
        $userKyc[] = ForBusiness::createDetail($detail, $item);
      }
    }

    return $listener->response(200, ["business_kyc" => $userKyc]);
  }

  public function check(PipelineListener $listener, $data) {

    if ($userKyc = $this->userKycRepo->checkCurrentKyc($data['auth']['user_id'])) {
      return $listener->response(200, [
        'id' => $userKyc->id,
        'business_type' => $userKyc->business_type,
        'status' => $userKyc->status
      ]);
    }
    return $listener->response(204);
  }

  public function checkById(PipelineListener $listener, $data) {

    if ($userKyc = $this->userKycRepo->findById($data['kyc_id'])) {
      return $listener->response(200, [
        'user_name' => $userKyc->user->name,
        'business_type' => $userKyc->business_type
      ]);
    }
    return $listener->response(204);
  }

}
