<?php
/**
 * Created by PhpStorm.
 * User: odeo
 * Date: 12/21/17
 * Time: 10:26 PM
 */

namespace Odeo\Domains\Account;


use Odeo\Domains\Core\PipelineListener;
use \Odeo\Domains\Account\Repository\UserBankAccountRepository;

class BankAccountRemover {

  private $bankAccountRepo;

  public function __construct() {
    $this->bankAccountRepo = app()->make(UserBankAccountRepository::class);
  }

  public function remove(PipelineListener $listener, $data) {

    if (!$account = $this->bankAccountRepo->findById($data['bank_account_id'])) {
      return $listener->response(400, trans('errors.data_not_exists'));
    }

    $userId = $data['auth']['user_id'];

    if ($account->user_id != $userId) {
      return $listener->response(400, trans('errors.account_mismatch'));
    }

    if ($this->bankAccountRepo->delete($account)) {
      return $listener->response(200);
    }

    return $listener->response(400, trans('errors.database'));
  }

}