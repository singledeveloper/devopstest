<?php

namespace Odeo\Domains\Account\Model;

use Odeo\Domains\Core\Entity;

class UserOtpAttempt extends Entity
{
    public $timestamps = true;
}