<?php

namespace Odeo\Domains\Account\Model;


use Odeo\Domains\Core\Entity;

class Faq extends Entity
{
    public $timestamps = true;
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'answer'];
}
