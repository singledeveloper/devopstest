<?php

namespace Odeo\Domains\Account\Model;

use Odeo\Domains\Core\Entity;

class UserHistory extends Entity  {

  protected $fillable = [
    'user_id', 'name', 'email', 'telephone', 'ktp_nik', 'ktp_image_url', 'status', 'changed_by_user_id'
  ];

}
