<?php

namespace Odeo\Domains\Account\Model;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Disbursement\Model\DisbursementApiUser;
use Odeo\Domains\Order\Receipt\Model\UserReceiptConfig;
use Odeo\Domains\Subscription\Model\UserStore;

class UserAccessLog extends Entity {

  public $timestamps = false;

  public function user() {
    return $this->belongsTo(User::class);
  }

}
