<?php

namespace Odeo\Domains\Account\Model;

use Odeo\Domains\Core\Entity;

class UserOtp extends Entity
{
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

}
