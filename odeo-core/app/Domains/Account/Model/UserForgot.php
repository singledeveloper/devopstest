<?php

namespace Odeo\Domains\Account\Model;

use Odeo\Domains\Core\Entity;

class UserForgot extends Entity {

  protected $hidden = ['created_at'];

  public $timestamps = false;

  public function user() {
    return $this->belongsTo(User::class);
  }
}
