<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/28/17
 * Time: 6:29 PM
 */

namespace Odeo\Domains\Account\Model;


use Odeo\Domains\Core\Entity;

class City extends Entity {
  public $timestamps = false;
}