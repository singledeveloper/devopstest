<?php

namespace Odeo\Domains\Account\Model;

use Odeo\Domains\Core\Entity;

class UserPasswordHistory extends Entity {

  public $timestamps = false;

}
