<?php

namespace Odeo\Domains\Account\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Account\Model\User;

class UserBannedBankAccount extends Entity {
  
  public $timestamps = true;
  
  /**
   * Get the bank of the account
   */
  public function bank()
  {
    return $this->belongsTo(Bank::class);
  }
}
