<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Helper\UserKtpReasonGenerator;
use Odeo\Domains\Account\Repository\UserKtpRepository;
use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Transaction\Helper\Currency;

class UserKtpSelector implements SelectorListener {

  private $userKtpRepo, $userKtpValidator, $currency;

  public function __construct() {
    $this->userKtpRepo = app()->make(UserKtpRepository::class);
    $this->userKtpValidator = app()->make(UserKtpValidator::class);
    $this->currency = app()->make(Currency::class);
  }

  public function _transforms($item, Repository $repository) {

    if (isAdmin()) {
      $data['id'] = $item->id;
      $data["ktp_nik"] = $item->ktp_nik;
      $data["ktp_name"] = $item->ktp_name;
      $data['ktp_image'] = AwsConfig::S3_BASE_URL . '/' . $item->ktp_image_url;
      $data['ktp_selfie_image'] = AwsConfig::S3_BASE_URL . '/' . $item->ktp_selfie_image_url;
      $data["last_ocash_balance"] = $item->last_ocash_balance;
      $data["last_transaction_description"] = $item->last_transaction_description;
      $data['created_at'] = $item->created_at;
      $data['verified_at'] = $item->verified_at;
      $data['rejected_at'] = $item->rejected_at;
      $data['nik_status'] = $item->nik_status;
      $data['ktp_status'] = $item->ktp_status;
      $data['ktp_selfie_status'] = $item->ktp_selfie_status;
      $data['last_ocash_balance_status'] = $item->last_ocash_balance_status;
      $data['last_transaction_description_status'] = $item->last_transaction_description_status;
      $data['name_status'] = $item->name_status;
     
      if ($item->last_ktp_nik) {
        $data['last_successful_verification'] = [
          'ktp_nik' => $item->last_ktp_nik,
          'name' => $item->last_ktp_name,
          'ktp_image' => AwsConfig::S3_BASE_URL . '/' . $item->last_ktp_image_url,
          'ktp_selfie_image' => AwsConfig::S3_BASE_URL . '/' . $item->last_ktp_selfie_image_url
        ];
      }

      if (isset($item->current_ocash_balance)) {
        $data['current_ocash_balance'] = $this->currency->formatPrice($item->current_ocash_balance);
      }

      $data['user'] = [
        'id' => $item->user_id,
        'name' => $item->name,
        'phone_number' => $item->telephone,
        'email' => $item->email
      ];

      $data['verified_by_user'] = null;
      if ($item->verified_by_user_id) {
        $data['verified_by_user'] = [
          'id' => $item->verified_by_user_id,
          'name' => $item->verified_by_user_name
        ];
      }

      $data['rejected_by_user'] = null;
      if ($item->rejected_by_user_id) {
        $data['rejected_by_user'] = [
          'id' => $item->rejected_by_user_id,
          'name' => $item->rejected_by_user_name
        ];
      }

      $data['total_attempt'] = $item->total_attempt;

    } else if (isset($item->status)) {
      $reasonGenerator = app()->make(UserKtpReasonGenerator::class);
      $data['nik_status'] = $reasonGenerator->getNikReason($item->nik_status);
      $data['name_status'] = $reasonGenerator->getNameReason($item->name_status);
      $data['ktp_status'] = $reasonGenerator->getKtpReason($item->ktp_status);
      $data['ktp_selfie_status'] = $reasonGenerator->getKtpSelfieReason($item->ktp_selfie_status);
      $data['status'] = $item->status;
      $data['rejected_at'] = $item->rejected_at;
      $data['verified_at'] = $item->verified_at;

    } else {
      $data["ktp_nik"] = isset($item->ktp_nik) ? substr_replace($item->ktp_nik, '************', 4, 12) : "";
      $data["ktp_name"] = isset($item->ktp_name) ? $item->ktp_name : "";
      $data['ktp_image'] = isset($item->ktp_image_url) ? 'uploaded' : false;
      $data['ktp_selfie_image'] = isset($item->ktp_selfie_image_url) ? 'uploaded' : false;
    }

    return $data;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }


  public function getByUser(PipelineListener $listener, $data) {

    $userKtp = $this->userKtpRepo->findByUserIdWithoutReject($data['auth']['user_id']);
    if ($userKtp) {
      $userKtp = $this->_transforms($userKtp, $this->userKtpRepo);
      return $listener->response(200, $userKtp);
    }
    return $listener->response(204);

  }

  public function getLastStatusByUser(PipelineListener $listener, $data) {
    $userKtp = $this->userKtpRepo->findLastVerificationByUserId($data['auth']['user_id']);
    if ($userKtp) {
      $userKtp = $this->_transforms($userKtp, $this->userKtpRepo);
      return $listener->response(200, $userKtp);
    }
    return $listener->response(204);
  }

  public function isUserKtpVerified(PipelineListener $listener, $data) {
    $verified = $this->userKtpValidator->isVerified($data['auth']['user_id']);
    return $listener->response(200, ['verified' => $verified]);
  }

  public function get(PipelineListener $listener, $data) {

    $this->userKtpRepo->normalizeFilters($data);
    $this->userKtpRepo->setSimplePaginate(true);

    $userKtpList = [];

    $userKtps = $this->userKtpRepo->gets();
    foreach ($userKtps as $userKtp) {
      $userKtpList[] = $this->_transforms($userKtp, $this->userKtpRepo);
    }

    if (sizeof($userKtpList) > 0) {
      return $listener->response(200, array_merge(
        ["user_ktps" => $this->_extends($userKtpList, $this->userKtpRepo)],
        $this->userKtpRepo->getPagination()
      ));
    }

    return $listener->response(204, ["user_ktps" => []]);
  }

  public function getUserKtpHistory(PipelineListener $listener, $data) {

    $this->userKtpRepo->normalizeFilters($data);
    $this->userKtpRepo->setSimplePaginate(true);

    $userKtpList = [];

    $userKtps = $this->userKtpRepo->getUserKtpHistory($data['user_id']);
    foreach ($userKtps as $userKtp) {
      $userKtpList[] = $this->_transforms($userKtp, $this->userKtpRepo);
    }

    if (sizeof($userKtpList) > 0) {
      return $listener->response(200, array_merge(
        ["user_ktps" => $this->_extends($userKtpList, $this->userKtpRepo)],
        $this->userKtpRepo->getPagination()
      ));
    }

    return $listener->response(204, ["user_ktps" => []]);
  }

}
