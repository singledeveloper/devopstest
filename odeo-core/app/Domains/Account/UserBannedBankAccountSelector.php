<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class UserBannedBankAccountSelector implements SelectorListener {

  public function __construct() {
    $this->userBannedBankAccounts = app()->make(\Odeo\Domains\Account\Repository\UserBannedBankAccountRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $userBannedBankAccounts=[];
    $userBannedBankAccounts["banned_bank_account_id"] = $item->id;
    $userBannedBankAccounts["bank_id"] = $item->bank_id;
    $userBannedBankAccounts["bank_name"] = $item->bank_name;
    $userBannedBankAccounts["account_number"] = $item->account_number;
    $userBannedBankAccounts["is_banned"] = $item->is_banned;
    if(isset($item->banned_reason)) $userBannedBankAccounts["banned_reason"] = $item->banned_reason;
    if(isset($item->created_at)) $userBannedBankAccounts["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if(isset($item->updated_at)) $userBannedBankAccounts["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");
    return $userBannedBankAccounts;
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->userBannedBankAccounts->normalizeFilters($data);
    $this->userBannedBankAccounts->setSimplePaginate(true);

    $userBannedBankAccounts = [];
    foreach ($this->userBannedBankAccounts->gets() as $item) {
      $userBannedBankAccounts[] = $this->_transforms($item, $this->userBannedBankAccounts);
    }

    if (sizeof($userBannedBankAccounts) > 0) {
      return $listener->response(200, array_merge(
        ["user_banned_bank_accounts" => $this->_extends($userBannedBankAccounts, $this->userBannedBankAccounts)],
        $this->userBannedBankAccounts->getPagination()
      ));
    }
    return $listener->response(204, ["user_banned_bank_accounts" => []]);
  }

}
