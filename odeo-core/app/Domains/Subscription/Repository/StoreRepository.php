<?php

namespace Odeo\Domains\Subscription\Repository;

use Carbon\Carbon;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\Model\Store;

class StoreRepository extends Repository {

  public function __construct(Store $store) {
    $this->model = $store;
  }

  public function get() {

    $filters = $this->getFilters();

    $query = $this->getCloneModel();


    if (isset($filters['start_date']) && $filters['start_date']) $query = $query->where('created_at', '>=', $filters['start_date']);
    if (isset($filters['end_date']) && $filters['end_date']) $query = $query->where('created_at', '<=', $filters['end_date']);

    if (isset($filters['search'])) {
      if (isset($filters['search']['user_id'])) {

        $tempq = $query->whereHas('userStores', function ($query) use ($filters) {
          $query->where('user_id', '=', $filters['search']['user_id']);
        });

        if (!$tempq->count()) {
          $tempq = $query->whereHas('userStores.user', function ($query) use ($filters) {
            $query->where('telephone', '=', purifyTelephone($filters['search']['user_id']));
          });
        }
        $query = $tempq;
      }

      if (isset($filters['search']['domain_name'])) {
        $query = $query->where(function ($query) use ($filters) {
          $query->orWhere("subdomain_name", "like", "%" . $filters['search']['domain_name'] . "%")
            ->orWhere("domain_name", "like", "%" . $filters['search']['domain_name'] . "%");
        });
      }
      if (isset($filters['search']['subdomain_name'])) {
        $query = $query->where(function ($query) use ($filters) {
          $query->orWhere("subdomain_name", "like", "%" . $filters['search']['subdomain_name'] . "%");
        });
      }
      if (isset($filters['search']['store_name'])) {

        $query = $query->where('name', 'ilike', '%' . $filters['search']['store_name'] . '%');
      }
      if (isset($filters['search']['store_id'])) {
        $query = $query->where('id', '=', $filters['search']['store_id']);
      }
      if (isset($filters['search']['plan_type'])) {
        $id = $filters['search']['plan_type'];
        $query = $query->whereHas('plan', function ($query) use ($id) {
          $query->where('plans.id', $id);
        });
      }
      if (isset($filters['search']['store_status'])) {
        $query = $query->where('status', $filters['search']['store_status']);
      } else {
        $query = $query->where("status", "<>", StoreStatus::CANCELLED);
      }

      if (isset($filters['search']['invited_by'])) {
        $name = $filters['search']['invited_by'];
        $query = $query->whereHas('network', function ($query) use ($name) {
          $query->whereHas('referredStore', function ($query) use ($name) {
            $query->where('name', 'ilike', '%' . $name . '%');
          });
        });
      }
    }

    if ($this->hasExpand('plan')) $query = $query->with('plan');
    if ($this->hasExpand('invitator')) $query = $query->with('network.referredStore');
    if ($this->hasExpand('network_status')) $query = $query->with('network');
    if ($this->hasExpand('account')) $query = $query->with('storeAccount');

    if (isset($filters["store_ids"])) $query = $query->whereIn("id", $filters["store_ids"]);

    if (!isset($filters['sort_by'])) $filters['sort_by'] = 'updated_at';
    if (!isset($filters['sort_type'])) $filters['sort_type'] = 'desc';

    if (isset($filters["is_active"]) && $filters["is_active"]) {
      $query = $query->whereIn("status", StoreStatus::ACTIVE);
    }

    return $this->getResult($query->orderBy($filters['sort_by'], $filters['sort_type']));
  }


  public function getUsersHaveNoStore() {


    $query = $this->getCloneModel();

    $query = $query
      ->with('plan')
      ->whereNotIn('id', value(function () {
        $userModel = app()->make(User::class);
        $users = $userModel
          ->with('userStores')
          ->whereHas('userStores.store', function ($query) {
            $query->whereIn('status', StoreStatus::ACTIVE);
          })
          ->get();

        $ids = [];

        foreach ($users as $user) {
          foreach ($user->userStores as $userStore) {
            $ids[] = $userStore->store_id;
          }
        }
        return $ids;
      }));

    return $this->getResult($query);
  }

  public function findExpiredStores() {
    return $this->model->whereIn('status', StoreStatus::ACTIVE)
      ->where('plan_id', '<>', Plan::FREE)
      ->where('renewal_at', '<=', Carbon::now()->toDateTimeString())->get();
  }

  public function findBySubdomain($subdomain) {
    return $this->model->where('subdomain_name', $subdomain)
      ->whereIn("status", StoreStatus::ACTIVE)->first();
  }

  public function findByDomain($domain, $options = []) {
    $filters = $this->getFilters();

    if (substr($domain, 0, 4) == 'www.') $domain = substr($domain, 4);
    if (app()->environment() == 'production') $checkDom = ".odeo.co.id";
    else if (app()->environment() == 'staging') $checkDom = "." . env('APP_STAGING_BASE_URL');
    else $checkDom = ".odeo.dev";

    if (strrpos($domain, $checkDom) !== false) $domain = str_replace($checkDom, "", $domain);

    $query = $this->getCloneModel();

    if (isset($options['with'])) {
      $query = $query->with($options['with']);
    }

    return $query->where(function ($query) use ($domain) {
      $query->where("subdomain_name", $domain)->orWhere("domain_name", $domain);
    })->whereIn("status", StoreStatus::ACTIVE)->first($filters["fields"]);
  }

  public function findByStoreIds($storeIds, $with = []) {
    $query = $this->model;
    $filters = $this->getFilters();
    if (count($with) > 0) $query = $query->with($with);
    return $query->whereIn('id', $storeIds)->where("status", "<>", StoreStatus::CANCELLED)->get(isset($filters["fields"]) ? $filters["fields"] : ['*']);
  }

  public function findOwner($storeId) {
    return $this->model->find($storeId)->userStores()->where("type", "owner")->first();
  }

  public function getStoreActivity() {
    return $this->runCache('activity', 'activity.store_activity.', 1440, function () {
      return [
        'active' => $this->model->whereIn('status', StoreStatus::ACTIVE)->count(),
      ];
    });
  }

  public function getAllActiveStore($with = []) {
    $query = $this->model;
    if (count($with) > 0) $query = $query->with($with);
    return $query->whereIn('status', StoreStatus::ACTIVE)
      ->orderBy('id', 'asc')->get();
  }

  public function getAllActiveMarketPlace($with = []) {
    $query = $this->model;
    if (count($with) > 0) $query = $query->with($with);
    return $query->whereIn('status', StoreStatus::ACTIVE)
      ->whereHas('distribution', function($query) {
        $query->where('marketplace', true);
      })
      ->orderBy('id', 'asc')->get();
  }

  public function getAllActiveStoreWithoutFreeStore($with = []) {
    $query = $this->model;
    if (count($with) > 0) $query = $query->with($with);
    return $query->whereIn('status', StoreStatus::ACTIVE)
      ->where('id', '<>', StoreStatus::STORE_DEFAULT)
      ->where('plan_id', '<>', Plan::FREE)
      ->orderBy('id', 'asc')->get();
  }

  public function getAllFreeActiveStoreIds() {
    $query = $this->getCloneModel();
    return $query->where('plan_id', Plan::FREE)
      ->whereIn('status', StoreStatus::ACTIVE)
      ->select('id')
      ->get();
  }

  public function getAvailableStores($userId, $serviceId = null) {
    $query = $this->model
      ->select('stores.*', 'services.name as service_name', 'is_selected')
      ->leftJoin('agents', 'stores.id', '=', 'agents.store_referred_id')
      ->leftJoin('agent_priorities', 'agents.id', '=', 'agent_priorities.agent_id')
      ->leftJoin('services', 'service_id', '=', 'services.id')
      ->where('user_id', $userId)
      ->where('priority', '>', 0)
      ->whereIn('agents.status', [AgentConfig::PENDING, AgentConfig::ACCEPTED])
      ->orderBy('priority');

    if ($serviceId) {
      $query = $query->where('service_id', $serviceId);
    }

    return $query->get();
  }

  public function getSupplierStores() {
    return $this->model->where('can_supply', true)->get();
  }

  public function getAllMasterStoreIds($userId) {
    return $this->model
      ->join('agents', 'stores.id', 'agents.store_referred_id')
      ->where('user_id', $userId)
      ->where('agents.status', AgentConfig::ACCEPTED)
      ->pluck('stores.id');
  }

}
