<?php

namespace Odeo\Domains\Subscription\Repository;

use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Salesreport\Helper\Initializer;

class PlanRepository extends Repository {

  public function __construct(\Odeo\Domains\Subscription\Model\Plan $plan) {
    $this->model = $plan;
  }

  public function get() {

    $filters = $this->getFilters();

    $query = $this->getCloneModel();

    $hide = [Plan::FREE, Plan::STARTER, Plan::LITE, Plan::BUSINESS, Plan::PRO, Plan::ENTERPRISE];
    
    // if (!isset($filters['show_free_domain'])) $hide[] = Plan::FREE;

    $query = $query->whereNotIn('id', $hide);

    if (isset($filters["current_cost"])) {
      $query = $query->where('cost_per_month', '>', $filters["current_cost"])->orderBy("cost_per_month", "asc");
    } else if (isset($filters["current_plan"])) {
      if (!$plan = $this->findById($filters["current_plan"])) return [];
      $query = $query->where('cost_per_month', '>', $plan->cost_per_month)->orderBy("cost_per_month", "asc");
    }

    $result = $this->getResult($query->orderBy("cost_per_month", "asc"));

    if (isset($plan)) {
      foreach ($result as $item) {
        $item->cost_per_month_before = $item->cost_per_month;
        $item->cost_per_month -= $plan->cost_per_month;
      }
    } else if (isset($filters["current_cost_difference"])) {
      foreach ($result as $item) {
        $item->cost_per_month_before = $item->cost_per_month;
        $item->cost_per_month -= $filters["current_cost_difference"];
      }
    }

    return $result;

  }

  public function findMostExpensive() {

    $key = $this->model->getTable();

    return $this->runCache($key, $key . '.most_expensive', 60 * 60 * 24, function () {

      return $this->model->orderBy('cost_per_month', 'DESC')->first();

    });
  }

  public function getByPlatformProduct() {

    $query = $this->getCloneModel();

    $query = $query->whereNotIn("id", [Initializer::PLAN_FREE]);

    return $query->get();

  }

}
