<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/3/17
 * Time: 17:11
 */

namespace Odeo\Domains\Subscription\DistributionChannel;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;

class ChannelUpdater {

  public function __construct() {
    $this->distChannelRepos = app()->make(\Odeo\Domains\Subscription\DistributionChannel\Repository\StoreDistributionChannelRepository::class);
    $this->storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->storeRepos = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->redis = Redis::connection();
  }

  public function createChannel(PipelineListener $listener, $data) {

    $store = $this->storeRepos->findById($data["store_id"]);

    if ($channel = $this->distChannelRepos->findByAttributes('store_id', $store->id));
    else {

      $channel = $this->distChannelRepos->getNew();

      $channel->store_id = $store->id;
      $channel->marketplace = $store->plan_id != Plan::FREE;
      $channel->webstore = true;

      $this->distChannelRepos->save($channel);
    }

    return $listener->response(200, [
      'marketplace' => $channel->marketplace
    ]);
  }


  public function updateStatus(PipelineListener $listener, $data) {

    $resSucc = false;

    $channel = $this->distChannelRepos->findByAttributes('store_id', $data['store_id']);

    if (!isset($data['content'])) {
      return $listener->response(400, 'no data received');
    }

    foreach ($data['content'] as $content) {

      $content['store_id'] = $data['store_id'];

      $channel->{$content['key']} = !$channel->{$content['key']};

      $key = ($channel->{$content['key']} ? 'enable' : 'disable') . '_' . $content['key'];

      try {

        switch ($key) {
          case 'enable_marketplace':
            $this->enableMarketPlace($content);
            break;
          case 'disable_marketplace':
            $this->disableMarketPlace($content);
            break;
        }

        $resSucc = true;

      } catch (\Exception $exception) {
        \Log::info(\json_encode($exception->getMessage()));
      }
    }

    if (!$resSucc) return $listener->response(400);

    $this->distChannelRepos->save($channel);
    return $listener->response(200);

  }


  private function enableMarketPlace($data) {

    if (!($store = $this->storeRepos->findById($data['store_id'])) && $store->plan_id != Plan::FREE) {
      throw new \Exception('store not found');
    }

    $inventories = json_decode($this->storeParser->currentData($store->plan_id), true);

    foreach ($inventories as $serviceDetailId) {

      $key = $store->id . ':' . $serviceDetailId;
      $score = $this->redis->hget('odeo_core:store_profit_temp', $key);

      $profitQueueNamespace = 'odeo_core:store_profit_queue:' . ServiceDetail::getServiceId($serviceDetailId);

      if (isset($score)) {
        $this->redis->zadd($profitQueueNamespace, 'nx', $score, $key);
        $this->redis->hdel('odeo_core:store_profit_temp', $key);

      } else if ($this->redis->zrank($profitQueueNamespace, $key) === null) {
        $this->redis->zadd($profitQueueNamespace, 0, $key);
      }
    }
  }

  private function disableMarketPlace($data) {

    if (!($store = $this->storeRepos->findById($data['store_id']))) {
      throw new \Exception('store not found');
    }

    $dataToBeInsert = [];
    $inventories = json_decode($this->storeParser->currentData($store->plan_id), true);

    foreach ($inventories as $serviceDetailId) {

      if (!($serviceId = ServiceDetail::getServiceId($serviceDetailId))) continue;

      $profitQueueNamespace = 'odeo_core:store_profit_queue:' . $serviceId;

      $key = $store->id . ':' . $serviceDetailId;

      $score = $this->redis->zscore($profitQueueNamespace, $key);

      if (isset($score)) $dataToBeInsert[$key] = $score;

    }

    !empty($dataToBeInsert) && $this->redis->pipeline(function ($pipe) use ($store, $inventories, $dataToBeInsert) {

      $tempNamespace = 'odeo_core:store_profit_temp';

      $pipe->hmset($tempNamespace, $dataToBeInsert);
      $pipe->expireAt($tempNamespace, Carbon::tomorrow()->timestamp);

      foreach ($inventories as $serviceDetailId) {
        $profitQueueNamespace = 'odeo_core:store_profit_queue:' . ServiceDetail::getServiceId($serviceDetailId);
        $key = $store->id . ':' . $serviceDetailId;
        $this->redis->zrem($profitQueueNamespace, $key);
      }
    });
  }


}