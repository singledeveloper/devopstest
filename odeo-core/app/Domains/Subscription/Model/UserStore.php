<?php

namespace Odeo\Domains\Subscription\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Network\Model\NetworkTree;
use Odeo\Domains\Core\Entity;

class UserStore extends Entity
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];



    public function storeData($userId, $store_id, $type = 'owner') {
        $this->user_id = $userId;
        $this->store_id = $store_id;
        $this->type = $type;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function store() {
        return $this->belongsTo(Store::class);
    }

    public function plan() {
        return $this->belongsTo(Plan::class);
    }
}
