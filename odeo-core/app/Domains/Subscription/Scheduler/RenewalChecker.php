<?php

namespace Odeo\Domains\Subscription\Scheduler;

use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\MdPlusStatus;

class RenewalChecker {

  public function __construct() {
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->cashes = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
  }

  public function check() {
    $storeMdPlus = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\StoreMdPlusRepository::class);

    if (Plan::RENEWAL_ACTIVE) {
      $stores = $this->stores->findExpiredStores();
      $storeDeposits = $this->cashes->findStoreBalance($stores->pluck('id')->toArray(), CashType::ODEPOSIT);

      foreach ($stores as $store) {
        $store->status = $store->status == StoreStatus::WAIT_FOR_RENEWAL_VERIFY ? StoreStatus::WAIT_FOR_EXP_RENEWAL_VERIFY : StoreStatus::EXPIRED;
        
        $mdPlus = $storeMdPlus->findByAttributes('store_id', $store->id);
        $mdPlus->status = MdPlusStatus::EXPIRED;
        
        if($this->stores->save($store) && $storeMdPlus->save($mdPlus) && isset($storeDeposits[$store->id]) && $storeDeposits[$store->id] > 0) {

          $userId = $store->owner->first()->id;
          $storeId = $store->id;
          $inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
          $inserter->add([
            'user_id' => $userId,
            'store_id' => $storeId,
            'trx_type' => \Odeo\Domains\Constant\TransactionType::RETURN_DEPOSIT,
            'cash_type' => \Odeo\Domains\Constant\CashType::ODEPOSIT,
            'amount' => -$storeDeposits[$storeId],
            'data' => json_encode([
              'notes' => 'store expired'
            ])
          ]);
          $inserter->add([
            'user_id' => $userId,
            'trx_type' => \Odeo\Domains\Constant\TransactionType::RETURN_DEPOSIT,
            'cash_type' => \Odeo\Domains\Constant\CashType::OCASH,
            'amount' => $storeDeposits[$storeId],
            'data' => json_encode([
              'notes' => 'store expired'
            ])
          ]);
          $inserter->run();
        }
      }
    }
  }
}
