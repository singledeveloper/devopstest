<?php

namespace Odeo\Domains\Subscription\MdPlus;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\MdPlusStatus;

class ReferredUserSelector {

  private $mdPlus, $store, $currency;

  public function __construct() {
    $this->userReferral = app()->make(\Odeo\Domains\Marketing\Userreferral\Repository\UserReferralCodeRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }
  
  public function _transforms($item, Repository $repository) {
    $referredUser = [];
    
    $referredUser['user_id'] = $item->user_id;
    $referredUser['user_telephone'] = $item->user->telephone;
    $referredUser['user_name'] = $item->user->name;
    $referredUser['join_date'] = $item->user->created_at->format('Y-m-d');

    return $referredUser;
  }
  
  public function _extends($data, Repository $repository) {
    if ($userIds = $repository->beginExtend($data, 'user_id')) {
      if ($repository->hasExpand('referral_cashbacks')) {
        $cashbackRepo = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\ReferralCashbackRepository::class);

        $today = \Carbon\Carbon::today();
        $dateRange = [
          [
            'date' => $today->toDateString(),
            'name' => 'today'
          ],
          [
            'date' => $today->subDay(1)->toDateString(),
            'name' => 'yesterday'
          ]
        ];
        $userCashbacks = $cashbackRepo->getByDateAndUser(array_pluck($dateRange, 'date'), $userIds)->toArray();
        $cashbacks = array();

        foreach ($userCashbacks as $userCashback) {
          $cashbacks[$userCashback['date']][$userCashback['user_id']] = $userCashback;
        }
        
        foreach ($dateRange as $date) {
          $key = $date['date'];
          foreach ($userIds as $userId) {
            $referralCashbacks[$userId][$date['name']] = [
              'total_claimed' => $this->currency->formatPrice(isset($cashbacks[$key][$userId]) ? $cashbacks[$key][$userId]['total_claimed'] : 0),
              'total_unclaimed' => $this->currency->formatPrice(isset($cashbacks[$key][$userId]) ? $cashbacks[$key][$userId]['total_unclaimed'] : 0),
              'total_sales' => isset($cashbacks[$key][$userId]) ? $cashbacks[$key][$userId]['total_sales'] : 0
            ];
          }
        }

        $repository->addExtend('referral_cashbacks', $referralCashbacks);
      }
    }
    return $repository->finalizeExtend($data);
  }
  
  public function get(PipelineListener $listener, $data) {
    $this->userReferral->normalizeFilters($data);

    $referredUsers = $this->userReferral->get(); 
    $referred = array();

    foreach ($referredUsers as $referredUser) {
      $referred[] = $this->_transforms($referredUser, $this->userReferral);
    }

    if (sizeof($referred) > 0) {
      return $listener->response(200, array_merge(['referred_users' => $this->_extends($referred, $this->userReferral)], $this->userReferral->getPagination()));
    }

    return $listener->response(204);
  }
  
}