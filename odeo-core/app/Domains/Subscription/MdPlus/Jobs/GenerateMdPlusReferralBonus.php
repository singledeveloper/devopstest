<?php

namespace Odeo\Domains\Subscription\MdPlus\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\MdPlus\MdPlusBonusRequester;
use Odeo\Jobs\Job;

class GenerateMdPlusReferralBonus extends Job implements ShouldQueue {

  use SerializesModels;

  private $userId, $mdPlusPlanId;

  public function  __construct($userId, $mdPlusPlanId) {
    parent::__construct();
    $this->userId = $userId;
    $this->mdPlusPlanId = $mdPlusPlanId;
  }

  public function handle() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(MdPlusBonusRequester::class, 'referralBonus'));

    $pipeline->enableTransaction();
    $pipeline->execute([
      'user_id' => $this->userId,
      'md_plus_plan_id' => $this->mdPlusPlanId
    ]);
  }

}
