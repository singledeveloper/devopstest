<?php

namespace Odeo\Domains\Subscription\ManageInventory\Repository;

use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\InventoryStore;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\ManageInventory\Model\StoreInventory;

class StoreInventoryRepository extends Repository {

  public function __construct(StoreInventory $storeInventory) {
    $this->model = $storeInventory;
  }

  public function findByStoreService($storeId, $serviceDetailId) {
    return $this->model->where('store_id', $storeId)->where('service_detail_id', $serviceDetailId)->first();
  }

  public function findByStoreIds($storeIds) {
    return $this->model->whereIn('store_id', $storeIds)->get();
  }

  public function findByStoreId($storeId) {
    return $this->model->where('store_id', $storeId)->get();
  }

  public function findByStoreIdAndServiceDetailId($storeId, $serviceDetailId) {
    return $this->model->where('store_id', $storeId)
      ->where('service_detail_id', $serviceDetailId)
      ->first();
  }

  public function getGroupPrice($storeId, $serviceDetailId) {
    return $this->model
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
      ->where('store_id', $storeId)
      ->where('service_detail_id', $serviceDetailId)
      ->select(
        'store_inventory_group_prices.id',
        'store_inventory_group_prices.type',
        'store_inventory_group_prices.name',
        'store_inventory_group_prices.description',
        'store_inventory_group_prices.keep_profit_consistent',
        'store_inventory_group_prices.pricing_apply_at'
      )
      ->orderBy('id', 'asc')
      ->get();
  }


  public function getMyVendorWithPriceLists($data) {

    $storeIds = '(' . implode(',', $data['store_id']) . ')';
    $groupPriceIds = $data['group_price_id'] ? '(' . implode(',', $data['group_price_id']) . ')' : null;

    return \DB::select(\DB::raw("
          WITH inventories AS (SELECT
             stores.id                                         AS vendor_store_id,
             stores.name                                       AS vendor_name,
             store_inventory_details.id                        AS inventory_detail_id,
             store_inventory_details.inventory_id              AS inventory_id,
             store_inventory_group_prices.type                 AS group_price_type,
             store_inventory_group_prices.pricing_apply_at     AS group_price_apply_at,
             store_inventory_group_price_details.sell_price    AS vendor_price,
             store_inventory_group_price_details.planned_price AS vendor_planned_price,
             store_inventory_group_price_details.id            AS inventory_group_price_detail_id,
             store_inventory_group_prices.id                   AS inventory_group_price_id
           FROM store_inventories
             JOIN stores ON store_inventories.store_id = stores.id
             JOIN store_inventory_details ON store_inventories.id = store_inventory_details.store_inventory_id
             JOIN store_inventory_group_prices
               ON store_inventories.id = store_inventory_group_prices.store_inventory_id
             JOIN store_inventory_group_price_details
               ON
                 store_inventory_group_price_details.store_inventory_detail_id = store_inventory_details.id
                 AND
                 store_inventory_group_price_details.store_inventory_group_price_id =
                 store_inventory_group_prices.id
           WHERE store_inventories.store_id IN " . $storeIds . " AND
                 store_inventories.service_detail_id = " . (int)$data['service_detail_id'] . " AND
                 (
                    " . ($groupPriceIds ? ("store_inventory_group_prices.id IN" . $groupPriceIds . " OR ") : "") .
      "store_inventory_group_prices.type = '" . InventoryGroupPrice::AGENT_DEFAULT . "'
                 )
          )
          SELECT *
          FROM inventories inv1
          WHERE NOT exists(
              SELECT 1
              FROM inventories inv2
              WHERE inv2.vendor_store_id = inv1.vendor_store_id AND
                    inv2.inventory_id = inv1.inventory_id AND
                    inv1.group_price_type != inv2.group_price_type 
                    " . ($groupPriceIds ? (" AND inv2.inventory_group_price_id IN " . $groupPriceIds) : "") . "
              )
          ORDER BY inv1.inventory_id ASC
    "));
  }

  public function getOdeoBasePriceByInventory($storeId, $serviceDetailId) {
    return $this->getModel()
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('pulsa_odeos', 'store_inventory_details.inventory_id', '=', 'pulsa_odeos.id')
      ->where('store_inventories.store_id', $storeId)
      ->where('store_inventories.service_detail_id', $serviceDetailId)
      ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
      ->select(
        'store_inventories.id as store_inventory_id',
        'store_inventory_details.id as inventory_detail_id',
        'store_inventory_details.inventory_id as inventory_id',
        'pulsa_odeos.price as sell_price'
      )
      ->get();
  }

  public function getBasePriceByInventory($storeId, $serviceDetailId) {
    return $this->model
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_price_details', 'store_inventory_group_price_details.id', '=', 'store_inventory_details.vendor_group_price_detail_id')
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.id', '=', 'store_inventory_group_price_details.store_inventory_group_price_id')
      ->where('store_inventories.store_id', $storeId)
      ->where('store_inventories.service_detail_id', $serviceDetailId)
      ->select(
        'store_inventories.id as store_inventory_id',
        'store_inventory_details.id as inventory_detail_id',
        'store_inventory_details.inventory_id as inventory_id',
        'store_inventory_group_price_details.sell_price as sell_price'
      )
      ->get();
  }

  public function getGroupPricePricingByInventory($groupPriceId, $storeId = null, $serviceDetailId = null) {
    $query = $this->model
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_price_details', 'store_inventory_group_price_details.store_inventory_detail_id', '=', 'store_inventory_details.id')
      ->whereIn('store_inventory_group_price_details.store_inventory_group_price_id', is_array($groupPriceId) ? $groupPriceId : [$groupPriceId])
      ->select(
        'store_inventory_group_price_details.store_inventory_group_price_id',
        'store_inventories.id as store_inventory_id',
        'store_inventory_details.id as inventory_detail_id',
        'store_inventory_details.inventory_id',
        'store_inventory_group_price_details.sell_price'
      );

    if ($storeId) $query->where('store_inventories.store_id', $storeId);
    if ($serviceDetailId) $query->where('store_inventories.service_detail_id', $serviceDetailId);

    return $query->get();
  }

  public function getStorePricing($data, $extendSelect = [], $order = false) {

    $query = $this->model
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('pulsa_odeos', 'store_inventory_details.inventory_id', '=', 'pulsa_odeos.id');

      if (isset($data['group_price_id']) && $data['group_price_id']) {
      $query
        ->join('store_inventory_group_price_details', 'store_inventory_group_price_details.store_inventory_detail_id', '=', 'store_inventory_details.id')
        ->where('store_inventory_group_price_details.store_inventory_group_price_id', $data['group_price_id']);
    } else {
      $query->join('store_inventory_group_prices', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
        ->join('store_inventory_group_price_details', function ($join) {
          $join->on('store_inventory_group_price_details.store_inventory_detail_id', '=', 'store_inventory_details.id');
          $join->on('store_inventory_group_price_details.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id');
        })
        ->where('store_inventory_group_prices.type', '=', $data['group_price_type'] ?? InventoryGroupPrice::AGENT_DEFAULT);
    }

    $query->where('store_inventories.store_id', '=', $data['store_id'])
      ->where('store_inventories.service_detail_id', '=', $data['service_detail_id'])
      ->where('pulsa_odeos.status', Pulsa::STATUS_OK);

    $order && $query->orderBy('pulsa_odeos.operator_id', 'asc')
      ->orderBy('pulsa_odeos.category', 'asc')
      ->orderBy('pulsa_odeos.price', 'asc');

    return $query->select($extendSelect)
      ->get();

  }

  public function getAllUplineSalePrice($data) {

    return \DB::select(\DB::raw("
      WITH RECURSIVE inventories (user_id, store_id, plan_id, store_name, store_status, store_can_supply, vendor_group_price_detail_id, sell_price, n) AS (
        SELECT
          " . $data['store']['user_id'] . "::BIGINT, 
          " . $data['store']['id'] . " :: BIGINT,
          " . $data['store']['plan_id'] . ":: INT,
          '" . $data['store']['name'] . "'::VARCHAR(50),
          '" . $data['store']['status'] . "'::CHAR(5),
          " . ($data['store']['can_supply'] ? 'true' : 'false') . "::BOOLEAN,
          vendor_group_price_detail_id,
          " . $data['sale_price'] . " :: NUMERIC,
          1
        FROM store_inventory_details
        WHERE id = " . $data['store_inventory_detail_id'] . "
        UNION ALL
          SELECT
            user_stores.user_id,
            store_inventories.store_id,
            stores.plan_id,
            stores.name AS store_name,
            stores.status AS store_status,
            stores.can_supply AS store_can_supply,
            store_inventory_details.vendor_group_price_detail_id,
            store_inventory_group_price_details.sell_price,
            inv.n + 1
          FROM
            store_inventory_group_price_details
            JOIN inventories inv
              ON store_inventory_group_price_details.id = inv.vendor_group_price_detail_id
            JOIN store_inventory_details
              ON store_inventory_group_price_details.store_inventory_detail_id = store_inventory_details.id
            JOIN store_inventories ON store_inventory_details.store_inventory_id = store_inventories.id
            JOIN user_stores ON user_stores.store_id = store_inventories.store_id
            JOIN stores ON stores.id = user_stores.store_id
            WHERE 
                  store_inventory_group_price_details.id = inv.vendor_group_price_detail_id AND
                  user_stores.type = 'owner'
      )
      SELECT
        user_id,
        store_id,
        plan_id,
        store_name,
        store_status,
        store_can_supply,
        vendor_group_price_detail_id,
        sell_price,
        n
      FROM inventories ORDER BY n ASC"));

  }


}
