<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/15/17
 * Time: 7:33 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory\Jobs;

use Odeo\Jobs\Job;
use Redis;

class AddNewProductToStoreInventory extends Job {

  private $data;
  private $storeInventories, $storesInventoryDetails, $currencyHelper, $pulsaOdeos,
    $serviceDetails;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->storesInventoryDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryDetailRepository::class);
    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
  }

  public function handle() {

    $data = $this->data;
    $redis = Redis::connection();

    if ($redis->setnx('odeo_core:add_new_product_to_store_inventory_lock', 1) === 0) return;

    $dataTobeInsert = [];

    $storeInventoryIds = $this->storeInventories->getCloneModel()
      ->select('id')
      ->where('service_detail_id', $data['service_detail_id'])
      ->get();

    $inventories = $this->pulsaOdeos->whereIn('id', [
      $data['inventory_ids']
    ])->select('id', 'price')
      ->get();

    $serviceDetail = $this->serviceDetails->findById($data['service_detail_id']);
    $now = date('Y-m-d H:i:s');

    foreach ($storeInventoryIds as $storeInventoryId) {
      foreach ($inventories as $inventory) {
        $vendorPrice = $this->currencyHelper->formatPrice($inventory->price, $serviceDetail)['merchant_price'];
        $dataTobeInsert[] = "('" . $storeInventoryId . "', '" . $vendorPrice . "', '" . $vendorPrice . "', '" . $inventory->id . "', '" . $now . "', '" . $now . "')";
      }
    }

    \DB::beginTransaction();

    \DB::statement('
      with inventories(store_inventory_id, vendor_price, sell_price, inventory_id, created_at, updated_at) as (
        values ' . implode(',', $dataTobeInsert) . '
        ) 
        insert into store_inventory_details (store_inventory_id, vendor_price, sell_price, inventory_id, created_at, updated_at)
        select d.store_inventory_id, vendor_price, sell_price, inventory_id, created_at, updated_at
        from inventories inv
        where not exists(
          select 1 
          from store_inventory_details 
          where 
            store_inventory_details.store_inventory_id = inv.store_inventory_id and
            store_inventory_details.inventory_id = inv.inventory_id
        )
      )
    ');

    \DB::commit();

    $redis->del('odeo_core:add_new_product_to_store_inventory_lock');


  }

}
