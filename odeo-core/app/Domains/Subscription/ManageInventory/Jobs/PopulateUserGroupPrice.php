<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/30/17
 * Time: 17:27
 */

namespace Odeo\Domains\Subscription\ManageInventory\Jobs;


use Carbon\Carbon;
use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository;
use Odeo\Jobs\Job;

class PopulateUserGroupPrice extends Job {

  private $data;
  private $storeInventories, $storeParser;
  private $storeInventoryGroupPrices, $storeInventoryGroupPriceUsers;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->storeInventoryGroupPrices = app()->make(StoreInventoryGroupPriceRepository::class);
    $this->storeInventoryGroupPriceUsers = app()->make(StoreInventoryGroupPriceUserRepository::class);

  }

  public function handle() {

    $data = $this->data;

    if (isset($data['user_id'])) {

      $this->populateSingleUser($data);

    } else {

      $this->populateRelatedUser($data);
    }

  }


  private function populateRelatedUser($data) {

    $dataToBeInsert = [];
    $now = Carbon::now();

    $users = \DB::select(\DB::raw("  
      SELECT
        agents.user_id,
        service_detail_id,
        agents.user_referred_id
      FROM store_inventories store_inv
        JOIN agents ON agents.store_referred_id = store_inv.store_id
      WHERE agents.status = '" . AgentConfig::ACCEPTED . "'
        AND store_inv.store_id = agents.store_referred_id AND store_id = " . $data['store_id'] . "
        AND service_id IN (
          " . implode(',', $data['service_ids']) . "
        )
        AND NOT exists(
          SELECT *
          FROM store_inventory_group_price_users
            JOIN store_inventory_group_prices
              ON store_inventory_group_price_users.store_inventory_group_price_id = store_inventory_group_prices.id
          WHERE  store_inv.id = store_inventory_group_prices.store_inventory_id
        )
     "));

    if (empty($users)) return;

    $groupPrices = $this->storeInventoryGroupPrices
      ->getGroupPriceByType($data['store_id'], $data['service_ids'], InventoryGroupPrice::AGENT_DEFAULT)
      ->keyBy('service_detail_id');

    foreach ($users as $user) {

      $userId = $user->user_id;

      if ($userId == $user->user_referred_id) {
        $gp = $this->storeInventories->getModel()
          ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
          ->where('store_id', $data['store_id'])
          ->where('service_detail_id', $user->service_detail_id)
          ->where('type', InventoryGroupPrice::SELF)
          ->select(
            'store_inventory_group_prices.id as store_inventory_group_price_id'
          )
          ->first();
      }
      else {
        $gp = $groupPrices[$user->service_detail_id];
      }

      $dataToBeInsert[] = [
        'user_id' => $userId,
        'store_inventory_group_price_id' => $gp->store_inventory_group_price_id,
        'created_at' => $now,
        'updated_at' => $now
      ];
    }

    $this->storeInventoryGroupPriceUsers->saveBulk($dataToBeInsert);
  }


  private function populateSingleUser($data) {
    $now = Carbon::now()->toDateTimeString();

    $storeAvailableService = isset($data['service_ids']) ? $data['service_ids'] : Plan::getAvailableServices($data['plan_id']);

    $usedServiceIds = $this->storeInventories->getModel()
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_price_users', 'store_inventory_group_price_users.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id')
      ->where('store_id', $data['store_id'])
      ->where('store_inventory_group_price_users.user_id', $data['user_id'])
      ->whereIn('service_id', $storeAvailableService)
      ->select(
        'service_id'
      )->get()->pluck('service_id')->all();

    $diffs = array_diff($storeAvailableService, $usedServiceIds);

    if (empty($diffs)) return;

    \DB::statement("
      with group_prices as (
        select 
          store_inventory_group_prices.id as group_price_id,
          " . $data['user_id'] . " as user_id,
          '$now'::timestamp(0) as created_at
         from store_inventories 
        join store_inventory_group_prices on store_inventory_group_prices.store_inventory_id = store_inventories.id
        where store_id = " . (int)$data['store_id'] . " and
        service_id in (" . implode(',', $diffs) . ") and 
        type = '" . ($data['group_price_type'] ?? InventoryGroupPrice::AGENT_DEFAULT) . "'
      )
      insert into store_inventory_group_price_users (user_id, store_inventory_group_price_id, created_at, updated_at)
      select gp.user_id, gp.group_price_id, gp.created_at, gp.created_at from group_prices gp
    ");
  }


}
