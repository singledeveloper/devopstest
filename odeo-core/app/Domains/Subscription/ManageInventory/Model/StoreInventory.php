<?php

namespace Odeo\Domains\Subscription\ManageInventory\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\ServiceDetail;
use Odeo\Domains\Subscription\Model\Store;

class StoreInventory extends Entity {

  public function store() {
    return $this->belongsTo(Store::class);
  }

  public function serviceDetail() {
    return $this->belongsTo(ServiceDetail::class);
  }

  public function inventoryDetails() {
    return $this->hasMany(StoreInventoryDetail::class);
  }

  public function inventoryVendors() {
    return $this->hasMany(StoreInventoryVendor::class);
  }

}
