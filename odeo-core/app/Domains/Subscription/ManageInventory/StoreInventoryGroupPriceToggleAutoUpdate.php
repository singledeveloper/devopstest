<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/24/17
 * Time: 21:56
 */

namespace Odeo\Domains\Subscription\ManageInventory;


use Odeo\Domains\Core\PipelineListener;

class StoreInventoryGroupPriceToggleAutoUpdate {

  public function __construct() {
    $this->storeInventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
  }

  public function toggle(PipelineListener $listener, $data) {

    if ($this->storeInventoryGroupPrices->getModel()
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
      ->where('store_id', $data['store_id'])
      ->where('service_detail_id', $data['service_detail_id'])
      ->where('store_inventory_group_prices.id', $data['group_price_id'])
      ->update([
        'keep_profit_consistent' => \DB::raw('not store_inventory_group_prices.keep_profit_consistent')
      ])) {

      return $listener->response(200);

    }

    return $listener->response(400);


  }

}