<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/11/17
 * Time: 19:10
 */

namespace Odeo\Domains\Subscription\ManageInventory;

use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Core\PipelineListener;

class StoreInventoryGroupPriceRemover {

  private $inventoryGroupPrices, $inventoryGroupPriceUsers;

  public function __construct() {
    $this->inventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
    $this->inventoryGroupPriceUsers = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository::class);
  }

  public function remove(PipelineListener $listener, $data) {

    $groupPrice = $this->inventoryGroupPrices->getModel()
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
      ->where('store_id', $data['store_id'])
      ->where('service_detail_id', $data['service_detail_id'])
      ->where('store_inventory_group_prices.id', $data['selected_group_price_id'])
      ->select('type')
      ->first();


    if (!$groupPrice || $groupPrice->type != InventoryGroupPrice::CUSTOM) {
      return $listener->response(400, 'Group price could not be deleted. group price not found.');
    }

    if ($this->inventoryGroupPriceUsers->getModel()
      ->where('store_inventory_group_price_id', $data['selected_group_price_id'])
      ->count()
    ) {
      return $listener->response(400, 'Group price could not be deleted. Agent still used this group price.');
    }

    if (!$this->inventoryGroupPrices->getModel()->where('id', $data['selected_group_price_id'])->delete()) {
      return $listener->response(400, 'Group price could not be deleted. group price not found.');
    }

    return $listener->response(200);

  }


}