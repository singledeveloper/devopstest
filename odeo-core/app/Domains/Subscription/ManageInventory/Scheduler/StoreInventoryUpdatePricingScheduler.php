<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/18/17
 * Time: 12:57 AM
 */

namespace Odeo\Domains\Subscription\ManageInventory\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Subscription\ManageInventory\Jobs\StabilizeStoreChildrenProfit;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceDetailRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository;

class StoreInventoryUpdatePricingScheduler {

  public function __construct() {
    $this->storeInventoryGroupPrices = app()->make(StoreInventoryGroupPriceRepository::class);
    $this->storeInventoryGroupPriceDetails = app()->make(StoreInventoryGroupPriceDetailRepository::class);
  }

  public function run() {
    $now = Carbon::now()->toDateTimeString();

    $inventoryDetails = $this->storeInventoryGroupPrices->getModel()
      ->join('store_inventory_group_price_details', 'store_inventory_group_price_details.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id')
      ->whereNotNull('store_inventory_group_prices.pricing_apply_at')
      ->where('pricing_apply_at', '<=', Carbon::now()->toDateTimeString())
      ->where('sell_price', '<>', \DB::raw('planned_price'))
      ->select(
        'store_inventory_group_price_details.id as group_price_detail_id',
        'store_inventory_group_prices.id as group_price_id'
      )
      ->get();

    if ($inventoryDetails->isEmpty()) {
      return;
    }

    try {

      \DB::beginTransaction();

      $groupPriceDetailid = join(',', $inventoryDetails->pluck('group_price_detail_id')->all());

      $responseSelect = \DB::select(\DB::raw("
        UPDATE store_inventory_group_price_details dtl
          SET 
            sell_price = dtl.planned_price,
            updated_at = '$now'
        FROM 
          store_inventory_group_price_details old_dtl 
          join store_inventory_details on old_dtl.store_inventory_detail_id = store_inventory_details.id
        WHERE 
          old_dtl.id = dtl.id AND
          dtl.id in ($groupPriceDetailid)
        RETURNING 
          old_dtl.sell_price as price_before,
          dtl.sell_price as price_after,
          store_inventory_details.inventory_id,
          dtl.store_inventory_group_price_id
      "));

      $this->storeInventoryGroupPrices->getModel()
        ->whereIn('id', $inventoryDetails->pluck('group_price_id'))
        ->update([
          'pricing_apply_at' => null
        ]);

      \DB::commit();

      $inventories = [];

      foreach ($responseSelect as $res) {
        if ($res->price_before == $res->price_after) continue;
        $inventories[$res->store_inventory_group_price_id][] = [
          'inventory_id' => $res->inventory_id,
          'price_before' => $res->price_before,
          'price_after' => $res->price_after,
        ];
      }

      !empty($inventories) && dispatch(new StabilizeStoreChildrenProfit([
        'inventories' => $inventories,
        'filter' => 'group_price'
      ]));


    } catch (\Exception $exception) {

      \DB::rollback();

      throw $exception;
    }

  }

}
