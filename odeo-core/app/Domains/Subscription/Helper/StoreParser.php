<?php

namespace Odeo\Domains\Subscription\Helper;

use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\StoreStatus;

class StoreParser {

  private $planResult = [], $plan;

  public function __construct() {
    $this->plan = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class);
  }

  public function domainName($data) {
    if (isset($data->domain_name) && $data->domain_name != null) {
      return $data->domain_name;
    } else if (isset($data->subdomain_name) && $data->subdomain_name != null) {
      return $data->subdomain_name . (app()->environment() == 'production' ? ".odeo.co.id" : '.' . env('APP_STAGING_BASE_URL'));
    }
    return "";
  }

  public function status($data) {

    $codes = [
      StoreStatus::OK => "OK",
      StoreStatus::PENDING => "PENDING",
      StoreStatus::CANCELLED => "CANCELLED",
      StoreStatus::WAIT_FOR_UPGRADE_VERIFY => "WAIT_FOR_UPGRADE_VERIFY",
      StoreStatus::WAIT_FOR_RENEWAL_VERIFY => "WAIT_FOR_RENEWAL_VERIFY",
      StoreStatus::EXPIRED => "EXPIRED",
      StoreStatus::WAIT_FOR_EXP_RENEWAL_VERIFY => "WAIT_FOR_EXP_RENEWAL_VERIFY"
    ];

    if (!isset($data->status) || (isset($data->status) && !isset($codes[$data->status]))) {
      $tempStatus = "PENDING";
    } else {
      $tempStatus = $codes[$data->status];
      if ($tempStatus == "OK" && $data->plan_id != Plan::FREE) {
        $ra = $data->renewal_at;
        if (strtotime($ra) <= time()) {
          $tempStatus = "RENEWAL_NOW";
        } else if (strtotime($ra) <= strtotime("+2 weeks")) {
          $tempStatus = "RENEWAL_WARNING";
        }
      }
    }

    return $tempStatus;
  }

  public function statusMessage($item) {
    $codes_message = [
      "OK" => "Active",
      "PENDING" => "Pending",
      "WAIT_FOR_UPGRADE_VERIFY" => "Verifying Upgrade",
      "WAIT_FOR_RENEWAL_VERIFY" => "Verifying Renewal",
      "RENEWAL_NOW" => "Must Renewal",
      "RENEWAL_WARNING" => "Expiring Soon",
      "EXPIRED" => "Expired",
      "WAIT_FOR_EXP_RENEWAL_VERIFY" => "Verifying Renewal"
    ];

    return isset($codes_message[$item]) ? $codes_message[$item] : "";
  }

  public function currentData($planId, $currentData = "", $overwriteCurrentData = false, $canSupply = false) {
    $defaultVendor = [
      "pulsa" => ServiceDetail::PULSA_ODEO,
      "paket_data" => ServiceDetail::PAKET_DATA_ODEO,
      'pln' => ServiceDetail::PLN_ODEO,
      "pulsa_postpaid" => ServiceDetail::PULSA_POSTPAID_ODEO,
      "bolt" => ServiceDetail::BOLT_ODEO,
      "emoney" => ServiceDetail::EMONEY_ODEO,
      'pln_postpaid' => ServiceDetail::PLN_POSTPAID_ODEO,
      'pdam' => ServiceDetail::PDAM_ODEO,
      'pgn' => ServiceDetail::PGN_ODEO,
      'bpjs_kes' => ServiceDetail::BPJS_KES_ODEO,
      'broadband' => ServiceDetail::BROADBAND_ODEO,
      'landline' => ServiceDetail::LANDLINE_ODEO,
      "flight" => ServiceDetail::FLIGHT_TIKET,
      "hotel" => ServiceDetail::HOTEL_TIKET,
      "credit_bill" => ServiceDetail::CREDIT_BILL_ODEO,
      "game_voucher" => ServiceDetail::GAME_VOUCHER_ODEO,
      "google_play" => ServiceDetail::GOOGLE_PLAY_ODEO,
      "transportation" => ServiceDetail::TRANSPORTATION_ODEO
    ];

    if ($canSupply) return json_encode($defaultVendor);

    $availableServices = Plan::getAvailableServices($planId);

    $current = json_decode($currentData, true);
    if ($overwriteCurrentData || !$current) {
      foreach ($defaultVendor as $vendorType => $defaultVendorId) {
        $serviceId = Service::getConstValueByKey($vendorType);
        $current[$vendorType] = in_array($serviceId, $availableServices) ? $defaultVendorId : 0;
      }
    } else {
      foreach ($defaultVendor as $vendorType => $defaultVendorId) {
        $serviceId = Service::getConstValueByKey($vendorType);

        if (!isset($current[$vendorType])) {
          $current[$vendorType] = in_array($serviceId, $availableServices) ? $defaultVendorId : 0;
        } else if ($current[$vendorType] && !in_array($serviceId, $availableServices)) {
          $current[$vendorType] = 0;
        }
      }
    }

    $currentNew = [];
    foreach ($defaultVendor as $key => $item) {
      if (isset($current[$key])) {
        $currentNew[$key] = $current[$key];
      }
    }
    return json_encode($currentNew);
  }
}


