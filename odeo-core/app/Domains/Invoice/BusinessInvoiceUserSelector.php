<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 30/12/19
 * Time: 11.17
 */

namespace Odeo\Domains\Invoice;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Invoice\Repository\BusinessInvoiceUserRepository;

class BusinessInvoiceUserSelector {

  function __construct() {
    $this->invoiceUsers = app()->make(BusinessInvoiceUserRepository::class);
  }

  public function getInvoiceUser(PipelineListener $listener, $data) {
    if (!$invoiceUser = $this->invoiceUsers->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('invoice/error.invoice_user_not_found'));
    }

    return $listener->response(200, [
      'business_invoice_user_id' => $invoiceUser->id,
      'charge_to_customer' => $invoiceUser->charge_to_customer,
    ]);
  }

}