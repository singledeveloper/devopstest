<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/12/19
 * Time: 14.11
 */

namespace Odeo\Domains\Invoice;


use Illuminate\Support\Facades\Validator;
use Odeo\Domains\Core\PipelineListener;

class BulkInvoiceValidator {

  public function validate(PipelineListener $listener, $data) {
    $rows = $data['rows'];

    $validator = $this->createValidator($rows);
    if ($validator->fails()) {
      return $listener->response(400, $validator->errors()->all());
    }

    return $listener->response(200);
  }

  private function createValidator($rows) {

    $fields = [
      'billed_user_id',
      'invoice_name',
      'items',
      'billed_user_name',
      'billed_user_email',
      'billed_user_telephone',
      'billed_user_address'
    ];
    $attributeNames = [];
    foreach ($rows as $key => $_) {
      $rowNo = $key + 1;

      foreach ($fields as $field) {
        $attributeNames["row.$key.$field"] = trans("misc.row_field", ['row' => $rowNo, 'field' => $field]);
      }
    }

    $validateData = [
      'rows' => $rows
    ];

    $validator = Validator::make($validateData, [
      'rows' => 'min:1|max:500',
      'rows.*.invoice_name' => 'required',
      'rows.*.items.*.amount' => 'required|integer|min:1',
      'rows.*.items.*.price' => 'required|integer|min:1000',
      'rows.*.billed_user_name' => 'required',
      'rows.*.billed_user_email' => 'email',
    ]);

    $validator->setAttributeNames($attributeNames);
    return $validator;
  }

}