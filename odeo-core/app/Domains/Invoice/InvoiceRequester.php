<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 12/11/19
 * Time: 19.47
 */

namespace Odeo\Domains\Invoice;


use Carbon\Carbon;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceUserRepository;
use Odeo\Domains\VirtualAccount\VirtualAccountManager;

class InvoiceRequester extends VirtualAccountManager {

  private $userInvoices, $userInvoiceUsers;

  function __construct() {
    parent::__construct();
    $this->userInvoices = app()->make(AffiliateUserInvoiceRepository::class);
    $this->userInvoiceUsers = app()->make(AffiliateUserInvoiceUserRepository::class);
  }

  public function addInvoiceItems(PipelineListener $listener, $data) {
    $invoiceItems = $data['invoice_items'];
    $userId = $data['auth']['user_id'];
    $subtotal = $this->calculateSubtotal($invoiceItems);

    if (isset($data['billed_user_id'])) {
      if (!$invoiceUser = $this->userInvoiceUsers->findUserByCreatedBy(UserInvoice::BILLER_ODEO, $data['billed_user_id'], $userId)) {
        return $listener->response(400, trans('invoice/error.billed_user_not_found'));
      }
    } else if (isset($data['billed_user_name'])){
      $invoiceUser = $this->userInvoiceUsers->getNew();
      $invoiceUser->biller_id = UserInvoice::BILLER_ODEO;
      $invoiceUser->user_id = time();
      $invoiceUser->name = $data['billed_user_name'] ?? '';
      $invoiceUser->email = $data['billed_user_email'] ?? '';
      $invoiceUser->telephone = $data['billed_user_telephone'] ?? '';
      $invoiceUser->address = $data['billed_user_address'] ?? '';
      $invoiceUser->created_by = $userId;
      $this->userInvoiceUsers->save($invoiceUser);
      $this->createVaUser($invoiceUser->user_id, $userId);
    } else {
      return $listener->response(400, trans('invoice/error.billed_user_not_found'));
    }

    if (!$invoice = $this->userInvoices->findOpenInvoiceUserInvoice($invoiceUser->user_id, $userId)) {
      $invoice = $this->userInvoices->getNew();
      $invoice->billed_user_id = $invoiceUser->user_id;
      $invoice->billed_user_name = $invoiceUser->name;
      $invoice->billed_user_email = $invoiceUser->email;
      $invoice->billed_user_telephone = $invoiceUser->telephone;
      $invoice->user_id = $userId;
      $invoice->biller_id = UserInvoice::BILLER_ODEO;
      $invoice->created_by = $userId;
    }

    $invoice->period = Carbon::now()->format('Y-m');
    $invoice->invoice_number = UserInvoice::generateInvoiceNumber(UserInvoice::BILLER_ODEO);
    $invoice->name = $data['invoice_name'];
    $invoice->items = $invoiceItems;
    $invoice->subtotal = $subtotal;
    $invoice->total = $subtotal;
    $invoice->charges = 0;
    $invoice->due_date = $data['invoice_due_date'] ?? null;
    $invoice->notes = $data['invoice_notes'] ?? '';
    $invoice->status = UserInvoice::OPENED;

    $this->userInvoices->save($invoice);

    $userVa = $this->userVirtualAccounts->findByUserIdAndCreatedBy($invoiceUser->user_id, $userId);
    $userVa->service_reference_id = $invoice->invoice_number;
    $userVa->display_name = $data['invoice_name'];
    $this->userVirtualAccounts->save($userVa);

    return $listener->response(200, [
      'id' => $invoice->id,
      'billed_user_id' => $invoice->billed_user_id,
      'invoice_name' => $invoice->name,
      'invoice_number' => $invoice->invoice_number,
      'invoice_items' => $invoiceItems,
      'invoice_subtotal' => $subtotal,
      'invoice_total' => $subtotal,
      'invoice_due_date' => $invoice->due_date,
      'invoice_notes' => $invoice->notes,
    ]);
  }

  private function calculateSubtotal($items) {
    $subtotal = 0;
    foreach ($items as $item) {
      $subtotal += $item['amount'] * $item['price'];
    }
    return $subtotal;
  }

}