<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 27/12/19
 * Time: 15.55
 */

namespace Odeo\Domains\Invoice;


use Odeo\Domains\Activity\Repository\HolidayRepository;
use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Invoice\Repository\BusinessInvoiceUserRepository;
use Odeo\Domains\Invoice\Repository\BusinessInvoiceUserSettingRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayChannelRepository;

class BusinessInvoiceUserSettingRequester {

  function __construct() {
    $this->invoiceUsers = app()->make(BusinessInvoiceUserRepository::class);
    $this->pgChannels = app()->make(PaymentGatewayChannelRepository::class);
    $this->invoiceUserSettings = app()->make(BusinessInvoiceUserSettingRepository::class);
  }

  function getInvoicePaymentChannels(PipelineListener $listener, $data) {
    if (!$invoiceUser = $this->invoiceUsers->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    $paymentChannels = $this->pgChannels->getPgInvoiceChannels($invoiceUser->id);

    $channels = [];
    foreach ($paymentChannels as $channel) {
      if (isProduction() && in_array($channel->id, [5,8,9]) && $data['auth']['user_id'] != 19) {
        continue;
      }

      $channels[] = [
        'channel_id' => $channel->id,
        'name' => trans('payment_gateway.channel_' . $channel->channel_name),
        'logo' => AssetAws::getAssetPath(AssetAws::PAYMENT, $channel->logo),
        'fee' => $channel->fee ?? $channel->default_fee,
        'settlement_type' => $channel->settlement_type ?? $channel->default_settlement_type,
        'settlement_fee' => $channel->settlement_fee ?? $channel->default_settlement_fee,
        'active' => $channel->active ?? false
      ];
    }

    $holidays = [];
    $holidayRepo = app()->make(HolidayRepository::class);
    foreach ($holidayRepo->getNextMonth(date('Y-m-d'), 2) as $item) {
      $holidays[] = [
        'date' => $item->event_date,
        'description' => trans('holiday.' . $item->description,
          $item->translation_data ? json_decode($item->translation_data, true) : []),
        'settlement_increment' => $item->settlement_increment
      ];
    }

    return $listener->response(200, [
      'channels' => $channels,
      'holidays' => $holidays
    ]);
  }

  function toggleInvoicePaymentChannel(PipelineListener $listener, $data) {
    if (!$invoiceUser = $this->invoiceUsers->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    if (!$channel = $this->pgChannels->findById($data['channel_id'])) {
      return $listener->response(400, trans('api_users.invalid_pg_channel'));
    }

    if ($invoiceSetting = $this->invoiceUserSettings->findByBusinessInvoiceUserIdAndChannelId($invoiceUser->id, $data['channel_id'])) {
      $invoiceSetting->active = !$invoiceSetting->active;
    } else {
      $invoiceSetting = $this->invoiceUserSettings->getNew();
      $invoiceSetting->business_invoice_user_id = $invoiceUser->id;
      $invoiceSetting->payment_gateway_channel_id = $channel->id;
      $invoiceSetting->settlement_type = $channel->default_settlement_type;
      $invoiceSetting->fee = $channel->default_fee;
      $invoiceSetting->settlement_fee = $channel->default_settlement_fee;
      $invoiceSetting->active = true;
    }
    $this->invoiceUserSettings->save($invoiceSetting);

    return $listener->response(200, [
      'channel_id' => $channel->id,
      'channel_fee' => $invoiceSetting->fee,
      'settlement_type' => $invoiceSetting->settlement_type,
      'settlement_fee' => $invoiceSetting->settlement_fee,
      'active' => $invoiceSetting->active
    ]);
  }

  function updateSettlement(PipelineListener $listener, $data) {
    if (!$invoiceUser = $this->invoiceUsers->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    $invoiceSetting = $this->invoiceUserSettings->findByBusinessInvoiceUserIdAndChannelId($invoiceUser->id, $data['channel_id']);

    if (!$invoiceSetting || !$invoiceSetting->active) {
      return $listener->response(400, trans('api_users.invalid_pg_channel'));
    }

    if (strlen($data['settlement_type']) != strlen($invoiceSetting->channel->default_settlement_type)) {
      return $listener->response(400, trans('api_users.invalid_settlement_type'));
    }

    foreach(str_split($data['settlement_type']) as $idx => $settlementDay) {
      if ($settlementDay > $invoiceSetting->channel->default_settlement_type[$idx]) {
        return $listener->response(400, trans('api_users.invalid_settlement_type'));
      }
    }

    $invoiceSetting->settlement_type = $data['settlement_type'];
    $this->invoiceUserSettings->save($invoiceSetting);

    return $listener->response(200, [
      'channel_id' => $invoiceSetting->payment_gateway_channel_id,
      'channel_fee' => $invoiceSetting->fee,
      'settlement_type' => $invoiceSetting->settlement_type,
      'settlement_fee' => $invoiceSetting->settlement_fee,
      'active' => $invoiceSetting->active
    ]);
  }

}