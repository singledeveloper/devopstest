<?php

namespace Odeo\Domains\Payment\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Model\PaymentSettlement;

class PaymentSettlementRepository extends Repository {

  public function __construct(PaymentSettlement $paymentSettlement) {
    $this->model = $paymentSettlement;
  }
  
  public function gets() {
    $query = $this->getCloneModel();
    $filters = $this->getFilters();

    if (isset($filters['search'])) {
      if (isset($filters['search']['id'])) {
        $query = $query->where('id', $filters['search']['id']);
      }
      if (isset($filters['search']['bank_id'])) {
        $query = $query->where('bank_id', $filters['search']['bank_id']);
      }
      if (isset($filters['search']['settlement_date_from'])) {
        $query = $query->where('settlement_date', '>=', $filters['search']['settlement_date_from']);
      }
      if (isset($filters['search']['settlement_date_to'])) {
        $query = $query->where('settlement_date_to', '<=', $filters['search']['settlement_date_to']);
      }
    }

    return $this->getResult($query->orderBy($filters['sort_by'], $filters['sort_type']));
  }
}