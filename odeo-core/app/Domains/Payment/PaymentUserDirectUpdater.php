<?php

namespace Odeo\Domains\Payment;

use Odeo\Domains\Core\PipelineListener;

class PaymentUserDirectUpdater {

  public function updateUserDirectPaymentPreferences(PipelineListener $listener, $data) {

    $users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);

    $user = $users->findById($data['auth']['user_id']);

    $user->direct_payment_reference_type = $data['opc_group'];
    $user->direct_payment_reference_id = $data['id'];

    if ($users->save($user)) {
      return $listener->response(200);
    }

    return $listener->response(400);
  }

  public function delete(PipelineListener $listener, $data) {
    $directPayments = app()->make(\Odeo\Domains\Payment\Repository\PaymentUserDirectPaymentRepository::class);
    $users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);

    if (!$directPayment = $directPayments->findById($data['direct_payment_id'])) {
      return $listener->response(400, trans('errors.data_not_exists'));
    }

    $userId = $data['auth']['user_id'];
    $user = $users->findById($userId);

    if ($directPayment->user_id != $userId) {
      return $listener->response(400, trans('errors.account_mismatch'));
    }

    if ($directPayments->delete($directPayment)) {
      if($user->direct_payment_reference_id == $directPayment->id) {
        $user->direct_payment_reference_type = \Odeo\Domains\Constant\Payment::OPC_GROUP_OCASH;
        $user->direct_payment_reference_id = \Odeo\Domains\Constant\Payment::OPC_GROUP_OCASH;

        $users->save($user);
      }
      return $listener->response(200);
    }

    return $listener->response(400, trans('errors.database'));
  }
}
