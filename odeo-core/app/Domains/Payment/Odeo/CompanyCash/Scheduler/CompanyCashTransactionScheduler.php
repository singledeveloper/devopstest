<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 08/06/18
 * Time: 19.48
 */

namespace Odeo\Domains\Payment\Odeo\CompanyCash\Scheduler;


use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Odeo\CompanyCash\CompanyCashTransactionUpdater;

class CompanyCashTransactionScheduler {

  public function run() {
    $pipeline = new Pipeline;
    $pipeline->add(new Task(CompanyCashTransactionUpdater::class, 'updateTransactions'));

    $pipeline->execute([]);
  }

}