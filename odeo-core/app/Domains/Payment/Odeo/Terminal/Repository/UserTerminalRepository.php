<?php


namespace Odeo\Domains\Payment\Odeo\Terminal\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\Terminal\Model\UserTerminal;

class UserTerminalRepository extends Repository {

  public function __construct(UserTerminal $terminal) {
    $this->model = $terminal;
  }

  public function findActiveByTerminalId($terminalId) {
    return $this->model
      ->where('terminal_id', '=', $terminalId)
      ->where('is_active', '=', true)
      ->first();
  }

  public function listEnabledEdcTransaction() {
    return $this->model
      ->where('is_active', true)
      ->where('enable_edc_transactions', true)
      ->get();
  }
}