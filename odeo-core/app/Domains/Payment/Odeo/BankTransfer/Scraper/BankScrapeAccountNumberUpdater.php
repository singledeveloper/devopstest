<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/6/16
 * Time: 1:48 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper;


use Odeo\Domains\Core\PipelineListener;
use Validator;


class BankScrapeAccountNumberUpdater {

  public function updateInquiry(PipelineListener $listener, $data) {
    if ($data['bank'] == 'mandiri_giro') {
      $inquiries = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository::class);
    } else {
      $class = 'Odeo\\Domains\\Payment\\Odeo\\BankTransfer\\Scraper\\' . ucfirst($data['bank']) . '\\Repository\\Bank' . ucfirst($data['bank']) . 'InquiryRepository';
      $inquiries = app()->make($class);
    }

    $inquiry = $inquiries->findById($data['inquiry_id']);

    $inquiry->reference = $data['reference_type'] == 'note' ? $data['reference'] : json_encode($data['reference']);
    $inquiry->reference_type = $data['reference_type'];

    if ($inquiries->save($inquiry)) {
      return $listener->response(200);
    }
    return $listener->response(400);
  }


  public function updateInquiries(PipelineListener $listener, $data) {


    foreach ($data as $key => $inquiriesData) {


      $class = 'App\\Domains\\' . ucfirst($key) . '\\' . ucfirst($key) . 'Inquiry';

      $model = new $class;

      foreach ($inquiriesData as $inquiryData) {

        $inquiry = $model->find($inquiryData['inquiry_id']);
        $inquiry->reference_type = 'order_id';
        $inquiry->reference = json_encode(["" . $inquiryData['order_id']]);

        $inquiry->save();

      }
    }

    return $listener->buildDataResponse(null);

  }

  public function setInquiriesToUnrecognized(PipelineListener $listener, $inquiries_ids) {

    foreach ($inquiries_ids as $target => $inquiries) {


      $class = 'App\\Domains\\' . ucfirst($target) . '\\' . ucfirst($target) . 'Inquiry';

      $model = new $class;

      foreach ($inquiries as $inquiry_id) {

        $inquiry = $model->find($inquiry_id);
        $inquiry->reference_type = 'note';
        $inquiry->reference = 'unrecognized';

        $inquiry->save();
      }
    }

    return $listener->buildDataResponse(null);

  }
}