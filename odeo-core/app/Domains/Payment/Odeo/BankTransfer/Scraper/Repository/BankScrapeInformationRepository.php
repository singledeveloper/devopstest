<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeInformation;

class BankScrapeInformationRepository extends Repository
{
  public function __construct(BankScrapeInformation $scrapeInformation)
  {
    $this->model = $scrapeInformation;
  }

  /**
   * @param $target
   * @return BankScrapeInformation
   */
  public function getActiveAccount($target)
  {
    return $this->model->with(
      [
        'accounts' => function ($query) {
          $query->where('active', true);
        },
        'accounts.numbers' => function ($query) {
          $query->where('active', true);
        },
      ])->where('target', $target)->first();
  }

}