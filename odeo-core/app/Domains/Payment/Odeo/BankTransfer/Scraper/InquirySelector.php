<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/1/16
 * Time: 10:50 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper;


use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Carbon\Carbon;
use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Constant\BankTransferInquiries;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;

class InquirySelector implements SelectorListener {


  public function __construct() {
    $this->scrapeAccountNumbers = app()->make(BankScrapeAccountNumberRepository::class);
  }

  public function _extends($data, Repository $repository) {
    if ($repository->hasExpand('order_users')) {
      $ids = $repository->beginExtend($data, 'id');
      $rows = [];
      $references = array_filter($data, function($ref) {
        return $ref->reference_type == BankTransferInquiries::REFERENCE_TYPE_ORDER_ID;
      });
      foreach($references as $ref) {
        $refIds = json_decode($ref->reference);
        $rows[$ref->id] = $refIds;
      }
      $orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
      $orderIds = array();

      foreach ($rows as $id => $refIds) {
        $orderIds = array_merge($orderIds, $refIds);
      }

      $orderUsers = $orders->getModel()->whereIn('id', $orderIds)->get()->pluck('user_id', 'id');

      $results = [];
      foreach ($ids as $id) {
        $temp = [];

        if (isset($rows[$id])) {
          $orderIds = $rows[$id];

          foreach ($orderIds as $orderId) {
            $temp[] = "" . $orderUsers[$orderId];
          }
        }
        $results[$id] = json_encode(array_unique($temp));
      }
      $repository->addExtend('user_ids', $results);
    }

    return $repository->finalizeExtend($data);
  }

  public function _transforms($item, Repository $repository) {
    if (isset($item->pdf_url)) {
      $item->pdf_url = AwsConfig::S3_BASE_URL . '/' . $item->pdf_url;
    }
    return $item;
  }

  public function get(PipelineListener $listener, $data) {
    $this->scrapeAccountNumbers->normalizeFilters($data);
    $this->scrapeAccountNumbers->setSimplePaginate(true);

    $inquiries = [];
    $inquiriesRaw = $this->scrapeAccountNumbers->inquiries($data['bank'], $data['id']);

    foreach ($inquiriesRaw['content'] as $item) {
      $inquiries[] = $this->_transforms($item, $this->scrapeAccountNumbers);
    }

    if (count($inquiries) > 0) {
      return $listener->response(200, array_merge([
        'inquiries' => $this->_extends($inquiries, $this->scrapeAccountNumbers),
        'last_time_scrapped' => $inquiriesRaw['last_time_scrapped']->toDateTimeString(),
      ],
        $this->scrapeAccountNumbers->getPagination()
      ));
    }
    return $listener->response(204);
  }


  public function getAmountById(PipelineListener $listener, $data) {

    switch ($data['bank']) {
      case 'mandiri_giro':
        $class = 'Odeo\\Domains\\Payment\\Odeo\\BankTransfer\\Scraper\\MandiriVer2\\Repository\\BankMandiriGiroInquiryRepository';
        break;
      default:
        $class = 'Odeo\\Domains\\Payment\\Odeo\\BankTransfer\\Scraper\\' . ucfirst($data['bank']) . '\\Repository\\Bank' . ucfirst($data['bank']) . 'InquiryRepository';
        break;
    }


    $inquiries = app()->make($class);

    $inquiry = $inquiries->findById($data['inquiry_id']);

    if (!$inquiry) return $listener->response(400, 'inquiry not found');

    return $listener->response(200, [
      'credit_amount' => $inquiry->credit,
      'debit_amount' => $inquiry->debit
    ]);
  }

  public function getExportedInquiry($data) {

    $this->scrapeAccountNumbers->normalizeFilters($data);

    $inquiriesRaw = $this->scrapeAccountNumbers->inquiries($data['bank'], $data['id']);

    $inquiries = $inquiriesRaw['content'];

    $writer = WriterFactory::create(Type::CSV);

    $writer->openToBrowser($data['bank'] . ' ' . ($data['start_date'] ?? '0000-00-00') . ' to ' . ($data['end_date'] ?? Carbon::now()->toDateString()) . '.csv');

    $headers = null;

    switch ($data['bank']) {
      case 'mandiri':
      case 'mandiri_giro':
        $headers = [
          'id','date', 'date_value', 'description', 'reference_number', 'credit', 'debit', 'balance', 'reference_type', 'reference'
        ];
        break;
      case 'bca':
        $headers = [
          'id','date', 'name', 'description', 'branch', 'credit', 'debit', 'balance', 'reference_type', 'reference'
        ];
        break;
      case 'bri':
        $headers = [
          'id','date', 'description', 'teller_id', 'credit', 'debit', 'balance', 'reference_type', 'reference'
        ];
        break;
      case 'bni':
        $headers = [
          'id','date', 'journal', 'description', 'branch', 'credit', 'debit', 'balance', 'reference_type', 'reference'
        ];
        break;
      case 'cimb':
        $headers = [
          'id','date', 'bank_reff_no', 'description', 'credit', 'debit', 'balance', 'reference_type', 'reference'
        ];
        break;
      case 'permata':
        $headers = [
          'id', 'post_date', 'eff_date', 'trx_code', 'reff_no', 'cust_reff_no', 'description', 'credit', 'debit', 'balance', 'reference_type', 'reference'
        ];
        break;
      case 'maybank':
        $headers = [
          'id', 'date', 'transaction_time', 'posting_date', 'processing_time', 'description', 'transaction_ref', 'debit', 'credit', 'end_balance', 'source_code', 'teller_id', 'transaction_code', 'reference_type', 'reference'
        ];
    }

    $writer->addRow($headers);

    foreach ($inquiries as $inquiry) {

      $temp = [];

      foreach ($headers as $h) {
        if ($h == 'reference' && ($val = json_decode($inquiry[$h]))) $temp[] = join(', ', $val);
        else $temp[] = $inquiry[$h];
      }

      $writer->addRow($temp);
    }

    $writer->close();
  }


}
