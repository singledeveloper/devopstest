<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/27/16
 * Time: 8:22 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository;

use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Model\BankBcaInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankInquiryRepository;

class BankBcaInquiryRepository extends BankInquiryRepository {

  public function __construct(BankBcaInquiry $bcaInquiry) {
    $this->setModel($bcaInquiry);
  }

  public function getInquriesFrom($from) {
    return $this->model->orderBy('date', 'asc')
      ->orderBy('id', 'asc')
      ->where(function($q) use ($from) {
        $q->whereDate('date', '>=', $from)
          ->orWhereNull('date');
      })
      ->get();
  }

  public function getPendingInquiry() {
    return $this->model
      ->whereNull('date')
      ->orderBy('id', 'asc')
      ->get();
  }

  public function getLastRecordedBalance($account_number_id) {
    return $this->model->whereNotNull('balance')
      ->where('bank_scrape_account_number_id', $account_number_id)
      ->orderBy('created_at', 'desc')->first();
  }

  public function getLastRecordedBalanceByAccountNumber($account_number) {
    return $this->model
      ->join('bank_scrape_account_numbers', 'bank_scrape_account_number_id', '=', 'bank_scrape_account_numbers.id')
      ->whereNotNull('balance')
      ->where('number', $account_number)
      ->orderBy('bank_bca_inquiries.created_at', 'desc')
      ->first();
  }

  function getInquiries($from, $to) {
    return $this->model->getmodel()
      ->orderBy('date', 'asc')
      ->orderBy('id', 'asc')
      ->whereNotNull('date')
      ->whereDate('date', '>=', $from)
      ->whereDate('date', '<=', $to)
      ->get();
  }

  function getUnknownVaSettlement($vaIndex) {
    $query = "
      WITH data AS (
          SELECT
            *,
            row_number() OVER (partition by date order by id asc) rn,
            count(*) OVER (partition by date) count
          FROM bank_bca_inquiries
          WHERE 
            description like :description 
          ORDER BY id
      )
      SELECT *
      FROM data
      WHERE 
        ((rn = :va_index and count > 1) or (rn = 1 and count = 1))
        and (reference_type = :reference_type or reference_type is null) 
        and deleted_at is null
    ";

    return \DB::select(\DB::raw($query), [
      'description' => 'TRM BCA VA %',
      'reference_type' => 'note',
      'va_index' => $vaIndex,
    ]);
  }

  function getUnreconciledDokuSettlement() {
    return $this->model
      ->orderBy('date', 'asc')
      ->orderBy('id', 'asc')
      ->whereRaw("description LIKE '%NUSA SATU INTI ART%'")
      ->where('reference_type', 'note')
      ->whereDate('date', '>=', '2019-08-16')
      ->get();
  }

}
