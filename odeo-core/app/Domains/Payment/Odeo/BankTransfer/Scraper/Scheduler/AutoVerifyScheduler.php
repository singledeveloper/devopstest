<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/1/16
 * Time: 2:55 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Scheduler;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Order\Jobs\VerifyOrder;

class AutoVerifyScheduler {

  private $orders, $bankAccounts;

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->bankAccounts = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository::class);
  }

  public function _separate($order) {
    if ($order->status == OrderStatus::CONFIRMED || $order->status == OrderStatus::FAKE_CONFIRMED) {
      $confirmation = $order->payment->bankTransferDetail;
      $temp['confirmation_amount'] = $confirmation->transfer_amount;
    }

    $temp['status'] = $order->status;
    $temp['total'] = $order->total;
    $temp['order_id'] = $order->id;
    $temp['created_at'] = Carbon::parse($order->created_at);

    return $temp;
  }


  public function _removeDuplicate($matched, $orderId) {
    return array_filter($matched, function ($item) use ($orderId) {
      return $item->order_id != $orderId;
    });
  }

  public function run() {

    $transferData = $this->bankAccounts->getUnreconciledInquiries();

    $count = 0;

    foreach ($transferData as $t) {
      $count += count($t);
    }

    if (!$count) return;

    $orders = [];

    $userOrders = $this->orders->getCloneModel()
      ->with('payment.bankTransferDetail')
      ->whereHas('payment', function ($q) {
        $q->whereIn('info_id', [
          Payment::OPC_GROUP_BANK_TRANSFER_BCA,
          Payment::OPC_GROUP_BANK_TRANSFER_BRI,
          Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI,
          Payment::OPC_GROUP_BANK_TRANSFER_CIMB,
          Payment::OPC_GROUP_BANK_TRANSFER_BNI,
          Payment::OPC_GROUP_BANK_TRANSFER_PERMATA
        ]);
      })
      ->whereIn('status', [OrderStatus::OPENED, OrderStatus::CONFIRMED, OrderStatus::FAKE_CONFIRMED])
      ->where('expired_at', '>', date('Y-m-d H:i:s', time() - 85500))
      ->get();

    foreach ($userOrders as $order) {

      switch ($order->payment->info_id) {
        case Payment::OPC_GROUP_BANK_TRANSFER_BCA:
          $orders['bca'][] = $this->_separate($order);
          break;
        case Payment::OPC_GROUP_BANK_TRANSFER_BRI:
          $orders['bri'][] = $this->_separate($order);
          break;
        case Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI:
          $orders['mandiri'][] = $this->_separate($order);
          $orders['mandiri_giro'][] = $this->_separate($order);
          break;
        case Payment::OPC_GROUP_BANK_TRANSFER_CIMB:
          $orders['cimb'][] = $this->_separate($order);
          break;
        case Payment::OPC_GROUP_BANK_TRANSFER_BNI:
          $orders['bni'][] = $this->_separate($order);
          break;
        case Payment::OPC_GROUP_BANK_TRANSFER_PERMATA:
          $orders['permata'][] = $this->_separate($order);
          break;
        default:
          continue;
      }
    }

    $mandiriShouldManualCheck = $mandiriMatched = [];
    $mandiriGiroShouldManualCheck = $mandiriGiroMatched = [];
    $bcaShouldManualCheck = $bcaMatched = [];
    $briShouldManualCheck = $briMatched = [];
    $cimbShouldManualCheck = $cimbMatched = [];
    $bniShouldManualCheck = $bniMatched = [];
    $permataShouldManualCheck = $permataMatched = [];

    if (isset($orders['mandiri_giro']) && isset($transferData['mandiri_giro']) && count($transferData['mandiri_giro']) > 0) {

      list($mandiriGiroShouldManualCheck, $mandiriGiroMatched) = $this->_validate($transferData['mandiri_giro'], collect($orders['mandiri_giro']), function ($trData) {

        return +$trData['credit'];

      }, function ($item, $credit, $trData) {

        return $item['total'] == $credit && $trData['date'] >= $item['created_at'];
      });
    }

    if (isset($orders['mandiri']) && isset($transferData['mandiri']) && count($transferData['mandiri']) > 0) {

      list($mandiriShouldManualCheck, $mandiriMatched) = $this->_validate($transferData['mandiri'], collect($orders['mandiri']), function ($trData) {

        return +$trData['credit'];

      }, function ($item, $credit, $trData) {

        return $item['total'] == $credit && $trData['date'] >= $item['created_at'];
      });
    }

    if (isset($orders['cimb']) && isset($transferData['cimb']) && count($transferData['cimb']) > 0) {

      list($cimbShouldManualCheck, $cimbMatched) = $this->_validate($transferData['cimb'], collect($orders['cimb']), function($trData) {

        return +$trData['credit'];

      }, function($item, $credit, $trData) {

        return $item['total'] == $credit && $trData['date'] >= $item['created_at']->toDateString();

      });
    }

    if (isset($orders['bca']) && isset($transferData['bca']) && count($transferData['bca']) > 0) {

      list($bcaShouldManualCheck, $bcaMatched) = $this->_validate($transferData['bca'], collect($orders['bca']), function ($trData) {

        return ((int)$trData['credit']) . '';

      }, function ($item, $credit, $trData) {

        if ($item['total'] != $credit) {
          return false;
        }


        if ($trData['date'] != 'PEND') {
          return Carbon::parse($trData['date'])->toDateString() >= $item['created_at']->toDateString();
        }

        return true;

      });

    }

    if (isset($orders['bri']) && isset($transferData['bri']) && count($transferData['bri']) > 0) {

      list($briShouldManualCheck, $briMatched) = $this->_validate(
        $transferData['bri'], collect($orders['bri']),
        function ($trData) {
          return +$trData['credit'];
        },
        function ($item, $credit, $trData) {
          return $item['total'] == $credit && $trData['date'] >= $item['created_at'];
        }
      );

    }

    if (isset($orders['bni']) && isset($transferData['bni']) && count($transferData['bni']) > 0) {
      list($bniShouldManualCheck, $bniMatched) = $this->_validate(
        $transferData['bni'], collect($orders['bni']),
        function ($trData) {
          return +$trData['credit'];
        },
        function ($item, $credit, $trData) {
          return $item['total'] == $credit && $trData['date'] >= $item['created_at'];
        }
      );
    }

    if (isset($orders['permata']) && isset($transferData['permata']) && count($transferData['permata']) > 0) {
      list($permataShouldManualCheck, $permataMatched) = $this->_validate($transferData['permata'], collect($orders['permata']), function($trData) {

        return +$trData['credit'];

      }, function($item, $credit, $trData) {
        return $item['total'] == $credit && $trData['post_date'] >= $item['created_at']->toDateString();
      });
    }

    $intersection = array_intersect(
      array_pluck($mandiriMatched, 'order_id'),
      array_pluck($mandiriGiroMatched, 'order_id'),
      array_pluck($bcaMatched, 'order_id'),
      array_pluck($briMatched, 'order_id'),
      array_pluck($cimbMatched, 'order_id'),
      array_pluck($bniMatched, 'order_id'),
      array_pluck($permataMatched, 'order_id')
    );

    if (count($intersection)) {

      foreach ($intersection as $orderId) {

        $mandiriMatched = $this->_removeDuplicate($mandiriMatched, $orderId);
        $mandiriGiroMatched = $this->_removeDuplicate($mandiriGiroMatched, $orderId);
        $bcaMatched = $this->_removeDuplicate($bcaMatched, $orderId);
        $briMatched = $this->_removeDuplicate($briMatched, $orderId);
        $cimbMatched = $this->_removeDuplicate($cimbMatched, $orderId);
        $bniMatched = $this->_removeDuplicate($bniMatched, $orderId);
        $permataMatched = $this->_removeDuplicate($permataMatched, $orders);

      }
    }

    $result = [
      'unrecognized' => [
        'bca' => $bcaShouldManualCheck,
        'bri' => $briShouldManualCheck,
        'mandiri' => $mandiriShouldManualCheck,
        'mandiri_giro' => $mandiriGiroShouldManualCheck,
        'cimb' => $cimbShouldManualCheck,
        'bni' => $bniShouldManualCheck,
        'permata' => $permataShouldManualCheck
      ],
      'matched' => [
        'bca' => $bcaMatched,
        'bri' => $briMatched,
        'mandiri' => $mandiriMatched,
        'mandiri_giro' => $mandiriGiroMatched,
        'cimb' => $cimbMatched,
        'bni' => $bniMatched,
        'permata' => $permataMatched
      ],
    ];

    $this->_updateInquiries($result['matched'], 'matched');
    $this->_updateInquiries($result['unrecognized'], 'unrecognized');


    foreach ($result['matched'] as $bulks) {
      foreach ($bulks as $order) {
        dispatch(new VerifyOrder($order['order_id']));
      }
    }
  }

  private function _updateInquiries($data, $opt) {


    foreach ($data as $key => $inquiriesData) {

      if ($key == 'mandiri_giro') {
        $repo = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository::class);
      } else {
        $class = 'Odeo\\Domains\\Payment\\Odeo\\BankTransfer\\Scraper\\' . ucfirst($key) . '\\Repository\\Bank' . ucfirst($key) . 'InquiryRepository';
        $repo = app()->make($class);
      }

      foreach ($inquiriesData as $inquiryData) {

        $inquiry = $repo->findById($inquiryData['inquiry_id']);

        switch ($opt) {
          case 'matched':
            $inquiry->reference = json_encode(["" . $inquiryData['order_id']]);
            $inquiry->reference_type = 'order_id';
            break;
          case 'unrecognized':
            $inquiry->reference = "unrecognized";
            $inquiry->reference_type = 'note';
            break;
          default:
            continue;
        }

        $repo->save($inquiry);

      }
    }
  }


  public function _validate($bankTransferData, Collection $userOrders, $processCredit, $filter) {
    $shouldManualCheck = [];
    $matched = [];
    $orderIds = [];

    foreach ($bankTransferData as $trData) {

      $credit = $processCredit($trData);

      if (!strlen($credit) || $credit == "0") {
        $shouldManualCheck[] = [
          'inquiry_id' => $trData['id']
        ];
        clog('auto_verify_fail', $trData['id'] . ': a');
        continue;
      } else if ($credit[strlen($credit) - 1] == '0') {

        $shouldManualCheck[] = [
          'inquiry_id' => $trData['id']
        ];

        continue;
      }

      $filtered = $userOrders->filter(function ($item) use ($credit, $trData, $filter) {

        return $filter($item, $credit, $trData);

      })->all();

      if (!count($filtered)) {
        $shouldManualCheck[] = [
          'inquiry_id' => $trData['id']
        ];
        clog('auto_verify_fail', $trData['id'] . ': c');
        continue;
      }

      if (count($filtered) > 1) {
        $shouldManualCheck[] = [
          'inquiry_id' => $trData['id']
        ];
        clog('auto_verify_fail', $trData['id'] . ': d');
        continue;
      }

      foreach ($filtered as $f) {
        $filtered = $f;
        break;
      }


      if ($filtered['status'] == OrderStatus::CONFIRMED && $filtered['confirmation_amount'] != $credit) {
        $shouldManualCheck[] = [
          'inquiry_id' => $trData['id']
        ];
        continue;
      }
      if ($index = array_search($filtered['order_id'], $orderIds) !== false) {

        $shouldManualCheck[] = [
          'inquiry_id' => $matched[$index - 1]['inquiry_id']
        ];
        $shouldManualCheck[] = [
          'inquiry_id' => $trData['id']
        ];

        array_splice($matched, $index - 1, 1);
        array_splice($orderIds, $index - 1, 1);
        continue;
      }

      $orderIds[] = $filtered['order_id'];

      $matched[] = [
        'inquiry_id' => $trData['id'],
        'order_id' => $filtered['order_id']
      ];
    }

    return [
      $shouldManualCheck,
      $matched
    ];
  }
}

