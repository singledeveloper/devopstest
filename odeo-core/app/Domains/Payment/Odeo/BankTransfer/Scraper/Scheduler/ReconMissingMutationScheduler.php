<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 13/03/18
 * Time: 13.54
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\Payment;
use Odeo\Exceptions\FailException;

class ReconMissingMutationScheduler {

  private $sequenceNumber = 0;

  public function run() {
    foreach ([Payment::OPC_GROUP_BANK_TRANSFER_BCA] as $opc) {
      $this->sequenceNumber = 0;
      $this->begin($opc);
    }
  }

  function begin($opc) {
    $balance = 0;
    $from = Carbon::create(2016, 1, 1)->toDateString();
    $to = Carbon::yesterday()->toDateString();

    $bankRepo = Payment::getBankInquiryRepository($opc);

    $lastSequenceData = $bankRepo->getLastSequenceData();

    if ($lastSequenceData) {

      if ($unassignedIq = $bankRepo->hasUnassignSequenceNumberBeforeDate($lastSequenceData->date)) {
        $lastSequenceData = $bankRepo->findLastSequenceDataBeforeDate(Carbon::parse($unassignedIq->date)->toDateString());
      }

      $from = Carbon::parse($lastSequenceData->date)->toDateString();
      $this->sequenceNumber = $lastSequenceData->sequence_number;

      $balance = $lastSequenceData->balance;

    }

    $result = $bankRepo->getInquiries($from, $to);
    $dataToBeAssign = $this->recon($result, $balance);

    !empty($dataToBeAssign) &&
    $bankRepo->updateBulk($dataToBeAssign);
  }

  function increaseSequenceNumber() {
    $this->sequenceNumber++;
  }

  function assignData($id) {
    return [
      'id' => $id,
      'sequence_number' => $this->sequenceNumber
    ];
  }

  function recon($data, $balance) {
    $result = $this->groupByPrevBalance($data);
    $dataToBeAssign = [];

    for ($i = 0; $i < count($data); $i++) {
      $this->increaseSequenceNumber();
      $selectedInquiry = $this->getInquiryByBalance($result, $balance);

      if (!$selectedInquiry) {
        break;
      }

      if ($this->isDifferentSequence($selectedInquiry)) {
        $dataToBeAssign[] = $this->assignData($selectedInquiry->id);
      }

      $balance = $selectedInquiry->balance;
    }

    return $dataToBeAssign;
  }

  function sum($s, $balance) {
    return $balance + $s->credit - $s->debit;
  }

  private function getInquiryByBalance(&$groupedInquiries, $balance) {
    if (!isset($groupedInquiries[$balance])) {
      return null;
    }

    $inquiries = $groupedInquiries[$balance];
    $selectedInquiry = $inquiries->shift();

    if (empty($inquiries)) unset($groupedInquiries[$balance]);

    return $selectedInquiry;
  }

  private function groupByPrevBalance($inquiries) {
    $groupInquiries = $inquiries->groupBy(function($inquiry) {
      return number_format($inquiry['balance'] - $inquiry['credit'] + $inquiry['debit'], 2, '.', '');
    });

    return $groupInquiries->transform(function($inquiries) {
      return $inquiries->sortBy('date');
    });
  }

  private function isDifferentSequence($inquiry) {
    return !$inquiry->sequence_number || $inquiry->sequence_number != $this->sequenceNumber;
  }

}
