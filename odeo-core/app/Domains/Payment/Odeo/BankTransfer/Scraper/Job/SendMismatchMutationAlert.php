<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 12-Nov-18
 * Time: 1:53 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Job;


use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendMismatchMutationAlert extends Job {

  private $bankName, $lastScrapedDate;

  public function __construct($bankName, $lastScrapedDate) {
    parent::__construct();
    $this->bankName = $bankName;
    $this->lastScrapedDate = $lastScrapedDate;
  }

  public function handle() {
    $data = [
      'bank_name' => strtoupper($this->bankName),
      'last_scraped_date' => $this->lastScrapedDate
    ];

    Mail::send('emails.bank_mutation_mismatch_alert', ['data' => $data], function ($m) use ($data) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to([
        'pg@odeo.co.id'
      ])->subject('Mismatch Bank Mutation Alert ' . $data['bank_name']);
    });
  }

}
