<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Job;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendBankMutationScraperAlert extends Job  {

  use SerializesModels;

  private $accountNumberList;

  public function __construct($accountNumberList) {
    parent::__construct();
    $this->accountNumberList = $accountNumberList;
  }

  public function handle() {

    $response['account_number'] = $this->accountNumberList;

    Mail::send('emails.bank_mutation_scraper_alert', ['data' => $response], function ($m) use ($response) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to([
        'disbursement@odeo.co.id',
      ])->subject('Scrape Bank Mutation Alert - ' . date('Y-m-d'));
    });


  }

}
