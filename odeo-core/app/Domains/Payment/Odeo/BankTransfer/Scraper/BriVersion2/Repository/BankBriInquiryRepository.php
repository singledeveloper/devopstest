<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 29/08/18
 * Time: 14.53
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Repository;


use Illuminate\Support\Collection;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Model\BankBriInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankInquiryRepository;

class BankBriInquiryRepository extends BankInquiryRepository {

  public function __construct(BankBriInquiry $briInquiry) {
    $this->setModel($briInquiry);
  }

  /**
   * @param $startDate
   * @param $endDate
   * @return Collection
   */
  public function getListByAccountNumberIdBetweenDate($startDate, $endDate) {
    return $this->model
      ->whereDate('date', '>=', $startDate)
      ->whereDate('date', '<=', $endDate)
      ->orderBy('id', 'desc')
      ->get();
  }

  public function getDeletedListByAccountNumberIdBetweenDate($startDate, $endDate) {
    return $this->model
      ->onlyTrashed()
      ->whereDate('date', '>=', $startDate)
      ->whereDate('date', '<=', $endDate)
      ->orderBy('id', 'desc')
      ->get();
  }

  /**
   * @return Collection
   */
  function getInquiries($from, $to) {
    return $this->model->getmodel()
      ->orderBy('date', 'asc')
      ->orderBy('id', 'asc')
      ->whereDate('date', '>=', $from)
      ->whereDate('date', '<=', $to)
      ->get();
  }

  function getInquiryFromDate($date) {
    return $this->model
      ->whereDate('date', '>=', $date)
      ->get();
  }

  public function getLastRecordedBalance($accountNumberId) {
    return $this->model->whereNotNull('balance')->where('balance', '<>', 0)
      ->where('bank_scrape_account_number_id', $accountNumberId)
      ->orderBy('date', 'desc')->first();
  }

  function findFirstUnsignedSequenceInquiry() {
    return $this->model
      ->whereNull('sequence_number')
      ->orderBy('date', 'asc')
      ->first();
  }

  function findLatestSequenceBeforeDate($date) {
    return $this->model
      ->whereDate('date', '<', $date)
      ->whereNotNull('sequence_number')
      ->orderBy('sequence_number', 'desc')
      ->first();
  }

}
