<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Commands;

use Exception;
use Log;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper\ActivatorMarker;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper\SeleniumStarter;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\BriScraperManager;

class ScrapeBri extends Command {

  use ActivatorMarker;

  protected $signature = 'scrape:bank-bri';

  protected $description = 'run Bri Inquiry Scraper';

  private $seleniumStarter;

  public function __construct() {
    parent::__construct();
    
    $this->seleniumStarter = app()->make(SeleniumStarter::class);
  }

  public function handle() {
    while (true) {
      try {
        // $now = Carbon::now();

        // if ($now->hour >= 0 && $now->hour <= 5) {
        //  continue;
        // }

        $this->seleniumStarter->start(4445, 10, '/usr/local/bin/selenium-server-standalone.jar');

        app()->make(BriScraperManager::class)->run();
      }
      catch (Exception $exception) {
        clog('bri', $exception->getMessage() . ': ' . $exception->getTraceAsString());
      }

      sleep(5);
    }
  }
}
