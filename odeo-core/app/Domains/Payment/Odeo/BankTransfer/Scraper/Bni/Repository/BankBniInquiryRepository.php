<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bni\Repository;

use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bni\Model\BankBniInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankInquiryRepository;
use Odeo\Domains\Payment\Pax\Helper\ParseHelper;

class BankBniInquiryRepository extends BankInquiryRepository {

  public function __construct(BankBniInquiry $briInquiry) {
    $this->setModel($briInquiry);
  }

  function getInquiries($from, $to) {
    return $this->getCloneModel()
      ->orderBy('date', 'asc')
      ->orderBy('id', 'asc')
      ->whereDate('date', '>=', $from)
      ->whereDate('date', '<=', $to)
      ->get();
  }


  function getUnreconciledEdcSettlement() {
    return $this->getCloneModel()
      ->where('description', 'ilike', '%MICRO PAY NUSANTARA%')
      ->where('reference', 'unrecognized')
      ->where('credit', '>', 0)
      ->get();
  }

  function getBniSettlementReferenceIds() {
    return $this->getCloneModel()
      ->where('reference_type', ParseHelper::REFERENCE_BNI_EDC_SETTLEMENT)
      ->select(\DB::raw('reference::json->>0'))
      ->get();
  }

  function getBniEdcSettlementList() {
    return $this->getCloneModel()
      ->join('bni_edc_settlements', function($join) {
        $join->on('bni_edc_settlements.total_amount', '=', 'bank_bni_inquiries.credit')
          ->whereRaw("bni_edc_settlements.trx_date::DATE BETWEEN bank_bni_inquiries.date::DATE AND (bank_bni_inquiries.date + INTERVAL '2 DAYS')::DATE");
      })
      ->where('bank_bni_inquiries.description', 'like', '%ODEO*%')
      ->where(function($query) {
        $query->where('reference_type', 'note')
          ->orWhere('reference_type', '')
          ->orWhereNull('reference_type');
      })
      ->select('bank_bni_inquiries.id AS inquiry_id', 'bni_edc_settlements.id AS settlement_id')
      ->get();
  }
}
