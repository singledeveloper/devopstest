<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper;

class SeleniumStarter {
  
  public function start($port = 4444, $sleepSec = 10, $path = '/var/www/s4o9Vx') {
    $cmd = "java -jar $path -port $port";

    $pids = shell_exec("pgrep -x -f \"$cmd\""); 
    $pidList = array_filter(explode(PHP_EOL, $pids));

    foreach ($pidList as $pid) {
      echo "killTree($pid, 9)" . PHP_EOL;
      $this->killTree($pid, 9);
    }

    shell_exec("/usr/bin/nohup $cmd > /dev/null 2>&1 &");
    sleep($sleepSec);
  }

  private function killTree($pid, $signal = 15) {
    $pids = shell_exec("pgrep -P $pid"); 
    $pidList = array_filter(explode(PHP_EOL, $pids));

    foreach ($pidList as $childPid) {
      $this->killTree($childPid, $signal);
    }

    echo "kill -$signal $pid". PHP_EOL;
    shell_exec("kill -$signal $pid > /dev/null 2>&1");
  }

}
