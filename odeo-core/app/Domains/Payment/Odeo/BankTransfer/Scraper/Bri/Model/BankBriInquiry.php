<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Model;

use Odeo\Domains\Core\Entity;

class BankBriInquiry extends Entity
{

  protected $dates = ['date', 'created_at', 'updated_at'];

}
