<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/04/19
 * Time: 14.30
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class PermataAccountManager {

  public function login(Client $client, $account) {
    clog('recon_permata', 'trying to login');

    $res = $client->request('POST', 'https://www.permatae-business.com/corp/common/login.do', [
      'query' => ['action' => 'login'],
      'form_params' => [
        'corpId' =>  $account['corp_id'],
        'userName' => $account['username'],
        'language' => 'en_US',
        'password' =>  md5($account['password'])
      ],
    ]);

    $html = $res->getBody()->getContents();

    if ($res->getStatusCode() != 200) {
      clog('recon_permata', 'got response ' . $res->getStatusCode() . ' with body ' . $html);
    }

    preg_match('/errorMessage=(.*)/', $html, $matches);
    if (isset($matches[1])) {
      throw new FailException($matches[1]);
    }
    
    clog('recon_permata', 'fetching top');
    $client->request('GET', 'https://www.permatae-business.com/corp/common/login.do?action=topRequest');

    try {
      clog('recon_permata', 'fetching menu');
      $res = $client->request('GET', 'https://www.permatae-business.com/corp/common/login.do?action=menuRequest');
    } catch(RequestException $e) {
      clog('recon_permata', Psr7\str($e->getRequest()));
      if ($e->hasResponse()) {
        clog('recon_permata', Psr7\str($e->getResponse()));
      }
    }

    if ($res->getStatusCode() != 200) {
      clog('recon_permata', 'got response ' . $res->getStatusCode() . ' with body ' . $res->getBody()->getContents());
    }
  }

  public function logout(Client $client) {
    $client->request('GET', 'https://www.permatae-business.com/corp/common/login.do', [
      'query' => ['action' => 'logout'],
    ]);
  }

}