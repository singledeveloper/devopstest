<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/04/19
 * Time: 15.09
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository;


use Illuminate\Support\Collection;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Model\BankPermataInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankInquiryRepository;

class BankPermataInquiryRepository extends BankInquiryRepository {

  function __construct(BankPermataInquiry $permataInquiry) {
    $this->model = $permataInquiry;
  }

  /**
   * @param $from
   * @param $to
   * @return Collection
   */
  public function getInquiries($from, $to) {
    return $this->model
      ->whereDate('post_date', '>=', $from)
      ->whereDate('post_date', '<=', $to)
      ->orderBy('post_date')
      ->get();
  }

  function findFirstUnsignedSequenceInquiry() {
    return $this->model
      ->whereNull('sequence_number')
      ->orderBy('post_date', 'asc')
      ->first();
  }

  function getInquiryByDate($date) {
    return $this->model
      ->whereDate('post_date', '>=', $date)
      ->get();
  }

  function findLatestSequenceByDate($date) {
    return $this->model
      ->whereDate('post_date', '=', $date)
      ->orderBy('sequence_number', 'desc')
      ->first();
  }

  public function getUnreconciledVaPayment($vaCode) {
    return $this->model
      ->whereRaw("description LIKE 'PAY ODEO " . $vaCode . "%'")
      ->where('credit', '>', 0)
      ->where(function($subquery){
        $subquery->whereNull('reference_type')
          ->orWhere('reference_type', 'note');
      })
      ->orderBy('id', 'asc')
      ->first();
  }
}