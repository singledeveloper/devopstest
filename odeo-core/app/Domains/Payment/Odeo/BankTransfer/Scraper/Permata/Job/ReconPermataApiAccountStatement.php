<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/09/19
 * Time: 13.51
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Job;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BankAccount;
use Odeo\Domains\Notification\Helper\InternalNoticer;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Helper\PermataHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository\BankPermataInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeInformationRepository;
use Odeo\Domains\Vendor\Permata\Helper\ApiManager;
use Odeo\Jobs\Job;

class ReconPermataApiAccountStatement extends Job {

  const BANK_TARGET = 'permata';

  /**
   * @var Carbon
   */
  private $startDate, $endDate;
  private $bankInquiryRepo, $api, $bankScrapeInformationRepo, $bankScrapeAccoutNumberRepo, $redis, $information;

  function __construct($startDate, $endDate) {
    parent::__construct();
    $this->startDate = $startDate;
    $this->endDate = $endDate;
  }

  public function init() {
    $this->bankInquiryRepo = app()->make(BankPermataInquiryRepository::class);
    $this->api = app()->make(ApiManager::class);
    $this->bankScrapeInformationRepo = app()->make(BankScrapeInformationRepository::class);
    $this->bankScrapeAccoutNumberRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->redis = Redis::connection();

    $this->information = $this->bankScrapeInformationRepo->getActiveAccount(self::BANK_TARGET);
  }

  public function handle() {
    $this->init();

    // clog('recon_permata', getmypid() . ': starting process at ' . Carbon::now());
    $inquiries = $this->getInquiries();

    // clog('recon_permata', 'processing inquiries');
    $scrapeAccountNumber = $this->bankScrapeAccoutNumberRepo->findByName(self::BANK_TARGET);
    $existingInquiries = $this->bankInquiryRepo->getInquiries($this->startDate->format('Y-m-d'), $this->endDate->format('Y-m-d'));
    
    $groupFetchedInq = collect($inquiries)->keyBy(function($inquiry) {
      return $this->getInquiryKey($inquiry);
    });
    $groupExistingInq = $existingInquiries->keyBy(function($inquiry) {
      return $this->getInquiryKey($inquiry);
    });
    
    $updates = $newInquiries = [];
    foreach ($groupFetchedInq as $key => $scrapeInquiry) {
      if (!$groupExistingInq->has($key)) {
        $newInquiries[] = $scrapeInquiry;
      }
    }
    
    // clog('recon_permata', 'found ' . count($newInquiries) . ' new inquiries');

    if (count($newInquiries)) {
      $this->bankInquiryRepo->saveBulk($newInquiries);
    }

    $scrapeAccountNumber->last_scrapped_date = Carbon::now();
    $scrapeAccountNumber->last_time_scrapped = Carbon::now();
    $this->bankScrapeAccoutNumberRepo->save($scrapeAccountNumber);

    // clog('recon_permata', getmypid() . ': ending process at ' . Carbon::now());
    return 1;
  }

  private function getInquiries() {
    $hasMore = true;
    $sequenceNum = '';
    $inquiries = [];

    while ($hasMore) {
      $apiRes = $this->api->accountStatement($this->startDate->format('Y-m-d'), $this->endDate->format('Y-m-d'), $sequenceNum);
      $now = Carbon::now();
      $balance = $apiRes['AcctStmtRs']['StatementInfo']['BalanceAmount'];
      $hasMore = $apiRes['AcctStmtRs']['StatementInfo']['PageCtrl']['MoreFlag'] == 'Y';
      $sequenceNum = $apiRes['AcctStmtRs']['StatementInfo']['PageCtrl']['SequenceNum'];

      if (!isset($apiRes['AcctStmtRs']['StatementInfo']['DetailStatement']['AcctStmtDtl'])) {
        return collect($inquiries);
      }

      foreach ($apiRes['AcctStmtRs']['StatementInfo']['DetailStatement']['AcctStmtDtl'] as $inquiry) {

        $debit = $inquiry['TrxType'] == 'D' ? $inquiry['TrxAmount'] : 0;
        $credit = $inquiry['TrxType'] == 'C' ? $inquiry['TrxAmount'] : 0;

        $balance = 0;

        $inquiries[] = [
          'post_date' => Carbon::parse($inquiry['PostingDate']),
          'eff_date' => Carbon::parse($inquiry['EffectiveDate']),
          'trx_code' => '000',
          'cheque_no' => $inquiry['ChequeNo'],
          'reff_no' => $inquiry['PayRef'],
          'cust_reff_no' => $inquiry['CustRef'],
          'description' => $inquiry['TrxDesc'],
          'debit' => $this->formatNumber($debit),
          'credit' => $this->formatNumber($credit),
          'balance' => $this->formatNumber($balance),
          'bank_scrape_account_number_id' => BankAccount::PERMATA_ODEO,
          'scrape_sequence_no' => 0,
          'created_at' => $now,
          'updated_at' => $now
        ];
      }
    }
    return collect($inquiries);
  }

  private function getInquiryKey($inquiry) {
    return preg_replace('/\s*/', '',
      $inquiry['post_date']->format('Y-m-d') .
      $inquiry['eff_date']->format('Y-m-d') .
      $inquiry['cheque_no'] .
      $inquiry['reff_no'] .
      $inquiry['cust_reff_no'] .
      $inquiry['debit'] .
      $inquiry['credit'] .
      $inquiry['description']
    );
  }

  private function formatNumber($string) {
    return number_format($string, 2, '.', '');
  }

}