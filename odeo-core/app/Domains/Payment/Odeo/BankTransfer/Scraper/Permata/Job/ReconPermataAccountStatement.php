<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/04/19
 * Time: 15.13
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Job;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Notification\Helper\InternalNoticer;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Job\SendMismatchMutationAlert;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeInformation;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Helper\PermataHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\PermataAccountManager;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\PermataAccountStatementScraper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository\BankPermataInquiryBalanceSummaryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository\BankPermataInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeInformationRepository;
use Odeo\Jobs\Job;

class ReconPermataAccountStatement extends Job {

  const BANK_TARGET = 'permata';
  const MAX_FAILED = 3;

  /**
   * The number of seconds the job can run before timing out.
   *
   * @var int
   */
  public $timeout = 120;
  
  /**
   * @var BankPermataInquiryRepository
   */
  private $bankInquiryRepo;

  /**
   * @var BankPermataInquiryBalanceSummaryRepository
   */
  private $balanceSummaryRepo;

  /**
   * @var PermataAccountStatementScraper
   */
  private $scraper;

  /**
   * @var PermataAccountManager
   */
  private $accountManager;

  /**
   * @var BankScrapeAccountNumberRepository
   */
  private $bankScrapeAccoutNumberRepo;

  /**
   * @var BankScrapeInformationRepository
   */
  private $bankScrapeInformationRepo;

  /**
   * @var BankScrapeInformation
   */
  private $information;

  /**
   * @var Redis
   */
  private $redis;

  /**
   * @var Carbon
   */
  private $startDate, $endDate;
  private $shouldRecon;

  function __construct($startDate, $endDate, $shouldRecon = false) {
    parent::__construct();
    $this->startDate = $startDate;
    $this->endDate = $endDate;
    $this->shouldRecon = $shouldRecon;
  }

  private function init() {
    $this->bankInquiryRepo = app()->make(BankPermataInquiryRepository::class);
    $this->balanceSummaryRepo = app()->make(BankPermataInquiryBalanceSummaryRepository::class);
    $this->scraper = app()->make(PermataAccountStatementScraper::class);
    $this->accountManager = app()->make(PermataAccountManager::class);
    $this->bankScrapeInformationRepo = app()->make(BankScrapeInformationRepository::class);
    $this->bankScrapeAccoutNumberRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->redis = Redis::connection();

    $this->information = $this->bankScrapeInformationRepo->getActiveAccount(self::BANK_TARGET);
  }

  private function switchAccount() {
    $count = $this->redis->get('odeo_core:permata_scraper_active_account');
    $numAccount = count($this->information->accounts);
    $nextAccountIdx = ($count + 1) % $numAccount;

    $this->redis->set('odeo_core:permata_scraper_active_account', $nextAccountIdx);

    clog('recon_permata', 'switching permata account, account=' . $nextAccountIdx);
  }

  private function getActiveAccount() {
    return $this->information->accounts[$this->redis->get('odeo_core:permata_scraper_active_account')];
  }

  private function backoff()
  {
    $backoff = $this->redis->get('odeo_core:permata_scraper_failed_backoff');
    if (!$backoff) {
      $this->redis->set('odeo_core:permata_scraper_failed_backoff', 5);
      return 0;
    }

    $this->redis->set('odeo_core:permata_scraper_failed_backoff', $backoff - 1);
    return $backoff;
  }

  private function clearBackoff()
  {
    $this->redis->set('odeo_core:permata_scraper_failed_backoff', 0);
  }

  private function clearFailed()
  {
    $this->redis->set('odeo_core:permata_scraper_failed_count', 0);
  }

  private function incrementFailed()
  {
    $failed = $this->redis->get('odeo_core:permata_scraper_failed_count');
    $failed = min(self::MAX_FAILED, $failed + 1);

    if ($failed == self::MAX_FAILED) {
      $this->clearFailed();
      $this->clearBackoff();
    } else {
      $backoff = ($failed - 1) * 5;
      
      clog('recon_permata', 'starting back off permata, backoff=' . $backoff);
      $this->redis->set('odeo_core:permata_scraper_failed_count', $failed);
      $this->redis->set('odeo_core:permata_scraper_failed_backoff', $backoff);
    }
  }

  public function handle() {
    clog('recon_permata', getmypid() . ': starting process at ' . Carbon::now());
    $this->init();
    $this->handleInternalNotice();
    
    if ($backoff = $this->backoff()) {
      clog('recon_permata', 'backing off permata, backoff=' . $backoff);
      clog('recon_permata', getmypid() . ': ending process at ' . Carbon::now());
      return;
    }
    
    $this->switchAccount();
    $this->run();

    clog('recon_permata', getmypid() . ': ending process at ' . Carbon::now());
  }

  private function run() {
    if (!$this->redis->setnx(PermataHelper::PERMATA_REDIS_KEY, 1)) {
      clog('recon_permata', 'permata scraper still running');
      return;
    }
    $this->redis->expire(PermataHelper::PERMATA_REDIS_KEY, 5 * 60);

    $scrapeAccount = json_decode($this->getActiveAccount()->account, true);
    clog('recon_permata', json_encode($scrapeAccount));

    $client = new Client([
      'cookies' => true, 
      'timeout' => 20,
      'proxy' => 'http://lum-customer-brian_j-zone-sc-ip-74.91.60.121:61p6a9y8hzjb@zproxy.lum-superproxy.io:22225'
    ]); 

    try {
      $this->accountManager->login($client, $scrapeAccount);
      $scrapeAccountNumber = $this->bankScrapeAccoutNumberRepo->findByName(self::BANK_TARGET);

      clog('recon_permata', 'begin scraping');
      $scrapeInquiries = $this->scraper->scrapeAccountStatement($client, $this->startDate, $this->endDate, $scrapeAccountNumber->number)
        ->keyBy(function($inquiry) {
          return $this->getInquiryKey($inquiry);
        });
      $scrapeSummary =  $this->scraper->scrapeBalanceStatement($client, Carbon::now(), $scrapeAccountNumber->number);

      $existingInquiries = $this->bankInquiryRepo->getInquiries($this->startDate, $this->endDate)
        ->keyBy(function($inquiry) {
          return $this->getInquiryKey($inquiry);
        });

      clog('recon_permata', 'finish scraping, processing data');

      $netMovement = 0;
      $updates = $newInquiries = [];

      foreach ($scrapeInquiries as $key => $inquiry) {
        if ($existingInquiries->has($key)) {
          $currInquiry = $existingInquiries[$key];
          $hasChanges = $currInquiry['description'] != $inquiry['description'] ||
            $currInquiry['balance'] != $inquiry['balance'] ||
            $currInquiry['scrape_sequence_no'] != $inquiry['scrape_sequence_no'];

          if ($hasChanges) {
            $updates[] = [
              'id' => $currInquiry['id'],
              'description' => $inquiry['description'],
              'balance' => $inquiry['balance'],
              'scrape_sequence_no' => $inquiry['scrape_sequence_no'],
              'updated_at' => Carbon::now()
            ];
          }
        } else {
          $netMovement += $inquiry['credit'] - $inquiry['debit'];
          $newInquiries[] = $inquiry;
        }
      }

//      $removeIds = $existingInquiries->diffKeys($scrapeInquiries)->map(
//        function($inquiry) use (&$netMovement) {
//          $netMovement -= $inquiry['credit'] - $inquiry['debit'];
//          return $inquiry['id'];
//        }
//      );

      $latestSummary = $this->balanceSummaryRepo->findLatestSummary();

      $newBalanceSummary = $this->balanceSummaryRepo->getNew();
      $newBalanceSummary->balance_change = empty($latestSummary) ? $scrapeSummary['scrape_effective_balance'] : $netMovement;
      $newBalanceSummary->previous_balance = $latestSummary->balance ?? 0;
      $newBalanceSummary->balance = empty($latestSummary) ? $scrapeSummary['scrape_effective_balance'] : $latestSummary->balance + $netMovement;
      $newBalanceSummary->scrape_ledger_balance = $scrapeSummary['scrape_ledger_balance'];
      $newBalanceSummary->scrape_effective_balance = $scrapeSummary['scrape_effective_balance'];
      $newBalanceSummary->scrape_ineffective_balance = $scrapeSummary['scrape_ineffective_balance'];
      $newBalanceSummary->scrape_start_date = $this->startDate;
      $newBalanceSummary->scrape_end_date = $this->endDate;
      list($newBalanceSummary->status, $newBalanceSummary->first_distinct_id) = $this->getDiffStatus($latestSummary, $newBalanceSummary);

      if ($newBalanceSummary->status == PermataHelper::STATUS_OK && $this->balanceSummaryRepo->findDistinctSummary()) {
        $this->balanceSummaryRepo->updateDistinctRow();
      }

      if (count($newInquiries)) {
        $this->bankInquiryRepo->saveBulk($newInquiries);
      }

//      if (count($removeIds)) {
//        $this->bankInquiryRepo->deleteById($removeIds);
//      }

      if (count($updates)) {
        $this->bankInquiryRepo->updateBulk($updates);
      }

      $scrapeAccountNumber->last_scrapped_date = Carbon::now();
      $scrapeAccountNumber->last_time_scrapped = Carbon::now();
      $this->bankScrapeAccoutNumberRepo->save($scrapeAccountNumber);
      $this->balanceSummaryRepo->save($newBalanceSummary);

      clog('recon_permata', 'done processing data');

      $this->clearFailed();
      $this->resetErrorCount();
    } catch (\Exception $e) {
      $this->incrementFailed();
      clog('recon_permata', 'error msg: ' . $e->getMessage());
      clog('recon_permata', 'error trace: ' . $e->getTraceAsString());
    } finally {
      $this->redis->del(PermataHelper::PERMATA_REDIS_KEY);
      clog('recon_permata', 'logout permata');
      $this->accountManager->logout($client);
    }

    if ($this->shouldRecon && $distinctSummary = $this->balanceSummaryRepo->findDistinctSummary()) {
      if ($this->startDate->startOfDay() > Carbon::now()->subDay(28)) {
        //   dispatch(new self($this->startDate->subDay(2), $this->endDate->subDay(2), true));
      } else {
        dispatch(new SendMismatchMutationAlert(self::BANK_TARGET, $this->startDate));
      }
    }
  }

  private function getInquiryKey($inquiry) {
    return preg_replace('/\s*/m', '',
      $inquiry['post_date']->toDateString() .
      $inquiry['eff_date']->toDateString() .
      $inquiry['trx_code'] .
      $inquiry['reff_no'] .
      $inquiry['cust_reff_no'] .
      $inquiry['debit'] .
      $inquiry['credit']);
  }

  private function getDiffStatus($previousSummary, $newSummary) {
    if ($newSummary->balance != $newSummary->scrape_effective_balance) {
      if ($previousSummary->status != PermataHelper::STATUS_OK) {
        return [PermataHelper::STATUS_DIFF, $previousSummary->first_distinct_id ?? $previousSummary->id];
      }
      return [PermataHelper::STATUS_DIFF, null];
    }
    return [PermataHelper::STATUS_OK, null];
  }

  private function resetErrorCount($count = 0) {
    $this->redis->set(PermataHelper::REDIS_RECON_ERROR_COUNT, $count);
  }

  private function handleInternalNotice() {
    $count = $this->redis->get(PermataHelper::REDIS_RECON_ERROR_COUNT) ?? 0;
    $scrapeAccount = json_decode($this->getActiveAccount()->account, true);

    if ($count == 59) {
      $noticer = app()->make(InternalNoticer::class);
      $noticer->saveMessage('Permata Scrape Failed: ' . $scrapeAccount['username'] . ' failed to scrape in the last hour.');
      $this->resetErrorCount();
    } else {
      $count += 1;
      $this->resetErrorCount($count);
    }
  }
}