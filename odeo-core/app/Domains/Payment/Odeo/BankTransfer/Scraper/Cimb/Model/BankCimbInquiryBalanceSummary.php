<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 24/09/18
 * Time: 11.19
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Model;


use Illuminate\Database\Eloquent\Model;

class BankCimbInquiryBalanceSummary extends Model {

  protected $dates = ['scrape_start_date', 'scrape_end_date', 'created_at', 'updated_at', 'date'];

}