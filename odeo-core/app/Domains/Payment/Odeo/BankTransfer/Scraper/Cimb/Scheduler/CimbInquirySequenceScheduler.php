<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 03/10/18
 * Time: 13.35
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository\BankCimbInquiryBalanceSummaryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository\BankCimbInquiryRepository;

class CimbInquirySequenceScheduler {

  /**
   * @var BankCimbInquiryRepository
   */
  private $cimbInquiryRepo;

  /**
   * @var BankCimbInquiryBalanceSummaryRepository
   */
  private $cimbInquirySummary;

  private $currSequence, $currBalance;

  private $date;

  private function initiate() {
    $this->cimbInquirySummary = app()->make(BankCimbInquiryBalanceSummaryRepository::class);
    $this->cimbInquiryRepo = app()->make(BankCimbInquiryRepository::class);
    $this->currSequence = 0;
    $this->currBalance = number_format(0);
    $this->date = Carbon::parse('2017-04-11');
  }

  public function run() {
    $this->initiate();
    if (!$this->cimbInquirySummary->findDistinctRow()) {
      if ($unassignedIq = $this->cimbInquiryRepo->findFirstUnsignedSequenceInquiry()) {
        $prevSequence = $this->cimbInquiryRepo->findLatestSequenceByDate($unassignedIq->date->subday(1));
        $this->date = $unassignedIq->date;

        if ($prevSequence) {
          $this->currSequence = $prevSequence->sequence_number;
          $this->currBalance = number_format($prevSequence->balance);
        }
      }

      $inquiries = $this->cimbInquiryRepo->getInquiryByDate($this->date);
      $hasUnsequenced = $inquiries->containsStrict(function($inquiry){
        return $inquiry->sequence_number === null;
      });

      if ($hasUnsequenced) {
        $groupedInquiries = $this->groupInquiries($inquiries);
        $inquiriesCount = count($inquiries);
        $updates = [];

        for ($i = 0; $i < $inquiriesCount; $i++) {
          $selectedInquiry = $this->getInquiryByBalance($groupedInquiries, $this->currBalance);
          if (!$selectedInquiry) {
            break;
          }

          $this->currSequence++;
          $this->currBalance = number_format($selectedInquiry['balance']);

          if ($selectedInquiry['sequence_number'] == $this->currSequence) {
            continue;
          }

          $updates[] = [
            'id' => $selectedInquiry['id'],
            'sequence_number' => $this->currSequence,
          ];
        }

        if (count($updates)) {
          $this->cimbInquiryRepo->updateBulk($updates);
        }
      }
    }
  }

  private function groupInquiries($inquiries) {
    $groupInquiries = $inquiries->groupBy(function ($inquiry) {
      return number_format($inquiry['balance'] - $inquiry['credit'] + $inquiry['debit']);
    });

    return $groupInquiries->transform(function ($inquiries) {
      return $inquiries->sortBy('date');
    });
  }

  private function getInquiryByBalance(&$groupedInquiries, $balance) {
    if (!isset($groupedInquiries[$balance])) {
      return null;
    }

    $inquiries = $groupedInquiries[$balance];
    $selectedInquiry = $inquiries->shift();

    if (count($inquiries) == 0) {
      unset($groupedInquiries[$balance]);
    }
    return $selectedInquiry;
  }

}
