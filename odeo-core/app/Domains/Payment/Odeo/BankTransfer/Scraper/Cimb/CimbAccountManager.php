<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 19/09/18
 * Time: 17.30
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb;


use GuzzleHttp\Client;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Helper\CimbHelper;
use Symfony\Component\DomCrawler\Crawler;

class CimbAccountManager {

  public function login(Client $client, $account) {

    $client->get(CimbHelper::LOGIN_URL, [
      'query' => [
        'action' => 'loginRequest'
      ]
    ]);

    $res = $client->request('GET', CimbHelper::LOGIN_URL, [
      'query' => [
        'action' => 'loginRequest',
      ]
    ]);

    $crawler = new Crawler($res->getBody()->getContents(), CimbHelper::LOGIN_URL);
    $form = $crawler->filter('form')->form();

    $client->request('POST', CimbHelper::LOGIN_URL, [
      'query' => [
        'action' => 'login'
      ],
      'form_params' => [
        'corpId' => $account['corp_id'],
        'userName' => $account['username'],
        'password' => sha1($account['password'], false),
        'language' => 'en_US',
        'screenPhase' => 'bizchannel',
      ]
    ]);
  }

  public function logout(Client $client) {
    $response = $client->request('GET', CimbHelper::LOGIN_URL, [
      'query' => [
        'action' => 'logout'
      ]
    ]);

    return $response->getBody()->getContents();
  }

}
