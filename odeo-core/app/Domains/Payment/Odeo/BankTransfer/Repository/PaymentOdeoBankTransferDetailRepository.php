<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:58 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Model\PaymentOdeoBankTransferDetail;

class PaymentOdeoBankTransferDetailRepository extends Repository {


  public function __construct(PaymentOdeoBankTransferDetail $detail) {
    $this->model = $detail;
  }
}