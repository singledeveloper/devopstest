<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 11:51 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Repository;


use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Model\PaymentOdeoBankTransferCode;

class PaymentOdeoBankTransferCodeRepository extends Repository {

  public function __construct(PaymentOdeoBankTransferCode $bankTransferCode) {
    $this->model = $bankTransferCode;
  }

  public function getAnExpiredCode($amount, $bank) {
    return $this->model
      ->where('expired_at', '<', date('Y-m-d H:i:s'))
      ->where('bank', $bank)
      ->where('amount', $amount)->first();
  }

  public function isUsed($amount, $bank) {
    return $this->model
      ->where('expired_at', '>=', date('Y-m-d H:i:s'))
      ->where('bank', $bank)
      ->where('total', $amount)
      ->first();
  }
}