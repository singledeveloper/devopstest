<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Odeo\BankTransfer\Repository\PaymentOdeoBankTransferDetailRepository;

class PaymentConfirmationSelector {

  private $details;

  public function __construct() {
    $this->details = app()->make(PaymentOdeoBankTransferDetailRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function getDetail(PipelineListener $listener, $data, $referenceId) {

    $detail = $this->details->findById($referenceId);

    $result = [
      'confirmation_name' => $detail->account_name ?? '-',
      'confirmation_amount' => $this->currency->getFormattedOnly($detail->transfer_amount),
      'confirmation_date' => $detail->created_at->toDateTimeString()
    ];

    return $listener->response(200, $result);
  }
}