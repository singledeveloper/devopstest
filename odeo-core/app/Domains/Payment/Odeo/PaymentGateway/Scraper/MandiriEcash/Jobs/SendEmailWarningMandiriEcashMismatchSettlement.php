<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendEmailWarningMandiriEcashMismatchSettlement extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    Mail::send('emails.email_warning_mandiri_ecash_mismatch_settlement', $this->data, function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('vincent.wu.vt@gmail.com')->subject('Mandiri Ecash Mismatch Settlement');
    });

  }

}
