<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Repository;

use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Model\PaymentGatewayMandiriEcashMutation;

class PaymentGatewayMandiriEcashMutationRepository extends Repository {

  public function __construct(PaymentGatewayMandiriEcashMutation $bankMandiriEcashInquiry) {
    $this->model = $bankMandiriEcashInquiry;
  }

  function getAllFromDate($date) {
    return $this->model->where('transaction_date', '>=', $date)->get();
  }

  function getMappingWithSettlement() {
    return $this->model
      ->select(\DB::raw('
        mutation.id AS id, 
        settlement.id AS settlement_id,
        mutation.amount,
        settlement.amount as settlement_amount
      '))
      ->from(\DB::raw('
        payment_gateway_mandiri_ecash_mutations AS mutation
        JOIN
          payment_gateway_mandiri_ecash_settlements AS settlement
          ON settlement.transaction_date_time = ( 
            SELECT MIN(transaction_date_time)
            FROM payment_gateway_mandiri_ecash_settlements
            WHERE transaction_date_time >= mutation.transaction_date_time 
          )
      '))
      ->whereNull('mutation.settlement_id')
      ->orderBy('mutation.id')
      ->get();
  }

  function getReconciliationDataFromDate($startDate) {
    return $this->model->select(\DB::raw("
      orders.id as order_id, 
      orders.subtotal,
      orders.status, 
      order_charges.amount as charge_amount, 
      payment_gateway_mandiri_ecash_mutations.order_id as mandiri_order_id, 
      payment_gateway_mandiri_ecash_mutations.amount,
      payment_gateway_mandiri_ecash_mutations.merchant_discount_rate
    "))
      ->from(\DB::raw("
      orders
      join order_charges on orders.id = order_charges.order_id
      join payments on orders.id = payments.order_id and payments.info_id = " . Payment::OPC_GROUP_MANDIRI_E_CASH . "
      full outer join payment_gateway_mandiri_ecash_mutations on orders.id = payment_gateway_mandiri_ecash_mutations.order_id
    "))
      ->where(function ($query) use ($startDate) {
        $query->where('orders.created_at', '>=', $startDate)
          ->orWhere('orders.created_at', '=', null);
      })
      ->where(function ($query) use ($startDate) {
        $query->where('payment_gateway_mandiri_ecash_mutations.transaction_date', '>=', $startDate)
          ->orWhere('payment_gateway_mandiri_ecash_mutations.transaction_date', '=', null);
      })
      ->where(function ($query) {
        $query->where('order_charges.type', OrderCharge::PAYMENT_SERVICE_COST)
          ->orWhere('order_charges.type', '=', null);
      })
      ->orderBy('orders.id')
      ->get();
  }

  function getEcashRowDiffByDate($startDate) {
    return $this->model
      ->select('payment_gateway_mandiri_ecash_mutations.order_id', 'payment_gateway_mandiri_ecash_mutations.amount', 'payment_gateway_mandiri_ecash_mutations.merchant_discount_rate')
      ->from(\DB::raw("payment_gateway_mandiri_ecash_mutations
        left join orders on orders.id = payment_gateway_mandiri_ecash_mutations.order_id and orders.status = '" . OrderStatus::COMPLETED . "'
      "))
      ->where('payment_gateway_mandiri_ecash_mutations.transaction_date', '>=', $startDate)
      ->whereNull('orders.id')
      ->orderBy('payment_gateway_mandiri_ecash_mutations.order_id')
      ->get();
  }

  function getOrderRowDiffByDate($startDate) {
    return $this->model
      ->select('orders.id as order_id')
      ->from(\DB::raw("orders
        join payments on orders.id = payments.order_id and payments.info_id = " . Payment::OPC_GROUP_MANDIRI_E_CASH . "
        left join payment_gateway_mandiri_ecash_mutations on payment_gateway_mandiri_ecash_mutations.order_id = orders.id  
      "))
      ->where('orders.created_at', '>=', $startDate)
      ->where('orders.status', '=', OrderStatus::COMPLETED)
      ->whereNull('payment_gateway_mandiri_ecash_mutations.order_id')
      ->orderBy('orders.id')
      ->get();
  }

  function getMissingChargeOrdersByDate($startDate) {
    return $this->model
      ->select('payment_gateway_mandiri_ecash_mutations.order_id as order_id')
      ->from(\DB::raw("
        orders join payments on orders.id = payments.order_id and payments.info_id = " . Payment::OPC_GROUP_MANDIRI_E_CASH . "
        join order_charges on orders.id = order_charges.order_id and order_charges.type = '" . OrderCharge::PAYMENT_SERVICE_COST . "'
        full outer join payment_gateway_mandiri_ecash_mutations on orders.id = payment_gateway_mandiri_ecash_mutations.order_id
      "))
      ->whereNull('orders.id')
      ->where('payment_gateway_mandiri_ecash_mutations.transaction_date', '>=', $startDate)
      ->orderBy('payment_gateway_mandiri_ecash_mutations.order_id')
      ->get();
  }

  function getRowDifferencesByDate($startDate) {
    return $this->model
      ->select(\DB::raw("
        (SELECT count(payment_gateway_mandiri_ecash_mutations.id)
          FROM payment_gateway_mandiri_ecash_mutations
          WHERE transaction_date >= '" . $startDate . "') mutation_count,
        (SELECT count(orders.id)
          FROM orders
            JOIN payments ON orders.id = payments.order_id AND payments.info_id = " . Payment::OPC_GROUP_MANDIRI_E_CASH . "
          WHERE orders.status = '" . OrderStatus::COMPLETED . "'
         ) order_count
      "))->first();
  }

}
