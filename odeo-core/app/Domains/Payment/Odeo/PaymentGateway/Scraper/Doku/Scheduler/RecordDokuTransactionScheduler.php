<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Scheduler;

use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Jobs\ScrapeDoku;

class RecordDokuTransactionScheduler {

  public function run() {
    dispatch(new ScrapeDoku());
  }

}
