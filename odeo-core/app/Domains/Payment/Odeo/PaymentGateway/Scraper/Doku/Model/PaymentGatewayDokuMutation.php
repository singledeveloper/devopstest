<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Model;

use Odeo\Domains\Core\Entity;

class PaymentGatewayDokuMutation extends Entity {
  protected $guarded = ['id'];
}
