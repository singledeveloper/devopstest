<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/15/17
 * Time: 2:35 PM
 */

namespace Odeo\Domains\Payment\Bni\Helper;


class BniApi {

  const URL_DEVELOPMENT = 'https://apibeta.bni-ecollection.com/';
  const URL_PRODUCTION = 'https://api.bni-ecollection.com/';
  const SECRET_KEY_DEV = '359c2ce0ffc75db3cc972b6efec1b05f';
  const CLIENT_ID_DEV = "297";
  const BILL_TYPE_CREATE = 'createbilling';
  const BILL_TYPE_INQUIRY = 'inquirybilling';
  const BILL_TYPE_UPDATE = 'updatebilling';

  const PAYMENT_TYPE_OPEN = 'o';
  const PAYMENT_TYPE_CLOSE = 'c';
  const PAYMENT_TYPE_INSTALLMENT = 'i';

  public static function getUrl() {
    return app()->environment('production') ? self::URL_PRODUCTION : self::URL_DEVELOPMENT;
  }

  public static function getCLientId() {
     if (app()->environment('production')){
       return env('BNI_ECOLLECTION_ID_PROD');
     }
     return self::CLIENT_ID_DEV;
  }

  public static function getSecret() {
    if (app()->environment() == 'production') {
      return env('BNI_ECOLLECTION_SECRET_KEY_PROD');
    }
    return self::SECRET_KEY_DEV;

  }

}