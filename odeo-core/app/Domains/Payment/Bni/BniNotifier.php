<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/15/17
 * Time: 2:35 PM
 */

namespace Odeo\Domains\Payment\Bni;


use Odeo\Domains\Constant\BillerQluster;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Payment\Bni\Helper\BniApi;
use Odeo\Domains\Payment\Bni\Helper\BniLibrary;
use Odeo\Domains\Payment\Bni\Helper\BniManager;
use Odeo\Domains\Payment\PaymentRequester;

class BniNotifier extends BniManager {

  public function __construct() {
    parent::__construct();
  }

  public function notify(PipelineListener $listener, $data) {
    $data = BniLibrary::parseData($data['data'], BniApi::getCLientId(), BniApi::getSecret());

    if (!($payment = $this->bniPayments->findById($data['trx_id']))) {
      return $listener->response(400);
    }

    $va = $this->virtualAccounts->findByNumber($data['virtual_account']);

    switch ($va->biller) {
      case VirtualAccount::BILLER_QLUSTER:
        $prefix = 'Qluster';
        $userId = BillerQluster::ODEO_USER_ID;
        break;
      default:
        $prefix = '';
        $userId = $va->user_id;
    }

    $vaData = [
      'trx_id' => $data['trx_id'],
      'opc' => $va->opc,
      'info_id' => Payment::OPC_GROUP_VA_BNI,
      'fee' => $va->fee,
      'virtual_account_id' => $va->user_virtual_account_id,
      'platform_id' => Platform::VIRTUAL_ACCOUNT,
      'gateway_id' => Payment::VIRTUAL_ACCOUNT,
      'store_id' => $va->store_id,
      'service_detail_id' => $va->service_detail_id,
      'biller' => $va->biller,
      'item_id' => $va->service_reference_id,
      'auth' => [
        'user_id' => $userId
      ],
      'first_name' => ($prefix != '' ? $prefix . ' - ' : '') . $va->display_name
    ];

    $payment->log_notify = json_encode($data);
    $payment->payment_ntb = $data['payment_ntb'];
    $this->bniPayments->save($payment);

    $listener->addNext(new Task(CartInserter::class, 'addToCart'));
    $listener->addNext(new Task(CartCheckouter::class, 'checkout'));
    $listener->addNext(new Task(BniConfirmer::class, 'confirm'));

    return $listener->response(200, $vaData);

  }
}