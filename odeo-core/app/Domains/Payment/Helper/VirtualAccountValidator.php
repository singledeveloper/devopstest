<?php
namespace Odeo\Domains\Payment\Helper;

use Odeo\Domains\Constant\VirtualAccount;

class VirtualAccountValidator {

  public function isDoku($vendorCode) {
    return (
      $vendorCode == VirtualAccount::VENDOR_DOKU_BCA ||
      $vendorCode == VirtualAccount::VENDOR_DOKU_MANDIRI ||
      $vendorCode == VirtualAccount::VENDOR_DOKU_INDOMARET ||
      $vendorCode == VirtualAccount::VENDOR_DOKU_BRI
    );
  }

  public function isPrismalink($vendorCode) {
    return $vendorCode == VirtualAccount::VENDOR_PRISMALINK_BCA;
  }

  public function isCimb($vendorCode) {
    return $vendorCode == VirtualAccount::VENDOR_DIRECT_CIMB;
  }

  public function isPermata($vendorCode) {
    return $vendorCode == VirtualAccount::VENDOR_DIRECT_PERMATA;
  }

  public function isArtajasaVa($vendorCode) {
    return $vendorCode == VirtualAccount::VENDOR_DIRECT_ARTAJASA;
  }

  public function isCimbVa($vendorCode) {
    return $vendorCode == VirtualAccount::VENDOR_DIRECT_CIMB;
  }

  public function isDokuVa($vendorCode) {
    return $vendorCode == VirtualAccount::VENDOR_DOKU_ALFA;
  }
}
