<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/21/17
 * Time: 8:42 PM
 */

namespace Odeo\Domains\Payment\Akulaku\Repository;


use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Akulaku\Model\PaymentAkulakuPayment;

class PaymentAkulakuPaymentRepository extends Repository {

  public function __construct(PaymentAkulakuPayment $paymentAkulakuPayment) {
    $this->model = $paymentAkulakuPayment;
  }

  public function findByRefNo($refNo) {
    return $this->model->where('ref_no', $refNo)->first();
  }

  public function getUnverifiedPayments() {
    return $this->model->where('is_verified', false)->get();
  }

  public function getUnconfirmedPayments() {

    return $this->model
      ->join('orders', 'orders.id', '=', 'ref_no')
      ->where('is_confirmed', false)
      ->where('is_verified', true)
      ->where('orders.status', OrderStatus::COMPLETED)
      ->select(
        'payment_akulaku_payments.*'
      )
      ->get();
  }

}