<?php

namespace Odeo\Domains\Payment\Akulaku\Scheduler;

use GuzzleHttp\Client;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuApi;
use Odeo\Domains\Payment\Akulaku\Repository\PaymentAkulakuPaymentVoidRepository;

class PaymentVoidScheduler {

  public function run() {

    $akulakuPaymentVoidRepo = app()->make(PaymentAkulakuPaymentVoidRepository::class);

    $unprocessedVoidOrder = $akulakuPaymentVoidRepo->findUnprocessedOrder();

    foreach ($unprocessedVoidOrder as $order) {

      try {

        $payload = $this->getPayload($order->order_id);
        $order->log_request = json_encode($payload);

        $result = $this->processVoid($payload);

        if ($result && !empty($result)) {
          $order->log_response = json_encode($result);
          $order->void_status = true;
        }

      } catch (\Exception $e) {
        clog('akulaku_error', 'REFUND');
        clog('akulaku_error', $e->getMessage());
        $order->log_response = $e->getMessage();
      }

      $akulakuPaymentVoidRepo->save($order);
    }

  }

  private function processVoid($payload) {

    $client = new Client();

    $response = $client->post(AkulakuApi::$CANCEL_ORDER_URL, [
      'form_params' => $payload,
      'timeout' => 100,
      'verify' => false
    ]);

    $result = json_decode($response->getBody(), true);

    return $result;

  }


  private function getPayload($orderId) {
    return [
      'appId' => AkulakuApi::$APP_ID,
      'refNo' => $orderId,
      'sign' => AkulakuApi::generateSign($orderId)
    ];
  }

}
