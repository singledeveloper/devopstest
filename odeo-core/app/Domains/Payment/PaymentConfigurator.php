<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 10/23/16
 * Time: 12:22 AM
 */

namespace Odeo\Domains\Payment;


use Odeo\Domains\Core\PipelineListener;

class PaymentConfigurator {

  private $paymentChannels;

  public function __construct() {
    $this->paymentChannels = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);
  }

  public function togglePaymentChannel(PipelineListener $listener, $data) {

    $channel = $this->paymentChannels->findById($data['id']);

    $channel->active = !$channel->active;

    $this->paymentChannels->save($channel);

    return $listener->response(200, [
      'channel' => $channel,
    ]);

  }
}