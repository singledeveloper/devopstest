<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 08/08/19
 * Time: 18.37
 */

namespace Odeo\Domains\Payment\Artajasa\Va\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\Payment;

class PaymentArtajasaVaPayment extends Entity {

  public function payment() {
    return $this->belongsTo(Payment::class);
  }

  public function inquiry() {
    return $this->belongsTo(PaymentArtajasaVaInquiry::class, 'inquiry_id');
  }

}