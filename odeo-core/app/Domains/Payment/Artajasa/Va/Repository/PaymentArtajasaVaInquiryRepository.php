<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 08/08/19
 * Time: 18.36
 */

namespace Odeo\Domains\Payment\Artajasa\Va\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Artajasa\Va\Model\PaymentArtajasaVaInquiry;

class PaymentArtajasaVaInquiryRepository extends Repository {

  function __construct(PaymentArtajasaVaInquiry $inquiry) {
    $this->model = $inquiry;
  }

  function get() {
    $filters = $this->getFilters();
    $q = $this->model;

    if (isset($filters['search'])) {
      $search = $filters['search'];

      if (isset($search['id'])) {
        $q = $q->where('id', $search['id']);
      }
      if (isset($search['va_id'])) {
        $q = $q->where('va_id', $search['va_id']);
      }
      if (isset($search['customer_name'])) {
        $q = $q->where('customer_name', $search['customer_name']);
      }
      if (isset($search['display_text'])) {
        $q = $q->where('display_text', $search['display_text']);
      }
      if (isset($search['booking_id'])) {
        $q = $q->where('booking_id', $search['booking_id']);
      }
    }

    return $this->getResult($q->orderBy('id', 'DESC'));
  }

  function findByBookingId($bookingId) {
    return $this->model->where('booking_id', $bookingId)
      ->first();
  }

}