<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:33 PM
 */

namespace Odeo\Domains\Payment\Model;


use Odeo\Domains\Core\Entity;

class PaymentOdeoPaymentChannel extends Entity {

  protected $primaryKey = 'code';
  
  public function information() {
    return $this->belongsTo(PaymentOdeoPaymentChannelInformation::class, 'info_id');
  }



}