<?php

namespace Odeo\Domains\Payment\Jobs;


use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\BillerPundi;
use Odeo\Domains\Constant\AssetAws;
use Odeo\Jobs\Job;

class SendPaymentPendingBankTransfer extends Job  {

  use SerializesModels, EmailHelper;

  private $order, $data;
  private $paymentValidator, $orderDetailizer, $currencyHelper, $paymentChannels;

  public function __construct($order, $data) {
    parent::__construct();
    $this->order = $order;
    $this->data = $data;

  }

  public function handle() {
    
    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->paymentValidator = app()->make(\Odeo\Domains\Payment\Helper\PaymentValidator::class);
    
    if ($this->order->actual_store_id == BillerPundi::ODEO_STORE_ID) return;

    $this->paymentInformations = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository::class);
    $this->paymentChannels = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);

    $response = [];

    if ($response['email'] = $this->getEmail($this->order, $this->data)) ;
    else return;

    $paymentData = $this->paymentChannels->findById($this->data['opc']);
    
    $response['date_opened'] = $this->order->created_at;
    $response['date_expired'] = $this->order->expired_at;
    $response['order_id'] = $this->order->id;
    $response['platform_id'] = $this->order->platform_id;
    $response['cart_data']['items'] = $this->orderDetailizer->detailizeItem($this->order, [
      'mask_phone_number'
    ]);

    $response['cart_data']['charges'] = $this->orderDetailizer->detailizeCharge($this->order->id);
    $response['cart_data']['total'] = $this->currencyHelper->formatPrice($this->order->total);
    $response['cart_data']['subtotal'] = $this->currencyHelper->formatPrice($this->order->subtotal);
    $response['payment_data'] = $paymentData->information->toArray(); 
    $response['payment_data']['detail'] = json_decode($response['payment_data']['detail'], true);

    $response = array_merge($response, $this->orderDetailizer->detailizeReceipt($response));
    $response = array_merge($response, $this->getStoreData($this->order->actual_store_id));

    $response['email_info'] = $this->getEmailInfo($this->order->actual_store_id);

    $paymentInformation = $this->paymentInformations->findById($paymentData->info_id);

    $response['store_logo_path'] = AssetAws::getAssetPath(AssetAws::BUSINESS_LOGO, AssetAws::DEFAULT_STORE_LOGO);
    $response['payment_logo_path'] = AssetAws::getAssetPath(AssetAws::PAYMENT, $paymentInformation->logo);

    Mail::send('emails.payment_pending_bank_transfer', ['data' => $response], function ($m) use ($response) {

      $m->from($response['email_info']['sender_email'], $response['email_info']['sender_name']);

      if ($response['email_info']['sender_name'] == 'Pundi-Pundi') {
        $m->bcc($response['seller_store_email']);
        $m->bcc('sales.report@odeo.co.id');
      }

      $m->to($response['email'])->subject('[' . strtoupper(ucfirst($response['email_info']['sender_name'])) . '] Payment Guide Order - ' . $response['order_id']);
    });
  }

}
