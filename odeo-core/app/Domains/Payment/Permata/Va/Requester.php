<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/10/19
 * Time: 14.58
 */

namespace Odeo\Domains\Payment\Permata\Va;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\PipelineListener;

class Requester {

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderChargeHelper = app()->make(\Odeo\Domains\Order\Helper\OrderCharger::class);
    $this->virtualAccounts = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountRepository::class);
  }

  public function request(PipelineListener $listener, $order, $paymentInformation, $data) {
    $virtualAccount = $this->virtualAccounts->findById($data['virtual_account_id']);

    $virtualAccount->order_id = $order->id;
    $this->virtualAccounts->save($virtualAccount);

    if ($order->status == OrderStatus::CREATED) {
      if (in_array($virtualAccount->biller, [VirtualAccount::BILLER_ASG_GLC, VirtualAccount::BILLER_ASG_GLC_FM, VirtualAccount::BILLER_DD])) {
        $this->orderChargeHelper->charge($order->id, OrderCharge::BILLER_FEE, $data['fee'], OrderCharge::GROUP_TYPE_COMPANY_PROFIT);
      } else {
        $this->orderChargeHelper->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $data['fee'], OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

        $order->total = $order->total + $data['fee'];
      }

      $order->status = OrderStatus::OPENED;
      $order->opened_at = date('Y-m-d H:i:s');

      $this->orders->save($order);
    }

    return $listener->response(200, [
      'customer_name' => $order->name,
      'total' => $order->total
    ]);
  }

}