<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 21/08/19
 * Time: 16.53
 */

namespace Odeo\Domains\Payment\Permata\Va;


use Odeo\Domains\Constant\PermataVaPaymentStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaPaymentRepository;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaPaymentReversalRepository;

class Reverser {

  function __construct() {
    $this->permataVaPayment = app()->make(PaymentPermataVaPaymentRepository::class);
    $this->permataVaReversal = app()->make(PaymentPermataVaPaymentReversalRepository::class);
  }

  public function vaReverse(PipelineListener $listener, $data) {
    if (isProduction()) {
      return $listener->response(400, ['message' => 'Forbidden', 'code' => '01']);
    }

    if (!isset($data['RevBillRq'])) {
      return $listener->response(400, ['message' => 'Unknown data format', 'code' => '01']);
    }

    $data = $data['RevBillRq'];
    $vaPayment = $this->permataVaPayment->findPaymentByVaNumberAndTraceNo($data['VI_VANUMBER'], $data['VI_TRACENO']);
    if (!$vaPayment) {
      return $listener->response(400, ['message' => 'Unknown data format', 'code' => '01']);
    }

    if ($vaPayment->bill_amount != $data['BILL_AMOUNT']) {
      return $listener->response(400, ['message' => 'Invalid amount', 'code' => '13']);
    }

    if (in_array($vaPayment->status, [PermataVaPaymentStatus::FAILED_NEED_UPDATE, PermataVaPaymentStatus::FAILED])) {
      return $listener->response(400, ['message' => 'Invalid Payment Status', 'code' => '01']);
    }

    $vaPayment->status = PermataVaPaymentStatus::FAILED_NEED_UPDATE;
    $this->permataVaPayment->save($vaPayment);

    $reversal = $this->permataVaReversal->getNew();
    $reversal->instcode = $data['INSTCODE'];
    $reversal->va_number = $data['VI_VANUMBER'];
    $reversal->trace_no = $data['VI_TRACENO'];
    $reversal->trn_date = $data['VI_TRNDATE'];
    $reversal->bill_amount = $data['BILL_AMOUNT'];
    $reversal->del_channel = $data['VI_DELCHANNEL'];
    $reversal->ref_info = json_encode($data['RefInfo']);
    $reversal->permata_payment_id = $vaPayment->id;
    $this->permataVaReversal->save($reversal);

    return $listener->response(200);
  }

}