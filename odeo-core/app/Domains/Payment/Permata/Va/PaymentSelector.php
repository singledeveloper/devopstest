<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/10/19
 * Time: 17.32
 */

namespace Odeo\Domains\Payment\Permata\Va;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaPaymentRepository;

class PaymentSelector {

  function __construct() {
    $this->permataVaPayment = app()->make(PaymentPermataVaPaymentRepository::class);
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->permataVaPayment->normalizeFilters($data);
    $this->permataVaPayment->setSimplePaginate(true);

    $result = [];
    $payments = $this->permataVaPayment->get();

    foreach ($payments as $inquiry) {
      $result[] = $inquiry;
    }

    if (count($result) > 0) {
      return $listener->response(200, array_merge(
        ['payments' => $result],
        $this->permataVaPayment->getPagination()
      ));
    }

    return $listener->response(200, [
      'payments' => []
    ]);

  }

}