<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/10/19
 * Time: 17.32
 */

namespace Odeo\Domains\Payment\Permata\Va;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaInquiryRepository;

class InquirySelector {

  function __construct() {
    $this->permataVaInquiry = app()->make(PaymentPermataVaInquiryRepository::class);
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->permataVaInquiry->normalizeFilters($data);
    $this->permataVaInquiry->setSimplePaginate(true);

    $result = [];
    $inquiries = $this->permataVaInquiry->get();

    foreach ($inquiries as $inquiry) {
      $result[] = $inquiry;
    }

    if (count($result) > 0) {
      return $listener->response(200, array_merge(
        ['inquiries' => $result],
        $this->permataVaInquiry->getPagination()
      ));
    }

    return $listener->response(200, [
      'inquiries' => []
    ]);

  }

}