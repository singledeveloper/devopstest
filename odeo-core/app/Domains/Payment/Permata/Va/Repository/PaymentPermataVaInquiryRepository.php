<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 20/08/19
 * Time: 13.17
 */

namespace Odeo\Domains\Payment\Permata\Va\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Permata\Va\Model\PaymentPermataVaInquiry;

class PaymentPermataVaInquiryRepository extends Repository {

  function __construct(PaymentPermataVaInquiry $vaInquiry) {
    $this->model = $vaInquiry;
  }

  function get() {
    $filters = $this->getFilters();
    $q = $this->model;

    if (isset($filters['search'])) {
      $search = $filters['search'];
      if (isset($search['id'])) {
        $q = $q->where('id', $search['id']);
      }

      if(isset($search['va_number'])) {
        $q = $q->where('va_number', $search['va_number']);
      }
      if (isset($search['reference_number'])) {
        $q = $q->where('reference_number', $search['reference_number']);
      }
    }

    return $this->getResult($q->orderBy('id', 'DESC'));
  }

  function findByVaCode($vaCode) {
    return $this->model
      ->where('va_number', $vaCode)
      ->whereDate('created_at', date('Y-m-d'))
      ->orderBy('id', 'DESC')
      ->first();
  }

  function findDuplicateInquiry($vaCode) {
    return $this->model
      ->where('va_number', $vaCode)
      ->whereDate('created_at', date('Y-m-d'))
      ->first();
  }

  function findDuplicateInquiryWithTrace($vaCode, $traceNo) {
    return $this->model
      ->where('va_number', $vaCode)
      ->where('trace_no', $traceNo)
      ->whereDate('created_at', date('Y-m-d'))
      ->first();
  }

}