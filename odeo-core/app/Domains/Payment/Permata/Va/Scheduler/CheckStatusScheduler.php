<?php

namespace Odeo\Domains\Payment\Permata\Va\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaPaymentRepository;
use Odeo\Domains\Payment\Permata\Va\Jobs\InquiryStatus;

class CheckStatusScheduler {

  private $permataVaPayment;

  public function __construct() {
    $this->permataVaPayment = app()->make(PaymentPermataVaPaymentRepository::class);
  }

  public function run() {
    $pendingPayments = $this->permataVaPayment->getPendingPayment();
    foreach($pendingPayments as $pendingPayment) {
      dispatch(new InquiryStatus($pendingPayment));
    }
  }

}
