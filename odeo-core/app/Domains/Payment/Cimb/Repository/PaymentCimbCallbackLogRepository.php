<?php

namespace Odeo\Domains\Payment\Cimb\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Cimb\Model\PaymentCimbCallbackLog;

class PaymentCimbCallbackLogRepository extends Repository {

  function __construct(PaymentCimbCallbackLog $cimbCallbackLog) {
    $this->model = $cimbCallbackLog;
  }

}