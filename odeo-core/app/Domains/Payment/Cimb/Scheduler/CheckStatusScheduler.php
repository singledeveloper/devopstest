<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/10/19
 * Time: 15.09
 */

namespace Odeo\Domains\Payment\Cimb\Scheduler;


use Odeo\Domains\Payment\Cimb\Jobs\InquiryStatus;
use Odeo\Domains\Payment\Cimb\Repository\PaymentCimbVaPaymentRepository;

class CheckStatusScheduler {

  public function __construct() {
    $this->cimbVaPayment = app()->make(PaymentCimbVaPaymentRepository::class);
  }

  public function run() {
    $pendingPayments = $this->cimbVaPayment->getPendingPayment();
    foreach($pendingPayments as $pendingPayment) {
      dispatch(new InquiryStatus($pendingPayment));
    }
  }

}