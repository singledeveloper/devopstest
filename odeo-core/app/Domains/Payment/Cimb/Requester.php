<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/24/17
 * Time: 2:02 PM
 */

namespace Odeo\Domains\Payment\Cimb;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Cimb\Helper\CimbManager;

class Requester extends CimbManager {


  public function __construct() {
    parent::__construct();
  }

  public function request(PipelineListener $listener, $order, $paymentInformation, $data) {

    if ($order->status == OrderStatus::CREATED) {

      $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $data['fee'], OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

      $order->total = $order->total + $data['fee'];
      $order->status = OrderStatus::OPENED;
      $order->opened_at = date('Y-m-d H:i:s');

      $this->orders->save($order);
    }

    $virtualAccount = $this->virtualAccounts->findById($data['virtual_account_id']);
    $virtualAccount->order_id = $order->id;

    $this->virtualAccounts->save($virtualAccount);

    return $listener->response(200, [
      'customer_name' => $order->name,
      'total' => $order->total
    ]);
  }

}