<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/17/17
 * Time: 3:11 PM
 */

namespace Odeo\Domains\Payment\Cimb\Helper;


class CimbHelper {

  const SERVICE_INQUIRY = 'Inquiry';
  const SERVICE_PAYMENT = 'Payment';
  const SERVICE_ECHO = 'Echo';

  const TRX_SUCCESS = '00';
  const INQUIRY_CODE_COMPANY_CODE_EMPTY = '10';
  const INQUIRY_CODE_CHANNEL_TYPE_EMPTY = '13';
  const INQUIRY_CODE_TRANS_DATE_EMPTY = '14';
  const INQUIRY_CODE_CUST_NOT_FOUND = '16';
  const INQUIRY_CODE_BILL_NOT_FOUND = '40';
  const INQUIRY_CODE_BILL_PAID = '41';
  const PAYMENT_CODE_COMPANY_CODE_EMPTY = '21';
  const PAYMENT_CODE_CUST_KEY_EMPTY = '22';
  const PAYMENT_CODE_TRANS_ID_EMPTY = '23';
  const PAYMENT_CODE_CHANNEL_ID_EMPTY = '24';
  const PAYMENT_CODE_TRANS_DATE_EMPTY = '25';
  const PAYMENT_CODE_AMOUNT_EMPTY = '27';
  const PAYMENT_CODE_REFF_EMPTY = '28';
  const PAYMENT_CODE_BILL_NOT_FOUND = '32';
  const PAYMENT_CODE_BILL_PAID = '33';
  const PAYMENT_CODE_INVALID_AMOUNT = '38';

  const FLAG_OPEN_PAYMENT = 0;
  const FLAG_CLOSED_PAYMENT = 1;

  const COST = 0;

  const RESPONSE_CODE_MAPPINGS = [
    '00' => 'Transaction Success',
    '10' => 'CompanyCode Empty',
    '11' => 'CustomerNumber Empty',
    '12' => 'RequestID Empty',
    '13' => 'ChannelType Empty',
    '14' => 'TransactionDate Empty',
    '15' => 'TransactionDate format not match',
    '16' => 'Customer Not Found',
    '17' => 'Provider Problem',
    '18' => 'Timeout',
    '19' => 'Connection Failure',
    '21' => 'Company Code Empty',
    '22' => 'Customer Key Empty',
    '23' => 'Transaction ID Empty',
    '24' => 'Channel ID Empty',
    '25' => 'Transaction Date Empty',
    '26' => 'Transaction Date format not match',
    '27' => 'Amount Empty',
    '28' => 'Reference Empty',
    '29' => 'Provider Problem',
    '30' => 'Timeout',
    '31' => 'Connection Failure',
    '32' => 'Bill Not Found',
    '33' => 'Bill Already Paid',
    '34' => 'Bill Suspended',
    '35' => 'Bill has Flagged, Reverse Not Possible',
    '36' => 'Transaction Accepted, Pending Processing',
    '37' => 'Bill Expired',
    '38' => 'Invalid Amount',
    '40' => 'Bill Not Found',
    '41' => 'Bill Already Paid',
    '42' => 'Bill Suspended',
    '43' => 'Bill has Flagged, Reverse Not Possible',
    '44' => 'Transaction Accepted, Pending Processing',
    '45' => 'Bill Expired',
    '46' => 'Invalid Amount',
    '99' => 'General Failure'
  ];

  public static function getResponseMessage($code) {
    return self::RESPONSE_CODE_MAPPINGS[$code] ?? self::RESPONSE_CODE_MAPPINGS['99'];
  }

  public static function generateResponseXml($data, $service) {

    $xml = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' . PHP_EOL .
      '<soap:Body>' . PHP_EOL .
      '<CIMB3rdParty_' . $service . 'Rs xmlns="http://CIMB3rdParty/BillPaymentWS">' . PHP_EOL .
      '<' . $service . 'Rs>' . PHP_EOL;

    foreach ($data as $key => $val) {
      if($val === '') $xml .= "<$key/>";
      else $xml .= "<$key>$val</$key>" . PHP_EOL;
    }
    $xml .= '</' . $service . 'Rs>' . PHP_EOL;
    $xml .= '</CIMB3rdParty_' . $service . 'Rs>';
    $xml .= '</soap:Body>' . PHP_EOL .
      '</soap:Envelope>';
    return $xml;
  }

  public static function validateInquiryData($data) {
    if(!isset($data['CompanyCode']) || $data['CompanyCode'] == '') return self::INQUIRY_CODE_COMPANY_CODE_EMPTY;
    if(!isset($data['ChannelID']) || $data['ChannelID'] == '') return self::INQUIRY_CODE_CHANNEL_TYPE_EMPTY;
    if(!isset($data['TransactionDate']) || $data['TransactionDate'] == '') return self::INQUIRY_CODE_TRANS_DATE_EMPTY;
    return self::TRX_SUCCESS;
  }

  public static function validatePaymentData($data) {
    if(!isset($data['CompanyCode']) || $data['CompanyCode'] == '') return self::PAYMENT_CODE_COMPANY_CODE_EMPTY;
    if(!isset($data['CustomerKey1']) || $data['CustomerKey1'] == '') return self::PAYMENT_CODE_CUST_KEY_EMPTY;
    if(!isset($data['TransactionID']) || $data['TransactionID'] == '') return self::PAYMENT_CODE_TRANS_ID_EMPTY;
    if(!isset($data['ChannelID']) || $data['ChannelID'] == '') return self::PAYMENT_CODE_CHANNEL_ID_EMPTY;
    if(!isset($data['TransactionDate']) || $data['TransactionDate'] == '') return self::PAYMENT_CODE_TRANS_DATE_EMPTY;
    if(!isset($data['Amount']) || $data['Amount'] == '') return self::PAYMENT_CODE_AMOUNT_EMPTY;
    if(!isset($data['ReferenceNumberTransaction']) || $data['ReferenceNumberTransaction'] == '') return self::PAYMENT_CODE_REFF_EMPTY;
    return self::TRX_SUCCESS;
  }

  public static function buildInquiryErrorXml($data, $errorCode) {
    return self::generateResponseXml([
      'TransactionID' => $data['TransactionID'],
      'TransactionDate' => $data['TransactionDate'],
      'CompanyCode' => $data['CompanyCode'],
      'CustomerKey1' => $data['CustomerKey1'],
      'BillDetailList' =>
        '<BillDetail>' . PHP_EOL .
        '<BillCurrency>IDR</BillCurrency>' . PHP_EOL .
        '<BillCode/>' . PHP_EOL .
        '<BillAmount>0</BillAmount>' . PHP_EOL .
        '<BillReference/>' . PHP_EOL .
        '</BillDetail>' . PHP_EOL,
      'Amount' => '',
      'Fee' => '',
      'PaidAmount' => '',
      'CustomerName' => '',
      'FlagPayment' => '',
      'ResponseCode' => $errorCode,
      'ResponseDescription' => self::getResponseMessage($errorCode)
    ], self::SERVICE_INQUIRY);
  }

  public static function buildInvoiceInquiryXml($data, $result) {
    $amount = $result['amount'];
    $cost = 0;
    $totalAmount = $amount + $cost;
    $customerName = $result['customer_name'];
    $reference = $result['reference'];
    $billName = $result['name'];

    $data = [
      'TransactionID' => $data['TransactionID'],
      'ChannelID' => $data['ChannelID'],
      'TerminalID' => $data['TerminalID'],
      'TransactionDate' => $data['TransactionDate'],
      'CompanyCode' => $data['CompanyCode'],
      'CustomerKey1' => $data['CustomerKey1'],
      'CustomerKey2' => $data['CustomerKey2'] ?? '',
      'CustomerKey3' => $data['CustomerKey3'] ?? '',
      'BillDetailList' =>
        '<BillDetail>' . PHP_EOL .
        '<BillCode/>' . PHP_EOL .
        '<BillReference>' . $reference . '</BillReference>' . PHP_EOL .
        '<BillAmount>' . $amount . '</BillAmount>' . PHP_EOL .
        '<BillCurrency>IDR</BillCurrency>' . PHP_EOL .
        '</BillDetail>' . PHP_EOL,
      'Currency' => 'IDR',
      'Amount' => $amount,
      'Fee' => $cost,
      'PaidAmount' => $totalAmount,
      'CustomerName' => $customerName,
      'AdditionalData1' => $billName,
      'AdditionalData2' => '',
      'AdditionalData3' => '',
      'AdditionalData4' => $reference,
      'FlagPayment' => self::FLAG_CLOSED_PAYMENT,
      'ResponseCode' => self::TRX_SUCCESS,
      'ResponseDescription' => self::RESPONSE_CODE_MAPPINGS[self::TRX_SUCCESS]
    ];

    return self::generateResponseXml($data, self::SERVICE_INQUIRY);
  }

  public static function buildPaymentErrorXml($data, $errorCode) {
    $data = [
      'TransactionID' => $data['TransactionID'],
      'ChannelID' => $data['ChannelID'],
      'TerminalID' => $data['TerminalID'] ?? '',
      'TransactionDate' => $data['TransactionDate'],
      'CompanyCode' => $data['CompanyCode'],
      'CustomerKey1' => $data['CustomerKey1'],
      'CustomerKey2' => $data['CustomerKey2'] ?? '',
      'CustomerKey3' => $data['CustomerKey3'] ?? '',
      'PaymentFlag' => $data['FlagPaymentList'],
      'CustomerName' => '',
      'Currency' => $data['Currency'],
      'Amount' => 0,
      'Fee' => 0,
      'PaidAmount' => 0,
      'ReferenceNumberTransaction' => '',
      'AdditionalData1' => '',
      'AdditionalData2' => '',
      'AdditionalData3' => '',
      'AdditionalData4' => '',
      'ResponseCode' => $errorCode,
      'ResponseDescription' => CimbHelper::getResponseMessage($errorCode)
    ];

    return self::generateResponseXml($data, self::SERVICE_PAYMENT);
  }

  public static function buildInvoicePaymentXml($data) {
    $data = [
      'TransactionID' => $data['TransactionID'],
      'ChannelID' => $data['ChannelID'],
      'TerminalID' => $data['TerminalID'],
      'TransactionDate' => $data['TransactionDate'],
      'CompanyCode' => $data['CompanyCode'],
      'CustomerKey1' => $data['CustomerKey1'],
      'CustomerKey2' => $data['CustomerKey2'],
      'CustomerKey3' => $data['CustomerKey3'],
      'PaymentFlag' => $data['FlagPaymentList'],
      'CustomerName' => $data['CustomerName'],
      'Currency' => $data['Currency'],
      'Amount' => $data['Amount'],
      'Fee' => $data['Fee'],
      'PaidAmount' => $data['PaidAmount'],
      'ReferenceNumberTransaction' => $data['ReferenceNumberTransaction'],
      'AdditionalData1' => $data['AdditionalData1'],
      'AdditionalData2' => $data['AdditionalData2'],
      'AdditionalData3' => $data['AdditionalData3'],
      'AdditionalData4' => $data['AdditionalData4'],
      'ResponseCode' => self::TRX_SUCCESS,
      'ResponseDescription' => self::getResponseMessage(self::TRX_SUCCESS)
    ];

    return self::generateResponseXml($data, self::SERVICE_PAYMENT);
  }

}