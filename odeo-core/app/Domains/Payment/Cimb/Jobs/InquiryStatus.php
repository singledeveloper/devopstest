<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/10/19
 * Time: 14.10
 */

namespace Odeo\Domains\Payment\Cimb\Jobs;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Cimb\Repository\PaymentCimbVaInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository\BankCimbInquiryRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;
use Odeo\Jobs\Job;

class InquiryStatus extends Job {

  private $vaPayment, $cimbInquiry, $vaInquiry, $payment, $redis;

  public function __construct($vaPayment) {
    parent::__construct();
    $this->vaPayment = $vaPayment;
    $this->cimbInquiry = app()->make(BankCimbInquiryRepository::class);
    $this->vaInquiry = app()->make(PaymentCimbVaInquiryRepository::class);
    $this->payment = app()->make(PaymentRepository::class);
    $this->redis = Redis::connection();
  }

  public function handle() {
    $key = 'odeo_core:cimb_change_status_' . $this->vaPayment->id;
    if (!$this->redis->setnx($key, 1)) {
      clog('cimb_va', 'ID: ' . $this->vaPayment->id . ' Inquiry Status locked');
      return null;
    }
    $this->redis->expire($key, 5 * 60);

    $inquiry = $this->vaInquiry->findById($this->vaPayment->inquiry_id);

    if (!$inquiry) {
      clog('cimb_va', 'ID: ' . $this->vaPayment->id . ' Inquiry Not found');
      return null;
    }

    $payment = $this->payment->findById($this->vaPayment->payment_id);
    if (!$payment) {
      clog('cimb_va', 'ID: ' . $this->vaPayment->id . ' Payment Not found');
      return null;
    }

    $bankInquiry = $this->cimbInquiry->getUnreconciledVaPayment($this->vaPayment->reference_number_transaction);
    if ($bankInquiry &&
      $bankInquiry->debit == 0 &&
      $bankInquiry->credit == $this->vaPayment->paid_amount
    ) {
      $this->vaPayment->status = PaymentGateway::SUCCESS;
      $this->vaPayment->save();

      $pipeline = new Pipeline;
      $pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyPaymentStatus'));
      $pipeline->execute([
        'pg_payment_id' => $this->vaPayment->payment->source_id,
        'pg_payment_status' => PaymentGateway::SUCCESS,
        'receive_notify_time' => Carbon::now(),
        'vendor_reference_id' => $this->vaPayment->reference_number_transaction
      ]);

      if ($pipeline->fail()) {
        clog('cimb_va', 'ID: ' . $this->vaPayment->id . ' Inquiry Status Fail Notify: ' . $pipeline->errorMessage);
        return null;
      }

      dispatch(new Reconciler([
        'bank_inquiry_id' => $bankInquiry->id,
        'pg_payment_id' => $this->vaPayment->payment->source_id,
        'va_payment_id' => $this->vaPayment->id
      ]));
    }

    $this->redis->del($key);
  }

}