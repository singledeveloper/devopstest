<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/14/16
 * Time: 8:24 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandiriecash\Helper;


use Odeo\Domains\Payment\Dam\Mandiriecash\Repository\PaymentDamMandiriEcashPaymentRepository;
use Odeo\Domains\Payment\Helper\PaymentManager;


class DamManager extends PaymentManager {

  protected $library, $api, $damMandiriEcash, $initializer;

  public function __construct() {
    parent::__construct();

    $this->damMandiriEcash = app()->make(PaymentDamMandiriEcashPaymentRepository::class);
    $this->library = app()->make(DamLibrary::class);
    $this->api = app()->make(DamApi::class);
    $this->initializer = app()->make(DamInitializer::class);

    $this->initializer->init();
    $this->api->initialize();
  }
}