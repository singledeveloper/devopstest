<?php

/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/14/16
 * Time: 8:17 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandiriecash;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Dam\Mandiriecash\Helper\DamInitializer;
use Odeo\Domains\Payment\Dam\Mandiriecash\Helper\DamManager;

class Requester extends DamManager {

  public function __construct() {
    parent::__construct();
  }

  public function request(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    $damMandiriEcash = $this->damMandiriEcash->getNew();

    $damMandiriEcash->finish_redirect_url = $data['finish_redirect_url'];
    $damMandiriEcash->unfinish_redirect_url = $data['unfinish_redirect_url'];


    if ($order->total > 250000) $fee = $this->feeGenerator->getFee(1, $order->total);
    else{
      $fee = 2500;
    }

    $orderTotal = $order->total + $fee;

    $data = array(
      'params' => [
        'amount' => $orderTotal,
        'clientAddress' => $data['client_ip_address'],
        'description' => "Odeo Purchase",
        'memberAddress' => $_SERVER['SERVER_ADDR'],
        'returnUrl' => baseUrl(DamInitializer::REDIRECT_URL),
        'toUsername' => DamInitializer::$mid,
        'hash' => $this->library->doHashingForm($orderTotal, $data['client_ip_address']),
        'trxid' => $order->id
      ]
    );

    $responseTransactionID = $this->api->requestTransactionId($data);

    if (!$responseTransactionID || $responseTransactionID == 'INVALID_DATA' || $responseTransactionID == 'INVALID_RETURNURL') {
      return $listener->response(400, 'Exception from Mandiri Ecash.');
    }

    $damMandiriEcash->ecash_id = $responseTransactionID;
    $this->damMandiriEcash->save($damMandiriEcash);

    $payment->reference_id = $damMandiriEcash->id;
    $this->payments->save($payment);

    return $listener->response(200, [
      'type' => \Odeo\Domains\Constant\Payment::REQUEST_PAYMENT_TYPE_URL,
      'content' => [
        'link' => $this->library->generateRedirectUrlId($responseTransactionID)
      ]
    ]);

  }

}
