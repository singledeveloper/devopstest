<?php

/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/19/16
 * Time: 11:06 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandiriecash\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Dam\Mandiriecash\Model\PaymentDamMandiriEcashPayment;

class PaymentDamMandiriEcashPaymentRepository extends Repository {

  public function __construct(PaymentDamMandiriEcashPayment $paymentDamMandiriEcash) {
    $this->model = $paymentDamMandiriEcash;
  }

  public function findByEcashId($ecashId) {

    return $this->model->where('ecash_id', $ecashId)->first();

  }

}