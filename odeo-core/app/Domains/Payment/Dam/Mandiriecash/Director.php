<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/22/16
 * Time: 11:46 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandiriecash;


use GuzzleHttp\Client;
use Illuminate\Database\QueryException;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Dam\Mandiriecash\Helper\DamInitializer;
use Odeo\Domains\Payment\Dam\Mandiriecash\Helper\DamManager;


class Director extends DamManager {

  protected $damMandiriEcash, $paymentDamMandiri;

  public function __construct() {
    parent::__construct();
    $this->damMandiriEcash = app()->make(\Odeo\Domains\Payment\Dam\Mandiriecash\Repository\PaymentDamMandiriEcashPaymentRepository::class);
    $this->paymentDamMandiri = app()->make(\Odeo\Domains\Payment\Dam\Mandiriecash\Repository\PaymentDamMandiriEcashPaymentRepository::class);

  }

  public function redirect(PipelineListener $listener, $order, $data) {

    $damMandiriEcash = $this->damMandiriEcash->findByEcashId($data['id']);

    if ($order->status >= OrderStatus::VERIFIED) {
      return $listener->response(200, [
        'redirect_to' => $damMandiriEcash->finish_redirect_url
      ]);
    }

    if (!$damMandiriEcash->status_id) {

      $client = new Client();
      $res = $client->get(DamInitializer::$validation . '?id=' . $data['id']);

      $data = explode(",", trim($res->getBody()));

      $damMandiriEcash->trace_number = $data[1];
      $damMandiriEcash->phone_number = $data[2];
      $damMandiriEcash->merchant_trx_id = $data[3];
      $damMandiriEcash->status = $data[4];
      $damMandiriEcash->status_id = $data[0] . $data[4];
      $damMandiriEcash->notified_date_time = date('Y-m-d H:i:s', strtotime("now"));

      if ($data[4] == 'FAILED') {

        $this->damMandiriEcash->save($damMandiriEcash);

        return $listener->response(200, [
          'redirect_to' => $damMandiriEcash->unfinish_redirect_url
        ]);
      }

      try {

        $this->damMandiriEcash->save($damMandiriEcash);

      } catch (QueryException $exception) {
        if (!str_contains($exception->getMessage(), 'duplicate')) {
          return $listener->response(200, [
            'redirect_to' => $damMandiriEcash->unfinish_redirect_url
          ]);
        }
      }

      if ($order->total > 250000) $fee = $this->feeGenerator->getFee(1, $order->total);
      else{
        $fee = 2500;
      }

      $order->status = OrderStatus::VERIFIED;
      $order->paid_at = date('Y-m-d H:i:s');

      $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

      $order->total = $order->total + $fee;

      $this->orders->save($order);

      $listener->pushQueue(new VerifyOrder($order->id));
    }


    return $listener->response(200, [
      'redirect_to' => $damMandiriEcash->finish_redirect_url
    ]);

  }

  public function translate(PipelineListener $listener, $data) {

    $paymentDamMandiriEcash = $this->paymentDamMandiri->findByEcashId($data['id']);
    $payment = $this->payments->findByReferenceId($paymentDamMandiriEcash->id, Payment::OPC_GROUP_MANDIRI_E_CASH);

    return $listener->response(200, [
      'order_id' => $payment->order_id
    ]);

  }

}