<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/2/16
 * Time: 10:17 PM
 */

namespace Odeo\Domains\Payment\Dam\Mandirimpt\Helper;


class DamInitializer {


  static $initPaymentApi, $callbackUrl, $validate;
  static $mid, $mpt, $mck, $atmDisplay;

  public function init() {
    if (!isProduction()) {

      self::$mid = 'odeompt';
      self::$mpt = '253F6BB3E0796C1C01D68E99A5B1121C';
      self::$mck = '582ca2cf6873f402e0000004';
      self::$atmDisplay = 'Odeo';
      self::$initPaymentApi = 'https://api.mandiriecash.biz/merchant/payment/initiate/';
      self::$callbackUrl = baseUrl('v1/payment/dam/mandiri/mpt/return-url');
      self::$validate = 'https://api.mandiriecash.biz/merchant/payment/validate';

    } else {

      self::$mid = '';
      self::$mpt = '';
      self::$mck = '';
      self::$atmDisplay = '';
      self::$initPaymentApi = '';
      self::$callbackUrl = '';
      self::$validate = '';

    }
  }

}
