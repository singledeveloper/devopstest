<?php


namespace Odeo\Domains\Payment\Pax\Repository;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Odeo\Domains\Constant\PaxPaymentStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Pax\Model\PaxPayment;

class PaxPaymentRepository extends Repository {

  public function __construct(PaxPayment $model) {
    $this->setModel($model);
  }

  public function findSuccessByOrderId($orderId) {
    return $this->getCloneModel()
      ->where('order_id', $orderId)
      ->where('status', PaxPaymentStatus::COMPLETED)
      ->first();
  }

  public function countByDateAndTerminalId($date, $terminalId) {
    $start = Carbon::parse($date)->startOfMonth();
    $end = Carbon::parse($date)->endOfMonth();
    return $this->getCloneModel()
      ->where('terminal_id', $terminalId)
      ->whereDate('trx_date', '>=', $start)
      ->whereDate('trx_date', '<=', $end)
      ->count();
  }

  public function updateBniEdcTransactionReference($beforeDate) {
    DB::statement("
      UPDATE pax_payments pax
      SET updated_at                   = now(),
        edc_transaction_reference_id   = bni.id,
        edc_transaction_reference_type = 'bni_edc_transactions'
      FROM bni_edc_transactions bni
      WHERE edc_transaction_reference_id IS NULL 
      AND pax.approval_code = bni.approval_code
      AND pax.trx_date = bni.trx_date
      AND pax.trx_date <= :before_date",
      [
        'before_date' => $beforeDate
      ]);
  }

  public function findNotSettled($beforeDate) {
    return $this->model
      ->where('trx_date', '<=', $beforeDate)
      ->where('status', '=', PaxPaymentStatus::COMPLETED)
      ->whereNull('edc_transaction_reference_id')
      ->get();
  }

  public function findByTID($tid) {
    return $this->getCloneModel()
      ->where('tid', $tid)
      ->orderByDesc('id')
      ->first();
  }
}