<?php


namespace Odeo\Domains\Payment\Pax;


use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\EdcTransactionStatus;
use Odeo\Domains\Constant\EdcTransactionVendor;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\PaxPayment;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\MarginFormatter;
use Odeo\Domains\Order\Helper\OrderCharger;
use Odeo\Domains\Payment\Odeo\Terminal\Repository\UserTerminalRepository;
use Odeo\Domains\Payment\Pax\Helper\CardTypeHelper;
use Odeo\Domains\Payment\Pax\Repository\EdcTransactionRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;

class EdcTransactionRequester {

  private $cashInserter, $cardTypeHelper, $userTerminalRepo, $edcTransactionRepo, $marginFormatter, $orderCharger;

  public function __construct() {
    $this->cashInserter = app()->make(CashInserter::class);
    $this->cardTypeHelper = app()->make(CardTypeHelper::class);
    $this->userTerminalRepo = app()->make(UserTerminalRepository::class);
    $this->edcTransactionRepo = app()->make(EdcTransactionRepository::class);
    $this->marginFormatter = app()->make(MarginFormatter::class);
    $this->orderCharger = app()->make(OrderCharger::class);
  }

  public function validate(PipelineListener $listener, $data) {
    $trx = $this->edcTransactionRepo->findByReferenceTypeAndReferenceId($data['edc_transaction_vendor'], $data['edc_transaction_id']);
    if ($trx) {
      return $listener->response(400, 'transaction handled before with edc_transactions.id=' . $trx->id);
    }

    $formatted = $this->marginFormatter->formatMargin($data['amount'], $data);
    $formatted['merchant_price'] = 0;
    $formatted['sale_price'] = $formatted['base_price'];
    return $listener->response(200, $formatted);
  }

  public function checkout(PipelineListener $listener, $data) {
    $terminal = $this->userTerminalRepo->findActiveByTerminalId($data['terminal_id']);
    $cardType = $this->cardTypeHelper->getCardType($data['card_number']);
    $mdrPct = $this->getMDRPercentage($cardType, $terminal);
    $mdr = round($data['amount'] * $mdrPct / 100);

    $trx = $this->edcTransactionRepo->getNew();
    $trx->order_id = $data['order_id'];
    $trx->user_id = $data['auth']['user_id'];
    $trx->amount = $data['amount'];
    $trx->trx_date = $data['trx_date'];
    $trx->mdr_pct = $mdrPct;
    $trx->mdr = $mdr;
    $trx->status = EdcTransactionStatus::ON_PROGRESS;
    $trx->reference_type = EdcTransactionVendor::BNI_EDC_TRANSACTIONS;
    $trx->reference_id = $data['edc_transaction_id'];
    $trx->save();

    return $listener->response(200);
  }

  public function completeOrder(PipelineListener $listener, $data) {
    clog('edc_trx', 'complete order: ' . json_encode($data));
    $trx = $this->edcTransactionRepo->findByOrderId($data['order_id']);
    $trx->status = EdcTransactionStatus::COMPLETED;
    $trx->save();

    $this->orderCharger->charge($data['order_id'], OrderCharge::PAYMENT_SERVICE_COST, $trx->mdr, OrderCharge::GROUP_TYPE_COMPANY_PROFIT);

    $this->cashInserter->add([
      'user_id' => $trx->user_id,
      'cash_type' => CashType::OCASH,
      'trx_type' => TransactionType::EDC_TRANSACTION_PAYMENT,
      'amount' => $trx->amount,
      'data' => json_encode([
        'order_id' => $trx->order_id,
      ]),
    ]);
    $this->cashInserter->add([
      'user_id' => $trx->user_id,
      'cash_type' => CashType::OCASH,
      'trx_type' => TransactionType::MDR,
      'amount' => -$trx->mdr,
      'data' => json_encode([
        'order_id' => $trx->order_id,
      ]),
    ]);
    $this->cashInserter->run();

    return $listener->response(200);
  }

  private function getMDRPercentage($cardType, $terminal) {
    switch ($cardType) {
      case PaxPayment::TYPE_DEBIT_OFF_US:
      case PaxPayment::TYPE_DEBIT_ON_US:
        return $terminal->debit_mdr;
      default:
        return $terminal->credit_mdr;
    }
  }

}