<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/08/19
 * Time: 15.00
 */

namespace Odeo\Domains\Payment\Pax\Helper;


class ParseHelper {

  const COLUMN_MAPPINGS = [
    'A' => 'proc_date',
    'B' => 'mid',
    'C' => 'ob',
    'D' => 'gb',
    'E' => 'seq',
    'F' => 'type',
    'G' => 'trx_date',
    'H' => 'approval_code',
    'I' => 'card_no',
    'J' => 'amount',
    'K' => 'tid',
    'L' => 'trx_type',
    'M' => 'ptr',
    'N' => 'mdr_pct',
    'O' => 'mdr',
    'P' => 'air_fare',
    'Q' => 'plan',
    'R' => 'ss_amount',
    'S' => 'ss_fee_type',
    'T' => 'flag',
    'U' => 'net_amount',
    'V' => 'merchant_account',
    'W' => 'merchant_name',
  ];

  const PARSE_SUBJECT_BNI_EDC = 'BNI Direct EDC Settlement';
  const REFERENCE_BNI_EDC_SETTLEMENT = 'bni_edc_settlements';

  public static function getAttachmentDir() {
    return storage_path('app/bni_edc_settlement');
  }

}