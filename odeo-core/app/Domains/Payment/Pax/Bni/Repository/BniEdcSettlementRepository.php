<?php


namespace Odeo\Domains\Payment\Pax\Bni\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Pax\Bni\Model\BniEdcSettlement;

class BniEdcSettlementRepository extends Repository {

  public function __construct(BniEdcSettlement $model) {
    $this->model = $model;
  }

  public function listByTotalAmounts($totalAmounts) {
    return $this->getCloneModel()
      ->whereIn('total_amount', $totalAmounts)
      ->get();
  }

  public function whereIdNotIn($reffs) {
    return $this->getCloneModel()
      ->whereNotIn('id', $reffs)
      ->get();
  }
}