<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 07/08/19
 * Time: 13.23
 */

namespace Odeo\Domains\Payment\Pax\Bni\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bni\Repository\BankBniInquiryRepository;
use Odeo\Domains\Payment\Pax\Bni\Jobs\SendUnsettledBniEdcSettlements;
use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcSettlementRepository;
use Odeo\Domains\Payment\Pax\Helper\ParseHelper;

class ReconBankBniEdcSettlementV2 {

  private $bankBniInquiryRepo;
  private $bniEdcSettlementRepo;

  public function init() {
    $this->bankBniInquiryRepo = app()->make(BankBniInquiryRepository::class);
    $this->bniEdcSettlementRepo = app()->make(BniEdcSettlementRepository::class);
  }

  public function run() {
    $this->init();
    $settlements = $this->bankBniInquiryRepo->getBniEdcSettlementList();

    if (count($settlements) > 0) {
      $inquiryUpdates = [];
      $now = Carbon::now();

      foreach ($settlements as $settlement) {
        $inquiryUpdates[] = [
          'id' => $settlement->inquiry_id,
          'reference_type' => ParseHelper::REFERENCE_BNI_EDC_SETTLEMENT,
          'reference' => $settlement->settlement_id,
          'updated_at' => $now
        ];
      }

      $this->bankBniInquiryRepo->updateBulk($inquiryUpdates);
    } else {
      clog('bni-edc', "there's no unreconciled settlements");
    }

    $references = $this->bankBniInquiryRepo->getBniSettlementReferenceIds();
    if ($unknownSettlements = $this->bniEdcSettlementRepo->whereIdNotIn($references)) {
      dispatch(new SendUnsettledBniEdcSettlements($unknownSettlements->pluck('id'), date('Y-m-d')));
    }

  }

}