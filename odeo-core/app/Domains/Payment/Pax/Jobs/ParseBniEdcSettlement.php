<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/08/19
 * Time: 18.43
 */

namespace Odeo\Domains\Payment\Pax\Jobs;


use Carbon\Carbon;
use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcParsedAttachmentRepository;
use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcSettlementRepository;
use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcTransactionRepository;
use Odeo\Domains\Payment\Pax\Helper\ParseHelper;
use Odeo\Jobs\Job;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

class ParseBniEdcSettlement extends Job {

  private $data, $parseAttachmentRepo, $edcSettlementRepo, $edcTransactionRepo;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function init() {
    $this->parseAttachmentRepo = app()->make(BniEdcParsedAttachmentRepository::class);
    $this->edcSettlementRepo = app()->make(BniEdcSettlementRepository::class);
    $this->edcTransactionRepo = app()->make(BniEdcTransactionRepository::class);
  }

  public function handle() {
    $this->init();
    $path = $this->data['path'];
    $files = scandir($path);

    if ($files) {
      $reader = new Xls();
      $ext = '.xls';
      $reader->setReadDataOnly(true);

      $transactions = [];
      $trxDate = null;

      foreach ($files as $fileName) {

        if (substr_compare($fileName, $ext, -strlen($ext)) == 0) {

          $file = $reader->load("$path/$fileName");
          $rowI = $file->getActiveSheet();

          $notEmpty = $rowI->getCell('A2')->getValue();
          if (str_replace(' ', '', $notEmpty) != '') {

            $settlement = $this->edcSettlementRepo->getNew();
            $settlement->total_amount = 0;
            $settlement->trx_count = 0;
            $settlement->trx_date = $this->data['trx_date'];
            $this->edcSettlementRepo->save($settlement);

            $settleAmount = $trxCount = 0;
            $now = Carbon::now();


            foreach ($rowI->getRowIterator() as $row) {
              if ($row->getRowIndex() == 1) continue;
              $cellI = $row->getCellIterator();
              if ($cellI->current() == " ") break;

              $tempRow = [];
              foreach ($cellI as $key => $cell) {
                $tempRow[ParseHelper::COLUMN_MAPPINGS[$key]] = $cell->getValue();
              }

              $tempRow['bni_edc_settlement_id'] = $settlement->id;
              $tempRow['proc_date'] = Carbon::createFromFormat('d/m/Y', $tempRow['proc_date']);
              $tempRow['trx_date'] = Carbon::createFromFormat('d/m/Y', $tempRow['trx_date']);
              $tempRow['trx_fee'] = 0;
              $tempRow['created_at'] = $now;
              $tempRow['updated_at'] = $now;

              $transactions[] = $tempRow;
              $settleAmount += $tempRow['net_amount'];
              $trxCount += 1;
            }

            $settlement->total_amount = $settleAmount;
            $settlement->trx_count = $trxCount;
            $this->edcSettlementRepo->save($settlement);
          }

          unlink("$path/$fileName");
          \Log::info("remove file: $fileName");
        }
      }

      if (count($transactions)) {
        $this->edcTransactionRepo->saveBulk($transactions);
      }

      $parseRow = $this->parseAttachmentRepo->findById($this->data['parse_id']);
      $parseRow->parsed = true;
      $this->parseAttachmentRepo->save($parseRow);
      rmdir($path);
      unlink("$path.zip");

    }


  }
}