<?php

namespace Odeo\Domains\Payment\Prismalink\VaDirect\Jobs;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Odeo\Jobs\Job;
use Odeo\Domains\Constant\PrismalinkPaymentStatus;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;
use Odeo\Domains\Payment\Prismalink\Helper\PrismalinkLibrary;
use Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaStatusRepository;
use Illuminate\Support\Facades\Redis;

class InquiryStatus extends Job {

  private $vaPayment, $redis, $library, $prismalinkVaStatus;

  public function __construct($vaPayment) {
    parent::__construct();
    $this->vaPayment = $vaPayment;
    $this->library = app()->make(PrismalinkLibrary::class);
    $this->prismalinkVaStatus = app()->make(PaymentPrismalinkVaStatusRepository::class);
    $this->redis = Redis::connection();
  }

  public function handle() {
    $key = 'odeo_core:prismalink_change_status_' . $this->vaPayment->id;
    if (!$this->redis->setnx($key, 1)) {
      \Log::notice('Inquiry Status locked');
      return null;
    };
    $this->redis->expire($key, 5 * 60);

    $signature = $this->library->generateSignature([
      'traceNo' => $this->vaPayment->trace_number,
      'code' => '00'
    ]);
    $payload = [
      'mercId' => env("PRISMALINK_PARTNER_ID"),
      'traceNo' => $this->vaPayment->trace_number,
      'sign' => $signature
    ];
    $client = new Client();
    $request = new Request('POST', env("PRISMALINK_CHECK_STATUS_URL"),
    [
      'Content-Type' => 'application/json',
    ], json_encode($payload));

    try {
      $response = $client->send($request);  
      $responseContent = $response->getBody()->getContents(); 
    }
    catch (\GuzzleHttp\Exception\RequestException $e) {
      $errorMessage = $e->getMessage();
      $response = $e->getResponse();
      $responseContent = '';
    }
    
    $requestDump = $this->getDump($request->getHeaders(), json_encode($payload));
    $responseDump = $this->getDump($response->getHeaders(), $responseContent);

    $responseContent = json_decode($responseContent);
    if (!$responseContent || !isset($responseContent->rc) || !isset($responseContent->status)) {
      \Log::notice('Inquiry Status Invalid response');
      return null;
    }
    
    $vaStatus = $this->prismalinkVaStatus->getNew();
    $vaStatus->prismalink_payment_id = $this->vaPayment->id;
    $vaStatus->request_dump = $requestDump;
    $vaStatus->response_dump = $responseDump;
    $vaStatus->status_code = $response->getStatusCode();
    $this->prismalinkVaStatus->save($vaStatus);

    $responseCode = $responseContent->rc;
    $responseStatus = $responseContent->status;
    if ($responseCode == '00' && $responseStatus == 'APRVD') {
      $newStatus = PrismalinkPaymentStatus::SUCCESS;
    } else if ($responseCode == '00' && $responseStatus == 'REJEC') {
      $newStatus = 
        $this->vaPayment->status == PrismalinkPaymentStatus::SUSPECT_IS_CANCELLED_BUT_NEED_STATUS_RECHECK 
        ? PrismalinkPaymentStatus::FAILED_PAYMENT_IS_CANCELLED
        : PrismalinkPaymentStatus::SUSPECT_REJECTED_BY_MERCHANT;
    } else if ($responseCode == '00' && $responseStatus == 'ONPRG') {
      $newStatus = PrismalinkPaymentStatus::SUSPECT_TIMEOUT;
    } else if ($responseCode == '14' && $responseStatus == 'NOT_FOUND') {
      $newStatus = 
        $this->vaPayment->status == PrismalinkPaymentStatus::SUSPECT_IS_CANCELLED_BUT_NEED_STATUS_RECHECK 
        ? PrismalinkPaymentStatus::FAILED_PAYMENT_IS_CANCELLED
        : PrismalinkPaymentStatus::SUSPECT_NOT_FOUND_ON_PRISMALINK;
    } else if ($responseCode == '997' && $responseStatus == 'ODEO_TEST') {
      $newStatus = PrismalinkPaymentStatus::SUSPECT_FOR_TESTING;
    } else if ($responseCode == '998' && $responseStatus == 'ODEO_TEST') {
      $newStatus = PrismalinkPaymentStatus::FAILED;
    } else if ($responseCode == '999' && $responseStatus == 'ODEO_TEST') {
      $newStatus = PrismalinkPaymentStatus::SUSPECT_NEED_STATUS_RECHECK;
    } else {
      $newStatus = PrismalinkPaymentStatus::UNKNOWN;
    }

    if (in_array($this->vaPayment->id, [328165,328170])) {
      $newStatus = PrismalinkPaymentStatus::SUCCESS;
    }

    if ($newStatus == PrismalinkPaymentStatus::SUSPECT_NEED_STATUS_RECHECK) {
      return null;
    } else if ($newStatus == PrismalinkPaymentStatus::UNKNOWN) {
      \Log::notice('Inquiry Status Status unknown');
      return null;
    } 

    $oldStatus = $this->vaPayment->status;
    
    if ($newStatus != $oldStatus) {
      $this->vaPayment->status = $newStatus;
      $this->vaPayment->save();
      
      $pipeline = new Pipeline;
      $pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyPaymentStatus'));
      $pipeline->execute([
        'pg_payment_id' => $this->vaPayment->payment->source_id,
        'pg_payment_status' => PrismalinkPaymentStatus::getPaymentGatewayStatus($newStatus),
        'receive_notify_time' => Carbon::now(),
        'vendor_reference_id' => $this->vaPayment->trace_number
      ]);

      if ($pipeline->fail()) {
        \Log::notice('Inquiry Status Fail Notify: ' . $pipeline->errorMessage);
        return null;
      }
    }

    $this->redis->del($key);
  }

  private function getDump($headers, $stringBody) {
    $dump = '';
    foreach ($headers as $key => $val) {
      $dump = $dump.$key.': '.$val[0]."\n";
    }
    return $dump."\n".$stringBody;
  }
}