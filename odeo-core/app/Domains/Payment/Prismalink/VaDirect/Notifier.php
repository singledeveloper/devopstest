<?php
namespace Odeo\Domains\Payment\Prismalink\VaDirect;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Prismalink\Helper\PrismalinkManager;

class Notifier extends PrismalinkManager {

  private $prismalinkVaDirectPayments;

  public function __construct() {
    parent::__construct();
    $this->prismalinkVaDirectPayments = app()->make(\Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaDirectPaymentRepository::class);
  }

  public function notified(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    $prismalinkVaDirectPayment = $this->prismalinkVaDirectPayments->findById($payment->reference_id);

    if (strtolower($data['signature']) == $this->library->generateSignature($data)) {

      $prismalinkVaDirectPayment->payment_code = $data['code'];
      $prismalinkVaDirectPayment->payment_fee = $data['feeAmount'];
      $prismalinkVaDirectPayment->payment_net_amount = $data['netAmount'];
      $prismalinkVaDirectPayment->currency_code = $data['codeCurrency'];
      $prismalinkVaDirectPayment->paid_at = \Carbon\Carbon::createFromFormat('YmdHis', $data['transDate']);
      $prismalinkVaDirectPayment->reference_number = $data['traceNo'];
      $prismalinkVaDirectPayment->payment_signature = $data['signature'];

      $this->prismalinkVaDirectPayments->save($prismalinkVaDirectPayment);

      $order->status = OrderStatus::VERIFIED;
      $order->paid_at = $prismalinkVaDirectPayment->paid_at;

      $this->orders->save($order);
      
      $listener->pushQueue(new VerifyOrder($order->id));

      return $listener->response(200, [
        'response_code' => '00',
        'response_message' => 'Success',
        'customer_name' => $order->name
      ]);
    } else {
      return $listener->response(400, [
        'response_code' => '01',
        'response_message' => 'Invalid Signature',
        'customer_name' => ''
      ]);
    }
  }
}
