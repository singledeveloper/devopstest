<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 28/11/18
 * Time: 14.53
 */

namespace Odeo\Domains\Payment\Prismalink\VaDirect\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Prismalink\VaDirect\Model\PaymentPrismalinkCallbackLog;

class PaymentPrismalinkCallbackLogRepository extends Repository {

  public function __construct(PaymentPrismalinkCallbackLog $log) {
    $this->model = $log;
  }

}
