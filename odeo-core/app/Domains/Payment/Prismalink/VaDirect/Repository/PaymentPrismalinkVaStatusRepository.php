<?php
namespace Odeo\Domains\Payment\Prismalink\VaDirect\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Prismalink\VaDirect\Model\PaymentPrismalinkVaStatus;

class PaymentPrismalinkVaStatusRepository extends Repository {

  public function __construct(PaymentPrismalinkVaStatus $prismalinkVaStatus) {
    $this->model = $prismalinkVaStatus;
  }

}
