<?php

namespace Odeo\Domains\Payment\Prismalink\VaDirect;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Prismalink\Helper\PrismalinkManager;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;
use Odeo\Exceptions\FailException;
use Carbon\Carbon;

class Inquirer extends PrismalinkManager {

  private $prismalinkVaDirectPayments, $prismalinkVaInquiry, $pgUserPaymentChannel;

  public function __construct() {
    parent::__construct();
    $this->prismalinkVaDirectPayments = app()->make(\Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaDirectPaymentRepository::class);
    $this->prismalinkVaInquiry = app()->make(\Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaInquiryRepository::class);
    $this->pgUserPaymentChannel =  app()->make(PaymentGatewayUserPaymentChannelRepository::class);
  }

  public function inquiry(PipelineListener $listener, $data) {
    $amount = floatval($data['total']);

    $prismalinkVaDirectPayment = $this->prismalinkVaDirectPayments->getNew();
    $prismalinkVaDirectPayment->code = $data['code'];
    $prismalinkVaDirectPayment->partner_id = $data['partnerId'];
    $prismalinkVaDirectPayment->amount = $amount;
    $prismalinkVaDirectPayment->signature = $data['signature'];
    $prismalinkVaDirectPayment->order_id = $data['order_id'];
    $prismalinkVaDirectPayment->virtual_account_number = $data['virtual_account_number'];
    $prismalinkVaDirectPayment->reference_number = $data['traceNo'];

    $this->prismalinkVaDirectPayments->save($prismalinkVaDirectPayment);

    $payment = $this->payments->findByOrderId($data['order_id']);
    $payment->reference_id = $prismalinkVaDirectPayment->id;

    $this->payments->save($payment);

    return $listener->response(200, [
      'va_name' => substr($data['customer_name'], 0, 30),
      'va_desc' => $data['item_name'] ?? $data['customer_name'],
      'response_code' => '00',
      'response_message' => 'Success',
      'billing_amount' => $amount
    ]);
  }


  public function vaInquiry(PipelineListener $listener, $data) {
    try {
      $this->validateCodeAndPartnerID($data, ['01']);
      $this->matchSignature($data);
    } catch (FailException $e) {
      return $listener->response(400, $e->getMessage());
    }

    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['accountNo']);
    if (!$pgUserChannel) {
      return $listener->response(400, 'payment.unrecognized_va_number');
    }

    if (PaymentGateway::exceedVaCodeLength($data['accountNo'], $pgUserChannel->max_length)) {
      return $listener->response(400, trans('payment.va_max_length_exceeded'));
    }

    return $listener->response(200, [
      'va_code' => $data['accountNo'],
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'trace_number' => $data['traceNo'],
      'vendor_reference_id' => $data['traceNo']
    ]);
  }

  public function createInquiry(PipelineListener $listener, $data) {
    $inquiry = $this->prismalinkVaInquiry->getNew();
    $inquiry->va_code = $data['va_code'];
    $inquiry->amount = $data['amount'];
    $inquiry->display_text = $data['display_text'];
    $inquiry->customer_name = $data['customer_name'];
    $inquiry->trace_number = $data['trace_number'];
    $this->prismalinkVaInquiry->save($inquiry);

    return $listener->response(200, []);
  }
  
}
