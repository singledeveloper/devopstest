<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/07/19
 * Time: 15.06
 */

namespace Odeo\Domains\Payment\Prismalink\VaDirect;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaInquiryRepository;

class InquirySelector {

  private $vaInquiryRepo;

  function __construct() {
    $this->vaInquiryRepo = app()->make(PaymentPrismalinkVaInquiryRepository::class);
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->vaInquiryRepo->normalizeFilters($data);
    $this->vaInquiryRepo->setSimplePaginate(true);

    $result = [];
    $inquiries = $this->vaInquiryRepo->get();

    foreach ($inquiries as $inquiry) {
      $result[] = [
        'id' => $inquiry->id,
        'va_code' => $inquiry->va_code,
        'display_text' => $inquiry->display_text,
        'customer_name' => $inquiry->customer_name,
        'amount' => $inquiry->amount,
        'created_at' => $inquiry->created_at,
        'trace_number' => $inquiry->trace_number ?? '-'
      ];
    }

    if (count($inquiries) > 0) {
      return $listener->response(200, array_merge(
        ['inquiries' => $result],
        $this->vaInquiryRepo->getPagination()
      ));
    }
    return $listener->response(200, ['inquiries' => []]);
  }


}