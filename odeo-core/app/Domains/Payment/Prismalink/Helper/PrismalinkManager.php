<?php

namespace Odeo\Domains\Payment\Prismalink\Helper;

use Odeo\Domains\Payment\Helper\PaymentManager;
use Odeo\Exceptions\FailException;

class PrismalinkManager extends PaymentManager {

  protected $library, $initializer, $api;

  public function __construct() {
    parent::__construct();

    $this->library = app()->make(PrismalinkLibrary::class);
  }

  protected function validateCodeAndPartnerID($data, $expectedCode) {
    if (!in_array($data['code'], $expectedCode)) {
      throw new FailException(trans('prismalink.invalid_code'));
    }

    $expectedPartnerId = env("PRISMALINK_PARTNER_ID");
    if ($data['partnerId'] !== $expectedPartnerId) {
      throw new FailException(trans('prismalink.invalid_partner_id'));
    }
  }

  protected function matchSignature($data) {
    $expectedSignature = $this->library->generateSignature($data);
    if (strtolower($data['signature']) !== $expectedSignature) {
      throw new FailException(trans('prismalink.signature_not_match'));
    }
  }

}