<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/10/19
 * Time: 10.52
 */

namespace Odeo\Domains\Payment\Maybank\Va;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Maybank\Va\Repository\PaymentMaybankVaInquiryRepository;
use Odeo\Domains\Payment\Maybank\Va\Repository\PaymentMaybankVaPaymentRepository;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class Inquirer {

  function __construct() {
    $this->maybankVaInquiry = app()->make(PaymentMaybankVaInquiryRepository::class);
    $this->maybankVaPayment = app()->make(PaymentMaybankVaPaymentRepository::class);
    $this->pgUserPaymentChannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->odeoPaymentChannel = app()->make(PaymentOdeoPaymentChannelRepository::class);
  }

  public function vaInquiry(PipelineListener $listener, $data) {
    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['vaNo']);

    if (!$pgUserChannel) {
      clog('maybank_va', trans('payment.unrecognized_va_number'));
      return $listener->response(400, ['message' => trans('payment.unrecognized_va_number'), 'code' => '14']);
    }

    if ($payment = $this->maybankVaPayment->findByVaNo($data['vaNo'])) {
      return $listener->response(400, ['message' => 'Bill Already Paid', 'code' => '88']);
    }

    $channel = $this->odeoPaymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      clog('maybank_va', 'channel not found');
      return $listener->response(400, ['message' => 'channel not found', 'code' => '14']);
    }

    $referenceId = round(microtime(true) * 10000) . "";

    return $listener->response(200, [
      'va_code' => $data['vaNo'],
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'vendor_reference_id' => $referenceId,
    ]);
  }

  public function createInquiry(PipelineListener $listener, $data) {
    $inquiry = $this->maybankVaInquiry->getNew();
    $inquiry->va_no = $data['vaNo'];
    $inquiry->bin_number = $data['binNumber'];
    $inquiry->trace_no = $data['traceNo'];
    $inquiry->transmission_date_time = $data['transmissionDateTime'];
    $inquiry->terminal_id = $data['terminalId'];
    $inquiry->delivery_channel_type = $data['deliveryChannelType'];
    $inquiry->display_text = $data['display_text'];
    $inquiry->amount = $data['amount'];
    $inquiry->customer_name = $data['customer_name'];
    $inquiry->reference = $data['vendor_reference_id'];
    $this->maybankVaInquiry->save($inquiry);

    return $listener->response(200);
  }

}