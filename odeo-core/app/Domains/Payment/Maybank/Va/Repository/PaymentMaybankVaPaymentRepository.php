<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/10/19
 * Time: 10.54
 */

namespace Odeo\Domains\Payment\Maybank\Va\Repository;


use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Maybank\Va\Model\PaymentMaybankVaPayment;

class PaymentMaybankVaPaymentRepository extends Repository {

  function __construct(PaymentMaybankVaPayment $vaPayment) {
    $this->model = $vaPayment;
  }

  function findByVaNo($vaNo) {
    return $this->model
      ->where('va_no', $vaNo)
      ->whereDate('created_at', Carbon::now()->format('Y-m-d'))
      ->orderBy('id', 'DESC')
      ->first();
  }

}