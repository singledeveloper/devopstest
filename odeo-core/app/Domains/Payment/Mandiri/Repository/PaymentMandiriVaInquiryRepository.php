<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/04/19
 * Time: 17.57
 */

namespace Odeo\Domains\Payment\Mandiri\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Mandiri\Model\PaymentMandiriVaInquiry;

class PaymentMandiriVaInquiryRepository extends Repository {

  function __construct(PaymentMandiriVaInquiry $billPaymentInquiry) {
    $this->model = $billPaymentInquiry;
  }

  function findLastInquiry($vaCode) {
    return $this->model
      ->where('bill_key_1', $vaCode)
      ->whereDate('created_at', date('Y-m-d'))
      ->orderBy('created_at', 'DESC')
      ->first();
  }

}