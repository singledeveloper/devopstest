<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/04/19
 * Time: 18.09
 */

namespace Odeo\Domains\Payment\Mandiri;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Helper\PaymentManager;
use Odeo\Domains\Payment\Mandiri\Repository\PaymentMandiriVaPaymentRepository;

class MandiriDirectConfirmer extends PaymentManager {

  function __construct() {
    parent::__construct();
    $this->mandiriPayments = app()->make(PaymentMandiriVaPaymentRepository::class);
  }

  public function confirm(PipelineListener $listener, $data) {

    if (!($order = $this->orders->findById($data['order_id']))) {
      return $listener->response(400);
    }

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');
    $order->total = $order->total + $data['fee'];;
    $this->orders->save($order);

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $data['fee'], OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    $mandiriPayment = $this->mandiriPayments->findById($data['mandiri_bill_id']);
    $mandiriPayment->order_id = $order->id;
    $this->mandiriPayments->save($mandiriPayment);

    $payment = $this->payments->getNew();
    $payment->order_id = $order->id;
    $payment->opc = 398;
    $payment->info_id = Payment::OPC_GROUP_VA_MANDIRI;
    $payment->reference_id = $mandiriPayment->id;
    $this->payments->save($payment);

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200, [
      'status_code' => '00',
      'bill_info_1' => $data['bill_info_1'],
      'bill_info_2' => $data['bill_info_2'],
      'bill_info_3' => $data['bill_info_3'],
      'bill_info_4' => 'Order ' . $order->id
    ]);

  }

}