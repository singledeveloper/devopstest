<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/10/19
 * Time: 14.42
 */

namespace Odeo\Domains\Payment\Mandiri\Scheduler;


use Odeo\Domains\Payment\Mandiri\Jobs\InquiryStatus;
use Odeo\Domains\Payment\Mandiri\Repository\PaymentMandiriVaPaymentRepository;

class CheckStatusScheduler {

  function __construct() {
    $this->mandiriVaPayment = app()->make(PaymentMandiriVaPaymentRepository::class);
  }

  public function run() {
    $pendingPayments = $this->mandiriVaPayment->getPendingPayment();
    foreach ($pendingPayments as $payment) {
      dispatch(new InquiryStatus($payment));
    }
  }

}