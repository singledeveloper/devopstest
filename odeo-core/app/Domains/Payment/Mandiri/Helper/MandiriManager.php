<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 23/04/19
 * Time: 12.54
 */

namespace Odeo\Domains\Payment\Mandiri\Helper;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Payment\Mandiri\Repository\PaymentMandiriVaInquiryRepository;
use Odeo\Domains\Payment\Mandiri\Repository\PaymentMandiriVaPaymentRepository;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class MandiriManager {

  public function __construct() {
    $this->users = app()->make(UserRepository::class);
    $this->helper = app()->make(MandiriHelper::class);
    $this->mandiriVaInquiry = app()->make(PaymentMandiriVaInquiryRepository::class);
    $this->mandiriVaPayment = app()->make(PaymentMandiriVaPaymentRepository::class);
    $this->pgUserPaymentChannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->odeoPaymentChannel = app()->make(PaymentOdeoPaymentChannelRepository::class);
    $this->redis = Redis::connection();
  }

}