<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/10/19
 * Time: 14.44
 */

namespace Odeo\Domains\Payment\Mandiri\Jobs;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\MandiriVaPaymentStatus;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Mandiri\Repository\PaymentMandiriVaInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;

class InquiryStatus {

  private $vaPayment, $mandiriInquiry, $inquiry, $payment, $redis;

  function __construct($vaPayment) {
    $this->vaPayment = $vaPayment;
  }

  private function init() {
    $this->mandiriInquiry = app()->make(BankMandiriGiroInquiryRepository::class);
    $this->inquiry = app()->make(PaymentMandiriVaInquiryRepository::class);
    $this->payment = app()->make(PaymentRepository::class);
    $this->redis = Redis::connection();
  }

  public function handle() {
    $this->init();

    $key = 'odeo_core:mandiri_change_status_' . $this->vaPayment->id;

    if (!$this->redis->setnx($key, 1)) {
      clog('mandiri_va', 'ID: ' . $this->vaPayment->id . ' Inquiry Status locked');
      return null;
    };
    $this->redis->expire($key, 5 * 60);

    $inquiry = $this->inquiry->findById($this->vaPayment->inquiry_id);

    if (!$inquiry) {
      clog('mandiri_va', 'ID: ' . $this->vaPayment->id . ' Inquiry Not found');
      return null;
    }

    $payment = $this->payment->findById($this->vaPayment->payment_id);
    if (!$payment) {
      clog('mandiri_va', 'ID: ' . $this->vaPayment->id . ' Payment Not found');
      return null;
    }

    $bankInquiry = $this->mandiriInquiry->getUnreconciledVaPayment($this->vaPayment->bill_key_1);
    if ($bankInquiry && $this->vaPayment->payment_amount == $bankInquiry->credit) {
      $this->vaPayment->status = MandiriVaPaymentStatus::SUCCESS;
      $this->vaPayment->save();

      $pipeline = new Pipeline;
      $pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyPaymentStatus'));
      $pipeline->execute([
        'pg_payment_id' => $this->vaPayment->payment->source_id,
        'pg_payment_status' => MandiriVaPaymentStatus::getPaymentGatewayStatus(MandiriVaPaymentStatus::SUCCESS),
        'receive_notify_time' => Carbon::now(),
        'vendor_reference_id' => $this->vaPayment->reference_number,
      ]);

      if ($pipeline->fail()) {
        clog('mandiri_va', 'ID: ' . $this->vaPayment->id . ' Inquiry Status Fail Notify: ' . $pipeline->errorMessage);
        return null;
      }

      dispatch(new Reconciler([
        'bank_inquiry_id' => $bankInquiry->id,
        'pg_payment_id' => $this->vaPayment->payment->source_id,
        'va_payment_id' => $this->vaPayment->id
      ]));
    }

    $this->redis->del($key);
  }

}