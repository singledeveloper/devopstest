<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 10/29/16
 * Time: 9:04 PM
 */

namespace Odeo\Domains\Payment\Doku\DebitOnlineBni;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class Selector implements SelectorListener {

  private $dokuDebitOnlineBniPayment;

  public function __construct() {
    $this->dokuDebitOnlineBniPayment = app()->make(\Odeo\Domains\Payment\Doku\DebitOnlineBni\Repository\PaymentDokuDebitOnlineBniPaymentRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $this->dokuDebitOnlineBniPayment->normalizeFilters($data);

    $dokuDebitOnlineBniPayments = [];

    foreach ($this->dokuDebitOnlineBniPayment->gets() as $dokuDebitOnlineBniPayment) {
      $dokuDebitOnlineBniPayments[] = $this->_transforms($dokuDebitOnlineBniPayment, $this->dokuDebitOnlineBniPayment);
    }

    if (sizeof($dokuDebitOnlineBniPayments) > 0) {
      return $listener->response(200, array_merge(
        ["payments" => $this->_extends($dokuDebitOnlineBniPayments, $this->dokuDebitOnlineBniPayment)],
        $this->dokuDebitOnlineBniPayment->getPagination()
      ));
    }

    return $listener->response(204);

  }
}