<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/25/17
 * Time: 10:34 PM
 */

namespace Odeo\Domains\Payment\Doku\Danamon\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\Danamon\Model\PaymentDokuDanamonInternetBankingPayment;

class PaymentDokuDanamonInternetBankingPaymentRepository extends Repository {

  public function __construct(PaymentDokuDanamonInternetBankingPayment $paymentDokuDanamonInternetBanking) {
    $this->model = $paymentDokuDanamonInternetBanking;
  }

}