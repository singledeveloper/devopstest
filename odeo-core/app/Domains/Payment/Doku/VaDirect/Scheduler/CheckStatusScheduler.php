<?php

namespace Odeo\Domains\Payment\Doku\VaDirect\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Payment\Doku\Vagroup\Repository\PaymentDokuVaPaymentRepository;
use Odeo\Domains\Payment\Doku\VaDirect\Jobs\InquiryStatus;

class CheckStatusScheduler {

  private $dokuVaPayment;

  public function __construct() {
    $this->dokuVaPayment = app()->make(PaymentDokuVaPaymentRepository::class);
  }

  public function run() {
    $pendingPayments = $this->dokuVaPayment->getPendingPayment();
    foreach($pendingPayments as $pendingPayment) {
      dispatch(new InquiryStatus($pendingPayment));
    }
  }

}
