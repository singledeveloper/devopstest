<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 8:03 PM
 */

namespace Odeo\Domains\Payment\Doku\Vagroup\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\Payment;
use Odeo\Domains\Payment\Doku\VaDirect\Model\PaymentDokuVaInquiry;

class PaymentDokuVaPayment extends Entity {

  public function payment() {
    return $this->belongsTo(Payment::class);
  }

  public function inquiry() {
    return $this->belongsTo(PaymentDokuVaInquiry::class);
  }
  
}