<?php

namespace Odeo\Domains\Payment\Doku\Helper;

use Odeo\Domains\Constant\Payment;

class DokuInitializer {

  public static $prePaymentUrl, $paymentUrl, $directPaymentUrl, $generateCodeUrl, $redirectPaymentUrl, $voidPaymentUrl;
  public static $sharedKey, $mallId;

  const CC_CODE = '15';
  const DEBIT_ONLINE_BNI_CODE = '15';
  const MANDIRI_CLICKPAY_CODE = '02';
  const DOKU_WALLET_CODE = '04';
  const BANK_TRANSFER_CODE = '05';
  const SINARMAS_CODE = '21';
  const ALFA_CODE = '14';
  const BCA_CLICKPAY_CODE = '24';
  const BRI_EPAY_CODE = '06';
  const DANAMON_CODE = '26';
  const MUAMALAT_CODE = '25';
  const CIMB_CLICKS = '19';

  private static $paymentValidator;


  public static function init() {

    if (!isProduction()) {

      self::$prePaymentUrl = 'http://staging.doku.com/api/payment/PrePayment';
      self::$paymentUrl = 'http://staging.doku.com/api/payment/paymentMip';
      self::$directPaymentUrl = 'http://staging.doku.com/api/payment/PaymentMIPDirect';
      self::$generateCodeUrl = 'http://staging.doku.com/api/payment/doGeneratePaymentCode';
      self::$redirectPaymentUrl = 'http://staging.doku.com/Suite/Receive';
      self::$voidPaymentUrl = 'http://staging.doku.com/Suite/VoidRequest';

    } else {

      self::$prePaymentUrl = 'https://pay.doku.com/api/payment/PrePayment';
      self::$paymentUrl = 'https://pay.doku.com/api/payment/paymentMip';
      self::$directPaymentUrl = 'https://pay.doku.com/api/payment/PaymentMIPDirect';
      self::$generateCodeUrl = 'https://pay.doku.com/api/payment/doGeneratePaymentCode';
      self::$redirectPaymentUrl = 'https://pay.doku.com/Suite/Receive';
      self::$voidPaymentUrl = 'https://pay.doku.com/Suite/VoidRequest';

    }

  }


  public static function switchAccount($infoId, $opt = []) {

    self::$paymentValidator = app()->make(\Odeo\Domains\Payment\Helper\PaymentValidator::class);

    if (!isProduction()) {
      self::$mallId = '2983';
      self::$sharedKey = 'nbb3Ec1B6S8Y';
      return;
    }

    if (self::$paymentValidator->isDebitOnlineBni($infoId)) {
      self::$mallId = '1901';
      self::$sharedKey = 'nbb3Ec1B6S8Y';
    } else if (in_array($infoId, [
      Payment::OPC_GROUP_MUAMALAT_INTERNET_BANKING,
      Payment::OPC_GROUP_DANAMON_INTERNET_BANKING,
      Payment::OPC_GROUP_BRI_EPAY,
      Payment::OPC_GROUP_CIMB_CLICK
    ])) {
      self::$mallId = '1372';
      self::$sharedKey = '80VrMkzX61Ng';
    } else if (in_array($infoId, [
      Payment::OPC_GROUP_ALFA
    ]) && isset($opt['is_payment_gateway']) && $opt['is_payment_gateway']) {
      self::$mallId = '6900';
      self::$sharedKey = 'nbb3Ec1B6S8Y';
    } else if (self::$paymentValidator->isCcInstallment($infoId)) {
      if (isset($opt['bank']) && $opt['bank'] == 'mandiri') {
        self::$mallId = '1372';
        self::$sharedKey = '80VrMkzX61Ng';
      } else {
        self::$mallId = '1390';
        self::$sharedKey = 'JVrCZkXH0urp';
      }
    } else if (self::$paymentValidator->isDokuVaDirect($infoId)) {
      self::$mallId = '2476';
      self::$sharedKey = 'nbb3Ec1B6S8Y';
    } else {
      self::$mallId = '1332';
      self::$sharedKey = 'nbb3Ec1B6S8Y';
    }

  }


}
