<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/14/17
 * Time: 11:34 PM
 */

namespace Odeo\Domains\Payment\Doku\Briepay;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Notifier extends DokuManager {

  private $briEpay;

  public function __construct() {
    parent::__construct();
    $this->briEpay = app()->make(\Odeo\Domains\Payment\Doku\Briepay\Repository\PaymentDokuBriEpayPaymentRepository::class);

  }

  public function notified(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    $this->initializer->switchAccount($paymentInformation->info_id);

    $dokuPayment = $this->dokuPayments->findById($payment->reference_id);

    $words = sha1($dokuPayment->amount . $dokuPayment->mall_id . $this->library->getSharedKey($dokuPayment->mall_id) . $order->id . $data['RESULTMSG'] . $data['VERIFYSTATUS']);

    if (!$dokuPayment || $dokuPayment->amount != $data['AMOUNT'] || $words != $data['WORDS']) {

      return $listener->response(400, null);

    }

    $briEpay = $this->briEpay->getNew();

    $briEpay->purchase_currency = $data['PURCHASECURRENCY'];
    $briEpay->payment_date_time = $data['PAYMENTDATETIME'];
    $briEpay->liability = $data['LIABILITY'];
    $briEpay->payment_channel = $data['PAYMENTCHANNEL'];
    $briEpay->payment_code = $data['PAYMENTCODE'];
    $briEpay->mcn = $data['MCN'];
    $briEpay->result_msg = $data['RESULTMSG'];
    $briEpay->verify_id = $data['VERIFYID'];
    $briEpay->bank = $data['BANK'];
    $briEpay->status_type = $data['STATUSTYPE'];
    $briEpay->approval_code = $data['APPROVALCODE'];
    $briEpay->edu_status = $data['EDUSTATUS'];
    $briEpay->secured_3ds_status = $data['THREEDSECURESTATUS'];
    $briEpay->verify_score = $data['VERIFYSCORE'];
    $briEpay->response_code = $data['RESPONSECODE'];
    $briEpay->verify_status = $data['VERIFYSTATUS'];
    $briEpay->session_id = $data['SESSIONID'];

    $this->briEpay->save($briEpay);

    $dokuPayment->reference_id = $briEpay->id;

    $this->dokuPayments->save($dokuPayment);


    if ($data['RESPONSECODE'] != '0000') {

      return $listener->response(400, null);

    }

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $fee = $data['AMOUNT'] - $order->total;

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    $order->total = $order->total + $fee;

    $this->orders->save($order);

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200);

  }

}