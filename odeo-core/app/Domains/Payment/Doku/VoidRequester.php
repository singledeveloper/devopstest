<?php

namespace Odeo\Domains\Payment\Doku;


use GuzzleHttp\Client;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class VoidRequester extends DokuManager {

  private $paymentDokuPayments, $paymentDokuVoidLogs;

  public function __construct() {
    parent::__construct();

    $this->paymentDokuPayments = app()->make(\Odeo\Domains\Payment\Doku\Repository\PaymentDokuPaymentRepository::class);
    $this->paymentDokuVoidLogs = app()->make(\Odeo\Domains\Payment\Doku\Repository\PaymentDokuVoidLogRepository::class);
  }

  public function void($data) {

    $client = new Client();
    if (!isset($data['session_id'])) {
      $data['session_id'] = $this->paymentDokuPayments->findById($data['payment_reference_id'])->session_id;
    }

    if (!DokuInitializer::$mallId) $this->initializer->switchAccount($data['payment_info_id']);

    $params = [
      'MALLID' => DokuInitializer::$mallId,
      'CHAINMERCHANT' => 'NA',
      'TRANSIDMERCHANT' => $data['order_id'],
      'SESSIONID' => $data['session_id'],
      'WORDS' => sha1(DokuInitializer::$mallId . DokuInitializer::$sharedKey . $data['order_id'] . $data['session_id']),
      'PAYMENTCHANNEL' => $data['payment_channel_code']
    ];

    $res = $client->post(DokuInitializer::$voidPaymentUrl, [
      'form_params' => $params
    ]);

    $paymentLog = $this->paymentDokuVoidLogs->getNew();

    $paymentLog->reference_type = $data['void_type'] ?? 'order';
    $paymentLog->reference_id = $data['reference_id'] ?? $data['order_id'];
    $paymentLog->log_request = json_encode($params);
    $paymentLog->log_response = json_encode($res->getBody());
    $paymentLog->voided = $res->getBody()->getContents() == "SUCCESS";
    $this->paymentDokuVoidLogs->save($paymentLog);

    return $paymentLog->voided;

  }

}