<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/2/16
 * Time: 11:40 PM
 */

namespace Odeo\Domains\Payment\Kredivo;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;

class Selector {

  private $kredivo;

  public function __construct() {
    $this->kredivo = app()->make(\Odeo\Domains\Payment\Kredivo\Repository\PaymentKredivoPaymentRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $this->kredivo->normalizeFilters($data);

    $r = [];

    foreach ($this->kredivo->gets() as $kredivo) {
      $r[] = $this->_transforms($kredivo, $this->kredivo);
    }

    if (sizeof($r) > 0) {
      return $listener->response(200, array_merge(
        ["payments" => $this->_extends($r, $this->kredivo)],
        $this->kredivo->getPagination()
      ));
    }

    return $listener->response(204, ["payments" => []]);

  }

}