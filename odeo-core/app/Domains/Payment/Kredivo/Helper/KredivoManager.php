<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:50 PM
 */

namespace Odeo\Domains\Payment\Kredivo\Helper;


use Odeo\Domains\Payment\Helper\PaymentManager;

class KredivoManager extends PaymentManager {

  protected $api, $kredivoPayments, $kredivoPaymentDetails;

  public function __construct() {
    parent::__construct();

    $this->api = app()->make(KredivoApi::class);
    $this->kredivoPayments = app()->make(\Odeo\Domains\Payment\Kredivo\Repository\PaymentKredivoPaymentRepository::class);
    $this->kredivoPaymentDetails = app()->make(\Odeo\Domains\Payment\Kredivo\Repository\PaymentKredivoPaymentDetailRepository::class);

    KredivoApi::init();

  }
}