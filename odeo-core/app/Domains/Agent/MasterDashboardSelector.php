<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/8/17
 * Time: 10:39 PM
 */

namespace Odeo\Domains\Agent;

use Odeo\Domains\Core\PipelineListener;

class MasterDashboardSelector {

  private $agentSelector, $agents, $stores, $masterSettingRetriever;

  public function __construct() {
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->agents = app()->make(\Odeo\Domains\Agent\Repository\AgentRepository::class);
    $this->agentSelector = app()->make(AgentSelector::class);
    $this->masterSettingRetriever = app()->make(\Odeo\Domains\Agent\Helper\MasterSettingRetriever::class);
  }

  public function get(PipelineListener $listener, $data) {

    $lang = app('translator');

    $result['allow'] = true;
    $result['master_code'] = $this->stores->findById($data['store_id'])->subdomain_name;
    $result['description'] = $lang->has('agent.description_agent_feature') ? trans('agent.description_agent_feature') : '';
    $result['recent_request'] = [];
    $result['agents'] = [];

    $masterSetting = $this->masterSettingRetriever->retrieve($data['store_id']);
    $result['auto_approve_agent'] = $masterSetting->auto_approve_agent;

    return $listener->response(200, $result, false, true);

  }

}