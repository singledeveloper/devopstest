<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/7/17
 * Time: 3:28 PM
 */

namespace Odeo\Domains\Agent\Repository;

use Odeo\Domains\Agent\Model\AgentHistory;
use Odeo\Domains\Core\Repository;

class AgentHistoryRepository extends Repository {

  public function __construct(AgentHistory $agentHistory) {
    $this->model = $agentHistory;
  }


}