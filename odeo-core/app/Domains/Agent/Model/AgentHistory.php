<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/7/17
 * Time: 3:28 PM
 */

namespace Odeo\Domains\Agent\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;

class AgentHistory extends Entity {

  public function agent() {
    return $this->belongsTo(Agent::class);
  }

  public function referredUser() {
    return $this->belongsTo(User::class, 'user_referred_id');
  }

  public function referredStore() {
    return $this->belongsTo(Store::class);
  }

}