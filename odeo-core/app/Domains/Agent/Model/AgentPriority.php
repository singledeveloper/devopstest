<?php

namespace Odeo\Domains\Agent\Model;


use Odeo\Domains\Core\Entity;

class AgentPriority extends Entity {

  public function agents() {
    return $this->belongsTo(Agent::class, 'agent_id');
  }

}