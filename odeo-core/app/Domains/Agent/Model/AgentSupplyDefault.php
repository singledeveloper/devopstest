<?php

namespace Odeo\Domains\Agent\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;

class AgentSupplyDefault extends Entity {

  public function store() {
    return $this->belongsTo(Store::class);
  }

}