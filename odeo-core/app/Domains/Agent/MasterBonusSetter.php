<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 5:50 PM
 */

namespace Odeo\Domains\Agent;


use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Core\PipelineListener;

class MasterBonusSetter {

  private $masterSettings;

  public function __construct() {
    $this->masterSettings = app()->make(\Odeo\Domains\Agent\Repository\StoreAgentMasterSettingRepository::class);
  }

  public function get(PipelineListener $listener, $data) {

    if ($setting = $this->masterSettings->findByStoreId($data['store_id'])) {

      return $listener->response(200, [
        'bonus' => $setting->bonus,
        'min_bonus' => 0,
        'max_bonus' => AgentConfig::MAX_BONUS,
        'step' => AgentConfig::STEP_BONUS
      ]);
    }

    return $listener->response(200, [
      'bonus' => 0,
      'min_bonus' => 0,
      'max_bonus' => AgentConfig::MAX_BONUS,
      'step' => AgentConfig::STEP_BONUS
    ]);

  }

  public function set(PipelineListener $listener, $data) {

    return $listener->response(400, "Fitur ini tidak dapat digunakan. Hubungi kami untuk informasi lebih lanjut.");
    if ($setting = $this->masterSettings->findByStoreId($data['store_id']));
    else {
      $setting = $this->masterSettings->getNew();
      $setting->store_id = $data['store_id'];
    }

    $setting->bonus = min(max(0, $data['bonus']), AgentConfig::MAX_BONUS);

    $this->masterSettings->save($setting);

    return $listener->response(200);

  }


}