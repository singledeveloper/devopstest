<?php

namespace Odeo\Domains\Agent;

use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Agent\Repository\AgentPriorityRepository;

class AgentPriorityUpdater {

  private $agentPriorities;

  public function __construct() {
    $this->agentPriorities = app()->make(AgentPriorityRepository::class);
  }

  public function updatePrioritiesByServiceId(PipelineListener $listener, $data) {
    $selectedPriorityId = $data['selected_priority_id'];

    $updatedAgentPriorities = array();
    $now = Carbon::now();

    $agentPriorities = $this->agentPriorities->getPreferredPrioritesExceptIds(
      $data['auth']['user_id'], 
      $data['service_id'], 
      $data['ids']
    );

    // Update removed priority
    foreach($agentPriorities as $agentPriority) {
      $id = $agentPriority->id;
      $updatedAgentPriorities[] = [
        'id' => $id,
        'priority' => 0,
        'updated_at' => $now,
        'is_selected' => false,
      ];
    }

    // Set new priority
    foreach($data['ids'] as $index => $id) {
      $updatedAgentPriorities[] = [
        'id' => $id,
        'priority' => $index + 1,
        'updated_at' => $now,
        'is_selected' => $selectedPriorityId === $id
      ];
    }
    if (sizeof($updatedAgentPriorities) > 0) {
      $this->agentPriorities->updateBulk($updatedAgentPriorities);
    }

    return $listener->response(200);
  }

}
