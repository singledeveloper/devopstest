<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/5/17
 * Time: 15:30
 */

namespace Odeo\Domains\Agent\Helper;


class MasterSettingRetriever {

  private $masterSettings;

  public function __construct() {
    $this->masterSettings = app()->make(\Odeo\Domains\Agent\Repository\StoreAgentMasterSettingRepository::class);
  }

  public function retrieve($storeId) {
    if ($setting = $this->masterSettings->findByStoreId($storeId)) ;
    else {
      $setting = $this->masterSettings->getNew();
      $setting->store_id = $storeId;
      $setting->bonus = 0;
      $setting->auto_approve_agent = true;
      $this->masterSettings->save($setting);
    }
    return $setting;
  }
}