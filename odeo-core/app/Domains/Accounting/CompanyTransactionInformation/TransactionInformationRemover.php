<?php

namespace Odeo\Domains\Accounting\CompanyTransactionInformation;

use Odeo\Domains\Accounting\CompanyTransactionInformation\Repository\CompanyTransactionAdditionalInformationRepository;
use Odeo\Domains\Core\PipelineListener;

class TransactionInformationRemover {

  public function remove(PipelineListener $listener, $data) {
    $inquiryFileImages = app()->make(CompanyTransactionAdditionalInformationRepository::class);

    $deleted = $inquiryFileImages->getModel()
      ->where('id', $data['id'])
      ->delete();

    return $listener->response($deleted ? 200 : 400);

  }

}