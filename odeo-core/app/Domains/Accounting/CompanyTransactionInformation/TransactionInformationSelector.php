<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 13/06/18
 * Time: 16.16
 */

namespace Odeo\Domains\Accounting\CompanyTransactionInformation;

use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class TransactionInformationSelector implements SelectorListener {

  private $cashTransactionRepo, $cashTransactionInformationRepo, $companyAccountRepo, $userRepo;

  public function __construct() {
    $this->cashTransactionRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->cashTransactionInformationRepo = app()->make(\Odeo\Domains\Accounting\CompanyTransactionInformation\Repository\CompanyTransactionAdditionalInformationRepository::class);
    $this->companyAccountRepo = app()->make(\Odeo\Domains\Payment\Odeo\CompanyCash\Repository\CompanyCashAccountRepository::class);
    $this->userRepo = app()->make(UserRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    return [
      'id' => $item->id,
      'type' => $item->type,
      'image_url' => isset($item->image_url) ? AwsConfig::url($item->image_url) : ''
    ];
  }

  public function getTransactionInformation(PipelineListener $listener, $data) {

    $this->cashTransactionInformationRepo->normalizeFilters($data);
    $this->cashTransactionInformationRepo->setSimplePaginate(true);

    $informationLists = array();

    if (!$transaction = $this->cashTransactionRepo->findById($data['cash_transaction_id'])) {
      return $listener->response(204);
    }

    if (!isAdmin() && $transaction->user_id != $data['auth']['user_id']) {
      return $listener->response(204);
    }

    $user = $this->userRepo->findById($transaction->user_id);

    $description = json_decode($transaction->data);
    foreach ($this->cashTransactionInformationRepo->gets($data['cash_transaction_id']) as $info) {
      $informationLists[] = $this->_transforms($info, $this->cashTransactionInformationRepo);
    }

    return $listener->response(200, array_merge(
      [
        'company_name' => $user ? $user->name : '',
        'description' => $description && isset($description->notes) ? $description->notes : '',
        'transaction_information_details' => $this->_extends($informationLists, $this->cashTransactionInformationRepo)
      ],
      $this->cashTransactionInformationRepo->getPagination()
    ));
  }
}