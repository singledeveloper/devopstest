<?php

namespace Odeo\Domains\Accounting\CompanyTransactionInformation;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Odeo\Domains\Core\PipelineListener;

class TransactionInformationUpdater {

  private $cashTransactionRepo, $cashTransactionInformationRepo;

  public function __construct() {
    $this->cashTransactionRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->cashTransactionInformationRepo = app()->make(\Odeo\Domains\Accounting\CompanyTransactionInformation\Repository\CompanyTransactionAdditionalInformationRepository::class);
  }

  public function updateTransactionInformation(PipelineListener $listener, $data) {

    $now = Carbon::now();

    if (!$cashTransaction = $this->cashTransactionRepo->findById($data['transaction_id'])) {
      return $listener->response(400, 'Transaction not exists');
    }

    if (!isAdmin() && $cashTransaction->user_id != $data['auth']['user_id']) {
      return $listener->response(400, 'No access');
    }

    if (isset($data['description']) && $data['description'] != '') {
      $cashData = json_decode($cashTransaction->data, true);
      if (!$cashData) $cashData = [];
      $cashData['notes'] = $data['description'];
      $cashTransaction->data = json_encode($cashData);
    }

    $this->cashTransactionRepo->save($cashTransaction);

    if (isset($data['files'])) {
      $informationFileImages = array();

      foreach ($data['files'] as $file) {
        $informationFileImages[] = [
          'cash_transaction_id' => $cashTransaction->id,
          'image_url' => $file['file_url'],
          'type' => $file['type'],
          'created_at' => $now,
          'updated_at' => $now
        ];
      }

      $this->cashTransactionInformationRepo->saveBulk($informationFileImages);
    }

    if (isset($data['file'])) {
      $url = (isProduction() ? 'transaction-info' : 'temp_transaction') . '/' . $data['auth']['user_id'] . '_' . $data['type'] . '_' . time() . '.' . $data['file']->getClientOriginalExtension();
      if (!Storage::disk('s3')->put($url, file_get_contents($data['file']), 'private')) {
        return $listener->response(400, 'Fail to upload the file. Please try again');
      }
      else {
        $information = $this->cashTransactionInformationRepo->getNew();
        $information->cash_transaction_id = $cashTransaction->id;
        $information->image_url = $url;
        $information->type = $data['type'];
        $this->cashTransactionInformationRepo->save($information);
      }
    }

    return $listener->response(200, [
      'cash_transaction_id' => $cashTransaction->id
    ]);
  }

}