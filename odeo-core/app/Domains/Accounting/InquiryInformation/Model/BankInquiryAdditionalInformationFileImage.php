<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/12/17
 * Time: 11.52
 */

namespace Odeo\Domains\Accounting\InquiryInformation\Model;


use Odeo\Domains\Core\Entity;

class BankInquiryAdditionalInformationFileImage extends Entity {

  public function inquiryInformation() {
    return $this->belongsTo(BankInquiryAdditionalInformation::class);
  }

}