<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/01/18
 * Time: 12.08
 */

namespace Odeo\Domains\Accounting\Xero\Helper;


use Carbon\Carbon;
use Odeo\Domains\Accounting\Xero\Job\PostManualJournal;
use Odeo\Domains\Core\PipelineListener;

class JournalManager {

  private static $maxManualJournalCount = 50;
  private $narration, $date, $type,
    $counter = 0, $isDifferentJournal = true,
    $manualJournals = array(), $journalLines = array(), $reffIds = array(),
    $journalLogs = array(), $refundOrder = array();
  protected $planPromotionMultiplier = 10000, $planReferralBonus = 200000;

  public function __construct() {
    $this->journalLineLogger = app(\Odeo\Domains\Accounting\Xero\Repository\ManualJournalLoggerRepository::class);
  }

  public function setType($type) {
    $this->type = $type;
  }

  public function isRefundOrder($data) {
    if ($data->total_cash == 0 && $data->total_deposit == 0 && $data->total_marketing == 0 && $data->total_mutation == 0 ) {
      $this->refundOrder[] = $data->order_id;
      return true;
    }
    return false;
  }

  public function setManualJournalProperties(PipelineListener $listener, $narration, $date, $id) {
    if ($this->narration != $narration . ' ' . $id) {
      $this->appendManualJournal();
      $this->isDifferentJournal = true;

      if ($this->isMaxManualJournal()) {
        $this->queueManualJournals($listener);
      }
    } else {
      $this->isDifferentJournal = false;
    }
    $this->narration = $narration . ' ' . $id;
    $this->date = $date;
    $this->reffIds[] = (string)$id;
  }

  public function finalizeSummarizedJournal(PipelineListener $listener, $narration, $date) {
    if (count($this->journalLines) > 0) {
      $this->manualJournals[] = XeroJsonFormatter::generateManualJournal($date, $narration, $this->journalLines);
      $this->queueManualJournals($listener);
    }
  }

  private function appendManualJournal() {
    if (count($this->journalLines) > 0) {
      $this->manualJournals[] = XeroJsonFormatter::generateManualJournal($this->date, $this->narration, $this->journalLines);
      $this->journalLines = array();
      $this->counter++;
    }
  }

  public function isDifferentJournal() {
    return $this->isDifferentJournal;
  }

  public function addJournalLines($lines) {
    foreach ($lines as $code => $amount) {
      if (is_array($amount) && count($amount)) {
        foreach ($amount as $value) {
          $this->addJournalLine($code, $value);
        }
      } else {
        if(isset($code) && $code != '') {
          $this->addJournalLine($code, $amount);
        }
      }
    }
  }

  public function addJournalLine($accountCode, $amount) {
    if (isset($amount) && $amount != 0 && $amount != '' && $amount != '-0') {
      $this->journalLines[] = XeroJsonFormatter::generateJournalLine($accountCode, $amount);
    }
  }

  public function isMaxManualJournal() {
    return $this->counter == self::$maxManualJournalCount;
  }

  private function queueManualJournals(PipelineListener $listener) {
    $date = Carbon::now();
    $journalLogs = [
      'reference_ids' => json_encode($this->reffIds),
      'manual_journal_xml' => XeroJsonFormatter::wrapManualJournals($this->manualJournals),
      'status' => JournalHelper::STATUS_PROCESSING,
      'type' => $this->type,
      'created_at' => $date,
      'updated_at' => $date
    ];
    $this->journalLogs[] = $journalLogs;
    $this->manualJournals = array();
    $this->reffIds = array();
    $this->counter = 0;
    $listener->pushQueue(new PostManualJournal($journalLogs['manual_journal_xml'], $journalLogs['reference_ids'], $journalLogs['type']));
  }

  public function finalizeJournalLogs(PipelineListener $listener) {
    if (count($this->journalLines) > 0 || count($this->manualJournals) > 0) {
      $this->appendManualJournal();
      $this->queueManualJournals($listener);
    }

//    $this->journalLineLogger->saveBulk($this->journalLogs);
    $this->journalLogs = array();
    $this->refundOrder = array();
  }

}