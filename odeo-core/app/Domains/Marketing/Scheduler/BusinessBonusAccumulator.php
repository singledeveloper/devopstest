<?php

namespace Odeo\Domains\Marketing\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;

class BusinessBonusAccumulator {

  private $apiDisbursementRepo, $apiDisbursementInquiryRepo, $pgPaymentRepo, $withdrawRepo, $businessTransactionRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(\Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository::class);
    $this->apiDisbursementInquiryRepo = app()->make(\Odeo\Domains\Disbursement\Repository\ApiDisbursementInquiryRepository::class);
    $this->pgPaymentRepo = app()->make(\Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository::class);
    $this->withdrawRepo = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->businessTransactionRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerTransactionRepository::class);
  }

  public function setDataToBeSaved($item) {
    return [
      'user_id' => $item->user_id,
      'payment_group_id' => $item->payment_group_id ? $item->payment_group_id : null,
      'transaction_date' => $item->transaction_date,
      'type' => $item->type,
      'qty' => $item->qty,
      'total_amount' => round($item->total_amount, 0),
      'total_fee' => round($item->total_fee, 0),
      'total_cost' => round($item->total_cost, 0),
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ];
  }

  public function run($date = '') {
    if ($date == '') $date = Carbon::now()->subDay(1)->format('Y-m-d');

    $apiDisbursementQuery = $this->apiDisbursementRepo->getCloneModel()
      ->where(\DB::raw('date(verified_at)'), $date)
      ->where('status', ApiDisbursement::COMPLETED)
      ->whereNull('batch_disbursement_id')
      ->select(
        'user_id',
        \DB::raw("'" . ForBusiness::TRANSACTION_DISBURSEMENT . "' as type"),
        \DB::raw('date(verified_at) as transaction_date'),
        \DB::raw("0 as payment_group_id"),
        \DB::raw('sum(amount) as total_amount'),
        \DB::raw('sum(fee) as total_fee'),
        \DB::raw('sum(cost) as total_cost'),
        \DB::raw('count(id) as qty')
      )->groupBy('user_id', \DB::raw('date(verified_at)'))->havingRaw('sum(fee) > 0');


    $apiDisbursementBatchQuery = $this->apiDisbursementRepo->getCloneModel()
      ->where(\DB::raw('date(verified_at)'), $date)
      ->where('status', ApiDisbursement::COMPLETED)
      ->whereNotNull('batch_disbursement_id')
      ->select(
        'user_id',
        \DB::raw("'" . ForBusiness::TRANSACTION_DISBURSEMENT_BATCH . "' as type"),
        \DB::raw('date(verified_at) as transaction_date'),
        \DB::raw("0 as payment_group_id"),
        \DB::raw('sum(amount) as total_amount'),
        \DB::raw('sum(fee) as total_fee'),
        \DB::raw('sum(cost) as total_cost'),
        \DB::raw('count(id) as qty')
      )->groupBy('user_id', \DB::raw('date(verified_at)'))->havingRaw('sum(fee) > 0');

    $apiDisbursementInquiryQuery = $this->apiDisbursementInquiryRepo->getCloneModel()
      ->where(\DB::raw('date(created_at)'), $date)
      ->whereIn('status', ApiDisbursement::NON_FREE_INQUIRY_STATUS)
      ->select(
        'user_id',
        \DB::raw("'" . ForBusiness::TRANSACTION_DISBURSEMENT_INQUIRY . "' as type"),
        \DB::raw('date(created_at) as transaction_date'),
        \DB::raw("0 as payment_group_id"),
        \DB::raw('0 as total_amount'),
        \DB::raw('sum(fee) as total_fee'),
        \DB::raw('sum(cost) as total_cost'),
        \DB::raw('count(id) as qty')
      )->groupBy('user_id', \DB::raw('date(created_at)'))->havingRaw('sum(fee) > 0');

    $withdrawQuery = $this->withdrawRepo->getCloneModel()
      ->where(\DB::raw('date(verified_at)'), $date)
      ->where('status', Withdraw::COMPLETED)
      ->select(
        'user_id',
        \DB::raw("'" . ForBusiness::TRANSACTION_WITHDRAW . "' as type"),
        \DB::raw('date(verified_at) as transaction_date'),
        \DB::raw("0 as payment_group_id"),
        \DB::raw('sum(amount) as total_amount'),
        \DB::raw('sum(fee) as total_fee'),
        \DB::raw('sum(cost) as total_cost'),
        \DB::raw('count(id) as qty')
      )->groupBy('user_id', \DB::raw('date(verified_at)'))->havingRaw('sum(fee) > 0');

    $result = $this->pgPaymentRepo->getCloneModel()
      ->join('payment_gateway_users', 'payment_gateway_payments.pg_user_id', '=', 'payment_gateway_users.id')
      ->where(\DB::raw('date(payment_gateway_payments.reconciled_at)'), $date)
      ->where('status', PaymentGateway::SUCCESS)
      ->select(
        'payment_gateway_users.user_id',
        \DB::raw("'" . ForBusiness::TRANSACTION_PAYMENT_GATEWAY . "' as type"),
        \DB::raw('date(payment_gateway_payments.created_at) as transaction_date'),
        'payment_group_id',
        \DB::raw('sum(amount) as total_amount'),
        \DB::raw('sum(fee) as total_fee'),
        \DB::raw('sum(payment_gateway_payments.cost) as total_cost'),
        \DB::raw('count(payment_gateway_payments.id) as qty')
      )->groupBy('payment_gateway_users.user_id', \DB::raw('date(payment_gateway_payments.created_at)'), 'payment_group_id')
      ->havingRaw('sum(fee) > 0')
      ->union($apiDisbursementQuery)
      ->union($apiDisbursementBatchQuery)
      ->union($apiDisbursementInquiryQuery)
      ->union($withdrawQuery)
      ->orderBy('user_id', 'asc')
      ->get();

    $savedData = $this->businessTransactionRepo->getByTransactionDate($date);
    $dataToBeSaved = [];
    if (count($savedData) <= 0) {
      foreach ($result as $item) {
        $dataToBeSaved[] = $this->setDataToBeSaved($item);
      }
    }
    else {
      $temp = [];
      foreach ($savedData as $item) {
        $pgGroupId = $item->payment_group_id ? $item->payment_group_id : 0;
        $temp[$item->user_id . '_' . $item->type . '_' . $pgGroupId] = $item;
      }
      foreach ($result as $item) {
        if (isset($temp[$item->user_id . '_' . $item->type . '_' . $item->payment_group_id])) {
          $detail = $temp[$item->user_id . '_' . $item->type . '_' . $item->payment_group_id];
          $isNeedChange = $detail->qty != $item->qty;
          $detail->qty = $item->qty;
          $detail->total_amount = round($item->total_amount, 0);
          $isNeedChange = $isNeedChange || $detail->total_fee != round($item->total_fee, 0);
          $detail->total_fee = round($item->total_fee, 0);
          $detail->total_cost = round($item->total_cost, 0);
          $detail->is_processed = !$isNeedChange;
          $this->businessTransactionRepo->save($detail);
        }
        else {
          $dataToBeSaved[] = $this->setDataToBeSaved($item);
        }
      }
    }

    if (count($dataToBeSaved) > 0) {
      $this->businessTransactionRepo->saveBulk($dataToBeSaved);
    }

    return $result;
  }
}
