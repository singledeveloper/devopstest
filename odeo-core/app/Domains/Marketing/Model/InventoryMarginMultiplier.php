<?php

namespace Odeo\Domains\Marketing\Model;

use Odeo\Domains\Core\Entity;

class InventoryMarginMultiplier extends Entity
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];

    protected $fillable = ['service_detail_id', 'margin', 'multiplier'];

    public $timestamps = false;

    protected $primaryKey = 'service_detail_id';

    public static function boot() {
      parent::boot();

      static::creating(function($model) {
        $timestamp = $model->freshTimestamp();
        $model->setCreatedAt($timestamp);
      });
    }

}
