<?php

namespace Odeo\Domains\Marketing\Model;

use Odeo\Domains\Core\Entity;

class IncomeOutcome extends Entity {

  protected $primaryKey = "date";

  protected $fillable = ['date', 'income', 'outcome'];

	public $timestamps = false;

  public $incrementing = false;
}
