<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/2/17
 * Time: 11:32 PM
 */

namespace Odeo\Domains\Marketing\Userreferral;


use Carbon\Carbon;
use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;

class UserReferralRequester {

  private $users, $userReferralCodes, $agents;

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->userReferralCodes = app()->make(\Odeo\Domains\Marketing\Userreferral\Repository\UserReferralCodeRepository::class);
    $this->agents = app()->make(\Odeo\Domains\Agent\Repository\AgentRepository::class);
  }

  public function getReferralCode(PipelineListener $listener, $data) {


    $this->userReferralCodes = app()->make(\Odeo\Domains\Marketing\Userreferral\Repository\UserReferralCodeRepository::class);

    //$user = $this->users->findById($data['auth']['user_id']);

    //$code = strtolower(substr($user->telephone, -4, 4) . base_convert($data['auth']['user_id'], 10, 36));

    $userReferred = $this->userReferralCodes->findByUserId($data['auth']['user_id']);

    return $listener->response(200, [
      'user_referral_code' => $userReferred ? $userReferred->referral_code : '-',
      'redeemed' => !!$userReferred
    ]);
  }

  public function deleteMasterCode($data) {


    $this->userReferralCodes = app()->make(\Odeo\Domains\Marketing\Userreferral\Repository\UserReferralCodeRepository::class);

    $userReferred = $this->userReferralCodes->findByUserId($data['user_id']);

    $this->userReferralCodes->delete($userReferred);


  }


  public function redeemReferralCode(PipelineListener $listener, $data) {


    if (strlen($data['referral_code']) < 4) {
      return $listener->response(400, 'Invalid master code');
    }

    $stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $referredStore = $stores->findBySubdomain(strtolower($data["referral_code"]));
    if ($referredStore && StoreStatus::isActive($referredStore->status)) {

      $user = $referredStore->owner->first();

      $userReferralCode = $this->userReferralCodes->getNew();
      $userReferralCode->user_id = $data['auth']['user_id'];
      $userReferralCode->user_referred_id = $user->id;
      $userReferralCode->store_referred_id = $referredStore->id;
      $userReferralCode->referral_code = $data['referral_code'];
      $this->userReferralCodes->save($userReferralCode);

      $me = $this->users->findById($data['auth']['user_id']);
      $me->purchase_preferred_store_id = $referredStore->id;
      $this->users->save($me);

      $agent = $this->agents->getNew();

      $agent->user_id = $data['auth']['user_id'];
      $agent->user_referred_id = $user->id;
      $agent->store_referred_id = $referredStore->id;
      $agent->status = AgentConfig::ACCEPTED;
      $agent->requested_at = datenow();
      $agent->joined_at = datenow();
      $agent->inputted_master_code = $data['referral_code'];

      $this->agents->save($agent);



      return $listener->response(200);

    }
    else {
      return $listener->response(400, 'Invalid master code');
    }


    if (in_array($data['referral_code'], [
        '922140c'
      ]) === false
    ) {
      return $listener->response(400, 'this feature is currently down for maintenance');
    }


    $code = strtolower($data['referral_code']);

    $phoneNumber = substr($code, 0, 4);
    $userId = base_convert(substr($code, 4), 36, 10);

    $user = $this->users->findById($userId);

    if ($userId == $data['auth']['user_id']) {
      return $listener->response(400, "You can't use your own invitation code.");
    } else if (!$user || substr($user->telephone, -4, 4) != $phoneNumber) {
      return $listener->response(400, 'Invalid invitation code');
    }

//    if (count($this->userReferralCodes->getUserReferredIdByDate($user->id, Carbon::now()->toDateString())) >= 1) {
//      return $listener->response(400, 'max invitation exceeded');
//    } else
    if (count($this->userReferralCodes->getIdentical($data['auth']['user_id'], $user->id)) > 0) {
      return $listener->response(400, 'you had redeemed this invitation code before');
    }

    $userReferralCode = $this->userReferralCodes->getNew();
    $userReferralCode->user_id = $data['auth']['user_id'];
    $userReferralCode->user_referred_id = $user->id;
    $userReferralCode->referral_code = $data['referral_code'];
    $this->userReferralCodes->save($userReferralCode);

    $inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);

    $inserter->add([
      'user_id' => $data['auth']['user_id'],
      'trx_type' => TransactionType::REFERRAL_CODE_REDEEM,
      'cash_type' => CashType::OCASH,
      'amount' => 1000,
      'data' => json_encode([
        'user_referral_code_id' => $userReferralCode->id,
      ])
    ]);

    $inserter->add([
      'user_id' => $user->id,
      'trx_type' => TransactionType::SHARE_REFERRAL_CODE,
      'cash_type' => CashType::OCASH,
      'amount' => 1000,
      'data' => json_encode([
        'user_referral_code_id' => $userReferralCode->id,
      ])
    ]);

    $inserter->run();

    $notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);

    $notification->setup($data['auth']['user_id'], NotificationType::CHECK, NotificationGroup::ACCOUNT);
    $notification->redeemUserReferralCode(1000);

    $notification->setup($user->id, NotificationType::CHECK, NotificationGroup::ACCOUNT);
    $notification->shareUserReferralCode(1000);

    $listener->pushQueue($notification->queue());

    return $listener->response(200);
  }

}
