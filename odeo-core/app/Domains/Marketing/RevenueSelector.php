<?php

namespace Odeo\Domains\Marketing;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Order\Jobs\SendStoreOrderReport;


class RevenueSelector implements SelectorListener {

  private $orders, $currencyHelper, $orderDetailizer, $payments, $paymentOdeoBankTransferDetails, $statusParser;

  public function __construct() {
    $this->revenues = app()->make(\Odeo\Domains\Marketing\Repository\StoreRevenueRepository::class);
    $this->cashRevenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($revenue, Repository $repository) {
    $filters = $repository->getFilters();

    $output = [];

    $output['total_revenue'] = $this->currency->formatPrice($revenue->total_revenue);
    $output['total_profit'] = $this->currency->formatPrice($revenue->total_profit);
    $output['total_order'] = intval($revenue->total_order);

    return $output;
  }

  public function getDetail(PipelineListener $listener, $data) {
    $revenues = [];

    $storeId = $data['search']['store_id'];
    $lifetimeRevenue = $this->revenues->findStoreLifetimeTotal($storeId);
    $storeRevenues = $this->revenues->getDetails($storeId, $data['dates']);


    if (isset($data['sum_with_cash']) && $data['sum_with_cash']) {
      $lifeTimeCashRevenue = $this->cashRevenue->getStoreLifetimeTotal($storeId);
      $lifetimeRevenue->lifetime_profit += $lifeTimeCashRevenue->total_amount;
      $lifetimeRevenue->lifetime_order += intval($lifeTimeCashRevenue->total_order);
    }

    $revenues['lifetime_revenue'] = $this->currency->formatPrice($lifetimeRevenue->lifetime_revenue);
    $revenues['lifetime_profit'] = $this->currency->formatPrice($lifetimeRevenue->lifetime_profit);
    $revenues['lifetime_order'] = intval($lifetimeRevenue->lifetime_order);

    $revenues['revenues'] = [];

    foreach($data['dates'] as $date) {
      $revenues['revenues'][$date] = [
        'total_revenue' => $this->currency->formatPrice(0),
        'total_profit' => $this->currency->formatPrice(0),
        'total_order' => 0
      ];
    }

    foreach ($storeRevenues as $storeRevenue) {
      $revenues['revenues'][$storeRevenue->date->format('Y-m-d')] = $this->_transforms($storeRevenue, $this->revenues);
    }

    return $listener->response(200, $revenues);
  }

  public function export(PipelineListener $listener, $data) {
    $revenues = [];
    $filters = ['start_date' => $data['start_date'], 'end_date' => $data['end_date']];
    $storeId = $data['search']['store_id'];
    $storeRevenues = $this->revenues->exportDetails($storeId, $data['dates'], $filters);
    foreach ($storeRevenues as $storeRevenue) {
      $temp['date'] = $storeRevenue->date->format('Y-m-d');
      $temp['revenue'] = intval($storeRevenue->total_revenue);
      $temp['profit'] = intval($storeRevenue->total_profit);
      $revenues[] = $temp;
    }
    $listener->pushQueue(new SendStoreOrderReport($data['orders'], $revenues, $filters, $data['email']));
    return $listener->response(200);
  }

}
