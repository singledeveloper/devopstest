<?php

namespace Odeo\Domains\Marketing\Commands;

use Illuminate\Console\Command;
use Odeo\Domains\Marketing\Scheduler\BudgetRefresher;


class RefreshMultiplier extends Command {

  protected $signature = 'marketing:refresh-multiplier';

  protected $description = 'Refresh inventory margin multiplier';


  public function __construct() {
    parent::__construct();
  }


  public function handle() {
    (new BudgetRefresher)->refreshMultiplier(app()->make(\Odeo\Domains\Core\Pipeline::class), []);
  }
}
