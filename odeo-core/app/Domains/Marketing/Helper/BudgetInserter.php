<?php

namespace Odeo\Domains\Marketing\Helper;

use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;

class BudgetInserter {

  public function __construct() {
    $this->userCash = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->revenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
    $this->depositRecords = app()->make(\Odeo\Domains\Transaction\Repository\DepositRecordRepository::class);
  }

  public function budgetData($amount, $userId, $storeId, $additional = []) {

    $orushAmount = floor($amount * 0.5);
    $ocashbackAmount = floor($amount * 0.5);

    $rate = floor($this->depositRecords->getMinOfLastTwoRecords($storeId) * 0.00031);

    $data = [];

    if ($orushAmount > 0) {
      $data[] = [
        'user_id' => $userId,
        'store_id' => $storeId,
        'trx_type' => TransactionType::MARKETING_BUDGET,
        'cash_type' => CashType::ORUSH,
        'amount' => $orushAmount,
        'data' => '',
      ];
    }

    if ($ocashbackAmount > 0) {
      $data[] = [
        'user_id' => $userId,
        'store_id' => $storeId,
        'trx_type' => TransactionType::MARKETING_BUDGET,
        'cash_type' => CashType::OCASHBACK,
        'amount' => $ocashbackAmount,
        'data' => json_encode(["store_id" => $storeId]),
      ];
    }

    if ($rate > 0) {

      $type = 'deposit_cashback';

      $data[] = [
        'user_id' => $userId,
        'store_id' => $storeId,
        'trx_type' => TransactionType::COMPOUND_INTEREST,
        'cash_type' => CashType::OCASHBACK,
        'amount' => $rate,
        'data' => json_encode([
          "store_id" => $storeId,
          'type' => $type
        ]),
      ];
    }

    return $data;
  }

  public function convertBudget($userId, $storeId, $trxType, $cashType, $amount, $data, $orderId = NULL, $insertRevenue = true) {

    if (!is_array($cashType)) $cashType = [$cashType];

    $totalInserted = 0;
    $amount = round($amount);

    $this->cashInserter->clear();

    foreach ($cashType as $type) {
      if (is_null($storeId)) {
        $cashBalance = $this->userCash->findByUserId($userId, $type);
      } else {
        $cashBalance = $this->userCash->findByStoreId($userId, $storeId, $type);
      }

      if (!$cashBalance) return 0;

      //temp
      $ocashBalance = $this->userCash->findByUserId($userId, 'cash');

      if ($ocashBalance->amount > floor($ocashBalance->amount)) {
        $amount += 1 - ($ocashBalance->amount - floor($ocashBalance->amount));
      }
      //temp

      $insertedAmount = min(floor($cashBalance->amount), $amount);

      if ($insertedAmount == 0) continue;

      $trxData = $data;

      if (isset($data['settlement_at'])) unset($data['settlement_at']);

      $this->cashInserter->add([
        'user_id' => $userId,
        'store_id' => $storeId,
        'trx_type' => $trxType,
        'cash_type' => $type,
        'amount' => -$insertedAmount,
        'data' => json_encode($data),
      ]);

      $totalInserted += $insertedAmount;

      if ($insertedAmount == $amount) break;
      else {
        $amount -= $insertedAmount;
      }

    }

    if ($totalInserted == 0) return 0;

    $this->cashInserter->add([
      'user_id' => $userId,
      'store_id' => $storeId,
      'trx_type' => $trxType,
      'cash_type' => CashType::OCASH,
      'amount' => $totalInserted,
      'data' => json_encode($trxData),
      'settlement_at' => isset($trxData['settlement_at']) ? $trxData['settlement_at'] : NULL
    ]);

    try {
      $this->cashInserter->run();
    } catch (\Exception $e) {
      return 0;
    }

    if ($insertRevenue) {
      $this->revenue->save([
        'user_id' => $userId,
        'store_id' => $storeId,
        'order_id' => $orderId,
        'amount' => $totalInserted,
        'trx_type' => $trxType,
        'data' => json_encode($data),
      ]);
    }

    return $totalInserted;
  }
}
