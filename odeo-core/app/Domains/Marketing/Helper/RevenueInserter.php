<?php

namespace Odeo\Domains\Marketing\Helper;

use Odeo\Domains\Constant\CashType;
use Carbon\Carbon;
use DB;

class RevenueInserter {

  private $rows = [];

  public function __construct() {
    $this->storeRevenue = app()->make(\Odeo\Domains\Marketing\Repository\StoreRevenueRepository::class);
  }

  public function add($row) {
    $this->rows[] = $row;
  }

  public function clear() {
    $this->rows = [];
  }

  public function run() {
    DB::beginTransaction();
    $this->storeRevenue->lock();
    try {
      foreach ($this->rows as $row) {
        $latestRevenue = $this->storeRevenue->findLatestByStore($row['store_id'], $row['service_id']);

        if(!$latestRevenue) {
          $revenue = $this->storeRevenue->getNew();
          $revenue->store_id = $row['store_id'];
          $revenue->service_id = $row['service_id'];
          $revenue->date = Carbon::now()->toDateString();
        } else {
          if ($latestRevenue->date->lt(Carbon::today())) {
            $revenue = $latestRevenue->replicate();
            $revenue->amount = 0;
            $revenue->amount_profit = 0;
            $revenue->amount_rush = 0;
            $revenue->amount_free = 0;
            $revenue->amount_profit_free = 0;
            $revenue->order_count = 0;
            $revenue->date = Carbon::now()->toDateString();
          } else {
            $revenue = $latestRevenue;
          }
        }

        $revenue->amount += $row['amount'];
        $revenue->total += $row['amount'];
        $revenue->amount_profit += $row['margin'];
        $revenue->total_profit += $row['margin'];
        $revenue->amount_rush += $row['rush'];
        $revenue->total_rush += $row['rush'];
        $revenue->order_count += $row['count'] ?? 1;
        $revenue->total_order_count += $row['count'] ?? 1;

        if($row['is_free']) {
          $revenue->amount_free += $row['amount'];
          $revenue->total_free += $row['amount'];
          $revenue->amount_profit_free += $row['margin'];
          $revenue->total_profit_free += $row['margin'];
        }

        $this->storeRevenue->save($revenue);
      }
    } catch (\Exception $e) {
      DB::rollback();
      throw $e;
    }
    DB::commit();
  }
}
