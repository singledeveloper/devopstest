<?php

namespace Odeo\Domains\Marketing\Competitor;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository;
use Odeo\Domains\Marketing\Competitor\Repository\CompetitorPriceRepository;

class CompetitorPriceSelector implements SelectorListener {

  private $prices, $pulsas;

  public function __construct() {
    $this->prices = app()->make(CompetitorPriceRepository::class);
    $this->pulsas = app()->make(PulsaOdeoRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $competitorPrice = [];

    if ($item->id) $competitorPrice["competitor_price_id"] = $item->id;
    if ($item->competitor) $competitorPrice["competitor_name"] = $item->competitor;
    if ($item->name) $competitorPrice["pulsa_odeo_name"] = $item->name;
    if ($item->price) $competitorPrice["price"] = $item->price;
    if ($item->created_at) $competitorPrice["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if ($item->updated_at) $competitorPrice["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");

    return $competitorPrice;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->prices->normalizeFilters($data);
    $this->prices->setSimplePaginate(true);

    $competitorPrices = [];
    foreach ($this->prices->gets() as $item) {
      $competitorPrices[] = $this->_transforms($item, $this->prices);
    }

    if (sizeof($competitorPrices) > 0) {
      return $listener->response(200, array_merge(
        ["competitor_prices" => $this->_extends($competitorPrices, $this->prices)],
        $this->prices->getPagination()
      ));
    }
    return $listener->response(204, ["competitor_prices" => []]);
  }

  public function getAllNames(PipelineListener $listener, $data) {
    $competitorNames = $this->prices->getCompetitorNames();
    if (sizeof($competitorNames) > 0) {
      return $listener->response(200,
        ["competitor_names" => $this->_extends($competitorNames, $this->prices)]
      );
    }
    return $listener->response(204, ["competitor_names" => []]);
  }

  public function getPriceComparison(PipelineListener $listener, $data) {
    $this->pulsas->normalizeFilters($data);
    $this->pulsas->setSimplePaginate(true);
    $pulsaOdeos = $this->pulsas->getAllWithPagination(true);
    $competitorPrices = $this->prices->getAllCompetitorLatestPrice($pulsaOdeos->pluck('id')->all())
      ->groupBy('pulsa_odeo_id');

    $response = [];
    foreach ($pulsaOdeos as $pulsaOdeo) {
      $responseDetail = [];
      $responseDetail['name'] = $pulsaOdeo->name;
      $responseDetail['prices'] = collect([
        [
          'competitor' => 'ODEO',
          'price' => $pulsaOdeo->price,
          'updated_at' => '-'
        ]
      ]);

      if (isset($competitorPrices[$pulsaOdeo->id])) {
        $responseDetail['prices'] = $responseDetail['prices']->merge($competitorPrices[$pulsaOdeo->id]);
      }

      // William: It is possible to have at least 2 "seller" ( including ODEO ) selling same item with same lowest or highest price.
      // To handle that, we store the lowest and highest price, instead of index of lowest or highest.
      $responseDetail['highest_price'] = $responseDetail['prices']->max('price');
      $responseDetail['lowest_price'] = $responseDetail['prices']->min('price');

      $response[] = $responseDetail;
    }

    if (sizeof($response) > 0) {
      return $listener->response(200, array_merge(
        ["compare_prices" => $this->_extends($response, $this->pulsas)],
        $this->pulsas->getPagination()
      ));
    }
    return $listener->response(204, ["compare_prices" => []]);
  }

}
