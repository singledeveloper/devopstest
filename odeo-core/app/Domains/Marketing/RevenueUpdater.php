<?php

namespace Odeo\Domains\Marketing;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;

class RevenueUpdater {

  public function __construct() {
    $this->redis = Redis::connection();
    $this->revenueInserter = app()->make(\Odeo\Domains\Marketing\Helper\RevenueInserter::class);
    $this->storeRevenues = app()->make(\Odeo\Domains\Marketing\Repository\StoreRevenueRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->parser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->serviceDetail = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->bonusCalculator = app()->make(\Odeo\Domains\Order\Helper\OrderBonusCalculator::class);
  }

  public function updateRevenue(PipelineListener $listener, $data) {

    $order = $this->orders->findById($data['order_id']);
    if (!$order || !isset($data['tree_data'])) return $listener->response(200);

    if (($user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class)->findById($order->user_id)) && $order->actual_store_id == $user->purchase_preferred_store_id) {
      $isFree = false;
    } else {
      $isFree = substr($order->gateway_id, -2) == '00' && substr($order->gateway_id, 0, 1) != '2';
    }

    $rushBonusData = $data['rush_bonus_inserted'];
    $orderBonus = $data['order_bonus'];

    foreach ($order->details as $item) {
      $serviceDetail = $this->serviceDetail->findById($item->service_detail_id);
      $serviceId = $serviceDetail->service_id;

      $exact = $data['tree_data'];
      foreach ($exact as $node => $nodeData) {
        if (!$exact[$node]) continue;
        if ($node == 'additionals') {
          foreach ($nodeData as $nodeDetail) {
            $this->insertRevenue($nodeDetail['store_id'], $serviceId, $item->service_detail_id, $item->sale_price,
              $orderBonus['default'], $rushBonusData['additionals'][$nodeDetail['store_id']] ?? 0, true, true);
          }
        } else {
          $this->insertRevenue($nodeData['store_id'], $serviceId, $item->service_detail_id, $item->sale_price,
            $orderBonus[$node], $rushBonusData[$node], $isFree || ($nodeData['is_community'] ?? false)
          );
        }
      }
      $this->revenueInserter->run();
    }

    return $listener->response(200);
  }

  private function insertRevenue($storeId, $serviceId, $serviceDetailId, $amount, $margin, $rush, $isFree, $dataOnly = false) {
    $this->revenueInserter->add([
      'store_id' => $storeId,
      'service_id' => $serviceId,
      'amount' => $amount,
      'margin' => $margin,
      'rush' => $rush,
      'is_free' => $isFree
    ]);

    if ($isFree && !$dataOnly) {
      $profitQueue = 'odeo_core:store_profit_queue:' . $serviceId;
      $key = $storeId . ':' . $serviceDetailId;
      $score = $this->redis->zscore($profitQueue, $key);
      $incAmount = $margin;
      if (isset($score)) {
        if ($score < 0) $incAmount += abs($score);
        $this->redis->zincrby($profitQueue, $incAmount, $key);
      } else {
        if (($val = $this->redis->hget('odeo_core:store_profit_temp', $key)) !== null) {
          if ($val < 0) $incAmount += abs($val);
          $this->redis->hincrby('odeo_core:store_profit_temp', $key, (int)$incAmount);
        }
      }
    }
  }

  public function initRevenue(PipelineListener $listener, $data) {
    $store = $this->store->findById($data["store_id"]);
    $currentData = json_decode($this->parser->currentData($store->plan_id, $store->current_data), true);
    $todayRevenues = [];

    foreach ($currentData as $serviceName => $serviceDetailId) {
      $serviceId = constant('Odeo\Domains\Constant\Service::' . strtoupper($serviceName));
      if ($serviceDetailId > 0) {
        $latestRevenue = $this->storeRevenues->findLatestByStore($store->id, $serviceId);

        if (isset($data['marketplace']) && $data['marketplace']) {
          if ($this->redis->zadd('odeo_core:store_profit_queue:' . $serviceId, 'nx', 0, $store->id . ":" . $serviceDetailId)) {
            $this->redis->hdel('odeo_core:store_profit_idx_queue', $serviceId);
          }
        }

        if (!$latestRevenue) {
          $revenue = $this->storeRevenues->getNew();
          $revenue->store_id = $store->id;
          $revenue->service_id = $serviceId;
          $revenue->date = Carbon::now()->toDateString();
        } else {
          if ($latestRevenue->date->lt(Carbon::today())) {
            $revenue = $latestRevenue->replicate();
            $revenue->amount = 0;
            $revenue->amount_profit = 0;
            $revenue->amount_rush = 0;
            $revenue->amount_free = 0;
            $revenue->amount_profit_free = 0;
            $revenue->order_count = 0;
            $revenue->date = Carbon::now()->toDateString();
          } else {
            continue;
          }
        }
        $todayRevenues[] = $revenue->attributesToArray();

      }
    }
    $this->storeRevenues->saveBulk($todayRevenues);

    return $listener->response(200);
  }

  public function recalculateRevenue(PipelineListener $listener, $data) {
    $revenues = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class)->getModel();
    $storeId = $data['store_id'];

    $revenues = $revenues->select(\DB::raw('*, date(created_at) as date'))->where('store_id', $storeId)->where('created_at', '>=', $data['date'])->whereNotNull('order_id')->orderBy('id', 'ASC')->get();
    $orderIds = $revenues->pluck('order_id')->toArray();
    $orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $serviceDetailr = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $serviceDetails = $serviceDetailr->getAll()->keyBy('id');
    $storeOrders = $orders->getModel()->whereIn('id', $orderIds)->with('details')->get()->keyBy('id');
    $storeRevenueRepository = app()->make(\Odeo\Domains\Marketing\Repository\StoreRevenueRepository::class);
    $users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);

    $currentOrderId = 0;
    $storeRevenues = array();
    foreach ($revenues as $revenue) {
      if ($revenue->order_id != $currentOrderId && $revenue->trx_type != TransactionType::SUBSCRIPTION_REVENUE) {
        $currentOrderId = $revenue->order_id;
        $order = $storeOrders[$currentOrderId];
        foreach ($order->details as $detail) {
          $serviceId = $serviceDetails[$detail->service_detail_id]->service_id;
        }
        if (($user = $users->findById($order->user_id)) && $order->actual_store_id == $user->purchase_preferred_store_id) {
          $free = '';
        } else {
          $free = substr($order->gateway_id, -2) == '00' && substr($order->gateway_id, 0, 1) != '2' ? '_free' : '';
        }
        $storeRevenues[$revenue->date][$serviceId]['order_count'] = ($storeRevenues[$revenue->date][$serviceId]['order_count'] ?? 0) + 1;
      }
      switch ($revenue->trx_type) {
        case TransactionType::ORDER_REVENUE:
        case TransactionType::MENTOR_ORDER_REVENUE:
        case TransactionType::LEADER_ORDER_REVENUE:
        case TransactionType::AGENT_ORDER_REVENUE:
        case TransactionType::PAYMENT:
          $storeRevenues[$revenue->date][$serviceId]['amount' . $free] = ($storeRevenues[$revenue->date][$serviceId]['amount' . $free] ?? 0) + $order->subtotal;
          break;
        case TransactionType::ORDER_BONUS:
        case TransactionType::MENTOR_ORDER_BONUS:
        case TransactionType::LEADER_ORDER_BONUS:
        case TransactionType::AGENT_ORDER_BONUS:
          $storeRevenues[$revenue->date][$serviceId]['profit' . $free] = ($storeRevenues[$revenue->date][$serviceId]['profit' . $free] ?? 0) + $revenue->amount;
          break;
        case TransactionType::RUSH_BONUS:
        case TransactionType::MENTOR_RUSH_BONUS:
        case TransactionType::LEADER_RUSH_BONUS:
          $storeRevenues[$revenue->date][$serviceId]['rush'] = ($storeRevenues[$revenue->date][$serviceId]['rush'] ?? 0) + $revenue->amount;
          break;
        default:
          break;
      }
    }
    $total = array();

    foreach ($storeRevenues as $date => $storeRevenue) {
      foreach ($storeRevenue as $serviceId => $revenue) {
        $updated = $storeRevenueRepository->getModel()->where('store_id', $storeId)->where('service_id', $serviceId)->where('date', $date)->first();
        if (!isset($total[$serviceId])) {
          $previous = $storeRevenueRepository->getCloneModel()->where('store_id', $storeId)->where('service_id', $serviceId)->where('date', '<', $data['date'])->orderBy('date', 'desc')->first();
          if ($previous) {
            $total[$serviceId]['total'] = $previous->total;
            $total[$serviceId]['total_free'] = $previous->total_free;
            $total[$serviceId]['total_profit'] = $previous->total_profit;
            $total[$serviceId]['total_profit_free'] = $previous->total_profit_free;
            $total[$serviceId]['total_rush'] = $previous->total_rush;
            $total[$serviceId]['total_order_count'] = $previous->total_order_count;
          }
        }
        $total[$serviceId]['total'] = ($total[$serviceId]['total'] ?? 0) + ($revenue['amount'] ?? 0) + ($revenue['amount_free'] ?? 0);
        $total[$serviceId]['total_free'] = ($total[$serviceId]['total_free'] ?? 0) + ($revenue['amount_free'] ?? 0);
        $total[$serviceId]['total_profit'] = ($total[$serviceId]['total_profit'] ?? 0) + ($revenue['profit'] ?? 0) + ($revenue['profit_free'] ?? 0);
        $total[$serviceId]['total_profit_free'] = ($total[$serviceId]['total_profit'] ?? 0) + ($revenue['profit'] ?? 0) + ($revenue['profit_free'] ?? 0);
        $total[$serviceId]['total_rush'] = ($total[$serviceId]['total_rush'] ?? 0) + ($revenue['rush'] ?? 0);
        $total[$serviceId]['total_order_count'] = ($total[$serviceId]['total_order_count'] ?? 0) + ($revenue['order_count'] ?? 0);

        if (!$updated) {
          $updated = new \Odeo\Domains\Marketing\Model\StoreRevenue;
          $updated->store_id = $storeId;
          $updated->service_id = $serviceId;
          $updated->date = $date;
        }

        $updated->amount = ($revenue['amount'] ?? 0) + ($revenue['amount_free'] ?? 0);
        $updated->total = $total[$serviceId]['total'];
        $updated->amount_free = ($revenue['amount_free'] ?? 0);
        $updated->total_free = $total[$serviceId]['total_free'];
        $updated->amount_profit = ($revenue['profit'] ?? 0) + ($revenue['profit_free'] ?? 0);
        $updated->total_profit = $total[$serviceId]['total_profit'];
        $updated->amount_profit_free = ($revenue['profit_free'] ?? 0);
        $updated->total_profit_free = $total[$serviceId]['total_profit_free'];
        $updated->amount_rush = ($revenue['rush'] ?? 0);
        $updated->total_rush = $total[$serviceId]['total_rush'];
        $updated->order_count = ($revenue['order_count'] ?? 0);
        $updated->total_order_count = $total[$serviceId]['total_order_count'];

        $storeRevenueRepository->save($updated);
      }
    }
  }
}
