<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\IncomeOutcome;
use Carbon\Carbon;

class IncomeOutcomeRepository extends Repository {

  public function __construct(IncomeOutcome $incomeOutcome) {
    $this->model = $incomeOutcome;
  }

  public function today() {
    return $this->model->firstOrNew(['date' => date('Y-m-d')]);
  }

  public function findLastNDay($dayNum) {
    $dateEnd = Carbon::today();
    $dateStart = Carbon::today()->subDays($dayNum);
    return $this->model->where('date', '>=', $dateStart)->where('date', '<=', $dateEnd)->get();
  }
}
