<?php

namespace Odeo\Domains\Marketing\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Jobs\Job;

class PopulateRevenueQueue extends Job  {

  public function __construct() {
    parent::__construct();
  }

  public function handle() {
    $this->redis = Redis::connection();
    $this->services = app()->make(\Odeo\Domains\Inventory\Repository\ServiceRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->storeRevenues = app()->make(\Odeo\Domains\Marketing\Repository\StoreRevenueRepository::class);
    $this->parser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $services = $this->services->getAll()->pluck('name', 'id')->toArray();
    $activeStores = $this->stores->getAllActiveStoreWithoutFreeStore(['distribution']);

    $this->redis->set('odeo_core:redispatching_queue', 1);

    foreach ($services as $serviceId => $serviceName) {
      $this->redis->del('odeo_core:store_profit_queue:' . $serviceId);
    }

    if ($activeStores) {
      $distChannel = $inventories = array();
      foreach ($activeStores as $activeStore) {
        $inventories[$activeStore->id] = json_decode($this->parser->currentData($activeStore->plan_id, $activeStore->current_data), true);
        $distChannel[$activeStore->id]['marketplace'] = $activeStore->distribution->marketplace;
      }

      $storeRevenues = $this->storeRevenues->findLatest($activeStores->pluck('id')->toArray());

      $todayRevenues = [];
      $maxProfit = array();
      foreach ($storeRevenues as $storeRevenue) {
        $storeId = $storeRevenue->store_id;
        $serviceId = $storeRevenue->service_id;
        if (!isset($inventories[$storeId][$services[$serviceId]])) continue;
        $serviceDetailId = $inventories[$storeId][$services[$serviceId]];
        unset($inventories[$storeId][$services[$serviceId]]);
        $isTodayRevenue = $storeRevenue->date->eq(Carbon::today());

        if (!$isTodayRevenue) {
          $todayRevenue = [];
          $todayRevenue['store_id'] = $storeId;
          $todayRevenue['service_id'] = $serviceId;
          $todayRevenue['total'] = $storeRevenue->total;
          $todayRevenue['total_free'] = $storeRevenue->total_free;
          $todayRevenue['total_profit'] = $storeRevenue->total_profit;
          $todayRevenue['total_profit_free'] = $storeRevenue->total_profit_free;
          $todayRevenue['total_rush'] = $storeRevenue->total_rush;
          $todayRevenue['total_order_count'] = $storeRevenue->total_order_count;
          $todayRevenue['date'] = Carbon::today();
          $todayRevenues[] = $todayRevenue;

          if (!isset($maxProfit[$serviceId])) {
            $maxProfit[$serviceId] = $storeRevenue->max_profit;
          }
          $score = $storeRevenue->total_profit_free - $maxProfit[$serviceId];
        } else {
          if (!isset($maxProfit[$serviceId])) {
            $maxProfit[$serviceId] = $storeRevenue->max_profit;
          }
          if ($storeRevenue->amount_profit_free > 0) {
            $score = $storeRevenue->amount_profit_free;
          } else {
            $score = $storeRevenue->total_profit_free - $maxProfit[$serviceId];
          }
        }

        if ($serviceDetailId > 0 && $distChannel[$storeId]['marketplace']) {
          $this->redis->zadd('odeo_core:store_profit_queue:' . $serviceId, $score, $storeId . ":" . $serviceDetailId);
        }
      }
      foreach ($inventories as $storeId => $currentData) {
        foreach ($currentData as $serviceName => $serviceDetailId) {
          $serviceId = constant('Odeo\Domains\Constant\Service::' . strtoupper($serviceName));
          if ($serviceDetailId > 0) {
            $todayRevenue = [];
            $todayRevenue['store_id'] = $storeId;
            $todayRevenue['service_id'] = $serviceId;
            $todayRevenue['total'] = 0;
            $todayRevenue['total_free'] = 0;
            $todayRevenue['total_profit'] = 0;
            $todayRevenue['total_profit_free'] = 0;
            $todayRevenue['total_rush'] = 0;
            $todayRevenue['total_order_count'] = 0;
            $todayRevenue['date'] = Carbon::today();
            $todayRevenues[] = $todayRevenue;
          }
          $score = isset($maxProfit[$serviceId]) ? -$maxProfit[$serviceId] : 0;
          if ($serviceDetailId > 0 && $distChannel[$storeId]['marketplace']) {
            $this->redis->zadd('odeo_core:store_profit_queue:' . $serviceId, $score, $storeId . ":" . $serviceDetailId);
          }
        }
      }
      $this->storeRevenues->saveBulk($todayRevenues);
    }
    $this->redis->del('odeo_core:redispatching_queue');
  }
}
