<?php

namespace Odeo\Domains\Affiliate\Formatter\ApiType;

use Odeo\Domains\Affiliate\Formatter\Contract\ApiTypeContract;
use Odeo\Domains\Constant\Supplier;

class AffiliateXmlRpcFormatter implements ApiTypeContract {

  public function deconstructRequest() {
    $typeManager = app()->make(\Odeo\Domains\Supply\Formatter\TypeManager::class);
    $formatter = $typeManager->setPath(Supplier::TYPE_XMLRPC);
    if ($result = $formatter->deconstructResponse($formatter->getRequestData(app()->make(\Illuminate\Http\Request::class)))) {
      $newResult = [];
      foreach ($result as $key => $item) {
        $newResult[strtolower($key)] = $item;
      }
      return $newResult;
    }
    return $result;
  }

  public function constructNotify($array, $code = Supplier::RC_OK) {
    return $this->constructResponse($array, $code);
  }

  public function constructResponse($array, $code = Supplier::RC_OK) {
    $array = array_merge([
      'RESPONSECODE' => $code
    ], $array);

    $xml = '<?xml version="1.0"?><methodResponse><params><param><value><struct>';
    foreach ($array as $key => $val) {
      if (is_array($val)) {
        $temp = [];
        foreach($val as $key2 => $val2) {
          $temp[] = $key2 . '=' . $val2;
        }
        $val = implode(';', $temp);
      }
      $xml .= '<member><name>' . strtoupper($key) . '</name><value><string>' . $val . '</string></value></member>';
    }
    $xml .= '</struct></value></param></params></methodResponse>';
    return $xml;
  }

}
