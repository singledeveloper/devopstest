<?php

namespace Odeo\Domains\Affiliate\Formatter\Contract;

interface ApiTypeContract {

  public function deconstructRequest();

  public function constructNotify($array, $code);

  public function constructResponse($array, $code);

}