<?php

namespace Odeo\Domains\Affiliate\Repository;

use Odeo\Domains\Affiliate\Model\AffiliateOrder;
use Odeo\Domains\Core\Repository;

class AffiliateOrderRepository extends Repository {

  public function __construct(AffiliateOrder $affiliateOrder) {
    $this->model = $affiliateOrder;
  }

  public function findByTrxId($trxId, $userId) {
    return $this->model->where('trx_id', $trxId)
      ->where('user_id', $userId)->whereNotNull('order_id')->first();
  }

}