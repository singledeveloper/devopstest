<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\TransactionType;

class RevenueSelector implements SelectorListener {

  public function __construct() {
    $this->revenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
  }
  
  public function _transforms($item, Repository $repository) {
    return $item;
  }
  
  public function _extends($data, Repository $repository) {
    return $data;
  }
  
  public function get(PipelineListener $listener, $data) {
    $this->revenue->normalizeFilters($data);
  
    $revenues = [];
    $storeIds = [];
    $todays = [];
    foreach($this->revenue->getUserDetailedToday() as $item) {
      if (!in_array($item->store_id, $storeIds)) {
        $storeIds[] = $item->store_id;
      }
      if (!isset($todays[$item->store_id])) {
        $todays[$item->store_id] = [];
      }
      $todays[$item->store_id][$item->trx_type] = $item->total_amount;
    }
    
    if (sizeof($storeIds) > 0) {
      $bonus = $this->cashManager->getStoreTodayProfit($storeIds, "", [TransactionType::DIRECT_REFERRAL, TransactionType::SPONSOR_BONUS, TransactionType::TEAM_COMMISSION]);
      $network = $this->store->findByStoreIds($storeIds, ['plan']);
      $stores = [];
      foreach ($network as $item) {
        $stores[$item->id]["name"] = $item->name;
        $stores[$item->id]["plan_name"] = $item->plan->name;
        $stores[$item->id]["plan_color"] = $item->plan->color;
      }
      
      foreach ($todays as $key => $item) {
        $row["store"] = $stores[$key];
        
        $hustler_store_profit = 0;
        $mentor_store_profit = 0;
        $leader_store_profit = 0;
        $hustler_store_revenue = 0;
        $mentor_store_revenue = 0;
        $leader_store_revenue = 0;
  
        if (isset($item[TransactionType::ORDER_BONUS])) {
          $hustler_store_profit += $item[TransactionType::ORDER_BONUS];
        }
        if (isset($item[TransactionType::ORDER_REVENUE])) {
          $hustler_store_revenue += $item[TransactionType::ORDER_REVENUE];
        }
        if (isset($item[TransactionType::RUSH_BONUS])) {
          $hustler_store_profit += $item[TransactionType::RUSH_BONUS];
        }
        if (isset($item[TransactionType::ADS_CLICK])) {
          $hustler_store_profit += $item[TransactionType::ADS_CLICK];
          $hustler_store_revenue += $item[TransactionType::ADS_CLICK];
        }
        if (isset($item[TransactionType::PAYMENT])) {
          $hustler_store_profit += $item[TransactionType::PAYMENT];
          $hustler_store_revenue += $item[TransactionType::PAYMENT];
        }
        if (isset($item[TransactionType::MENTOR_ORDER_REVENUE])) {
          $mentor_store_revenue += $item[TransactionType::MENTOR_ORDER_REVENUE];
        }
        if (isset($item[TransactionType::MENTOR_ORDER_BONUS])) {
          $mentor_store_profit += $item[TransactionType::MENTOR_ORDER_BONUS];
        }
        if (isset($item[TransactionType::MENTOR_RUSH_BONUS])) {
          $mentor_store_profit += $item[TransactionType::MENTOR_RUSH_BONUS];
        }
        if (isset($item[TransactionType::LEADER_ORDER_REVENUE])) {
          $leader_store_revenue += $item[TransactionType::LEADER_ORDER_REVENUE];
        }
        if (isset($item[TransactionType::LEADER_ORDER_BONUS])) {
          $leader_store_profit += $item[TransactionType::LEADER_ORDER_BONUS];
        }
        if (isset($item[TransactionType::LEADER_RUSH_BONUS])) {
          $leader_store_profit += $item[TransactionType::LEADER_RUSH_BONUS];
        }
        
        $list = [];
        $list[] = [
          "type" => "Franchise Revenue",
          "price" => $this->currency->formatPrice(isset($item[TransactionType::SUBSCRIPTION_REVENUE]) ? $item[TransactionType::SUBSCRIPTION_REVENUE] : 0)
        ];
        
        $list[] = ["type" => "Franchise Bonus", "price" => $this->currency->formatPrice(isset($bonus[$key]) ? $bonus[$key] : 0)];
        $list[] = ["type" => "Hustler Store Revenue", "price" => $this->currency->formatPrice($hustler_store_revenue)];
        $list[] = ["type" => "Hustler Store Profit", "price" => $this->currency->formatPrice($hustler_store_profit)];
        $list[] = ["type" => "Mentor Store Revenue", "price" => $this->currency->formatPrice($mentor_store_revenue)];
        $list[] = ["type" => "Mentor Store Profit", "price" => $this->currency->formatPrice($mentor_store_profit)];
        $list[] = ["type" => "Leader Store Revenue", "price" => $this->currency->formatPrice($leader_store_revenue)];
        $list[] = ["type" => "Leader Store Profit", "price" => $this->currency->formatPrice($leader_store_profit)];
  
        $row["details"] = $list;
        $revenues[] = $row;
      }
    }
    
    if (sizeof($revenues) > 0)
      return $listener->response(200, $revenues);
    
    return $listener->response(204);
  }
}
