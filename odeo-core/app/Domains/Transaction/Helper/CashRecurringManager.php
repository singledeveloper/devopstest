<?php

namespace Odeo\Domains\Transaction\Helper;

use Odeo\Domains\Constant\CashConfig;

class CashRecurringManager {

  private $cashRecurring;

  public function __construct() {
    $this->cashRecurring = app()->make(\Odeo\Domains\Transaction\Repository\CashRecurringRepository::class);
  }

  public function check($data) {
    // return consist of 3 params,
    // param 1 is error or not, param 2 is continue or not,
    // param 3 is error message when param 1 is true or additional data when param 2 is false
    if (isset($data['transaction_type'])) {
      if ($data['transaction_type'] == CashConfig::TRANSACTION_IMMEDIATE) {
        return [false, true, ''];
      }
      else if (in_array($data['transaction_type'], [CashConfig::TRANSACTION_ONCE, CashConfig::TRANSACTION_RECURRING])) {
        try {
          if ($data['transaction_type'] == CashConfig::TRANSACTION_ONCE) {
            return [false, false, CashConfig::TRANSACTION_ONCE . '_' . $data['recurring_date'] . '_' . $data['recurring_time']];
          }
          else if ($data['transaction_type'] == CashConfig::TRANSACTION_RECURRING) {
            return [false, false, $data['recurring_type'] . ($data['recurring_type'] == 'every_month' ? ('_' . $data['recurring_month']) : '') . '_' . $data['recurring_time']];
          }
        }
        catch (\Exception $e) {}
      }
      return [true, false, 'Invalid data'];
    }
    return [false, true, ''];
  }

}
