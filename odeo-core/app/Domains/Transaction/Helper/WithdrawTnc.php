<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/6/17
 * Time: 8:22 PM
 */

namespace Odeo\Domains\Transaction\Helper;


class WithdrawTnc {

  public function getTnc($isBankTransfer = false) {
    return [
      [
        '1_text' => trans('withdraw.tnc_withdraw_1', [
          'process' => $isBankTransfer ? trans('withdraw.transfer'): trans('withdraw.withdraw')
        ]),
      ],
    ];
  }

}