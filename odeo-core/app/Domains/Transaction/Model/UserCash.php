<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Core\Entity;

class UserCash extends Entity
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['user_id', 'store_id', 'cash_type'];

    public $timestamps = true;

}
