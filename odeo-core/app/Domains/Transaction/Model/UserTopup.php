<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class UserTopup extends Entity {
  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = ['created_at', 'updated_at'];
  
  public $timestamps = true;
  
  public function user() {
    return $this->belongsTo(User::class);
  }
}
