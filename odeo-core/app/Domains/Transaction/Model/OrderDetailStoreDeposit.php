<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Core\Entity;

class OrderDetailStoreDeposit extends Entity
{
  protected $dates = ['created_at', 'updated_at'];
}
