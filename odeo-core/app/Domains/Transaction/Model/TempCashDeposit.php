<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;

class TempCashDeposit extends Entity
{
  public function store() {
    return $this->belongsTo(Store::class);
  }
}