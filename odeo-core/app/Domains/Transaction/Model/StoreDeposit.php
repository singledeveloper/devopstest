<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;

class StoreDeposit extends Entity {

  protected $dates = ['created_at', 'updated_at'];
  
  public $timestamps = true;
  
  public function store() {
    return $this->belongsTo(Store::class);
  }
}
