<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Transaction\Model\CashTransaction;

class UserTempCash extends Entity
{

  public $fillable = [
    'user_id',
    'cash_transaction_id',
    'order_id',
    'amount',
    'settlement_at'
  ];

  public $timestamps = false;

  public static function boot() {
    parent::boot();

    static::creating(function($model) {
      $timestamp = $model->freshTimestamp();
      $model->setCreatedAt($timestamp);
    });
  }

  public function cashTransaction() {
    return $this->belongsTo(CashTransaction::class, 'cash_transaction_id');
  }
}
