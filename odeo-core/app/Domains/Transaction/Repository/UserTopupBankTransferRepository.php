<?php

namespace Odeo\Domains\Transaction\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\UserTopupBankTransfer;

class UserTopupBankTransferRepository extends Repository {

  public function __construct(UserTopupBankTransfer $userTopupBankTransfer) {
    $this->model = $userTopupBankTransfer;
  }

  public function findDifferent($userId, $bankId, $bankAccountUserName) {
    return $this->model->where('user_id', $userId)
      ->where('created_at', '>', Carbon::now()->subDays(5)->toDateTimeString())
      ->where(function($query2) use ($bankId, $bankAccountUserName){
        $query2->where('from_bank_id', '<>', $bankId)
          ->orWhere(\DB::raw('lower(from_bank_account_name)'), '<>', strtolower($bankAccountUserName));
      })->first();
  }

  public function findSame($userId, $bankId, $bankAccountUserName) {
    return $this->model->where('user_id', $userId)->where('from_bank_id', $bankId)
      ->where(\DB::raw('lower(from_bank_account_name)'), strtolower($bankAccountUserName))->first();
  }

  public function getUserIdSuspectCount($userId) {
    return $this->model->where('user_id', $userId)->where('is_suspected', true)
      ->where('updated_at', '>', Carbon::now()->subDays(5)->toDateTimeString())->count();
  }

  public function getSuspectList($userId) {
    return $this->model->where('user_id', $userId)->where('is_suspected', true)
      ->where('updated_at', '>', Carbon::now()->subDays(5)->toDateTimeString())->get();
  }

  public function gets() {
    $query = $this->getCloneModel()
      ->join('banks', 'banks.id', '=', 'user_topup_bank_transfers.from_bank_id')
      ->join('users', 'users.id', '=', 'user_topup_bank_transfers.user_id');

    $filters = $this->getFilters();

    if (isset($filters['search'])) $filters = $filters['search'];

    if (isset($filters['user_id'])) $query = $query->where('user_topup_bank_transfers.user_id', $filters['user_id']);

    return $this->getResult($query
      ->select(
        'user_topup_bank_transfers.id',
        'user_topup_bank_transfers.user_id',
        'users.name as user_name',
        'users.telephone',
        'banks.name as bank_name',
        'user_topup_bank_transfers.from_bank_account_name',
        'user_topup_bank_transfers.is_suspected',
        'user_topup_bank_transfers.created_at'
      )->orderBy('user_topup_bank_transfers.id', 'desc'));
  }

}
