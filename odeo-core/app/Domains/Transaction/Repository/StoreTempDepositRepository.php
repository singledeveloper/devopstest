<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\StoreTempDeposit;
use Odeo\Domains\Constant\StoreTempDeposit as StoreTempDepositConstant;

class StoreTempDepositRepository extends Repository {

  private $lock = false;

  public function __construct(StoreTempDeposit $tempDeposit) {
    $this->model = $tempDeposit;
  }

  public function lock() {
    $this->lock = true;
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->model;

    if (isset($filters["store_id"])) $query = $query->where('store_id', $filters["store_id"]); //seharusnya di sini
    if (isset($filters["created_at"])) $query = $query->where('created_at', '<', $filters["created_at"]);

    return $this->getResult($query);
  }

  public function sumGroupByStoreId($store_ids) {
    if (!is_array($store_ids)) $store_ids = explode(",", $store_ids);
    return $this->model->groupBy('store_id')->selectRaw('sum(amount) as total_amount, store_id')
      ->whereIn("store_id", $store_ids)->where("type", StoreTempDepositConstant::ORDER)->pluck('total_amount', 'store_id');
  }

  public function sumAgentServerAmountGroupByStoreId($store_ids) {
    if (!is_array($store_ids)) $store_ids = explode(",", $store_ids);
    return $this->model->groupBy('store_id')->selectRaw('sum(amount) as total_amount, store_id')
      ->whereIn("store_id", $store_ids)->where("type", StoreTempDepositConstant::AGENT_SERVER_ORDER)->pluck('total_amount', 'store_id');
  }

  public function findByStoreId($storeId) {
    return $this->model->where("store_id", $storeId)->get();
  }

  public function findByOrderId($orderId, $type = StoreTempDepositConstant::ORDER) {
    $model = $this->model;
    if ($this->lock) $model = $model->lockForUpdate();

    $query = $model->where("reference_id", $orderId);

    $type && (is_array($type) && $query->whereIn("type", $type) || $query->where("type", $type));

    if ($type == StoreTempDepositConstant::ORDER) {
      return $query->first();
    }
    return $query->get();

  }

  public function getTransactionBy($storeId) {

    $query = $this->getCloneModel();

    $query = $query->where('store_id', $storeId);

    if ($this->hasExpand('order')) $query = $query->with('order');
    if ($this->hasExpand('user')) $query = $query->with('order.user');

    return $this->getResult($query->orderBy('id', 'desc'));

  }

}
