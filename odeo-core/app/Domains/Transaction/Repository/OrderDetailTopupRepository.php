<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\OrderDetailTopup;

class OrderDetailTopupRepository extends Repository {
  
  public function __construct(OrderDetailTopup $topup) {
    $this->model = $topup;
  }

  public function findByOrderDetailId($order_detail_id) {
    return $this->model->where('order_detail_id', $order_detail_id)->first();
  }

  public function findByTopupId($topupId) {
    return $this->model->where('topup_id', $topupId)->first();
  }
}
