<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\StoreDeposit;

use Carbon\Carbon;

class StoreDepositRepository extends Repository {

  public function __construct(StoreDeposit $storeDeposit) {
    $this->model = $storeDeposit;
  }

  public function findLastStoreDeposit($storeId) {
    return $this->model->where("store_id", $storeId)->where('status', \Odeo\Domains\Constant\StoreDeposit::PENDING)->orderBy("id", "DESC")->first();
  }

  public function getAverageLastDeposit($storeId, $defaultValue = 100000) {
    $date = Carbon::yesterday()->toDateString();
    $dateLastTwoWeek = Carbon::yesterday()->subWeeks(1)->toDateString();
    $avg = \DB::select(
      <<<query
          select (sum(topup_amount) / 3) as sum
            from (
              SELECT topup_amount
              FROM preset_cashbacks
              WHERE store_id = $storeId
                 AND created_at::DATE <= '$date'
                 AND created_at::DATE >= '$dateLastTwoWeek'
              ORDER BY topup_amount DESC
              LIMIT 5
            ) x
query
    );

    return $avg[0]->sum ?? $defaultValue;
  }

  public function getAverageAllDeposit($defaultValue = 100000) {
    $date = Carbon::yesterday()->toDateString();
    $dateLastTwoWeek = Carbon::yesterday()->subWeeks(1)->toDateString();
    $avg = \DB::select(
      <<<query
        SELECT avg(topup_amount)
        FROM (
          SELECT topup_amount
          FROM preset_cashbacks
          WHERE created_at :: DATE <= '$date'
                AND created_at :: DATE >= '$dateLastTwoWeek'
        ) x
query
    );

    return $avg[0]->avg ?? $defaultValue;
  }
}
