<?php
/**
 * Created by PhpStorm.
 * User: mrp130
 * Date: 10/31/2017
 * Time: 7:24 PM
 */

namespace Odeo\Domains\Transaction\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\DepositRecord;

class DepositRecordRepository extends Repository {
  public function __construct(DepositRecord $depositRecord) {
    $this->model = $depositRecord;
  }

  public function getMinOfLastTwoRecords($storeId) {
    $min = \DB::select(
      <<<query
select min(amount)
from (
  SELECT amount
  FROM deposit_records
  WHERE store_id = $storeId
  ORDER BY created_at DESC
  LIMIT 2
) x
query
    );
    return $min[0]->min ?? 0;
  }
}