<?php

namespace Odeo\Domains\Transaction\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\UserWithdraw;
use Odeo\Domains\Constant\Withdraw;

class UserWithdrawRepository extends Repository {

  public function __construct(UserWithdraw $withdraw) {
    $this->model = $withdraw;
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->model;

    if ($this->hasExpand('user')) $query = $query->with('user');
    if ($this->hasExpand('clearing')) {
      $query = $query->with(['bank']);

      if (isset($filters['search']['not_allowed_bank'])) {
        $query = $query->whereHas('bank', function ($query) use ($filters) {
          $query->whereNotIn('id', $filters['search']['not_allowed_bank']);
        });
      }
    }

    if (!isset($filters['all']) || !$filters['all']) {
      $query = $query->where('status', Withdraw::PENDING);
    }

    if (isset($filters['search'])) {
      if (isset($filters['search']['status'])) {
        $query->where('status', '=', $filters['search']['status']);
      }
      if (isset($filters['search']['withdraw_status'])) {
        $query->where('status', '=', $filters['search']['withdraw_status']);
      }
      if (isset($filters['search']['user_id'])) {
        $query->where('user_id', '=', $filters['search']['user_id']);
      }
      if (isset($filters['search']['withdraw_id'])) {
        $query->where('id', '=', $filters['search']['withdraw_id']);
      }
      if (isset($filters['search']['auto_disbursement_vendor'])) {
        $query->where('auto_disbursement_vendor', '=', $filters['search']['auto_disbursement_vendor']);
      }
    }

    if (!isAdmin($filters)) $query = $query->where('user_id', $filters["auth"]["user_id"]);

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function findLastWithdraw($userId) {
    return $this->model->where("user_id", $userId)
      ->where('status', '<', Withdraw::COMPLETED)
      ->where('status', '<>', Withdraw::SUSPECT_CON)
      ->orderBy("id", "DESC")->first();
  }

  public function getTodayLimit($userId) {
    $row = $this->model->selectRaw('sum(amount) as total_amount')->where('user_id', $userId)
      ->whereDate('created_at', '=', Carbon::now())
      ->where('status', '<', Withdraw::CANCELLED)
      ->first();

    return $row->total_amount ?? 0;
  }

  public function getCurrentMonthTotalWithdraw($userId) {
    $row = $this->model->selectRaw('sum(amount) as total_amount')
      ->where('user_id', $userId)
      ->where('created_at', '>=', Carbon::now()->firstOfMonth())
      ->where('status', '<', Withdraw::CANCELLED)
      ->first();

    return $row->total_amount ?? 0;
  }

  public function getTotalWithdrawalByUserId($userId) {
    return $this->model->where('user_id', $userId)->sum('amount');
  }

  public function getWithdraws($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');
    return $this->model->where(\DB::raw('date(updated_at)'), $date)
      ->where('status', Withdraw::COMPLETED)->orderBy('id', 'asc')->get();
  }

  public function getUnProcessedAutoWithdraw() {

    return $this->model
      ->whereIn('status', [
        Withdraw::PENDING_PROGRESS_AUTO_TRANSFER,
        Withdraw::PENDING_PROGRESS_AUTO_TRANSFER_NOT_ENOUGH_DEPOSIT
      ])
      ->with(['bank'])
      ->get();

  }

  public function getWithdrawById($ids) {
    return $this->model->whereIn('id', $ids)->get();
  }

  public function getUnreconciledTransferByVendor($vendor) {
    return $this->model
      ->where('is_bank_transfer', true)
      ->where('is_reconciled', false)
      ->where('status', Withdraw::COMPLETED)
      ->where('auto_disbursement_vendor', $vendor)
      ->get();
  }

  public function getSumByMonthWithUserIdList($date, $type, $userIdList) {
    $query = $this->getCloneModel()
      ->select(\DB::raw("
        to_char(verified_at, 'YYYY-MM') as period,
        sum(amount+fee)                 as gmv,
        sum(fee-cost)                   as revenue,
        count(id)                       as trx
      "))
      ->whereNotNull('verified_at')
      ->where('status', Withdraw::COMPLETED)
      ->whereBetween(\DB::raw('verified_at::date'), [$date->copy()->subYear()->format('Y-m-d'), $date->format('Y-m-d')])
      ->groupBy('period');

    if ($type == 'except') {
      $query = $query->whereNotIn('user_id', $userIdList);
    }
    else {
      $query = $query->whereIn('user_id', $userIdList);
    }

    return $query->get();
  }

  public function getByUserPhones($phones) {
    $filters = $this->getFilters();

    $query = $this->model
      ->join('users', 'users.id', '=', 'user_withdraws.user_id')
      ->whereIn('users.telephone', $phones);

    if (isset($filters['start_date'])) {
      $query = $query->whereDate('user_withdraws.created_at', '>=', $filters['start_date']);
    }

    if (isset($filters['end_date'])) {
      $query = $query->whereDate('user_withdraws.created_at', '<=', $filters['end_date']);
    }

    return $query->select(
      'users.telephone',
      'user_withdraws.id',
      'user_withdraws.amount',
      'user_withdraws.fee',
      'user_withdraws.account_bank',
      'user_withdraws.account_name',
      'user_withdraws.account_number',
      'user_withdraws.status',
      'user_withdraws.created_at',
      'user_withdraws.updated_at'
    )->orderBy('user_withdraws.created_at')->get();

  }
}
