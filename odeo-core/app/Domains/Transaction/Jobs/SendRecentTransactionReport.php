<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/11/17
 * Time: 6:53 PM
 */

namespace Odeo\Domains\Transaction\Jobs;


use Box\Spout\Writer\WriterFactory;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendRecentTransactionReport extends Job {
  private $filters, $email, $selector, $exportId;

  public function __construct($filters, $email, $exportId) {
    parent::__construct();
    $this->filters = $filters;
    $this->email = $email;
    $this->exportId = $exportId;
  }

  public function handle() {
    
    $this->selector = app()->make(\Odeo\Domains\Transaction\RecentSelector::class);
    $exportQueueManager = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class);
    
    $transactions = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $writer = WriterFactory::create($this->filters['file_type']);
    ob_start();

    $writer->openToFile('php://output');
    $transactions->normalizeFilters($this->filters);
    $report = $transactions->getTransactionReport();

    if ($this->filters['type'] == 'deposit') {

      $writer->addRow([
        'Tanggal Transaksi',
        'Via',
        'Deskripsi',
        'Dari',
        'Ke',
        'Catatan',
        'Toko',
        'Kredit',
        'Debit',
        'Saldo Sebelum',
        'Saldo Sesudah'
      ]);
    } else {
      $writer->addRow([
        'Tanggal Transaksi',
        'Via',
        'Deskripsi',
        'Dari',
        'Ke',
        'Catatan',
        'Kredit',
        'Debit',
        'Saldo Sebelum',
        'Saldo Sesudah'
      ]);
    }

    $this->selector->setUserName($this->filters['user_name']);
    while ($row = $report->fetch(\PDO::FETCH_OBJ)) {
      $transform = $this->selector->_transforms($row, $transactions);

      if ($this->filters['type'] == 'deposit') {
        $writer->addRow([
          $row->created_at,
          $transform['via'] ?? '-',
          $transform['description'],
          $transform['from'] ?? '-',
          $transform['to'] ?? '-',
          isset($transform['notes']) ? str_replace("\n", ' ', $transform['notes']) : '-',
          $row->store_name,
          $row->amount > 0 ? floor($row->amount) : 0,
          $row->amount < 0 ? abs(floor($row->amount)) : 0,
          floor($row->balance_before),
          floor($row->balance_after),
        ]);
      } else {
        $writer->addRow([
          $row->created_at,
          $transform['via'] ?? '-',
          $transform['description'],
          $transform['from'] ?? '-',
          $transform['to'] ?? '-',
          isset($transform['notes']) ? str_replace("\n", ' ', $transform['notes']) : '-',
          $row->amount > 0 ? floor($row->amount) : 0,
          $row->amount < 0 ? abs(floor($row->amount)) : 0,
          floor($row->balance_before),
          floor($row->balance_after),
        ]);
      }
    }

    $writer->close();

    $file = ob_get_clean();

    $fileName = 'Laporan Mutasi ' . ucwords($this->filters['type']) . ' ' . $this->filters['start_date'] . ' - ' . $this->filters['end_date'] . '.' . $this->filters['file_type'];
    $exportQueueManager->update($this->exportId, $file, $fileName);

    Mail::send('emails.email_transaction_report', [
      'data' => $this->filters,
      'title' => 'Laporan Mutasi ' . ucwords($this->filters['type'])
    ], function ($m) use ($file, $fileName) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->attachData($file, $fileName);

      $m->to($this->email)->subject('[ODEO] Laporan Mutasi ' . ucwords($this->filters['type']));
    });
  }
}
