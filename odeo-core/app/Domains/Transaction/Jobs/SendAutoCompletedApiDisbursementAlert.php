<?php

namespace Odeo\Domains\Transaction\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendAutoCompletedApiDisbursementAlert extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.auto_completed_api_disbursement_alert', [
      'data' => $this->data
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo')
        ->to('disbursement@odeo.co.id')
        ->subject('Auto Completed Api Disbursement - ' . date('Y-m-d'));
    });
  }

}
