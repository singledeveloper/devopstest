<?php

namespace Odeo\Domains\Transaction\Jobs;

use Odeo\Domains\Account\Repository\SubUserRepository;
use Odeo\Domains\Constant\CashWebhookType;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Transaction\Helper\CashWebhookNotifier;
use Odeo\Domains\Transaction\Repository\CashApiUserRepository;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Jobs\Job;

class NotifyWithdrawStatusChanged extends Job {

  private $withdrawId;

  public function __construct($withdrawId) {
    parent::__construct();
    $this->withdrawId = $withdrawId;
  }

  public function handle() {
    $userWithdrawRepo = app()->make(UserWithdrawRepository::class);
    $subUserRepo = app()->make(SubUserRepository::class);
    $cashApiUserRepo = app()->make(CashApiUserRepository::class);
    $cashWebhookNotifier = app()->make(CashWebhookNotifier::class);

    $withdraw = $userWithdrawRepo->findById($this->withdrawId);
    if (!$withdraw || !$withdraw->reference_id) {
      return;
    }

    $userId = $withdraw->user_id;
    $subUser = $subUserRepo->getParentUserId($userId);
    if ($subUser) {
      $userId = $subUser->parent_user_id;
    }

    $apiUser = $cashApiUserRepo->findByUserId($userId);
    if (!$apiUser || !$apiUser->callback_url) {
      return;
    }

    $body = [
      'event_type' => 'withdraw_status_changed',
      'withdraw' => [
        'withdraw_id' => $withdraw->id . '',
        'user_id' => $withdraw->user_id . '',
        'bank_id' => +$withdraw->bank_id,
        'account_number' => $withdraw->account_number,
        'account_name' => $withdraw->account_name,
        'amount' => intval($withdraw->amount) . '',
        'reference_id' => $withdraw->reference_id . '',
        'status' => Withdraw::mapStatus($withdraw->status) . '',
      ],
    ];

    $res = $cashWebhookNotifier
      ->notify($userId, $apiUser->callback_url, $apiUser->signing_key, CashWebhookType::USER_WITHDRAW, $withdraw->id, $body);

    if ($res['retry']) {
      dispatch((new NotifyWithdrawStatusChanged($this->withdrawId))->delay(30));
    }
  }


}
