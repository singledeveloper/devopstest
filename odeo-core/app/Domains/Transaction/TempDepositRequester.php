<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/10/17
 * Time: 21:25
 */

namespace Odeo\Domains\Transaction;


use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\StoreTempDeposit;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\MarginFormatter;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository;
use Odeo\Domains\Subscription\Repository\StoreRepository;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;
use Odeo\Domains\Transaction\Helper\CashInserter;
use Odeo\Domains\Transaction\Repository\StoreTempDepositRepository;

class TempDepositRequester {

  //const FLAG = 'flag';

  public function __construct() {
    $this->storeInventories = app()->make(StoreInventoryRepository::class);
    $this->storeTempDeposits = app()->make(StoreTempDepositRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
    $this->orders = app()->make(OrderRepository::class);
    $this->stores = app()->make(StoreRepository::class);
    $this->marginFormatter = app()->make(MarginFormatter::class);
  }

  public function tempDeposit(PipelineListener $listener, $data) {
    if (!isset($data['order_id']) || !($order = $this->orders->findById($data['order_id']))) {
      return $listener->response(400, "Can't check deposit of non-existing order.");
    }

    if ($order->status != OrderStatus::CREATED) return $listener->response(200);

    if (!$order->seller_store_id || $order->skip_check_temp_deposit || $this->storeTempDeposits->findByOrderId($order->id)) {
      return $listener->response(200);
    }

    $serviceDetail = null;
    $orderDetails = $order->details->first();

    $tempDeposit = $this->storeTempDeposits->getNew();

    $tempDeposit->reference_id = $order->id;
    $tempDeposit->store_id = NULL;
    $tempDeposit->type = StoreTempDeposit::ORDER;
    $tempDeposit->amount = $orderDetails->merchant_price;
    $tempDeposit->sale_amount = $orderDetails->sale_price;
    $tempDeposit->description = \json_encode([]);

    $this->storeTempDeposits->save($tempDeposit);

    if ($orderDetails->agent_base_price == null) {
      return $listener->response(200);
    }

    $userStores = $this->stores->getModel()
      ->join('user_stores', 'user_stores.store_id', '=', 'stores.id')
      ->where('type', 'owner')
      ->where('store_id', $order->seller_store_id)
      ->select('user_id', 'name', 'status', 'stores.plan_id', 'stores.can_supply')
      ->first();

    $tempDepositTobeInsert = [];

    if ($userStores->user_id == $order->user_id && $userStores->can_supply) {
      $tempDepositTobeInsert[] = [
        'reference_id' => $data['order_id'],
        'store_id' => $order->seller_store_id,
        'type' => StoreTempDeposit::SUPPLIER_CASH,
        'amount' => $orderDetails->sale_price,
        'sale_amount' => $orderDetails->sale_price,
        'description' => \json_encode([
          'store_name' => $userStores->name,
          'store_user_id' => $userStores->user_id,
          'purchaser_user_id' => $order->user_id,
          'pay_in_ocash' => true,
          'disable_mentor_leader' => true,
        ]),
        'created_at' => $order->created_at,
        'updated_at' => $order->created_at
      ];
    }
    else {
      $uplineSales = $this->storeInventories->getAllUplineSalePrice([
        'store' => [
          'id' => $order->seller_store_id,
          'user_id' => $userStores->user_id,
          'name' => $userStores->name,
          'status' => $userStores->status,
          'plan_id' => $userStores->plan_id,
          'can_supply' => $userStores->can_supply
        ],
        'sale_price' => $orderDetails->sale_price,
        'store_inventory_detail_id' => $orderDetails->store_inventory_detail_id
      ]);

      if ($userStores->plan_id == Plan::FREE && (count($uplineSales) <= 0 ||
          (count($uplineSales) == 1 && !$userStores->can_supply))) {
        $serviceId = ServiceDetail::getServiceId($orderDetails->service_detail_id);
        $serviceName = str_replace('_', ' ', Service::getConstKeyByValue($serviceId));
        return $listener->response(400, 'Master Anda belum menyediakan ' . title_case($serviceName));
      }

      foreach ($uplineSales as $key => $upSales) {
        if (!StoreStatus::isActive($upSales->store_status)) return $listener->response(400, 'Toko master tidak aktif');

        $isFreeServer = $upSales->plan_id == Plan::FREE;

        $trxData = [
          'order_id' => $data['order_id'],
          'recent_desc' => $upSales->store_name,
        ];

        if ($upSales->store_id != $order->seller_store_id) {
          $trxData['is_community'] = true;
          $trxData['role'] = 'agent';
        }

        $tempDepositDesc = [
          'store_name' => $upSales->store_name,
          'store_user_id' => $upSales->user_id,
          'purchaser_user_id' => $key == 0 ? $order->user_id : $uplineSales[$key - 1]->user_id,
          'pay_in_ocash' => true,
          'disable_mentor_leader' => true,
        ];

        if (isset($uplineSales[$key + 1])) {
          $basePrice = $this->getSellPrice($uplineSales[$key + 1]->sell_price, $orderDetails);
          $tempDepositDesc['item_from_store_id'] = $trxData['item_from_store_id'] = $uplineSales[$key + 1]->store_id;
        } else {
          $basePrice = $orderDetails->base_price;
        }

        $upSales->sell_price = $key == 0 ? $upSales->sell_price : $this->getSellPrice($upSales->sell_price, $orderDetails);

        if (!$isFreeServer && !$upSales->store_can_supply) {
          $this->cashInserter->add([
            'user_id' => $upSales->user_id,
            'store_id' => $upSales->store_id,
            'trx_type' => TransactionType::ORDER_CONVERT,
            'cash_type' => CashType::ODEPOSIT,
            'amount' => -$basePrice,
            'data' => json_encode($trxData),
            //'flag' => $key
          ]);
          //$tempDepositDesc['flag'] = $key;
        }

        $tempDepositTobeInsert[] = [
          'reference_id' => $data['order_id'],
          'store_id' => $upSales->store_id,
          'type' => $upSales->store_can_supply && $upSales->n == sizeof($uplineSales) ? StoreTempDeposit::SUPPLIER_CASH :
            ($isFreeServer ? StoreTempDeposit::AGENT_ORDER : StoreTempDeposit::AGENT_SERVER_ORDER),
          'amount' => $basePrice,
          'sale_amount' => $upSales->sell_price,
          'description' => \json_encode($tempDepositDesc),
          'created_at' => $order->created_at,
          'updated_at' => $order->created_at
        ];
      }

      try {
        $this->cashInserter->run();
      } catch (InsufficientFundException $e) {
        return $listener->response(400, 'Saldo master tidak mencukupi.');
      }
    }

    $this->storeTempDeposits->saveBulk($tempDepositTobeInsert);
    return $listener->response(200);
  }


  public function getSellPrice($price, $orderDetails) {
    if (in_array($orderDetails->service_detail_id, ServiceDetail::groupPostpaid())) {
      $extendedDetail = \json_decode($orderDetails->extended_detail);
      return $orderDetails->merchant_price + (($price - $extendedDetail->postpaid_base_fee - $extendedDetail->postpaid_merchant_fee) * $extendedDetail->postpaid_multiplier);
    }

    return $price;
  }

}
