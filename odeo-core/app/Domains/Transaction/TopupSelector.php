<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\TopupStatus;

class TopupSelector implements SelectorListener {

  private $topup, $currency;

  public function __construct() {
    $this->topup = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function _transforms($item, Repository $repository) {
    $topup = [];
    if ($item->id) $topup["topup_id"] = $item->id;
    if ($repository->hasExpand('user') && $user = $item->user) {
      $topup["user"] = [
        "id" => $item->user->id,
        "name" => $item->user->name
      ];
    }
    if ($item->amount) $topup["amount"] = $this->currency->formatPrice($item->amount);
    if ($item->status) {
      if ($item->status == TopupStatus::PENDING) $status = "PENDING";
      else if ($item->status == TopupStatus::COMPLETED) $status = "COMPLETED";
      else if ($item->status == TopupStatus::CANCELLED) $status = "CANCELLED";
      else $status = "UNKNOWN";
      $topup["status"] = $status;
    }
    if ($item->created_at) $topup["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if ($item->updated_at) $topup["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");

    return $topup;
  }

  public function _extends($data, Repository $repository) {
    if ($topupIds = $repository->beginExtend($data, "topup_id")) {
      if ($repository->hasExpand('order')) {
        $order = app()->make(\Odeo\Domains\Order\Helper\OrderManager::class);
        $result = $order->getByTopupId($topupIds);
        $repository->addExtend('order', $result);
      }
    }
    return $repository->finalizeExtend($data);
  }

  public function get(PipelineListener $listener, $data) {
    $this->topup->normalizeFilters($data);
    $this->topup->setSimplePaginate(true);

    $topups = [];

    foreach ($this->topup->get() as $item) {
      $topups[] = $this->_transforms($item, $this->topup);
    }
    if (sizeof($topups) > 0)
      return $listener->response(200, array_merge(
        ["topups" => $this->_extends($topups, $this->topup)],
        $this->topup->getPagination()
      ));

    return $listener->response(204, ["topups" => []]);
  }


  public function getDetail(PipelineListener $listener, $data) {

    $this->topup->normalizeFilters($data);

    if (isset($data["topup_id"]))
      $topup = $this->topup->findById($data["topup_id"]);
    else if (isset($data["last_topup"]))
      $topup = $this->topup->findLastTopup($data["auth"]["user_id"]);

    if (isset($topup) && $topup) {
      $topup = $this->_transforms($topup, $this->topup);
      return $listener->response(200, $this->_extends($topup, $this->topup));
    }

    return $listener->response(204);
  }
}
