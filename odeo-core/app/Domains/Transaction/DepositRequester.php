<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\StoreDeposit;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class DepositRequester {

  public function __construct() {
    $this->presetCashback = app()->make(\Odeo\Domains\Campaign\DepositCashback\Repository\PresetCashbackRepository::class);
    $this->tempDeposit = app()->make(\Odeo\Domains\Transaction\Repository\StoreTempDepositRepository::class);
    $this->tempCash = app()->make(\Odeo\Domains\Transaction\Repository\TempCashDepositRepository::class);
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->cash = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->revenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
    $this->orderDetailStoreDeposits = app()->make(\Odeo\Domains\Transaction\Repository\OrderDetailStoreDepositRepository::class);
    $this->storeDeposits = app()->make(\Odeo\Domains\Transaction\Repository\StoreDepositRepository::class);
  }

  public function request(PipelineListener $listener, $data) {

    if ($this->storeDeposits->findLastStoreDeposit($data["store_id"])) {
      return $listener->response(400, trans('errors.cash.pending_deposit_exists'));
    }

    $amount = $this->currency->getAmountOnly($data['deposit']['amount'], $data['deposit']['currency']);

    $storeDeposit = $this->storeDeposits->getNew();
    $storeDeposit->store_id = $data["store_id"];
    $storeDeposit->amount = $amount;
    $storeDeposit->status = StoreDeposit::PENDING;

    if ($this->storeDeposits->save($storeDeposit)) {
      return $listener->response(201);
    }

    return $listener->response(400);
  }

  public function checkoutPresetCashback(PipelineListener $listener, $data) {

    $item = $data['item'];
    $storeId = $item['item_detail']['store_id'];

    if (isset($item['bonus'])) {

      if ($presetCashback = $this->presetCashback->todayPreset($storeId)) ;
      else {
        $presetCashback = $this->presetCashback->getNew();
        $presetCashback->store_id = $storeId;
      }

      $presetCashback->topup_amount = $item['topup_amount'];
      $presetCashback->bonus = $item['bonus'];
      $presetCashback->preset = $item['preset'];
      $presetCashback->order_id = $data['order_id'];

      $this->presetCashback->save($presetCashback);
    }

    return $listener->response(200);
  }

  public function checkout(PipelineListener $listener, $data) {

    if ($storeDeposit = $this->storeDeposits->findLastStoreDeposit($data['item']['item_detail']['store_id'])) {
      $orderDetail = $this->orderDetailStoreDeposits->getNew();
      $orderDetail->store_deposit_id = $storeDeposit->id;
      $orderDetail->order_detail_id = $data['order_detail']['id'];

      $this->orderDetailStoreDeposits->save($orderDetail);

      $data['order_detail_repository_id'] = $orderDetail->id;
      $listener->addNext(new Task(__CLASS__, 'checkoutPresetCashback', $data));

      return $listener->response(201);
    }
    return $listener->response(400, "Unable to checkout deposit.");
  }

  public function cancel(PipelineListener $listener, $data) {
    if ($storeDeposit = $this->storeDeposits->findById($data['store_deposit_id'])) {
      $storeDeposit->status = StoreDeposit::CANCELLED;
      if ($this->storeDeposits->save($storeDeposit)) {
        return $listener->response(200);
      }
    }
    return $listener->response(400);
  }


  public function complete(PipelineListener $listener, $data) {

    $stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);

    $storeDeposit = $this->storeDeposits->findById($data['store_deposit_id']);

    $storeDeposit->status = StoreDeposit::COMPLETED;
    $storeDeposit->save();

    $store = $stores->findById($storeDeposit->store_id);
    $userId = $store->owner->first()->id;

    $this->inserter->add([
      'user_id' => $userId,
      'store_id' => $store->id,
      'trx_type' => TransactionType::CONVERT,
      'cash_type' => CashType::ODEPOSIT,
      'amount' => $storeDeposit->amount,
      'data' => json_encode([
        "recent_desc" => $store->name,
        "order_id" => $data['order_id']
      ]),
      'service_id' => Service::ODEPOSIT
    ]);

    $this->inserter->run();

    if ($store) {
      $this->notification->setup($userId, NotificationType::CHECK, NotificationGroup::TRANSACTION);
      $this->notification->deposit_request($storeDeposit->amount, $store->name, $data['order_id']);
      $listener->pushQueue($this->notification->queue());
    }

    $listener->addNext(new Task(__CLASS__, 'completePresetCashback', array_merge($data, [
      'store_name' => $store->name,
      'store_id' => $store->id
    ])));

    return $listener->response(200);
  }

  function completePresetCashback(PipelineListener $listener, $data) {
    $presetCashback = $this->presetCashback->findByOrderId($data['order_id']);

    if (isset($presetCashback)) {
      $info = [
        'order_id' => $data['order_id'],
        "recent_desc" => $data['store_name'],
        "preset_cashback_id" => $presetCashback->id,
      ];

      $this->inserter->add([
        'user_id' => $data['user_id'],
        'store_id' => $presetCashback->store_id,
        'trx_type' => TransactionType::PRESET_CASHBACK,
        'cash_type' => CashType::OCASHBACK,
        'amount' => -$presetCashback->bonus,
        'data' => json_encode($info),
      ]);

      $this->inserter->add([
        'user_id' => $data['user_id'],
        'store_id' => $presetCashback->store_id,
        'trx_type' => TransactionType::PRESET_CASHBACK,
        'cash_type' => CashType::OCASH,
        'amount' => $presetCashback->bonus,
        'data' => json_encode($info),
      ]);

      $this->revenue->save([
        'user_id' => $data['user_id'],
        'store_id' => $data['store_id'],
        'order_id' => $data['order_id'],
        'amount' => $presetCashback->bonus,
        'trx_type' => TransactionType::PRESET_CASHBACK,
        'data' => json_encode($info),
      ]);

      $this->inserter->run();
    }

    return $listener->response(200);
  }
}