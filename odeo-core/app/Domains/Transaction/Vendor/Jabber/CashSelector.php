<?php

namespace Odeo\Domains\Transaction\Vendor\Jabber;

use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Vendor\Jabber\JabberManager;

class CashSelector extends JabberManager {

  private $cashManager;

  public function __construct() {
    parent::__construct();
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $cash = $this->cashManager->getCashBalance($data['auth']['user_id'], Currency::IDR);

    $message = 'Sisa oCash Anda ' . $cash[$data['auth']['user_id']]['formatted_amount'];

    $listener->appendResponseMessage($message, InlineJabber::RES_PARAM);

    return $listener->response(200);
  }

}