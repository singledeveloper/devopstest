<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Transaction\Helper\Currency;
use Carbon\Carbon;

class TempDepositSelector implements SelectorListener {

  private $tempDeposits, $orderDetailizer, $currencyHelper, $currency;

  public function __construct() {
    $this->tempDeposits = app()->make(\Odeo\Domains\Transaction\Repository\StoreTempDepositRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    Carbon::setLocale(checkHeaderLanguage());
  }

  public function _transforms($item, Repository $repository) {
    $temp = [];

    $temp["id"] = $item->id;
    $temp["type"] = $item->type;
    $temp["store_id"] = $item->store_id;
    $temp["reference_id"] = $item->reference_id;
    $temp["amount"] = $this->currency->formatPrice($item->amount);
    $temp["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    $temp["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");

    if ($repository->hasExpand('order')) {
      $order = $item->order;
      $temp['order_id'] = $item->order->id;
      $temp['expired_at'] = (new Carbon($item->order->expired_at))->diffForHumans();

      if (isset($order->seller_store_id)) $temp['cart_data']['seller_store_id'] = $order->seller_store_id;

      $temp['cart_data']['total'] = $this->currencyHelper->formatPrice($order->total);
      $temp['cart_data']['subtotal'] = $this->currencyHelper->formatPrice($order->subtotal);
      $temp['cart_data']['items'] = $this->orderDetailizer->detailizeItem($order, (function () {
        if (!isAdmin()) {
          return ['mask_phone_number'];
        }
        return [];
      })());
      $temp['cart_data']['charges'] = $this->orderDetailizer->detailizeCharge($order->id, (function () {
        if (isAdmin()) {
          return ['doku_fee'];
        }
        return [];
      })());

    }

    if ($repository->hasExpand('user')) {
      $order = $item->order;
      $user = $order->user;

      $temp['user']['id'] = $user->id;

      if (isset($user->telephone)) $temp['user']['telephone'] = $user->telephone;
      else if (isset($order->phone_number)) $temp['user']['telephone'] = $order->phone_number;

      if (isset($user->email)) $temp['user']['email'] = $user->email;
      else if (isset($order->email)) $temp['user']['email'] = $order->email;
      else $temp['user']['email'] = '';

      if (isset($user->name)) $temp['user']['name'] = $user->name;
      else if (isset($order->name)) $temp['user']['name'] = $order->name;

      if (!isAdmin() && ($order->gateway_id == Payment::WEB_SWITCHER || $order->gateway_id == Payment::ANDROID || $order->gateway_id == Payment::WEB_APP)) {
        if ($length = strlen($temp['user']['telephone'])) {
          $temp['user']['telephone'] = str_repeat('*', $length - 4) . substr($temp['user']['telephone'], $length - 4);
        }
        if($length = strlen($temp['user']['email'])) {
          $temp['user']['email'] = substr($temp['user']['email'], 0, 5) . str_repeat('*', max(0, $length - 5));
        }
        $temp['user']['name'] = "";
      }
    }

    return $temp;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    if (isset($data["store_id"])) { // mana ada beginian
      $temp = $this->tempDeposits->findByStoreId($data["store_id"]);
      if ($temp) {
        return $listener->response(200,
          ["deposit_requests" => $this->_transforms($temp, $this->tempDeposits)]
        );
      }
    } else {
      $this->tempDeposits->normalizeFilters($data);

      $temps = [];
      foreach ($this->tempDeposits->get() as $item) {
        $temps[] = $this->_transforms($item, $this->tempDeposits);
      }

      if (sizeof($temps) > 0)
        return $listener->response(200, array_merge(
          ["deposit_requests" => $this->_extends($temps, $this->tempDeposits)],
          $this->tempDeposits->getPagination()
        ));
    }

    return $listener->response(204, ["deposit_requests" => []]);
  }

  public function getLockedDeposit(PipelineListener $listener, $data) {
    $locks = [];

    $this->tempDeposits->normalizeFilters($data);

    foreach ($this->tempDeposits->getTransactionBy($data['store_id']) as $item) {
      $locks[] = $this->_transforms($item, $this->tempDeposits);
    }

    if (sizeof($locks) > 0) {

      $response['locks'] = $locks;

      if ($this->tempDeposits->hasExpand('total_deposit')) {
        $response['total_deposit'] = $this->currencyHelper->formatPrice($this->tempDeposits->getTransactionBy($data['store_id'])->sum('amount'));
      }

      return $listener->response(200, array_merge($response,
        $this->tempDeposits->getPagination()
      ));
    }

    return $listener->response(204);
  }

}
