<?php

namespace Odeo\Domains\Approval\Repository;

use Odeo\Domains\Approval\Model\ApprovalUser;
use Odeo\Domains\Core\Repository;

class ApprovalUserRepository extends Repository {

  public function __construct(ApprovalUser $approvalUser) {
    $this->model = $approvalUser;
  }

  public function findMakerPath($userId, $path) {
    return $this->getCloneModel()->where('maker_user_id', $userId)
      ->where('path', $path)->first();
  }

  public function findApprover($userId) {
    return $this->getCloneModel()->where('approver_user_id', $userId)->first();
  }

}