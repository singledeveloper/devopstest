<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 05/11/19
 * Time: 14.48
 */

namespace Odeo\Domains\Approval;


use Carbon\Carbon;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Account\Repository\UserGroupRepository;
use Odeo\Domains\Approval\Detailizer\DetailizerManager;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;

class ApprovalUpdater {

  function __construct() {
    $this->approvalPendingRequest = app()->make(ApprovalPendingRequestRepository::class);
    $this->users = app()->make(UserRepository::class);
    $this->userGroupRepo = app()->make(UserGroupRepository::class);
  }

  public function approve(PipelineListener $listener, $data) {
    $user = $this->users->findById($data['auth']['user_id']);
    if (isAdmin($data)) {
      if (!isset($data['password'])) {
        return $listener->response(400, 'Password is required');
      }

      if ($data['password'] != env('APPROVE_PENDING_APPROVAL_PASSWORD')) {
        return $listener->response(400, 'Invalid Password');
      }

      if (!$user) {
        return $listener->response(400, 'Invalid/unauthorized User');
      }

      if (!$this->userGroupRepo->findUserWithinGroup($user->id, UserType::GROUP_BULK_PURCHASE_APPROVER)) {
        return $listener->response(400, 'Invalid/unauthorized User');
      }
    }

    if (!$approval = $this->approvalPendingRequest->findById($data['approval_id'])) {
      return $listener->response(400, 'Invalid pending approval');
    }

    if (!isAdmin($data) && $approval->need_approver_from != $data['auth']['user_id']) {
      return $listener->response(400, 'Invalid user to approve');
    }

    if ($approval->status > Approval::CREATED) {
      return $listener->response(400, 'Already processed');
    }

    $approval->status = Approval::APPROVED;
    $approval->approved_by = $user->id;
    $approval->approved_at = Carbon::now();
    $this->approvalPendingRequest->save($approval);

    return $listener->response(200, ['message' => 'Pending data approved']);
  }

  public function cancel(PipelineListener $listener, $data) {
    $user = $this->users->findById($data['auth']['user_id']);

    if (isAdmin($data)) {
      if (!isset($data['password'])) {
        return $listener->response(400, 'Password is required');
      }

      if ($data['password'] != env('CANCEL_PENDING_APPROVAL_PASSWORD')) {
        return $listener->response(400, 'Invalid Password');
      }

      if (!$user) { // TODO: add whitelist user here
        return $listener->response(400, 'Invalid/unauthorized User');
      }
    }

    if (!$approval = $this->approvalPendingRequest->findById($data['approval_id'])) {
      return $listener->response(400, 'Invalid pending approval');
    }

    if (!isAdmin($data) && $approval->need_approver_from != $data['auth']['user_id']) {
      return $listener->response(400, 'Invalid user to approve');
    }

    $approval->status = Approval::CANCELLED;
    $this->approvalPendingRequest->save($approval);

    $path = app()->make(DetailizerManager::class)->parse($approval->path);
    if ($path != '') $path->cancel($approval);

    return $listener->response(200, ['message' => 'Pending data cancelled']);
  }
}