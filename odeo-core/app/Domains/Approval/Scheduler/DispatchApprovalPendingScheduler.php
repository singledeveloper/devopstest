<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/11/19
 * Time: 12.14
 */

namespace Odeo\Domains\Approval\Scheduler;

use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Job\ApproveBatchDisbursement;
use Odeo\Domains\Approval\Job\ApproveBulkPulsa;
use Odeo\Domains\Approval\Job\ApproveOCashTransfer;
use Odeo\Domains\Approval\Job\ApproveWithdraw;
use Odeo\Domains\Approval\Job\ManualInsertBankInquiry;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;

class DispatchApprovalPendingScheduler {

  function __construct() {
    $this->approvalPendingRepository = app()->make(ApprovalPendingRequestRepository::class);
  }

  public function run() {
    $pendings = $this->approvalPendingRepository->getApprovedPendingApproval();

    foreach ($pendings as $pending) {
      $paths = explode(':', $pending->path);
      switch ($paths[0]) {
        case Approval::PATH_BANK_INQUIRY: {
          dispatch(new ManualInsertBankInquiry($pending->id, $paths[1]));
          break;
        }
        case Approval::PATH_OCASH_TRANSFER: {
          dispatch(new ApproveOCashTransfer($pending->id));
          break;
        }
        case Approval::PATH_BULK_PULSA: {
          dispatch(new ApproveBulkPulsa($pending->id));
          break;
        }
        case Approval::PATH_BATCH_DISBURSEMENT: {
          dispatch(new ApproveBatchDisbursement($pending->id));
          break;
        }
        case Approval::PATH_WITHDRAW: {
          dispatch(new ApproveWithdraw($pending->id));
          break;
        }
      }
    }
  }

}