<?php

namespace Odeo\Domains\Approval\Detailizer;

use Odeo\Domains\Approval\Helper\Approval;

class DetailizerManager {

  public function parse($path) {
    switch (explode(':', $path)[0]) {
      case Approval::PATH_OCASH_TRANSFER: {
        return app()->make(OCashTransferDetailizer::class);
      }
      case Approval::PATH_BULK_PULSA: {
        return app()->make(BulkPulsaDetailizer::class);
      }
      case Approval::PATH_BATCH_DISBURSEMENT: {
        return app()->make(BatchDisbursementDetailizer::class);
      }
      case Approval::PATH_WITHDRAW: {
        return app()->make(WithdrawDetailizer::class);
      }
    }
    return '';
  }

}