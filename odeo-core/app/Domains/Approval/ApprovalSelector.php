<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/11/19
 * Time: 18.15
 */

namespace Odeo\Domains\Approval;


use Odeo\Domains\Accounting\CompanyTransactionInformation\Repository\CompanyTransactionAdditionalInformationRepository;
use Odeo\Domains\Approval\Detailizer\DetailizerManager;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;
use Odeo\Domains\Approval\Repository\ApprovalUserRepository;
use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class ApprovalSelector implements SelectorListener {

  private $approvalPendingRequest, $approvalUserRepo, $detailizerManager;

  function __construct() {
    $this->approvalPendingRequest = app()->make(ApprovalPendingRequestRepository::class);
    $this->approvalUserRepo = app()->make(ApprovalUserRepository::class);
    $this->detailizerManager = app()->make(DetailizerManager::class);
  }

  private $isAdmin = false;

  public function get(PipelineListener $listener, $data) {
    $this->approvalPendingRequest->normalizeFilters($data);
    $this->approvalPendingRequest->setSimplePaginate(true);

    $result = [];
    $this->isAdmin = isAdmin($data);

    if ($pendingApprovals = $this->approvalPendingRequest->get($this->isAdmin ? null : $data['auth']['user_id'])) {
      foreach ($pendingApprovals as $pendingApproval) {
        $result[] = $this->_transforms($pendingApproval, $this->approvalPendingRequest);
      }
      return $listener->response(200, array_merge([
        'approvals' => $this->_extends($result, $this->approvalPendingRequest),
      ], $this->approvalPendingRequest->getPagination()));
    }

    return $listener->response(204, ['approvals' => []]);
  }

  public function getRawDataById(PipelineListener $listener, $data) {
    if ($approval = $this->approvalPendingRequest->findById($data['approval_id'])) {
      return $listener->response(200, ["raw_data" => $approval->raw_data]);
    }
    return $listener->response(204, ["raw_data" => '-']);
  }

  public function _extends($data, Repository $repository) {
    if ($ids = $repository->beginExtend($data, "id")) {
      if ($repository->hasExpand('file')) {
        $additionalInformationRepo = app()->make(CompanyTransactionAdditionalInformationRepository::class);
        $additionalFiles = [];
        foreach ($additionalInformationRepo->getInformationByReferenceIds($ids, 'approval') as $item) {
          if (!isset($additionalFiles[$item->reference_id])) $additionalFiles[$item->reference_id] = [];
          $additionalFiles[$item->reference_id][] = [
            'id' => $item->id,
            'type' => $item->type,
            'image_url' => isset($item->image_url) ? AwsConfig::url($item->image_url) : ''
          ];
        }
        foreach ($ids as $item) {
          if (!isset($additionalFiles[$item])) $additionalFiles[$item] = [];
        }
        $repository->addExtend('additional_files', $additionalFiles);
      }
    }
    return $repository->finalizeExtend($data);
  }

  public function _transforms($item, Repository $repository) {
    $data = json_decode($item->raw_data);
    $manager = $this->detailizerManager->parse($item->path);
    $output = [
      'id' => $item->id,
      'path' => $item->path,
      'path_name' => Approval::getPathName($item->path),
      'reference_id' => $item->reference_id,
      'detail' => $manager != '' ? $manager->format($data) : [],
      'description' => $manager != '' ? $manager->description($data) : '',
      'status' => $item->status,
      'status_message' => Approval::getStatusMessage($item->status),
      'user_message' => $item->user_message,
      'requested_by' => $item->requester ? $item->requester->name : $item->requested_by,
      'approved_by' => $item->approved_by,
      'processed_at' => $item->processed_at,
    ];
    if ($this->isAdmin) {
      $output['raw_data'] = $item->raw_data;
    }
    return $output;
  }

  public function guardNonMakerEndpoint(PipelineListener $listener, $data) {
    if ($this->approvalUserRepo->findMakerPath($data['auth']['user_id'], $data['path'])) {
      return $listener->response(400, 'Access denied');
    }
    return $listener->response(200);
  }

  public function checkMaker(PipelineListener $listener, $data) {
    if ($this->approvalUserRepo->findMakerPath($data['auth']['user_id'], $data['path'])) {
      return $listener->response(200, ['maker' => true]);
    }
    return $listener->response(200, ['maker' => false]);
  }

}