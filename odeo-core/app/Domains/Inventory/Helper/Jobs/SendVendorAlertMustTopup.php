<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Inventory\Helper\Jobs;


use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendVendorAlertMustTopup extends Job  {

  use SerializesModels;

  private $current_balance, $vendor, $minimum_balance;

  public function __construct($current_balance, $minimum_balance, $vendor) {
    parent::__construct();
    $this->current_balance = $current_balance;
    $this->minimum_balance = $minimum_balance;
    $this->vendor = $vendor;

  }

  public function handle() {

    if (app()->environment() != 'production') return;

    Mail::send('emails.vendor_topup_alert', [
      'data' => [
        'current_balance' => $this->current_balance,
        'minimum_balance' => $this->minimum_balance,
        'from' => $this->vendor
      ]
    ], function ($m) {

      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('replenishment@odeo.co.id')->subject($this->vendor . ' Topup Alert ' . time());

    });


  }

}
