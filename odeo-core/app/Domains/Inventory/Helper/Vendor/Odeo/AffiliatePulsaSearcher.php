<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;
use Odeo\Domains\Transaction\Helper\Currency;

class AffiliatePulsaSearcher {

  private $pulsaOdeo, $serviceDetails, $marginFormatter, $currency, $cashInserter, $inquiryFees;

  public function __construct() {
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->currency = app()->make(Currency::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->inquiryFees = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateInquiryFeeRepository::class);
  }

  public function getByCategory(PipelineListener $listener, $data) {
    $temp = [];
    foreach ($this->pulsaOdeo->getByCategory(PulsaCode::getConstValueByKey(strtoupper($data['category_code']))) as $item) {
      $temp[] = [
        'code' => $item->inventory_code,
        'name' => $item->name
      ];
    }

    if (sizeof($temp) > 0) {
      $fee = $this->inquiryFees->findByUserId($data['auth']['user_id']);
      $fee = $fee && $fee->product_code_fee != null ? $fee->product_code_fee : Affiliate::DEFAULT_PRODUCT_CODE_FEE;
      if ($fee > 0) {
        try {
          $this->cashInserter->add([
            'user_id' => $data['auth']['user_id'],
            'trx_type' => TransactionType::AFFILIATE_INQUIRY_FEE,
            'cash_type' => CashType::OCASH,
            'amount' => -$fee,
            'data' => json_encode([
              'type' => 'products'
            ])
          ]);
          $this->cashInserter->run();
        }
        catch (InsufficientFundException $e) {
          return $listener->response(400, 'Saldo Anda tidak mencukupi');
        }
      }

      return $listener->response(200, ['products' => $temp]);
    }
    return $listener->response(204);
  }

  public function getCodePrice(PipelineListener $listener, $data) {
    if ($product = $this->pulsaOdeo->findByInventoryCode($data['product_code'])) {
      $fee = $this->inquiryFees->findByUserId($data['auth']['user_id']);
      $fee = $fee && $fee->product_price_fee != null ? $fee->product_price_fee : Affiliate::DEFAULT_PRODUCT_PRICE_FEE;
      if ($fee > 0) {
        try {
          $this->cashInserter->add([
            'user_id' => $data['auth']['user_id'],
            'trx_type' => TransactionType::AFFILIATE_INQUIRY_FEE,
            'cash_type' => CashType::OCASH,
            'amount' => -$fee,
            'data' => json_encode([
              'type' => 'product_price'
            ])
          ]);
          $this->cashInserter->run();
        }
        catch (InsufficientFundException $e) {
          return $listener->response(400, 'Saldo Anda tidak mencukupi');
        }
      }

      return $listener->response(200, [
        'code' => $product->inventory_code,
        'name' => $product->name,
        'price' => $this->currency->formatPrice($this->marginFormatter->formatMargin($product->price, [
          'service_detail' => $this->serviceDetails->findById($product->service_detail_id),
          'default' => true
        ])['sale_price'])
      ]);
    }
    return $listener->response(204);
  }

  public function getCodeDescription(PipelineListener $listener, $data) {
    if ($product = $this->pulsaOdeo->findByInventoryCode($data['product_code'])) {
      $description = isset($product->description) && $product->description != '' ? explode('<br>', $product->description) : [];
      if (sizeof($description) > 0) {
        $fee = $this->inquiryFees->findByUserId($data['auth']['user_id']);
        $fee = $fee && $fee->product_description_fee != null ? $fee->product_description_fee : Affiliate::DEFAULT_PRODUCT_DESCRIPTION_FEE;
        if ($fee > 0) {
          try {
            $this->cashInserter->add([
              'user_id' => $data['auth']['user_id'],
              'trx_type' => TransactionType::AFFILIATE_INQUIRY_FEE,
              'cash_type' => CashType::OCASH,
              'amount' => -$fee,
              'data' => json_encode([
                'type' => 'product_description'
              ])
            ]);
            $this->cashInserter->run();
          }
          catch (InsufficientFundException $e) {
            return $listener->response(400, 'Saldo Anda tidak mencukupi');
          }
        }

        return $listener->response(200, [
          'code' => $product->inventory_code,
          'name' => $product->name,
          'description' => $description
        ]);
      }
      return $listener->response(204);
    }
    return $listener->response(204);
  }

}