<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Affiliate\Jobs\AffiliateTelegramBroadcaster;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Vendor\Jabber\Jobs\ReplySocket;

class PulsaH2HBroadcaster {

  private $pulsaH2hUsers, $pulsaH2hGroupDetails, $currency, $affiliates, $telegramUsers, $jabberUsers, $jabberStores;

  public function __construct() {
    $this->pulsaH2hUsers = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hUserRepository::class);
    $this->pulsaH2hGroupDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hGroupDetailRepository::class);
    $this->currency = app()->make(Currency::class);
    $this->affiliates = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateRepository::class);
    $this->telegramUsers = app()->make(\Odeo\Domains\Vendor\Telegram\Repository\TelegramUserRepository::class);
    $this->jabberUsers = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberUserRepository::class);
    $this->jabberStores = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberStoreRepository::class);
  }

  public function groupBroadcast(PipelineListener $listener, $data) {
    $userIds = [];
    foreach ($this->pulsaH2hUsers->getUserIdByGroupId($data['h2h_group_id']) as $item) {
      $userIds[] = $item->user_id;
    }

    $isBroadcast = false;

    if (count($userIds) > 0) {
      $category = '';
      $message = "Dear Mitra,\n";
      $message .= 'Berikut update harga terbaru dari ODEO (' . date('d/m/Y') . ')' . "\n\n";
      foreach ($this->pulsaH2hGroupDetails->getByGroupId($data['h2h_group_id']) as $item) {
        if ($category != '' && $category != $item->category) $message .= "\n";
        $message .= $item->inventory_code . ' - ' . $item->name . ': ' . $this->currency->formatPrice($item->price)['formatted_amount'] . "\n";
        $category = $item->category;
      }

      foreach ($this->telegramUsers->getByUserId($userIds) as $item) {
        if ($item->chat_id != null) {
          $isBroadcast = true;
          dispatch(new AffiliateTelegramBroadcaster($item->chat_id, $message));
        }
      }

      $jabberStore = $this->jabberStores->findById(3);

      foreach ($this->jabberUsers->getByUserId($userIds) as $item) {
        if ($item->email != null) {
          dispatch(new ReplySocket(InlineJabber::JAXL_CHAT, [
            'to' => $item->email,
            'message' => $message
          ]));
          if ($jabberStore->socket_path != null)
            dispatch(new ReplySocket(InlineJabber::JAXL_CHAT, [
              'to' => $item->email,
              'message' => $message
            ], $jabberStore->socket_path));
        }
      }
    }

    if (!$isBroadcast) return $listener->response(400, 'No user can be broadcasted in this group');
    return $listener->response(200);
  }

}