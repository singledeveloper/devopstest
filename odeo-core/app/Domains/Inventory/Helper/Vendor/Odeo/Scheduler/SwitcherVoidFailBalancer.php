<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Scheduler;

use Odeo\Domains\Constant\SwitcherConfig;

class SwitcherVoidFailBalancer {

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  }

  public function run() {

    foreach ($this->switcherOrder->getAllVoidFailStatus() as $item) {
      $item->status = SwitcherConfig::SWITCHER_COMPLETED;
      $this->switcherOrder->save($item);
    }

  }

}
