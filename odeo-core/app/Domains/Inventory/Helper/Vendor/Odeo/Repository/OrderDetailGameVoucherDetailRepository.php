<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/21/17
 * Time: 12:51 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailGameVoucherDetail;

class OrderDetailGameVoucherDetailRepository extends Repository {

  public function __construct(OrderDetailGameVoucherDetail $orderDetailGameVoucherDetail) {
    $this->model = $orderDetailGameVoucherDetail;
  }

}