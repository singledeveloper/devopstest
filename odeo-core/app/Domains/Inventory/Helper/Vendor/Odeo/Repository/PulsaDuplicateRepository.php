<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaDuplicate;

class PulsaDuplicateRepository extends Repository {

  public function __construct(PulsaDuplicate $pulsaDuplicate) {
    $this->model = $pulsaDuplicate;
  }

  public function findSame($number, $inventoryId) {
    return $this->model->where('number', $number)
      ->where('inventory_id', $inventoryId)
      ->where('is_active', true)
      ->whereNull('new_order_id')
      ->first();
  }

}
