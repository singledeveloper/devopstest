<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaOdeoReport;

class PulsaOdeoReportRepository extends Repository {

  public function __construct(PulsaOdeoReport $pulsaOdeoReport) {
    $this->model = $pulsaOdeoReport;
  }

  public function getLastDate() {
    return $this->getCloneModel()->orderBy('report_date', 'desc')->select('report_date')->first();
  }

  public function getAllReportByDate($date) {
    return $this->getCloneModel()->where('report_date', $date)->get();
  }

}
