<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Carbon\Carbon;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaOdeo;

class PulsaOdeoRepository extends Repository {

  public function __construct(PulsaOdeo $pulsaOdeo) {
    $this->model = $pulsaOdeo;
  }

  public function findByCategory($categoryName) {
    return $this->model->where('category', $categoryName)
      ->where('status', Pulsa::STATUS_OK)
      ->whereNull('owner_store_id')
      ->where('is_active', true)
      ->orderBy('price', 'asc')
      ->first();
  }

  public function getByCategory($categoryName) {
    return $this->model->where('category', $categoryName)
      ->where('status', Pulsa::STATUS_OK)
      ->whereNull('owner_store_id')
      ->where('is_active', true)
      ->orderBy('name', 'asc')
      ->get();
  }

  public function findByNominal($nominal, $operatorId) {
    if (intval($nominal) != $nominal) return false;
    return $this->model->where('nominal', $nominal)->where('operator_id', $operatorId)
      ->where('is_active', true)
      ->where('status', Pulsa::STATUS_OK)
      ->whereNull('owner_store_id')
      ->first();
  }

  public function findByServiceDetailId($serviceDetailId, $inventoryIds = []) {
    $query = $this->model->where('service_detail_id', $serviceDetailId)
      ->where('is_active', true)
      ->whereIn('status', [Pulsa::STATUS_OK, Pulsa::STATUS_OK_SUPPLIER])
      ->where(function ($query) use ($inventoryIds) {
        if (isNewDesign()) {
          if (count($inventoryIds) > 0) {
            $query->whereIn('id', $inventoryIds);
          } else {
            $query->whereNull('owner_store_id');
          }
        } else {
          $query->whereNull('owner_store_id');
          if (count($inventoryIds) > 0) $query->orWhereIn('id', $inventoryIds);
        }
      });

    if ($serviceDetailId == ServiceDetail::BPJS_KES_ODEO)
      $query = $query->orderBy('nominal', 'asc');

    if ($serviceDetailId == ServiceDetail::PDAM_ODEO)
      $query = $query->orderBy('name', 'asc');
    else $query = $query->orderBy('category', 'asc')
      ->orderBy('price', 'asc')
      ->orderBy('name', 'asc');

    return $query->get();
  }

  public function findByName($name, $active = true) {
    return $this->model->whereRaw('LOWER(name) = ?', [$name])
      ->where('status', Pulsa::STATUS_OK)
      ->whereNull('owner_store_id')
      ->where('is_active', $active)
      ->first();
  }

  public function findAllActive($data) {
    $query = $this->model->where('pulsa_odeos.is_active', true);

    $status = [Pulsa::STATUS_OK];
    if (isset($data['is_supply'])) $status[] = Pulsa::STATUS_OK_SUPPLIER;

    return $query
      ->select('pulsa_odeos.*', 'service_details.service_id')
      ->join('service_details','service_details.id','=','pulsa_odeos.service_detail_id')
      ->whereIn('status', $status)
      ->whereNull('owner_store_id')
      ->orderBy('category', 'asc')
      ->orderBy('price', 'asc')->get();
  }

  public function findByInventoryCode($code) {
    return $this->model->where('inventory_code', $code)
      ->where('is_active', true)
      ->whereNull('owner_store_id')
      ->first();
  }

  public function getByOperator($operatorId, $category = [], $inventoryIds = []) {

    $query = $this->model;
    if (count($category) > 0) $query = $query->whereIn('category', $category);
    return $query->where('operator_id', $operatorId)
      ->where('is_active', true)
      ->whereIn('status', [Pulsa::STATUS_OK, Pulsa::STATUS_OK_SUPPLIER])
      ->where(function ($query) use ($inventoryIds) {
        if (isNewDesign()) {
          if (count($inventoryIds) > 0) {
            $query->whereIn('id', $inventoryIds);
          } else {
            $query->whereNull('owner_store_id');
          }
        } else {
          $query->whereNull('owner_store_id');
          if (count($inventoryIds) > 0) $query->orWhereIn('id', $inventoryIds);
        }
      })
      ->orderBy('operator_id', 'asc')
      ->orderBy('category', 'asc')
      ->orderBy('price', 'asc')
      ->select('id', 'inventory_code', 'category', 'name', 'service_detail_id', 'price as base_price',
        'nominal', 'description', 'link', 'status', 'owner_store_id', 'replicate_flag', 'srp_price')->get();
  }

  public function gets($data) {
    if (!isset($data['ignore_active'])) {
      if (isset($data['only_hidden'])) $data['status'] = Pulsa::STATUS_OK_HIDDEN;
      else if (!isset($data['status'])) $data['status'] = Pulsa::STATUS_OK;
      if (!is_array($data['status'])) $data['status'] = explode(',', $data['status']);
    }

    $query = $this->model->where(function ($subquery) use ($data) {
      $subquery->whereNull('owner_store_id');
      if (isset($data['inventory_ids'])) $subquery->orWhereIn('id', $data['inventory_ids']);
    });

    if (isset($data['status'])) $query = $query->whereIn('status', $data['status'])
      ->where('is_active', true);
    else $query = $query->where('status', '<>', Pulsa::STATUS_PERMANENT_DELETED);

    return $query->orderBy('operator_id', 'asc')
      ->orderBy('service_detail_id', 'asc')
      ->orderBy('category', 'asc')
      ->orderBy('price', 'asc')->get();
  }

  public function getAllCategory() {
    return $this->model->select('category', 'operator_id')->distinct()
      ->whereNull('owner_store_id')
      ->whereIn('service_detail_id', [
        ServiceDetail::PULSA_ODEO,
        ServiceDetail::PLN_ODEO,
        ServiceDetail::BOLT_ODEO,
        ServiceDetail::PAKET_DATA_ODEO,
        ServiceDetail::TRANSPORTATION_ODEO,
        ServiceDetail::PDAM_ODEO,
        ServiceDetail::PGN_ODEO,
        ServiceDetail::EMONEY_ODEO,
        ServiceDetail::GAME_VOUCHER_ODEO
      ])
      ->orderBy('category', 'asc')
      ->get();
  }

  public function getAllCategoryByServiceDetailId($serviceDetailId, $skipCategories = [], $onlyCategories = [], $active = true) {
    $query = $this->model->select('category', 'operator_id')->distinct()
//      ->whereNull('owner_store_id')
      ->where('is_active', $active)
      ->where('status', Pulsa::STATUS_OK)
      ->where('service_detail_id', $serviceDetailId);

    if (count($skipCategories) > 0) {
      $query->whereNotIn('category', $skipCategories);
    }
    if (count($onlyCategories) > 0) {
      $query->whereIn('category', $onlyCategories);
    }

    return $query->orderBy('category', 'asc')
      ->get();
  }

  public function findByServiceDetailIdAndCategory($serviceDetailId, $category, $inventoryIds = []) {
    return $this->model->where('service_detail_id', $serviceDetailId)
      ->where('is_active', true)
      ->whereIn('status', [Pulsa::STATUS_OK, Pulsa::STATUS_OK_SUPPLIER])
      ->where(function ($query) use ($inventoryIds) {
        if (isNewDesign()) {
          if (count($inventoryIds) > 0) {
            $query->whereIn('id', $inventoryIds);
          } else {
            $query->whereNull('owner_store_id');
          }
        } else {
          $query->whereNull('owner_store_id');
          if (count($inventoryIds) > 0) $query->orWhereIn('id', $inventoryIds);
        }

      })
      ->where('category', $category)
      ->orderBy('price', 'asc')
      ->get();
  }

  public function getAllWithPagination($active = true) {
    $filters = $this->getFilters();

    $query = $this->model
      ->whereNull('owner_store_id')
      ->orderBy('operator_id', 'asc')
      ->orderBy('service_detail_id', 'asc')
      ->orderBy('category', 'asc')
      ->orderBy('price', 'asc');
    if ($active) $query = $query->where('status', Pulsa::STATUS_OK);

    if (isset($filters['search'])) {
      if (isset($filters['search']['name'])) {
        $query->where('name', 'ilike', '%' . $filters['search']['name'] . '%');
      }
    }

    return $this->getResult($query);
  }

  public function findSameCodeFromStore($code, $storeId) {
    return $this->model->where('inventory_code', $code)
      ->where('owner_store_id', $storeId)->where('status', Pulsa::STATUS_OK)->first();
  }

  public function getByReplicationFlag($replicationFlagId) {
    return $this->model->where('replicate_flag', $replicationFlagId)->get();
  }

  public function getByCategories($categories) {
    return $this->model
      ->select('pulsa_odeos.*', 'service_details.service_id')
      ->join('service_details', 'service_details.id', '=', 'pulsa_odeos.service_detail_id')
      ->where('pulsa_odeos.is_active', true)
      ->where('status', Pulsa::STATUS_OK)
      ->whereIn('category', $categories)
      ->orderBy('category', 'asc')
      ->get();
  }

  public function getByIds($ids) {
    return $this->getCloneModel()->whereIn('id', $ids)->get();
  }

  public function getAllSKU($isHaveOwner = false, $mustActive = false) {
    $query = $this->getCloneModel()->whereNotIn('status', [
      Pulsa::STATUS_OK_SUPPLIER, Pulsa::STATUS_PERMANENT_DELETED
    ]);

    if ($mustActive) $query = $query->where('is_active', true);
    if ($isHaveOwner) $query = $query->whereNotNull('owner_store_id');
    else $query = $query->whereNull('owner_store_id');

    $result = $query->select(\DB::raw('distinct(replicate_flag)'))->get();

    $ids = [];
    foreach ($result as $item) $ids[] = $item->replicate_flag;
    return $ids;
  }

  public function getAllOOS() {
    return \DB::select(\DB::raw("
      select po.id, po.name from pulsa_odeos po
      where po.status not in ('" . Pulsa::STATUS_OK_SUPPLIER . "', '" . Pulsa::STATUS_PERMANENT_DELETED . "')
      and po.owner_store_id is null
      and (select count(id)
        from pulsa_odeo_inventories poi
        where poi.pulsa_odeo_id = po.id and poi.is_active = true) = 0
      order by po.name asc"));
  }

  public function getDeadStock() {
    return $this->getCloneModel()->whereNull('owner_store_id')
      ->where('is_active', true)
      ->whereIn('status', [Pulsa::STATUS_OK, Pulsa::STATUS_OK_HIDDEN])
      ->where('category', '<>', PostpaidType::BPJS_KES)
      ->where('service_detail_id', '<>', ServiceDetail::GAME_VOUCHER_ODEO)
      ->where(function($query){
        $query->whereIn('category', PulsaCode::groupData())
          ->orWhere('price', '<=', 150000);
      })
      ->where(function($query){
        $query->whereNull('last_succeeded_at')
          ->orWhere('last_succeeded_at', '<', Carbon::now()->subMonth()->toDateTimeString());
      })
      ->orderByRaw('last_succeeded_at asc nulls first')
      ->orderBy('name', 'asc')->select('name', 'last_succeeded_at')->get();
  }

  public function getAllOdeoPulsaWithOperator() {
    return $this->getCloneModel()->whereNotNull('operator_id')
      ->where('service_detail_id', ServiceDetail::PULSA_ODEO)
      ->whereNotNull('nominal')
      ->whereNull('owner_store_id')
      ->where('is_active', true)
      ->where('status', Pulsa::STATUS_OK)
      ->orderBy('nominal', 'desc')
      ->get();
  }

}
