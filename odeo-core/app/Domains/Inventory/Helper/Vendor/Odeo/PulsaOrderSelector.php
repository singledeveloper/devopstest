<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:54 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Constant\SwitcherConfig;

class PulsaOrderSelector implements SelectorListener {

  private $currencyHelper, $switcherOrder, $vendorRecons;

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->vendorRecons = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
  }

  public function _transforms($item, Repository $repository) {

    $output['order_detail_pulsa_switcher_id'] = $item->id;
    $output['created_at'] = (string) (isset($item->requested_at) && $item->requested_at != NULL ? $item->requested_at : $item->created_at);
    $output['updated_at'] = (string) $item->updated_at;
    $output['current_biller_name'] = $item->current_biller_name;

    if ($item->status == SwitcherConfig::SWITCHER_IN_QUEUE) $output['status'] = "IN_QUEUE";
    else if ($item->status == SwitcherConfig::SWITCHER_FAIL) $output['status'] = "FAILED";
    else if ($item->status == SwitcherConfig::SWITCHER_COMPLETED) $output['status'] = "COMPLETED";
    else if ($item->status == SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER) $output['status'] = "WRONG NUMBER";
    else if ($item->status == SwitcherConfig::SWITCHER_FAIL_UNDEFINED_ROUTE) $output['status'] = "UNDEFINED ROUTE";
    else if ($item->status == SwitcherConfig::SWITCHER_REFUNDED) $output['status'] = "REFUNDED";
    else if ($item->status == SwitcherConfig::SWITCHER_VOID_IN_QUEUE) $output['status'] = "VOID IN QUEUE";
    else if ($item->status == SwitcherConfig::SWITCHER_VOID_FAIL) $output['status'] = "VOID FAILED";
    else if ($item->status == SwitcherConfig::SWITCHER_VOIDED) $output['status'] = "VOID COMPLETED";
    else $output['status'] = "PENDING";

    $output['number'] = revertTelephone($item->number);
    $output['name'] = $item->name;
    $output['price'] = $this->currencyHelper->formatPrice($item->current_base_price);
    if (isset($item->subsidy_amount)) $output['subsidy'] = $this->currencyHelper->formatPrice($item->subsidy_amount + ($item->current_base_price - $item->first_base_price));
    $output['order_detail_id'] = $item->order_detail_id;
    $output['order_id'] = $item->order_id;

    $diff = (in_array($output['status'], ['COMPLETED', 'REFUNDED']) ? strtotime($item->updated_at) : time()) - strtotime($item->requested_at != NULL ? $item->requested_at : $item->created_at);
    $minutes = floor($diff / 60);
    $seconds = $diff - ($minutes * 60);
    $output["lead_time"] = "";
    if ($minutes > 0) $output["lead_time"] .= $minutes . ($minutes > 1 ? " minutes " : " minute ");
    if ($seconds > 0) $output["lead_time"] .= $seconds . ($seconds > 1 ? " seconds" : " second");
    else if ($minutes == 0) $output["lead_time"] = "< 1 second";

    if (isset($item->serial_number)) $output['serial_number'] = $item->serial_number;
    $output['admin_note'] = $item->admin_note;
    $output['owner_store_id'] = $item->owner_store_id;
    $output['owner_store_name'] = $item->owner_store_name;
    $output['user_note'] = Supplier::getFailedReasonToUser($item->current_response_status_code, $item->status);

    return $output;

  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getOrder(PipelineListener $listener, $data) {

    $orders = [];

    $this->switcherOrder->normalizeFilters($data);
    $this->switcherOrder->setSimplePaginate(true);

    foreach($this->switcherOrder->gets() as $item) {
      $orders[] = $this->_transforms($item, $this->switcherOrder);
    }

    if (sizeof($orders) > 0)
      return $listener->response(200, array_merge(
        ["orders" => $this->_extends($orders, $this->switcherOrder)],
        $this->switcherOrder->getPagination()
      ));

    return $listener->response(204, ["orders" => []]);
  }

  public function getLog(PipelineListener $listener, $data) {
    $logs = [];

    foreach ($this->vendorRecons->getLog($data['order_detail_pulsa_switcher_id']) as $item) {
      $row = [
        'biller_name' => $item->vendor_switcher_name,
        'request' => json_decode($item->request) ? json_decode($item->request) : $item->request,
        'response' => json_decode($item->response) ? json_decode($item->response) : $item->response,
        'notify' => json_decode($item->notify) ? json_decode($item->notify) : $item->notify,
        'date' => $item->created_at->format('Y-m-d H:i:s')
      ];

      if ($item->status == SwitcherConfig::BILLER_IN_QUEUE) $row['status'] = "IN QUEUE";
      else if ($item->status == SwitcherConfig::BILLER_TIMEOUT) $row['status'] = 'TIMEOUT';
      else if ($item->status == SwitcherConfig::BILLER_SUSPECT) $row['status'] = 'SUSPECT';
      else if ($item->status == SwitcherConfig::BILLER_SUCCESS) $row['status'] = "SUCCESS";
      else if ($item->status == SwitcherConfig::BILLER_FAIL) $row['status'] = "FAILED";
      else if ($item->status == SwitcherConfig::BILLER_FAKE_SUCCESS) $row['status'] = 'FAKE SUCCESS';
      else if ($item->status == SwitcherConfig::BILLER_CREATED) $row['status'] = "PENDING";
      else if ($item->status == SwitcherConfig::BILLER_TESTING) $row['status'] = "TEST PURPOSE";
      else if ($item->status == SwitcherConfig::BILLER_DUPLICATE_SUCCESS) $row['status'] = 'DUPLICATE SUCCESS';
      else if ($item->status == SwitcherConfig::BILLER_VOID_FAIL) $row['status'] = 'VOID FAILED';
      else if ($item->status == SwitcherConfig::BILLER_VOIDED) $row['status'] = 'VOID SUCCESS';
      else $row['status'] = "UNKNOWN";

      $logs[] = $row;
    }

    return $listener->response(200, ["logs" => $logs]);
  }

  public function checkRepeat(PipelineListener $listener, $data) {
    $same = $this->switcherOrder->getSamePurchaseCount(purifyTelephone($data['number']), $data['item_id'], 720, true);
    if (count($same) > 0) {
      return $listener->response(200, [
        'name' => $same[0]->currentInventory->pulsa->name,
        'number' => purifyTelephone($data['number']),
        'counts' => count($same),
        'hours_within' => 12
      ]);
    }
    return $listener->response(204);
  }

  public function getRecent(PipelineListener $listener, $data) {
    $recentOrders = [];

    $this->switcherOrder->normalizeFilters($data);
    $this->switcherOrder->setSimplePaginate(true);

    foreach ($this->switcherOrder->getRecent($data['auth']['user_id']) as $item) {
      $recentOrders[] = [
        'number' => revertTelephone($item->number),
        'order_total' => $item->order_total
      ];
    }

    if (sizeof($recentOrders) > 0)
      return $listener->response(200, array_merge(
        ["recent_orders" => $this->_extends($recentOrders, $this->switcherOrder)],
        $this->switcherOrder->getPagination()
      ));

    return $listener->response(204, ["recent_orders" => []]);
  }

}
