<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Core\PipelineListener;

class PulsaInventoryEditor {

  private $pulsaInventories, $pulsaOdeo, $pulsaOdeoHistories, $telegramManager;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->pulsaOdeoHistories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoHistoryRepository::class);
    $this->telegramManager = app()->make(\Odeo\Domains\Vendor\Telegram\TelegramManager::class);
  }

  public function edit(PipelineListener $listener, $data) {
    if (isset($data['vendor_switcher_id'])) {
      if ($data['type'] == 'disable-all')
        $this->pulsaInventories->disableInventory($data['vendor_switcher_id']);
      else if ($data['type'] == 'refresh')
        $this->pulsaInventories->updateActiveAllSeeded($data['vendor_switcher_id']);
      return $listener->response(200);
    }
    else if (isset($data['is_base_price_change_fixed']) && $data['is_base_price_change_fixed'] != "false") {
      $pulsa = $this->pulsaOdeo->findById($data["item_id"]);
      if (isset($data['inventory_code'])) $pulsa->inventory_code = $data['inventory_code'];
      if (isset($data['is_active'])) $pulsa->is_active = $data['is_active'];
      if (isset($data['is_remove'])) {
        $pulsa->status = Pulsa::STATUS_PERMANENT_DELETED;
        $pulsa->is_active = false;
        $this->pulsaOdeo->save($pulsa);
      }
      else {
        $oldPulsaDescription = $pulsa->description;
        $pulsaDescription = isset($data['description']) ? str_replace(["\r\n", "\n", "\r"], "<br>", $data['description']) : '';
        $pulsaName = isset($data['name']) ? $data['name'] : '';
        $pulsaLink = isset($data['link']) ? $data['link'] : '';
        $srpPrice = $data['srp_price'] ?? 0;

        if ($pulsaDescription != '') $pulsa->description = $pulsaDescription;
        if ($pulsaName != '') $pulsa->name = $pulsaName;
        if ($pulsaLink != '') $pulsa->link = $pulsaLink;
        if ($srpPrice) $pulsa->srp_price = $srpPrice;
        if (isset($data['status'])) $pulsa->status = $data['status'];
        if (isset($data['nominal']) && $data['nominal'] != '') $pulsa->nominal = $data['nominal'];
        $this->pulsaOdeo->save($pulsa);

        foreach ($this->pulsaOdeo->getByReplicationFlag($pulsa->replicate_flag) as $item) {
          if ($item->id != $pulsa->id) {
            $item->name = $pulsa->name;
            $item->link = $pulsa->link;

            if ($item->description == $oldPulsaDescription)
              $item->description = $pulsa->description;

            $this->pulsaOdeo->save($item);
          }
        }

        if (isset($data['base_price']) && $data['base_price'] != $pulsa->price) {
          if ($pulsa->price - ceil(5/100 * $pulsa->price) > $data['base_price']) {
            $isBaseChanged = false;
            $flagPulsaInventories = $this->pulsaInventories->getByPulsaOdeoId($pulsa->id, 5);
            if (sizeof($flagPulsaInventories) <= 0)
              $flagPulsaInventories = $this->pulsaInventories->getByPulsaOdeoId($pulsa->id);
            foreach ($flagPulsaInventories as $item) {
              if ($data['base_price'] < $item->base_price) {
                $data['base_price'] = $item->base_price;
                $isBaseChanged = true;
              }
            }
            if ($isBaseChanged) $data['base_price'] += $pulsa->serviceDetail->referral_cashback;
            else if ($data['base_price'] > $pulsa->price) $data['base_price'] = $pulsa->price;
          }
          if ($data['base_price'] != $pulsa->price) {
            $this->pulsaOdeoHistories->getModel()->where('is_changed', false)
              ->where('pulsa_odeo_id', $pulsa->id)->update(['is_changed' => true]);
            $history = $this->pulsaOdeoHistories->getNew();
            $history->pulsa_odeo_id = $pulsa->id;
            $history->price = $data['base_price'];
            $history->changed_at = isset($data['changed_at']) && $data['changed_at'] != '' ? $data['changed_at'] : date('Y-m-d H:i:s');
            if (isset($data['auth']) && isset($data['auth']['user_id']))
              $history->changed_user_id = $data['auth']['user_id'];
            $this->pulsaOdeoHistories->save($history);

            if ($pulsa->status == Pulsa::STATUS_OK) {
              $code = strtoupper(PulsaCode::getConstKeyByValue($pulsa->category));
              $this->telegramManager->postChannel($pulsa->name . ($data['base_price'] >= $pulsa->price ? ' akan mengalami penyesuaian harga' : ' TURUN HARGA')
                . ' pada tanggal ' . date('d/m/Y H:i', strtotime($history->changed_at)) . '. Ketik HARGA.' . $code . ' untuk pengecekan di jam tersebut');
            }
          }
        }
      }

      $table = $this->pulsaOdeo->getModel()->getTable();
      $cache = app()->make(\Illuminate\Contracts\Cache\Repository::class);
      $cache->tags($table)->forget($table . '.filter.operator.' . $pulsa->operator_id . '.pulsa');
      $cache->tags($table)->forget($table . '.filter.operator.' . $pulsa->operator_id . '.paket.data');
      $cache->tags($table)->forget($table . '.filter.operator.' . $pulsa->operator_id . '.sms.telp.packages');
      $cache->tags($table)->forget($table . '.filter.operator.' . $pulsa->operator_id);

      return $listener->response(200, ["item" => $pulsa]);
    }
    else {
      $inventory = $this->pulsaInventories->findById($data["item_id"]);
      if (isset($data['base_price'])) $inventory->base_price = $data['base_price'];
      if (isset($data['is_active'])) {
        $inventory->is_active = $data['is_active'];
        $inventory->is_seeded = $data['is_active'];
        if ($inventory->is_active && $inventory->base_price - $inventory->pulsa->price > Pulsa::DIFFERENCE_PRICE_LIMIT)
          return $listener->response(400, "You can't input this inventory. Too expensive. Please change the default price up first.");
      }
      if (isset($data['is_remove'])) {
        $inventory->is_removed = true;
        $inventory->is_active = false;
        $inventory->is_seeded = false;
      }
      if (isset($data['change_priority'])) {
        if ($inventory->pulsa->category == PulsaCode::GOJEK)
          return $listener->response(400, 'Anda tidak dapat melakukan perubahan urutan untuk produk ini.');
        $this->pulsaInventories->disableSort($inventory->pulsa_odeo_id);
        $inventory->priority_sort = 1;
      }
      $this->pulsaInventories->save($inventory);
      return $listener->response(200, ["item" => $inventory]);
    }
  }

  public function add(PipelineListener $listener, $data) {
    $pulsa = $this->pulsaOdeo->findById($data['pulsa_odeo_id']);
    if ($data['base_price'] - $pulsa->price > Pulsa::DIFFERENCE_PRICE_LIMIT) {
      return $listener->response(400, "You can't input this inventory. Too expensive. Please change the default price up first.");
    }

    if (!$inventory = $this->pulsaInventories->findSame($data['pulsa_odeo_id'], $data['vendor_switcher_id'], $data['inventory_code'])) {
      $inventory = $this->pulsaInventories->getNew();
      $inventory->pulsa_odeo_id = $data['pulsa_odeo_id'];
      $inventory->vendor_switcher_id = $data['vendor_switcher_id'];
      $inventory->code = $data['inventory_code'];
    }

    $inventory->base_price = $data['base_price'];
    $inventory->is_seeded = true;
    $inventory->is_active = true;

    $this->pulsaInventories->save($inventory);

    return $listener->response(200);
  }

  public function addPulsa(PipelineListener $listener, $data) {
    $pulsa = $this->pulsaOdeo->getNew();

    $inventoryManager = app()->make(\Odeo\Domains\Inventory\Helper\InventoryManager::class);
    $pulsaData = $inventoryManager->getServiceDataFromCategory($data['category']);

    if (!isset($pulsaData['service_detail_id'])) return $listener->response(400, 'Category invalid');

    $pulsa->service_detail_id = $pulsaData['service_detail_id'];
    if (isset($pulsaData['operator_id'])) $pulsa->operator_id = $pulsaData['operator_id'];

    $nominal = isset($data['nominal']) && $data['nominal'] != '' ? $data['nominal'] : 0;
    if ($nominal == 0 && $data['category'] == PulsaCode::GOJEK)
      return $listener->response(400, 'Nominal must be inputted.');

    $pulsa->category = $data['category'];
    $pulsa->nominal = $nominal;
    $pulsa->price = $data['price'];
    $pulsa->is_active = false;
    $pulsa->name = $data['name'];
    $pulsa->inventory_code = $data['inventory_code'];
    $pulsa->description = $data['description'];
    $pulsa->link = $data['link'];
    $pulsa->status = $data['status'];

    $this->pulsaOdeo->save($pulsa);

    $pulsa->replicate_flag = $pulsa->id;
    $this->pulsaOdeo->save($pulsa);

    return $listener->response(200);
  }

  public function replicate(PipelineListener $listener, $data) {
    $pulsa = $this->pulsaOdeo->findById($data['pulsa_odeo_id']);
    $newPulsa = $pulsa->replicate();
    $newPulsa->name = $data['name'];
    $newPulsa->inventory_code = $data['inventory_code'];
    $newPulsa->price = $data['price'];
    $newPulsa->status = isset($data['status']) ? $data['status'] : Pulsa::STATUS_OK_HIDDEN;
    $newPulsa->last_succeeded_at = null;

    $this->pulsaOdeo->save($newPulsa);

    $newPulsa->replicate_flag = $newPulsa->id;
    $this->pulsaOdeo->save($newPulsa);

    foreach ($this->pulsaInventories->getByPulsaOdeoId($data['pulsa_odeo_id']) as $item) {
      $newItem = $item->replicate();
      $newItem->pulsa_odeo_id = $newPulsa->id;
      if ($newItem->base_price - $newPulsa->price > Pulsa::DIFFERENCE_PRICE_LIMIT) {
        $newItem->is_active = false;
        $newItem->is_seeded = false;
      }
      $this->pulsaInventories->save($newItem);
    }

    return $listener->response(200);
  }

}
