<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/21/17
 * Time: 12:52 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;


use Odeo\Domains\Core\Entity;

class OrderDetailGameVoucherDetail extends Entity {
  public function switcher() {
    return $this->belongsTo(OrderDetailPulsaSwitcher::class);
  }
}