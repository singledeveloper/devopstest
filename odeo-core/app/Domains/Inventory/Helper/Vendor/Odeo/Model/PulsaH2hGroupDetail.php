<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;

use Odeo\Domains\Core\Entity;

class PulsaH2hGroupDetail extends Entity
{
  function pulsa() {
    return $this->belongsTo(PulsaOdeo::class, 'pulsa_odeo_id');
  }

  function group() {
    return $this->belongsTo(PulsaH2hGroup::class, 'pulsa_h2h_group_id');
  }
}
