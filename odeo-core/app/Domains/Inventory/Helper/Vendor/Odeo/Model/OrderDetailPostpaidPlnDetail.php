<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;

use Odeo\Domains\Core\Entity;

class OrderDetailPostpaidPlnDetail extends Entity
{

  public function switcher() {
    return $this->belongsTo(OrderDetailPulsaSwitcher::class);
  }

}
