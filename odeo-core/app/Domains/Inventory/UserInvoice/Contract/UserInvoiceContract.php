<?php
namespace Odeo\Domains\Inventory\UserInvoice\Contract;

use Odeo\Domains\Core\PipelineListener;

interface UserInvoiceContract {

  public function setBiller($biller);

  public function validateInventory(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function completeOrder(PipelineListener $listener, $data);

}
