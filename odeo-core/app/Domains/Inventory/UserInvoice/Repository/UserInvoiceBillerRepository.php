<?php
namespace Odeo\Domains\Inventory\UserInvoice\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\UserInvoice\Model\UserInvoiceBiller;
use Odeo\Domains\Core\Repository;

class UserInvoiceBillerRepository extends Repository {

  public function __construct(UserInvoiceBiller $userInvoiceBiller) {
    $this->model = $userInvoiceBiller;
  }
  
}
