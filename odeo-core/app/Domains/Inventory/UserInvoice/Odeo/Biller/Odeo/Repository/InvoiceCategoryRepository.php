<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Model\InvoiceCategory;
use Odeo\Domains\Core\Repository;

class InvoiceCategoryRepository extends Repository {

  public function __construct(InvoiceCategory $invoiceCategory) {
    $this->model = $invoiceCategory;
  }

  public function get() {
    $query = $this->model;
    return $this->getResult($query->orderBy('id', 'asc'));
  }

}
