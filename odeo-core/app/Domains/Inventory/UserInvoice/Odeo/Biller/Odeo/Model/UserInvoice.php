<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Subscription\Model\Store;

class UserInvoice extends Entity {

  protected $casts = [
    'items' => 'array',
  ];

  protected $dates = ['due_date'];

  public function user() {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function store() {
    return $this->belongsTo(Store::class, 'store_id');
  }

  public function billedUser() {
    return $this->belongsTo(User::class, 'billed_user_id');
  }

  public function category() {
    return $this->belongsTo(InvoiceCategory::class, 'category_id');
  }

  public function invoicePayments() {
    return $this->hasMany(UserInvoicePayment::class, 'invoice_id', 'id');
  }
}
