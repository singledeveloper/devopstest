<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class InvoiceCategorySelector implements SelectorListener {

  public function __construct() {
    $this->invoiceCategories = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Repository\InvoiceCategoryRepository::class);
  }

  public function _extends($items, Repository $repository) {
    return $items;
  }

  public function _transforms($item, Repository $repository) {
    $data = [];

    $data['id'] = $item['id'];
    $data['name'] = $item['name'];

    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    $categories = [];

    foreach ($this->invoiceCategories->get() as $category) {
      $categories[] = $this->_transforms($category, $this->invoiceCategories);
    }

    if (count($categories) > 0) {
      return $listener->response(200, ['categories' => $this->_extends($categories, $this->invoiceCategories)]);
    }
    return $listener->response(204, [], false, true);
  }
}
