<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate;

use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Constant\UserInvoice;

class UserInvoiceUserSelector implements SelectorListener {

  public function __construct() {
    $this->userInvoiceUsers = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceUserRepository::class);
    $this->vaDirectSelector = app()->make(\Odeo\Domains\Payment\PaymentVaDirectSelector::class);
  }

  public function _extends($items, Repository $repository) {
    $virtualAccounts = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountDetailRepository::class);
    
    if ($userIds = $repository->beginExtend($items, 'user_id')) {
      if ($repository->hasExpand('virtual_accounts')) {
        $vas = $virtualAccounts->getVirtualAccountInfoByUserId($userIds);
        $results = [];
        foreach($vas as $va) {
          $result = $this->vaDirectSelector->_transforms($va, $repository);
          $results[$va->user_id][] = $result;
        }
        $repository->addExtend('virtual_accounts', $results);
      }
    }

    return $repository->finalizeExtend($items);
  }

  public function _transforms($item, Repository $repository) {
    $data = [];

    $data['user_id'] = $item->user_id;
    
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    $this->userInvoiceUsers->normalizeFilters($data);

    $invoiceUsers = [];

    foreach ($this->userInvoiceUsers->get() as $invoice) {
      $invoiceUsers[] = $this->_transforms($invoice, $this->userInvoiceUsers);
    }

    if (count($invoiceUsers) > 0) {
      return $listener->response(200, array_merge(
        ['users' => $this->_extends($invoiceUsers, $this->userInvoiceUsers)],
          $this->userInvoiceUsers->getPagination()
      ));
    }
    return $listener->response(204, [], false, true);
  }
}
