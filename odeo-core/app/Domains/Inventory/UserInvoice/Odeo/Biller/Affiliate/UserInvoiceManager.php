<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate;

use Odeo\Domains\Core\PipelineListener;

class UserInvoiceManager {

  public function validateInventory(PipelineListener $listener, $data) {
    return app()->make(InventoryValidator::class)->validateInventory($listener, $data);
  }

  public function completeOrder(PipelineListener $listener, $data) {
    return app()->make(Requester::class)->completeOrder($listener, $data);
  }
  
  public function initializeInvoiceUsers(PipelineListener $listener, $data) {
    return $listener->response(200);
  }
  
  public function inquiry(PipelineListener $listener, $data) {
    return $listener->response(200);
  }
  
  public function pay(PipelineListener $listener, $data) {
    return $listener->response(200);
  }

  public function terminalInquiry(PipelineListener $listener, $data) {
    return app()->make(Inquirer::class)->getByPhone($listener, $data);
  }
}
