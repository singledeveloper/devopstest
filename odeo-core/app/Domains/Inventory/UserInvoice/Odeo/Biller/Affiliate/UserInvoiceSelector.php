<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate;

use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Constant\UserInvoice;

class UserInvoiceSelector implements SelectorListener {

  public function __construct() {
    $this->userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository::class);
    $this->invoiceParser = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Helper\UserInvoiceParser::class);
    $this->vaDirectSelector = app()->make(\Odeo\Domains\Payment\PaymentVaDirectSelector::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function _extends($items, Repository $repository) {
    $virtualAccounts = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountDetailRepository::class);
    
    if ($billedUsers = $repository->beginExtend($items, 'billed_user_id')) {
      if ($repository->hasExpand('virtual_accounts')) {
        $vas = $virtualAccounts->getVirtualAccountInfoByUserId($billedUsers);
        $results = [];
        foreach($vas as $va) {
          $results[$va->service_reference_id][] = $this->vaDirectSelector->_transforms($va, $repository);
        }
        $repository->addExtend('virtual_accounts', $results);
      }
    }

    return $repository->finalizeExtend($items);
  }

  public function _transforms($item, Repository $repository) {
    $data = [];

    $data['invoice_number'] = $item->invoice_number;
    $data['biller_id'] = $item->biller_id;
    $data['name'] = $item->name;
    $data['period'] = $item->period;
    $data['items'] = $item->items;
    $data['billed_user_id'] = $item->billed_user_id;
    $data['billed_user_name'] = $item->billed_user_name;
    $data['billed_user_email'] = $item->billed_user_email;
    $data['billed_user_telephone'] = $item->billed_user_telephone;
    
    $data['status'] = $this->invoiceParser->parseStatus($item);

    if(isset($item->parent_invoice_id)) {
      $data['merged_to'] = $repository->findById($item->parent_invoice_id)->invoice_number;
    }

    $data['subtotal'] = $this->currency->getAmountOnly($item->subtotal);
    $data['charges'] = $this->currency->getAmountOnly($item->charges);

    if(!empty($item->child_invoice_ids)) {
      $childInvoices = $repository->getChildren($item->id);

      if(count($childInvoices)) {
        $data['total_previous'] = $this->currency->getAmountOnly($item->total_child + $item->total_charges_child);
        $data['merged_invoices'] = [];
        foreach($childInvoices as $childInvoice) {
          $temp = [];
          $temp['invoice_number'] = $childInvoice['invoice_number'];
          $temp['name'] = $childInvoice['name'];
          $temp['period'] = $childInvoice['period'];
          $temp['items'] = $childInvoice['items'];
          $temp['subtotal'] = $this->currency->getAmountOnly($childInvoice['subtotal']);
          $temp['charges'] = $this->currency->getAmountOnly($childInvoice['charges']);
          $temp['total'] = $this->currency->getAmountOnly($childInvoice['total'] - ($childInvoice['total_child'] + $childInvoice['total_charges_child']));

          $data['merged_invoices'][] = $temp;
        }
      }
    }

    $data['total'] = $this->currency->getAmountOnly($item->total - ($data['status'] == 'MERGED' ? $item->total_child + $item->total_charges_child : 0));
    $data['created_at'] = Carbon::parse($item->created_at)->toISO8601String();
    $data['due_date'] = Carbon::parse($item->due_date)->toDateString();

    if($repository->hasExpand('config')) {
      $data['late_charge'] = (float)$item->late_charge;
      $data['late_charge_interval'] = $item->late_charge_interval;
      $data['enable_partial_payment'] = $item->enable_partial_payment;
      $data['minimum_payment'] = (float)$item->minimum_payment;
    }

    $data['total_paid'] = $this->currency->getAmountOnly($item->total_paid);
    $data['paid_at'] = Carbon::parse($item->paid_at)->toISO8601String();
    $data['payments'] = [];

    foreach($item->invoicePayments as $invoicePayment) {
      $temp = [];
      $temp['paid_at'] = Carbon::parse($invoicePayment->paid_at)->toISO8601String();
      $temp['amount'] = $this->currency->getAmountOnly($invoicePayment->amount);
      $temp['payment_method'] = $invoicePayment->payment->information->name;
      $data['payments'][] = $temp;
    }
    
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    $this->userInvoices->normalizeFilters($data);

    $invoices = [];

    foreach ($this->userInvoices->get() as $invoice) {
      $invoices[] = $this->_transforms($invoice, $this->userInvoices);
    }

    if (count($invoices) > 0) {
      return $listener->response(200, array_merge(
        ['invoices' => $this->_extends($invoices, $this->userInvoices)],
          $this->userInvoices->getPagination()
      ));
    }
    return $listener->response(204, [], false, true);
  }

  public function getDetail(PipelineListener $listener, $data) {
    $invoice = $this->userInvoices->findByAttributes('invoice_number', $data['invoice_number']);

    if ($invoice) {
      return $listener->response(200, $this->_transforms($invoice, $this->userInvoices));
    }
    return $listener->response(204, [], false, true);
  }
}
