<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Jobs;

use Odeo\Jobs\Job;

class UserInvoiceNotifier extends Job  {

  private $data, $affiliates, $userInvoiceBiller;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;

    $this->userInvoiceBiller = app()->make(\Odeo\Domains\Inventory\UserInvoice\Repository\UserInvoiceBillerRepository::class);
    $this->affiliates = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateRepository::class);
  }

  public function handle() {
    try {

      $aff = $this->affiliates->findByUserId($this->data['user_id']);

      $client = new \GuzzleHttp\Client();

      $signature = \hash("sha256", $this->data['invoice_number'] . $this->data['total'] . $aff->user_id . $aff->secret_key);

      $request = [
        'invoice_number' => $this->data['invoice_number'],
        'total' => $this->data['total'],
        'signature' => $signature
      ];

      $result = $client->request('POST', $aff->notify_url, [
        'json' => $request,
        'timeout' => 60
      ]);

      $statusCode = $result->getStatusCode();
      $response = $result->getBody()->getContents();

      if (empty($response)) {
        $response = 'SUCCESS';
      }

      if ($statusCode != 200) {
        $response = 'FAIL';
      }

    } catch (\Exception $exception) {
      $request = null;
      $statusCode = $exception->getCode();
      $response = $exception->getMessage();
    }

    $affiliateNotifyLogs = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceNotifyLogRepository::class);
    $log = $affiliateNotifyLogs->getNew();
    $log->user_id = $this->data['user_id'];
    $log->invoice_number = $this->data['invoice_number'];
    $log->request = json_encode($request);
    $log->response = $response;
    $log->response_code = $statusCode;

    $affiliateNotifyLogs->save($log);
  }
}
