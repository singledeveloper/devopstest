<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\ApiManager;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Jobs\VoidInvoicePayment;
use Odeo\Domains\Order\Repository\OrderDetailRepository;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;
use Odeo\Domains\Transaction\Repository\CashTransactionRepository;

class Refunder {

  private $apiManager;
  private $affiliateUserInvoiceRepo;
  private $cashInserter;
  private $cashTransactionRepo;
  private $orderRepo;
  private $orderDetailRepo;

  public function __construct() {
    $this->apiManager = app()->make(ApiManager::class);
    $this->affiliateUserInvoiceRepo = app()->make(AffiliateUserInvoiceRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
    $this->cashTransactionRepo = app()->make(CashTransactionRepository::class);
    $this->orderRepo = app()->make(OrderRepository::class);
    $this->orderDetailRepo = app()->make(OrderDetailRepository::class);
  }

  public function refund(PipelineListener $listener, $data) {
    $order = $this->orderRepo->findById($data['order_id']);
    $orderDetail = $this->orderDetailRepo->findByOrderId($data['order_id']);
    $inv = $this->affiliateUserInvoiceRepo->findByOrderId($data['order_id']);

    if ($order->status != OrderStatus::COMPLETED) {
      return $listener->response(400, "order status is not completed, it's {$order->status}");
    }

    if ($inv->status != UserInvoice::COMPLETED) {
      return $listener->response(400, "invoice status is not completed, it's {$inv->status}");
    }

    $cashTransactions = $this->cashTransactionRepo->findAllByAttributes('order_id', $data['order_id']);

    foreach ($cashTransactions as $ct) {
      if ($ct->trx_type == TransactionType::REFUND) {
        return $listener->response(400, 'invoice has been refunded before');
      }

      $data = json_decode($ct->data, true);
      unset($data['settlement_at']);
      $this->cashInserter->add([
        'user_id' => $ct->user_id,
        'store_id' => $ct->store_id,
        'trx_type' => $ct->amount > 0 ? TransactionType::MANUAL_VOID : TransactionType::REFUND,
        'cash_type' => $ct->cash_type,
        'amount' => -$ct->amount,
        'data' => json_encode($data),
        'order_id' => $ct->order_id,
      ]);
    }
    
    $this->cashInserter->run();

    $inv->status = UserInvoice::VOID;
    $order->status = OrderStatus::VOIDED;
    $orderDetail->refunded_at = date('Y-m-d H:i:s');

    $inv->save();
    $order->save();
    $orderDetail->save();

    $listener->pushQueue(new VoidInvoicePayment($data['order_id']));

    return $listener->response(200);
  }
}
