<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper;

use Illuminate\Support\Facades\Redis;

class InvoiceTempInquiry {

  private $redis, $namespace;

  public function __construct() {
    $this->redis = Redis::connection();
    $this->namespace = 'odeo_core:asg_invoice_inquiry';
  }

  public function getInvoiceNumber($biller, $userId) {
    return $this->redis->get($this->namespace . ':' . $biller . ':' . $userId);
  }

  public function saveInvoiceNumber($biller, $userId, $invoiceNumber) {
    $key = $this->namespace . ':' . $biller . ':' . $userId;
    
    $this->redis->set($key, $invoiceNumber);
    $this->redis->expire($key, 24 * 60 * 60);
  }

  public function deleteInvoiceNumber($biller, $userId) {
    $key = $this->namespace . ':' . $biller . ':' . $userId;

    $this->redis->del($key);
  }
}