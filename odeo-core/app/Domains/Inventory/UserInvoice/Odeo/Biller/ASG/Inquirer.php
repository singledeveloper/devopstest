<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\BillerASG;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\UserInvoiceSelector;
use Odeo\Domains\Inventory\UserInvoice\Repository\UserInvoiceBillerRepository;
use Odeo\Domains\Payment\PaymentVaDirectSelector;

class Inquirer {

  public function __construct() {
    $this->biller = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository::class);
    $this->userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository::class);
    $this->userInvoiceDataGenerator = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Helper\UserInvoiceDataGenerator::class);
    $this->users = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceUserRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->api = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\ApiManager::class);
    $this->tempInquiry = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\InvoiceTempInquiry::class);
    $this->userInvoiceBillerRepo = app()->make(UserInvoiceBillerRepository::class);
  }

  public function getUnpaid(PipelineListener $listener, $data) {
    $user = $this->users->findByAttributes('user_id', $data['user_id']);

    $invoices = $this->api->getUnitInvoiceByStatus($data['user_id'], BillerASG::UNPAID, 100);

    $items = [];
    $total = 0;

    if (!$user || !$invoices || !isset($invoices['data'])) {
      return $listener->response(400, ['error_code' => 3002]);
    }

    $existingInvoices = $this->userInvoices->getUnpaidByBilledUser($data['user_id'])->keyBy('name')->toArray();

    $paidInvoiceNumbers = array_keys($existingInvoices);
    $paidInvoiceNumbers = array_flip($paidInvoiceNumbers);

    $updatedInvoices = $newInvoices = [];

    $totalUnpaid = 0;
    $billerInvoiceNumbers = [];

    foreach ($invoices['data'] as $item) {
      $invoiceNumber = $item['id'];
      unset($paidInvoiceNumbers[$invoiceNumber]);
      $totalUnpaid += $item['total'];
      $billerInvoiceNumbers[] = $invoiceNumber;

      $invoice = $this->api->getByInvoiceId($invoiceNumber);

      $dueDate = \Carbon\Carbon::parse($invoice['dueDate']);

      $user->name = $this->purifyName($invoice['name']);
      $user->telephone = trim($invoice['phone']);

      if ($user->isDirty()) $this->users->save($user);

      $items = [];
      foreach ($invoice['invoices'] as $invoiceDetail) {
        $items[] = [
          'name' => $invoiceDetail['name'],
          'price' => $invoiceDetail['remaining'],
          'picture' => $invoiceDetail['picture']
        ];
      }

      if (array_key_exists($invoiceNumber, $existingInvoices)) {
        $invoiceData = $this->userInvoiceDataGenerator->generate([
          'billed_user_id' => $invoice['house'],
          'billed_user_name' => $user->name,
          'billed_user_telephone' => $user->telephone,
          'period' => $dueDate->format('Y-m'),
          'due_date' => $dueDate,
          'name' => $invoice['invoiceNumber'],
          'items' => $items
        ], $data);
        $updatedInvoices[] = [
          'id' => $existingInvoices[$invoiceNumber]['id'],
          'items' => $invoiceData['items'],
          'subtotal' => $invoiceData['subtotal'],
          'total' => $invoiceData['total'],
          'billed_user_id' => $invoiceData['billed_user_id'],
          'billed_user_name' => $invoiceData['billed_user_name'],
          'billed_user_telephone' => $invoiceData['billed_user_telephone']
        ];
      } else {
        $newInvoices[] = $this->userInvoiceDataGenerator->generate([
          'billed_user_id' => $invoice['house'],
          'billed_user_name' => $user->name,
          'billed_user_telephone' => $invoice['phone'],
          'period' => $dueDate->format('Y-m'),
          'due_date' => $dueDate,
          'name' => $invoice['invoiceNumber'],
          'items' => $items
        ], $data);
      }
    }
    try {
      count($newInvoices) && $this->userInvoices->saveBulk($newInvoices);
    } catch (\Illuminate\Database\QueryException $e) {
      clog('asg_invoice', 'Duplicate invoice. ' . $e->getMessage());
      return $listener->response(400, ['error_code' => 3002]);
    }

    count($updatedInvoices) && $this->userInvoices->updateBulk($updatedInvoices);

    $paidInvoices = [];
    foreach ($paidInvoiceNumbers as $paidInvoiceNumber => $temp) {
      $paidInvoices[] = [
        'id' => $existingInvoices[$paidInvoiceNumber]['id'],
        'status' => UserInvoice::COMPLETED_WITH_EXTERNAL_PAYMENT
      ];
    }

    count($paidInvoices) && $this->userInvoices->updateBulk($paidInvoices);

    $listener->addNext(new Task(UserInvoiceSelector::class, 'get', [
      'biller_invoice_number' => $billerInvoiceNumbers,
      'is_paginate' => false
    ]));

    $listener->addNext(new Task(PaymentVaDirectSelector::class, 'get', [
      'service_detail_id' => ServiceDetail::USER_INVOICE_ODEO,
      'virtual_account_vendors' => BillerASG::VIRTUAL_ACCOUNT_VENDOR_FEE,
      'amount' => $totalUnpaid,
      'user_id' => $data['user_id'],
      'user_name' => $user->name
    ]));

    return $listener->response(200, [
      'total_due' => $totalUnpaid
    ]);
  }

  public function getByNumber(PipelineListener $listener, $data) {
    $forPayment = isset($data['for_payment']) && $data['for_payment'];

    $existingInvoice = $this->userInvoices->findUnpaidBillerInvoice($data['biller_id'], $data['biller_invoice_number']);

    $invoiceDetail = $this->api->getByInvoiceId($data['biller_invoice_number']);
    if (!$invoiceDetail || !isset($invoiceDetail['id']) || !isset($invoiceDetail['house'])) {
      return $listener->response(400, ['error_code' => 3002]);
    }

    $user = $this->users->findByAttributes('user_id', $invoiceDetail['house']);
    if (!$user) {
      return $listener->response(400, ['error_code' => 3002]);
    }

    $items = [];
    foreach ($invoiceDetail['invoices'] as $item) {
      $items[] = [
        'name' => $item['name'],
        'price' => $item['remaining'],
        'picture' => $item['picture']
      ];
    }

    $dueDate = \Carbon\Carbon::parse($invoiceDetail['dueDate']);

    $user->name = $this->purifyName($invoiceDetail['name']);
    $user->telephone = $invoiceDetail['phone'];

    if ($user->isDirty()) $this->users->save($user);

    $invoiceData = [
      'billed_user_id' => $invoiceDetail['house'],
      'billed_user_name' => $user->name,
      'billed_user_telephone' => $invoiceDetail['phone'],
      'period' => $dueDate->format('Y-m'),
      'due_date' => $dueDate,
      'name' => $invoiceDetail['invoiceNumber'],
      'items' => $items
    ];

    if (strtolower($invoiceDetail['status']) == BillerASG::PAID) {
      if ($existingInvoice) {
        $existingInvoice->status = UserInvoice::COMPLETED_WITH_EXTERNAL_PAYMENT;
      } else {
        $existingInvoice = $this->userInvoiceDataGenerator->generate($invoiceData, $data);
        $existingInvoice['items'] = json_decode($existingInvoice['items']);
        $existingInvoice['status'] = UserInvoice::COMPLETED_WITH_EXTERNAL_PAYMENT;
      }
      try {
        $this->userInvoices->save($existingInvoice);
      } catch (\Illuminate\Database\QueryException $e) {
        clog('asg_invoice', 'Duplicate invoice. ' . $e->getMessage());
      }

      return $listener->response(400, ['error_code' => 3002]);
    }

    if ($existingInvoice) {
      if ($existingInvoice->total != $invoiceDetail['remaining']) {
        $existingInvoice->items = $items;
        $existingInvoice->subtotal = $invoiceDetail['remaining'];
        $existingInvoice->total = $invoiceDetail['remaining'];
      }

      $existingInvoice->billed_user_id = $invoiceDetail['house'];
      $existingInvoice->billed_user_name = $user->name;
      $existingInvoice->billed_user_telephone = $invoiceDetail['phone'];

      if ($existingInvoice->isDirty()) $this->userInvoices->save($existingInvoice);
    } else {
      $newInvoice = $this->userInvoiceDataGenerator->generate($invoiceData, $data);
      $newInvoice['items'] = json_decode($newInvoice['items']);

      $this->userInvoices->save($newInvoice);
    }

    if ($forPayment) {
      $this->tempInquiry->saveInvoiceNumber($data['biller_id'], $invoiceDetail['house'], $invoiceDetail['invoiceNumber']);
    }

    $listener->addNext(new Task(UserInvoiceSelector::class, 'get', [
      'biller_invoice_number' => $invoiceDetail['invoiceNumber'],
      'biller_invoice_status' => UserInvoice::UNPAID,
      'is_paginate' => false
    ]));

    $listener->addNext(new Task(PaymentVaDirectSelector::class, 'get', [
      'service_detail_id' => ServiceDetail::USER_INVOICE_ODEO,
      'virtual_account_vendors' => BillerASG::VIRTUAL_ACCOUNT_VENDOR_FEE,
      'amount' => $invoiceDetail['remaining'],
      'user_id' => $invoiceDetail['house'],
      'user_name' => $user->name
    ]));

    return $listener->response(200, [
      'total_due' => $invoiceDetail['remaining']
    ]);
  }

  private function purifyName($name) {
    return trim(str_replace('&', 'DAN', $name));
  }

  public function getByPhoneNumber(PipelineListener $listener, $data) {
    $asgBillerIds = [UserInvoice::BILLER_ASG_GLC, UserInvoice::BILLER_ASG_GLC_FM];
    $phoneNumber = substr($data['phone_number'], 2, strlen($data['phone_number']) - 2);
    $billerId = $data['biller_id'] ?? '';
    $houseNumber = $data['billed_user_id'] ?? '';
    $period = $data['period'] ?? '';
    $row = $data['row'] ?? 1;
    $count = 10;
    $invoices = $this->api->getByPhoneNumber($phoneNumber, BillerASG::UNPAID, $count, $row);

    if (!isset($invoices) || !isset($invoices['data'])) {
      return $listener->response(400, ['error_code' => 3002]);
    }

    $nextRow = isset($invoices['after']) && $invoices['after'] ? $row + $count : 0;

    $invoiceNames = collect($invoices['data'])
      ->pluck('id')
      ->toArray();

    $existingInvoices = $this->userInvoices
      ->getByInvoiceNamesAndNotStatus($asgBillerIds, $invoiceNames, UserInvoice::VOID)
      ->keyBy('name')
      ->toArray();

    $houseNumbers = collect($invoices['data'])
      ->pluck('houseNumber')
      ->toArray();
    $existingUsers = $this->users->getByUserIds($asgBillerIds, $houseNumbers)
      ->keyBy('user_id')
      ->toArray();

    $billers = $this->userInvoiceBillerRepo
      ->getAll()
      ->keyBy('id')
      ->toArray();

    $newInvoices = [];
    $newUsers = [];
    $response = [];

    foreach ($invoices['data'] as $item) {
      $billerInvoiceNumber = $item['id'];
      $billerId = BillerASG::getBillerByProjectId(trim($item['projectId']));
      $biller = $billers[$billerId] ?? [];
      $existingInv = $existingInvoices[$billerInvoiceNumber] ?? null;

      if ($existingInv && in_array($existingInv['status'], UserInvoice::PAID)) {
        continue;
      }

      $invoiceData = $this->mapInvoice($data, $item);
      if ($existingInv) {
        $invoiceData['id'] = $existingInv['id'];
        $invoiceData['invoice_number'] = $existingInv['invoice_number'];
      }

      $response[] = [
        'invoice_number' => $invoiceData['invoice_number'],
        'period' => $invoiceData['period'],
        'invoice_name' => $billerInvoiceNumber,
        'billed_user_id' => $item['houseNumber'],
        'billed_user_name' => $invoiceData['billed_user_name'],
        'descriptions' => [$item['address']],
        'total' => +$item['total'],
        'biller_id' => $billerId,
        'biller_name' => $biller['biller_name'],
      ];

      if ($existingInv) {
        continue;
      }

      $this->tempInquiry->saveInvoiceNumber($billerId, $item['houseNumber'], $billerInvoiceNumber);
      $newInvoices[] = $invoiceData;

      if (!isset($existingUsers[$item['houseNumber']])) {
        $key = $billerId . '.' . $item['houseNumber'];
        $newUsers[$key] = [
          'biller_id' => $billerId,
          'user_id' => $item['houseNumber'],
          'name' => $invoiceData['billed_user_name'],
          'telephone' => $data['phone_number'],
          'address' => $item['address'],
          'email' => '',
        ];
      }
    }

    if (!empty($newInvoices)) {
      $this->userInvoices->saveBulk($newInvoices);
    }

    if (!empty($newUsers)) {
      $this->users->saveBulk(array_values($newUsers));
    }

    $response = array_filter($response, function ($item) use ($houseNumber, $billerId, $period) {
      if (!!$period && $item['period'] != $period) {
        return false;
      }

      if (!!$houseNumber && stripos($item['billed_user_id'], $houseNumber) !== false) {
        return false;
      }

      if (!!$billerId && $item['biller_id'] != $billerId) {
        return false;
      }

      return true;
    });

    return $listener->response(200, [
      'invoices' => $response,
      'next_row' => $nextRow,
    ]);
  }

  private function mapInvoice($data, $invoice) {
    $billerInvoiceNumber = $invoice['id'];
    $period = \Carbon\Carbon::parse($invoice['period']);
    $dueDate = \Carbon\Carbon::parse($invoice['dueDate']);
    $name = $this->purifyName($invoice['resident']);
    $billerId = BillerASG::getBillerByProjectId(trim($invoice['projectId']));

    $invoiceDetails = $invoice['details'];
    $items = [];
    foreach ($invoiceDetails as $item) {
      $items[] = [
        'name' => $item['name'],
        'price' => $item['remaining'],
        'picture' => $item['picture'],
      ];
    }

    $invoiceData = $this->userInvoiceDataGenerator->generate([
      'billed_user_id' => $invoice['houseNumber'],
      'billed_user_name' => $name,
      'billed_user_telephone' => $data['phone_number'],
      'period' => $period->format('Y-m'),
      'due_date' => $dueDate,
      'name' => $billerInvoiceNumber,
      'items' => $items,
    ], array_merge($data, [
      'biller_id' => $billerId,
    ]));
    $invoiceData['subtotal'] = $invoiceData['total'] = $invoice['total'];
    $invoiceData['biller_id'] = $billerId;
    return $invoiceData;
  }

}
