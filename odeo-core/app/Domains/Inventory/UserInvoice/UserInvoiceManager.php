<?php
namespace Odeo\Domains\Inventory\UserInvoice;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class UserInvoiceManager extends InventoryManager {

  private static $userInvoiceInstance;

  public function setVendor(PipelineListener $listener, $data) {
    self::$userInvoiceInstance = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\OdeoManager::class);
    $this->setServiceDetail($data['service_detail_id']);

    return $listener->response(200, []);
  }

  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    self::$userInvoiceInstance->setBiller($argument[1]['biller_id']);
    return call_user_func_array([self::$userInvoiceInstance, $name], $argument);
  }

}
