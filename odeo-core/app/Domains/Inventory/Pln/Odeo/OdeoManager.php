<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 5:16 PM
 */

namespace Odeo\Domains\Inventory\Pln\Odeo;


use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Pln\Contract\PlnContract;

class OdeoManager implements PlnContract {

  private $searcher, $checkouter, $postpaidCheckouter, $switcher, $refunder;

  public function __construct() {
    $this->searcher = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaSearcher::class);
    $this->checkouter = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaCheckouter::class);
    $this->postpaidCheckouter = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PostpaidCheckouter::class);
    $this->switcher = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaSwitcher::class);
    $this->refunder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaRefunder::class);
  }

  public function get(PipelineListener $listener, $data) {
    $data['operator_code'] = 'PLN';
    return $this->searcher->searchNominal($listener, $data);
  }

  public function inquiryPostpaid(PipelineListener $listener, $data) {
    return $this->switcher->inquiryPostpaid($listener, $data);
  }

  public function checkout(PipelineListener $listener, $data) {
    if ($data['service_detail_id'] == ServiceDetail::PLN_POSTPAID_ODEO)
      return $this->postpaidCheckouter->checkout($listener, $data);
    return $this->checkouter->checkout($listener, $data);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->switcher->purchase($listener, $data);
  }

  public function purchasePostpaid(PipelineListener $listener, $data) {
    return $this->switcher->purchasePostpaid($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->switcher->validateInventory($listener, $data);
  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    return $this->switcher->validatePostpaidInventory($listener, $data);
  }
  
  public function refund(PipelineListener $listener, $data) {
    return $this->refunder->refund($listener, $data);
  }

}