<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 3:31 PM
 */

namespace Odeo\Domains\Inventory\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Model\Service;
use Odeo\Domains\Order\Salesreport\Helper\Initializer;

class ServiceRepository extends Repository {

  public function __construct(Service $service) {
    $this->model = $service;
  }


  public function findByIds($ids) {
    return $this->model->whereIn('id', $ids)->orderBy('id', 'asc')->get();
  }

  public function getOcommerceProduct() {

    $query = $this->getCloneModel();

    $query = $query->whereIn("id", Initializer::getOcommerceServiceIds());

    return $this->getResult($query);
  }

}