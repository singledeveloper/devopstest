<?php

namespace Odeo\Domains\Inventory\Flight\Tiket\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class TiketAirline extends Entity
{
  use SoftDeletes;


  protected $dates = ['deleted_at'];

  protected $fillable = ['code', 'name', 'logo_path'];

  protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
