<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 10:25 PM
 */

namespace Odeo\Domains\Inventory\Flight\Tiket\Crawler;


use Log;
use Odeo\Domains\Constant\Tiket;

class AirportCrawler {


  private $tokenManager, $airports, $connector;

  public function __construct() {

    $this->tokenManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\TokenManager::class);
    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
    $this->airports = app()->make(\Odeo\Domains\Inventory\Flight\Tiket\Repository\TiketAirportRepository::class);

  }


  public function getAirports() {
    list($token) = $this->tokenManager->getToken(['user_id' => 0]);
    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      return [$token];
    }

    $query = [
      'token' => $token['data']['token'],
      'output' => 'json',
    ];

    $this->connector->request('GET', 'tiket', '/flight_api/all_airport', $query);
    $responses = $this->connector->response([
      200 => function ($body) {

        if (isset($body['all_airport']['airport'])) {

          foreach ($body['all_airport']['airport'] as $a) {

            $airport = $this->airports->findById($a['airport_code']);
            if (!$airport) {
              $airport = $this->airports->getNew();
            }

            $airport->id = $a['airport_code'];
            $airport->name = ucwords($a['airport_name']);
            $airport->location = ucwords($a['location_name']);
            $airport->country_id = $a['country_id'];
            $airport->country_name = ucwords($a['country_name']);

            try {

              $this->airports->save($airport);

            } catch (\Illuminate\Database\QueryException $exception) {
              continue;
            }
          }
        }

        return [
          'data' => ['airports' => $this->airports->getAll()],
        ];
      },
    ]);

    return $responses;
  }


}