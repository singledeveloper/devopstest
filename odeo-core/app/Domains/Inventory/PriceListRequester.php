<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 4/28/17
 * Time: 12:44 PM
 */

namespace Odeo\Domains\Inventory;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\DataExporterType;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Jobs\SendPriceList;
use Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter;

class PriceListRequester {

  private $marginFormatter, $serviceDetails, $currencyHelper, $isH2H = false, $pulsaH2hGroupDetails,
    $pulsaOdeo, $users, $includeInventoryIds = [], $masterInventories = [], $redis, $stores;

  public function __construct() {
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->priceGetter = app()->make(\Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->pulsaH2hGroupDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hGroupDetailRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->redis = Redis::connection();
  }

  public function sendPriceList(PipelineListener $listener, $data) {

    $user = $this->users->findById(getUserId());
    $this->checkH2H($user);

    $storeName = 'TOP';
    if (isset($data['store_id'])) {
      if ($store = $this->stores->findById($data['store_id'])) {
        $this->getMasterPrice($user, $store->id);
        $storeName = $store->name;
      } else {
        return $listener->response(400, "Store not found");
      }
    }

    $fileType = $data['file_type'] ?? DataExporterType::CSV;
    $pulsaInventories = $this->pulsaOdeo->gets(['inventory_ids' => $this->includeInventoryIds]);
    if (!$pulsaInventories) return $listener->response(204);

    $pulsas = [[
      "category" => "Category",
      "inventory_code" => "Code",
      "name" => "Name",
      "price" => "Price"
    ]];

    foreach ($this->formatPulsaData($pulsaInventories, $user, in_array($fileType, [
      DataExporterType::PDF
    ]), true) as $item) {
      $pulsas[] = $item;
    }

    $listener->pushQueue(new SendPriceList($pulsas, $data['email'], $fileType, $storeName));

    return $listener->response(200);
  }

  public function getPriceList(PipelineListener $listener, $data) {
    $user = $this->users->findById($data['auth']['user_id']);
    $this->checkH2H($user);
    if (isset($data['store_id'])) {
      if ($store = $this->stores->findById($data['store_id'])) {
        $this->getMasterPrice($user, $store->id);
      } else {
        return $listener->response(400, "Store not found");
      }
    }

    $pulsaInventories = $this->pulsaOdeo->gets(['inventory_ids' => $this->includeInventoryIds]);
    if (!$pulsaInventories) return $listener->response(204);

    return $listener->response(200, $this->formatPulsaData($pulsaInventories, $user));
  }

  private function getMasterPrice($user, $storeId) {

    if ($this->isH2H || !$user->purchase_preferred_store_id) {
      return;
    }

    if (!isVersionSatisfy(Platform::ANDROID, '3.1.4') && !$user->purchase_preferred_store_id) {
      return;
    }

    $agentPrices = $this->priceGetter->getPriceForInventory($storeId, [
      'user_id' => $user->id,
    ], PriceGetter::FLAG_DISABLE_BASE_PRICE);

    if (count($agentPrices) > 0) {
      $this->includeInventoryIds = $agentPrices->pluck('inventory_id');
    }
    else return;

    $this->masterInventories = $agentPrices->all();

  }

  private function checkH2H($user) {
    $h2hManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaH2HManager::class);
    $this->isH2H = $h2hManager->isUserH2H($user->id);
  }

  private function formatPulsaData($pulsaInventories, $user, $needFormatPrice = true, $needCategory = false) {
    $pulsas = [];
    $pulsaTemp = [];
    $replicateFlags = [];

    $serviceDetailData = $this->serviceDetails->getAll();
    $serviceDetails = [];
    foreach ($serviceDetailData as $item) {
      $serviceDetails[$item->id] = $item;
    }

    $h2hPrices = $this->isH2H ? $this->pulsaH2hGroupDetails->getByUserId($user->id) : [];

    foreach ($pulsaInventories as $item) {
      if (!$item->service_detail_id || $item->status != Pulsa::STATUS_OK ||
        in_array($item->service_detail_id, [
          ServiceDetail::PULSA_POSTPAID_ODEO,
          ServiceDetail::PLN_POSTPAID_ODEO,
          ServiceDetail::BPJS_KES_ODEO,
          ServiceDetail::LANDLINE_ODEO,
          ServiceDetail::BROADBAND_ODEO,
          ServiceDetail::PDAM_ODEO,
          ServiceDetail::PGN_ODEO,
          ServiceDetail::EMONEY_ODEO,
          ServiceDetail::MULTI_FINANCE_ODEO
        ])) continue;
      $temp = [];

      $temp['category'] = $item->category;
      $temp['inventory_code'] = $item->inventory_code;
      $temp["name"] = $item->name;

      if (isset($h2hPrices[$item->id]))
        $temp['price'] = $h2hPrices[$item->id]->price;
      else if (isset($this->masterInventories[$item->id]))
        $temp['price'] = $this->masterInventories[$item->id]['sell_price'];
      else if ($item->status == Pulsa::STATUS_OK_SUPPLIER) continue;
      else {
        $temp['price'] = $this->marginFormatter->formatMargin($item->price, [
          'service_detail' => $serviceDetails[$item->service_detail_id],
          'user_id' => $user->id
        ])['sale_price'];
      }

      if ($needFormatPrice) $temp['price'] = $this->currencyHelper->formatPrice($temp['price']);

      if ($item->replicate_flag) {
        $replicateFlags[$item->replicate_flag][] = $item->id;
      }

      $temp['owner_store_id'] = $item->owner_store_id;
      $temp['replicate_flag'] = $item->replicate_flag;

      $pulsaTemp[] = $temp;
    }

    foreach ($pulsaTemp as $item) {
      if ($item['owner_store_id'] == null && (isset($replicateFlags[$item['replicate_flag']]) && count($replicateFlags[$item['replicate_flag']]) > 1))
        continue;
      unset($item['owner_store_id'], $item['replicate_flag']);
      if ($needCategory) $pulsas[] = $item;
      else {
        $category = $item['category'];
        unset($item['category']);
        if (!isset($pulsas[$category])) $pulsas[$category] = [];
        $pulsas[$category][] = $item;
      }
    }

    return $pulsas;
  }

}
