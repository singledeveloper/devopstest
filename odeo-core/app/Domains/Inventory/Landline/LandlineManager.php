<?php

namespace Odeo\Domains\Inventory\Landline;

use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;
use Validator;

class LandlineManager extends InventoryManager {

  private static $landlineInstance;

  public function setVendor(PipelineListener $listener, $data) {

    self::$landlineInstance = app()->make(\Odeo\Domains\Inventory\Landline\Odeo\OdeoManager::class);

    $this->setServiceDetail($data['service_detail_id']);

    return $listener->response(200);

  }

  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$landlineInstance, $name], $argument);
  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {

    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    if (!isset($data['service_detail_id']) || (isset($data['service_detail_id']) && $data['service_detail_id'] == '0'))
      $data['service_detail_id'] = ServiceDetail::LANDLINE_ODEO;

    $repository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    if ($inventory = $repository->getCheapestFee($data['service_detail_id'])) {
      $this->setVendor($listener, $data);
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      $listener->replaceData($data);

      self::$landlineInstance->validatePostpaidInventory($listener, $data);
    }
    else return $listener->response(400, trans('pulsa.cant_purchase'));
  }

}
