<?php

namespace Odeo\Domains\Inventory\Transportation\Odeo;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Transportation\Contract\TransportationContract;

class OdeoManager implements TransportationContract  {

  private $searcher, $checkouter, $switcher, $refunder;

  public function __construct() {
    $this->searcher = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaSearcher::class);
    $this->checkouter = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaCheckouter::class);
    $this->switcher = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaSwitcher::class);
    $this->refunder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaRefunder::class);
  }

  public function searchNominal(PipelineListener $listener, $data) {
    return $this->searcher->searchNominal($listener, $data);
  }

  public function checkout(PipelineListener $listener, $data) {
    return $this->checkouter->checkout($listener, $data);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->switcher->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->switcher->validateInventory($listener, $data);
  }
  
  public function refund(PipelineListener $listener, $data) {
    return $this->refunder->refund($listener, $data);
  }

  public function getAllCategoryByServiceDetailId(PipelineListener $listener, $data) {
    return $this->searcher->getAllCategoryByServiceDetailId($listener, $data);
  }
}