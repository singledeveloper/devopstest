<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/21/16
 * Time: 9:15 PM
 */

namespace Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi;


use Odeo\Domains\Core\PipelineListener;

class PundiManager {

  public function validateInventory(PipelineListener $listener, $data) {
    return app()->make(InventoryValidator::class)->validateInventory($listener, $data);
  }

  public function request(PipelineListener $listener, $data) {
    return app()->make(Requester::class)->request($listener, $data);
  }

  public function checkout(PipelineListener $listener, $data) {
    return app()->make(Requester::class)->checkout($listener, $data);
  }

  public function completeOrder(PipelineListener $listener, $data) {
    return app()->make(Requester::class)->completeOrder($listener, $data);
  }

  public function cancel(PipelineListener $listener, $data) {
    return app()->make(Requester::class)->cancel($listener, $data);
  }

}