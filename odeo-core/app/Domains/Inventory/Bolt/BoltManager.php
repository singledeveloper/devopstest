<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 6:01 PM
 */

namespace Odeo\Domains\Inventory\Bolt;

use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class BoltManager extends InventoryManager {

  private static $boltInstance;
  private $switcherOrder;

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  }

  public function setVendor(PipelineListener $listener, $data) {
    self::$boltInstance = app()->make(\Odeo\Domains\Inventory\Bolt\Odeo\OdeoManager::class);
    $this->setServiceDetail(isset($data['service_detail_id']) ? $data['service_detail_id'] : ServiceDetail::BOLT_ODEO);

    return $listener->response(200, []);

  }


  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$boltInstance, $name], $argument);
  }

  public function validateInventory(PipelineListener $listener, $data) {

    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    $this->setVendor($listener, $data);
    if ($inventory = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class)->getCheapestPrice($data['item_id'])) {
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      return self::$boltInstance->validateInventory($listener, $data);
    }
    return $listener->responseWithErrorStatus(ErrorStatus::EMPTY_STOCK, trans('pulsa.empty_stock'));
  }

}
