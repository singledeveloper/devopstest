<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/18/17
 * Time: 12:06 PM
 */

namespace Odeo\Domains\Inventory\GameVoucher;

use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class GameVoucherManager extends InventoryManager {

  public static $gameInstance;

  private $pulsaInventories;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
  }

  public function setVendor(PipelineListener $listener, $data) {

    self::$gameInstance = app()->make(\Odeo\Domains\Inventory\GameVoucher\Odeo\OdeoManager::class);

    $this->setServiceDetail($data['service_detail_id']);

    return $listener->response(200);

  }

  public function __call($name, $arguments) {

    $this->setVendor($arguments[0], $arguments[1]);
    return call_user_func_array([self::$gameInstance, $name], $arguments);

  }

  public function validateInventory(PipelineListener $listener, $data) {

    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    $this->setVendor($listener, $data);
    if ($inventory = $this->pulsaInventories->getCheapestPrice($data['item_id'])) {
      $this->setVendor($listener, $data);
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      $listener->replaceData($data);

      return self::$gameInstance->validateInventory($listener, $data);
    }
    return $listener->response(ErrorStatus::EMPTY_STOCK, trans('pulsa.empty_stock'));
  }

  public function getTermAndCon(PipelineListener $listener, $data) {

    return $listener->response(200, [
      [
        '1_text' => trans('pulsa.tnc_general_wrong_number'),
      ],
      [
        '1_text' => trans('pulsa.tnc_game_voucher_1'),
        '2_list' => [
          [
            '1_text' => trans('pulsa.tnc_game_voucher_1_list_1'),
          ],
          [
            '1_text' => trans('pulsa.tnc_game_voucher_1_list_2'),
          ],
          [
            '1_text' => trans('pulsa.tnc_game_voucher_1_list_3'),
          ],
          [
            '1_text' => trans('pulsa.tnc_game_voucher_1_list_4'),
          ]
        ]
      ],
      [
        '1_text' => trans('pulsa.tnc_general_suspect'),
        '2_list' => [
          [
            '1_text' => trans('pulsa.tnc_general_suspect_max_complain'),
          ],
          [
            '1_text' => trans('pulsa.tnc_general_suspect_max_wait'),
          ],
          [
            '1_text' => trans('pulsa.tnc_general_suspect_more_than_24h'),
          ],
          [
            '1_text' => trans('pulsa.tnc_beforehand_refund_risk'),
          ]
        ]
      ],
      [
        '1_text' => trans('pulsa.tnc_general_complain_statement'),
        '2_list' => [
          [
            '1_text' => trans('pulsa.tnc_general_complain_statement_suspect'),
          ]
        ]
      ],
    ]);
  }

}
