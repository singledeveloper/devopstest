<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 11:10 PM
 */

namespace Odeo\Domains\Inventory\Pulsa;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\UserKtpValidator;
use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\DoublePurchaseValidator;
use Odeo\Domains\Inventory\Helper\InventoryManager;
use Odeo\Domains\Inventory\Pulsa\Odeo\OdeoManager;
use Odeo\Domains\Notification\Jobs\SaveInternalNotice;
use Validator;

class PulsaManager extends InventoryManager {

  private static $pulsaInstance;
  private $pulsaInventories, $switcherOrder, $pulsaOdeos, $redis, $userKtpValidator;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->doublePurchaseValidator = app()->make(DoublePurchaseValidator::class);
    $this->userKtpValidator = app()->make(UserKtpValidator::class);
    $this->redis = Redis::connection();
  }

  public function setVendor(PipelineListener $listener, $data) {
    self::$pulsaInstance = app()->make(OdeoManager::class);
    $this->setServiceDetail(isset($data['service_detail_id']) ? $data['service_detail_id'] : ServiceDetail::PULSA_ODEO);
    return $listener->response(200);
  }

  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$pulsaInstance, $name], $argument);
  }

  public function getLastDetectedDoublePurchase(PipelineListener $listener, $data) {
    $itemId = $data['item_id'];
    $purifiedNumber = purifyTelephone($data['number']);

    $res = $this->doublePurchaseValidator
      ->getLastDetectedDoublePurchase($itemId, $data['service_detail_id'], [
        'number' => $purifiedNumber
      ], getUserId());

    return $listener->response(200, $res);
  }

  private function isDoublePurchase($data) {
    if (isset($data['gateway_id']) && in_array($data['gateway_id'], [Payment::AFFILIATE, Payment::JABBER])) {
      return [false, ''];
    }

    $itemId = $data['item_id'];
    $purifiedNumber = purifyTelephone($data['item_detail']['number']);
    $isDoublePurchase = $this->doublePurchaseValidator
      ->check($itemId, $data['service_detail_id'], [
        'number' => $purifiedNumber
      ], getUserId());

    if ($isDoublePurchase) {
      if (!$this->userKtpValidator->isVerified($data['auth']['user_id'])) {
        dispatch(new SaveInternalNotice(NotificationType::PARSE_MULTIPLIER .
          ' percobaan dari user ' . $data['auth']['user_id'] . ' untuk membeli lebih dari satu pulsa tanpa KYC',
          NotificationType::NOTICE_NO_KYC, $data['auth']['user_id'], true));
        return [true, trans('pulsa.cant_buy_without_kyc')];
      }
      
      if (isset($data['check_double_purchase'])) {
        $status = $this->doublePurchaseValidator
          ->getLastDetectedDoublePurchase($itemId, $data['service_detail_id'], [
            'number' => $purifiedNumber
          ], getUserId());

        if ($status['status'] == OrderStatus::COMPLETED || $status['status'] == OrderStatus::VERIFIED) {
          return [true, ''];
        }
      }
    }
    return [false, ''];
  }

  public function validateInventory(PipelineListener $listener, $data) {
    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    $validator = Validator::make($data, ['item_detail.number' => 'required']);

    if ($validator->fails()) return $listener->response(400, $validator->errors()->all());

    //HARDCODE
    if (in_array(purifyTelephone($data['item_detail']['number']), ['6281295181322']))
      return $listener->response(400, 'Number blocked. Too many attempts');

    $this->setVendor($listener, $data);
    $inventory = $this->pulsaInventories->getCheapestPrice($data['item_id'], purifyTelephone($data['item_detail']['number']));

    if (!$inventory) {
      if ($pulsa = $this->pulsaOdeos->findById($data['item_id'])) {
        clog('empty_stock_logs', json_encode($data));
        dispatch(new SaveInternalNotice(
          NotificationType::PARSE_MULTIPLIER . ' gagal stok kosong terjadi di ' . $pulsa->name,
          NotificationType::NOTICE_EMPTY_STOCK, $pulsa->id, true
        ));
      }
      return $listener->responseWithErrorStatus(ErrorStatus::EMPTY_STOCK, trans('pulsa.empty_stock'));
    }

    $pulsa = $inventory->pulsa;
    if ($pulsa->price > 100000 && !$this->userKtpValidator->isVerified($data['auth']['user_id'])) {
      return $listener->response(400, trans('pulsa.cant_buy_without_kyc'));
    }

    list($mustWarn, $errorMessage) = $this->isDoublePurchase($data);
    if ($mustWarn) {
      return $listener->responseWithErrorStatus(ErrorStatus::DOUBLE_PURCHASE, $errorMessage);
    }

    $data['inventory_id'] = $inventory->id;
    $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
    $data['item_detail']['number'] = purifyTelephone($data['item_detail']['number']);
    $listener->replaceData($data);

    return self::$pulsaInstance->validateInventory($listener, $data);
  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    $type = isset($data['postpaid_type']) ? $data['postpaid_type'] : $data['item_detail']['postpaid_type'];

    if ($inventory = $this->pulsaInventories->getCheapestFee($data['service_detail_id'], PostpaidType::getConstValueByKey(strtoupper($type)))) {
      $this->setVendor($listener, $data);
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      $listener->replaceData($data);

      self::$pulsaInstance->validatePostpaidInventory($listener, $data);
    } else return $listener->response(400, trans('pulsa.cant_purchase'));
  }

  public function getList(PipelineListener $listener, $data) {
    $sorts = array_merge(
      PulsaCode::groupOperatorTelkomsel(),
      PulsaCode::groupOperatorIndosat(),
      PulsaCode::groupOperatorTri(),
      PulsaCode::groupOperatorXL(),
      PulsaCode::groupOperatorAxis(),
      PulsaCode::groupOperatorSmartFren(),
      PulsaCode::groupBolt(),
      PulsaCode::groupPln()
    );

    $response = [];
    foreach ($sorts as $item) $response[$item] = [];
    foreach ($this->pulsaOdeos->findAllActive($data) as $item) {
      if ($item->inventory_code == '' || $item->inventory_code == null) continue;
      if (!isset($response[$item->category])) $response[$item->category] = [];
      $response[$item->category][] = [
        'id' => $item->id,
        'name' => $item->name,
        'code' => $item->inventory_code,
        'service_id' => $item->service_id
      ];
    }

    return $listener->response(200, ['pulsa' => $response]);
  }

  public function getPostpaidList(PipelineListener $listener, $data) {
    $categories = PostpaidType::getall();
    
    $response = [];
    foreach ($this->pulsaOdeos->getByCategories($categories) as $item) {
      $response[] = [
        'id' => $item->id,
        'name' => $item->name,
        'service_id' => $item->service_id
      ];
    }

    return $listener->response(200, ['postpaid_types' => $response]);
  }

  public function getTermAndCon(PipelineListener $listener, $data) {
    return $listener->response(200, [
      [
        '1_text' => trans('pulsa.tnc_pulsa_right_number'),
      ],
      [
        '1_text' => trans('pulsa.tnc_general_wrong_number'),
      ],
      [
        '1_text' => trans('pulsa.tnc_pulsa_suspect'),
        '2_list' => [
          [
            '1_text' => trans('pulsa.tnc_pulsa_suspect_list_1'),
          ],
          [
            '1_text' => trans('pulsa.tnc_pulsa_suspect_max_complain'),
          ],
          [
            '1_text' => trans('pulsa.tnc_pulsa_suspect_max_wait'),
          ],
          [
            '1_text' => trans('pulsa.tnc_general_suspect_more_than_24h'),
          ],
          [
            '1_text' => trans('pulsa.tnc_beforehand_refund_risk'),
          ],
        ]
      ],
      [
        '1_text' => trans('pulsa.tnc_general_complain_statement'),
        '2_list' => [
          [
            '1_text' => trans('pulsa.tnc_general_complain_statement_suspect'),
          ]
        ]
      ],
      [
        '1_text' => trans('pulsa.tnc_general_operator_change')
      ]
    ]);
  }

}
