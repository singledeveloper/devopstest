<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 11:10 PM
 */

namespace Odeo\Domains\Inventory\BPJS;

use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;
use Validator;

class BPJSManager extends InventoryManager {


  private static $bpjsInstance;

  public function setVendor(PipelineListener $listener, $data) {

    self::$bpjsInstance = app()->make(\Odeo\Domains\Inventory\BPJS\Odeo\OdeoManager::class);

    $this->setServiceDetail($data['service_detail_id']);

    return $listener->response(200);

  }

  public function __call($name, $argument) {

    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$bpjsInstance, $name], $argument);

  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {

    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    $repository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    if (isset($data['item_id'])) $inventory = $repository->getCheapestFeeByItemId($data['item_id']);
    else {
      if (!isset($data['service_detail_id']) || (isset($data['service_detail_id']) && $data['service_detail_id'] == '0'))
        $data['service_detail_id'] = ServiceDetail::BPJS_KES_ODEO;
      $inventory = $repository->getCheapestFee($data['service_detail_id']);
    }

    if ($inventory) {
      $this->setVendor($listener, $data);
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      $listener->replaceData($data);

      self::$bpjsInstance->validatePostpaidInventory($listener, $data);
    }
    else return $listener->response(400, trans('pulsa.cant_purchase'));
  }

}
