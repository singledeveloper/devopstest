<?php

namespace Odeo\Domains\Inventory\MultiFinance;

use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository;

class MultiFinanceManager extends InventoryManager {

  private static $multiFinanceInstance;

  public function setVendor(PipelineListener $listener, $data) {

    self::$multiFinanceInstance = app()->make(\Odeo\Domains\Inventory\MultiFinance\Odeo\OdeoManager::class);

    $this->setServiceDetail($data['service_detail_id']);

    return $listener->response(200);

  }

  public function __call($name, $argument) {

    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$multiFinanceInstance, $name], $argument);

  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    $repository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    if ($inventory = $repository->getCheapestFeeByItemId($data['item_id'])) {
      $this->setVendor($listener, $data);
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      $listener->replaceData($data);

      self::$multiFinanceInstance->validatePostpaidInventory($listener, $data);
    }
    else return $listener->response(400, trans('pulsa.cant_purchase'));
  }

}
