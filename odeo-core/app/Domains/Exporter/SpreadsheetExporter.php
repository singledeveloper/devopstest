<?php

namespace Odeo\Domains\Exporter;

use Box\Spout\Writer\WriterFactory;

class SpreadsheetExporter {

  public function generate($data, $fileType) {

    $writer = WriterFactory::create($fileType);
    ob_start();

    $writer->openToFile('php://output');
    $writer->addRows($data);

    $file = ob_get_clean();
    $writer->close();

    return $file;
  }

}
