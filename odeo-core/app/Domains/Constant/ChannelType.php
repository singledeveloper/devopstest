<?php

namespace Odeo\Domains\Constant;

class ChannelType
{
  const FIXED = 'static';
  const VARIABLE = 'variable';
  const PULSA = 'pulsa';
}
