<?php

namespace Odeo\Domains\Constant;

class Supplier {

  const TYPE_JSON = 'post_json';
  const TYPE_XML = 'post_xml';
  const TYPE_XMLRPC = 'post_xmlrpc';
  const TYPE_TEXT = 'text';
  const TYPE_HTTP_QUERY = 'get';
  const TYPE_FORM = 'post_form';

  const STATUS_FAIL = 'gagal';
  const STATUS_SUCCESS = 'sukses';
  const STATUS_IN_QUEUE = 'pending';
  const STATUS_LOOP_FAIL = 'sudah_pernah_terjadi';

  const RC_FAIL = '13';
  const RC_FAIL_EMPTY_STOCK = '14';
  const RC_FAIL_DISABLED_STOCK = '15';
  const RC_FAIL_BILLER_CUTOFF = '16';
  const RC_FAIL_BILLER_TOPUP_NOT_ENOUGH = '17';
  const RC_FAIL_DISTURBANCE = '18';
  const RC_FAIL_WRONG_NUMBER = '19';
  const RC_FAIL_UNPROCESSED_NUMBER = '20';
  const RC_FAIL_UNREGISTERED_NUMBER = '21';
  const RC_FAIL_PAYMENT_ALREADY_DONE = '22';
  const RC_FAIL_PLN_OVERLIMIT = '51';
  const RC_FAIL_PLN_CUTOFF = '52';
  const RC_FAIL_PLN_SUSPEND = '53';
  const RC_OK = '00';
  const RC_PENDING = '68';
  const RC_EVEN = '67';
  const RC_OK_BROADCAST = '01';

  const ROUTE_RESPONSE = 'response';
  const ROUTE_REPORT = 'report';
  const ROUTE_BOTH = 'both';

  const ALLOW_HEADERS = ['Authorization', 'Content-Type'];

  const POSTPAID_INQ_PREFIX = 'INQ';
  const POSTPAID_INQ_TYPE_CHECK = 'check';
  const POSTPAID_INQ_TYPE_PRE_PURCHASE = 'pre_purchase';

  const LOG_UNKNOWN_REPORT = 'unknown_report';
  const LOG_UNKNOWN_TRANSACTION = 'unknown_transaction';
  const LOG_UNKNOWN_FATAL_TRANSACTION = 'unknown_fatal_transaction';
  const LOG_UNMATCH_TRANSACTION = 'unmatch_transaction';

  const SF_PARAM_TOKEN = 'token';
  const SF_PARAM_USERID = 'userid';
  const SF_PARAM_USERNAME = 'username';
  const SF_PARAM_PIN = 'pin';
  const SF_PARAM_PASSWORD = 'password';
  const SF_PARAM_MSISDN = 'no';
  const SF_PARAM_CODE = 'kode';
  const SF_PARAM_REFID = 'refid';
  const SF_PARAM_TRXID = 'trxid';
  const SF_PARAM_PRICE = 'harga';
  const SF_PARAM_BALANCE = 'saldo';
  const SF_PARAM_SN = 'sn';
  const SF_PARAM_VOUCHER_CODE = 'voucher_code';
  const SF_PARAM_NUMBER = 'universal_number';
  const SF_PARAM_STATUS = 'status';
  const SF_PARAM_RESPONSE_CODE = 'responsecode';
  const SF_PARAM_UNIT_BALANCE = 'saldo_unit';
  const SF_PARAM_TIME_HMS = 'time_hms';
  const SF_PARAM_DATETIME = 'datetime';
  const SF_PARAM_REVERSAL_URL = 'reversal';
  const SF_PARAM_BANK = 'bank';
  const SF_PARAM_DEPOSIT = 'deposit';
  const SF_PARAM_PRODUCT_NAME = 'nama_produk';
  const SF_PARAM_ERROR = 'pesan_error';
  const SF_PARAM_DETAILS_LOOP = 'loop';
  const SF_PARAM_OWNER_NAME = 'nama';

  const SF_PARAM_POSTPAID_PERIODE = 'periode';
  const SF_PARAM_POSTPAID_MONTH_COUNT = 'jumlah_bulan';
  const SF_PARAM_POSTPAID_PRICE = 'tagihan';
  const SF_PARAM_POSTPAID_PRICE_TOTAL = 'total_tagihan';
  const SF_PARAM_POSTPAID_ADMIN = 'biaya_admin';
  const SF_PARAM_POSTPAID_FINE = 'denda';
  const SF_PARAM_POSTPAID_BILL_REST = 'sisa_tagihan';
  const SF_PARAM_POSTPAID_METER_CHANGES = 'sm';
  const SF_PARAM_POSTPAID_USAGES = 'pemakaian';

  const SF_PARAM_PLN_INFO = 'pln_info';
  const SF_PARAM_PLN_ID = 'pln_id';
  const SF_PARAM_PLN_TARIFF = 'pln_tariff';
  const SF_PARAM_PLN_POWER = 'pln_power';
  const SF_PARAM_PLN_KWH = 'pln_kwh';
  const SF_PARAM_PLN_REF = 'pln_ref';
  const SF_PARAM_PLN_MATERAI = 'pln_materai';
  const SF_PARAM_PLN_PPN = 'pln_ppn';
  const SF_PARAM_PLN_PPJ = 'pln_ppj';
  const SF_PARAM_PLN_RPTOKEN = 'pln_rptoken';

  const SF_PARAM_BPJS_PERSON_COUNT = 'bpjs_peserta';
  const SF_PARAM_BPJS_BRANCH_CODE = 'bpjs_kodecabang';
  const SF_PARAM_BPJS_BRANCH_NAME = 'bpjs_namacabang';

  const SF_PARAM_MF_NAME = 'mf_nama';
  const SF_PARAM_MF_CREDIT_CURRENT = 'mf_angsuranke';
  const SF_PARAM_MF_CREDIT_TOTAL = 'mf_angsurantotal';
  const SF_PARAM_MF_CREDIT_DUE_DATE = 'mf_jatuhtempo';
  const SF_PARAM_MF_PLATFORM = 'mf_platform';
  const SF_PARAM_MF_COLL_FEE = 'mf_biayapenagih';

  const SF_PATTERN_START = '%';
  const SF_PATTERN_END = '%';
  const SF_PATTERN = '/' . self::SF_PATTERN_START . '(.*?)' . self::SF_PATTERN_END . '/';

  const SIGN_HEADER = '#sign#';
  const SIGN_TYPE_MD5 = 'md5';
  const SIGN_TYPE_SHA1 = 'sha1';
  const SIGN_TYPE_SHA256 = 'sha256';
  const SIGN_TYPE_BASE64 = 'base64';
  const SIGN_TYPE_CUSTOM = 'custom';

  const SIGN_CUSTOM_OTOMAX = 'otomax';
  const SIGN_CUSTOM_TRUGEE = 'trugee';
  const SIGN_CUSTOM_KOPNUS = 'kopnus';

  const CONTAIN_HEADER = '#contain#';
  const COMPARE_HEADER = '#compare#';

  const DEFAULT_PARAM_TEXT = 'body';

  const REDIS_BILLER = 'odeo_core:billers';
  const REDIS_BILLER_PENDING_DISABLE = 'pending_disables';
  const REDIS_BILLER_POI_QUEUE = 'poi_queue_';
  const REDIS_BILLER_POI_COUNT = 'odeo-core:poi_count_';

  const SF_REQUESTS = [
    self::SF_PARAM_USERID,
    self::SF_PARAM_USERNAME,
    self::SF_PARAM_PIN,
    self::SF_PARAM_PASSWORD,
    self::SF_PARAM_MSISDN,
    self::SF_PARAM_CODE,
    self::SF_PARAM_TRXID,
    self::SF_PARAM_TOKEN,
    self::SF_PARAM_TIME_HMS,
    self::SF_PARAM_DATETIME,
    self::SF_PARAM_PRICE,
    self::SF_PARAM_REVERSAL_URL
  ];
  const SF_RESPONSES = [
    self::SF_PARAM_CODE,
    self::SF_PARAM_MSISDN,
    self::SF_PARAM_REFID,
    self::SF_PARAM_TRXID,
    self::SF_PARAM_PRICE,
    self::SF_PARAM_BALANCE,
    self::SF_PARAM_UNIT_BALANCE,
    self::SF_PARAM_SN,
    self::SF_PARAM_VOUCHER_CODE,
    self::SF_PARAM_OWNER_NAME,
    self::SF_PARAM_PLN_INFO,
    self::SF_PARAM_PLN_ID,
    self::SF_PARAM_PLN_TARIFF,
    self::SF_PARAM_PLN_POWER,
    self::SF_PARAM_PLN_KWH,
    self::SF_PARAM_PLN_REF,
    self::SF_PARAM_PLN_MATERAI,
    self::SF_PARAM_PLN_PPN,
    self::SF_PARAM_PLN_PPJ,
    self::SF_PARAM_PLN_RPTOKEN,
    self::SF_PARAM_BPJS_PERSON_COUNT,
    self::SF_PARAM_BPJS_BRANCH_CODE,
    self::SF_PARAM_BPJS_BRANCH_NAME,
    self::SF_PARAM_MF_NAME,
    self::SF_PARAM_MF_CREDIT_CURRENT,
    self::SF_PARAM_MF_CREDIT_TOTAL,
    self::SF_PARAM_MF_CREDIT_DUE_DATE,
    self::SF_PARAM_MF_PLATFORM,
    self::SF_PARAM_MF_COLL_FEE,
    self::SF_PARAM_POSTPAID_PERIODE,
    self::SF_PARAM_POSTPAID_MONTH_COUNT,
    self::SF_PARAM_POSTPAID_METER_CHANGES,
    self::SF_PARAM_POSTPAID_USAGES,
    self::SF_PARAM_POSTPAID_ADMIN,
    self::SF_PARAM_POSTPAID_PRICE,
    self::SF_PARAM_POSTPAID_PRICE_TOTAL
  ];
  const SF_REPLENISHMENTS = [
    self::SF_PARAM_BANK,
    self::SF_PARAM_DEPOSIT
  ];

  public static function getFormats() {
    return [
      self::TYPE_XML,
      self::TYPE_XMLRPC,
      self::TYPE_HTTP_QUERY,
      self::TYPE_JSON,
      self::TYPE_FORM,
      self::TYPE_TEXT
    ];
  }

  public static function getStatuses() {
    return [
      self::STATUS_SUCCESS => 'Sukses',
      self::STATUS_IN_QUEUE => 'Pending',
      self::STATUS_LOOP_FAIL => 'Sudah pernah terjadi',
      self::STATUS_FAIL => 'Gagal',
      self::STATUS_FAIL . '_' . self::RC_FAIL_EMPTY_STOCK => 'Gagal: biller stok kosong',
      self::STATUS_FAIL . '_' . self::RC_FAIL_DISABLED_STOCK => 'Gagal: stok tidak aktif/kode salah',
      self::STATUS_FAIL . '_' . self::RC_FAIL_BILLER_CUTOFF => 'Gagal: biller tutup/maintenance',
      self::STATUS_FAIL . '_' . self::RC_FAIL_BILLER_TOPUP_NOT_ENOUGH => 'Gagal: biller saldo tidak cukup',
      self::STATUS_FAIL . '_' . self::RC_FAIL_DISTURBANCE => 'Gagal: produk gangguan',
      self::STATUS_FAIL . '_' . self::RC_FAIL_WRONG_NUMBER => 'Gagal: nomor tujuan salah/masa tenggang/bukan prabayar',
      self::STATUS_FAIL . '_' . self::RC_FAIL_UNPROCESSED_NUMBER => 'Gagal: nomor tujuan tidak dapat diproses',
      self::STATUS_FAIL . '_' . self::RC_FAIL_UNREGISTERED_NUMBER => 'Gagal: nomor tujuan tidak terdaftar',
      self::STATUS_FAIL . '_' . self::RC_FAIL_PAYMENT_ALREADY_DONE => 'Gagal: tagihan nomor sudah terbayar',
      self::STATUS_FAIL . '_' . self::RC_FAIL_PLN_OVERLIMIT => 'Gagal: PLN melebihi batas KWH',
      self::STATUS_FAIL . '_' . self::RC_FAIL_PLN_CUTOFF => 'Gagal: PLN tutup',
      self::STATUS_FAIL . '_' . self::RC_FAIL_PLN_SUSPEND => 'Gagal: nomor PLN suspend. Mohon hubungi kantor PLN terdekat.'
    ];
  }

  public static function getFailedReasonToUser($resCode, $switcherStatus) {
    if (!in_array($switcherStatus, [SwitcherConfig::SWITCHER_REFUNDED, SwitcherConfig::SWITCHER_FAIL]) ||
      $resCode == null || $resCode == self::RC_FAIL) return '';
    $statuses = self::getStatuses();
    if (isset($statuses[self::STATUS_FAIL . '_' . $resCode])) return $statuses[self::STATUS_FAIL . '_' . $resCode];
    else return '';
  }

  public static function getWrongNumberGroup() {
    return [
      self::RC_FAIL_WRONG_NUMBER,
      self::RC_FAIL_PLN_OVERLIMIT,
      self::RC_FAIL_PLN_SUSPEND,
      self::RC_FAIL_UNREGISTERED_NUMBER
    ];
  }

  public static function getRoutes() {
    return [
      self::ROUTE_RESPONSE,
      self::ROUTE_REPORT
    ];
  }

  public static function getSignTypes() {
    return [
      self::SIGN_TYPE_MD5,
      self::SIGN_TYPE_SHA1,
      self::SIGN_TYPE_SHA256,
      self::SIGN_TYPE_BASE64,
      self::SIGN_TYPE_CUSTOM
    ];
  }

  public static function addPattern($string) {
    return self::SF_PATTERN_START . $string . self::SF_PATTERN_END;
  }

  public static function createSlug($string) {
    return 'http://api.odeo.co.id/v1/pulsa/' . $string . '/notify';
  }

  public static function checkPostpaidTotal($data) {
    return isset($data[self::SF_PARAM_POSTPAID_PRICE_TOTAL]) ? $data[self::SF_PARAM_POSTPAID_PRICE_TOTAL] :
      ((isset($data[self::SF_PARAM_POSTPAID_PRICE]) ? $data[self::SF_PARAM_POSTPAID_PRICE] : 0) +
        (isset($data[self::SF_PARAM_POSTPAID_FINE]) ? $data[self::SF_PARAM_POSTPAID_FINE] : 0)) +
        (isset($data[self::SF_PARAM_POSTPAID_BILL_REST]) ? $data[self::SF_PARAM_POSTPAID_BILL_REST] : 0) +
        (isset($data[self::SF_PARAM_POSTPAID_ADMIN]) ? $data[self::SF_PARAM_POSTPAID_ADMIN] : 0);
  }

}
