<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/14/17
 * Time: 19:36
 */

namespace Odeo\Domains\Constant;


class InventoryPricing {

  public static function getUpdateTime() {

    //satuan menit

    return [
      1 => trans('inventory/inventory_pricing.update_price_on_' . 1),
      5 => trans('inventory/inventory_pricing.update_price_on_' . 5),
      30 => trans('inventory/inventory_pricing.update_price_on_' . 30),
      60 => trans('inventory/inventory_pricing.update_price_on_' . 60),
      120 => trans('inventory/inventory_pricing.update_price_on_' . 120),
      240 => trans('inventory/inventory_pricing.update_price_on_' . 240),
      480 => trans('inventory/inventory_pricing.update_price_on_' . 480),
      720 => trans('inventory/inventory_pricing.update_price_on_' . 720),
      960 => trans('inventory/inventory_pricing.update_price_on_' . 960),
      1440 => trans('inventory/inventory_pricing.update_price_on_' . 1440),
      2880 => trans('inventory/inventory_pricing.update_price_on_' . 2880),
    ];


  }

}
