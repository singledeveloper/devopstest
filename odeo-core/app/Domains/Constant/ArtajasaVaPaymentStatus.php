<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 15/08/19
 * Time: 15.13
 */

namespace Odeo\Domains\Constant;

class ArtajasaVaPaymentStatus {

  const PENDING = 10000;
  const ACCEPTED = 30000;
  const SUCCESS = 50000;
  const SUSPECT = 80000;
  const FAILED = 90000;
  const SUSPECT_NEED_STATUS_RECHECK = 80004;
  const SUSPECT_IS_CANCELLED_BUT_NEED_STATUS_RECHECK = 80005;
  const SUSPECT_FOR_TESTING = 89000;
  const SUSPECT_NOT_FOUND_ON_AJ = 90001;

  public static function getPaymentGatewayStatus($status) {
    $map = [
      self::SUCCESS => PaymentGateway::SUCCESS,
      self::SUSPECT => PaymentGateway::SUSPECT,
      self::SUSPECT_NOT_FOUND_ON_AJ => PaymentGateway::SUSPECT,
      self::FAILED => PaymentGateway::FAILED
    ];
    return $map[$status];
  }


}