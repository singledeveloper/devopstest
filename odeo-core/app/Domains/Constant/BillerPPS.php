<?php

namespace Odeo\Domains\Constant;

class BillerPPS {

  const PROD_SERVER = 'https://119.110.68.35:8443/';
  const USER_ID = 'ODEO';
  const PASSWORD = 'e807f1fcf82d132f9bb018ca6738a19f';

  public static function checkWrongNumberPattern($string) {
    return checkPatterns(strtolower($string), [
      'gagal karena status nomor penerima',
      'phone number is blocked',
      'nomer tidak ditemukan',
      'nomor tidak ditemukan',
      'unidentified phone number',
      'nomor tujuan anda diblokir'
    ]);
  }

}