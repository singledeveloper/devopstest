<?php

namespace Odeo\Domains\Constant;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class AwsConfig {

  const S3_BASE_URL = 'https://odeo.s3-ap-southeast-1.amazonaws.com';

  public static function url($path, $minutes = 10) {
    return Storage::disk('s3')->temporaryUrl(
      $path, Carbon::now()->addMinutes($minutes)
    );
  }

}
