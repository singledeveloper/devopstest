<?php

namespace Odeo\Domains\Constant;

class PaymentChannel {

  const INVOICE = 'invoice';
  const PAYMENT_GATEWAY = 'payment_gateway';
  const ORDER = 'order';

}
