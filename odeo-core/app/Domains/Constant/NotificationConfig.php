<?php

namespace Odeo\Domains\Constant;

class NotificationConfig
{
    const URL = 'https://fcm.googleapis.com/fcm/send';
    const CONTENT_TYPE = 'application/json';
    const AUTHORIZATION = 'key=AIzaSyAxIjWqBkayfv4t1CJaa9JzK40J-WBC1Zg';
}