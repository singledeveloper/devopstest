<?php

namespace Odeo\Domains\Constant;

class SwitcherConfig {

  const DATACELL = 2;
  const RAJABILLER = 3;
  const MKM = 4;
  const INDOSMART = 5;
  const JATELINDO = 6;
  const MITRACOMM = 7;
  const BAKOEL = 8;
  const SERVINDO = 10;
  const PPS = 11;
  const JAVAH2H = 13;
  const ERATEL = 22;
  const ZENZIVA = 24;
  const MOBILEPULSA = 27;
  const PRINGSEWU = 29;
  const BLACKHAWK = 30;
  const KUDO = 31;
  const UNIPIN = 41;
  const GOC = 42;
  const REDIG = 43;
  const ARTAJASA_NEW = 44;
  const DARTMEDIA = 45;
  const INTERNAL_GOJEK = 118;

  const SWITCHER_PENDING = 10000;
  const SWITCHER_IN_QUEUE = 10001;
  const SWITCHER_VOID_IN_QUEUE = 10005;
  const SWITCHER_FAIL = 40000;
  const SWITCHER_FAIL_WRONG_NUMBER = 40001;
  const SWITCHER_FAIL_UNDEFINED_ROUTE = 40003;
  const SWITCHER_VOID_FAIL = 40007;
  const SWITCHER_COMPLETED = 50000;
  const SWITCHER_REFUNDED = 90000;
  const SWITCHER_VOIDED = 90001;

  const REPURCHASE_TRIES = 1;
  const MAX_PENDING_ALLOWED = 20;
  const MAX_BILLER_PENDING_ALLOWED = 2;
  const AUTO_DISABLE_INVENTORY_THRESHOLD = 10;
  const AUTO_DISABLE_INVENTORY_THRESHOLD_PERMANENT = 3;

  const BILLER_CREATED = 10000;
  const BILLER_IN_QUEUE = 10001;
  const BILLER_TIMEOUT = 10002;
  const BILLER_SUSPECT = 10004;
  const BILLER_SUCCESS = 20000;
  const BILLER_DUPLICATE_SUCCESS = 20001;
  const BILLER_FAIL = 40000;
  const BILLER_FAKE_SUCCESS = 40006;
  const BILLER_VOID_FAIL = 40008;
  const BILLER_INTERNAL_ERROR = 50000;
  const BILLER_UNKNOWN_ERROR = 50001;
  const BILLER_TESTING = 50002;
  const BILLER_VOIDED = 90002;

  const BILLER_STATUS_OK = 50000;
  const BILLER_STATUS_HIDDEN_GOJEK = 50001;
  const BILLER_STATUS_HIDDEN_OVO = 50002;
  const BILLER_STATUS_HIDDEN_TCASH = 50003;

  const ROUTE_ORDER_VERIFICATOR = 'order_verificator';
  const ROUTE_REPLENISHER = 'replenisher';
  const ROUTE_EDITOR = 'pulsa_editor';
  const ROUTE_DOUBLE_PURCHASE = 'double_purchase_inserter';
  const ROUTE_VOID_DOUBLE_PURCHASE = 'void_double_purchase_inserter';
  const ROUTE_BALANCER = 'balancer';
  const ROUTE_CASHBACK = 'cashback';
  const ROUTE_UNKNOWN_DIFF = 'unknown_diff';
  const ROUTE_REFUNDER = 'refunder';
  const ROUTE_VOID = 'void';

  const DUPLICATE_TYPE_SAME_DAY = 'same_day';
  const DUPLICATE_TYPE_24_HOURS = '24_hours';

  public static function debugNumber($number, $ignore = false) {
    if (substr($number, -2) == '14') return self::BILLER_FAIL;
    else if (substr($number, -2) == '01') {
      if ($ignore) {
        if (substr($number, -3) == '401') return self::BILLER_FAIL;
        return self::BILLER_SUCCESS;
      }
      return self::BILLER_IN_QUEUE;
    }
    else return self::BILLER_SUCCESS;
  }

  public static function getVendorSwitcherManagerById($id) {
    switch ($id) {
      case self::DATACELL:
        return app()->make(\Odeo\Domains\Biller\Datacell\DatacellManager::class);
      case self::RAJABILLER:
        return app()->make(\Odeo\Domains\Biller\RajaBiller\RajaBillerManager::class);
      case self::MKM:
        return app()->make(\Odeo\Domains\Biller\MKM\MKMManager::class);
      case self::INDOSMART:
        return app()->make(\Odeo\Domains\Biller\Indosmart\IndosmartManager::class);
      case self::JATELINDO:
        return app()->make(\Odeo\Domains\Biller\Jatelindo\JatelindoManager::class);
      case self::BAKOEL:
        return app()->make(\Odeo\Domains\Biller\Bakoel\BakoelManager::class);
      case self::SERVINDO:
        return app()->make(\Odeo\Domains\Biller\Servindo\ServindoManager::class);
      case self::PPS:
        return app()->make(\Odeo\Domains\Biller\PPS\PPSManager::class);
      case self::JAVAH2H:
        return app()->make(\Odeo\Domains\Biller\JavaH2H\JavaH2HManager::class);
      /*case self::MOBILEPULSA:
        return app()->make(\Odeo\Domains\Biller\MobilePulsa\MobilePulsaManager::class);*/
      case self::BLACKHAWK:
        return app()->make(\Odeo\Domains\Biller\Blackhawk\BlackhawkManager::class);
      case self::KUDO:
        return app()->make(\Odeo\Domains\Biller\Kudo\KudoManager::class);
      case self::UNIPIN:
        return app()->make(\Odeo\Domains\Biller\Unipin\UnipinManager::class);
      case self::GOC:
        return app()->make(\Odeo\Domains\Biller\GOC\GOCManager::class);
      case self::ARTAJASA_NEW:
        return app()->make(\Odeo\Domains\Biller\Artajasa\ArtajasaManager::class);
    }
    return '';
  }

  public static function getVendorSwitcherReplenisherById($id) {
    switch ($id) {
      case self::JAVAH2H:
        return app()->make(\Odeo\Domains\Biller\JavaH2H\Replenisher::class);
      case self::ERATEL:
        return app()->make(\Odeo\Domains\Biller\Eratel\Replenisher::class);
      case self::ZENZIVA:
        return app()->make(\Odeo\Domains\Vendor\SMS\Zenziva\Replenisher::class);
      case self::MOBILEPULSA:
        return app()->make(\Odeo\Domains\Biller\MobilePulsa\Replenisher::class);
      case self::PRINGSEWU:
        return app()->make(\Odeo\Domains\Biller\Pringsewu\Replenisher::class);
    }
    return '';
  }

  public static function getVendorSwitcherRepositoryById($id) {
    switch ($id) {
      case self::DATACELL:
        return app()->make(\Odeo\Domains\Biller\Datacell\Repository\OrderDetailDatacellRepository::class);
      case self::RAJABILLER:
        return app()->make(\Odeo\Domains\Biller\RajaBiller\Repository\OrderDetailRajaBillerRepository::class);
      case self::MKM:
        return app()->make(\Odeo\Domains\Biller\MKM\Repository\OrderDetailMkmRepository::class);
      case self::INDOSMART:
        return app()->make(\Odeo\Domains\Biller\Indosmart\Repository\OrderDetailIndosmartRepository::class);
      case self::JATELINDO:
        return app()->make(\Odeo\Domains\Biller\Jatelindo\Repository\OrderDetailJatelindoRepository::class);
      case self::BAKOEL:
        return app()->make(\Odeo\Domains\Biller\Bakoel\Repository\OrderDetailBakoelRepository::class);
      case self::SERVINDO:
        return app()->make(\Odeo\Domains\Biller\Servindo\Repository\OrderDetailServindoRepository::class);
      case self::PPS:
        return app()->make(\Odeo\Domains\Biller\PPS\Repository\OrderDetailPpsRepository::class);
      case self::JAVAH2H:
        return app()->make(\Odeo\Domains\Biller\JavaH2H\Repository\OrderDetailJavaH2hRepository::class);
      /**case self::MOBILEPULSA:
        return app()->make(\Odeo\Domains\Biller\MobilePulsa\Repository\OrderDetailMobilePulsaRepository::class);*/
      case self::BLACKHAWK:
        return app()->make(\Odeo\Domains\Biller\Blackhawk\Repository\OrderDetailBlackhawkRepository::class);
      case self::KUDO:
        return app()->make(\Odeo\Domains\Biller\Kudo\Repository\OrderDetailKudoRepository::class);
    }
    return '';
  }

  public static function getPluginName($status) {
    switch($status) {
      case self::BILLER_STATUS_HIDDEN_GOJEK:
        return 'plugin_gojek';
      case self::BILLER_STATUS_HIDDEN_TCASH:
        return 'plugin_tcash';
    }
    return 'plugin';
  }

  public static function getPluginPurchaser($status) {
    switch($status) {
      case self::BILLER_STATUS_HIDDEN_GOJEK:
        return app()->make(\Odeo\Domains\Biller\Gojek\Purchaser::class);
      case self::BILLER_STATUS_HIDDEN_TCASH:
        return app()->make(\Odeo\Domains\Biller\TCash\Purchaser::class);
    }
    return '';
  }

  public static function getPluginFee($status) {
    switch($status) {
      case self::BILLER_STATUS_HIDDEN_GOJEK:
        return 400;
      case self::BILLER_STATUS_HIDDEN_TCASH:
        return 25;
    }
    return 0;
  }

  public static function getSwitcherFailedStatus() {
    return [
      self::SWITCHER_VOID_IN_QUEUE,
      self::SWITCHER_FAIL,
      self::SWITCHER_FAIL_WRONG_NUMBER,
      self::SWITCHER_FAIL_UNDEFINED_ROUTE,
      self::SWITCHER_VOID_FAIL,
      self::SWITCHER_REFUNDED,
      self::SWITCHER_VOIDED,
    ];
  }
}