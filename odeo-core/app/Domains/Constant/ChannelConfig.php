<?php

namespace Odeo\Domains\Constant;

class ChannelConfig {

  const CHANNEL_MINIMUM_DEPOSIT = 500000;
  const REQUIRED_SECURITY_DEPOSIT = 500000;
  const DAILY_SECURITY_DEPOSIT = 10000;
  const MDR_REFERRER_MULTIPLIER = 0.5;
  const CODE_BASE_URL = 'http://odeo.s3-ap-southeast-1.amazonaws.com';

  const COMPANY_STORE_IDS = [ 5198, 5200 ];
}
