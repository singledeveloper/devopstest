<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/31/17
 * Time: 12:19 PM
 */

namespace Odeo\Domains\Constant;


class BillerVhancell {

  const EMAIL = 'centervhancell@jabber.at';
  const PHONE_NO = '0895351232125';
  const PIN = '1736';
  const TOPUP_REQUEST_URL = 'http://36.79.176.65:8080/webportal/api/h2hxml';

  public static function formatXML($data) {
    $xml = '';
    foreach ($data as $key => $val) {
      $xml .= '<member><name>' . $key . '</name><value><string>' . $val . '</string></value></member>';
    }
    $xml = '<?xml version="1.0"?><methodCall><methodName>topUpRequest</methodName><params><param><value><struct>' . $xml . '</struct></value></param></params></methodCall>';
    return $xml;
  }

  public static function getXMLResponse($data) {

    switch (SwitcherConfig::debugNumber($data['NOHP'])) {
      case SwitcherConfig::BILLER_FAIL:
        $response = '<?xml version="1.0"?><methodResponse><params><param><value><struct><member><name>RESPONSECODE</name><value>99</value></member><member><name>REQUESTID</name><value>' . $data["REQUESTID"] . '</value></member><member><name>MESSAGE</name><value>TOPUP GAGAL</value></member></struct></value></param></params></methodResponse>';
        break;
      case SwitcherConfig::BILLER_SUCCESS:
        $response = '<?xml version="1.0"?><methodResponse><params><param><value><struct><member><name>RESPONSECODE</name><value>00</value></member><member><name>REQUESTID</name><value>' . $data["REQUESTID"] . '</value></member><member><name>MESSAGE</name><value>Trx ' . $data['NOM'] . '.' . $data['NOHP'] . ' BERHASIL,Harga: 5.300 SN: 1169076472 Sisa Saldo: 80.000 - 5.000 = 75.000 @2018-09-03 00:00:00</value></member></struct></value></param></params></methodResponse>';
        break;
      default:
        $response = '<struct><member><name>RESPONSECODE</name><value>68</value></member><member><name>REQUESTID</name><value>1000000010</value></member><member><name>MESSAGE</name><value>Sedang di proses</value></member></struct>';
        break;
    }
    return $response;
  }

}