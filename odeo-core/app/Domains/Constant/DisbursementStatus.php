<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/3/17
 * Time: 3:53 PM
 */

namespace Odeo\Domains\Constant;


class DisbursementStatus {

  const PENDING = 10000;
  const PENDING_NOT_ENOUGH_DEPOSIT = 10001;
  const ON_PROGRESS = 30000;
  const ON_PROGRESS_WAIT_FOR_NOTIFY = 30001;
  const FAIL_PROCESSING = 40000;
  const COMPLETED = 50000;
  const SUSPECT = 80000;
  const FAILED = 90000;
  const FAILED_WRONG_ACCOUNT_NUMBER = 90001;
  const FAILED_CLOSED_BANK_ACCOUNT = 90003;
  const FAILED_BANK_REJECTION = 90004;
  const FAILED_VENDOR_DOWN = 90005;
  const FAILED_DUPLICATE_REQUEST = 90006;

}