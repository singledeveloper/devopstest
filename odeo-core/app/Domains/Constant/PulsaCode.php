<?php

namespace Odeo\Domains\Constant;

class PulsaCode {

  const TSEL = 'Telkomsel';
  const TSELTRANSFER = 'Telkomsel Transfer';
  const TSELDATA = 'Telkomsel Data';
  const TSELDATAMINI = 'Telkomsel Data Mini';
  const TSELDATABULK = 'Telkomsel Data Bulk';
  const TSELDATABULKSIMPATI = 'Telkomsel Data Bulk Simpati';
  const TSELDATABULKAS = 'Telkomsel Data Bulk AS';
  const TSELDATABULKLOOP = 'Telkomsel Data Bulk Loop';
  const TSELDATAFLASH = 'Telkomsel Data Flash';
  const TSELSMS = 'Telkomsel SMS';
  const TSELTELP = 'Telkomsel Telepon';
  const TSELROAM = 'Telkomsel Roaming';
  const TSELTCASH = 'Telkomsel tCash';
  const ISAT = 'Indosat';
  const ISATTRANSFER = 'Indosat Transfer';
  const ISATDATA = 'Indosat Data';
  const ISATDATAYELLOW = 'Indosat Yellow';
  const ISATDATAUNLIMITED = 'Indosat Unlimited';
  const ISATDATAFREEDOM = 'Indosat Freedom';
  const ISATDATARATU = 'Indosat Ratu';
  const ISATDATAEXTRA = 'Indosat Extra Kuota';
  const ISATSMS = 'Indosat SMS';
  const ISATTELP = 'Indosat Telepon';
  const ISATROAM = 'Indosat Roaming';
  const XL = 'XL';
  const XLTRANSFER = 'XL Transfer';
  const XLDATAHOTROD = 'XL HotRod';
  const XLDATACOMBOXTRA = 'XL Combo Xtra';
  const XLDATAEXTRA = 'XL Extra Kuota';
  const XLDATAMIFI = 'XL Data MIFI';
  const XLSMS = 'XL SMS';
  const XLTELP = 'XL Telepon';
  const XLROAM = 'XL Roaming';
  const TRI = 'Three';
  const TRITRANSFER = 'Three Transfer';
  const TRIDATA = 'Three Data';
  const TRIDATAAON = 'Three AON';
  const TRIDATAGETMORE = 'Three Get More';
  const TRIDATACINTA = 'Three Cinta';
  const TRIDATAEXTRA = 'Three Extra Kuota';
  const TRISMS = 'Three SMS';
  const TRITELP = 'Three Telepon';
  const TRIROAM = 'Three Roaming';
  const AXIS = 'Axis';
  const AXISTRANSFER = 'Axis Transfer';
  const AXISDATA = 'Axis Data';
  const AXISDATAVOUCHER = 'Axis Voucher';
  const AXISDATAVOUCHERAIGO = 'Axis Voucher AIGO';
  const AXISDATABRONET = 'Axis Bronet';
  const AXISDATABRONETTB = 'Axis Bronet Timeband';
  const AXISDATAOWSEM = 'Axis Bronet Owsem';
  const AXISDATAKZL = 'Axis Internet KZL';
  const AXISSMS = 'Axis SMS';
  const AXISTELP = 'Axis Telepon';
  const AXISROAM = 'Axis Roaming';
  const SMARTFREN = 'SmartFren';
  const SMARTFRENDATA = 'SmartFren Data';
  const BOLT = 'Bolt';
  const BOLTDATA = 'Bolt Data';
  const PLN = 'PLN Token';

  const GP = 'Kode Voucher Google Play';

  const GOJEK = 'Transportasi Gojek';
  const GRAB = 'Transportasi Grab';
  const GRABDRIVER = 'Transportasi Grab Driver';
  const ETOLL = 'Transportasi e-Toll';
  const ETOLLBNI = 'Transportasi e-Toll BNI';

  const DANA = 'eMoney DANA';
  const OVO = 'eMoney OVO';
  const LINKAJA = 'eMoney LinkAja';

  const MOBILELEGENDS = 'Game Mobile Legends';
  const GEMSCOOL = 'Game Gemscool';
  const MEGAXUS = 'Game Megaxus';
  const DIGICASH = 'Game Digicash';
  const GARENA = 'Game GArena';
  const QEON = 'Game Qeon';
  const VTCONLINE = 'Game VTC Online';
  const NETMARBLE = 'Game Netmarble';
  const FAVEO = 'Game Faveo';
  const OLEH4U = 'Game OLEH4U';
  const ZYNGA = 'Game Zynga';
  const ORANGEGAME = 'Game Orange';
  const LYTO = 'Game Lyto';
  const STEAM = 'Game Steam Wallet';
  const MOL = 'Game Mol';
  const PUBG = 'Game PUBG';
  const PSNID = 'Game Playstation Network ID';
  const PSNUS = 'Game Playstation Network US';
  const UNIPIN = 'Game UNIPIN';
  const GAMEON = 'Game GameON';
  const GRAVINDO = 'Game Gravindo';

  public static function getConstKeyByValue($searchVal) {

    $constants = (new \ReflectionClass(__CLASS__))->getConstants();

    foreach ($constants as $name => $value) {
      if ($value == $searchVal) return $name;
    }
    return '';
  }

  public static function getConstValueByKey($keyVal) {

    $constants = (new \ReflectionClass(__CLASS__))->getConstants();

    return isset($constants[$keyVal]) ? $constants[$keyVal] : '';
  }

  public static function groupPulsa() {
    return [
      self::TSEL, self::TSELTRANSFER, self::TSELSMS, self::TSELTELP, self::TSELROAM, self::TSELTCASH,
      self::XL, self::XLTRANSFER, self::XLSMS, self::XLTELP, self::XLROAM,
      self::ISAT, self::ISATTRANSFER, self::ISATSMS, self::ISATTELP, self::ISATROAM,
      self::TRI, self::TRITRANSFER, self::TRISMS, self::TRITELP, self::TRIROAM,
      self::SMARTFREN,
      self::AXIS, self::AXISTRANSFER, self::AXISSMS, self::AXISTELP, self::AXISROAM,
      self::BOLT
    ];
  }

  public static function groupSmsTelephonePackages() {
    return [
      self::TSELSMS, self::TSELTELP, self::TSELROAM,
      self::XLSMS, self::XLTELP, self::XLROAM,
      self::ISATSMS, self::ISATTELP, self::ISATROAM,
      self::TRISMS, self::TRITELP, self::TRIROAM,
      self::AXISSMS, self::AXISTELP, self::AXISROAM
    ];
  }

  public static function groupData() {
    return [
      self::TSELDATA, self::TSELDATAMINI, self::TSELDATABULK, self::TSELDATABULKAS, self::TSELDATABULKLOOP, self::TSELDATABULKSIMPATI, self::TSELDATAFLASH,
      self::XLDATAHOTROD, self::XLDATACOMBOXTRA, self::XLDATAEXTRA, self::XLDATAMIFI,
      self::ISATDATA, self::ISATDATAFREEDOM, self::ISATDATARATU, self::ISATDATAEXTRA, self::ISATDATAYELLOW, self::ISATDATAUNLIMITED,
      self::TRIDATA, self::TRIDATAAON, self::TRIDATACINTA, self::TRIDATAEXTRA, self::TRIDATAGETMORE,
      self::SMARTFRENDATA,
      self::AXISDATA, self::AXISDATAVOUCHER, self::AXISDATAVOUCHERAIGO, self::AXISDATABRONET, self::AXISDATABRONETTB, self::AXISDATAOWSEM, self::AXISDATAKZL,
      self::BOLTDATA
    ];
  }

  public static function groupBolt() {
    return [self::BOLT, self::BOLTDATA];
  }

  public static function groupPln() {
    return [self::PLN];
  }

  public static function groupTransportation() {
    return [self::GOJEK, self::GRAB, self::GRABDRIVER, self::ETOLL, self::ETOLLBNI];
  }

  public static function groupGameVoucher() {
    return [
      self::MOBILELEGENDS,
      self::GEMSCOOL,
      self::MEGAXUS,
      self::DIGICASH,
      self::GARENA,
      self::QEON,
      self::VTCONLINE,
      self::NETMARBLE,
      self::FAVEO,
      self::OLEH4U,
      self::ZYNGA,
      self::ORANGEGAME,
      self::LYTO,
      self::STEAM,
      self::MOL,
      self::PUBG,
      self::PSNID,
      self::PSNUS,
      self::UNIPIN,
      self::GAMEON,
      self::GRAVINDO
    ];
  }

  public static function groupGooglePlay() {
    return [self::GP];
  }

  public static function groupEMoney() {
    return [self::DANA, self::OVO, self::LINKAJA];
  }

  public static function groupOperatorTelkomsel() {
    return [
      self::TSEL,
      self::TSELTRANSFER,
      self::TSELDATA,
      self::TSELDATAMINI,
      self::TSELDATABULK,
      self::TSELDATABULKSIMPATI,
      self::TSELDATABULKAS,
      self::TSELDATABULKLOOP,
      self::TSELDATAFLASH,
      self::TSELSMS,
      self::TSELTELP,
      self::TSELROAM,
      self::TSELTCASH
    ];
  }

  public static function groupOperatorIndosat() {
    return [
      self::ISAT,
      self::ISATTRANSFER,
      self::ISATDATA,
      self::ISATDATAYELLOW,
      self::ISATDATAUNLIMITED,
      self::ISATDATAFREEDOM,
      self::ISATDATARATU,
      self::ISATDATAEXTRA,
      self::ISATSMS,
      self::ISATTELP,
      self::ISATROAM
    ];
  }

  public static function groupOperatorTri() {
    return [
      self::TRI,
      self::TRITRANSFER,
      self::TRIDATA,
      self::TRIDATAAON,
      self::TRIDATAGETMORE,
      self::TRIDATACINTA,
      self::TRIDATAEXTRA,
      self::TRISMS,
      self::TRITELP,
      self::TRIROAM
    ];
  }

  public static function groupOperatorXL() {
    return [
      self::XL,
      self::XLTRANSFER,
      self::XLDATAHOTROD,
      self::XLDATACOMBOXTRA,
      self::XLDATAEXTRA,
      self::XLDATAMIFI,
      self::XLSMS,
      self::XLTELP,
      self::XLROAM
    ];
  }

  public static function groupOperatorAxis() {
    return [
      self::AXIS,
      self::AXISTRANSFER,
      self::AXISDATA,
      self::AXISDATAVOUCHER,
      self::AXISDATAVOUCHERAIGO,
      self::AXISDATABRONET,
      self::AXISDATABRONETTB,
      self::AXISDATAOWSEM,
      self::AXISDATAKZL,
      self::AXISSMS,
      self::AXISTELP,
      self::AXISROAM
    ];
  }

  public static function groupOperatorSmartFren() {
    return [self::SMARTFREN, self::SMARTFRENDATA];
  }
}
