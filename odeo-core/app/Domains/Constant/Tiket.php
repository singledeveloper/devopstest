<?php

namespace Odeo\Domains\Constant;

class Tiket {

  const DEV_SECRET_KEY = 'd651cf65946c360af27766fab1798247';
  const DEV_CONFIRM_KEY = '8854ea';
  const DEV_API_URI = 'https://api-sandbox.tiket.com';
  const DEV_ODEO_BUSINESS_ID = '21419491';

  const PROD_SECRET_KEY = 'd651cf65946c360af27766fab1798247';
  const PROD_CONFIRM_KEY = 'badb90';
  const PROD_API_URI = 'https://api.tiket.com';
  const PROD_ODEO_BUSINESS_ID = '22277699';

  const ODEO_BUSINESS_NAME = 'Odeo';
  const ODEO_USERNAME = 'caholic@hotmail.com';

  const STATUS_SUCCESS = 'SUCCESS';
  const STATUS_BAD_REQUEST = 'BAD_REQUEST';
  const STATUS_NOT_FOUND = 'NOT_FOUND';

  const REQUEST_TIMEOUT = 120;

  const TIKET_ORDER_CREATED = 10000;
  const TIKET_ORDER_CHECKOUT = 10001;
  const TIKET_ORDER_CANCELLED = 90000;

}