<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 8:10 PM
 */

namespace Odeo\Domains\Constant;


class Service {

  const PLAN = 1;
  const OCASH = 2;
  const WARRANTY = 3;
  const PULSA = 4;
  const HOTEL = 5;
  const FLIGHT = 6;
  const PLN = 7;
  const BOLT = 8;
  const DOMAIN = 9;
  const ODEPOSIT = 10;
  const CREDIT_BILL = 11;
  const PAKET_DATA = 12;
  const PLN_POSTPAID = 13;
  const PLN_NONTAGLIS = 14;
  const BPJS_KES = 15;
  const BPJS_TK = 16;
  const PULSA_POSTPAID = 17;
  const BROADBAND = 18;
  const LANDLINE = 19;
  const PDAM = 20;
  const USER_INVOICE = 21;
  const GAME_VOUCHER = 22;
  const GOOGLE_PLAY = 23;
  const TRANSPORTATION = 24;
  const MULTI_FINANCE = 25;
  const MD_PLUS = 26;
  const PAYMENT_GATEWAY = 27;
  const DDLENDING_LOAN = 28;
  const DDLENDING_PAYMENT = 29;
  const PGN = 30;
  const EMONEY = 31;
  const EDC_TRANSACTION = 32;

  const PPOB = [
    self::PULSA,
    self::PLN,
    self::BOLT,
    self::PULSA_POSTPAID,
    self::PLN_POSTPAID,
    self::PLN_NONTAGLIS,
    self::BPJS_KES,
    self::LANDLINE,
    self::BROADBAND,
    self::PAKET_DATA,
    self::PDAM,
    self::GAME_VOUCHER,
    self::GOOGLE_PLAY,
    self::TRANSPORTATION,
    self::MULTI_FINANCE,
    self::PGN,
    self::EMONEY
  ];

  public static function getServicesForAgents() {
    return [
      self::PULSA,
      self::PAKET_DATA,
      self::BOLT,
      self::PLN,
      self::GAME_VOUCHER,
      self::GOOGLE_PLAY,
      self::BPJS_KES,
      self::PLN_POSTPAID,
      self::PULSA_POSTPAID,
      self::BROADBAND,
      self::LANDLINE,
      self::MULTI_FINANCE,
      self::TRANSPORTATION
    ];
  }

  public static function getConstKeyByValue($searchVal) {

    $constants = (new \ReflectionClass(__CLASS__))->getConstants();

    foreach ($constants as $name => $value) {
      if ($value == $searchVal) return $name;
    }

    return null;

  }

  public static function getConstValueByKey($searchVal) {

    $constants = (new \ReflectionClass(__CLASS__))->getConstants();

    foreach ($constants as $name => $value) {
      if (strtolower($name) == strtolower($searchVal)) return $value;
    }

    return null;

  }

  public static function getValidatorVariables($serviceId) {

    switch($serviceId) {
      case self::PLAN:
        return [
          'store_id' => 'required',
          'plan_id' => 'required'
        ];
        break;
      case self::PULSA:
      case self::BOLT:
      case self::PAKET_DATA:
      case self::PLN:
        return [
          'store_id' => 'required',
          'item_id' => 'required',
          'item_detail.operator' => 'required',
          'item_detail.number' => 'required|regex:/[0-9]*$/|min:8|max:16'
        ];
        break;
      case self::PLN_POSTPAID:
        return [
          'store_id' => 'required',
          'item_id' => 'required',
          'item_detail.number' => 'required|regex:/[0-9]*$/|min:8|max:16'
        ];
        break;
      case self::EMONEY:
      case self::TRANSPORTATION:
      case self::MULTI_FINANCE:
        return [
          'store_id' => 'required',
          'item_id' => 'required',
          'item_detail.number' => 'required'
        ];
        break;
      case self::GAME_VOUCHER:
        return [
          'store_id' => 'required',
          'item_id' => 'required',
          'item_detail.number' => 'required'
        ];
        break;
      case self::GOOGLE_PLAY:
        return [
          'store_id' => 'required',
          'item_id' => 'required'
        ];
        break;
      case self::PULSA_POSTPAID:
        return [
          'store_id' => 'required',
          'item_id' => 'required',
          'item_detail.number' => 'required|regex:/[0-9]*$/|min:8|max:16',
          'item_detail.postpaid_type' => 'required'
        ];
        break;
      case self::BPJS_KES:
        return [
          'store_id' => 'required',
          'item_id' => 'required',
          'item_detail.number' => 'required|regex:/[0-9]*$/|min:8|max:16',
        ];
        break;
      case self::BPJS_TK:
        return [
          'store_id' => 'required',
          'item_id' => 'required',
          'item_detail.nik' => 'required|regex:/[0-9]*$/'
        ];
        break;
      case self::FLIGHT:
        return [
          'store_id' => 'required',
          'item_id' => 'required'
        ];
        break;
      case self::HOTEL:
        return [
          'store_id' => 'required',
          'item_id' => 'required',
          'item_detail.book_uri' => 'required'
        ];
        break;
      case self::WARRANTY:
        return [
          'store_id' => 'required',
          'warranty_id' => 'required'
        ];
        break;
      case self::CREDIT_BILL:
        return [
          'amount.amount' => 'required|integer|min:2000',
        ];
        break;
      case self::USER_INVOICE:
        return [
          'biller_id' => 'required',
        ];
        break;
      case self::MD_PLUS:
        return [
          'store_id' => 'required',
          'md_plus_plan_id' => 'required',
          'is_auto_renew' => 'required'
        ];
        break;
      default:
        return [];
        break;
    }
  }

}
