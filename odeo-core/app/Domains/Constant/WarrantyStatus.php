<?php

namespace Odeo\Domains\Constant;

class WarrantyStatus {
 
 const PENDING = 10000;
 const OK = 50000;
 const OVERWRITTEN = 90000;
 
}
