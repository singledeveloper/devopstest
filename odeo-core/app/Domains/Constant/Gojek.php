<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/10/17
 * Time: 11:58 PM
 */

namespace Odeo\Domains\Constant;


class Gojek {

  const PENDING_OTP = '10001';
  const PENDING_PIN = '10002';
  const ON_HOLD = '30000';
  const ON_HOLD_PERMANENT = '30001';
  const CONNECTED = '50000';
  const REMOVED = '90000';

  const LOGIN_GRANT_TYPE = 'password';
  const LOGIN_CLIENT_ID = 'gojek:cons:android';
  const LOGIN_SCOPES = 'gojek:customer:transaction gojek:customer:readonly';

  const REDIS_GOJEK = 'odeo-core:gojek';
  const REDIS_GOJEK_REQUEST_COUNT = 'odeo-core:gojek-req-count';
  const REDIS_GOJEK_NO_ACC_COUNT = 'odeo-core:gojek-no-acc-count';
  const REDIS_GOJEK_TOKEN_CACHE = 'odeo-core:gojek-token-cache';
  const REDIS_KEY_GOJEK_LAST_INQUIRY_CHECK_DATE = 'last_inquiry_check_date';
  const REDIS_KEY_HOLD_FLAG = 'on_hold';
  const REDIS_KEY_GOJEK_VERSION = 'latest_version';
  const REDIS_KEY_USER_TRANSACTION_LOCK = 'user_lock_';
  const REDIS_KEY_COUNTER_LOCK = 'counter_lock_';
  const REDIS_KEY_DEPOSIT_COUNTER_LOCK = 'deposit_counter_lock_';
  const REDIS_KEY_HOLD_PERMANENT = 'on_hold_permanent_';

  const MAX_MONTH_CASH = 20000000;
  const MAX_DEPOSIT_COUNTER = 15;
  const WITHDRAW_PREFIX = 898;
  const TOPUP_MULTIPLIER = 5000000;

  public static function generateUniqueId($length = 16) {
    $string = 'abcdefghijklmnopqrstuvwxyz1234567890';
    $uniqueId = '';
    for ($x=0; $x<$length; $x++) {
      $uniqueId .= $string[rand(0, strlen($string) - 1)];
    }
    return $uniqueId;

  }

  const GOJEK_SUPPS = [
    3474 => 45506,
    5239 => 83488,
    "A" => 26580,
    //"B" => 14,
    8219 => 135782,
    8260 => 136534
  ];
}
