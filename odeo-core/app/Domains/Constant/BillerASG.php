<?php

namespace Odeo\Domains\Constant;

use Carbon\Carbon;

class BillerASG {

  const GLC_PROJECT_ID = 'GLC';
  const GLC_FM_PROJECT_ID = 2134;
  const SCK_PROJECT_ID = 'SCK';

  const PAID = 'paid';
  const UNPAID = 'unpaid';
  const OTYPE_OWNER = 'pemilik';
  
  const ALL = 'all';

  const USERNAME = 'timothy@qlue.id';
  const PASSWORD = 'User*456#';
  const GRANT_TYPE = 'password';

  const BASE_URL = 'https://ap.agungsedayu.com/';
  const BASE_URL_STAGING = 'https://ap.agungsedayu.com/';
  const TOKEN_URL = self::BASE_URL . 'token';
  const TOKEN_URL_STAGING = self::BASE_URL_STAGING . 'token';

  const ODEO_USER_ID = 110283;
  
  const NOTIFICATION_URL = 'https://qluster-asg-api.appspot.com/api/residents/notification';
  const NOTIFICATION_URL_STAGING = 'https://soc-dot-qluster-demo.appspot.com/api/residents/notification'; 

  const NOTIFICATION_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiajlzNXM3dHEiLCJpYXQiOjE1MjYyODM2MTV9.vG8weihp34wJNwKiawIrgiiwm31SITQHcXWhmdZt6sc';
  const NOTIFICATION_TOKEN_STAGING = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiajlzNXM3dHEiLCJpYXQiOjE1MjYyODM2NTV9.9lkoL8dcfQF3VGabasowigrzwelu6ooVoY2BljihlLY';

  const VIRTUAL_ACCOUNT_VENDOR_FEE = [
    VirtualAccount::VENDOR_PRISMALINK_BCA => 5300,
    VirtualAccount::VENDOR_DIRECT_PERMATA => 5300,
    VirtualAccount::VENDOR_DIRECT_ARTAJASA => 5300,
    VirtualAccount::VENDOR_DIRECT_CIMB => 5300,
    VirtualAccount::VENDOR_DIRECT_MANDIRI => 5300,
    VirtualAccount::VENDOR_DOKU_BRI => 5300,
    VirtualAccount::VENDOR_DOKU_ALFA => 5300,
  ];

  const ODEO_BILLER_PROJECT_MAP = [
    self::GLC_PROJECT_ID => UserInvoice::BILLER_ASG_GLC,
    self::GLC_FM_PROJECT_ID => UserInvoice::BILLER_ASG_GLC_FM,
    self::SCK_PROJECT_ID => UserInvoice::BILLER_ASG_SCK
  ];

  public static function getDebitMdrPercentage() {
    if (Carbon::now()->greaterThanOrEqualTo(Carbon::create(2019, 4, 1))) {
      return 1;
    }
    return 0.15;
  }

  public static function getCreditMdrPercentage() {
    return 3;
  }

  public static function getDebitBillerFee() {
    return 5300;
  }

  public static function getCreditBillerFee() {
    return 5300;
  }

  public static function getOdeoUserId() {
    if (isProduction()) return self::ODEO_USER_ID;
    else if (isStaging()) return 16506;
    else return 26;
  }

  public static function getBillerByProjectId($projectId) {
    return self::ODEO_BILLER_PROJECT_MAP[$projectId] ?? 0;
  }

  public static function getProjectIdByBiller($billerId) {
    return array_search($billerId, self::ODEO_BILLER_PROJECT_MAP);
  }
}
