<?php

namespace Odeo\Domains\Constant;

class RedigDisbursementMutation {

  const TYPE_TOPUP = 'topup';
  const TYPE_DISBURSEMENT = 'disbursement';
  const TYPE_FEE = 'fee';
  const TYPE_REFUND_FEE = 'refund_fee';
  const TYPE_REFUND_DISBURSEMENT = 'refund_disbursement';

  const REFERENCE_TYPE_REDIG_DISBURSEMENT = 'disbursement_redig_disbursements';

}