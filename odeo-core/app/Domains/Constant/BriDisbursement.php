<?php

namespace Odeo\Domains\Constant;

use Carbon\Carbon;

class BriDisbursement {

  const PENDING = '10000';
  const SUCCESS = '50000';
  const FAIL = '90000';

  const RESPONSE_CODE_INQUIRY_FAILED = 'INQUIRY_FAILED';

  const BANK_REJECTION_ERROR_DESC = [
    'Saldo Tidak Cukup',
    'Teller record in use',
    'VA number not found',
    'Duplicate journal seq#',
    'Nomor VA expired'
  ];

  const COST = 100;

  public static function isOperational($hourStart = 3, $hourEnd = 23) {
    $now = Carbon::now();
    if ($now->hour <= $hourEnd && $now->hour >= $hourStart) {
      return true;
    }
    return false;
  }

}