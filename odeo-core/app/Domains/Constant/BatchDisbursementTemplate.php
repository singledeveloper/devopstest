<?php

namespace Odeo\Domains\Constant;

class BatchDisbursementTemplate {

  const VERSION = 'Version 1';
  const SHEET_INFORMATION = 'information';
  const SHEET_TEMPLATE = 'template';

}
