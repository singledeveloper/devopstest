<?php

namespace Odeo\Domains\Constant;

class DataExporterType {

  const CSV = 'csv';
  const PDF = 'pdf';
  const XLSX = 'xlsx';
  const ODS = 'ods';

}
