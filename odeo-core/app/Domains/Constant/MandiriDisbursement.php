<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 26/03/19
 * Time: 17.24
 */

namespace Odeo\Domains\Constant;


class MandiriDisbursement {

  const PENDING = '10000';
  const TIMEOUT = '30000';
  const SUCCESS = '50000';
  const SUSPECT = '20000';
  const FAIL = '90000';

  const INQUIRY_COST = 0;
  const DISBURSEMENT_COST = 150;

  const RESPONSE_CODE_INQUIRY_FAILED = 'INQUIRY_FAILED';
}