<?php

namespace Odeo\Domains\Constant;

class SupplierTemplate {

  const ERATEL = 6;
  const IDBILLER = 13;

  public static function getTemplateIds() {
    return [
      self::ERATEL,
      self::IDBILLER
    ];
  }

}