<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/26/16
 * Time: 11:08 PM
 */

namespace Odeo\Domains\Constant;


class ServiceDetail {

  const PLAN_ODEO = 1;
  const TOPUP_ODEO = 2;
  const WARRANTY_ODEO = 3;
  const PULSA_ODEO = 4;
  const PULSA_SEPULSA = 5;
  const HOTEL_TRAVELOKA = 6;
  const HOTEL_TIKET = 7;
  const FLIGHT_TRAVELOKA = 8;
  const FLIGHT_TIKET = 9;
  const PLN_ODEO = 10;
  const BOLT_ODEO = 11;
  const DOMAIN_ODEO = 12;
  const DEPOSIT_ODEO = 13;
  const CREDIT_BILL_ODEO = 14;
  const PAKET_DATA_ODEO = 15;
  const PLN_POSTPAID_ODEO = 16;
  const PLN_NONTAGLIS_ODEO = 17;
  const BPJS_KES_ODEO = 18;
  const BPJS_TK_ODEO = 19;
  const PULSA_POSTPAID_ODEO = 20;
  const BROADBAND_ODEO = 21;
  const LANDLINE_ODEO = 22;
  const PDAM_ODEO = 23;
  const USER_INVOICE_ODEO = 24;
  const GAME_VOUCHER_ODEO = 25;
  const GOOGLE_PLAY_ODEO = 26;
  const TRANSPORTATION_ODEO = 27;
  const MULTI_FINANCE_ODEO = 28;
  const MD_PLUS_ODEO = 29;
  const PAYMENT_GATEWAY = 30;
  const DDLENDING_LOAN = 31;
  const DDLENDING_PAYMENT = 32;
  const PGN_ODEO = 33;
  const EMONEY_ODEO = 34;
  const EDC_TRANSACTION = 35;

  public static function getServiceId($serviceDetailId) {
    switch ($serviceDetailId) {
      case self::PLAN_ODEO:
        return Service::PLAN;
        break;
      case self::TOPUP_ODEO:
        return Service::OCASH;
        break;
      case self::WARRANTY_ODEO:
        return Service::WARRANTY;
        break;
      case self::PULSA_ODEO:
      case self::PULSA_SEPULSA:
        return Service::PULSA;
        break;
      case self::HOTEL_TRAVELOKA:
      case self::HOTEL_TIKET:
        return Service::HOTEL;
        break;
      case self::FLIGHT_TRAVELOKA:
      case self::FLIGHT_TIKET:
        return Service::FLIGHT;
        break;
      case self::PLN_ODEO:
        return Service::PLN;
        break;
      case self::BOLT_ODEO:
        return Service::BOLT;
        break;
      case self::DOMAIN_ODEO:
        return Service::DOMAIN;
        break;
      case self::DEPOSIT_ODEO:
        return Service::ODEPOSIT;
        break;
      case self::CREDIT_BILL_ODEO:
        return Service::CREDIT_BILL;
        break;
      case self::PAKET_DATA_ODEO:
        return Service::PAKET_DATA;
        break;
      case self::PLN_POSTPAID_ODEO:
        return Service::PLN_POSTPAID;
        break;
      case self::PLN_NONTAGLIS_ODEO:
        return Service::PLN_NONTAGLIS;
        break;
      case self::BPJS_KES_ODEO:
        return Service::BPJS_KES;
        break;
      case self::BPJS_TK_ODEO:
        return Service::BPJS_TK;
        break;
      case self::PULSA_POSTPAID_ODEO:
        return Service::PLN_POSTPAID;
        break;
      case self::BROADBAND_ODEO:
        return Service::BROADBAND;
        break;
      case self::LANDLINE_ODEO:
        return Service::LANDLINE;
        break;
      case self::PDAM_ODEO:
        return Service::PDAM;
        break;
      case self::PGN_ODEO:
        return Service::PGN;
        break;
      case self::EMONEY_ODEO:
        return Service::EMONEY;
        break;
      case self::USER_INVOICE_ODEO:
        return Service::USER_INVOICE;
        break;
      case self::GAME_VOUCHER_ODEO:
        return Service::GAME_VOUCHER;
        break;
      case self::GOOGLE_PLAY_ODEO:
        return Service::GOOGLE_PLAY;
        break;
      case self::TRANSPORTATION_ODEO:
        return Service::TRANSPORTATION;
        break;
      case self::MULTI_FINANCE_ODEO:
        return Service::MULTI_FINANCE;
        break;
      case self::MD_PLUS_ODEO:
        return Service::MD_PLUS;
        break;
      case self::PAYMENT_GATEWAY:
        return Service::PAYMENT_GATEWAY;
        break;
      case self::DDLENDING_LOAN:
        return Service::DDLENDING_LOAN;
        break;
      case self::DDLENDING_PAYMENT:
        return Service::DDLENDING_PAYMENT;
      case self::EDC_TRANSACTION:
        return Service::EDC_TRANSACTION;
        break;
    }
    return null;
  }

  public static function groupPpob() {
    return [
      self::PLN_POSTPAID_ODEO,
      self::PLN_NONTAGLIS_ODEO,
      self::BPJS_KES_ODEO,
      self::BPJS_TK_ODEO,
      self::PULSA_POSTPAID_ODEO,
      self::PULSA_ODEO,
      self::PAKET_DATA_ODEO,
      self::BOLT_ODEO,
      self::PLN_ODEO
    ];
  }

  public static function groupPostpaid() {
    return [
      self::PLN_POSTPAID_ODEO,
      self::PLN_NONTAGLIS_ODEO,
      self::BPJS_KES_ODEO,
      self::BPJS_TK_ODEO,
      self::PULSA_POSTPAID_ODEO,
      self::PGN_ODEO,
      self::PDAM_ODEO,
      self::MULTI_FINANCE_ODEO,
      self::BROADBAND_ODEO,
      self::LANDLINE_ODEO
    ];
  }

  public static function getConstKeyByValue($searchVal) {

    $constants = (new \ReflectionClass(__CLASS__))->getConstants();

    foreach ($constants as $name => $value) {
      if ($value == $searchVal) return $name;
    }

    return null;

  }


  public static function getIdFromCurrentData($data) {

    $parser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);

    $serviceName = strtolower(Service::getConstKeyByValue($data['service_id']));
    $currentData = json_decode($parser->currentData($data['plan_id'],
      json_encode($data['current_data']), false, isset($data['can_supply']) ? $data['can_supply'] : false));

    return $currentData->{$serviceName};


  }

}
