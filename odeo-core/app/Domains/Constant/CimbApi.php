<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 24/01/18
 * Time: 11.31
 */

namespace Odeo\Domains\Constant;

class CimbApi {

  const WITHDRAW_OCASH_PREFIX = 'WOC';
  const BILLER_REPLENISHMENT_PREFIX = 'BREP';
  const API_DISBUSEMENT_PREFIX = 'ADIS';
  const GET_MUTATION_PREFIX = 'GMUT';
  const GET_BALANCE_PREFIX = 'GBA';

  static function generateSeqUniqTrxId($prefix) {
    return $prefix . round(microtime(true) * 1000);
  }
}
