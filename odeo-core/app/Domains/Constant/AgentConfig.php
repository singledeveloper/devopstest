<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 12:10 AM
 */

namespace Odeo\Domains\Constant;


class AgentConfig {

  const AVAILABLE = 10000;
  const PENDING = 10001;
  const ACCEPTED = 50000;

  const MAX_BONUS = 10000;
  const STEP_BONUS = 1000;

  const ROLE_AGENT_SERVER = 'agent_server';
  const ROLE_AGENT = 'agent';
  const ROLE_SERVER = 'server';

  const REFERENCE_TYPE_ODEO = 'odeo';
  const REFERENCE_TYPE_SUPPLY = 'supply';

  const MAX_PRIORITY = 5;
}