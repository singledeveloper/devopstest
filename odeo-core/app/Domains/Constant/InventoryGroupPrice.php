<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/3/17
 * Time: 17:28
 */

namespace Odeo\Domains\Constant;


class InventoryGroupPrice {

  const MARKET_PLACE = 'market_place';
  const AGENT_DEFAULT = 'agent_default';
  const CUSTOM = 'custom';
  const SELF = 'self';

}