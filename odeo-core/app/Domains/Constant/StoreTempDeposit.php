<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 12:10 AM
 */

namespace Odeo\Domains\Constant;


class StoreTempDeposit {

  const AGENT_ORDER = 'agent_order';
  const AGENT_SERVER_ORDER = 'agent_server_order';
  const ORDER = 'order';
  const CART = 'cart';
  const SUPPLIER_CASH = 'supplier_cash';
}