<?php

namespace Odeo\Domains\Constant;

class Obox {

  /*const SERVER1 = 'obox.supplier1@jabber.id';
  const SERVER1_PIN = '140489';
  const SERVER2 = 'obox.supplier2@jabber.id';
  const SERVER2_PIN = '241632';*/
  const CENTER = 'obox.center1@jabber.id';
  const CENTER2 = 'obox.center2@jabber.id';
  const SENDER = 'obox.sender1@jabber.id';

  public static function getParserPath($cmd) {
    switch($cmd) {
      case Inline::CMD_AGENT_REGISTRATION:
        return \Odeo\Domains\Account\SMSRegistrar::class;
      case Inline::CMD_SALDO_SHORT:
        return \Odeo\Domains\Transaction\Vendor\SMS\CashSelector::class;
      case Inline::CMD_CEK_HARGA_SHORT:
        return \Odeo\Domains\Inventory\Helper\Vendor\Odeo\SMSCenterPulsaSearcher::class;
      case Inline::CMD_BELI:
        return \Odeo\Domains\Order\Vendor\SMS\OrderPurchaser::class;
      case Inline::CMD_CEK_ORDER_BY_DEST:
        return \Odeo\Domains\Order\Vendor\SMS\OrderSelector::class;
      case Inline::CMD_TIKET:
        return \Odeo\Domains\Order\Vendor\SMS\OrderTopupRequester::class;
      case Inline::CMD_HAPUS_TIKET:
        return \Odeo\Domains\Order\Vendor\SMS\OrderTopupCanceller::class;
      case Inline::CMD_CHANGE_PIN_SHORT:
        return \Odeo\Domains\Vendor\SMS\Center\PasswordUpdateParser::class;
      case Inline::CMD_TAMBAH_NOMOR_SHORT:
        return \Odeo\Domains\Vendor\SMS\Center\RegisterParser::class;
      case Inline::CMD_COMPLAINT:
        return \Odeo\Domains\Vendor\SMS\Center\ComplaintParser::class;
      default:
        return \Odeo\Domains\Order\Vendor\SMS\OrderPurchaser::class;
    }
  }

  public static function getDiscounted($price, $percentage = 1.75) {
    return $price - floor($percentage / 100 * $price);
  }

  public static function getCommandDescriptions() {
    $descriptions = Inline::COMMAND_DESCRIPTIONS;
    return [
      $descriptions[Inline::CMD_AGENT_REGISTRATION],
      $descriptions[Inline::CMD_SALDO_SHORT],
      $descriptions[Inline::CMD_CEK_HARGA_SHORT],
      $descriptions[Inline::CMD_TIKET],
      $descriptions[Inline::CMD_HAPUS_TIKET],
      $descriptions[Inline::CMD_BELI],
      $descriptions[Inline::CMD_CEK_ORDER_BY_DEST],
      $descriptions[Inline::CMD_CHANGE_PIN_SHORT],
      $descriptions[Inline::CMD_TAMBAH_NOMOR_SHORT],
      $descriptions[Inline::CMD_COMPLAINT]
    ];
  }

}