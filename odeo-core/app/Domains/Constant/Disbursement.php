<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/3/17
 * Time: 3:53 PM
 */

namespace Odeo\Domains\Constant;


class Disbursement {

  const DISBURSEMENT_FOR_WITHDRAW_OCASH = 'withdraw_ocash';
  const API_DISBURSEMENT = 'api_disbursement';
  const DISBURSEMENT = 'disbursement';
  const DISBURSEMENT_FOR_BILLER_REPLENISHMENT = 'biller_replenishment';
  const INTERNAL_REFUND = 'internal_refund';

  const SALES_TYPE_RECIPIENTS = 'disbursement_recipients';
  const SALES_TYPE_VENDORS = 'disbursement_vendors';

  public static function isAutoDisbursement($bankId) {
    return in_array(strtolower($bankId), [
      Bank::BCA,
      Bank::BNI,
      Bank::BRI,
      Bank::MANDIRI,
      Bank::CIMBNIAGA,
      Bank::MANDIRI_SYARIAH,
      Bank::MUAMALAT
    ]);
  }

}