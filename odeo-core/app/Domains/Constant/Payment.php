<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 11:30 PM
 */

namespace Odeo\Domains\Constant;


use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository\BankPermataInquiryRepository;

class Payment {

  // 00 = buy inventory as user
  // 01 = buy plan
  // 02 = topup oCash
  // 03 = buy warranty
  // 04 = topup oDeposit
  // 05 = buy inventory as seller (cod)
  // 06 = buy inventory using oPay

  const ANDROID = 100;
  const ANDROID_PLAN = 101;
  const ANDROID_OCASH = 102;
  const ANDROID_WARRANTY = 103;
  const ANDROID_ODEPOSIT = 104;
  const ANDROID_STORE = 105;
  const ANDROID_OPAY = 106;
  const ANDROID_MD_PLUS = 107;

  const WEB_STORE = 200;

  const WEB_APP = 300;
  const WEB_APP_PLAN = 301;
  const WEB_APP_OCASH = 302;
  const WEB_APP_WARRANTY = 303;
  const WEB_APP_ODEPOSIT = 304;
  const WEB_APP_STORE = 305;
  const WEB_APP_MD_PLUS = 307;

  const IOS = 400;
  const IOS_PLAN = 401;
  const IOS_OCASH = 402;
  const IOS_WARRANTY = 403;
  const IOS_ODEPOSIT = 404;
  const IOS_STORE = 405;
  const IOS_MD_PLUS = 407;

  const ADMIN = 500;
  const ADMIN_PLAN = 501;
  const ADMIN_OCASH = 502;

  const WEB_SWITCHER = 600;

  const AFFILIATE = 700;

  const TELEGRAM = 800;

  const JABBER = 900;

  const SMS_CENTER = 1000;

  const AUTO_RECURRING = 1100;

  const VIRTUAL_ACCOUNT = 1300;

  const PAYMENT_GATEWAY = 1400;

  const AUTO_RECURRING_ODEPOSIT = 1500;

  const DEBIT_CREDIT_CARD_PRESENT_BNI_SWITCHING = 1600;

  const H2H = 1700;

  const SELLER_CENTER = 1800;
  const SELLER_CENTER_OCASH = 1802;

  const DOKU = 1;
  const KREDIVO = 2;
  const VERITRANS = 3;
  const ODEO = 4;

  const REQUEST_PAYMENT_TYPE_VIEW = 1;
  const REQUEST_PAYMENT_TYPE_URL = 2;
  const REQUEST_PAYMENT_TYPE_REDIRECT_TO_PAYMENT_DETAIL = 3;
  const REQUEST_PAYMENT_TYPE_SHOW_DETAILS = 4;
  const REQUEST_PAYMENT_TYPE_VERIFY_PIN = 5;

  const OPC_GROUP_CC = 1;
  const OPC_GROUP_DOKU_WALLET = 2;
  const OPC_GROUP_ATM_TRANSFER_VA = 3;
  const OPC_GROUP_VA_PERMATA = 3;
  const OPC_GROUP_SINARMAS = 4;
  const OPC_GROUP_ALFA = 5;
  const OPC_GROUP_MANDIRI_CLICKPAY = 6;
  const OPC_GROUP_KREDIVO = 7;
  const OPC_GROUP_CIMB_CLICK = 8;
  const OPC_GROUP_TELKOMSEL_CASH = 9;
  const OPC_GROUP_XL_TUNAI = 10;
  const OPC_GROUP_MANDIRI_BILL_PAYMENT = 11;
  const OPC_GROUP_BBM_MONEY = 12;
  const OPC_GROUP_INDOMARET = 13;
  const OPC_GROUP_INDOSAT_DOMPETKU = 14;
  const OPC_GROUP_MANDIRI_E_CASH = 15;
  const OPC_GROUP_BCA_CLICKPAY = 16;
  const OPC_GROUP_BRI_EPAY = 17;
  const OPC_GROUP_BANK_TRANSFER_MANDIRI = 18;
  const OPC_GROUP_BANK_TRANSFER_BCA = 19;
  const OPC_GROUP_BANK_TRANSFER_BNI = 20;
  const OPC_GROUP_CC_INSTALLMENT = 21;
  const OPC_GROUP_BANK_TRANSFER_BRI = 22;
  const OPC_GROUP_POST_PAID = 23;
  const OPC_GROUP_CC_TOKENIZATION = 24;
  const OPC_GROUP_CASH_ON_DELIVERY = 25;
  const OPC_GROUP_OCASH = 26;
  const OPC_GROUP_MANDIRI_MPT = 27;
  const OPC_GROUP_DANAMON_INTERNET_BANKING = 28;
  const OPC_GROUP_DEBIT_ONLINE_BNI = 29;
  const OPC_GROUP_MUAMALAT_INTERNET_BANKING = 30;
  const OPC_GROUP_VA_BCA = 31;
  const OPC_GROUP_VA_MANDIRI = 32;
  const OPC_GROUP_VA_INDOMARET = 33;
  const OPC_GROUP_VA_BRI = 34;
  const OPC_GROUP_AKULAKU = 35;
  const OPC_GROUP_VA_BNI = 36;
  const OPC_GROUP_BANK_TRANSFER_CIMB = 37;
  const OPC_GROUP_VA_CIMB = 38;
  const OPC_GROUP_DEBIT_CREDIT_CARD_PRESENT_PAX_BNI_SWITCHING = 39;
  const OPC_GROUP_BANK_TRANSFER_PERMATA = 40;
  const OPC_GROUP_VA_ARTAJASA = 41;
  const OPC_GROUP_DEBIT_CREDIT_CARD_PRESENT_PAX_ARTAJASA_SWITCHING = 42;
  const OPC_GROUP_VA_PERMATA_DIRECT = 43;

  const CREATED = 10000;
  const OPENED = 10001;
  const CONFIRMED = 20001;
  const COMPLETED = 50000;
  const CANCELLED = 90000;

  const PAYMENT_METHOD_GROUP_EMONEY = 1;
  const PAYMENT_METHOD_GROUP_BANK_TRANSFER = 2;
  const PAYMENT_METHOD_GROUP_INTERNET_BANKING = 3;
  const PAYMENT_METHOD_GROUP_ATM_TRANSFER = 4;
  const PAYMENT_METHOD_GROUP_CREDIT_CARD = 5;
  const PAYMENT_METHOD_GROUP_DEBIT_CARD = 6;
  const PAYMENT_METHOD_GROUP_ONLINE_LOAN = 7;
  const PAYMENT_METHOD_GROUP_INSTALLMENT = 8;
  const PAYMENT_METHOD_GROUP_GROCERY_STORE = 9;
  const PAYMENT_METHOD_GROUP_DEBIT_CREDIT_CARD_PRESENT = 10;

  public static function getBankTransferGroup() {
    return [
      self::OPC_GROUP_BANK_TRANSFER_MANDIRI,
      self::OPC_GROUP_BANK_TRANSFER_BCA,
      self::OPC_GROUP_BANK_TRANSFER_BNI,
      self::OPC_GROUP_BANK_TRANSFER_BRI,
      self::OPC_GROUP_BANK_TRANSFER_CIMB,
      self::OPC_GROUP_BANK_TRANSFER_PERMATA
    ];
  }

  public static function getConstKeyByValue($searchVal) {

    $constants = (new \ReflectionClass(__CLASS__))->getConstants();

    foreach ($constants as $name => $value) {
      if (starts_with($name, 'OPC_GROUP_') && $value == $searchVal) {
        return substr($name, 10);
      }
    }
    return null;
  }

  public static function getGatewayName($gatewayId) {
    switch ($gatewayId) {
      case Payment::ANDROID:
        return 'Android';
      case Payment::ANDROID_PLAN:
        return 'Android Plan';
      case Payment::ANDROID_OCASH:
        return 'Android oCash';
      case Payment::ANDROID_WARRANTY:
        return 'Android Warranty';
      case Payment::ANDROID_ODEPOSIT:
        return 'Android oDeposit';
      case Payment::ANDROID_STORE:
        return 'Android Cash on Delivery';
      case Payment::ANDROID_OPAY:
        return 'Android oPay';
      case Payment::ANDROID_MD_PLUS:
        return 'Android MD Plus';
      case Payment::WEB_STORE:
        return 'Web Store';
      case Payment::WEB_APP:
        return 'Web App';
      case Payment::WEB_APP_PLAN:
        return 'Web App Plan';
      case Payment::WEB_APP_OCASH:
        return 'Web App oCash';
      case Payment::WEB_APP_WARRANTY:
        return 'Web App Warranty';
      case Payment::WEB_APP_ODEPOSIT:
        return 'Web App oDeposit';
      case Payment::WEB_APP_STORE:
        return 'Web App Cash on Delivery';
      case Payment::WEB_APP_MD_PLUS:
        return 'Web App MD Plus';
      case Payment::WEB_SWITCHER:
        return "odeo Web";
      case Payment::IOS:
        return "IOS";
      case Payment::IOS_PLAN:
        return 'IOS Plan';
      case Payment::IOS_OCASH:
        return 'IOS oCash';
      case Payment::IOS_WARRANTY:
        return 'IOS Warranty';
      case Payment::IOS_ODEPOSIT:
        return 'IOS oDeposit';
      case Payment::IOS_STORE:
        return 'IOS Cash on Delivery';
      case Payment::IOS_MD_PLUS:
        return 'IOS MD Plus';
      case Payment::TELEGRAM:
        return 'Telegram App';
      case Payment::JABBER:
        return 'Jabber';
      case Payment::AFFILIATE:
        return 'Host to Host';
      case Payment::SMS_CENTER:
        return 'SMS Center';
      case Payment::ADMIN:
        return 'Admin';
      case Payment::ADMIN_OCASH:
        return 'Admin oCash';
      case Payment::AUTO_RECURRING:
        return 'Auto Recurring';
      case Payment::VIRTUAL_ACCOUNT:
        return 'Virtual Account Inquiry';
      case Payment::DEBIT_CREDIT_CARD_PRESENT_BNI_SWITCHING:
        return 'Debit / Credit Card';
      case Payment::SELLER_CENTER:
        return 'Seller Center';
      case Payment::SELLER_CENTER_OCASH:
        return 'Seller Center oCash';
    }
    return null;

  }

  public static function getBankInquiryRepository($opcGroup) {
    switch ($opcGroup) {
      case self::OPC_GROUP_BANK_TRANSFER_BCA:
        return app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository::class);
      case self::OPC_GROUP_BANK_TRANSFER_MANDIRI:
        return app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri\Repository\BankMandiriInquiryRepository::class);
      case self::OPC_GROUP_BANK_TRANSFER_BRI:
        return app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Repository\BankBriInquiryRepository::class);
      case self::OPC_GROUP_BANK_TRANSFER_CIMB:
        return app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository\BankCimbInquiryRepository::class);
      case self::OPC_GROUP_BANK_TRANSFER_BNI:
        return app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bni\Repository\BankBniInquiryRepository::class);
      case self::OPC_GROUP_BANK_TRANSFER_PERMATA:
        return app()->make(BankPermataInquiryRepository::class);
    }
    return null;
  }

  public static function getBankId($opcGroup) {
    switch ($opcGroup) {
      case self::OPC_GROUP_BANK_TRANSFER_BCA:
        return Bank::BCA;
      case self::OPC_GROUP_BANK_TRANSFER_MANDIRI:
        return Bank::MANDIRI;
      case self::OPC_GROUP_BANK_TRANSFER_BRI:
        return Bank::BRI;
      case self::OPC_GROUP_BANK_TRANSFER_BNI:
        return Bank::BNI;
      case self::OPC_GROUP_BANK_TRANSFER_PERMATA:
        return Bank::PERMATA;
    }
    return null;
  }

  public static function getPaymentMethodGroup($opcGroup) {
    switch ($opcGroup) {
      case self::OPC_GROUP_OCASH:
        return [
          'id' => self::PAYMENT_METHOD_GROUP_EMONEY,
          'name' => trans('payment/payment_group.payment_group_' . self::PAYMENT_METHOD_GROUP_EMONEY)
        ];
      case self::OPC_GROUP_MANDIRI_CLICKPAY:
      case self::OPC_GROUP_BRI_EPAY:
      case self::OPC_GROUP_MANDIRI_E_CASH:
      case self::OPC_GROUP_DANAMON_INTERNET_BANKING:
      case self::OPC_GROUP_MUAMALAT_INTERNET_BANKING:
      case self::OPC_GROUP_CIMB_CLICK:
      case self::OPC_GROUP_DOKU_WALLET:
      case self::OPC_GROUP_BCA_CLICKPAY:
        return [
          'id' => self::PAYMENT_METHOD_GROUP_INTERNET_BANKING,
          'name' => trans('payment/payment_group.payment_group_' . self::PAYMENT_METHOD_GROUP_INTERNET_BANKING)
        ];
      case self::OPC_GROUP_DEBIT_ONLINE_BNI:
        return [
          'id' => self::PAYMENT_METHOD_GROUP_DEBIT_CARD,
          'name' => trans('payment/payment_group.payment_group_' . self::PAYMENT_METHOD_GROUP_DEBIT_CARD)
        ];
      case self::OPC_GROUP_CC:
      case self::OPC_GROUP_CC_TOKENIZATION:
        return [
          'id' => self::PAYMENT_METHOD_GROUP_CREDIT_CARD,
          'name' => trans('payment/payment_group.payment_group_' . self::PAYMENT_METHOD_GROUP_CREDIT_CARD)
        ];
      case self::OPC_GROUP_KREDIVO:
      case self::OPC_GROUP_AKULAKU:
      case self::OPC_GROUP_CC_INSTALLMENT:
        return [
          'id' => self::PAYMENT_METHOD_GROUP_INSTALLMENT,
          'name' => trans('payment/payment_group.payment_group_' . self::PAYMENT_METHOD_GROUP_INSTALLMENT)
        ];
      case self::OPC_GROUP_BANK_TRANSFER_BCA:
      case self::OPC_GROUP_BANK_TRANSFER_MANDIRI:
      case self::OPC_GROUP_BANK_TRANSFER_BNI:
      case self::OPC_GROUP_BANK_TRANSFER_BRI:
      case self::OPC_GROUP_BANK_TRANSFER_CIMB:
      case self::OPC_GROUP_BANK_TRANSFER_PERMATA:
        return [
          'id' => self::PAYMENT_METHOD_GROUP_BANK_TRANSFER,
          'name' => trans('payment/payment_group.payment_group_' . self::PAYMENT_METHOD_GROUP_BANK_TRANSFER)
        ];
      case self::OPC_GROUP_ATM_TRANSFER_VA:
//      case self::OPC_GROUP_MANDIRI_MPT:
        return [
          'id' => self::PAYMENT_METHOD_GROUP_ATM_TRANSFER,
          'name' => trans('payment/payment_group.payment_group_' . self::PAYMENT_METHOD_GROUP_ATM_TRANSFER)
        ];
      case self::OPC_GROUP_ALFA:
      case self::OPC_GROUP_INDOMARET:
      case self::OPC_GROUP_MANDIRI_MPT:
        return [
          'id' => self::PAYMENT_METHOD_GROUP_GROCERY_STORE,
          'name' => trans('payment/payment_group.payment_group_' . self::PAYMENT_METHOD_GROUP_GROCERY_STORE)
        ];
      case self::OPC_GROUP_DEBIT_CREDIT_CARD_PRESENT_PAX_BNI_SWITCHING:
      case self::OPC_GROUP_DEBIT_CREDIT_CARD_PRESENT_PAX_ARTAJASA_SWITCHING:
        return [
          'id' => self::PAYMENT_METHOD_GROUP_DEBIT_CREDIT_CARD_PRESENT,
          'name' => trans('payment/payment_group.payment_group_' . self::PAYMENT_METHOD_GROUP_DEBIT_CREDIT_CARD_PRESENT),
        ];
    }
    return null;
  }

}
