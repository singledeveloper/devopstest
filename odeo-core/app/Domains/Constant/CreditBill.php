<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/21/16
 * Time: 9:18 PM
 */

namespace Odeo\Domains\Constant;


class CreditBill {

  const PENDING = 10000;
  const COMPLETED = 50000;
  const CANCELLED = 90000;

}