<?php

namespace Odeo\Domains\Constant;

use Illuminate\Support\Facades\Redis;

class VirtualAccount {

  const BILLER_ODEO = 1;
  const BILLER_QLUSTER = 2;
  const BILLER_ASG_GLC = 3;
  const BILLER_ASG_GLC_FM = 4;
  const BILLER_DD = 5;
  const BILLER_ASG_SCK = 8;

  const DOKU_BCA_PREFIX = '74104';
  const DOKU_MANDIRI_PREFIX = '88899043';
  const DOKU_INDOMARET_PREFIX = '88229';
  const DOKU_BRI_PREFIX = '63213';
  const VA_CIMB_PREFIX = '8999';

  const VENDOR_DOKU_BCA = 'doku_bca';
  const VENDOR_DOKU_MANDIRI = 'doku_mandiri';
  const VENDOR_DOKU_INDOMARET = 'doku_indomaret';
  const VENDOR_DOKU_BRI = 'doku_bri';

  const VENDOR_VA_CIMB = 'va_cimb';

  const VENDOR_BNI_ECOLLECTION = 'bni_ecollection';

  const VENDOR_PRISMALINK_BCA = 'prismalink_bca';
  const VENDOR_DIRECT_ARTAJASA = 'direct_artajasa';
  const VENDOR_DIRECT_CIMB = 'direct_cimb';
  const VENDOR_DIRECT_PERMATA = 'direct_permata';
  const VENDOR_DIRECT_MANDIRI = 'direct_mandiri';
  const VENDOR_DOKU_ALFA = 'doku_alfamart';

  const PARTIAL = 'partial';
  const FIXED = 'fixed';
  const OPEN = 'open';

  const PREFIX_VENDOR_MAP = [
    self::DOKU_BCA_PREFIX => self::VENDOR_DOKU_BCA,
    self::DOKU_MANDIRI_PREFIX => self::VENDOR_DOKU_MANDIRI,
    self::DOKU_INDOMARET_PREFIX => self::VENDOR_DOKU_INDOMARET,
    self::DOKU_BRI_PREFIX => self::VENDOR_DOKU_BRI,
  ];

  public static function getVendorByPrefix($paymentCode) {
    foreach (self::PREFIX_VENDOR_MAP as $prefix => $vendor) {
      if (strpos($paymentCode, $prefix . '') === 0) {
        return $vendor;
      }
    }

    return null;
  }

  public static function generateVirtualAccountNo($prefix, $maxLength, $vaId) {
    $availableLength = $maxLength - strlen($prefix);
    return $prefix . sprintf('%0' . $availableLength . "s", $vaId);
  }

  public static function getBillerNewVaSequence($billerId) {
    $redis = Redis::connection();
    return ($redis->get("odeo_core::biller_{$billerId}_last_va_id") ?? 0) + 1;
  }

  public static function setBillerLastVaSequence($billerId, $vaId) {
    $redis = Redis::connection();
    $redis->set("odeo_core::biller_{$billerId}_last_va_id", $vaId);
  }
}
