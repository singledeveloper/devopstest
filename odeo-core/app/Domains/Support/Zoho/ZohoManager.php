<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/08/19
 * Time: 18.41
 */

namespace Odeo\Domains\Support\Zoho;


use Odeo\Domains\Support\Zoho\Helper\ZohoApi;
use Odeo\Domains\Support\Zoho\Repository\ZohoMailApiFetchRepository;

class ZohoManager {

  /**
   * @var ZohoApi
   */
  protected $zohoApi;
  /**
   * @var ZohoMailApiFetchRepository
   */
  protected $zohoFetchRepo;

  function init() {
    $this->zohoApi = app()->make(ZohoApi::class);
    $this->zohoFetchRepo = app()->make(ZohoMailApiFetchRepository::class);
  }

}