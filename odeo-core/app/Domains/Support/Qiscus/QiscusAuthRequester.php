<?php

namespace Odeo\Domains\Support\Qiscus;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Core\PipelineListener;

class QiscusAuthRequester {

  private $userRepo;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
  }


  public function getJwtToken(PipelineListener $listener, $data) {

    if (!isset($data['nonce'])) {
      return $listener->response(401);
    }

    $user = $this->userRepo->findById($data["auth"]['user_id']);

    if (!$user) {
      return $listener->response(401);
    }

    $timestamp = time();

    $token = (new Builder())
      ->setHeader('ver', 'v2')
      ->setNotBefore($timestamp)
      ->setExpiration($timestamp + 130)
      ->setIssuedAt($timestamp)
      ->setIssuer(env('QISCUS_APP_ID'))
      ->set('prn', (string)$user->id)
      ->set('nce', $data['nonce'])
      ->set('name', $user->name)
      ->sign(new Sha256(), env('QISCUS_SECRET_KEY'))
      ->getToken();

    return $listener->response(200, [
      "token" => (string) $token,
      "has_qiscus_account" => $user->has_qiscus_account
    ]);

  }

}
