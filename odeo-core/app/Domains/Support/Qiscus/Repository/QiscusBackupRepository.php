<?php

namespace Odeo\Domains\Support\Qiscus\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Support\Qiscus\Model\QiscusBackup;

class QiscusBackupRepository extends Repository {

  public function __construct(QiscusBackup $qiscusBackup) {
    $this->model = $qiscusBackup;
  }

  public function getLast() {
    return $this->model->orderBy('end_date', 'desc')->first();
  }

  public function getUnprocessed() {
    return $this->model
      ->where('download_url', null)
      ->orderBy('start_date', 'asc')->get();
  }
}
