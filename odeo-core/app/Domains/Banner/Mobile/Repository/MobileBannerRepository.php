<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/23/17
 * Time: 4:48 PM
 */

namespace Odeo\Domains\Banner\Mobile\Repository;


use Odeo\Domains\Banner\Mobile\Model\MobileBanner;
use Odeo\Domains\Core\Repository;

class MobileBannerRepository extends Repository {


  public function __construct(MobileBanner $mobileBanner) {
    $this->model = $mobileBanner;
  }

  public function getActive() {
    return $this->model
      ->where('active', true)
      ->orderBy('priority', 'asc')
      ->get();
  }


}