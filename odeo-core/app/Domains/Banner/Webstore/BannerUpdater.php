<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/5/17
 * Time: 11:45 PM
 */

namespace Odeo\Domains\Banner\Webstore;

use Odeo\Domains\Core\PipelineListener;

class BannerUpdater {

  private $banners;

  public function __construct() {
    $this->banners = app()->make(\Odeo\Domains\Banner\Webstore\Repository\BannerRepository::class);
  }

  public function activate(PipelineListener $listener, $data) {

    $banner = $this->banners->findById($data['banner_id']);

    $banner->active = !$banner->active;

    $this->banners->save($banner);

    return $listener->response(200);

  }


}