<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 4:55 PM
 */

namespace Odeo\Domains\Core;


interface TransformerListener {

  public function _transforms($item, Repository $repository);

}