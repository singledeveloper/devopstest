<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 26/04/18
 * Time: 17.29
 */

namespace Odeo\Domains\Core;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Redis;

class Scheduler {

  private $redis;
  private $kernelScheduler;

  const REDIS_NAMESPACE = 'shared:scheduler-mutex';

  public function __construct(Schedule $ch) {
    $this->redis = Redis::connection();
    $this->kernelScheduler = $ch;
  }

  public function everyMinute($name, $caller) {
    return $this->processEvent($name, $caller, 1)
      ->everyMinute();
  }

  public function everyFiveMinutes($name, $caller) {
    return $this->processEvent($name, $caller, 5 - 1)
      ->everyFiveMinutes();
  }

  public function everyTenMinutes($name, $caller) {
    return $this->processEvent($name, $caller, 10 - 1)
      ->everyTenMinutes();
  }

  public function everyFiveTeenMinutes($name, $caller) {
    return $this->processEvent($name, $caller, 15 - 1)
      ->cron('*/15 * * * *');
  }

  public function everyThirtyMinutes($name, $caller) {
    return $this->processEvent($name, $caller, 30 - 1)
      ->everyThirtyMinutes();
  }

  public function hourly($name, $caller) {
    return $this->processEvent($name, $caller, 60 - 1
    )->hourly();
  }

  public function everyThreeHour($name, $caller) {
    return $this->processEvent($name, $caller, 180 - 1)
      ->cron('0 */3 * * *');
  }

  public function daily($name, $caller) {
    $this->debugNotCalledScheduler($name, 'begin to run');
    return $this->processEvent($name, $caller, 1440 - 1)
      ->daily();
  }

  public function dailyAt($name, $caller, $time = "00:00") {
    $this->debugNotCalledScheduler($name, 'begin to run');
    return $this->processEvent($name, $caller, 1440 - 1)
      ->dailyAt($time);
  }

  public function monthlyOn($name, $caller, $day = 1, $time = "0:0") {
    return $this->processEvent($name, $caller, 1440 - 1)
      ->monthlyOn($day, $time);
  }

  private function processEvent($name, $caller, $duration) {
    return $this->kernelScheduler->call(function () use ($name, $caller, $duration) {
      if ($this->lock($name, $duration)) {
        try {
          $caller();
        } catch (\Exception $e) {
          if (isProduction()) {
            app('sentry')->captureException($e);
          } else {
            \Log::error($e);
          }
        } finally {
          $this->unlock($name);
        }
      }
    });
  }

  private function isProccessing($name) {
    return $this->redis->hsetnx(self::REDIS_NAMESPACE, $name, 1);
  }

  private function lock($name, $duration) {
    $this->debugNotCalledScheduler($name, 'lock called');
    if (!$this->isProccessing($name)) {
      $this->debugNotCalledScheduler($name, 'is still processing');
      return false;
    }

    $lockProps = $this->getLockProps($name);

    if ($lockProps) {
      $s = explode('#', $lockProps);

      $time = $s[0];
      $serverNickName = $s[1];

      if (!$this->timeIsSatisfy($time, $duration) && !$this->compareEnv($serverNickName)) {
        $this->unlock($name);
        $this->debugNotCalledScheduler($name, 'lockProps error');
        $this->debugNotCalledScheduler($name, $this->timeIsSatisfy($time, $duration));
        $this->debugNotCalledScheduler($name, $this->compareEnv($serverNickName));
        return false;
      }
    }

    $this->setLockProps($name);
    $this->debugNotCalledScheduler($name, 'lock is true');
    return true;
  }

  private function compareEnv($serverNickName) {
    return $serverNickName == env("SERVER_NICKNAME");
  }

  private function timeIsSatisfy($time, $duration) {
    $prev = Carbon::parse($time)->second(0);
    return Carbon::now()->diffInMinutes($prev) >= $duration;
  }

  private function unlock($name) {
    return $this->redis->hdel(self::REDIS_NAMESPACE, $name);
  }

  private function setLockProps($name) {
    $this->redis->hset(self::REDIS_NAMESPACE, $this->getKeyProps($name), $this->populateProps());
  }

  private function getLockProps($name) {
    return $this->redis->hget(self::REDIS_NAMESPACE, $this->getKeyProps($name));
  }

  private function populateProps() {
    return implode('#', [
      date('d-m-Y H:i'),
      env('SERVER_NICKNAME')
    ]);
  }

  private function getKeyProps($name) {
    return $name . ':props';
  }


  //temporary
  private function debugNotCalledScheduler($name, $message) {
    $suspected = [
      'fetch_zoho_bni_edc_settlement'
    ];
    if (!in_array($name, $suspected)) {
      return;
    }

    clog('debug_scheduler', "$name: $message");
  }
}
