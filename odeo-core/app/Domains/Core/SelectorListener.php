<?php

namespace Odeo\Domains\Core;

interface SelectorListener extends TransformerListener {

    public function _extends($data, Repository $repository);

}