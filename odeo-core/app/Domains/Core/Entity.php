<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/26/16
 * Time: 1:20 PM
 */

namespace Odeo\Domains\Core;


use Illuminate\Database\Eloquent\Model;

abstract class Entity extends Model {

  public static function table() {
    $instance = new static;
    return $instance->getTable();
  }
}
