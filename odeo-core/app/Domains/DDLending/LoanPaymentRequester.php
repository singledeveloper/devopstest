<?php

namespace Odeo\Domains\DDLending;

use Carbon\Carbon;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\LoanStatus;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\DDLending\Jobs\NotifyLoan;
use Odeo\Domains\DDLending\Repository\DDLendingUserRepository;
use Odeo\Domains\DDLending\Repository\LoanRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;

class LoanPaymentRequester {

  private $loanRepo;
  private $cashInserter;
  private $ddlendingUserRepo;

  public function __construct() {
    $this->loanRepo = app()->make(LoanRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
    $this->ddlendingUserRepo = app()->make(DDLendingUserRepository::class);
  }

  public function checkout(PipelineListener $listener, $data) {
    $loan = $this->loanRepo->findById($data['item_id']);
    $loan->payment_order_id = $data['order']['id'];
    $this->loanRepo->save($loan);
    return $listener->response(200);
  }

  public function completeOrder(PipelineListener $listener, $data) {
    $loan = $this->loanRepo->findByPaymentOrderId($data['order_id']);
    if (!$loan) {
      throw new \Exception("there should be a loan with payment_order_id={$data['order_id']}");
    }

    if ($loan->status != LoanStatus::ON_PROGRESS_DISBURSED) {
      throw new \Exception("loan #{$loan->id} should be " . LoanStatus::ON_PROGRESS_DISBURSED . " but got {$loan->status} instead");
    }

    $ddlendingUser = $this->ddlendingUserRepo->findByUserId($loan->user_id);
    if (!$ddlendingUser) {
      throw new \Exception("there should be ddlending user with user_id {$loan->user_id}");
    }

    $loan->paid_at = Carbon::now();
    $loan->status = LoanStatus::COMPLETED;
    $this->loanRepo->save($loan);

    $this->cashInserter->add([
      'user_id' => $loan->user_id,
      'trx_type' => TransactionType::DDLENDING_LOAN_PAYMENT,
      'cash_type' => CashType::OCASH,
      'amount' => $loan->payment_amount,
      'data' => json_encode([
        'order_id' => $data['order_id'],
        'loan_id' => $loan->id,
      ]),
    ]);

    $this->cashInserter->add([
      'user_id' => $loan->user_id,
      'trx_type' => TransactionType::DDLENDING_LOAN_PAYMENT_FEE,
      'cash_type' => CashType::OCASH,
      'amount' => -$loan->payment_fee,
      'data' => json_encode([
        'order_id' => $data['order_id'],
        'loan_id' => $loan->id,
      ]),
    ]);

    $this->cashInserter->run();

    $listener->pushQueue(new NotifyLoan($loan->id));

    return $listener->response(200);
  }
}