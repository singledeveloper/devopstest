<?php


namespace Odeo\Domains\DDLending\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\DDLending\Model\LoanNotifyLog;

class LoanNotifyLogRepository extends Repository {

  public function __construct(LoanNotifyLog $log) {
    $this->model = $log;
  }

}