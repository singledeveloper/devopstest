<?php

namespace Odeo\Domains\Notification\Helper;

use Odeo\Domains\Constant\NotificationType;

class InternalNoticer {

  private $notificationInternalRepo;

  public function __construct() {
    $this->notificationInternalRepo = app()->make(\Odeo\Domains\Notification\Repository\NotificationInternalRepository::class);
  }

  public function saveMessage($message, $messageType = NotificationType::NOTICE_REMINDER, $referenceId = null, $isMultiplier = false) {
    $notification = false;
    if ($isMultiplier) {
      $notification = $this->notificationInternalRepo->findByMessageType($messageType, $referenceId);
    }

    if (!$notification) {
      $notification = $this->notificationInternalRepo->getNew();
      $notification->message = $message;
      $notification->message_type = $messageType;
      $notification->multiplier = 1;
      $notification->reference_id = $referenceId;
    }
    else {
      $notification->multiplier = $notification->multiplier + 1;
    }

    $this->notificationInternalRepo->save($notification);
  }

}
