<?php

namespace Odeo\Domains\Notification\Jobs;

use Odeo\Domains\Constant\NotificationType;
use Odeo\Jobs\Job;

class SaveInternalNotice extends Job {

  protected $messages, $type, $referenceId, $isMultiplier;

  public function __construct($messages, $type = NotificationType::NOTICE_REMINDER, $referenceId = null, $isMultiplier = false) {
    parent::__construct();
    $this->messages = $messages;
    $this->type = $type;
    $this->referenceId = $referenceId;
    $this->isMultiplier = $isMultiplier;
  }

  public function handle() {
    $internalNoticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
    $internalNoticer->saveMessage($this->messages, $this->type, $this->referenceId, $this->isMultiplier);
  }
}
