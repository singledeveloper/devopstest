<?php

namespace Odeo\Domains\Notification;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\NotificationType;

class NotificationUpdater {

  public function __construct() {
    $this->notification = app()->make(\Odeo\Domains\Notification\Repository\NotificationRepository::class);
  }
  
  public function toClaimed(PipelineListener $listener, $data) {
    if ($notification = $this->notification->findLikeClaim($data["store_id"])) {
      if ($desc = json_decode($notification->description, true)) {
        $new_desc = [];
        foreach ($desc as $item) {
          if ($item["type"] == "button") {
            unset($item["layer"]);
            unset($item["id"]);
            $item["text"] = "Claimed";
            $notification->type = NotificationType::CHECK;
          }
          $new_desc[] = $item;
        }
        $notification->description = json_encode($new_desc);
        $notification->data = "";
        $this->notification->save($notification);
      }
    }
    
    return $listener->response(200);
  }

  public function readInternal(PipelineListener $listener, $data) {
    $notificationInternalRepo = app()->make(\Odeo\Domains\Notification\Repository\NotificationInternalRepository::class);
    if ($notification = $notificationInternalRepo->findById($data['notification_id'])) {
      $notification->is_read = true;
      $notification->read_by_user_id = $data['auth']['user_id'];
      $notificationInternalRepo->save($notification);
    }
    return $listener->response(200);
  }

  public function saveBalanceNotification(PipelineListener $listener, $data) {
    foreach (explode(';', $data['email']) as $item) {
      if (!filter_var(trim($item), FILTER_VALIDATE_EMAIL)) {
        return $listener->response(400, 'Your email format is not valid');
      }
    }
    $notificationBalanceRepo = app()->make(\Odeo\Domains\Notification\Repository\NotificationBalanceRepository::class);
    if (!$notification = $notificationBalanceRepo->findByUserId($data['auth']['user_id'])) {
      $notification = $notificationBalanceRepo->getNew();
      $notification->user_id = $data['auth']['user_id'];
    }

    $notification->amount = intval(str_replace(['.', ','], '', $data['amount']));
    $notification->email = str_replace(' ', '', strtolower($data['email']));

    $notificationBalanceRepo->save($notification);

    return $listener->response(200);
  }
}
