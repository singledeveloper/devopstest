<?php

namespace Odeo\Domains\Notification;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;

class AnnouncementSelector implements SelectorListener {

  private $user;

  public function __construct() {
    $this->announcements = app()->make(\Odeo\Domains\Notification\Repository\AnnouncementRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $announcements = [];

    $announcements['id'] = $item->id;
    $announcements['title'] = $item->title;
    $announcements['content'] = $item->content;
    $announcements['start_date'] = $item->start_date;
    $announcements['end_date'] = $item->end_date;
    $announcements['is_active'] = $item->is_active;
    $announcements['priority'] = $item->priority;

    return $announcements;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    $this->announcements->normalizeFilters($data);

    $announcements = $this->announcements->get();

    if (sizeof($announcements) > 0)
    {
      $announcements = $announcements->map(function($item) {
        return $this->_transforms($item, $this->announcements);
      });

      return $listener->response(200, ["announcements" => $announcements]);
    }

    return $listener->response(204, ["announcements" => []]);
  }

  public function getActive(PipelineListener $listener, $data) {
    $announcements = $this->announcements->getActive();

    if (sizeof($announcements) > 0)
      return $listener->response(200, ["announcements" => $announcements]);

    return $listener->response(204, ["announcements" => []]);
  }

}
