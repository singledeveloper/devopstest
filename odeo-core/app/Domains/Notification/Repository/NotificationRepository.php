<?php

namespace Odeo\Domains\Notification\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Notification\Model\Notification;

class NotificationRepository extends Repository {

  public function __construct(Notification $notification) {
    $this->model = $notification;
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->model
      ->select('user_id', 'type', 'group', 'title', 'description', 'data', 'created_at')
      ->where('user_id', NULL)->orWhere('user_id', $filters["auth"]["user_id"])->orderBy("id", "DESC");

    return $this->getResult($query);
  }

  public function getByGroup($group) {
    $filters = $this->getFilters();
    $query = $this->model
      ->select('user_id', 'type', 'group', 'title', 'description', 'data', 'created_at')
      ->where('user_id', $filters["auth"]["user_id"])->where('group', $group)
      ->orderBy("id", "DESC");

    return $this->getResult($query);
  }

  public function getNumberOfUnread($userId, $lastReadDate) {
    return $this->model->where('user_id', $userId)->where('created_at', '>', $lastReadDate)->count();
  }

  public function findLike($key, $item) {
    return $this->model->where("data", "like", '%"' . $key . '":"' . $item . '"%')->get();
  }

  public function findLikeAndDelete($key, $item) {
    return $this->model->where("data", "like", '%"' . $key . '":"' . $item . '"%')->delete();
  }

  public function findLikeClaim($storeId) {
    return $this->model->where("data", "like", '%"claim_referral":"' . $storeId . '"%')->orderBy("id", "ASC")->first();
  }
}
