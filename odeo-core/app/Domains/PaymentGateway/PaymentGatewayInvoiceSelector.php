<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 18/10/19
 * Time: 11.41
 */

namespace Odeo\Domains\PaymentGateway;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Helper\PaymentInvoiceHelper;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayInvoicePaymentRepository;

class PaymentGatewayInvoiceSelector {

  function __construct() {
    $this->pgInvoicePayment = app()->make(PaymentGatewayInvoicePaymentRepository::class);
    $this->redis = Redis::connection();
  }

  public function getDetailByVaNo(PipelineListener $listener, $data) {
    $invoice = $this->pgInvoicePayment->findByVaNo($data['vendor_code'], $data['virtual_account_number']);
    return $this->buildResponse($listener, $data, $invoice);
  }

  public function getDetailByReference(PipelineListener $listener, $data) {
    $invoice = $this->pgInvoicePayment->findByReference($data['vendor_code'], $data['reference']);
    return $this->buildResponse($listener, $data, $invoice);
  }

  public function getDetailByVaNoAndReference(PipelineListener $listener, $data) {
    $invoice = $this->pgInvoicePayment->findByVaNoAndReference($data['vendor_code'], $data['virtual_account_number'], $data['reference']);
    return $this->buildResponse($listener, $data, $invoice);
  }

  public function getPaidTransactionCount(PipelineListener $listener, $data) {
    $count = $count = $this->pgInvoicePayment->getPaidTransactionCountByVendor(VirtualAccount::VENDOR_PRISMALINK_BCA, $data['start_date'], $data['end_date']);
    return $listener->response(200, ['va_invoice_payment_count' => $count]);
  }

  private function buildResponse(PipelineListener $listener, $data, $invoice) {

    if (!$invoice) {
      return $listener->response(400, ['error_message' => 'Invoice not found', 'error_code' => PaymentInvoiceHelper::INVOICE_NOT_FOUND]);
    }

    if ($invoice->amount != $data['amount']) {
      return $listener->response(400, ['error_message' => 'Invalid amount', 'error_code' => PaymentInvoiceHelper::INVOICE_INVALID_AMOUNT]);
    }

    if ($invoice->paid_at != null || $invoice->paid_at != '') {
      return $listener->response(400, ['error_message' => 'Invoice already paid', 'error_code' => PaymentInvoiceHelper::INVOICE_ALREADY_PAID]);
    }

    $namespace = $this->getRedisNamespace($data['vendor_code'], $invoice->virtual_account_number);
    if ($this->redis->get($namespace)) {
      return $listener->response(400, ['error_message' => 'Invoice already paid', 'error_code' => PaymentInvoiceHelper::INVOICE_ALREADY_PAID]);
    }

    return $listener->response(200, ['order_id' => $invoice->order_id]);
  }

  private function getRedisNamespace($vendor, $vaNo) {
    return "odeo_core:invoice_payment_{$vendor}_{$vaNo}";
  }
}