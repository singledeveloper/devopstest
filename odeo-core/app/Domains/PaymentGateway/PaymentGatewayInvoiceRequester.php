<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/10/19
 * Time: 19.00
 */

namespace Odeo\Domains\PaymentGateway;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceUserRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayInvoicePaymentRepository;

class PaymentGatewayInvoiceRequester {

  function __construct() {
    $this->invoiceUsers = app()->make(AffiliateUserInvoiceUserRepository::class);
    $this->pgInvoicePayments = app()->make(PaymentGatewayInvoicePaymentRepository::class);
    $this->payments = app()->make(PaymentRepository::class);
  }

  public function request(PipelineListener $listener, $data) {
    $invoiceUser = $this->invoiceUsers->findUserByCreatedBy($data['biller_id'], $data['user_id'], $data['auth']['user_id']);
    $customerName =
      $invoiceUser && $invoiceUser->name != '' && $invoiceUser->name != null
      ? $invoiceUser->name : $data['customer_name'];

    $invoicePayment = $this->pgInvoicePayments->getNew();

    $invoicePayment->virtual_account_number = $data['virtual_account_number'];
    $invoicePayment->customer_name = $customerName;
    $invoicePayment->description = $data['item_name'] ?? $data['customer_name'];
    $invoicePayment->reference = $data['reference'];
    $invoicePayment->amount = $data['total'];
    $invoicePayment->vendor = $data['vendor_code'];
    $invoicePayment->order_id = $data['order_id'];
    $invoicePayment->cost = $data['cost'];
    $invoicePayment->invoice_id = $data['invoice_id'];
    $invoicePayment->settlement_at = null;
    $invoicePayment->settlement_status = null;

    $this->pgInvoicePayments->save($invoicePayment);

    $payment = $this->payments->findByOrderId($data['order_id']);
    $payment->reference_id = $invoicePayment->id;
    $this->payments->save($payment);

    return $listener->response(200, [
      'order_id' => $data['order_id'],
      'virtual_account_number' => $data['virtual_account_number'],
      'customer_name' => substr(trim($invoicePayment->customer_name), 0, 30),
      'description' => $invoicePayment->description,
      'item_name' => $invoicePayment->description,
      'reference' => $invoicePayment->reference,
      'amount' => $invoicePayment->amount
    ]);
  }

}