<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 12/12/19
 * Time: 20.52
 */

namespace Odeo\Domains\PaymentGateway\Jobs;


use Carbon\Carbon;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;
use Odeo\Jobs\Job;

class NotifyPaymentGatewayPaymentStatus extends Job {

  private $vaPayment;
  private $vendorReference;
  private $vendorName;

  public function __construct($vaPayment, $vendorReference, $vendorName) {
    parent::__construct();
    $this->vaPayment = $vaPayment;
    $this->vendorReference = $vendorReference;
    $this->vendorName = $vendorName;
  }

  public function handle() {
    $this->vaPayment->status = PaymentGateway::SUCCESS;
    $this->vaPayment->save();

    $pipeline = new Pipeline();
    $pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyPaymentStatus'));
    $pipeline->execute([
      'pg_payment_id' => $this->vaPayment->payment->source_id,
      'pg_payment_status' => PaymentGateway::SUCCESS,
      'receive_notify_time' => Carbon::now(),
      'vendor_reference_id' => $this->vendorReference
    ]);

    if ($pipeline->fail()) {
      clog($this->vendorName, 'ID: ' . $this->vaPayment->id . ' Fail Notify: ' . $pipeline->errorMessage);
    }
  }

}