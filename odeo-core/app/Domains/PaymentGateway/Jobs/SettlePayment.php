<?php

namespace Odeo\Domains\PaymentGateway\Jobs;


use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Constant\Settlement;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;
use Odeo\Jobs\Job;

class SettlePayment extends Job implements ShouldQueue {

  private $pgPaymentId, $redis;

  public function __construct($pgPaymentId) {
    parent::__construct();
    $this->pgPaymentId = $pgPaymentId;
    $this->redis = Redis::connection();
  }

  public function handle() {
    try {
      $key = 'odeo_core:pg_settlement_' . $this->pgPaymentId;
      if (!$this->redis->setnx($key, 1)) {
        throw new \Exception('Duplicate settlement call');
      };
      $this->redis->expire($key, 10 * 60);

      $paymentGatewayPaymentRepo = app()->make(PaymentGatewayPaymentRepository::class);
      $cashInserter = app()->make(CashInserter::class);

      $pgInfoRepo = app()->make(PaymentOdeoPaymentChannelInformationRepository::class);
      
      $pgPayment = $paymentGatewayPaymentRepo->findById($this->pgPaymentId);
      $description = '';
      if ($pgInfo = $pgInfoRepo->findById($pgPayment->payment_group_id))
        $description = $pgInfo->name;

      $description .= $pgPayment->external_reference_id ? (' / ' . $pgPayment->external_reference_id) : '';

      $this->validateHasBeenSettled($pgPayment);

      $pgPayment->settlement_status = Settlement::ON_PROGRESS;
      $paymentGatewayPaymentRepo->save($pgPayment);

      $cashInserter->clear();

      $base = [
        'user_id' => $pgPayment->pgUser->user_id,
        'cash_type' => CashType::OCASH,
        'reference_id' => $pgPayment->id,
        'reference_type' => TransactionType::REF_PAYMENT_GATEWAY,
        'data' => json_encode([
          'description' => $description
        ])
      ];

      $cashInserter->add(array_merge($base, [
        'trx_type' => TransactionType::PAYMENT_GATEWAY_PAYMENT,
        'amount' => $pgPayment->amount,
        'transaction_key' => 'paymentgateway:settlement_payment:' . $pgPayment->id
      ]));

      $cashInserter->add(array_merge($base, [
        'trx_type' => TransactionType::PAYMENT_GATEWAY_FEE,
        'amount' => -$pgPayment->fee,
        'transaction_key' => 'paymentgateway:settlement_payment:' . $pgPayment->id . ':fee'
      ]));

      if ($pgPayment->settlement_fee > 0) {
        $cashInserter->add(array_merge($base, [
          'trx_type' => TransactionType::PAYMENT_GATEWAY_REALTIME_SETTLEMENT_FEE,
          'amount' => -$pgPayment->settlement_fee,
          'transaction_key' => 'paymentgateway:settlement_payment:' . $pgPayment->id . ':settlement_fee'
        ]));
      }

      $cashInserter->run();

      $pgPayment->settlement_status = Settlement::SUCCESS;
      $paymentGatewayPaymentRepo->save($pgPayment);
    } catch (\Exception $e) {
      $this->redis->del($key);
      clog('pg_settlement', $this->$pgPaymentId . ": " . $e->getMessage());
    }
  }

  private function validateHasBeenSettled($pgPayment) {
    if ($pgPayment->status != PaymentGateway::SUCCESS) {
      throw new \Exception('Called settle but status is not success');
    }

    if (Carbon::now()->lt($pgPayment->settlement_at)) {
      throw new \Exception('Called settle but settlementAt is still before the date');
    }

    if ($pgPayment->settlement_status != Settlement::ON_HOLD) {
      throw new \Exception('Called settle but settlementStatus is not on hold');
    }
  }

}
