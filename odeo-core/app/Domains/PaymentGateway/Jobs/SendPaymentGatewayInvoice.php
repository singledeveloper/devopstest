<?php

namespace Odeo\Domains\PaymentGateway\Jobs;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\DataExporterType;
use Odeo\Domains\Constant\DataExporterView;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementInquiryRepository;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Exporter\Helper\DataExporter;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserRepository;
use Odeo\Jobs\Job;

class SendPaymentGatewayInvoice extends Job implements ShouldQueue {

  private $userId, $period, $exportId;

  public function __construct($userId, $period, $exportId = '0') {
    parent::__construct();
    $this->userId = $userId;
    $this->period = $period;
    $this->exportId = $exportId;
  }

  public function handle() {
    $paymentGatewayPaymentRepo = app()->make(PaymentGatewayPaymentRepository::class);
    $userRepo = app()->make(UserRepository::class);
    $pgUserRepo = app()->make(PaymentGatewayUserRepository::class);
    $dataExporter = app()->make(DataExporter::class);
    $exportQueueManager = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class);

    $user = $userRepo->findById($this->userId);
    $pgUser = $pgUserRepo->findByUserId($user->id);

    $rows = $paymentGatewayPaymentRepo
      ->getInvoiceRows($pgUser->id, $this->period)
      ->map(function ($row) {
        return $this->toRow($row);
      })
      ->toBase();

    $data = [
      'invoice_no' => 'INV/PG/' . $this->userId . '/' . str_replace('-', '/', $this->period),
      'period' => $this->period,
      'user_id' => $user->id,
      'user_name' => $user->name,
      'rows' => $rows,
      'total' => number_format($rows->sum('amount')),
    ];

    $file = $dataExporter->generate($data, DataExporterType::PDF, DataExporterView::PAYMENT_GATEWAY_INVOICE_KEY);
    $fileName = 'Tagihan Payment Gateway - ' . $this->period . '.pdf';
    $exportQueueManager->update($this->exportId, $file, $fileName);

    Mail::send('emails.payment-gateway-invoice', [
      'data' => $data
    ], function ($m) use ($file, $user, $fileName) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->bcc('pg@odeo.co.id');
      $m->attachData($file, $fileName);
      $m->to($user->email)->subject('Tagihan Payment Gateway - ' . $this->period);
    });
  }

  private function toRow($item) {
    return [
      'name' => $item->name,
      'period' => $this->period,
      'quantity' => number_format($item->quantity),
      'price' => number_format($item->price),
      'formatted_amount' => number_format($item->amount),
      'amount' => $item->amount,
    ];
  }

}
