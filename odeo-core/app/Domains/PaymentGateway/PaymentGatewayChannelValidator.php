<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 29/10/19
 * Time: 14.38
 */

namespace Odeo\Domains\PaymentGateway;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\VirtualAccount\Repository\VirtualAccountVendorRepository;

class PaymentGatewayChannelValidator {

  function __construct() {
    $this->vaVendor = app()->make(VirtualAccountVendorRepository::class);
  }

  public function validatePaymentVendor(PipelineListener $listener, $data) {
    if ($vendor = $this->vaVendor->findByPrefix($data['virtual_account_number'])) {
      if ($vendor->code == $data['vendor_code']) {
        return $listener->response(200);
      }
    }
    return $listener->response(400);
  }

  public function validateDokuVendor(PipelineListener $listener, $data) {
    if ($vendor = $this->vaVendor->findDokuPrefix($data['virtual_account_number'])) {
      return $listener->response(200);
    }
    return $listener->response(400);
  }

}