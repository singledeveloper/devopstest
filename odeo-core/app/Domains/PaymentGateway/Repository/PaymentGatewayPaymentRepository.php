<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 27/11/18
 * Time: 19.47
 */

namespace Odeo\Domains\PaymentGateway\Repository;

use Carbon\Carbon;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Constant\Settlement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayPayment;

class PaymentGatewayPaymentRepository extends Repository {

  public function __construct(PaymentGatewayPayment $pgPayment) {
    $this->model = $pgPayment;
  }

  public function getAllPaymentByUser($userId) {
    $filter = $this->getFilters();

    $query = $this->model
      ->join('payment_gateway_users', 'payment_gateway_payments.pg_user_id', '=', 'payment_gateway_users.id')
      ->join('payments', 'payments.source_id', '=', 'payment_gateway_payments.id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
      ->where('user_id', $userId)
      ->select([
        'payment_gateway_payments.id',
        'payment_odeo_payment_channel_informations.name as payment_method',
        'payment_gateway_payments.amount',
        'payment_gateway_payments.fee',
        'payment_gateway_payments.settlement_fee',
        'payment_gateway_payments.external_reference_id',
        'payment_gateway_payments.settlement_at',
        'payment_gateway_payments.status',
        'payment_gateway_payments.settlement_status',
        'payment_gateway_payments.va_code',
        'payment_gateway_payments.created_at',
      ]);

    if (isset($filter['id']) && $filter['id']) {
      $query->where('payment_gateway_payments.id', '=', $filter['id']);
    }

    if (isset($filter['va_code']) && $filter['va_code']) {
      $query->where('payment_gateway_payments.va_code', '=', $filter['va_code']);
    }

    if (isset($filter['external_reference_id']) && $filter['external_reference_id']) {
      $query->where('external_reference_id', '=', $filter['external_reference_id']);
    }

    if (isset($filter['status']) && $filter['status']) {
      $query->whereIn('payment_gateway_payments.status', PaymentGateway::MERCHANT_MESSAGE_TO_STATUS[$filter['status']]);
    }

    if (isset($filter['start_date']) && $filter['start_date'] && isset($filter['end_date']) && $filter['end_date']) {
      $query->whereBetween('payment_gateway_payments.created_at', [Carbon::parse($filter['start_date'])->startOfDay()->toDateTimeString(), Carbon::parse($filter['end_date'])->endOfDay()->toDateTimeString()]);
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function get() {
    $filter = $this->getFilters();

    $query = $this->model
      ->join('payment_gateway_users', 'payment_gateway_payments.pg_user_id', '=', 'payment_gateway_users.id')
      ->join("users", 'users.id', '=', 'payment_gateway_users.user_id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payment_gateway_payments.payment_group_id')
      ->join('payments', 'payments.source_id', '=', 'payment_gateway_payments.id')
      ->select([
        'user_id',
        'users.name as user_name',
        'payment_gateway_payments.id',
        'payment_gateway_payments.amount',
        'payment_gateway_payments.fee',
        'payment_gateway_payments.settlement_fee',
        'payment_gateway_payments.external_reference_id',
        'payment_gateway_payments.settlement_at',
        'payment_gateway_payments.status',
        'payment_gateway_payments.settlement_status',
        'payment_odeo_payment_channel_informations.name as payment_method',
        'vendor_id',
        'vendor_type',
        'payment_gateway_payments.created_at',
      ]);

    if (isset($filter['search'])) {
      $search = $filter['search'];

      if (isset($search['id'])) {
        $query->where('payment_gateway_payments.id', $search['id']);
      }

      if (isset($search['user_id'])) {
        $query->where('payment_gateway_users.user_id', $search['user_id']);
      }

      if (isset($search['status'])) {
        $query->where('payment_gateway_payments.status', $search['status']);
      }

      if (isset($search['external_reference_id'])) {
        $query->where('payment_gateway_payments.external_reference_id', $search['external_reference_id']);
      }

      if (isset($search['payment_group_id'])) {
        $query->where('payment_odeo_payment_channel_informations.id', $search['payment_group_id']);
      }

      if (isset($search['vendor_type'])) {
        $query->where('payments.vendor_type', $search['vendor_type']);
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function getInvoiceRows($pgUserId, $period) {
    return $this->getCloneModel()
      ->select(\DB::raw('name, sum(fee) as amount, count(*) as quantity, fee as price'))
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payment_gateway_payments.payment_group_id')
      ->where('status', PaymentGateway::SUCCESS)
      ->where('pg_user_id', $pgUserId)
      ->where(\DB::raw("to_char(payment_gateway_payments.created_at, 'YYYY-MM')"), $period)
      ->groupBy(['fee', 'name'])
      ->get();
  }

  public function findByIdAndPaymentGatewayUserId($id, $paymentGatewayUserId) {
    return $this->model
      ->where('id', $id)
      ->where('pg_user_id', $paymentGatewayUserId)
      ->first();
  }

  public function findByPaymentGatewayUserIdAndExternalReferenceId($paymentGatewayUserId, $externalReferenceId) {
    return $this->model
      ->where('pg_user_id', $paymentGatewayUserId)
      ->where('external_reference_id', $externalReferenceId)
      ->where('status', '<>', PaymentGateway::FAILED_PAYMENT_EXTERNAL_REFERENCE_ID_IS_DUPLICATE)
      ->first();
  }

  public function existsByPaymentGatewayUserIdAndExternalReferenceId($paymentGatewayUserId, $externalReferenceId) {
    return $this->model
      ->where('pg_user_id', $paymentGatewayUserId)
      ->where('external_reference_id', $externalReferenceId)
      ->first();
  }

  public function changeSettlementStatus($id, $settlementStatus) {
    $this->model
      ->where('id', '=', $id)
      ->update([
        'settlement_status' => $settlementStatus,
        'updated_at' => Carbon::now()
      ]);
  }

  public function getUnsettledPayment() {
    return $this->model
      ->where('status', PaymentGateway::SUCCESS)
      ->where('settlement_at', '<=', Carbon::now())
      ->where('settlement_status', Settlement::ON_HOLD)
      ->get();
  }

  public function getReport() {
    $filters = $this->getFilters();

    $query = $this->model
      ->join('payment_gateway_users', 'payment_gateway_payments.pg_user_id', '=', 'payment_gateway_users.id')
      ->join("users", 'users.id', '=', 'payment_gateway_users.user_id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payment_gateway_payments.payment_group_id')
      ->join('payments', 'payments.source_id', '=', 'payment_gateway_payments.id')
      ->leftJoin('payment_prismalink_va_payments', function ($join) {
        $join->on('payment_prismalink_va_payments.id', '=', 'payments.vendor_id');
        $join->where('payments.vendor_type', '=', 'va_prismalink');
      })
      ->select(\DB::raw('
        user_id,
        users.name as user_name,
        payment_gateway_payments.id,
        payment_gateway_payments.amount as payment_amount,
        payment_gateway_payments.fee,
        payment_gateway_payments.settlement_fee,
        payment_gateway_payments.amount - payment_gateway_payments.fee - payment_gateway_payments.fee as settlement_amount,
        payment_gateway_payments.external_reference_id as merchant_reference_id,
        payment_gateway_payments.created_at as payment_date,
        payment_gateway_payments.status,
        payment_odeo_payment_channel_informations.name as payment_method,
        vendor_type as vendor,
        payment_prismalink_va_payments.trace_number as va_prismalink_reference_id
      '));

    if (isset($filters['start_date'])) $query->where('payment_gateway_payments.created_at', '>=', $filters['start_date']);
    if (isset($filters['end_date'])) $query->where('payment_gateway_payments.created_at', '<=', $filters['end_date']);

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function getSumByMonth($date) {
    $query = "
      select
        to_char(pgp.created_at, 'YYYY-MM')                                    as period, 
        count(*)                                                              as trx, 
        sum(pgp.amount)                                                       as gmv, 
        sum(pgp.fee + pgp.settlement_fee - pgp.cost)                          as revenue
      from payment_gateway_payments pgp
      join payments p ON p.source_id = pgp.id
      where 
        pgp.status = :status 
        and date(pgp.created_at) >= :start_date 
        and date(pgp.created_at) <= :end_date
      group by period";

    return \DB::select(\DB::raw($query), [
      'status' => PaymentGateway::SUCCESS,
      'start_date' => $date->copy()->subYear()->format('Y-m-d'),
      'end_date' => $date->format('Y-m-d'),
    ]);
  }

  public function getSuccessfulPayment($date) {
    return $this->getCloneModel()
      ->select(\DB::raw("
        actual_name                                                                                 as vendor_name, 
        sum(payment_gateway_payments.amount)                                                        as gmv, 
        sum(payment_gateway_payments.fee + payment_gateway_payments.settlement_fee - 
          case payments.vendor_type 
          when 'va_prismalink' then payment_prismalink_va_payments.cost 
          when 'va_doku' then payment_doku_va_payments.cost 
          when 'va_artajasa' then payment_artajasa_va_payments.cost 
          when 'va_permata' then payment_permata_va_payments.cost 
          else 0 end
        )                                                                                           as profit,
        count(*)                                                                                    as total"
      ))
      ->join('payments', 'payments.source_id', '=', 'payment_gateway_payments.id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payment_gateway_payments.payment_group_id')
      ->leftJoin('payment_prismalink_va_payments', 'payment_prismalink_va_payments.id', '=', 'payments.vendor_id')
      ->leftJoin('payment_doku_va_payments', 'payment_doku_va_payments.id', '=', 'payments.vendor_id')
      ->leftJoin('payment_artajasa_va_payments', 'payment_artajasa_va_payments.id', '=', 'payments.vendor_id')
      ->leftJoin('payment_permata_va_payments', 'payment_permata_va_payments.id', '=', 'payments.vendor_id')
      ->whereDate('payment_gateway_payments.created_at', '=', $date)
      ->where('payment_gateway_payments.status', PaymentGateway::SUCCESS)
      ->orderBy(\DB::raw('gmv'), 'desc')
      ->groupBy('actual_name')->get();
  }

  public function getTopUsers($date) {
    return $this->getCloneModel()
      ->select(\DB::raw('
        users.name, 
        count(*)                              as transaction_total, 
        sum(amount)                           as gross_amount,
        count(*) * 100 / sum(count(*)) over() as percentage'
      ))
      ->join('payment_gateway_users', 'payment_gateway_users.id', '=', 'payment_gateway_payments.pg_user_id')
      ->join('users', 'users.id', '=', 'payment_gateway_users.user_id')
      ->whereDate('payment_gateway_payments.created_at', '=', $date)
      ->where('payment_gateway_payments.status', PaymentGateway::SUCCESS)
      ->orderBy(\DB::raw('transaction_total'), 'desc')
      ->limit(10)
      ->groupBy('users.name')->get();
  }

  public function getLastDatePaymentGroup() {
    return $this->getCloneModel()
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payment_gateway_payments.payment_group_id')
      ->where('payment_gateway_payments.status', PaymentGateway::SUCCESS)
      ->select(
        \DB::raw('max(payment_gateway_payments.created_at) as last_created_at'),
        'payment_group_id',
        'payment_odeo_payment_channel_informations.name'
      )->groupBy('payment_group_id', 'payment_odeo_payment_channel_informations.name')
      ->get();
  }

  public function getSuspectCount() {
    $query = $this->getCloneModel()
      ->select(\DB::raw("
        count(
          case payments.vendor_type 
            when 'va_prismalink' then payment_prismalink_va_payments.status IN ('30000','50000')
            when 'va_doku' then payment_doku_va_payments.status IN ('30000','50000')
            when 'va_artajasa' then payment_artajasa_va_payments.status IN ('30000','50000')
            when 'va_permata' then payment_permata_va_payments.status IN ('30000','50000')
            else NULL end
        ) as count
      "))
      ->join('payments', 'payments.source_id', '=', 'payment_gateway_payments.id')
      ->leftJoin('payment_prismalink_va_payments', 'payment_prismalink_va_payments.id', '=', 'payments.vendor_id')
      ->leftJoin('payment_doku_va_payments', 'payment_doku_va_payments.id', '=', 'payments.vendor_id')
      ->leftJoin('payment_artajasa_va_payments', 'payment_artajasa_va_payments.id', '=', 'payments.vendor_id')
      ->leftJoin('payment_permata_va_payments', 'payment_permata_va_payments.id', '=', 'payments.vendor_id')
      ->where('payment_gateway_payments.status', '=', PaymentGateway::ACCEPTED)
      ->where('payment_gateway_payments.created_at', '<', Carbon::now()->subMinutes(30)->toDateTimeString())
      ->first();
    
    return isset($query) ? $query->count : 0;
  }

  public function getSuspectTransactions() {
    return $this->getCloneModel()
      ->join('payments', 'payments.source_id', '=', 'payment_gateway_payments.id')
      ->leftJoin('payment_prismalink_va_payments', function($join) {
        $join->on('payment_prismalink_va_payments.id', '=', 'payments.vendor_id')
          ->where('payment_prismalink_va_payments.status', '50000');
      })
      ->leftJoin('payment_doku_va_payments', function($join) {
        $join->on('payment_doku_va_payments.id', '=', 'payments.vendor_id')
          ->where('payment_doku_va_payments.status', '50000');
      })
      ->leftJoin('payment_artajasa_va_payments', function($join) {
        $join->on('payment_artajasa_va_payments.id', '=', 'payments.vendor_id')
          ->where('payment_artajasa_va_payments.status', '50000');
      })
      ->leftJoin('payment_permata_va_payments', function($join) {
        $join->on('payment_permata_va_payments.id', '=', 'payments.vendor_id')
          ->where('payment_permata_va_payments.status', '50000');
      })
      ->where('payment_gateway_payments.status', '=', PaymentGateway::ACCEPTED)
      ->where('payment_gateway_payments.created_at', '<', Carbon::now()->subMinutes(5)->toDateTimeString())
      ->get();
  }
}
