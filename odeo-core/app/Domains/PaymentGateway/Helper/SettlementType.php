<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 21/06/19
 * Time: 16.33
 */

namespace Odeo\Domains\PaymentGateway\Helper;


class SettlementType {

  const DEFAULT = 'default';
  const REALTIME_WEEKDAY = 'realtime_weekday';
  const REALTIME_WEEKEND = 'realtime_weekend';
  const REALTIME = 'realtime';

}