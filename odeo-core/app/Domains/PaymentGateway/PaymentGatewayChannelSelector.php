<?php

namespace Odeo\Domains\PaymentGateway;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayChannelRepository;

class PaymentGatewayChannelSelector {

  private $pgChannelRepo;

  function __construct() {
    $this->pgChannelRepo = app()->make(PaymentGatewayChannelRepository::class);
  }

  public function getActivePaymentGroups(PipelineListener $listener, $data) {
    return $listener->response(200, [
      'pg_groups' => $this->pgChannelRepo->getActivePaymentGroups()
    ]);
  }

}