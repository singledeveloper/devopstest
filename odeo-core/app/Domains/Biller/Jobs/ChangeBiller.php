<?php

namespace Odeo\Domains\Biller\Jobs;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaSwitcher;
use Odeo\Jobs\Job;

class ChangeBiller extends Job  {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    $pipeline = new Pipeline();
    $pipeline->add(new Task(PulsaSwitcher::class, 'changeBiller'));
    $pipeline->execute($this->data);

  }

}


