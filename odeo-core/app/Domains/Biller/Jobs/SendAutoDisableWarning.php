<?php

namespace Odeo\Domains\Biller\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendAutoDisableWarning extends Job  {

  private $vendorName, $inventoryName, $hasBackup;

  public function __construct($vendorName, $inventoryName, $hasBackup) {
    parent::__construct();
    $this->vendorName = $vendorName;
    $this->inventoryName = $inventoryName;
    $this->hasBackup = $hasBackup;
  }

  public function handle() {

    Mail::send('emails.pulsa_inventory_disabled', [
      'inventory_name' => $this->inventoryName,
      'vendor_name' => $this->vendorName,
      'has_backup' => $this->hasBackup
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('merchandiser@odeo.co.id')->subject('Odeo Inventory Disabled - ' . time());
    });

  }

}


