<?php

namespace Odeo\Domains\Biller\Datacell;

use Odeo\Domains\Constant\BillerDatacell;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class Notifier extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::DATACELL);
    $this->request = app()->make(\Illuminate\Http\Request::class);
  }

  public function notify(PipelineListener $listener, $data) {
    if (isProduction() && !in_array(getClientIP(), BillerDatacell::IP_WHITELIST)) {
      return $listener->response(400, "FAIL, IP MISMATCH.");
    } else {
      $xml = $this->request->getContent();
      try {
        $content = simplexml_load_string($xml);
      }
      catch (\Exception $e) {
        return $listener->response(400, "INVALID XML, PLEASE FIX ASAP.");
      }

      if ($content && (isset($content->ref_trxid) && $transaction = $this->billerRepository->findById($content->ref_trxid))) {
        $this->load($transaction);
        if ($this->billerOrder->status != SwitcherConfig::BILLER_FAIL) {
          if (!isset($content->msg) || (isset($content->msg) && strpos($content->msg, " GAGAL,") !== false)) {
            $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
          }
          else if (isset($content->msg) &&
            (strpos($content->msg, " dianggap sukses") === false &&
              strpos($content->msg, " SN Operator: 00 ") === false &&
              strpos($content->msg, " The process failed ") === false
            )) {
            $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;

            try {
              $temp = explode('SN Operator:', $content->msg);
              $temp[1] = trim($temp[1]);
              $temp = explode(" ", $temp[1]);
              $temp = explode("/", $temp[0]);
              $this->currentSwitcher->serial_number = trim($temp[0]);
            }
            catch (\Exception $e) {}

            if (isset($content->trx_id)) $this->currentRecon->biller_transaction_id = $content->trx_id;
          }
        }

        $this->billerOrder->log_notify = $xml;
        $this->finalize($listener, false);
      }
      else if (isset($data["ref_trxid"]) && $transaction = $this->billerRepository->findById($data["ref_trxid"])) {
        $this->load($transaction);
        if ($this->billerOrder->status != SwitcherConfig::BILLER_FAIL) {
          $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;

          if ($this->currentSwitcher->vendor_switcher_id == SwitcherConfig::DATACELL) {
            $this->currentSwitcher->status = SwitcherConfig::SWITCHER_IN_QUEUE;
          }

          $this->finalize($listener, false);
        }
      }
      else clog($this->currentRoute, "RESPONSE DATACELL UNKNOWN: " . $xml);

      return $listener->response(200, ["status" => "OK"]);
    }
  }

}
