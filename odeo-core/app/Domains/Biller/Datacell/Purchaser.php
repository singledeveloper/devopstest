<?php

namespace Odeo\Domains\Biller\Datacell;

use Odeo\Domains\Constant\BillerDatacell;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class Purchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::DATACELL);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);
    
    $time = date("His", time());
    $number = $this->currentSwitcher->number;
    $code = $this->currentSwitcher->currentInventory->code;
    if (strpos($code, "PLNS") === false) $number = revertTelephone($number);
    
    $xml = '<?xml version="1.0"?>
      <datacell>
        <perintah>charge</perintah>
        <oprcode>' . $code . '</oprcode>
        <userid>' . BillerDatacell::USER_ID . '</userid>
        <time>' . $time . '</time>
        <msisdn>' . $number . '</msisdn>
        <ref_trxid>' . (string) $this->billerOrder->id . '</ref_trxid>
        <sgn>' . base64_encode((substr($number, -4) . $time) ^ (substr(BillerDatacell::USER_ID, 4) . BillerDatacell::PASSWORD)) . '</sgn>
      </datacell>';
      
    $this->billerOrder->log_request = preg_replace('/> +</','><', str_replace(array("\t", "\n"), "", $xml));

    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ]
    ]);
    
    try {
      $response = $client->request('POST', BillerDatacell::PROD_SERVER, [
        "body" => $xml,
        "timeout" => 100
      ]);
      $this->billerOrder->log_response = $response->getBody()->getContents();
      $content = simplexml_load_string($this->billerOrder->log_response);

      if (!$content) $statusTransaction = SwitcherConfig::BILLER_SUSPECT;
      else if (isset($content->resultcode) && $content->resultcode == "999") {
        $statusTransaction = SwitcherConfig::BILLER_FAIL;
        if (BillerDatacell::checkWrongNumberPattern($content->message)) {
          $this->currentSwitcher->status = SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER;
        }
      }
      else $statusTransaction = SwitcherConfig::BILLER_IN_QUEUE;

      try {
        $temp = explode('SN Kami :', $content->message);
        $temp = explode('.', $temp[1]);
        $this->currentRecon->biller_transaction_id = trim($temp[0]);
      }
      catch(\Exception $e) {}

      try {
        $temp = explode('Harga:', $content->message);
        $temp = explode('.', $temp[1]);
        $this->currentSwitcher->current_base_price = trim($temp[0]);
      }
      catch(\Exception $e) {}
    }
    catch(\Exception $e) {
      $this->timeoutMessage = 'Error timeout: ' . $e->getMessage();
      $statusTransaction = SwitcherConfig::BILLER_TIMEOUT;
    }

    $this->billerOrder->status = $statusTransaction;

    if (isset($content->message) && $position = strpos($content->message, "Saldo: Rp ")) {
      $position += 10;
      $s = substr($content->message, $position);
      $balance = substr($s, 0, strpos($s, "."));

      $this->currentBiller->current_balance = $balance;
    }

    return $this->finalize($listener);
  }
}