<?php

namespace Odeo\Domains\Biller\Blackhawk\Helper;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BillerBlackhawk;

class UrlManager {

  private $redis, $namespace;

  public function __construct() {
    $this->redis = Redis::connection();
    $this->namespace = 'odeo_core:blackhawk_credential';
  }

  public function get() {

    if ($url = $this->redis->hget($this->namespace, 'url')) {
      return $url;
    }

    $url = BillerBlackhawk::getUrl();
    $this->redis->hset($this->namespace, 'url', $url);

    return $url;
  }

  public function change() {

    if (app()->environment() == 'production') {
      $url = $this->redis->hget($this->namespace, 'url');
      $url == BillerBlackhawk::PROD_URL ? BillerBlackhawk::PROD_URL2 : BillerBlackhawk::PROD_URL;

      $this->redis->hset($this->namespace, 'url', $url);
    }

  }

}