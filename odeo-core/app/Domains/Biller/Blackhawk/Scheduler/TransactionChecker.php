<?php

namespace Odeo\Domains\Biller\Blackhawk\Scheduler;

use Odeo\Domains\Biller\Blackhawk\Purchaser;
use Odeo\Domains\Biller\Blackhawk\VoidRequester;
use Odeo\Domains\Constant\BillerBlackhawk;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class TransactionChecker {

  private $billerRepository;

  public function __construct() {
    $this->billerRepository = app()->make(\Odeo\Domains\Biller\Blackhawk\Repository\OrderDetailBlackhawkRepository::class);
  }

  public function run() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }

  public function check(PipelineListener $listener, $data) {
    foreach ($this->billerRepository->getPendingTransactions() as $item) {
      if (time() - strtotime($item->created_at) >= 60) {
        if ($item->transaction_type == BillerBlackhawk::TRX_TYPE_VOID)
          (new VoidRequester())->reversal($listener, ['item' => $item]);
        else (new Purchaser())->reversal($listener, ['item' => $item]);
      }
    }
  }
}