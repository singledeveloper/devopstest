<?php

namespace Odeo\Domains\Biller\PPS;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\Contract\BillerContract;

class PPSManager implements BillerContract {

  private $purchaser, $inventoryValidator;

  public function __construct() {
    $this->purchaser = app()->make(Purchaser::class);
    $this->inventoryValidator = app()->make(InventoryValidator::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->purchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validateInventory($listener, $data);
  }

}