<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/15/17
 * Time: 6:39 PM
 */

namespace Odeo\Domains\Biller\JavaH2H;


use Odeo\Domains\Biller\BillerManager;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class Notifier extends BillerManager {

  private $request;

  public function __construct() {
    parent::__construct(SwitcherConfig::JAVAH2H);
    $this->request = app()->make(\Illuminate\Http\Request::class);
  }

  public function notify(PipelineListener $listener, $data) {
    $data = json_decode($data['content'], true);
    if (isset($data['trxid_api']) && $transaction = $this->billerRepository->findById($data['trxid_api'])) {
      $this->load($transaction);
      if ($this->billerOrder->status != SwitcherConfig::BILLER_SUCCESS) {
        if ($data['status'] == 4) {
          if ($this->currentSwitcher->currentInventory->pulsa->service_detail_id == ServiceDetail::PAKET_DATA_ODEO && trim($data['sn']) == '') {
            $this->isCheckSN = false;
          }

          $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
          $this->currentSwitcher->serial_number = $data['sn'];
          $this->currentSwitcher->current_base_price = $data['price'];
          $this->currentRecon->biller_transaction_id = $data['trxid'];

          try {
            $this->currentBiller->current_balance = $data['last_balance'];
          } catch (\Exception $e) {}

        }
        else if ($data['status'] == 1) $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
        else if ($data['status'] == 2 && strpos($data['note'], 'Nomor tujuan tidak ditemukan') !== false)
          $this->billerOrder->status = SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER;
        else if (strpos($data['note'], 'Telah diorder sebelumnya') !== false) {
          $this->billerOrder->status = SwitcherConfig::BILLER_DUPLICATE_SUCCESS;
          try {
            $temp = explode(' SN: ', $data['note']);
            $this->currentSwitcher->serial_number = $temp[1];
          }
          catch(\Exception $e) {
            $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
          }
        }
        else $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;

        $this->billerOrder->log_notify = json_encode($data);
        $this->finalize($listener, false);
      }
    }

    return $listener->response(200);
  }
}