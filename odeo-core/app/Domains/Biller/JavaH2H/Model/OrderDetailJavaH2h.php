<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/15/17
 * Time: 11:55 AM
 */

namespace Odeo\Domains\Biller\JavaH2H\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPulsaSwitcher;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaOdeoInventory;

class OrderDetailJavaH2h extends Entity {

  public function inventory() {
    return $this->belongsTo(PulsaOdeoInventory::class, 'inventory_id');
  }

  public function switcher() {
    return $this->belongsTo(OrderDetailPulsaSwitcher::class, 'switcher_reference_id');
  }
}