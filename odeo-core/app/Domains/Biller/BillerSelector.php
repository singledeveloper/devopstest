<?php

namespace Odeo\Domains\Biller;

use Odeo\Domains\Constant\BankAccount;
use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\UserKycStatus;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class BillerSelector implements SelectorListener {


  private $vendorSwitchers, $currencyHelper, $vendorRecons, $billerConnections, $supplyValidator;

  public function __construct() {
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->vendorRecons = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
    $this->billerConnections = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherConnectionRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
  }

  public function _transforms($item, Repository $repository) {
    $output = [];
    $output['id'] = $item->id;
    $output['name'] = $item->name;
    $output['is_active'] = $item->is_active;
    if ($repository->hasField('current_balance'))
      $output['current_balance'] = $this->currencyHelper->formatPrice($item->current_balance);
    if ($repository->hasField('notify_slug'))
      $output['notify'] = Supplier::createSlug($item->notify_slug);
    if ($repository->hasField('additional_data'))
      $output['additional_data'] = json_decode($item->additional_data);
    if ($repository->hasExpand('store_owner')) {
      $store = $item->ownerStore;
      $output['owner_store_id'] = $item->owner_store_id;
      $output['owner_store_name'] = $store ? $store->name : '';
    }
    if ($repository->hasField('status')) {
      $output['status'] = $item->status;
    }
    return $output;
  }

  public function _extends($data, Repository $repository) {
    if ($vendorSwitcherIds = $repository->beginExtend($data, "id")) {
      if ($repository->hasExpand('sla')) {
        $repository->addExtend('sla', $this->getSLA($vendorSwitcherIds));
      }
      if ($repository->hasExpand('lead_time')) {
        $repository->addExtend('average_lead_time', $this->getLeadTime($vendorSwitcherIds));
      }
      if ($repository->hasExpand('connection')) {
        $repository->addExtend('connection', $this->getConnection($vendorSwitcherIds));
      }
    }
    return $repository->finalizeExtend($data);
  }

  public function get(PipelineListener $listener, $data) {
    if (isset($data['store_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
      if (!$isValid) return $listener->response(400, $message);
    }

    $this->vendorSwitchers->normalizeFilters($data);

    $vendorSwitchers = [];
    foreach ($this->vendorSwitchers->gets() as $item) {
      $vendorSwitchers[] = $this->_transforms($item, $this->vendorSwitchers);
    }

    if (sizeof($vendorSwitchers) > 0) {
      $result = [
        "billers" => $this->_extends($vendorSwitchers, $this->vendorSwitchers)
      ];
      if (isset($data['store_id'])) {
        $userProxies = app()->make(\Odeo\Domains\Account\Repository\UserProxyRepository::class);
        $proxy = $userProxies->findIpByStoreId($data['store_id']);
        $result['ip'] = $proxy ? $proxy->ip : '52.74.36.212';
      }
      return $listener->response(200, $result);
    }
    return $listener->response(204, ["billers" => []]);
  }

  public function getSLA($vendorSwitcherIds, $date = '') {
    $slas = [];
    if (sizeof($vendorSwitcherIds) > 0) {
      $allPurchases = $this->vendorRecons->getCountByStatus($vendorSwitcherIds, [
        SwitcherConfig::BILLER_SUCCESS,
        SwitcherConfig::BILLER_FAIL,
        SwitcherConfig::BILLER_INTERNAL_ERROR,
        SwitcherConfig::BILLER_UNKNOWN_ERROR
      ], $date);
      $allPurchaseTemps = [];
      foreach ($allPurchases as $item) {
        $allPurchaseTemps[$item->vendor_switcher_id] = $item->total_counts;
      }

      $successOnlyPurchases = $this->vendorRecons->getCountByStatus($vendorSwitcherIds, [
        SwitcherConfig::BILLER_SUCCESS
      ], $date);
      $successOnlyPurchaseTemps = [];
      foreach ($successOnlyPurchases as $item) {
        $successOnlyPurchaseTemps[$item->vendor_switcher_id] = $item->total_counts;
      }

      $successOnlyWithSNs = $this->vendorRecons->getSNCount($vendorSwitcherIds, $date);
      $successOnlyWithSNTemps = [];
      foreach ($successOnlyWithSNs as $item) {
        $successOnlyWithSNTemps[$item->vendor_switcher_id] = $item->total_counts;
      }
    }

    foreach($vendorSwitcherIds as $item) {
      $slas[$item] = [
        'all_purchase_count' => isset($allPurchaseTemps[$item]) ? $allPurchaseTemps[$item] : 0,
        'success_purchase_count' => isset($successOnlyPurchaseTemps[$item]) ? $successOnlyPurchaseTemps[$item] : 0,
        'success_purchase_with_sn_count' => isset($successOnlyWithSNTemps[$item]) ? $successOnlyWithSNTemps[$item] : 0,
        'success_percentage' => isset($allPurchaseTemps[$item]) && isset($successOnlyPurchaseTemps[$item]) ?
          round($successOnlyPurchaseTemps[$item] / $allPurchaseTemps[$item], 2) : 0,
        'success_percentage_with_sn' => isset($successOnlyWithSNTemps[$item]) && isset($successOnlyPurchaseTemps[$item]) ?
          round($successOnlyWithSNTemps[$item] / $successOnlyPurchaseTemps[$item], 7) : 0
      ];
    }

    return $slas;
  }

  public function getLeadTime($vendorSwitcherIds, $date = '') {
    $leads = [];
    if ($date == '') $date = date('Y-m-d');
    if (sizeof($vendorSwitcherIds) > 0) {
      $lead = $this->vendorRecons->getLeadTime($vendorSwitcherIds, $date);
      foreach($lead as $item) $leads[$item->vendor_switcher_id] = $item->average_lead_time;
    }
    foreach ($vendorSwitcherIds as $item) {
      if (!isset($leads[$item])) $leads[$item] = null;
    }
    return $leads;
  }

  public function getConnection($vendorSwitcherIds) {
    $connections = [];
    if (sizeof($vendorSwitcherIds) > 0) {
      $connection = $this->billerConnections->getByVendorSwitcherIds($vendorSwitcherIds);
      foreach($connection as $item)
        $connections[$item->vendor_switcher_id] = [
          'server_destination' => $item->server_destination,
          'template_name' => $item->template->name,
          'format_type' => $item->template->format_type
        ];
    }
    foreach ($vendorSwitcherIds as $item) {
      if (!isset($connections[$item])) $connections[$item] = null;
    }
    return $connections;
  }

  public function getReport(PipelineListener $listener, $data) {
    $reports = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoReportRepository::class);
    if ($last = $reports->getLastDate()) {
      $result = ['last_report_date' => $last->report_date];
      foreach ($reports->getAllReportByDate($last->report_date) as $item) {
        $result[$item->report_type] = $item->total;
        if ($item->additional_data != null) {
          if (!$adata = json_decode($item->additional_data)) $adata = $item->additional_data;
          $result[$item->report_type . '_data'] = $adata;
        }
      }
      return $listener->response(200, $result);
    }

    return $listener->response(204);
  }

}