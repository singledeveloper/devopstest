<?php

namespace Odeo\Domains\Biller\Indosmart\Repository;

use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;
use Odeo\Domains\Biller\Indosmart\Model\OrderDetailIndosmart;

class OrderDetailIndosmartRepository extends BillerRepository {

  public function __construct(OrderDetailIndosmart $orderDetailIndosmart) {
    $this->setModel($orderDetailIndosmart);
  }
  
  public function getTodayLatestId() {
    return $this->model->where('created_at', '>=', date('Y-m-d').' 00:00:00')
        ->whereNotNull('transaction_id')->count() + 1;
  }

}
