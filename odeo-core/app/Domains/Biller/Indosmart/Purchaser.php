<?php

namespace Odeo\Domains\Biller\Indosmart;

use Odeo\Domains\Constant\BillerIndosmart;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class Purchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::INDOSMART);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    $this->billerOrder->transaction_id = sprintf("%06d", $this->billerRepository->getTodayLatestId());
    
    list($cardNumber, $voucherType, $amount) = explode(".", $this->currentSwitcher->currentInventory->code);
    
    $xml = BillerIndosmart::formatXML("iZoneGateway.topupRequest", [
      "ProcessingCode" => BillerIndosmart::PROCESSING_CODE,
      "TransactionID" => $this->billerOrder->transaction_id,
      "TransactionDateTime" => '<dateTime.iso8601>' . date("Ymd\TH:i:s") . '</dateTime.iso8601>',
      "Destination" => $this->currentSwitcher->number,
      "Amount" => $amount . "000",
      "VoucherType" => $voucherType,
      "PIN" => BillerIndosmart::getPin(),
      "MerchantType" => BillerIndosmart::MERCHANT_TYPE,
      "TerminalID" => BillerIndosmart::getTerminalId(),
      "MerchantID" => BillerIndosmart::MERCHANT_ID,
      "CardNumber" => $cardNumber
    ]);
    
    $this->billerOrder->log_request = $xml;
    
    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ]
    ]);
    
    try {
      $response = $client->request('POST', BillerIndosmart::getServer(), [
        "body" => $xml,
        "timeout" => 100
      ]);
      $this->billerOrder->log_response = $response->getBody()->getContents();
      
      $dom = new \DOMDocument;
      if (@$dom->loadXML($this->billerOrder->log_response)) {
        $arrays = $dom->getElementsByTagName('member');

        $i = 0; foreach ($arrays as $array) {
          $name = $array->getElementsByTagName('name');
          $value = $array->getElementsByTagName('value');
          $a[$name[0]->nodeValue] = $value[0]->nodeValue;
        }

        if (isset($a['ResponseCode'])) {
          list ($statusTransaction, $message) = BillerIndosmart::translateResponse($a['ResponseCode']);
          if ($message != "") $this->billerOrder->log_response = str_replace('</methodResponse>', '<message>' . $message . '</message></methodResponse>', $this->billerOrder->log_response);
        }
        else $statusTransaction = SwitcherConfig::BILLER_IN_QUEUE;

        if (isset($a['ReferenceNo'])) $this->currentRecon->biller_transaction_id = $a['ReferenceNo'];
      }
      else $statusTransaction = SwitcherConfig::BILLER_SUSPECT;
    }
    catch (\Exception $e) {
      $this->timeoutMessage = 'Error timeout: ' . $e->getMessage();
      $statusTransaction = SwitcherConfig::BILLER_TIMEOUT;
    }

    $this->billerOrder->status = $statusTransaction;

    if ($this->billerOrder->status == SwitcherConfig::BILLER_SUCCESS) {
      if (isset($a['VoucherSN'])) $this->currentSwitcher->serial_number = trim($a['VoucherSN']);
      if (isset($a['TerminalBalance'])) $this->currentBiller->current_balance = $a['TerminalBalance'];
    }

    return $this->finalize($listener);
  }
}