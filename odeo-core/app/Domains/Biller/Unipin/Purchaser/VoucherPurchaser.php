<?php

namespace Odeo\Domains\Biller\Unipin\Purchaser;

use Odeo\Domains\Constant\BillerUnipin;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class VoucherPurchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::UNIPIN);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    $guid = BillerUnipin::getGUID(BillerUnipin::TYPE_VOUCHER);
    $secret = BillerUnipin::getSecret(BillerUnipin::TYPE_VOUCHER);
    $qty = 1;

    try {
      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ],
        'timeout' => 20
      ]);

      $request = [
        'partner_guid' => $guid,
        'denomination_code' => $this->currentPulsaInventory->code,
        'quantity' => $qty,
        'reference_no' => $this->currentRecon->id,
        'remark' => '',
        'signature' => hash('sha256', $guid . $this->currentPulsaInventory->code . $qty . $this->currentRecon->id . $secret)
      ];
      $this->currentRecon->request = json_encode($request);

      $response = $client->request('POST', BillerUnipin::getVoucherURL('voucher/request'), [
        'json' => $request
      ]);
      $response = json_decode($response->getBody()->getContents());

      if (isset($response->status)) {
        if ($response->status == '1') {
          $this->currentRecon->status = SwitcherConfig::BILLER_SUCCESS;
          $this->currentSwitcher->current_base_price = $response->total_amount;
          $this->currentRecon->biller_transaction_id = $response->order;
          $this->currentSwitcher->serial_number = $response->items[0]->serial_1 . '/' . $response->items[0]->serial_2;
        }
        else if ($response->status == '0') $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
        else $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;
      }
      else $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;

      $this->currentRecon->response = json_encode($response);
    }
    catch (\Exception $e) {
      $this->currentRecon->response = 'Error: ' . $e->getMessage();
      $this->currentRecon->status = SwitcherConfig::BILLER_TIMEOUT;
    }

    try {
      $response = $client->request('POST', BillerUnipin::getVoucherURL('voucher/balance'), [
        'json' => [
          'partner_guid' => $guid,
          'signature' => hash('sha256', $guid . $secret)
        ]
      ]);
      $response = json_decode($response->getBody()->getContents());
      if (isset($response->balance)) $this->currentBiller->current_balance = $response->balance;
    }
    catch(\Exception $e) {}

    return $this->finalize($listener);
  }
}