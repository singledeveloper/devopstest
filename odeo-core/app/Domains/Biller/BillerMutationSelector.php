<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:54 PM
 */

namespace Odeo\Domains\Biller;

use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Constant\SwitcherConfig;

class BillerMutationSelector implements SelectorListener {

  private $currencyHelper, $billerMutations, $mutations;

  public function __construct() {
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->billerMutations = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationMutationRepository::class);
    $this->mutations = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherMutationRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    return $item;

  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getMutationDetails(PipelineListener $listener, $data) {

    $mutations = [];

    $this->billerMutations->normalizeFilters($data);
    $this->billerMutations->setSimplePaginate(true);

    foreach ($this->billerMutations->gets($data['vendor_switcher_id']) as $item) {
      $mutations[] = [
        "order_id" => $item->order_id,
        "difference" => $item->last_balance_recorded_from_biller - $item->balance_after,
        "amount" => $this->currencyHelper->formatPrice($item->amount),
        "number" => revertTelephone($item->number),
        "balance_before" => $this->currencyHelper->formatPrice($item->balance_before),
        "balance_after" => $this->currencyHelper->formatPrice($item->balance_after),
        "last_balance_recorded_from_biller" => $this->currencyHelper->formatPrice($item->last_balance_recorded_from_biller),
        "created_at" => date('Y-m-d H:i:s', strtotime($item->created_at)),
        "route" => $item->route
      ];
    }

    if (sizeof($mutations) > 0)
      return $listener->response(200, array_merge(
        ["mutations" => $this->_extends($mutations, $this->billerMutations)],
        $this->billerMutations->getPagination()
      ));

    return $listener->response(204, ["mutations" => []]);
  }

  public function getLastMutation(PipelineListener $listener, $data) {
    $mutations = [];

    foreach ($this->mutations->findAllWithVendorSwitcher() as $item) {
      $mutations[] = [
        "vendor_switcher_id" => $item->vendor_switcher_id,
        "biller_name" => $item->vendorSwitcher->name,
        "last_balance_recorded_from_biller" => $item->vendorSwitcher->current_balance,
        "difference" => $item->vendorSwitcher->current_balance - $item->current_mutation,
        "last_zero_at" => $item->last_zero_at
      ];
    }

    if (sizeof($mutations) > 0)
      return $listener->response(200, ["mutations" => $mutations]);

    return $listener->response(204, ["mutations" => []]);
  }

}
