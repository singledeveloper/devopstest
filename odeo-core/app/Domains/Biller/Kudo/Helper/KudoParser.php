<?php

namespace Odeo\Domains\Biller\Kudo\Helper;

use Odeo\Domains\Biller\BillerManager;
use Odeo\Domains\Constant\BillerKudo;
use Odeo\Domains\Constant\SwitcherConfig;

class KudoParser extends BillerManager {

  protected $biller, $client;

  public function __construct() {
    parent::__construct(SwitcherConfig::KUDO);
    $this->client = new \GuzzleHttp\Client();
  }

  private function getToken() {
    $billerData = json_decode($this->biller->additional_data);
    return !$billerData || ($billerData && !isset($billerData->current_token)) ? BillerKudo::GUEST_TOKEN : $billerData->current_token;
  }

  private function checkAuth($response, $func, $url, $data = [], $noloop) {
    $response = json_decode($response);
    if ($response->code == '2019') {
      if ($noloop) return false;
      $this->generateNewToken();
      return $this->$func($url, $data, true);
    }
    return $response;
  }

  protected function postForm($url, $data, $noloop = false) {
    $response = $this->client->request('POST', $url, [
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Authorization' => $this->getToken(),
        'User-Agent' => BillerKudo::USER_AGENT,
        'Channel' => BillerKudo::CHANNEL,
        'Client-Version' => BillerKudo::CLIENT_VERSION
      ],
      'form_params' => $data
    ]);
    return $this->checkAuth($response->getBody()->getContents(), 'postForm', $url, $data, $noloop);
  }

  protected function postJson($url, $data, $noloop = false) {
    $response = $this->client->request('POST', $url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => $this->getToken(),
        'User-Agent' => BillerKudo::USER_AGENT,
        'Channel' => BillerKudo::CHANNEL,
        'Client-Version' => BillerKudo::CLIENT_VERSION
      ],
      'json' => $data
    ]);
    return $this->checkAuth($response->getBody()->getContents(), 'postJson', $url, $data, $noloop);
  }

  protected function get($url, $data = [], $noloop = false) {
    $response = $this->client->request('GET', $url, [
      'headers' => [
        'Authorization' => $this->getToken(),
        'User-Agent' => BillerKudo::USER_AGENT,
        'Channel' => BillerKudo::CHANNEL,
        'Client-Version' => BillerKudo::CLIENT_VERSION
      ]
    ]);
    return $this->checkAuth($response->getBody()->getContents(), 'get', $url, $data, $noloop);
  }

  protected function generateNewToken() {
    $response = $this->postForm(BillerKudo::SERVER . '/users/credentials', [
      'credentials' => hash('sha1', BillerKudo::USERNAME) . '::' . hash('sha256',BillerKudo::PASSWORD)
    ]);
    clog('haha_kudo', json_encode($response));
    $this->biller->additional_data = json_encode(['current_token' => $response->message->token]);
    $this->vendorSwitcherRepository->save($this->biller);
  }
}