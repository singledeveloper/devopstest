<?php

namespace Odeo\Domains\Biller\Kudo;

use GuzzleHttp\Exception\RequestException;
use Odeo\Domains\Biller\Kudo\Helper\KudoParser;
use Odeo\Domains\Constant\BillerKudo;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class Purchaser extends KudoParser {

  public function __construct() {
    parent::__construct();
  }

  public function purchase(PipelineListener $listener, $data) {
    if (!$this->initiate($listener, $data)) return $listener->response(200);

    $this->biller = $this->vendorSwitchers->findById(SwitcherConfig::KUDO);
    $billerData = json_decode($this->biller->additional_data);
    if (!$billerData || ($billerData && !isset($billerData->current_token))) {
      $this->generateNewToken();
    }

    $stuck = false;
    try {
      if (!$response = $this->get(BillerKudo::SERVER . '/products/pulsa'))
        $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
      else {
        $cartData = [];
        $inventory = $this->currentSwitcher->currentInventory;
        list($category, ) = explode(' ', $inventory->code);
        $number = revertTelephone($this->currentSwitcher->number);

        foreach ($response->message->vouchers as $item) {
          if ($item->name == $category) {
            foreach ($item->voucher_list as $output) {
              if (strpos($output->desc, $inventory->code)) {
                $cartData = [
                  'cart_id' => hash('sha256', $this->billerOrder->id . microtime()),
                  'items' => [
                    [
                      'attributes' => json_encode([
                        'customer' => ['cellphone_number' => $number],
                        'purchase_referral' => ['action' => 'pulsa']
                      ]),
                      'item_komisi' => 0,
                      'mIsBukalapakItem' => false,
                      'item_id' => intval($output->id),
                      'image_url' => $item->image,
                      'item_name' => $output->desc . ' (' . $number . ')',
                      'price' => $output->price,
                      'item_reference_id' => $output->ref_id,
                      'mMaxQty' => 1,
                      'mMaxSku' => 1,
                      'mMinQty' => 1,
                      'quantity' => 1,
                      'mRequireAddress' => false,
                      'mRequireNote' => false,
                      'vendor_id' => intval($item->v_id)
                    ]
                  ],
                  'phone' => $number
                ];
                $this->billerOrder->log_request = json_encode($cartData);
                break;
              }
            }
          }
        }

        if (!$cart = $this->postJson(BillerKudo::SERVER . '/order/store', $cartData))
          $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
        else {
          if ($cart->code == '2055') $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
          else {
            $this->billerOrder->log_response = json_encode($cart);
            $stuck = true;
            if (!$response = $this->postForm(BillerKudo::SERVER . '/order', [
              'reference' => $cart->message->reference,
              'pin' => hash('sha256', BillerKudo::PIN),
              'voucher_code' => ''
            ])) $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
            else {
              $this->billerOrder->log_response = json_encode($response);
              if (in_array($response->code, ['2010', '2036'])) $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
              else $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
            }
          }
        }
      }
    }
    catch (RequestException $e) {
      $this->timeoutMessage = 'Error fail: ' . $e->getMessage() . "\n" . $e->getTraceAsString();
      if ($e->getResponse()->getStatusCode() == '400' || !$stuck) $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
      else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;
    }
    catch (\Exception $e) {
      $this->timeoutMessage = 'Error timeout: ' . $e->getMessage() . "\n" . $e->getTraceAsString();
      $this->billerOrder->status = ($stuck) ? SwitcherConfig::BILLER_TIMEOUT : SwitcherConfig::BILLER_FAIL;
    }

    return $this->finalize($listener);

  }
}