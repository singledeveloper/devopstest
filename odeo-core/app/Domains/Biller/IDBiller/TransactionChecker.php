<?php

namespace Odeo\Domains\Biller\IDBiller;

use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Helper\BillerStatusUpdater;

class TransactionChecker extends BillerStatusUpdater {

  public function __construct() {
    parent::__construct();
  }

  public function check(PipelineListener $listener, $data) {

    $this->setConnectionData($data);

    $recons = $this->reconRepository->getByIds($this->getReconIds());
    if (count($recons)) {

      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ]
      ]);

      foreach ($recons as $item) {

        if ($item->biller_transaction_id == null) continue;

        $this->currentRecon = $item;
        $this->currentSwitcher = $this->currentRecon->switcher;

        $response = false;
        try {
          $response = $client->request('POST', str_replace('APITopupService', 'APIDataTransactionService', $this->getDestination()), [
            "json" => [
              "MitraId" => $this->getUserId(),
              "MitraKey" => md5($this->getUserId() . 'DataTransactionService' . $this->getToken()),
              "RequestMethod" => "DataTransactionService",
              "RefNumber" => $this->currentRecon->biller_transaction_id,
              "StartDate" => date('Y-m-d', strtotime($item->created_at)),
              "EndDate" => date('Y-m-d', strtotime($item->created_at)),
              "Limit" => 1
            ]
          ]);
          $response = $response->getBody()->getContents();
        }
        catch (\Exception $e) {
          clog('idbiller_error', $e->getMessage());
        }

        if ($response) {
          try {
            $result = json_decode($response, true);
            $result = $result['DataTransactions'][0];

            $this->currentRecon->notify = $response;
            $this->statusBefore = $this->currentRecon->status;

            if (in_array($result['TransRc'], ['00', '34'])) {
              $this->currentRecon->status = $result['TransRc'] == '00' ? SwitcherConfig::BILLER_SUCCESS : SwitcherConfig::BILLER_DUPLICATE_SUCCESS;
              $this->currentSwitcher->current_base_price = $result['TransPrice'];
              $this->currentSwitcher->serial_number = $result['TransSn'];
            }
            else if (in_array($result['TransRc'], ['01', '35']))
              $this->currentRecon->status = SwitcherConfig::BILLER_IN_QUEUE;
            else $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;

            $this->supplyFinalize($listener);
          }
          catch (\Exception $e) {
            clog('idbiller_error', $e->getMessage());
          }
        }

      }
    }
  }

}

