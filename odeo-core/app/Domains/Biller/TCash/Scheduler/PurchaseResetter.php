<?php

namespace Odeo\Domains\Biller\TCash\Scheduler;

use Odeo\Domains\Biller\TCash\Jobs\DistributeDeposit;
use Odeo\Domains\Constant\SwitcherConfig;

class PurchaseResetter {

  private $tcashUsers, $vendorSwitchers, $pulsaInventories;

  public function __construct() {
    $this->tcashUsers = app()->make(\Odeo\Domains\Biller\TCash\Repository\TcashUserRepository::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
  }

  public function run() {

    foreach ($this->vendorSwitchers->getByStatus(SwitcherConfig::BILLER_STATUS_HIDDEN_TCASH) as $item) {
      $this->pulsaInventories->updateActiveAllSeeded($item->id);
    }

    dispatch(new DistributeDeposit());
  }

  public function reset() {
    $this->tcashUsers->resetPurchaseCount();
  }
}
