<?php

namespace Odeo\Domains\Biller\TCash;

use Illuminate\Support\Facades\Crypt;
use Odeo\Domains\Biller\TCash\Helper\TCashManager;
use Odeo\Domains\Biller\TCash\Jobs\UpdateTCashUser;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TCash;
use Odeo\Domains\Core\PipelineListener;

class AccountRequester extends TCashManager {

  private $supplyValidator, $vendorSwitchers, $users, $selector;

  public function __construct() {
    parent::__construct();
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->selector = app()->make(AccountSelector::class);
  }

  private function checkBiller($tcashUser) {
    if (!$this->vendorSwitchers->findByStatus(SwitcherConfig::BILLER_STATUS_HIDDEN_TCASH, $tcashUser->store_id)) {
      $biller = $this->vendorSwitchers->getNew();
      $biller->vendor_id = 1;
      $biller->name = 'TCASH-' . $tcashUser->store_id;
      $biller->is_active = true;
      $biller->minimum_topup = BillerReplenishment::DEFAULT_MINIMAL_DEPOSIT;
      $biller->status = SwitcherConfig::BILLER_STATUS_HIDDEN_TCASH;
      $biller->owner_store_id = $tcashUser->store_id;
      $biller->is_auto_replenish = false;
      $this->vendorSwitchers->save($biller);
    }
  }

  public function requestOtp(PipelineListener $listener, $data) {
    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $storeId = isAdmin($data) ? null : $data['store_id'];
    $tcashUser = $this->tcashUsers->findByTelephone(purifyTelephone($data['telephone']));
    if ($tcashUser && $tcashUser->store_id != $storeId) {
      return $listener->response(400, 'Nomor ini sudah pernah digunakan oleh toko lain.');
    }

    if (isProduction()) {
      $this->setMSISDN(revertTelephone($data['telephone']));
      $this->request('POST', '/user/requestOtp', 'requestOtpRq');
      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);
    }

    if (!$tcashUser) {
      $tcashUser = $this->tcashUsers->getNew();
      $tcashUser->store_id = $storeId;
      $tcashUser->telephone = purifyTelephone($data['telephone']);
      $tcashUser->imei = TCash::generateIMEI();
      $tcashUser->imsi = TCash::DEFAULT_IMSI;
    }
    if (isProduction()) {
      $tcashUser->cookie_jar = json_encode($this->getCookieJar()->toArray());
    }
    $tcashUser->status = TCash::PENDING_OTP;
    $this->tcashUsers->save($tcashUser);

    return $listener->response(200, $this->selector->_transforms($tcashUser, $this->tcashUsers));
  }

  public function verifyOtp(PipelineListener $listener, $data) {
    if (!$tcashUser = $this->tcashUsers->findById($data['tcash_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($tcashUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    if (isProduction()) {
      $this->setCommonHeader($tcashUser);
      $verifyOtpRes = $this->request('POST', '/user/verifyOtp', 'verifyOtpRq', [
        'imei' => $tcashUser->imei,
        'otp' => $data['otp']
      ]);
      if ($this->errorMessage != '') return $listener->response(400, 'verifyOtpErr: ' . $this->errorMessage);

      //$verifyOtpRes['status'] >> IMEI_FOUND, IMEI_NOT_FOUND, NEW_CUSTOMER
      if ($verifyOtpRes['status'] == 'NEW_CUSTOMER') {
        $user = $this->users->findById($data['auth']['user_id']);
        $this->request('POST', '/user/registerUser', 'registerUserRq', [
          'mobileInfo' => $this->getMobileInfo($tcashUser),
          'userInfo' => [
            'email' => str_replace('odeo.co.id', 'gmail.com', $user->email),
            'token' => $this->getToken($tcashUser),
            'mno' => TCash::DEFAULT_MNO
          ]
        ]);
        if ($this->errorMessage != '') return $listener->response(400, 'regErr: ' . $this->errorMessage);

        $pin = rand('000000', '999999');
        $encTCashPin = $this->getLoginPin($pin);
        if ($this->errorMessage != '') return $listener->response(400, 'certiErr: ' . $this->errorMessage);

        $pinRegisterRes = $this->request('POST', '/user/setTcashPinForRegister', 'setTcashPinForRegisterRq', [
          'encNewPin' => $encTCashPin
        ]);
        if ($this->errorMessage != '') return $listener->response(400, 'pinRegErr: ' . $this->errorMessage);

        dispatch(new UpdateTCashUser($tcashUser->id, [
          'enc_token_key' => $pinRegisterRes['encTokenKey'],
          'pin' => $pin
        ]));

        $tcashUser->enc_token_key = $pinRegisterRes['encTokenKey'];
        $tcashUser->pin = Crypt::encrypt($pin);
        $tcashUser->customer_id = $pinRegisterRes['customerId'];

        $this->setCustomerID($tcashUser->customer_id);

        sleep(5);
        $this->deleteCookieJar();
        $tcashUser = $this->requestLogin($tcashUser);
        if ($this->errorMessage != '') {
          $tcashUser->last_error_message = $this->errorMessage;
          $tcashUser->status = TCash::PENDING_OTP;
        }
        else {
          $tcashUser->cookie_jar = json_encode($this->getCookieJar()->toArray());
          $tcashUser->status = TCash::CONNECTED;
        }
        $this->tcashUsers->save($tcashUser);

        $this->checkBiller($tcashUser);
      }
      else if ($verifyOtpRes['pinStatus'] != 'Normal')
        return $listener->response(400, 'Login gagal. Status PIN Akun Anda tidak normal/terblokir. Mohon cek kembali di aplikasi TCash.');
      else {
        $tcashUser->status = TCash::PENDING_PIN;
        $this->tcashUsers->save($tcashUser);
      }
    }
    else {
      $tcashUser->status = TCash::PENDING_PIN;
      $this->tcashUsers->save($tcashUser);
    }

    return $listener->response(200, $this->selector->_transforms($tcashUser, $this->tcashUsers));
  }

  public function login(PipelineListener $listener, $data) {
    if (!$tcashUser = $this->tcashUsers->findById($data['tcash_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($tcashUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    if (isProduction()) {
      $pin = isset($data['pin']) ? $data['pin'] : $tcashUser->pin;

      $encTCashPin = $this->getLoginPin($pin);
      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

      $this->setCommonHeader($tcashUser);

      $registerMobRes = $this->requestWithRetry('POST', '/settings/registerUserMobile', 'registerUserMobileRq', [
        'encTcashPin' => $encTCashPin,
        'mobileInfo' => $this->getMobileInfo($tcashUser),
        'token' => $this->getToken($tcashUser)
      ]);
      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

      $tcashUser->customer_id = $registerMobRes['customerId'];
      $tcashUser->enc_token_key = $registerMobRes['encTokenKey'];
      $tcashUser->pin = Crypt::encrypt($pin);

      $this->setCustomerID($tcashUser->customer_id);

      $tcashUser = $this->requestLogin($tcashUser);
      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

      $balanceRes = $this->request('POST', '/user/getTcashBalance', 'getTcashBalanceRq');
      if ($this->errorMessage == '')  $tcashUser->current_balance = $balanceRes['tcashBalance'];
    }

    $tcashUser->status = TCash::CONNECTED;
    $this->tcashUsers->save($tcashUser);

    $this->checkBiller($tcashUser);

    return $listener->response(200, $this->selector->_transforms($tcashUser, $this->tcashUsers));
  }

  public function getHistories(PipelineListener $listener, $data) {

    if (isset($data['search'])) $search = $data['search'];
    else $search = $data;

    if (!isset($search['telephone'])) return $listener->response(400, 'Telephone required');
    if (!isset($search['date'])) $search['date'] = date('Ymd');
    if (!$tcashUser = $this->tcashUsers->findByTelephone(purifyTelephone($search['telephone']))) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($tcashUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $this->setCommonHeader($tcashUser);

    $balanceRes = $this->request('POST', '/user/getTcashBalance', 'getTcashBalanceRq');

    $date = trim(str_replace('-', '', $search['date']));
    $response = $this->request('POST', '/tcash/getTcashTransactionHistory', 'getTcashTransactionHistoryRq',[
      'startDate' => $date . '000000',
      'endDate' => $date . '235959'
    ]);
    if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

    return $listener->response(200, [
      'balance' => $balanceRes['tcashBalance'],
      'transactions' => $response['transactions']
    ]);
  }

  public function test(PipelineListener $listener, $data) {

    if (!$tcashUser = $this->tcashUsers->findById($data['tcash_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($tcashUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    //app()->make(\Odeo\Domains\Biller\TCash\Scheduler\PurchaseResetter::class)->run();

    $this->setCommonHeader($tcashUser);

    $balanceRes = $this->request('POST', '/user/getTcashBalance', 'getTcashBalanceRq');
    if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

    return $listener->response(200, $balanceRes);

    //$this->deleteCookieJar();
    /*$tcashUser = $this->requestLogin($tcashUser);
    if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);
    $tcashUser->cookie_jar = json_encode($this->getCookieJar()->toArray());
    $this->tcashUsers->save($tcashUser);*/

    /*$response = $this->request('POST', '/tcash/getServiceDetail', 'getServiceDetailRq', [
      'msisdn' => '085216721900',
      'type' => 'PurchaseAirtime' // or PurchaseDataPackage
    ]);*/
    //$response = $this->request('POST', '/settings/getTcashFavoriteTransaction', 'getTcashFavoriteTransactionRq');
    //$response = $this->request('POST', '/bank/getBankList', 'getBankListRq');
    //$response = $this->request('POST', '/tcash/getBillerInfo', 'getBillerInfoRq');

    //if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

    //return $listener->response(200, $response);
  }

}

