<?php

namespace Odeo\Domains\Biller\TCash\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\TCash;
use Odeo\Jobs\Job;

class DistributeDeposit extends Job  {

  private $storeId;

  public function __construct($storeId = 0) {
    parent::__construct();
    $this->storeId = $storeId;
  }

  public function handle() {

    $redis = Redis::connection();

    if (!$redis->hget(TCash::REDIS_TCASH, TCash::REDIS_KEY_TCASH_DISTRIBUTE_LOCK_CORE) &&
      !$redis->hget(TCash::REDIS_TCASH, TCash::REDIS_KEY_TCASH_DISTRIBUTE_LOCK . $this->storeId)) {
      $redis->hsetnx(TCash::REDIS_TCASH, TCash::REDIS_KEY_TCASH_DISTRIBUTE_LOCK . $this->storeId, '1');

      $amountToBeTransferred = 50000;
      $histories = [];

      $tcashUsers = app()->make(\Odeo\Domains\Biller\TCash\Repository\TcashUserRepository::class);
      $transferrer = app()->make(\Odeo\Domains\Biller\TCash\Helper\Transferrer::class);

      foreach ($tcashUsers->getAllFullAccount($this->storeId) as $tcashUser) {

        $transferData = [];
        $totalAmountToBeTransferred = 0;

        $balanceRes = $transferrer->getBalance($tcashUser);
        if (isset($balanceRes['tcashBalance'])) $tcashUser->current_balance = $balanceRes['tcashBalance'];

        $basicTcashUsers = $tcashUsers->getAllBasicAccountWithZeroBalance($tcashUser->store_id, 15);
        foreach ($basicTcashUsers as $item) {
          if (isset($balanceRes['tcashBalance']) && $totalAmountToBeTransferred > $balanceRes['tcashBalance']) break;
          $amount = $amountToBeTransferred - ($item->purchase_counts * 10000);
          $transferData[$item->telephone] = $amount;
          $totalAmountToBeTransferred += $amount;
        }

        if (count($transferData) > 0) {

          $result = $transferrer->transfer([
            'tcash_user_id' => $tcashUser->id,
            'transfers' => $transferData
          ]);

          foreach ($basicTcashUsers as $item) {
            if (!isset($transferData[$item->telephone])) continue;
            $history = [
              'tcash_user_id' => $tcashUser->id,
              'transfer_tcash_user_id' => $item->id,
              'amount' => $transferData[$item->telephone],
              'created_at' => Carbon::now()->toDateTimeString()
            ];
            if (isset($result[$item->telephone])) {
              if ($result[$item->telephone]['status'] == SwitcherConfig::BILLER_SUCCESS) {
                $tcashUser->current_balance -= $transferData[$item->telephone];
                $item->current_balance += $transferData[$item->telephone];
                $tcashUsers->save($item);

                $history['ref'] = $result[$item->telephone]['ref'];
              }
              $history['status'] = $result[$item->telephone]['status'];
              $history['res_message'] = $result[$item->telephone]['response'];
            }
            else {
              $history['status'] = SwitcherConfig::BILLER_UNKNOWN_ERROR;
            }
            $histories[] = $history;
          }
          $tcashUsers->save($tcashUser);
        }
      }

      $redis->hdel(TCash::REDIS_TCASH, TCash::REDIS_KEY_TCASH_DISTRIBUTE_LOCK . $this->storeId);

      if (count($histories) > 0) {
        $tcashTransferHistories = app()->make(\Odeo\Domains\Biller\TCash\Repository\TcashTransferHistoryRepository::class);
        $tcashTransferHistories->saveBulk($histories);
      }
    }

  }

}


