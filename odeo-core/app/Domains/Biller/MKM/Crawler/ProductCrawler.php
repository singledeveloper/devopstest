<?php

namespace Odeo\Domains\Biller\MKM\Crawler;

use Odeo\Domains\Constant\BillerMKM;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SwitcherConfig;

class ProductCrawler {

  private $client;

  public function __construct() {
    
    $this->client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ]
    ]);

    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    
    $this->mapping = [
      Pulsa::OPERATOR_TELKOMSEL => '0852',
      Pulsa::OPERATOR_INDOSAT => '0815',
      Pulsa::OPERATOR_XL => '0878',
      Pulsa::OPERATOR_AXIS => '0878',
      Pulsa::OPERATOR_THREE => '0897',
      Pulsa::OPERATOR_SMARTFREN => '0881',
      Pulsa::OPERATOR_BOLT => '999'
    ];
  }
  
  public function getInquiry($code, $number) {
    $json = [
      "Action" => BillerMKM::ACTION_INQUIRY,
      "ClientId" => BillerMKM::getClientId(),
      "MCC" => BillerMKM::MCC,
      "KodeProduk" => BillerMKM::VOUCHER_ID,
      "KodeVoucher" => $code,
      "NomorPelanggan" => $number
    ];
    
    try {
      $response = $this->client->request('POST', BillerMKM::getServer(), ["json" => $json]);
      $content = $response->getBody()->getContents();
      return [$json, $content];
    }
    catch (\Exception $e) {
      return [$json, ""];
    }
  }

  public function crawl() {
    foreach ($this->pulsaInventories->findByVendorSwitcherId(SwitcherConfig::MKM) as $item) {
      if (isset($this->mapping[$item->pulsa->operator_id])) {
        list($request, $response) = $this->getInquiry($item->code, $this->mapping[$item->pulsa->operator_id] . "67878088");
        $response = json_decode($response, true);
        if (isset($response['Status']) && $response['Status'] == '0000') {
          $item->base_price = $response['TotalTagihan'];
          $item->is_active = true;
        }
        else $item->is_active = false;
      }
      else $item->is_active = false;
      
      $this->pulsaInventories->save($item);
    }

    $this->pulsaInventories->finalizeProducts(SwitcherConfig::MKM);
  }

}