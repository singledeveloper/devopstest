<?php

namespace Odeo\Domains\Biller\Bakoel\Purchaser;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class PulsaPurchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::BAKOEL);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    try {
      $pulsaInventory = $this->currentSwitcher->currentInventory;
      $request = [
        'product_id' => $pulsaInventory->code,
        'msisdn' => $this->currentSwitcher->number,
        'purchase_amount' => $pulsaInventory->base_price
      ];

      $this->billerOrder->log_request = json_encode($request);
      $response = BillerBakoel::setClient(BillerBakoel::PULSA_SERVER)->call(BillerBakoel::CMD_PAYMENT, $request);
      $this->billerOrder->log_response = json_encode($response);

      if (isset($response['status']) && $response['status'] == '00') {
        if (isset($response['VoucherSN']) && $response['VoucherSN'] != null) {
          $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
          $this->currentSwitcher->serial_number = $response['VoucherSN'];
        }
        else $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
        if (isset($response['price'])) $this->currentSwitcher->current_base_price = $response['price'];
      }
      else if (isset($response['status'])) {
        if (isset($response['message']) && BillerBakoel::checkWrongNumberPattern($response['message']))
          $this->currentSwitcher->status = SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER;
        $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
      }
      else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;

      if (isset($response['trxID'])) $this->currentRecon->biller_transaction_id = $response['trxID'];
    }
    catch (\Exception $e) {
      $this->billerOrder->log_response = 'Error: ' . $e->getMessage();
      $this->billerOrder->status = SwitcherConfig::BILLER_TIMEOUT;
    }

    if ($this->billerOrder->status == SwitcherConfig::BILLER_SUCCESS) {
      try {
        $info = BillerBakoel::setClient(BillerBakoel::PULSA_SERVER)->call(BillerBakoel::CMD_MITRA_INFO);
        $this->currentBiller->current_balance = $info['quota'];
      }
      catch(\Exception $e) {}
    }

    return $this->finalize($listener);
  }
}