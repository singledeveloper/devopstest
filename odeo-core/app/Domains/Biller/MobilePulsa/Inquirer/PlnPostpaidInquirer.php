<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/30/17
 * Time: 4:06 PM
 */

namespace Odeo\Domains\Biller\MobilePulsa\Inquirer;


use GuzzleHttp\Client;
use Odeo\Domains\Constant\BillerMobilePulsa;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class PlnPostpaidInquirer {

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
  }

  public function inquiryPostpaid(PipelineListener $listener, $data) {
    if (isset($data['inventory_code'])) $code = $data['inventory_code'];
    else {
      $inventory = $this->pulsaInventories->findById($data['inventory_id']);
      $code = $inventory->code;
    }

    $refId = isset($data['ref_id']) ? $data['ref_id'] : (new \DateTime())->getTimestamp();
    $query = [
      'commands' => BillerMobilePulsa::COMMAND_POSTPAID_INQUIRY,
      'username' => BillerMobilePulsa::USERNAME,
      'code' => $code,
      'hp' => isset($data['item_detail']) ? $data['item_detail']['number'] : $data['number'],
      'ref_id' => $refId,
      'sign' => md5(BillerMobilePulsa::USERNAME . BillerMobilePulsa::getKey() . $refId)
    ];

    $xml = new \SimpleXMLElement('<mp/>');
    array_walk_recursive($query, function ($value, $key) use ($xml) {
      $xml->addChild($key, $value);
    });

    $client = new Client();
    $response = $client->post(BillerMobilePulsa::getPostpaidUrl(), ['body' => $xml->asXML()]);
    $xmlContent = $response->getBody()->getContents();

    $dom = new \DOMDocument();
    if (@$dom->loadXML($xmlContent)) {
      $inquiryResult = [];
      for ($i = 0; $i < $dom->getElementsByTagName('mp')->item(0)->childNodes->length; $i++) {
        $current = $dom->getElementsByTagName('mp')->item(0);

        $key = $current->childNodes->item($i)->nodeName;
        $val = $current->childNodes->item($i)->nodeValue;
        $inquiryResult[$key] = $val;
      }

      if (isset($inquiryResult['response_code']) && $inquiryResult['response_code'] == '00') {
        $inquiryDetails = [];

        $inquiryResult['tariff'] = $dom->getElementsByTagName('tarif')[0]->nodeValue . '/' . $dom->getElementsByTagName('daya')[0]->nodeValue . 'VA';

        $nodeDetails = $dom->getElementsByTagName('detail');

        for ($i = 0; $i < $nodeDetails->length; $i++) {
          $currentDetail = $nodeDetails->item($i);

          $period = $currentDetail->getElementsByTagName('periode')[0]->nodeValue;

          $inquiryDetails[] = [
            'period' => substr($period, 0, 4) . '-' . substr($period, -2),
            'base_price' => $currentDetail->getElementsByTagName('nilai_tagihan')[0]->nodeValue,
            'fine' => $currentDetail->getElementsByTagName('denda')[0]->nodeValue,
            'admin_fee' => $currentDetail->getElementsByTagName('admin')[0]->nodeValue,
          ];
        }

        return [
          'tr_id' => $inquiryResult['tr_id'],
          'name' => strtoupper(PostpaidType::PLN_POSTPAID),
          'subscriber_id' => $inquiryResult['hp'],
          'subscriber_name' => $inquiryResult['tr_name'],
          'tariff' => $inquiryResult['tariff'],
          'multiplier' => count($inquiryDetails),
          'ref_id' => $inquiryResult['ref_id'],
          'biller_price' => $inquiryResult['price'],
          'details' => $inquiryDetails
        ];
      } else {
        return [
          'status' => SwitcherConfig::BILLER_FAIL,
          'error_message' => BillerMobilePulsa::POSTPAID_ERROR_MAPPINGS[$inquiryResult['response_code']] ?? BillerMobilePulsa::GENERAL_ERROR_MESSAGE,
          'log' => $xmlContent
        ];
      }
    }

    return [
      'status' => SwitcherConfig::BILLER_FAIL,
      'error_message' => BillerMobilePulsa::GENERAL_ERROR_MESSAGE,
      'log' => $xmlContent
    ];
  }
}