<?php

namespace Odeo\Domains\Biller\Jatelindo\Repository;

use Odeo\Domains\Biller\Jatelindo\Model\OrderDetailJatelindo;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;

class OrderDetailJatelindoRepository extends BillerRepository {

  public function __construct(OrderDetailJatelindo $orderDetailJatelindo) {
    $this->setModel($orderDetailJatelindo);
  }
  
  public function getTodayLatestId() {
    return $this->model->where('created_at', '>=', date('Y-m-d').' 00:00:00')
        ->whereNotNull('transaction_id')->count() + 1;
  }

  public function getPendingTransaction() {
    return $this->model->where(\DB::raw('date(created_at)'), date('Y-m-d'))
      ->where('status', SwitcherConfig::BILLER_IN_QUEUE)->get();
  }
}
