<?php

namespace Odeo\Domains\Biller\Artajasa\Purchaser;

use Odeo\Domains\Constant\BillerArtajasa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class IndosatPostpaidPurchaser extends BillerManager {

  private $inquirer;

  public function __construct() {
    parent::__construct(SwitcherConfig::ARTAJASA_NEW);
    $this->inquirer = app()->make(\Odeo\Domains\Biller\Artajasa\IndosatPostpaidInquirer::class);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    try {
      $data['user_id'] = $this->currentOrder->user_id;
      $inquiry = $this->inquirer->inquiry($listener, $data);
      if ($inquiry->status != SwitcherConfig::BILLER_SUCCESS) {
        $this->billerOrder->status = $inquiry->status;
        $this->billerOrder->log_request = '';
        $this->billerOrder->log_response = $inquiry->response;
      }
      else {
        $inquiry = $this->inquiryFormatter->setPath($this->currentPulsa->service_detail_id)->toJsonInquiry(json_decode($inquiry->result, true));
        $latestInquiryPrice = $inquiry['biller_price'] + ($inquiry['multiplier'] * $this->currentPulsaInventory->base_price);
        if ($latestInquiryPrice != $this->currentSwitcher->current_base_price) {
          $this->billerOrder->log_response = json_encode($inquiry);
          $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
        }
        else {
          $this->currentSwitcher->current_base_price = $latestInquiryPrice;

          $client = new \GuzzleHttp\Client([
            'curl' => [
              CURLOPT_SSL_VERIFYPEER => false,
              CURLOPT_SSL_VERIFYHOST => false
            ],
            'timeout' => 20,
            'headers' => [
              'Pass-Key' => env('ARTAJASA_BILLER_PASS_KEY', ''),
              'Client-Id' => env('ARTAJASA_BILLER_CLIENT_ID', '')
            ]
          ]);

          sleep(1);

          $dateISO = date('Ymd\THis+07');
          $number = revertTelephone($this->currentSwitcher->number);
          $stanId = BillerArtajasa::generateSTAN();
          $acquireId = env('ARTAJASA_BILLER_ACQUIRE_ID');

          $request = [
            'billerId' => $this->currentPulsaInventory->code,
            'signature' => strtoupper(sha1($stanId . $this->currentRecon->id .
              $dateISO . $number . $number . $acquireId . env('ARTAJASA_BILLER_SECRET_KEY', ''))),
            'transId' => [
              'STAN' => $stanId,
              'RRN' => (string)$this->currentRecon->id,
              'Timestamp' => $dateISO,
              'termId' => $number,
              'acquireId' => $acquireId,
              'Amount' => $inquiry['biller_price'],
              'authKey' => $inquiry['ref_id'],
              'respCode' => '00'
            ],
            'transData' => [
              'idCustomer' => $number
            ]
          ];
          $this->currentRecon->request = json_encode($request);

          $response = $client->request('POST', BillerArtajasa::INDOSAT_JSON_SERVER . 'api/payment', [
            'json' => $request
          ]);
          $response = json_decode($response->getBody()->getContents());

          if (isset($response->transId) && isset($response->transId->respCode)) {
            if ($response->transId->respCode == '00') {
              $this->currentRecon->status = SwitcherConfig::BILLER_SUCCESS;
              $this->currentSwitcher->serial_number = $response->transData->payRef;
            }
            else if (in_array($response->transId->respCode, ['68']))
              $this->currentRecon->status = SwitcherConfig::BILLER_IN_QUEUE;
            else $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
          }
          else $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;

          $this->currentRecon->response = json_encode($response);
        }
      }
    }
    catch (\Exception $e) {
      $this->currentRecon->response = 'Error: ' . $e->getMessage() . (isset($response) ? (', res: ' . json_encode($response)) : '');
      $this->billerOrder->status = SwitcherConfig::BILLER_TIMEOUT;
    }

    return $this->finalize($listener);
  }
}