<?php

namespace Odeo\Domains\Biller\Gojek\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendIPBanned extends Job  {

  private $gojekUserId, $bannedIp;

  public function __construct($gojekUserId, $bannedIp) {
    parent::__construct();
    $this->gojekUserId = $gojekUserId;
    $this->bannedIp = $bannedIp;
  }

  public function handle() {

    Mail::send([], [], function ($m) {

      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('ops@odeo.co.id')
        ->subject('Alert IP Banned '  . date('Y-m-d'));
      $m->setBody('Gojek melakukan ban di setting proxy terakhir [' . $this->bannedIp . '] dari Gojek User ID: ' . $this->gojekUserId, 'text/html');

    });

  }

}


