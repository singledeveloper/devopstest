<?php

namespace Odeo\Domains\Biller\Gojek;

use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class AccountSelector implements SelectorListener {


  private $gojekUsers, $currencyHelper, $supplyValidator;

  public function __construct() {
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->gojekUsers = app()->make(\Odeo\Domains\Biller\Gojek\Repository\GojekUserRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    if ($item->status == Gojek::CONNECTED) $status = 'CONNECTED';
    else if ($item->status == Gojek::PENDING_OTP) $status = 'NEED OTP';
    else if ($item->status == Gojek::PENDING_PIN) $status = 'NEED PIN';
    else if ($item->status == Gojek::ON_HOLD || $item->status == Gojek::ON_HOLD_PERMANENT) $status = 'ON HOLD';
    else $status = 'UNKNOWN';

    return [
      'id' => $item->id,
      'telephone' => revertTelephone($item->telephone),
      'name' => $item->name,
      'email' => $item->email,
      'va_number' => '70001-' . revertTelephone($item->telephone),
      'current_balance' => $this->currencyHelper->formatPrice($item->current_balance),
      'monthly_cash_in' => $this->currencyHelper->formatPrice($item->monthly_cash_in),
      'last_error_message' => $item->status != Gojek::CONNECTED ? $item->last_error_message : '',
      'status' => $status
    ];
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getAccounts(PipelineListener $listener, $data) {
    if (isset($data['store_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
      if (!$isValid) return $listener->response(400, $message);
    }
    else if (!isAdmin($data))
      return $listener->response(400, 'Store ID is needed');
    else $data['store_id'] = '';

    $users = [];

    $this->gojekUsers->normalizeFilters($data);

    foreach ($this->gojekUsers->getAccounts($data['store_id']) as $item) {
      $user = $this->_transforms($item, $this->gojekUsers);
      if ($data['store_id'] == '')
        $user['store'] = $item->store ? [
          'id' => $item->store->id,
          'name' => $item->store->name
        ] : null;
      $users[] = $user;
    }

    if (count($users) > 0)
      return $listener->response(200, array_merge(
        ["gojek_users" => $this->_extends($users, $this->gojekUsers)],
        $this->gojekUsers->getPagination()
      ));
    return $listener->response(204, ['gojek_users' => []]);
  }

  public function getSummaries(PipelineListener $listener, $data) {
    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $isAccountGojek = false;
    $anyActiveAccount = false;
    foreach ($pulsaOdeos->getModel()->where('category', PulsaCode::GOJEK)
               ->where('owner_store_id', $data['store_id'])->get() as $item) {
      $isAccountGojek = true;
      if ($item->is_active && strpos($item->name, 'GoPay') !== false)
        $anyActiveAccount = true;
    }

    return $listener->response(200, [
      'notes' => $isAccountGojek && !$anyActiveAccount ? 'Akun Gojek Anda sedang dimatikan oleh sistem ODEO karena kegagalan transaksi lebih dari 20 kali.
        Untuk mengaktifkan lagi, mohon cek menu Manage Inventory/Atur Produk dan pastikan tombol dalam posisi ON' : ''
    ]);

  }

}