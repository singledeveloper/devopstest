<?php

namespace Odeo\Domains\Biller\Gojek\Scheduler;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Gojek;

class GojekRedisChecker {

  private $redis;

  public function __construct() {
    $this->redis = Redis::connection();
  }

  public function run() {

    foreach ($this->redis->hgetall(Gojek::REDIS_GOJEK) as $key => $item) {
      if (strpos($key, Gojek::REDIS_KEY_USER_TRANSACTION_LOCK) !== false) {
        $id = str_replace(Gojek::REDIS_KEY_USER_TRANSACTION_LOCK, '', $key);
        $counter = $this->redis->hget(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_COUNTER_LOCK . $id);
        if ($item != '') {
          if (!$counter) $counter = 0;
          $counter++;
          if ($counter == 2) {
            $this->redis->hdel(Gojek::REDIS_GOJEK, $key);
            $this->redis->hdel(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_COUNTER_LOCK . $id);
          }
          else $this->redis->hset(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_COUNTER_LOCK . $id, $counter);
        }
        else if ($counter) $this->redis->hdel(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_COUNTER_LOCK . $id);
      }
    }

  }

}
