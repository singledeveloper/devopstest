<?php

namespace Odeo\Domains\Biller\Gojek\Scheduler;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\Gojek;

class GojekVaInquiryChecker {

  private $gojekUsers, $bcaInquiries, $billerReplenishments, $redis;

  public function __construct() {
    $this->gojekUsers = app()->make(\Odeo\Domains\Biller\Gojek\Repository\GojekUserRepository::class);
    $this->bcaInquiries = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository::class);
    $this->billerReplenishments = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository::class);
    $this->redis = Redis::connection();
  }

  public function run() {

    if (!$lastInquiryCheckDate = $this->redis->hget(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_GOJEK_LAST_INQUIRY_CHECK_DATE))
      $lastInquiryCheckDate = Carbon::now()->toDateTimeString();

    $gojekInquiries = $this->bcaInquiries->getModel()->where('description', 'like', '%GO-PAY CUSTO%')
      ->where('created_at', '>', $lastInquiryCheckDate)->get();

    $isUpdateTime = true;
    foreach($gojekInquiries as $item) {
      if ($item->reference_type == null) {
        $cashIn = intval($item->debit - 1000);

        if ($replenishment = $this->billerReplenishments->findByAmount(118, $cashIn, BillerReplenishment::DISBURSEMENT_VENDOR_BCA_MANUAL)) {
          $item->reference_type = 'biller_replenishment_id';
          $item->reference = \json_encode(["" . $replenishment->id]);
          $this->bcaInquiries->save($item);

          $replenishment->reconciled_at = Carbon::now()->toDateTimeString();
          $this->billerReplenishments->save($replenishment);

          $temp = explode('-', $item->description);
          if ($gojekUser = $this->gojekUsers->findByTelephone(purifyTelephone(trim($temp[count($temp) - 1])))) {
            $gojekUser->monthly_cash_in += $cashIn;
            $gojekUser->current_balance += $cashIn;
            $this->gojekUsers->save($gojekUser);
          }
        }
        else $isUpdateTime = false;
      }
    }

    if ($isUpdateTime) $this->redis->hset(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_GOJEK_LAST_INQUIRY_CHECK_DATE, Carbon::now()->toDateTimeString());
  }

  public function reset() {
    $this->gojekUsers->getModel()->update(['monthly_cash_in' => 0]);
  }

}
