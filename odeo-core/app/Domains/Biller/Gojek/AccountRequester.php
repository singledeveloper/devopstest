<?php

namespace Odeo\Domains\Biller\Gojek;

use Illuminate\Support\Facades\Crypt;
use Odeo\Domains\Biller\Gojek\Helper\GojekManager;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class AccountRequester extends GojekManager {

  private $supplyValidator, $vendorSwitchers, $selector;

  public function __construct() {
    parent::__construct();
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->selector = app()->make(AccountSelector::class);
  }

  public function requestOtp(PipelineListener $listener, $data) {
    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $storeId = isAdmin($data) ? null : $data['store_id'];
    $gojekUser = $this->gojekUsers->findByTelephone(purifyTelephone($data['telephone']));
    if ($gojekUser && $gojekUser->store_id != $storeId) {
      return $listener->response(400, 'Nomor ini sudah pernah digunakan oleh toko lain.');
    }

    if (!$gojekUser) {
      $gojekUser = $this->gojekUsers->getNew();
      $gojekUser->store_id = $storeId;
      $gojekUser->telephone = purifyTelephone($data['telephone']);
      $gojekUser->client_secret = $this->getClientSecret();
      $gojekUser->unique_id = Gojek::generateUniqueId();
      $gojekUser->user_agent = \Campo\UserAgent::random([
        'os_type' => ['Android'],
        'device_type' => ['Mobile', 'Tablet']
      ]);
      $gojekUser->status = Gojek::PENDING_OTP;
      $gojekUser->login_token = '-';
      $gojekUser->proxy_session_name = randomStr(10);
      $this->gojekUsers->save($gojekUser);
    }

    if (isProduction()) {
      $this->setUniqueId($gojekUser->unique_id);
      $this->setCurrentId($gojekUser->id);
      $response = $this->request('POST', '/v4/customers/login_with_phone', [
        'phone' => revertTelephone($data['telephone'])
      ]);
      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);
    }

    $gojekUser->login_token = isset($response) ? $response->data->login_token : '';
    $this->gojekUsers->save($gojekUser);

    return $listener->response(200, $this->selector->_transforms($gojekUser, $this->gojekUsers));
  }

  public function generateToken(PipelineListener $listener, $data) {
    if (!$gojekUser = $this->gojekUsers->findById($data['gojek_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($gojekUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    if (isProduction()) {
      $this->setCurrentId($gojekUser->id);
      $this->setUniqueId($gojekUser->unique_id);
      $response = $this->request('POST', '/v4/customers/login/verify', [
        'client_secret' => $gojekUser->client_secret,
        'client_name' => Gojek::LOGIN_CLIENT_ID,
        'scopes' => Gojek::LOGIN_SCOPES,
        'data' => [
          'otp' => $data['otp'],
          'otp_token' => $gojekUser->login_token
        ],
        'grant_type' => Gojek::LOGIN_GRANT_TYPE
      ]);

      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);
    }

    $gojekUser->token = isset($response) ? $response->data->access_token : '';
    $gojekUser->refresh_token = isset($response) ? $response->data->refresh_token : '';
    $gojekUser->status = $gojekUser->pin == null ? Gojek::PENDING_PIN : Gojek::CONNECTED;
    $this->gojekUsers->save($gojekUser);

    return $listener->response(200, $this->selector->_transforms($gojekUser, $this->gojekUsers));
  }

  public function setupPin(PipelineListener $listener, $data) {
    if (!$gojekUser = $this->gojekUsers->findById($data['gojek_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($gojekUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $this->setCurrentId($data['gojek_user_id']);
    $this->setToken($gojekUser->token);

    if (isProduction()) {
      $this->setUniqueId($gojekUser->unique_id);
      $response = $this->request('GET', '/wallet/profile/detailed');
      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

      if (!$response->data->preferences->p2p->enabled)
        return $listener->response(400, 'Anda belum melakukan upload KTP di apps Gojek');
      if (!$response->data->pin_setup)
        return $listener->response(400, 'Anda belum melakukan set pin di apps Gojek');
    }

    $pin = isset($data['pin']) && trim($data['pin']) != '' ? trim($data['pin']) : '';
    if ($pin == '' && $gojekUser->pin == null)
      return $listener->response(400, 'Mohon input pin Anda.');
    else if ($pin == '123456')
      return $listener->response(400, 'Sistem Gojek tidak memperbolehkan kombinasi pin yang Anda input. Mohon gunakan kombinasi lain.');
    if ($pin != '') $gojekUser->pin = Crypt::encrypt($pin);
    $gojekUser->current_balance = isset($response) ? $response->data->balance : 0;
    $gojekUser->status = Gojek::CONNECTED;
    $this->gojekUsers->save($gojekUser);

    if (!$this->vendorSwitchers->findByStatus(SwitcherConfig::BILLER_STATUS_HIDDEN_GOJEK, $gojekUser->store_id)) {
      $biller = $this->vendorSwitchers->getNew();
      $biller->vendor_id = 1;
      $biller->name = 'GOJEK-' . $gojekUser->store_id;
      $biller->is_active = true;
      $biller->minimum_topup = BillerReplenishment::DEFAULT_MINIMAL_DEPOSIT;
      $biller->status = SwitcherConfig::BILLER_STATUS_HIDDEN_GOJEK;
      $biller->owner_store_id = $gojekUser->store_id;
      $biller->is_auto_replenish = false;
      $this->vendorSwitchers->save($biller);
    }

    return $listener->response(200, $this->selector->_transforms($gojekUser, $this->gojekUsers));
  }

  public function resetUniqueId(PipelineListener $listener, $data) {
    if (!$gojekUser = $this->gojekUsers->findById($data['gojek_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($gojekUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $gojekUser->unique_id = Gojek::generateUniqueId();
    $this->gojekUsers->save($gojekUser);

    return $listener->response(200, $this->selector->_transforms($gojekUser, $this->gojekUsers));
  }

  public function checkBalance(PipelineListener $listener, $data) {
    if (!$gojekUser = $this->gojekUsers->findById($data['gojek_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($gojekUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    if (isProduction()) {
      $this->setCurrentId($data['gojek_user_id']);
      $this->setToken($gojekUser->token);
      $this->setUniqueId($gojekUser->unique_id);

      $response = $this->request('GET', '/wallet/profile/detailed');
      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

      $balanceBefore = $gojekUser->current_balance;

      $gojekUser->current_balance = $response->data->balance;
      $gojekUser->name = $response->data->name;

      if (!$response->data->preferences->p2p->enabled) {
        $gojekUser->last_error_message = 'KYC di akun ini tiba-tiba ditolak. Mohon cek kembali dan lakukan upload ulang.';
        $gojekUser->status = Gojek::PENDING_PIN;
      }
      else if ($gojekUser->current_balance > $balanceBefore) {
        if ($biller = $this->vendorSwitchers->findByStatus(SwitcherConfig::BILLER_STATUS_HIDDEN_GOJEK, $gojekUser->store_id)) {
          $pulsaInventoryRepo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
          $pulsaInventoryRepo->updateActiveAllSeeded($biller->id);
        }
      }
    }
    else $gojekUser->current_balance = rand(1000000, 9999999);
    $this->gojekUsers->save($gojekUser);

    if ($gojekUser->current_balance <= 10000) {
      $response = $this->requestWithRetry('GET', '/wallet/qr-code?phone_number=%2B6285267878089');
      try {
        $qrId = $response->data->qr_id;
        $this->requestWithRetry('POST', '/v2/fund/transfer', [
          'qr_id' => $qrId,
          'amount' => $gojekUser->current_balance + 100,
          'description' => ''
        ], [
          'pin' => Crypt::decrypt($gojekUser->pin)
        ]);
        if (strpos($this->errorMessage, ': 403') !== false) {
          $this->errorMessage = 'System detect that you can\'t transfer using this account.';
          $gojekUser->status = Gojek::ON_HOLD_PERMANENT;
          $this->gojekUsers->save($gojekUser);
        }
        $transferErrorMessage = $this->errorMessage;
      }
      catch(\Exception $e) {
        $transferErrorMessage = 'Error, can\'t check transfer status. Please try again. Log: ' . $e->getMessage();
      }
    }

    $result = $this->selector->_transforms($gojekUser, $this->gojekUsers);
    if (isset($response)) $result['gojek_response'] = $response;
    if (isset($transferErrorMessage)) $result['transfer_error_message'] = $transferErrorMessage;

    return $listener->response(200, $result);

  }

  public function changeTelephone(PipelineListener $listener, $data) {
    if (!$gojekUser = $this->gojekUsers->findById($data['gojek_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($gojekUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    if (isProduction()) {
      $this->setCurrentId($data['gojek_user_id']);
      $this->setToken($gojekUser->token);
      $this->setUniqueId($gojekUser->unique_id);

      $this->requestWithRetry('PATCH', '/v4/customers', [
        'phone' => $data['new_telephone']
      ], [
        'pin' => Crypt::decrypt($gojekUser->pin)
      ]);

      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);
    }
    return $listener->response(200, $this->selector->_transforms($gojekUser, $this->gojekUsers));
  }

  public function verifyChangeTelephone(PipelineListener $listener, $data) {
    if (!$gojekUser = $this->gojekUsers->findById($data['gojek_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($gojekUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    if (isProduction()) {
      $this->setCurrentId($data['gojek_user_id']);
      $this->setToken($gojekUser->token);
      $this->setUniqueId($gojekUser->unique_id);

      $response = $this->requestWithRetry('GET', '/gojek/v2/customer');
      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

      $this->requestWithRetry('POST', '/gojek/v2/customer/verificationUpdateProfile', [
        'id' => $response->customer->id,
        'phone' => $data['new_telephone'],
        'verificationCode' => $data['vcode']
      ]);

      if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);
    }

    $newGojekUser = $gojekUser->replicate();
    $newGojekUser->telephone = purifyTelephone($data['new_telephone']);
    $this->gojekUsers->save($newGojekUser);

    $gojekUser->is_active = false;
    $gojekUser->token = null;
    $gojekUser->refresh_token = null;
    $gojekUser->status = Gojek::REMOVED;
    $gojekUser->last_error_message = 'Changed to ' . purifyTelephone($data['new_telephone']) . ' by [' . $data['auth']['user_id'] . ']';
    $this->gojekUsers->save($gojekUser);

    return $listener->response(200, $this->selector->_transforms($newGojekUser, $this->gojekUsers));
  }

  public function remove(PipelineListener $listener, $data) {
    if (!$gojekUser = $this->gojekUsers->findById($data['gojek_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($gojekUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $gojekUser->is_active = false;
    $gojekUser->token = null;
    $gojekUser->refresh_token = null;
    $gojekUser->status = Gojek::REMOVED;
    $this->gojekUsers->save($gojekUser);

    return $listener->response(200);
  }

  public function updatePin(PipelineListener $listener, $data) {
    if (!$gojekUser = $this->gojekUsers->findById($data['gojek_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($gojekUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $this->setCurrentId($data['gojek_user_id']);
    $this->setToken($gojekUser->token);
    $this->setUniqueId($gojekUser->unique_id);

    $response = $this->requestWithRetry('PUT', '/wallet/update-pin', [
      'new_pin' => $data['new_pin']
    ], [
      'pin' => Crypt::decrypt($gojekUser->pin)
    ]);

    if ($this->errorMessage == '') {
      $gojekUser->pin = Crypt::encrypt($data['new_pin']);
      $this->gojekUsers->save($gojekUser);
    }
    else return $listener->response(400, $this->errorMessage);

    return $listener->response(200, $response);

    /*if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

    $gojekUser->pin = Crypt::encrypt($data['new_pin']);
    $this->gojekUsers->save($gojekUser);

    return $listener->response(200);*/

  }

  public function test(PipelineListener $listener, $data) {
    if (!$gojekUser = $this->gojekUsers->findById($data['gojek_user_id'])) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($gojekUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $this->setCurrentId($data['gojek_user_id']);
    $this->setToken($gojekUser->token);
    $this->setUniqueId($gojekUser->unique_id);

    $response = $this->requestWithRetry('GET', '/gojek/v2/customer');

    if (isset($response->customer)) {
      $gojekUser->name = $response->customer->name;
      $gojekUser->email = $response->customer->email;

      $this->gojekUsers->save($gojekUser);
    }

    //$response = $this->request('GET', '/wallet/profile/detailed');

    //if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);
    return $listener->response(200, $response);

  }

}

