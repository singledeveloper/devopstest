<?php

namespace Odeo\Domains\Biller\Gojek;

use Carbon\Carbon;
use Odeo\Domains\Biller\Gojek\Helper\GojekManager;
use Odeo\Domains\Core\PipelineListener;

class HistoryScrapper extends GojekManager {

  private $supplyValidator;

  public function __construct() {
    parent::__construct();
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
  }

  public function getHistories(PipelineListener $listener, $data) {

    if (!isProduction()) return $listener->response(200, ['gojek_histories' => []]);

    if (!isset($data['limit'])) $data['limit'] = 20;
    if (isset($data['offset'])) $data['page'] = ceil($data['offset'] / $data['limit']) + 1;
    else if (!isset($data['page'])) $data['page'] = 1;

    if (isset($data['search'])) $search = $data['search'];
    else $search = $data;

    if (!isset($search['telephone'])) return $listener->response(400, 'Telephone required');
    if (!$gojekUser = $this->gojekUsers->findByTelephone(purifyTelephone($search['telephone']))) {
      return $listener->response(400, 'User not exist');
    }

    list ($isValid, $message) = $this->supplyValidator->checkStore($gojekUser->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $this->setCurrentId($gojekUser->id);
    $this->setToken($gojekUser->token);
    $this->setUniqueId($gojekUser->unique_id);

    $response = $this->request('GET', '/wallet/history?page=' . $data['page'] . '&limit=' . $data['limit']);
    if ($this->errorMessage != '') return $listener->response(400, $this->errorMessage);

    $gojekHistories = [];
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    foreach (['in_process', 'success'] as $keyword) {
      if (isset($response->data->$keyword)) {
        foreach ($response->data->$keyword as $item) {
          $gojekHistories[] = [
            'transaction_date' => Carbon::parse($item->transacted_at)->addHours(7)->toDateTimeString(),
            'status' => $item->status . '/' . strtoupper(str_replace('_', ' ', $keyword)),
            'description' => str_replace('+62', '0', $item->description),
            'amount' => $currencyHelper->formatPrice($item->amount),
            'transaction_ref' => strtoupper($item->transaction_ref),
            'remaining_deposit' => isset($item->effective_balance_after_transaction) ?
              $currencyHelper->formatPrice($item->effective_balance_after_transaction) : null
          ];
        }
      }
    }

    return $listener->response(200, [
      'gojek_histories' => $gojekHistories,
      'metadata' => [
        'resultset' => [
          'limit' => 20,
          'offset' => ($data['page'] - 1) * $data['limit'],
          'count' => ($data['page'] * $data['limit']) + (isset($response->next_page) && $response->next_page != '' ? $data['limit'] : 0)
        ]
      ]
    ]);

  }

}

