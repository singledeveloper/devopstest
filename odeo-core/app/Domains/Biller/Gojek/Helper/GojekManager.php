<?php

namespace Odeo\Domains\Biller\Gojek\Helper;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Biller\Gojek\Jobs\SendIPBanned;
use Odeo\Domains\Biller\Gojek\Jobs\UpdateGojekUser;
use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SwitcherConfig;

class GojekManager {

  const BASE_URL = 'https://api.gojekapi.com/';
  const DEFAULT_ERROR = 'Gagal. Silakan coba lagi';

  private $uniqueId = '9774d56d682e549c',
    $latLang = '-6.180495,106.824992',
    $appVersion = '3.16.1',
    $clientSecret = '83415d06-ec4e-11e6-a41b-6c40088ab51e',
    $token = '',
    $currentId = '',
    $proxyData = '',
    $proxyIP = '';

  protected $gojekUsers, $errorMessage = '', $responseStatus = '', $isDebug = false, $redis;

  public function __construct() {
    $this->gojekUsers = app()->make(\Odeo\Domains\Biller\Gojek\Repository\GojekUserRepository::class);
    $this->redis = Redis::connection();
  }

  protected function setUniqueId($uniqueId) {
    $this->uniqueId = $uniqueId;
  }

  protected function setLatLang($gojekUser, $latLang = '') {
    if ($latLang == '') {
      try {
        $client = new \GuzzleHttp\Client([
          'timeout' => 30,
          'proxy' => Pulsa::getProxyURL($gojekUser->proxy_session_name)
        ]);
        $response = $client->request('GET', 'http://lumtest.com/myip.json');
        $result = json_decode($response->getBody()->getContents());
        $this->latLang = $result->geo->latitude . ',' . $result->geo->longitude;
        $this->proxyData = $result->ip . ':' . $result->geo->city;
        $this->proxyIP = $result->ip;
      }
      catch(\Exception $e) {}
    }
    else $this->latLang = $latLang;
  }

  protected function setAppVersion($appVersion = '') {
    if ($appVersion == '') {
      if ($appVersion = $this->redis->hget(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_GOJEK_VERSION))
        $this->appVersion = $appVersion;
    }
    $this->appVersion = $appVersion;
  }

  protected function setToken($token) {
    $this->token = $token;
  }

  protected function setCurrentId($currentId) {
    $this->currentId = $currentId;
  }

  protected function getClientSecret() {
    return $this->clientSecret;
  }

  protected function getProxyData() {
    return $this->proxyData;
  }

  private function checkErrors($gojekUser, $response, $code = '') {
    clog('gojek_error', $response);
    $response = json_decode($response);
    try {
      $errors = [];
      foreach ($response->errors as $item) $errors[] = $item->message;
      $errors = implode(', ', $errors);

      $this->errorMessage = $errors != '' ? $errors : self::DEFAULT_ERROR;
      $this->responseStatus = SwitcherConfig::BILLER_FAIL;
    } catch (\Exception $e) {
      if (isset($response->message) && strpos($response->message, 'not allowed') !== false) {
        $this->errorMessage = $response->message . ' [' . $this->proxyIP . ']';
        $this->responseStatus = SwitcherConfig::BILLER_FAIL;
        //dispatch(new SendIPBanned($gojekUser->id, $this->proxyIP));
        dispatch(new UpdateGojekUser($gojekUser->id, [
          'proxy_session_name' => randomStr(10)
        ]));
      } else {
        $this->errorMessage = self::DEFAULT_ERROR;
        if (isset($response->message)) {
          $this->errorMessage .= ': ' . $response->message;
        }
        else if ($code != '') {
          $this->errorMessage .= ': ' . $code;
        }
        else {
          $this->errorMessage .= ': ' . json_encode($response);
        }
        $this->responseStatus = SwitcherConfig::BILLER_SUSPECT;
      }
    }
  }

  private function countRequest() {
    $this->redis->hincrby(Gojek::REDIS_GOJEK_REQUEST_COUNT, date('Ymd-Hi'), 1);
  }

  protected function request($method, $url, $json = [], $headers = [], $loop = true) {
    $gojekUser = false;
    if ($this->currentId != '') $gojekUser = $this->gojekUsers->findById($this->currentId);

    if ($gojekUser) $this->setLatLang($gojekUser);
    $this->setAppVersion();

    $defaultHeaders = [
      'Content-Type' => 'application/json',
      'X-AppVersion' => $this->appVersion,
      'X-UniqueId' => $this->uniqueId,
      'X-Location' => $this->latLang
    ];
    if ($this->token != '') $defaultHeaders['Authorization'] = 'Bearer ' . $this->token;
    if ($gojekUser && $gojekUser->user_agent != null)
      $defaultHeaders['User-Agent'] = $gojekUser->user_agent;

    try {
      $body['headers'] = array_merge($defaultHeaders, $headers);
      if (count($json) > 0) $body['json'] = $json;
      $params = [
        'timeout' => 30,
        'proxy' => Pulsa::getProxyURL($gojekUser->proxy_session_name),
      ];
      if ($this->isDebug) $params['debug'] = true;
      $client = new \GuzzleHttp\Client($params);

      $this->countRequest();
      $response = $client->request($method, self::BASE_URL . ltrim($url, '/'), $body);
      $this->responseStatus = SwitcherConfig::BILLER_SUCCESS;
      $content = json_decode($response->getBody()->getContents());

      if ($gojekUser && $gojekUser->last_error_message != null) {
        $gojekUser->last_error_message = null;
        $this->gojekUsers->save($gojekUser);
      }
      return $content;
    } catch (\GuzzleHttp\Exception\ClientException $e) {
      if ($loop && $gojekUser && $e->getCode() == '401') {
        $this->token = '';
        $this->errorMessage = '';
        $this->responseStatus = '';
        $response = $this->request('POST', '/v3/auth/refreshtoken', [
          'refresh_token' => $gojekUser->refresh_token,
          'client_secret' => $gojekUser->client_secret,
          'grant_type' => Gojek::LOGIN_GRANT_TYPE,
          'client_id' => Gojek::LOGIN_CLIENT_ID
        ], [], false);
        if ($this->errorMessage == '') {
          $gojekUser->token = $response->data->access_token;
          $gojekUser->refresh_token = $response->data->refresh_token;

          $this->redis->hset(Gojek::REDIS_GOJEK_TOKEN_CACHE, $gojekUser->id, $gojekUser->token . ';' . $gojekUser->refresh_token);

          dispatch(new UpdateGojekUser($gojekUser->id, [
            'token' => $gojekUser->token,
            'refresh_token' => $gojekUser->refresh_token
          ]));

          $this->gojekUsers->save($gojekUser);

          $this->token = $gojekUser->token;
          return $this->request($method, $url, $json, $headers, false);
        } else {
          $this->errorMessage = 'Kemungkinan harus ulangi login dengan OTP';
          $this->responseStatus = SwitcherConfig::BILLER_FAIL;
          return false;
        }
      } else $this->checkErrors($gojekUser, $e->getResponse()->getBody()->getContents(), $e->getCode());
    } catch (\GuzzleHttp\Exception\ServerException $e) {
      clog('gojek_error', $e->getMessage());
      $this->checkErrors($gojekUser, $e->getResponse()->getBody()->getContents(), $e->getCode());

      $this->responseStatus = SwitcherConfig::BILLER_SUSPECT;
      $this->errorMessage = 'Timeout. ' . $this->errorMessage;

      if ($url == '/v3/auth/refreshtoken' && $gojekUser) {
        $statusCode = $e->getResponse()->getStatusCode();
        clog('gojek_error', 'REFRESH TOKEN ERROR ABOVE . ' . $statusCode . ', ID: ' . $gojekUser->id);
        if ($statusCode == '504') {
          dispatch(new UpdateGojekUser($gojekUser->id, [
            'status' => Gojek::PENDING_OTP,
            'last_error_message' => 'Login ulang dengan OTP'
          ]));
        }
      }
    } catch (\Exception $e) {
      $this->errorMessage = $e->getMessage();
      if (strpos($this->errorMessage, 'Failed to connect to'))
        $this->responseStatus = SwitcherConfig::BILLER_FAIL;
      else $this->responseStatus = SwitcherConfig::BILLER_SUSPECT;
    }

    if ($this->errorMessage != '') {
      $this->errorMessage = 'Gojek error: ' . $this->errorMessage;
      if ($gojekUser) {
        $params = ['last_error_message' => $this->errorMessage];
        if (strpos($this->errorMessage, 'incorrect PIN') !== false ||
          strpos($this->errorMessage, 'Upgrade your') !== false)
          $params['status'] = Gojek::PENDING_PIN;
        else if (strpos($this->errorMessage, 'activate your account') !== false)
          $params['status'] = Gojek::ON_HOLD;
        else if (strpos($this->errorMessage, 'Permintaan Anda ditolak') !== false)
          $params['unique_id'] = Gojek::generateUniqueId();
        else if (strpos($this->errorMessage, 'Gagal. Silakan coba') !== false) {
          $this->responseStatus = SwitcherConfig::BILLER_FAIL;
          if ($this->redis->hincrby(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_HOLD_PERMANENT . $gojekUser->id, 1) >= 10) {
            $this->redis->hdel(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_HOLD_PERMANENT . $gojekUser->id);
            $params['status'] = Gojek::ON_HOLD_PERMANENT;
            $internalNoticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
            $internalNoticer->saveMessage(revertTelephone($gojekUser->telephone) . ' di-disable dari sistem karena kegagalan purchase, withdraw secepatnya jika masih ada sisa deposit.');
          }
        }
        dispatch(new UpdateGojekUser($gojekUser->id, $params));
      }
    }

    return false;
  }

  protected function requestWithRetry($method, $url, $json = [], $headers = [], $loopLimit = 3) {
    $loop = 0;
    $isTryAgain = true;
    do {
      $this->errorMessage = '';
      $response = $this->request($method, $url, $json, $headers);
      if ($this->errorMessage != '') {
        if (strpos($this->errorMessage, 'Could not resolve host') === false || $loop >= $loopLimit)
          $isTryAgain = false;
        $loop++;
      } else $isTryAgain = false;
    } while ($isTryAgain);

    return $response;
  }


}

