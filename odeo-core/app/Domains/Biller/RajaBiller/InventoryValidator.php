<?php

namespace Odeo\Domains\Biller\RajaBiller;

use Odeo\Domains\Core\PipelineListener;

class InventoryValidator {


  private $pulsaInventories, $marginFormatter;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
  }

  public function validateInventory(PipelineListener $listener, $data) {

    $inventory = $this->pulsaInventories->findById($data['inventory_id']);
    $pulsa = $inventory->pulsa;
    $formatted = $this->marginFormatter->formatMargin($pulsa->price, $data);

    return $listener->response(200, array_merge($formatted, [
      'inventory_id' => $inventory->id,
      'name' => $pulsa->name
    ]));
  }
}
