<?php

namespace Odeo\Domains\Biller\Eratel;

use Illuminate\Support\Facades\Crypt;
use Odeo\Domains\Biller\Contract\ReplenishmentContract;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class Replenisher implements ReplenishmentContract {

  private $loginner, $connections;

  const REPLENISHMENT_URL = 'https://report.narindo.com/topup.php?dl=1';

  public function __construct() {
    $this->loginner = app()->make(\Odeo\Domains\Biller\Eratel\Helper\Loginner::class);
    $this->connections = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherConnectionRepository::class);
  }

  public function request(PipelineListener $listener, $data) {

    if (!BcaDisbursement::isOperational()) {
      if (isset($data['in_pipeline'])) return $listener->response(200, [
        'amount' => $data['amount'],
        'status' => BillerReplenishment::PENDING_PROGRESS_AUTO_TRANSFER
      ]);
      return [
        'amount' => $data['amount'],
        'status' => BillerReplenishment::PENDING_PROGRESS_AUTO_TRANSFER
      ];
    }

    $connection = $this->connections->findByVendorSwitcherId(SwitcherConfig::ERATEL); // DEFAULT ODEO'S ERATEL
    $client = $this->loginner->login($connection->server_user_id, $connection->server_user_name, Crypt::decrypt($connection->server_password));

    $client->setHeader('Referer', self::REPLENISHMENT_URL);
    $crawler = $client->request('POST', self::REPLENISHMENT_URL, [
      'depamt' => $data['amount']
    ]);

    $elements = $crawler->filter('.amtblue')->each(function($node){
      return $node->text();
    });

    if (isset($elements[0])) {
      $amount = str_replace(['Rp.', ' ', '.', ','], '', $elements[0]);
      if (isset($data['in_pipeline'])) return $listener->response(200, ['amount' => $amount]);
      return ['amount' => $amount];
    }
    else {
      $elements = $crawler->filter('center')->each(function($node){
        return $node->text();
      });

      if (isset($data['in_pipeline'])) return $listener->response(400, $elements[0]);
      return ['is_error' => true];
    }
  }

  public function complete(PipelineListener $listener, $data) {
    return [];
  }
}