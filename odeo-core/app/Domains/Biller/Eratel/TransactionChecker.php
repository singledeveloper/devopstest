<?php

namespace Odeo\Domains\Biller\Eratel;

use Odeo\Domains\Biller\Eratel\Helper\Loginner;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Helper\BillerStatusUpdater;

class TransactionChecker extends BillerStatusUpdater {

  private $loginner;

  const TRANSACTION_LIST_AJAX_URL = 'https://report.narindo.com/ajax/trx_list_ajax.php';

  public function __construct() {
    parent::__construct();
    $this->loginner = app()->make(Loginner::class);
  }

  public function check(PipelineListener $listener, $data) {

    $this->setConnectionData($data);

    $recons = $this->reconRepository->getByIds($this->getReconIds());
    if (count($recons)) {
      $client = $this->loginner->login($this->getUserId(), $this->getUserName(), $this->getPassword());

      foreach ($recons as $item) {

        $this->currentRecon = $item;
        $this->currentSwitcher = $this->currentRecon->switcher;

        try {
          $crawler = $client->request('POST', self::TRANSACTION_LIST_AJAX_URL, [
            'txtKeyword' => $this->currentRecon->id,
            'daterange' => '',
            'pageno' => '',
            'entries' => '',
            'agentid' => $this->getUserId(),
            'productId' => '',
            'st' => '',
            'sb' => 'PI' // txtKeyword Code for Partner ID
          ]);

          $this->currentRecon->notify = $crawler->html();
          $td = $crawler->filter('td');

          $status = strtolower($td->getNode(11)->textContent);

          $this->statusBefore = $this->currentRecon->status;

          switch ($status) {
            case 'success':
              $this->currentRecon->status = SwitcherConfig::BILLER_SUCCESS;
              $this->currentRecon->biller_transaction_id = $td->getNode(0)->textContent;
              $this->currentSwitcher->current_base_price = preg_replace("/[^0-9]/", "", $td->getNode(9)->textContent);
              $this->currentSwitcher->serial_number = explode('"', $td->getNode(12)->textContent)[0];
              break;
            case 'fail':
              $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
              break;
            case 'pending':
            case 'waiting':
              $this->currentRecon->status = SwitcherConfig::BILLER_IN_QUEUE;
              break;
            default:
              $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;
              break;
          }
          $this->supplyFinalize($listener);

          $client->back();
        }
        catch (\Exception $e) {
          clog('eratel_crawl_error', 'switcher ' . $this->currentSwitcher->id . ': ' . $e->getMessage());
        }
      }
    }
  }

}

