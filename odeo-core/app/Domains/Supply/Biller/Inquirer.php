<?php

namespace Odeo\Domains\Supply\Biller;

use Illuminate\Support\Facades\Crypt;
use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Helper\BillerStatusUpdater;
use Odeo\Domains\Supply\Jobs\ContinuePurchasePostpaid;

class Inquirer extends BillerStatusUpdater {

  const DEFAULT_ERROR = 'Sedang dalam gangguan. Mohon dicoba beberapa saat lagi.';

  private $client, $inquiryHistories, $history, $postpaidManager;

  public function __construct() {
    parent::__construct();
    $this->client = app()->make(\Odeo\Domains\Supply\Formatter\ClientParser::class);
    $this->inquiryHistories = app()->make(\Odeo\Domains\Supply\Repository\PostpaidInquiryHistoryRepository::class);
    $this->postpaidManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PostpaidDataManager::class);
  }

  public function format(PipelineListener $listener, $data, $inquiry) {
    $inventory = $inquiry->pulsaInventory;
    $pulsa = $inventory->pulsa;
    $snManager = $this->inquiryFormatter->setPath($pulsa->service_detail_id);
    if ($snManager != '') {
      $result = json_decode($inquiry->result, true);
      if (!$result) return $listener->response(400, 'Data error. Mohon kontak @odeo_cs');
      $translateResult = array_merge($result, [Supplier::SF_PARAM_NUMBER => $inquiry->number]);
      $formatterData = [
        'store_id' => $inquiry->seller_store_id,
        'user_id' => $inquiry->user_id
      ];
      if (isset($data['for_bill'])) $formatterData['for_bill'] = true;
      if (isset($data['for_affiliate'])) $formatterData['for_affiliate'] = true;
      $jsonInquiry = $snManager->toJsonInquiry($translateResult);
      if (!isset($jsonInquiry['biller_price']) || (isset($jsonInquiry['biller_price']) && $jsonInquiry['biller_price'] <= 0)) {
        $noticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
        $noticer->saveMessage('Ada parsingan error di ' . $pulsa->name . ' dari biller ' .
          $inventory->vendorSwitcher->name . '. Tagihan yang muncul seharga Rp.0');
        return $listener->response(400, 'Data error. Mohon kontak @odeo_cs');
      }
      return $listener->response(200, $this->postpaidManager->formatInquiry($formatterData, $jsonInquiry, $inventory));
    }
    return $listener->response(400, 'Data error. Mohon kontak @odeo_cs');
  }

  public function inquiry(PipelineListener $listener, $data) {
    $data['number'] = isset($data['item_detail']['number']) ? $data['item_detail']['number'] : $data['number'];

    if (!isset($data['order_detail_pulsa_switcher_id']) &&
      $this->history = $this->inquiryHistories->findRecentInquiry($data['number'], $data['auth']['user_id'],
        $data['inventory_id'], Supplier::POSTPAID_INQ_TYPE_CHECK, 10, true)) {
      return $this->waitInquiry($listener, $data);
      // return $this->check($listener, $data);
    } else if ($connection = $this->vendorSwitcherConnections->findCacheByVendorSwitcherId($data['vendor_switcher_id'])) {
      $template = $connection->template;
      $pulsaInventory = $this->pulsaInventoryRepository->findById($data['inventory_id']);
      $pulsa = $pulsaInventory->pulsa;

      $this->history = $this->inquiryHistories->getNew();
      if (isset($data['store_id'])) $this->history->seller_store_id = $data['store_id'];
      $this->history->user_id = isset($data['user_id']) ? $data['user_id'] : $data['auth']['user_id'];
      $this->history->inventory_id = $pulsaInventory->id;
      if (isset($data['order_detail_pulsa_switcher_id'])) {
        $this->history->reference_id = $data['order_detail_pulsa_switcher_id'];
        $this->history->reference_type = Supplier::POSTPAID_INQ_TYPE_PRE_PURCHASE;
      } else $this->history->reference_type = Supplier::POSTPAID_INQ_TYPE_CHECK;
      $this->history->number = $data['number'];
      $this->history->status = SwitcherConfig::BILLER_CREATED;

      $this->inquiryHistories->save($this->history);

      $dataToTranslate = [
        'user_id' => $connection->server_user_id,
        'password' => $connection->server_password ? Crypt::decrypt($connection->server_password) : null,
        'pin' => $connection->server_pin ? Crypt::decrypt($connection->server_pin) : null,
        'token' => $connection->server_token ? Crypt::decrypt($connection->server_token) : null,
        'denom' => explode('.', $pulsaInventory->code)[0],
        'service_detail_id' => $pulsa->service_detail_id,
        'number' => $data['number'],
        'id' => Supplier::POSTPAID_INQ_PREFIX . $this->history->id
      ];

      $encryptedBody = [];
      $body = json_decode($template->body_details, true);
      $headers = json_decode($template->headers, true);
      if (!$headers) $headers = [];

      foreach ($body as $key => $value) {
        $body[$key] = $this->translateRequest($value, $dataToTranslate);
        $encryptedBody[$key] = $this->translateRequest($value, array_merge($dataToTranslate, ['encrypt' => true]));
      }
      foreach ($headers as $key => $value) $headers[$key] = $this->translateRequest($value, $dataToTranslate);

      $body = $this->typeFormatter->setPath($template->format_type)->constructRequest($body, $template->body_title);
      $encryptedBody = $this->typeFormatter->setPath($template->format_type)->constructRequest($encryptedBody, $template->body_title);

      $this->history->request = is_array($encryptedBody) ? http_build_query($encryptedBody) : $encryptedBody;

      list($isValid, $response) = $this->client->parse($template->format_type, $connection->server_destination, $body, $headers);

      if (!$isValid) {
        $this->history->response = $response;
        $this->history->status = SwitcherConfig::BILLER_FAIL;
      } else if ($response) {
        $this->history->response = $response;
        foreach ($this->vendorSwitcherTemplateResponseDetails->getResponseList($template->id,
          $data['vendor_switcher_id'], Supplier::ROUTE_RESPONSE) as $item) {
          if ($this->translateResponse($item, $response)) break;
        }

        if (!isset($this->translateResult[Supplier::SF_PARAM_STATUS]))
          $this->history->status = SwitcherConfig::BILLER_FAIL;
        else $this->history->status = $this->translateResult[Supplier::SF_PARAM_STATUS];

      } else {
        $this->history->error_message = self::DEFAULT_ERROR;
        $this->history->status = SwitcherConfig::BILLER_FAIL;
      }

      $this->translateResult[Supplier::SF_PARAM_PRODUCT_NAME] = $pulsa->name;
      if (isset($this->translateResult[Supplier::SF_PARAM_ERROR]))
        $this->history->error_message = $this->translateResult[Supplier::SF_PARAM_ERROR];
      else if ($this->history->status == SwitcherConfig::BILLER_FAIL)
        $this->history->error_message = self::DEFAULT_ERROR;
      $this->history->result = json_encode($this->translateResult);
      $this->inquiryHistories->save($this->history);

      if (isset($data['order_detail_pulsa_switcher_id'])) return $this->history;

      if ($this->history->status == SwitcherConfig::BILLER_IN_QUEUE) {
        return $this->waitInquiry($listener, array_merge($data, ['postpaid_inquiry_id' => $this->history->id]));
        //return $listener->response(200, ['status' => 'PENDING', 'postpaid_inquiry_id' => $this->history->id]);
      } else if ($this->history->status == SwitcherConfig::BILLER_SUCCESS) {
        return $this->format($listener, $data, $this->history);
        // return $this->check($listener, $data);
      } else return $listener->response(400, $this->history->error_message);
    }

    if (isset($data['order_detail_pulsa_switcher_id'])) return false;
    return $listener->response(ErrorStatus::EMPTY_STOCK, trans('pulsa.empty_stock'));
  }

  public function check(PipelineListener $listener, $data) {
    if (isset($data['postpaid_inquiry_id'])) $this->history = $this->inquiryHistories->findById($data['postpaid_inquiry_id']);
    if ($this->history) {
      if ($this->history->user_id != $data['auth']['user_id'] && $this->history->reference_type != Supplier::POSTPAID_INQ_TYPE_CHECK)
        return $listener->response(400, 'Anda tidak dapat mengakses data ini.');
      else if ($this->history->status == SwitcherConfig::BILLER_IN_QUEUE)
        return $listener->response(200, ['status' => 'PENDING', 'postpaid_inquiry_id' => $this->history->id]);
      else if ($this->history->status == SwitcherConfig::BILLER_FAIL) {
        $this->history->is_processed = true;
        $this->inquiryHistories->save($this->history);
        return $listener->response(400, $this->history->error_message);
      } else return $this->format($listener, $data, $this->history);
    }
    return $listener->response(400, 'Data tidak valid.');
  }

  public function waitInquiry(PipelineListener $listener, $data) {
    $currentTrialTime = 2;
    $trialGapTime = 2;

    $elapsedTime = 0;
    $executionTime = 20;
    $startTime = microtime(true);

    while ($elapsedTime < $executionTime) {
      if ($elapsedTime > $currentTrialTime) {
        if (isset($data['postpaid_inquiry_id'])) $this->history = $this->inquiryHistories->findById($data['postpaid_inquiry_id']);
        if ($this->history) {
          if ($this->history->user_id != $data['auth']['user_id'] && $this->history->reference_type != Supplier::POSTPAID_INQ_TYPE_CHECK) {
            return $listener->response(400, 'Anda tidak dapat mengakses data ini.');
          }
          if ($this->history->status == SwitcherConfig::BILLER_FAIL) {
            $this->history->is_processed = true;
            $this->inquiryHistories->save($this->history);
            return $listener->response(400, $this->history->error_message);
          }
          if ($this->history->status != SwitcherConfig::BILLER_IN_QUEUE) {
            return $this->format($listener, $data, $this->history);
          }
        } else {
          return $listener->response(400, 'Data tidak valid.');
        }

        $currentTrialTime = $currentTrialTime + $trialGapTime;
      }
      $elapsedTime = microtime(true) - $startTime;
    }

    return $listener->response(400, 'Cek tagihan sedang diproses. Anda harus kembali ke halaman sebelumnya' .
      ' dan mencoba melakukan input yang sama kira-kira sekitar 2-3 menit lagi untuk menampilkan hasil cek tagihan.');
  }

  public function complete($data) {
    if ($this->history = $this->inquiryHistories->findById(
      str_replace(Supplier::POSTPAID_INQ_PREFIX, '', $data['postpaid_inquiry_id']))) {
      $this->translateResult = $data['translate_result'];
      if (!isset($this->translateResult[Supplier::SF_PARAM_PRODUCT_NAME]))
        $this->translateResult[Supplier::SF_PARAM_PRODUCT_NAME] = $this->history->pulsaInventory->pulsa->name;
      $this->history->notify = $data['notify'];
      $this->history->result = json_encode($this->translateResult);
      $this->history->status = !isset($this->translateResult[Supplier::SF_PARAM_STATUS]) ?
        SwitcherConfig::BILLER_FAIL : $this->translateResult[Supplier::SF_PARAM_STATUS];
      if (isset($this->translateResult[Supplier::SF_PARAM_ERROR]))
        $this->history->error_message = $this->translateResult[Supplier::SF_PARAM_ERROR];
      else if ($this->history->status == SwitcherConfig::BILLER_FAIL) {
        $statuses = Supplier::getStatuses();
        if (isset($this->translateResult[Supplier::SF_PARAM_RESPONSE_CODE]) &&
          isset($statuses[Supplier::STATUS_FAIL . '_' . Supplier::SF_PARAM_RESPONSE_CODE]))
          $this->history->error_message = $statuses[Supplier::STATUS_FAIL . '_' . Supplier::SF_PARAM_RESPONSE_CODE];
        else $this->history->error_message = self::DEFAULT_ERROR;
      }

      $this->inquiryHistories->save($this->history);

      if ($this->history->reference_type == Supplier::POSTPAID_INQ_TYPE_PRE_PURCHASE)
        dispatch(new ContinuePurchasePostpaid($this->history->reference_id));
      return true;
    }
    return false;
  }

  public function saveManually(PipelineListener $listener, $data) {
    $this->history = $this->inquiryHistories->getNew();
    if (isset($data['store_id'])) $this->history->seller_store_id = $data['store_id'];
    $this->history->user_id = isset($data['user_id']) ? $data['user_id'] : $data['auth']['user_id'];
    $this->history->inventory_id = $data['inventory_id'];
    if (isset($data['order_detail_pulsa_switcher_id'])) {
      $this->history->reference_type = Supplier::POSTPAID_INQ_TYPE_PRE_PURCHASE;
      $this->history->reference_id = $data['order_detail_pulsa_switcher_id'];
    } else $this->history->reference_type = Supplier::POSTPAID_INQ_TYPE_CHECK;
    $this->history->number = $data['number'];
    $this->history->status = $data['status'];
    $this->history->request = $data['request'];
    if (isset($data['response'])) $this->history->response = $data['response'];
    if (isset($data['result'])) $this->history->result = $data['result'];
    if (isset($data['error_message'])) $this->history->error_message = $data['error_message'];
    else if ($this->history->status == SwitcherConfig::BILLER_FAIL)
      $this->history->error_message = self::DEFAULT_ERROR;
    $this->inquiryHistories->save($this->history);

    return $this->history;
  }

}
