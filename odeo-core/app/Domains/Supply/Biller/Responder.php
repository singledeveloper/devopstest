<?php

namespace Odeo\Domains\Supply\Biller;

use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Helper\BillerStatusUpdater;
use Odeo\Domains\Supply\Jobs\SaveLog;

class Responder extends BillerStatusUpdater {

  public function __construct() {
    parent::__construct();
  }

  public function parse(PipelineListener $listener, $data) {

    if (isset($data['jabber_from']) && $connection = $this->vendorSwitcherConnections->findByServerDestination($data['jabber_from'], isset($data['jabber_store_id']) ? $data['jabber_store_id'] : '')) {
      $data['template_id'] = $connection->template_id;
      $data['vendor_switcher_id'] = $connection->vendor_switcher_id;
    }
    else if (!isset($data['template_id']) && isset($data['vendor_switcher_id']) && $connection = $this->vendorSwitcherConnections->findCacheByVendorSwitcherId($data['vendor_switcher_id']))
      $data['template_id'] = $connection->template_id;

    if (isset($data['template_id'])) {
      foreach ($this->vendorSwitcherTemplateResponseDetails->getResponseList($data['template_id'], $data['vendor_switcher_id'], Supplier::ROUTE_REPORT) as $item) {
        if ($this->translateResponse($item, $data['jabber_message'])) break;
      }
    }

    if (isset($this->translateResult[Supplier::SF_PARAM_TRXID]))
      $this->currentRecon = $this->reconRepository->findById($this->translateResult[Supplier::SF_PARAM_TRXID]);
    else if (isset($this->translateResult[Supplier::SF_PARAM_MSISDN]) && isset($this->translateResult[Supplier::SF_PARAM_CODE]))
      $this->currentRecon = $this->reconRepository->findByInventoryCodeAndNumber([
        'code' => trim($this->translateResult[Supplier::SF_PARAM_CODE]),
        'no' => trim($this->translateResult[Supplier::SF_PARAM_MSISDN]),
        'vendor_switcher_id' => $data['vendor_switcher_id'],
        'format_type' => Supplier::TYPE_TEXT
      ]);
    else if (isset($data['jabber_from'])) {
      $settings = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository\VendorSwitcherReplenishmentSettingRepository::class);
      if ($setting = $settings->findByData($data['jabber_from'], BillerReplenishment::SETTING_CHANNEL_JABBER)) {
        $settingData = json_decode($setting->data, true);
        if (isset($settingData[BillerReplenishment::SETTING_CHANNEL_JABBER_REPLY_SAMPLE])) {
          foreach ($settingData[BillerReplenishment::SETTING_CHANNEL_JABBER_REPLY_SAMPLE] as $item) {
            $result = $this->responseTranslator->parse($item, $data['jabber_message']);
            if (isset($result[Supplier::SF_PARAM_DEPOSIT])) {
              $jabberReplenishmentHistories = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository\VendorSwitcherReplenishmentJabberHistoryRepository::class);
              $jabberHistory = $jabberReplenishmentHistories->findByVendorSwitcherId($setting->vendor_switcher_id);
              $jabberHistory->log_reply = $data['jabber_message'];
              $jabberHistory->status = SwitcherConfig::BILLER_SUCCESS;
              $jabberReplenishmentHistories->save($jabberHistory);

              app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\ReplenishmentRequester::class)
                ->updateAmount($listener, [
                  'vendor_switcher_id' => $setting->vendor_switcher_id,
                  'amount' => $result[Supplier::SF_PARAM_DEPOSIT]
                ]);

              return $listener->response(200);
            }
          }
        }
        $data['vendor_switcher_id'] = $setting->vendor_switcher_id;
      }

      if (isset($data['vendor_switcher_id']))
        dispatch(new SaveLog($data['vendor_switcher_id'], Supplier::LOG_UNKNOWN_REPORT, $data['jabber_message']));
      else {
        $jabberManager = app()->make(\Odeo\Domains\Vendor\Jabber\JabberManager::class);
        $jabberManager->createLog($this->currentRoute, InlineJabber::TYPE_UNMATCH_PATTERN, $data['jabber_message']);
      }

      return $listener->response(400, 'Unknown report.');
    }

    if ($this->currentRecon && in_array($this->currentRecon->status, [SwitcherConfig::BILLER_IN_QUEUE, SwitcherConfig::BILLER_SUSPECT])) {
      $this->expandSwitcher($this->currentRecon->switcher);

      if ($this->currentSwitcher->vendor_switcher_id != $this->currentRecon->vendor_switcher_id) {
        dispatch(new SaveLog($data['vendor_switcher_id'], Supplier::LOG_UNMATCH_TRANSACTION, $this->currentRecon->notify != null ? $this->currentRecon->notify : $data['jabber_message']));
      }

      $this->currentRecon->notify = $data['jabber_message'];
      $this->statusBefore = $this->currentRecon->status;

      return $this->supplyFinalize($listener);
    }
    else {
      dispatch(new SaveLog($data['vendor_switcher_id'], Supplier::LOG_UNKNOWN_TRANSACTION, $data['jabber_message']));
      return $listener->response(400, 'Unknown transaction.');
    }

  }

}
