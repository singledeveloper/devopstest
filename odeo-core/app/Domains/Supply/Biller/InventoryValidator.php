<?php

namespace Odeo\Domains\Supply\Biller;

use Odeo\Domains\Core\PipelineListener;

class InventoryValidator {

  private $pulsaInventories, $marginFormatter, $pulsaH2hGroupDetails, $h2hManager;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->pulsaH2hGroupDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaH2hGroupDetailRepository::class);
    $this->h2hManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaH2HManager::class);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    if ($inventory = $this->pulsaInventories->findById($data['inventory_id'])) {
      $pulsa = $inventory->pulsa;
      $price = $pulsa->price;
      if ($this->h2hManager->isUserH2H($data['auth']['user_id']) && $detail = $this->pulsaH2hGroupDetails->findByUserId($data['auth']['user_id'], $pulsa->id)) {
        if (!$detail->is_enabled) return $listener->response(400, trans('pulsa.cant_purchase_no_backup'));

        $price = $detail->price;
        $data['is_h2h'] = true;
        $listener->replaceData($data);
      }

      $data['is_own_product'] = isset($pulsa->owner_store_id);
      $formatted = $this->marginFormatter->formatMargin($price, $data);

      return $listener->response(200, array_merge($formatted, [
        'inventory_id' => $inventory->id,
        'name' => $pulsa->name
      ]));
    }

    return $listener->response(400, trans('pulsa.cant_purchase_no_backup'));
  }
}
