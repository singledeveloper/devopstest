<?php

namespace Odeo\Domains\Supply\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class VendorSwitcherTemplateResponseDetail extends Entity
{
  public $timestamps = true;

  public function response() {
    return $this->belongsTo(VendorSwitcherTemplateResponse::class, 'template_response_id');
  }

  public function vendorSwitcher() {
    return $this->belongsTo(VendorSwitcher::class, 'predefined_vendor_switcher_id');
  }
}
