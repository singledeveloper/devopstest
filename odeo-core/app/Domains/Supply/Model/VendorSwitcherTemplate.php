<?php

namespace Odeo\Domains\Supply\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class VendorSwitcherTemplate extends Entity
{
  public $timestamps = true;
}
