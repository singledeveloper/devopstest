<?php

namespace Odeo\Domains\Supply\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Supply\Model\VendorSwitcherTemplate;

class VendorSwitcherTemplateRepository extends Repository {

  public function __construct(VendorSwitcherTemplate $vendorSwitcherTemplate) {
    $this->model = $vendorSwitcherTemplate;
  }

  public function gets($storeId) {
    $query = $this->model->where('is_active', true);

    if ($storeId == '') $query = $query->whereNull('predefined_store_id');
    else $query = $query->where(function($subquery) use ($storeId){
      $subquery->whereNull('predefined_store_id')->orWhere('predefined_store_id', $storeId);
    });

    return $query->orderBy('name', 'asc')->get();
  }

  public function getByStoreId($storeId) {
    return $this->model->where('predefined_store_id', $storeId)
      ->where('is_active', true)->orderBy('name', 'asc')->get();
  }

}
