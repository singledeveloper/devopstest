<?php

namespace Odeo\Domains\Supply\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Supply\Model\VendorSwitcherConnectionLog;

class VendorSwitcherConnectionLogRepository extends Repository {

  public function __construct(VendorSwitcherConnectionLog $vendorSwitcherConnectionLog) {
    $this->model = $vendorSwitcherConnectionLog;
  }

  public function getByVendorSwitcherId($vendorSwitcherId) {
    return $this->model->where('vendor_switcher_id', $vendorSwitcherId)
      ->orderBy('id', 'desc')->get();
  }

}
