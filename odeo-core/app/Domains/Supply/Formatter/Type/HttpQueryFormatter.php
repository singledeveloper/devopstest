<?php

namespace Odeo\Domains\Supply\Formatter\Type;

use Illuminate\Http\Request;
use Odeo\Domains\Supply\Formatter\Contract\TypeContract;

class HttpQueryFormatter implements TypeContract {

  public function constructRequest($array, $title = null) {
    return $array;
  }

  public function deconstructResponse($string) {
    if (is_string($string)) {
      parse_str($string, $output);
      return $output;
    }
    return $string;
  }

  public function getRequestData(Request $request) {
    $data = [];
    foreach ($request->input() as $key => $item) $data[$key] = $item;
    foreach ($request->file() as $key => $file) $data[$key] = $file;
    return $data;
  }

  public function constructRequestPreview($array, $title = null) {
    return http_build_query($array);
  }

}
