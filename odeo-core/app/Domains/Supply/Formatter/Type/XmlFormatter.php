<?php

namespace Odeo\Domains\Supply\Formatter\Type;

use Illuminate\Http\Request;
use Odeo\Domains\Supply\Formatter\Contract\TypeContract;

class XmlFormatter implements TypeContract {

  public function constructRequest($array, $title = null) {
    $xml = '';
    foreach($array as $key => $item) {
      $xml .= '<' . $key . '>' . $item . '</' . $key . '>';
    }
    if ($title) $xml = '<' . $title . '>' . $xml . '</' . $title . '>';
    return '<?xml version="1.0"?>' . $xml;
  }

  public function deconstructResponse($string) {
    if ($response = simplexml_load_string($string)) {
      return xmlToArray($response);
    }
    return false;
  }

  public function getRequestData(Request $request) {
    return $request->getContent();
  }

  public function constructRequestPreview($array, $title = null) {
    $raw = $this->constructRequest($array, $title);
    try {
      $dom = new \DOMDocument();

      $dom->preserveWhiteSpace = false;
      $dom->formatOutput = true;

      $dom->loadXML($raw);
      $out = $dom->saveXML();
    }
    catch (\Exception $e) {
      $out = 'XML not valid.';
    }
    return $out;
  }

}
