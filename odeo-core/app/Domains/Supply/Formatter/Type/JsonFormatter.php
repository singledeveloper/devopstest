<?php

namespace Odeo\Domains\Supply\Formatter\Type;

use Illuminate\Http\Request;
use Odeo\Domains\Supply\Formatter\Contract\TypeContract;

class JsonFormatter implements TypeContract {

  private function readable($in, $indent = 0)
  {
    $escape = function ($str) {
      return preg_replace("!([\b\t\n\r\f\"\\'])!", "\\\\\\1", $str);
    };

    $out = '';

    foreach ($in as $key => $value) {
      $out .= str_repeat("\t", $indent + 1);
      $out .= "\"" . $escape((string)$key) . "\": ";

      if (is_object($value) || is_array($value)) {
        $out .= "\n";
        $out .= $this->readable($value, $indent + 1);
      }
      else if (is_bool($value)) $out .= $value ? 'true' : 'false';
      else if (is_null($value)) $out .= 'null';
      else if (is_string($value)) $out .= "\"" . $escape($value) . "\"";
      else $out .= $value;
      $out .= ",\n";
    }

    if (!empty($out)) $out = substr($out, 0, -2);

    $out = str_repeat("\t", $indent) . "{\n" . $out;
    $out .= "\n" . str_repeat("\t", $indent) . "}";

    return $out;
  }


  public function constructRequest($array, $title = null) {
    if ($title) $array[$title] = $array;
    return json_encode($array);
  }

  public function deconstructResponse($string) {
    if (is_array($string)) return $string;
    return json_decode($string, true);
  }

  public function getRequestData(Request $request) {
    return $request->json()->all();
  }

  public function constructRequestPreview($array, $title = null) {
    return $this->readable(json_decode($this->constructRequest($array, $title)));
  }

}
