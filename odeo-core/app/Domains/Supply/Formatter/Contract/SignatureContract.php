<?php

namespace Odeo\Domains\Supply\Formatter\Contract;

interface SignatureContract {

  public function generate($data);

}