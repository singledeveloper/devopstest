<?php

namespace Odeo\Domains\Supply\Formatter\Signature;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Supply\Formatter\Contract\SignatureContract;

class KopnusGenerator implements SignatureContract {

  public function generate($data) {

    return sha1(
      str_pad($data['denom'], 7) .
      str_pad($data['number'], 20) . '01' .
      str_pad($data['user_id'], 20) .
      str_pad(md5($data['password']), 40)
    );

  }

}
