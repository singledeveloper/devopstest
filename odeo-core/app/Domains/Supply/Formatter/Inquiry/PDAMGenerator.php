<?php

namespace Odeo\Domains\Supply\Formatter\Inquiry;

use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Supply\Formatter\Contract\InquiryContract;

class PDAMGenerator implements InquiryContract {

  public function toString($data, $currentSerialNumber = '') {
    $currentSerialNumber = (isset($data[Supplier::SF_PARAM_OWNER_NAME]) ? $data[Supplier::SF_PARAM_OWNER_NAME] : '-');
    $currentSerialNumber .= '/PTAG:' . (isset($data[Supplier::SF_PARAM_POSTPAID_PERIODE]) ? $data[Supplier::SF_PARAM_POSTPAID_PERIODE] : '-');
    $currentSerialNumber .= '/JBLN:' . (isset($data[Supplier::SF_PARAM_POSTPAID_MONTH_COUNT]) ? $data[Supplier::SF_PARAM_POSTPAID_MONTH_COUNT] : '-');
    $currentSerialNumber .= '/SM:' . (isset($data[Supplier::SF_PARAM_POSTPAID_METER_CHANGES]) ? $data[Supplier::SF_PARAM_POSTPAID_METER_CHANGES] : '-');
    $currentSerialNumber .= '/TAG:' . (isset($data[Supplier::SF_PARAM_POSTPAID_PRICE]) ? $data[Supplier::SF_PARAM_POSTPAID_PRICE] : '-');
    $currentSerialNumber .= '/ADM:' . (isset($data[Supplier::SF_PARAM_POSTPAID_ADMIN]) ? $data[Supplier::SF_PARAM_POSTPAID_ADMIN] : 0);
    $currentSerialNumber .= '/TTAG:' . Supplier::checkPostpaidTotal($data);
    $currentSerialNumber .= '/REF:' . (isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-');
    return $currentSerialNumber;
  }

  private function _toJsonInquiryDetail($item) {
    return [
      'period' => isset($item[Supplier::SF_PARAM_POSTPAID_PERIODE]) ? $item[Supplier::SF_PARAM_POSTPAID_PERIODE] : '-',
      'base_price' => isset($item[Supplier::SF_PARAM_POSTPAID_PRICE]) ? $item[Supplier::SF_PARAM_POSTPAID_PRICE] : '-',
      'fine' => isset($item[Supplier::SF_PARAM_POSTPAID_FINE]) ? $item[Supplier::SF_PARAM_POSTPAID_FINE] : 0,
      'admin_fee' => isset($item[Supplier::SF_PARAM_POSTPAID_ADMIN]) ? $item[Supplier::SF_PARAM_POSTPAID_ADMIN] : 0,
      'meter_changes' => isset($item[Supplier::SF_PARAM_POSTPAID_METER_CHANGES]) ? $item[Supplier::SF_PARAM_POSTPAID_METER_CHANGES] : '-'
    ];
  }

  public function toJsonInquiry($data) {
    if (isset($data[Supplier::SF_PARAM_DETAILS_LOOP])) {
      $details = [];
      foreach ($data[Supplier::SF_PARAM_DETAILS_LOOP] as $item) $details[] = $this->_toJsonInquiryDetail($item);
    }
    else $details = [$this->_toJsonInquiryDetail($data)];

    return [
      'name' => isset($data[Supplier::SF_PARAM_PRODUCT_NAME]) ? $data[Supplier::SF_PARAM_PRODUCT_NAME] : strtoupper(PostpaidType::PDAM),
      'subscriber_id' => isset($data[Supplier::SF_PARAM_NUMBER]) ? $data[Supplier::SF_PARAM_NUMBER] : '-',
      'subscriber_name' => isset($data[Supplier::SF_PARAM_OWNER_NAME]) ? $data[Supplier::SF_PARAM_OWNER_NAME] : '-',
      'multiplier' => isset($data[Supplier::SF_PARAM_DETAILS_LOOP]) ? count($data[Supplier::SF_PARAM_DETAILS_LOOP]) : 1,
      'ref_id' => isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-',
      'biller_price' => Supplier::checkPostpaidTotal($data),
      'details' => $details
    ];
  }

  public function toOrderInquiry($item, $orderDetail) {
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $firstRow = '';
    $monthly = [];
    $grossTotal = $totalMonth = $totalAdminFee = $totalFine = $totalBasePrice = $totalBillRest = 0;
    $pdamDetails = $item && $item->pdamDetails ? $item->pdamDetails : [];
    if (count($pdamDetails) > 0) {
      $firstRow = $pdamDetails[0];
      foreach ($pdamDetails as $output) {
        $subtotal = $output->base_price + $output->fine + $output->admin_fee + $output->bill_rest;
        $monthly[] = [
          'period' => $output->period,
          'meter_changes' => $output->meter_changes,
          'price' => $currencyHelper->formatPrice($output->base_price),
          'fine' => $currencyHelper->formatPrice($output->fine),
          'bill_rest' => $currencyHelper->formatPrice($output->bill_rest),
          'admin_fee' => $currencyHelper->formatPrice($output->admin_fee),
          'subtotal' => $currencyHelper->formatPrice($subtotal)
        ];
        $totalMonth++;
        $totalAdminFee += $output->admin_fee;
        $grossTotal += $subtotal;
        $totalBasePrice += $output->base_price;
        $totalFine += $output->fine;
        $totalBillRest += $output->bill_rest;
      }
    }

    return [
      'subscriber_id' => ($firstRow != '') ? $firstRow->subscriber_id : "",
      'subscriber_name' => ($firstRow != '') ? $firstRow->subscriber_name : "",
      'monthly' => $monthly,
      'total_month' => $totalMonth,
      'total_admin_fee' => $currencyHelper->formatPrice($totalAdminFee),
      'total_fine' => $currencyHelper->formatPrice($totalFine),
      'total_bill_rest' => $currencyHelper->formatPrice($totalBillRest),
      'total_bill' => $currencyHelper->formatPrice($totalBasePrice),
      'discount' => $currencyHelper->formatPrice($item && $grossTotal != 0 ? $grossTotal - $orderDetail->sale_price : 0)
    ];
  }

  public function record($switcherId, $detail) {
    $postpaidDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPostpaidPdamDetailRepository::class);
    foreach ($detail['inquiries'] as $item) {
      $switcherDetail = $postpaidDetails->getNew();
      $switcherDetail->order_detail_pulsa_switcher_id = $switcherId;
      $switcherDetail->subscriber_id = $detail['subscriber_id'];
      $switcherDetail->subscriber_name = $detail['subscriber_name'];
      $switcherDetail->period = $item['period'];
      $switcherDetail->base_price = intval($item['base_price']);
      $switcherDetail->fine = isset($item['fine']) ? intval($item['fine']) : 0;
      $switcherDetail->bill_rest = isset($item['bill_rest']) ? intval($item['bill_rest']) : 0;
      $switcherDetail->admin_fee = intval($item['admin_fee']);
      $postpaidDetails->save($switcherDetail);
    }
  }

}
