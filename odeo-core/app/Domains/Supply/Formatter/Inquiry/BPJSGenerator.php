<?php

namespace Odeo\Domains\Supply\Formatter\Inquiry;

use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Supply\Formatter\Contract\InquiryContract;

class BPJSGenerator implements InquiryContract {

  public function toString($data, $currentSerialNumber = '') {
    $currentSerialNumber = (isset($data[Supplier::SF_PARAM_OWNER_NAME]) ? $data[Supplier::SF_PARAM_OWNER_NAME] : '-');
    $currentSerialNumber .= '/PST:' . (isset($data[Supplier::SF_PARAM_BPJS_PERSON_COUNT]) ? $data[Supplier::SF_PARAM_BPJS_PERSON_COUNT] : '-');
    $currentSerialNumber .= '/CBG:' . ((isset($data[Supplier::SF_PARAM_BPJS_BRANCH_CODE]) ? $data[Supplier::SF_PARAM_BPJS_BRANCH_CODE] : '?') . '-' .
        (isset($data[Supplier::SF_PARAM_BPJS_BRANCH_NAME]) ? $data[Supplier::SF_PARAM_BPJS_BRANCH_NAME] : '?'));
    $currentSerialNumber .= '/PTAG:' . (isset($data[Supplier::SF_PARAM_POSTPAID_PERIODE]) ? $data[Supplier::SF_PARAM_POSTPAID_PERIODE] : '-');
    $currentSerialNumber .= '/JBLN:' . (isset($data[Supplier::SF_PARAM_POSTPAID_MONTH_COUNT]) ? $data[Supplier::SF_PARAM_POSTPAID_MONTH_COUNT] : '-');
    $currentSerialNumber .= '/TAG:' . (isset($data[Supplier::SF_PARAM_POSTPAID_PRICE]) ? $data[Supplier::SF_PARAM_POSTPAID_PRICE] : '-');
    $currentSerialNumber .= '/ADM:' . (isset($data[Supplier::SF_PARAM_POSTPAID_ADMIN]) ? $data[Supplier::SF_PARAM_POSTPAID_ADMIN] : 0);
    $currentSerialNumber .= '/TTAG:' . Supplier::checkPostpaidTotal($data);
    $currentSerialNumber .= '/REF:' . (isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-');
    return $currentSerialNumber;
  }

  public function toJsonInquiry($data) {
    return [
      'name' => strtoupper(PostpaidType::BPJS_KES),
      'subscriber_id' => isset($data[Supplier::SF_PARAM_NUMBER]) ? $data[Supplier::SF_PARAM_NUMBER] : '-',
      'subscriber_name' => isset($data[Supplier::SF_PARAM_OWNER_NAME]) ? $data[Supplier::SF_PARAM_OWNER_NAME] : '-',
      'multiplier' => 1,
      'ref_id' => isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-',
      'biller_price' => Supplier::checkPostpaidTotal($data),
      'details' => [
        'branch_code' => isset($data[Supplier::SF_PARAM_BPJS_BRANCH_CODE]) ? $data[Supplier::SF_PARAM_BPJS_BRANCH_CODE] : '-',
        'branch_name' => isset($data[Supplier::SF_PARAM_BPJS_BRANCH_NAME]) ? $data[Supplier::SF_PARAM_BPJS_BRANCH_NAME] : '-',
        'month_counts' => isset($data[Supplier::SF_PARAM_POSTPAID_MONTH_COUNT]) ? $data[Supplier::SF_PARAM_POSTPAID_MONTH_COUNT] : '-',
        'participant_counts' => isset($data[Supplier::SF_PARAM_BPJS_PERSON_COUNT]) ? $data[Supplier::SF_PARAM_BPJS_PERSON_COUNT] : '-',
        'base_price' => isset($data[Supplier::SF_PARAM_POSTPAID_PRICE]) ? $data[Supplier::SF_PARAM_POSTPAID_PRICE] : '-',
        'bill_rest' => isset($data[Supplier::SF_PARAM_POSTPAID_BILL_REST]) ? $data[Supplier::SF_PARAM_POSTPAID_BILL_REST] : 0,
        'admin' => isset($data[Supplier::SF_PARAM_POSTPAID_ADMIN]) ? $data[Supplier::SF_PARAM_POSTPAID_ADMIN] : 0
      ]
    ];
  }

  public function toOrderInquiry($item, $orderDetail) {
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $bpjsDetail = $item && $item->bpjsKesDetail ? $item->bpjsKesDetail : false;

    $inquiry = [
      'subscriber_id' => $bpjsDetail ? $bpjsDetail->subscriber_id : "",
      'subscriber_name' => $bpjsDetail ? $bpjsDetail->subscriber_name : "",
      'discount' => $currencyHelper->formatPrice(0)
    ];

    if ($bpjsDetail) {
      $inquiry = array_merge($inquiry, [
        'ref_number' => ($item) ? $item->serial_number : "",
        'month_counts' => $bpjsDetail->month_counts,
        'price' => $currencyHelper->formatPrice($bpjsDetail->base_price),
        'admin_fee' => $currencyHelper->formatPrice($bpjsDetail->admin_fee),
        'discount' => $currencyHelper->formatPrice($bpjsDetail->base_price + $bpjsDetail->admin_fee - $orderDetail->sale_price)
      ]);
    }

    return $inquiry;

  }

  public function record($switcherId, $detail) {
    $postpaidDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPostpaidBpjsKesDetailRepository::class);
    $switcherDetail = $postpaidDetails->getNew();
    $switcherDetail->order_detail_pulsa_switcher_id = $switcherId;
    $switcherDetail->subscriber_id = $detail['subscriber_id'];
    $switcherDetail->subscriber_name = $detail['subscriber_name'];
    $switcherDetail->branch_code = $detail['inquiries']['branch_code'];
    $switcherDetail->branch_name = $detail['inquiries']['branch_name'];
    $switcherDetail->month_counts = $detail['inquiries']['month_counts'];
    $switcherDetail->participant_counts = $detail['inquiries']['participant_counts'];
    $switcherDetail->base_price = $detail['inquiries']['base_price'];
    $switcherDetail->bill_rest = $detail['inquiries']['bill_rest'];
    $switcherDetail->admin_fee = $detail['inquiries']['admin'];
    $postpaidDetails->save($switcherDetail);
  }

}
