<?php

namespace Odeo\Domains\Supply\Formatter\Inquiry;

use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Supply\Formatter\Contract\InquiryContract;

class MultiFinanceGenerator implements InquiryContract {

  public function toString($data, $currentSerialNumber = '') {
    return isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-';
  }

  public function toJsonInquiry($data) {
    return [
      'name' => isset($data[Supplier::SF_PARAM_MF_NAME]) ? $data[Supplier::SF_PARAM_MF_NAME] : strtoupper(PostpaidType::MULTIFINANCE),
      'subscriber_id' => isset($data[Supplier::SF_PARAM_NUMBER]) ? $data[Supplier::SF_PARAM_NUMBER] : '-',
      'subscriber_name' => isset($data[Supplier::SF_PARAM_OWNER_NAME]) ? $data[Supplier::SF_PARAM_OWNER_NAME] : '-',
      'multiplier' => 1,
      'ref_id' => isset($data[Supplier::SF_PARAM_SN]) ? $data[Supplier::SF_PARAM_SN] : '-',
      'biller_price' => Supplier::checkPostpaidTotal($data),
      'details' => [
        'installment' => isset($data[Supplier::SF_PARAM_MF_CREDIT_CURRENT]) ? ($data[Supplier::SF_PARAM_MF_CREDIT_CURRENT] .
          (isset($data[Supplier::SF_PARAM_MF_CREDIT_TOTAL]) ? ('/' . $data[Supplier::SF_PARAM_MF_CREDIT_TOTAL]) : '')) : '-',
        'due_date' => isset($data[Supplier::SF_PARAM_MF_CREDIT_DUE_DATE]) ? $data[Supplier::SF_PARAM_MF_CREDIT_DUE_DATE] : '-',
        'platform' => isset($data[Supplier::SF_PARAM_MF_PLATFORM]) ? $data[Supplier::SF_PARAM_MF_PLATFORM] : '-',
        'base_price' => isset($data[Supplier::SF_PARAM_POSTPAID_PRICE]) ? $data[Supplier::SF_PARAM_POSTPAID_PRICE] : '-',
        'coll_fee' => isset($data[Supplier::SF_PARAM_MF_COLL_FEE]) ? $data[Supplier::SF_PARAM_MF_COLL_FEE] : null,
        'fine' => isset($data[Supplier::SF_PARAM_POSTPAID_FINE]) ? $data[Supplier::SF_PARAM_POSTPAID_FINE] : 0,
        'admin' => isset($data[Supplier::SF_PARAM_POSTPAID_ADMIN]) ? $data[Supplier::SF_PARAM_POSTPAID_ADMIN] : 0
      ]
    ];
  }

  public function toOrderInquiry($item, $orderDetail) {
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $multiFinanceDetail = $item && $item->multiFinanceDetail ? $item->multiFinanceDetail : false;
    $inquiry = [
      'subscriber_id' => $multiFinanceDetail ? $multiFinanceDetail->subscriber_id : "",
      'subscriber_name' => $multiFinanceDetail ? $multiFinanceDetail->subscriber_name : "",
      'discount' => $currencyHelper->formatPrice(0)
    ];
    if ($multiFinanceDetail) {
      $inquiry = array_merge($inquiry, [
        'ref_number' => ($item) ? $item->serial_number : "",
        'installment' => $multiFinanceDetail->installment,
        'due_date' => $multiFinanceDetail->due_date,
        'price' => $currencyHelper->formatPrice($multiFinanceDetail->base_price),
        'coll_fee' => $multiFinanceDetail->coll_fee != null ? array_merge(
          $currencyHelper->formatPrice($multiFinanceDetail->coll_fee), [
          'label' => $multiFinanceDetail->platform == PostpaidType::MULTIFINANCE_TYPE_SYARIAH ? 'Tazir' : 'Coll Fee'
        ]) : null,
        'fine' => array_merge($currencyHelper->formatPrice($multiFinanceDetail->fine), [
          'label' => $multiFinanceDetail->platform == PostpaidType::MULTIFINANCE_TYPE_SYARIAH ? 'Tawidh' : 'Denda'
        ]),
        'admin_fee' => $currencyHelper->formatPrice($multiFinanceDetail->admin_fee),
        'platform' => $multiFinanceDetail->platform,
        'discount' => $currencyHelper->formatPrice($multiFinanceDetail->base_price +
          ($multiFinanceDetail->coll_fee ? $multiFinanceDetail->coll_fee : 0) +
          $multiFinanceDetail->fine + $multiFinanceDetail->admin_fee - $orderDetail->sale_price)
      ]);
    }

    return $inquiry;
  }

  public function record($switcherId, $detail) {
    $postpaidDetails = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPostpaidMultiFinanceDetailRepository::class);
    $switcherDetail = $postpaidDetails->getNew();
    $switcherDetail->order_detail_pulsa_switcher_id = $switcherId;
    $switcherDetail->subscriber_id = $detail['subscriber_id'];
    $switcherDetail->subscriber_name = $detail['subscriber_name'];
    $switcherDetail->installment = $detail['inquiries']['installment'];
    $switcherDetail->due_date = $detail['inquiries']['due_date'];
    $switcherDetail->base_price = $detail['inquiries']['base_price'];
    $switcherDetail->coll_fee = $detail['inquiries']['coll_fee'];
    $switcherDetail->fine = $detail['inquiries']['fine'];
    $switcherDetail->admin_fee = $detail['inquiries']['admin'];
    $switcherDetail->platform = $detail['inquiries']['platform'] != '-' ? $detail['inquiries']['platform'] : null;
    $postpaidDetails->save($switcherDetail);
  }

}
