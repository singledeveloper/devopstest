<?php

namespace Odeo\Domains\Supply;

use Carbon\Carbon;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Core\PipelineListener;

class SupplyInventoryGroupEditor {

  private $storeInventories, $inventoryGroupPrices, $currency, $storeInventoryGroupPriceUsers,
    $supplyValidator, $storeInventoryDetails, $storeInventoryGroupPriceDetails, $agentSupplyDefaults;

  public function __construct() {
    $this->inventoryGroupPrices = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository::class);
    $this->storeInventoryDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryDetailRepository::class);
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->storeInventoryGroupPriceDetails = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceDetailRepository::class);
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->storeInventoryGroupPriceUsers = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository::class);
    $this->agents = app()->make(\Odeo\Domains\Agent\Repository\AgentRepository::class);
    $this->agentSupplyDefaults = app()->make(\Odeo\Domains\Agent\Repository\AgentSupplyDefaultRepository::class);
  }

  public function add(PipelineListener $listener, $data) {
    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $groupPricesToBeSave = [];
    $dateTime = Carbon::now();
    $supplyGroupId = $this->inventoryGroupPrices->generateSupplyGroupId();

    foreach ($this->storeInventories->findByStoreId($data['store_id']) as $item) {
      $groupPricesToBeSave[] = [
        'supply_group_id' => $supplyGroupId,
        'store_inventory_id' => $item->id,
        'type' => InventoryGroupPrice::CUSTOM,
        'name' => title_case($data['name']),
        'description' => isset($data['description']) ? $data['description'] : '',
        'keep_profit_consistent' => true,
        'created_at' => $dateTime,
        'updated_at' => $dateTime
      ];
    }

    $this->inventoryGroupPrices->saveBulk($groupPricesToBeSave);

    return $listener->response(200);
  }

  public function edit(PipelineListener $listener, $data) {
    $groupPrices = $this->inventoryGroupPrices->getBySupplyGroupId($data['supply_group_id']);
    $storeInventory = $groupPrices[0]->storeInventory;

    list ($isValid, $message) = $this->supplyValidator->checkStore($storeInventory->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    foreach ($groupPrices as $item) {
      $item->name = title_case($data['name']);
      $item->description = isset($data['description']) ? $data['description'] : '';
      $this->inventoryGroupPrices->save($item);
    }

    return $listener->response(200);
  }

  public function addPriceDetails(PipelineListener $listener, $data) {
    $groupPrices = $this->inventoryGroupPrices->getBySupplyGroupId($data['supply_group_id']);
    $storeInventory = $groupPrices[0]->storeInventory;

    list ($isValid, $message) = $this->supplyValidator->checkStore($storeInventory->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $inventoryIds = [];
    $priceDetails = [];
    foreach ($data['price_details'] as $item) {
      $inventoryIds[] = $item['id'];
      $priceDetails[$item['id']] = $item['price'];
    }

    $groupByServiceDetails = [];
    foreach($groupPrices as $item) {
      $groupByServiceDetails[$item->storeInventory->service_detail_id] = $item->id;
    }

    $storeInventoryIds = [];
    foreach ($groupPrices as $item) {
      $storeInventoryIds[] = $item->store_inventory_id;
    }

    $dataToSave = [];
    $nowDateString = Carbon::now();
    foreach($this->storeInventoryDetails->getByInventoryIds($storeInventoryIds, $inventoryIds) as $item) {
      if (isset($priceDetails[$item->inventory_id])) {
        $dataToSave[] = [
          'store_inventory_detail_id' => $item->id,
          'store_inventory_group_price_id' => $groupByServiceDetails[$item->storeInventory->service_detail_id],
          'sell_price' => $priceDetails[$item->inventory_id],
          'planned_price' => $priceDetails[$item->inventory_id],
          'updated_at' => $nowDateString,
          'created_at' => $nowDateString
        ];
      }
    }

    if (sizeof($dataToSave) > 0)
      $this->storeInventoryGroupPriceDetails->saveBulk($dataToSave);
    else return $listener->response(400, 'Tidak ada data yang akan disimpan.');

    return $listener->response(200);
  }

  public function editPriceDetail(PipelineListener $listener, $data) {
    $groupPriceDetail = $this->storeInventoryGroupPriceDetails->findById($data['store_inventory_group_price_detail_id']);
    $storeInventoryDetail = $groupPriceDetail->storeInventoryDetail;
    list ($isValid, $message) = $this->supplyValidator->checkStore($storeInventoryDetail->storeInventory->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $groupPriceDetail->planned_price = $data['sell_price'];

    $groupPrice = $groupPriceDetail->groupPrice;
    $groupPrice->pricing_apply_at = Carbon::now();

    $this->storeInventoryGroupPriceDetails->save($groupPriceDetail);
    $this->inventoryGroupPrices->save($groupPrice);

    $pulsa = $this->pulsaOdeo->findById($storeInventoryDetail->inventory_id);

    return $listener->response(200, [
      'sell_price' => $this->currency->formatPrice($data['sell_price']),
      'price_diff' => $data['sell_price'] - $pulsa->price
    ]);
  }

  public function removePriceDetail(PipelineListener $listener, $data) {
    $groupPriceDetail = $this->storeInventoryGroupPriceDetails->findById($data['store_inventory_group_price_detail_id']);
    list ($isValid, $message) = $this->supplyValidator->checkStore($groupPriceDetail->storeInventoryDetail->storeInventory->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    $this->storeInventoryGroupPriceDetails->delete($groupPriceDetail);

    return $listener->response(200);
  }

  public function editAgentGroup(PipelineListener $listener, $data) {
    $groupPrices = $this->inventoryGroupPrices->getBySupplyGroupId($data['supply_group_id']);

    if (!isset($data['set_as_default'])) {
      $storeInventory = $groupPrices[0]->storeInventory;

      list ($isValid, $message) = $this->supplyValidator->checkStore($storeInventory->store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      if (!$this->agents->findAgentFromStore($data['user_id'], $storeInventory->store_id))
        return $listener->response(400, 'User tidak terdaftar sebagai agen Anda yang aktif. Kemungkinan besar agen ini belum melakukan set master.');
    }
    else $data['user_id'] = isset($data['auth']) ? $data['auth']['user_id']: $data['user_id'];

    foreach ($groupPrices as $item) {
      if (!$groupUser = $this->storeInventoryGroupPriceUsers->findSupplyAgent($data['user_id'], $item->store_inventory_id)) {
        $groupUser = $this->storeInventoryGroupPriceUsers->getNew();
        $groupUser->user_id = $data['user_id'];
        $groupUser->store_inventory_group_price_id = $item->id;
      }
      else {
        $groupUser->new_store_inventory_group_price_id = $item->id;
        $groupUser->group_price_apply_at = Carbon::now();
      }
      $this->storeInventoryGroupPriceUsers->save($groupUser);
    }

    return $listener->response(200);
  }

  public function setGroupAsDefault(PipelineListener $listener, $data) {
    $groupPrices = $this->inventoryGroupPrices->getBySupplyGroupId($data['supply_group_id']);
    $storeInventory = $groupPrices[0]->storeInventory;

    list ($isValid, $message) = $this->supplyValidator->checkStore($storeInventory->store_id, $data);
    if (!$isValid) return $listener->response(400, $message);

    if (!$default = $this->agentSupplyDefaults->findByStoreId($storeInventory->store_id)) {
      $default = $this->agentSupplyDefaults->getNew();
      $default->store_id = $storeInventory->store_id;
    }

    $default->supply_group_id = $data['supply_group_id'];
    $this->agentSupplyDefaults->save($default);

    return $listener->response(200);

  }

}
