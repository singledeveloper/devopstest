<?php

namespace Odeo\Domains\Supply\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\DataExporterType;
use Odeo\Domains\Constant\DataExporterView;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Exporter\Helper\DataExporter;
use Odeo\Jobs\Job;

class SendTransactionList extends Job  {

  protected $data;

  function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    $vendorRecons = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->data['is_paginate'] = false;
    $vendorRecons->normalizeFilters($this->data);

    $recons = [[
      'order_id' => 'Order ID',
      'agent_name' => 'Nama Agen',
      'status' => 'Status',
      'biller_name' => 'Nama Biller',
      'pulsa_name' => 'Nama Pulsa',
      'pulsa_code' => 'Kode Biller',
      'destination' => 'Pengirim',
      'number' => 'Nomor Tujuan',
      'sale_price' => 'Omset',
      'profit' => 'Keuntungan',
      'reference_number' => 'No. Referensi',
      'created_at' => 'Tanggal Transaksi'
    ]];

    $totalProfit = 0;
    $totalSalePrice = 0;
    $reconTemp = [];
    foreach ($vendorRecons->getsForSupply() as $recon) {
      if ($recon->order_status == SwitcherConfig::SWITCHER_COMPLETED) $status = "SUCCESS";
      else if ($recon->order_status == SwitcherConfig::SWITCHER_REFUNDED) $status = "REFUNDED";
      else if (in_array($recon->order_status, [
        SwitcherConfig::SWITCHER_FAIL,
        SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER
      ])) $status = "FAILED";
      else $status = 'PENDING';

      $profit = $recon->sale_amount != null && $recon->sale_amount > 0 ? $recon->sale_amount - $recon->base_price : 0;
      $salePrice = $recon->sale_amount != null ? ($recon->sale_amount < $recon->base_price ? $recon->base_price : $recon->sale_amount) : 0;

      $reconTemp[] = [
        'order_id' => $recon->order_id,
        'agent_name' => $recon->order_user_name,
        'status' => $status,
        'biller_name' => $recon->vendor_switcher_name,
        'pulsa_name' => $recon->inventory_name,
        'pulsa_code' => strpos($recon->vendor_switcher_name, 'GOJEK') === false ? $recon->inventory_code : '',
        'destination' => strpos($recon->vendor_switcher_name, 'GOJEK') !== false ? $recon->destination : '',
        'number' => revertTelephone($recon->number),
        'sale_price' => $salePrice > 0 ? ($currencyHelper->formatPrice($salePrice))['formatted_amount'] : '',
        'profit' => $profit > 0 ? ($currencyHelper->formatPrice($profit))['formatted_amount'] : '',
        'reference_number' => $recon->reference_number,
        'created_at' => $recon->created_at->format('Y-m-d H:i:s')
      ];

      $totalProfit += $profit;
      $totalSalePrice += $salePrice;
    }

    $recons[] = [
      'order_id' => 'TOTAL',
      'agent_name' => '',
      'status' => '',
      'biller_name' => '',
      'pulsa_name' => '',
      'pulsa_code' => '',
      'destination' => '',
      'number' => '',
      'sale_price' => ($currencyHelper->formatPrice($totalSalePrice))['formatted_amount'],
      'profit' => ($currencyHelper->formatPrice($totalProfit))['formatted_amount'],
      'reference_number' => '',
      'created_at' => ''
    ];

    $recons = array_merge($recons, $reconTemp);

    $dataExporter = app()->make(DataExporter::class);
    $file = $dataExporter->generate($recons, DataExporterType::CSV, DataExporterView::SUPPLY_TRANSACTION_LIST_KEY);

    Mail::send('emails.supply_transaction_list_mail', [
      'data' => [
        'date' => Carbon::now()->toDateTimeString()
      ]
    ], function ($m) use ($file) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->attachData($file, 'List Transaksi - ' . $this->data['date'] . '.' . DataExporterType::CSV);
      $m->to($this->data['email'])->subject('List Transaksi [ODEO]');
    });

  }
}
