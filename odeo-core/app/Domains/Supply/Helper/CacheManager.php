<?php

namespace Odeo\Domains\Supply\Helper;

use Odeo\Domains\Constant\Supplier;

class CacheManager {

  private $vendorSwitcherTemplateResponseDetails, $vendorSwitcherConnections, $vendorSwitchers, $cache;

  public function __construct() {
    $this->cache = app()->make(\Illuminate\Contracts\Cache\Repository::class);
    $this->vendorSwitcherTemplateResponseDetails = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseDetailRepository::class);
    $this->vendorSwitcherConnections = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherConnectionRepository::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
  }

  public function resetResponse($templateId, $vendorSwitcherId, $routeType = '') {
    $table = $this->vendorSwitcherTemplateResponseDetails->getModel()->getTable();
    $vendorSwitcherId = ($vendorSwitcherId != null && $vendorSwitcherId != '0' ? ('.biller.' . $vendorSwitcherId) : '');
    if ($routeType != '' && $routeType != Supplier::ROUTE_BOTH)
      $this->cache->tags($table)->forget($table . '.template.' . $templateId . $vendorSwitcherId . '.' . $routeType);
    else {
      $this->cache->tags($table)->forget($table . '.template.' . $templateId . $vendorSwitcherId . '.' . Supplier::ROUTE_RESPONSE);
      $this->cache->tags($table)->forget($table . '.template.' . $templateId . $vendorSwitcherId . '.' . Supplier::ROUTE_REPORT);
    }
  }

  public function resetConnection($vendorSwitcherId) {
    $table = $this->vendorSwitcherConnections->getModel()->getTable();
    $this->cache->tags($table)->forget($table . '.biller.' . $vendorSwitcherId);
    if ($biller = $this->vendorSwitchers->findById($vendorSwitcherId)) {
      $this->cache->tags($table)->forget($table . '.biller-slug.' . $biller->notify_slug);
      $this->cache->tags($table)->forget($table . '.biller-from.' . md5($biller->server_destination));
    }
  }

}
