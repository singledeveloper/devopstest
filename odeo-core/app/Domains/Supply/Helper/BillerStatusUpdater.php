<?php

namespace Odeo\Domains\Supply\Helper;

use Odeo\Domains\Biller\BillerManager;
use Odeo\Domains\Biller\Jobs\InsertMutation;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\Jobs\ChangePulsaFixedPrice;
use Odeo\Domains\Subscription\ManageInventory\Jobs\StabilizeAllStoreProfit;

class BillerStatusUpdater extends BillerManager {

  protected $typeFormatter, $responseTranslator, $vendorSwitcherTemplateResponses, $signatureFormatter,
    $vendorSwitcherTemplateResponseDetails, $vendorSwitcherConnections;
  public $translateResult = [];
  private $requestTimestamp = [],
    $jobConnDest, $jobConnUserId, $jobConnPin, $jobConnUserName,
    $jobConnPassword, $jobConnToken, $jobReconIds = [];

  public function __construct() {
    parent::__construct();
    $this->typeFormatter = app()->make(\Odeo\Domains\Supply\Formatter\TypeManager::class);
    $this->signatureFormatter = app()->make(\Odeo\Domains\Supply\Formatter\SignatureManager::class);
    $this->vendorSwitcherTemplateResponses = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseRepository::class);
    $this->vendorSwitcherTemplateResponseDetails = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseDetailRepository::class);
    $this->responseTranslator = app()->make(\Odeo\Domains\Supply\Formatter\ResponseTranslator::class);
    $this->vendorSwitcherConnections = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherConnectionRepository::class);
  }

  public function setConnectionData($data) {
    $this->jobConnDest = $data['connection']['destination'];
    $this->jobConnUserId = $data['connection']['user_id'];
    $this->jobConnUserName = $data['connection']['user_name'];
    $this->jobConnPassword = $data['connection']['password'];
    $this->jobConnPin = $data['connection']['pin'];
    $this->jobConnToken = $data['connection']['token'];
    $this->jobReconIds = $data['recon_ids'];
  }

  public function getUserId() {
    return $this->jobConnUserId;
  }

  public function getUserName() {
    return $this->jobConnUserName;
  }

  public function getPassword() {
    return $this->jobConnPassword;
  }

  public function getPin() {
    return $this->jobConnPin;
  }

  public function getToken() {
    return $this->jobConnToken;
  }

  public function getReconIds() {
    return $this->jobReconIds;
  }

  public function getDestination() {
    return $this->jobConnDest;
  }

  public function translateRequest($string, $data) {
    $isSignFormat = substr($string, 0, strlen(Supplier::SIGN_HEADER)) == Supplier::SIGN_HEADER;

    if ($this->currentRecon) $data['id'] = $this->currentRecon->id;

    $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_USERID), $data['user_id'], $string);
    if (isset($data['encrypt']) && !$isSignFormat) $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_PASSWORD), 'XXXX', $string);
    else $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_PASSWORD), $data['password'], $string);
    if (isset($data['encrypt']) && !$isSignFormat) $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_PIN), 'XXXX', $string);
    else $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_PIN), $data['pin'], $string);
    if (isset($data['encrypt']) && !$isSignFormat) $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_TOKEN), 'XXXX', $string);
    else $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_TOKEN), $data['token'], $string);
    $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_CODE), $data['denom'], $string);
    if (isset($data['base_price'])) $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_PRICE), $data['base_price'], $string);
    $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_MSISDN), isset($data['number']) ? $data['number'] : revertTelephone($this->currentSwitcher->number), $string);
    if (isset($data['id'])) $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_TRXID), $data['id'], $string);
    if ($this->currentBiller) $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_REVERSAL_URL), Supplier::createSlug($this->currentBiller->notify_slug), $string);

    if (strpos($string, Supplier::addPattern(Supplier::SF_PARAM_TIME_HMS)) !== false) {
      if (!isset($this->requestTimestamp[Supplier::SF_PARAM_TIME_HMS])) $this->requestTimestamp[Supplier::SF_PARAM_TIME_HMS] = date('His');
      $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_TIME_HMS), $this->requestTimestamp[Supplier::SF_PARAM_TIME_HMS], $string);
    }
    if (strpos($string, Supplier::addPattern(Supplier::SF_PARAM_DATETIME)) !== false) {
      if (!isset($this->requestTimestamp[Supplier::SF_PARAM_DATETIME])) $this->requestTimestamp[Supplier::SF_PARAM_DATETIME] = date('Y-m-d H:i:s');
      $string = str_replace(Supplier::addPattern(Supplier::SF_PARAM_DATETIME), $this->requestTimestamp[Supplier::SF_PARAM_DATETIME], $string);
    }

    if ($isSignFormat) {
      $noHeader = substr($string, strlen(Supplier::SIGN_HEADER), strlen($string) - 1);
      $hashFunction = substr($noHeader, 0, strpos($noHeader, '#'));
      $string = substr($noHeader, strpos($noHeader, '#') + 1, strlen($noHeader) - 1);

      if ($hashFunction != Supplier::SIGN_TYPE_CUSTOM) {
        $temp = explode('&', $hashFunction);
        for ($x = sizeof($temp) - 1; $x >= 0; $x--) {
          if ($temp[$x] == Supplier::SIGN_TYPE_MD5) $string = md5($string);
          else if ($temp[$x] == Supplier::SIGN_TYPE_SHA1) $string = sha1($string);
          else if ($temp[$x] == Supplier::SIGN_TYPE_SHA256) $string = hash('sha256', $string);
          else if ($temp[$x] == Supplier::SIGN_TYPE_BASE64) $string = base64_encode($string);
        }
      }
      else {
        $data['number'] = isset($data['number']) ? $data['number'] : revertTelephone($this->currentSwitcher->number);
        $data['date'] = $this->requestTimestamp;
        $string = $this->signatureFormatter->setPath($string)->generate($data);
      }
    }
    else if (substr($string, 0, strlen(Supplier::COMPARE_HEADER)) == Supplier::COMPARE_HEADER) {
      $anyResult = '';
      foreach(explode(',', str_replace(Supplier::COMPARE_HEADER, '', $string)) as $item) {
        try {
          list($optionCompareData, $optionResult) = explode('=', $item);
          if ($optionCompareData == 'any') $anyResult = $optionResult;
          list($optionKey, $optionCompare) = explode(':', $optionCompareData);
          if (isset($data[$optionKey]) && $data[$optionKey] == $optionCompare) return $optionResult;
        }
        catch (\Exception $e) {}
      }
      if ($anyResult != '') return $anyResult;
    }

    return $string;
  }

  public function translateResponse($item, $response) {
    if ($responseDetail = $this->typeFormatter->setPath($item->format_type)->deconstructResponse($response)) {

      $responseConditions = $item->format_type == Supplier::TYPE_TEXT ? $item->response_details : json_decode($item->response_details, true);

      if ($result = $this->responseTranslator->parse($responseConditions, $responseDetail)) {
        if ($item->response_status_type == Supplier::STATUS_SUCCESS) {
          $result[Supplier::SF_PARAM_STATUS] = SwitcherConfig::BILLER_SUCCESS;
          $result[Supplier::SF_PARAM_RESPONSE_CODE] = Supplier::RC_OK;
        }
        else if (strpos($item->response_status_type, Supplier::STATUS_FAIL) !== false) {
          $result[Supplier::SF_PARAM_STATUS] = SwitcherConfig::BILLER_FAIL;
          $temp = explode('_', $item->response_status_type);
          $result[Supplier::SF_PARAM_RESPONSE_CODE] = isset($temp[1]) ? $temp[1] : Supplier::RC_FAIL;
        }
        else if ($item->response_status_type == Supplier::STATUS_LOOP_FAIL) {
          $result[Supplier::SF_PARAM_STATUS] = SwitcherConfig::BILLER_DUPLICATE_SUCCESS;
        }
        else {
          $result[Supplier::SF_PARAM_STATUS] = SwitcherConfig::BILLER_IN_QUEUE;
          $result[Supplier::SF_PARAM_RESPONSE_CODE] = Supplier::RC_PENDING;
        }

        $this->translateResult = $result;

        return true;
      }
    }
    return false;
  }

  public function supplyFinalize(PipelineListener $listener) {

    if (isset($this->translateResult[Supplier::SF_PARAM_STATUS]))
      $this->currentRecon->status = $this->translateResult[Supplier::SF_PARAM_STATUS];
    if (isset($this->translateResult[Supplier::SF_PARAM_REFID]))
      $this->currentRecon->biller_transaction_id = $this->translateResult[Supplier::SF_PARAM_REFID];
    if (isset($this->translateResult[Supplier::SF_PARAM_PRICE])) {
      if ($this->currentPulsaInventory->supply_pulsa_id == null)
        $this->currentSwitcher->current_base_price = $this->translateResult[Supplier::SF_PARAM_PRICE];
      $this->currentRecon->base_price = $this->translateResult[Supplier::SF_PARAM_PRICE];
    }
    if (isset($this->translateResult[Supplier::SF_PARAM_RESPONSE_CODE]) &&
      ($this->currentSwitcher->current_response_status_code == null ||
        ($this->currentSwitcher->current_response_status_code != null &&
          $this->translateResult[Supplier::SF_PARAM_RESPONSE_CODE] != Supplier::RC_FAIL)))
      $this->currentSwitcher->current_response_status_code = $this->translateResult[Supplier::SF_PARAM_RESPONSE_CODE];
    if (isset($this->translateResult[Supplier::SF_PARAM_BALANCE]))
      $this->currentBiller->current_balance = $this->translateResult[Supplier::SF_PARAM_BALANCE];
    if (isset($this->translateResult[Supplier::SF_PARAM_SN])) {
      $this->currentSwitcher->serial_number = trim($this->translateResult[Supplier::SF_PARAM_SN], '<>.,-');
      $snManager = $this->inquiryFormatter->setPath($this->currentPulsa->service_detail_id);
      if ($snManager != '') {
        $this->translateResult[Supplier::SF_PARAM_NUMBER] = $this->currentSwitcher->number;
        $this->currentSwitcher->serial_number = $snManager->toString($this->translateResult, $this->currentSwitcher->serial_number);
      }
    }
    if (isset($this->translateResult[Supplier::SF_PARAM_VOUCHER_CODE]))
      $this->currentSwitcher->serial_number .= '/VC:' . $this->translateResult[Supplier::SF_PARAM_VOUCHER_CODE];

    $this->currentRecon->reference_number = $this->currentSwitcher->serial_number;

    $this->updateStatusBasedOnConditions();

    if (isset($this->translateResult[Supplier::SF_PARAM_UNIT_BALANCE]))
      $this->pulsaInventoryRepository->updateUnit($this->currentBiller->id, $this->currentPulsaInventory->code, $this->translateResult[Supplier::SF_PARAM_UNIT_BALANCE]);

    if (PostpaidType::getConstKeyByValue($this->currentPulsa->category) == '') {
      if ($this->currentPulsa->owner_store_id) {
        if ($this->currentPulsa->price != $this->currentRecon->base_price) {
          $listener->pushQueue(new StabilizeAllStoreProfit([
            'inventories' => [
              $this->currentPulsa->service_detail_id => [
                [
                  'inventory_id' => $this->currentPulsa->id,
                  'price_before' => $this->currentPulsa->price,
                  'price_after' => $this->currentRecon->base_price
                ]
              ]
            ]
          ]));
          $this->currentPulsa->price = $this->currentRecon->base_price;
          $this->pulsaOdeoRepository->save($this->currentPulsa);
        }
      }
      else if ($this->currentPulsaInventory->base_price != $this->currentRecon->base_price) {
        if ($this->currentPulsaInventory->supply_pulsa_id == null)
          $this->currentPulsaInventory->base_price = $this->currentRecon->base_price;

        if ($this->currentBiller->status == SwitcherConfig::BILLER_STATUS_OK
          && $this->currentRecon->status == SwitcherConfig::BILLER_SUCCESS
          && ($this->currentPulsa->owner_store_id != null ||
            ($this->currentPulsa->owner_store_id == null
              && strpos($this->currentPulsa->name, 'Promo') === false)))
          $listener->pushQueue(new ChangePulsaFixedPrice($this->currentPulsaInventory->id, $this->currentRecon->base_price));
      }
    }

    $this->finalizeSwitcher($listener);

    if ($this->currentRecon->status == SwitcherConfig::BILLER_SUCCESS) {
      if (!$this->currentPulsaInventory->is_unit) {
        if ($this->currentSwitcher->vendor_switcher_id == $this->currentRecon->vendor_switcher_id) {
          $mutationData = [
            'order_detail_pulsa_switcher_id' => $this->currentSwitcher->id,
            'vendor_switcher_id' => $this->currentBiller->id,
            'transaction_id' => $this->currentRecon->id,
            'transaction_status' => $this->currentRecon->status,
            'transaction_status_prior' => SwitcherConfig::BILLER_FAIL,
            'amount' => ('-' . $this->currentRecon->base_price),
            'mutation_route' => $this->currentRoute
          ];
          if ($this->currentBiller->current_balance) $mutationData['current_balance'] = $this->currentBiller->current_balance;
          $listener->pushQueue(new InsertMutation($mutationData));
        }
      }
    }

    $this->reconRepository->save($this->currentRecon);
    $this->redis->hdel(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_PULSA_CHECK_LOCK . $this->currentRecon->id);

    return $listener->response(200);
  }

}
