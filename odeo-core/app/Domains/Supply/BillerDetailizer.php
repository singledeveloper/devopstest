<?php

namespace Odeo\Domains\Supply;

use Illuminate\Support\Facades\Crypt;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;

class BillerDetailizer {

  private $vendorSwitcherConnections, $supplyValidator;

  public function __construct() {
    $this->vendorSwitcherConnections = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherConnectionRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
  }

  public function get(PipelineListener $listener, $data) {

    if ($connection = $this->vendorSwitcherConnections->findByVendorSwitcherId($data['vendor_switcher_id'])) {
      $biller = $connection->vendorSwitcher;

      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      $template = $connection->template;

      return $listener->response(200, [
        'connection' => [
          'server_destination' => $connection->server_destination,
          'server_user_id' => $connection->server_user_id,
          'server_user_name' => $connection->server_user_name ? $connection->server_user_name : null,
          'server_pin' => $connection->server_pin ? Crypt::decrypt($connection->server_pin) : null,
          'server_password' => $connection->server_password ? Crypt::decrypt($connection->server_password) : null,
          'server_token' => $connection->server_token ? Crypt::decrypt($connection->server_token) : null,
          'biller' => [
            'id' => $biller->id,
            'name' => $biller->name,
            'current_balance' => $biller->current_balance,
            'recon_start' => $biller->recon_started_at,
            'recon_end' => $biller->recon_ended_at,
            'notify' => $template->format_type == Supplier::TYPE_TEXT ? InlineJabber::DEFAULT_ACCOUNT : Supplier::createSlug($biller->notify_slug),
            'queue_limit' => $biller->queue_limit,
            'duplicate_check_type' => $biller->duplicate_check_type
          ],
          'template' => [
            'id' => $template->id,
            'name' => $template->name,
            'format_type' => $template->format_type,
            'predefined_store_id' => $template->predefined_store_id
          ]
        ]
      ]);
    }

    return $listener->response(204);
  }

}
