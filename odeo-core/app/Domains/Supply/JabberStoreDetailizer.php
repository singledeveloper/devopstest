<?php

namespace Odeo\Domains\Supply;

use Odeo\Domains\Core\PipelineListener;

class JabberStoreDetailizer {

  private $jabberStores, $supplyValidator;

  public function __construct() {
    $this->jabberStores = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberStoreRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
  }

  public function getDetail(PipelineListener $listener, $data) {

    if ($jabberStore = $this->jabberStores->findByStoreId($data['store_id'])) {

      list ($isValid, $message) = $this->supplyValidator->checkStore($jabberStore->store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      return $listener->response(200, [
        'jabber_store' => [
          'account' => $jabberStore->email,
          'full_account' => $jabberStore->email . '/jaxl#odeo',
          'resource' => 'jaxl#odeo',
          'status_message' => $jabberStore->status_message,
          'is_active' => $jabberStore->socket_path != null
        ]
      ]);
    }

    return $listener->response(204);
  }

}
