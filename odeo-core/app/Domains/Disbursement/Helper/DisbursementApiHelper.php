<?php

namespace Odeo\Domains\Disbursement\Helper;

use DateTime;
use Exception;
use Illuminate\Http\Request;
use Odeo\Domains\Constant\ApiDisbursement;

class DisbursementApiHelper {

  const DATE_TIMEOUT_SECONDS = 15 * 60;
  const DATE_OFFSET_THRESHOLD_SECONDS = 60;
  const SIGNATURE_HEADER = 'X-Odeo-Signature';
  const TIMESTAMP_HEADER = 'X-Odeo-Timestamp';

  const DISBURSEMENT_FEE = 5500;
  const DISB_INQUIRY_FEE = 1000;
  const DISB_INQUIRY_VALIDATION_FEE = 0;

  public function isValid($components, $timestamp, $signature, $secretAccessKey) {
    $now = time();

    if ($timestamp == null) {
      return [false, ApiDisbursement::ERROR_INVALID_TIMESTAMP];
    }
    else {
      $diff = $now - $timestamp;
    }

    if ($diff < -self::DATE_OFFSET_THRESHOLD_SECONDS) {
      return [false, ApiDisbursement::ERROR_INVALID_TIMESTAMP];
    }

    if ($diff > self::DATE_TIMEOUT_SECONDS) {
      return [false, ApiDisbursement::ERROR_TIMESTAMP_EXPIRED];
    }

    if ($signature != $this->generateSignature($components, $timestamp, $secretAccessKey)) {
      return [false, ApiDisbursement::ERROR_SIGNATURE_MISMATCH];
    }

    return [true, null];
  }

  public function generateSignature($components, $timestamp, $secretAccessKey) {
    $components = array_merge($components, [
      $timestamp
    ]);

    return hash_hmac('sha256', implode(':', $components), $secretAccessKey);
  }

}