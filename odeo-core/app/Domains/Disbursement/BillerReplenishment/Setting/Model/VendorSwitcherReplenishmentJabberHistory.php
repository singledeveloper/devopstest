<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class VendorSwitcherReplenishmentJabberHistory extends Entity {

  public function vendorSwitcher() {
    return $this->belongsTo(VendorSwitcher::class);
  }

}