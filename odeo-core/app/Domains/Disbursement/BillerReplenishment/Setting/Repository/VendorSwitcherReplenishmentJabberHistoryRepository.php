<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository;

use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\BillerReplenishment\Setting\Model\VendorSwitcherReplenishmentJabberHistory;

class VendorSwitcherReplenishmentJabberHistoryRepository extends Repository {

  public function __construct(VendorSwitcherReplenishmentJabberHistory $vendorSwitcherReplenishmentJabberHistory) {
    $this->model = $vendorSwitcherReplenishmentJabberHistory;
  }

  public function findByVendorSwitcherId($vendorSwitcherId) {
    return $this->model->where("vendor_switcher_id", $vendorSwitcherId)
      ->where('status', SwitcherConfig::BILLER_IN_QUEUE)->orderBy('id', 'desc')->first();
  }

  public function findNoReplyReplenishment() {
    return $this->model->whereNull('log_reply')->where('status', SwitcherConfig::BILLER_IN_QUEUE)
      ->orderBy('id', 'desc')->get();
  }

}