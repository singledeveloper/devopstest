<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository;

use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\InlineTelegram;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\BillerReplenishment\Setting\Model\VendorSwitcherReplenishmentSetting;

class VendorSwitcherReplenishmentSettingRepository extends Repository {

  public function __construct(VendorSwitcherReplenishmentSetting $vendorSwitcherReplenishmentSetting) {
    $this->model = $vendorSwitcherReplenishmentSetting;
  }

  public function getByVendorSwitcherId($vendorSwitcherId, $routeType = '') {
    $query = $this->model->where("vendor_switcher_id", $vendorSwitcherId);
    if ($routeType != '') $query = $query->where('route_type', $routeType);
    return $query->where('is_active', true)->get();
  }

  public function findByVendorSwitcherId($vendorSwitcherId, $routeType) {
    return $this->model->where("vendor_switcher_id", $vendorSwitcherId)
      ->where('route_type', $routeType)->where('is_active', true)->orderBy('id', 'desc')->first();
  }

  public function findByData($string, $channelType = '') {
    $query = $this->model->where("data", "like", "%" . $string . "%");
    if ($channelType != '') $query = $query->where('channel_type', $channelType);
    return $query->where('is_active', true)->orderBy('id', 'desc')->first();
  }

  public function getTelegramIgnoredChatId() {
    $key = $this->model->getTable();

    return $this->runCache($key, $key . '.' . InlineTelegram::REDIS_IGNORED_CHAT_ID_KEY, 60 * 60 * 24 * 30, function() {
      $chatIds = [];
      foreach ($this->model->where('channel_type', BillerReplenishment::SETTING_CHANNEL_TELEGRAM)->where('is_active', true)->get() as $item) {
        $settingData = json_decode($item->data, true);
        if (!in_array($settingData[BillerReplenishment::SETTING_CHANNEL_TELEGRAM_CHAT_ID], $chatIds))
          $chatIds[] = $settingData[BillerReplenishment::SETTING_CHANNEL_TELEGRAM_CHAT_ID];
      }
      return $chatIds;
    });
  }

}