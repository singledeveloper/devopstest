<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting;

use Illuminate\Support\Facades\Validator;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;

class ChannelManager {

  public function setPath($channelType) {
    switch ($channelType) {
      case BillerReplenishment::SETTING_CHANNEL_TELEGRAM:
        return app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Channel\TelegramFormatter::class);
      case BillerReplenishment::SETTING_CHANNEL_JABBER:
        return app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Channel\JabberFormatter::class);
      case BillerReplenishment::SETTING_CHANNEL_EMAIL:
        return app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Channel\EmailFormatter::class);
      case BillerReplenishment::SETTING_CHANNEL_CONSTANT_VALUE:
        return app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Channel\ConstantValueFormatter::class);
      default:
        return '';
    }
  }

}
