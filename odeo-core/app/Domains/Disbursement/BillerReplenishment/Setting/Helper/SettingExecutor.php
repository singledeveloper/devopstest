<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Helper;

use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class SettingExecutor {

  private $settings, $channelManager, $billerReplenishments;

  public function __construct() {
    $this->settings = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository\VendorSwitcherReplenishmentSettingRepository::class);
    $this->channelManager = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\ChannelManager::class);
    $this->billerReplenishments = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository::class);
  }

  public function beforeSend(PipelineListener $listener, $data) {
    $manual = SwitcherConfig::getVendorSwitcherReplenisherById($data['vendor_switcher_id']);
    if ($manual != '') return $manual->request($listener, $data);

    if ($setting = $this->settings->findByVendorSwitcherId($data['vendor_switcher_id'], BillerReplenishment::SETTING_ROUTE_BEFORE)) {
      return $this->channelManager->setPath($setting->channel_type)->beforeSend($data, $setting);
    }
    $count = $this->billerReplenishments->getSameDayCompletedReplenishmentCount($data['vendor_switcher_id']);
    $data['amount']+= ($count * BillerReplenishment::SAME_DAY_MULTIPLIER);
    return ['amount' => $data['amount']];
  }

  public function onComplete(PipelineListener $listener, $data) {
    $manual = SwitcherConfig::getVendorSwitcherReplenisherById($data['vendor_switcher_id']);
    if ($manual != '') return $manual->complete($listener, $data);

    foreach ($this->settings->getByVendorSwitcherId($data['vendor_switcher_id'], BillerReplenishment::SETTING_ROUTE_AFTER) as $item) {
      $this->channelManager->setPath($item->channel_type)->onComplete($data, $item);
    }
    return [];
  }

}
