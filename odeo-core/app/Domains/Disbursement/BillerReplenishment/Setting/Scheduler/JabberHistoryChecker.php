<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Scheduler;

use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Vendor\Jabber\Jobs\ReplySocket;

class JabberHistoryChecker {

  private $settingJabberHistories, $jabberManager, $settings;

  public function __construct() {
    $this->settingJabberHistories = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository\VendorSwitcherReplenishmentJabberHistoryRepository::class);
    $this->jabberManager = app()->make(\Odeo\Domains\Vendor\Jabber\JabberManager::class);
    $this->settings = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository\VendorSwitcherReplenishmentSettingRepository::class);
  }

  public function run() {

    foreach ($this->settingJabberHistories->findNoReplyReplenishment() as $item) {
      if (time() - strtotime($item->created_at) > 600) {
        $item->log_reply = 'No response from biller';
        $this->settingJabberHistories->save($item);
      }
      else if (time() - strtotime($item->created_at) > 60) {
        $setting = $this->settings->findByVendorSwitcherId($item->vendor_switcher_id, BillerReplenishment::SETTING_ROUTE_BEFORE);
        $settingData = json_decode($setting->data, true);
        if (isset($settingData[BillerReplenishment::SETTING_CHANNEL_JABBER_CENTER])) {
          dispatch(new ReplySocket(InlineJabber::JAXL_CHAT, [
            'to' => $settingData[BillerReplenishment::SETTING_CHANNEL_JABBER_CENTER],
            'message' => $item->log_request
          ]));
        }
      }
    }
  }

}
