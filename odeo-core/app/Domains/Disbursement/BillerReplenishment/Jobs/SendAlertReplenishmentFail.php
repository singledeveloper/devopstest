<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/25/17
 * Time: 9:51 PM
 */

namespace Odeo\Domains\Disbursement\BillerReplenishment\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Jobs\Job;

class SendAlertReplenishmentFail extends Job {

  private $vendor, $topupNeeded, $currencyHelper;

  public function __construct($vendor, $topupNeeded) {
    parent::__construct();
    $this->vendor = $vendor;
    $this->topupNeeded = $topupNeeded;
    $this->currencyHelper = app()->make(Currency::class);
  }


  public function handle() {

    Mail::send('emails.vendor_replenishment_fail_alert', [
      'data' => [
        'topup_needed' => $this->currencyHelper->formatPrice($this->topupNeeded),
        'from' => $this->vendor
      ]
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      if (isProduction()) {
        $m->to('replenishment@odeo.co.id')->subject('Replenishment Alert Fail - ' . time());
        $m->to('christina.angga@gmail.com')->subject('Replenishment Alert Fail - ' . time());
      } else {
        $m->to('brian.japutra@gmail.com')->subject('[TEST] Replenishment Alert Fail - ' . time());
      }
    });

  }

}
