<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Scheduler;

use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\BeginReplenishment;
use Odeo\Domains\Inventory\Repository\VendorSwitcherRepository;

class DailyReplenishmentScheduler {

  private $startTime, $endTime;

  private $vendorSwitcherRepo;

  public function __construct($startTime, $endTime) {

    $this->startTime = $startTime;
    $this->endTime = $endTime;

    $this->vendorSwitcherRepo = app()->make(VendorSwitcherRepository::class);
  }

  public function run() {

    foreach ($this->vendorSwitcherRepo->getAutoReplenishBillers() as $item) {

      dispatch(new BeginReplenishment([
        'vendor_switcher_id' => $item->id,
        'start_time' => $this->startTime,
        'end_time' => $this->endTime
      ]));

    }
  }

}
