<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Bri\Helper;

use Odeo\Domains\Constant\BriDisbursement;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Disbursement\BillerReplenishment\Bri\Jobs\TransferReplenishment;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\SendAlertReplenishmentFail;
use Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository;
use Odeo\Domains\Vendor\Bri\Repository\DisbursementBriDisbursementRepository;

class ExecuteTransfer {

  private $billerReplenishmentRepo, $briDisbursementRepo;

  public function __construct() {
    $this->billerReplenishmentRepo = app()->make(VendorSwitcherReplenishmentRepository::class);
    $this->briDisbursementRepo = app()->make(DisbursementBriDisbursementRepository::class);
  }

  public function execute($replenishment) {

    $briId = null;

    if (!$replenishment->vendorSwitcher->bri_account_number) {
      $replenishment->status = BillerReplenishment::FAILED;
      $this->billerReplenishmentRepo->save($replenishment);
      dispatch(new SendAlertReplenishmentFail($replenishment->vendorSwitcher->name, $replenishment->amount));
      return;
    }


    if ($replenishment->auto_disbursement_reference_id) {
      $briId = $replenishment->auto_disbursement_reference_id;
    } else {

      $bri = $this->briDisbursementRepo->getNew();

      $bri->amount = $replenishment->amount;
      $bri->disbursement_type = Disbursement::DISBURSEMENT_FOR_BILLER_REPLENISHMENT;
      $bri->disbursement_reference_id = $replenishment->id;
      $bri->status = BriDisbursement::PENDING;

      $this->briDisbursementRepo->save($bri);

      $briId = $bri->id;

      $replenishment->auto_disbursement_reference_id = $bri->id;
    }

    $replenishment->status = BillerReplenishment::ON_PROGRESS_AUTO_TRANSFER;

    $this->billerReplenishmentRepo->save($replenishment);

    dispatch(new TransferReplenishment([
      'replenishment_id' => $replenishment->id,
      'bri_disbursement_id' => $briId
    ]));

  }

}
