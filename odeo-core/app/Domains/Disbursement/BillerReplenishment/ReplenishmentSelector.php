<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment;

use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class ReplenishmentSelector implements SelectorListener {

  private $currencyHelper, $billerReplenishments;

  public function __construct() {
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->billerReplenishments = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $output = [];
    $output['replenishment_id'] = $item->id;
    $output['biller_name'] = $item->vendorSwitcher->name;
    $output['amount'] = $this->currencyHelper->formatPrice($item->amount);
    $output['auto_disbursement_vendor'] = $item->auto_disbursement_vendor;

    if($item->auto_disbursement_vendor) {
      list($name, $type) = explode("_", $item->auto_disbursement_vendor);
      if($type == "api") $type = "automatic";
      $output['to_bank'] = strtoupper($name)." - ".strtoupper($type);
    } else {
      $output['to_bank'] = '';
    }

    if ($item->status == BillerReplenishment::PENDING) $output['status'] = 'TRANSFER QUEUE';
    else if ($item->status == BillerReplenishment::PENDING_PROGRESS_AUTO_TRANSFER) $output['status'] = 'TRANSFER PENDING';
    else if ($item->status == BillerReplenishment::ON_PROGRESS_AUTO_TRANSFER) $output['status'] = 'TRANSFER PROGRESS';
    else if ($item->status == BillerReplenishment::FAILED) $output['status'] = 'FAILED';
    else if ($item->status == BillerReplenishment::WAIT_FOR_NOTIFY) $output['status'] = 'WAITING COMPLETION';
    else if ($item->status == BillerReplenishment::WAIT_FOR_MANUAL_VERIFY) $output['status'] = 'WAITING MANUAL VERIFY';
    else if ($item->status == BillerReplenishment::WAIT_FOR_BILLER_UPDATE) $output['status'] = 'WAITING BILLER UPDATE';
    else if ($item->status == BillerReplenishment::COMPLETED) $output['status'] = 'COMPLETED';
    else if ($item->status == BillerReplenishment::CANCELLED) $output['status'] = 'CANCELLED';
    else $output['status'] = 'UNKNOWN: ' . $item->status;

    $output['last_error_message'] = $item->error_message;

    $output['created_at'] = $item->created_at->format('Y-m-d H:i:s');
    $output['verified_at'] = isset($item->verified_at) ? $item->verified_at : "";
    $output['cancelled_at'] = isset($item->cancelled_at) ? $item->cancelled_at : "";
    return $output;

  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $replenishments = [];

    $this->billerReplenishments->normalizeFilters($data);
    $this->billerReplenishments->setSimplePaginate(true);

    foreach ($this->billerReplenishments->gets() as $item) {
      $replenishments[] = $this->_transforms($item, $this->billerReplenishments);
    }

    if (sizeof($replenishments) > 0)
      return $listener->response(200, array_merge(
        ["replenishments" => $this->_extends($replenishments, $this->billerReplenishments)],
        $this->billerReplenishments->getPagination()
      ));

    return $listener->response(204, ["replenishments" => []]);
  }

}
