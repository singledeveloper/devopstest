<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Bca\Jobs;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\SendAlertReplenishmentFail;
use Odeo\Domains\Disbursement\BillerReplenishment\ReplenishmentRequester;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager;
use Odeo\Domains\Vendor\Bca\Transfer;
use Odeo\Jobs\Job;

class TransferReplenishment extends Job  {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    $bcaApi = app()->make(ApiManager::class);
    $billerReplenishments = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository::class);
    $redis = Redis::connection();

    if ($replenishment = $billerReplenishments->findById($this->data['replenishment_id'])) {

      if ($replenishment->status != BillerReplenishment::ON_PROGRESS_AUTO_TRANSFER) {
        return;
      }
    }

    if ($redis->hget(BillerReplenishment::REDIS_NS, BillerReplenishment::REDIS_KEY_ID . $this->data['replenishment_id'])) {
      return;
    }

    $result = $bcaApi->getBalance();

    if (isset($result['data']['AccountDetailDataSuccess']) && isset($result['data']['AccountDetailDataSuccess'][0])
      && isset($result['data']['AccountDetailDataSuccess'][0]['AvailableBalance'])
      && $result['data']['AccountDetailDataSuccess'][0]['AvailableBalance'] - $replenishment->amount < 5000000) {
      $replenishment->status = BillerReplenishment::FAILED;
      $replenishment->error_message = 'Not enough balance';
      $billerReplenishments->save($replenishment);

      dispatch(new SendAlertReplenishmentFail($replenishment->vendorSwitcher->name, $replenishment->amount));
    }
    else {

      $redis->hsetnx(BillerReplenishment::REDIS_NS, BillerReplenishment::REDIS_KEY_ID . $replenishment->id, 1);

      $pipeline = new Pipeline;
      $pipeline->add(new Task(Transfer::class, 'exec', [
        'corporate_id' => ApiManager::$CORPORATEID,
        'remark_1' => 'VSREPL',
        'remark_2' => $replenishment->id,
        'transfer_from' => ApiManager::$ACCOUNTNUMBER[0],
        'transfer_to' => $replenishment->vendorSwitcher->bca_account_number,
        'transaction_key' => 'VSREPL#' . $this->data['replenishment_id']
      ]));

      $pipeline->add(new Task(ReplenishmentRequester::class, 'verify'));

      $pipeline->execute($this->data);

      if ($pipeline->success()) {
        $redis->hdel(BillerReplenishment::REDIS_NS, BillerReplenishment::REDIS_KEY_ID . $replenishment->id);
        return;
      }
      else {
        $redis->hdel(BillerReplenishment::REDIS_NS, BillerReplenishment::REDIS_KEY_ID . $replenishment->id);

        $replenishment->status = BillerReplenishment::FAILED;
        $replenishment->error_message = ('Error code: ' . $pipeline->errorMessage['error_code']) ?? null;
        $billerReplenishments->save($replenishment);

        dispatch(new SendAlertReplenishmentFail($replenishment->vendorSwitcher->name, $replenishment->amount));
      }
    }

  }

}
