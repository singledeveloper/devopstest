<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Bca\Helper;

use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Disbursement\BillerReplenishment\Bca\Jobs\TransferReplenishment;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\SendAlertReplenishmentFail;

class ExecuteTransfer {

  private $billerReplenishments, $bcaDisbursements;

  public function __construct() {
    $this->billerReplenishments = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository::class);
    $this->bcaDisbursements = app()->make(\Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository::class);
  }

  public function execute($replenishment) {

    $bcaId = null;

    if ($replenishment->vendorSwitcher->bca_account_number != null) {
      if ($replenishment->auto_disbursement_reference_id) {
        $bcaId = $replenishment->auto_disbursement_reference_id;
      }
      else {
        $bca = $this->bcaDisbursements->getNew();

        $bca->amount = $replenishment->amount;
        $bca->disbursement_type = Disbursement::DISBURSEMENT_FOR_BILLER_REPLENISHMENT;
        $bca->disbursement_reference_id = $replenishment->id;
        $bca->status = BcaDisbursement::PENDING;

        $this->bcaDisbursements->save($bca);

        $bcaId = $bca->id;

        $replenishment->auto_disbursement_reference_id = $bca->id;
      }

      $replenishment->status = BillerReplenishment::ON_PROGRESS_AUTO_TRANSFER;

      $this->billerReplenishments->save($replenishment);

      dispatch(new TransferReplenishment([
        'replenishment_id' => $replenishment->id,
        'bca_disbursement_id' => $bcaId
      ]));
    }
    else {
      $replenishment->status = BillerReplenishment::FAILED;
      $this->billerReplenishments->save($replenishment);

      dispatch(new SendAlertReplenishmentFail($replenishment->vendorSwitcher->name, $replenishment->amount));
    }
  }

}
