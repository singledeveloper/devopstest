<?php

namespace Odeo\Domains\Disbursement\Model;


use Odeo\Domains\Account\Model\Bank;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class ApiDisbursement extends Entity {

  protected $dates = ['delayed_at', 'notified_at', 'cancelled_at', 'verified_at'];

  public function bank() {
    return $this->belongsTo(Bank::class);
  }

  public function user() {
    return $this->belongsTo(User::class);
  }

}