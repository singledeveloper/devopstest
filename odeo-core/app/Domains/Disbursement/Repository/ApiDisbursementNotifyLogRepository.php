<?php

namespace Odeo\Domains\Disbursement\Repository;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\Model\ApiDisbursementNotifyLog;

class ApiDisbursementNotifyLogRepository extends Repository {

  public function __construct(ApiDisbursementNotifyLog $apiDisbursementNotifyLog) {
    $this->model = $apiDisbursementNotifyLog;
  }

  public function listByDisbursementId($disbursementId) {
    return $this->getCloneModel()
      ->where('api_disbursement_id', $disbursementId)
      ->orderByDesc('id')
      ->get();
  }

  public function countByApiDisbursementIdAndStatus($id, $status) {
    $model = $this->getCloneModel();
    return $model->where('api_disbursement_id', $id)->where('status', $status)->count();
  }

  public function hasSuccessfulNotify($disbursementId, $status) {
    return $this->getCloneModel()
        ->where('api_disbursement_id', $disbursementId)
        ->whereIn('status', [$status, ApiDisbursement::COMPLETED])
        ->where('is_success', true)
        ->count() > 0;
  }
}