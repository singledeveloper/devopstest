<?php

namespace Odeo\Domains\Disbursement\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\Model\BatchDisbursement;

class BatchDisbursementRepository extends Repository {

  private $lock = false;

  public function __construct(BatchDisbursement $model) {
    $this->model = $model;
  }

  public function listByUserId($userId) {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->where('user_id', $userId)
      ->orderBy('id', 'desc');

    if (isset($filters['name']) && $filters['name'] != '') {
      $query->where('name', 'ilike', '%' . $filters['name'] . '%');
    }

    if (isset($filters['status']) && $filters['status'] != '') {
      $query->where('status', '=', $filters['status']);
    }

    if (isset($filters['start_date']) && $filters['start_date']) {
      $query->where('created_at', '>=', $filters['start_date'] . ' 00:00:00');
    }

    if (isset($filters['end_date']) && $filters['end_date']) {
      $query->where('created_at', '<=', $filters['end_date'] . ' 00:00:00');
    }

    return $this->getResult($query);
  }


  public function lock() {
    $this->lock = true;
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }


}