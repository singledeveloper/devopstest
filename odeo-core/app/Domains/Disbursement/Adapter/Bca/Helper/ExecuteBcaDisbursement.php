<?php

namespace Odeo\Domains\Disbursement\Adapter\Bca\Helper;

use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\DisbursementStatus;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Adapter\Helper\DisbursementInquirer;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;
use Odeo\Domains\Vendor\Bca\Transfer;

class ExecuteBcaDisbursement {

  private $bcaDisbursementRepo, $disbursementInquirer;

  public function __construct() {
    $this->bcaDisbursementRepo = app()->make(DisbursementBcaDisbursementRepository::class);
    $this->disbursementInquirer = app()->make(DisbursementInquirer::class);
  }

  public function exec($disbursement) {
    $inquiryStatus = $this->disbursementInquirer->inquire($disbursement);

    if ($inquiryStatus != BankAccountInquiryStatus::COMPLETED) {
      $bca = $this->bcaDisbursementRepo->findById($disbursement->disbursement_vendor_reference_id);
      $bca->status = BcaDisbursement::FAIL;
      $bca->error_code = BcaDisbursement::ERROR_CODE_INQUIRY_FAILED;
      $this->bcaDisbursementRepo->save($bca);

      return [
        'disbursement_id' => $disbursement->id,
        'status' => BankAccountInquiryStatus::toDisbursementStatus($inquiryStatus),
        'disbursement_vendor' => VendorDisbursement::BCA,
        'disbursement_vendor_reference_id' => $bca->id,
        'disbursement_vendor_updated_at' => strtotime($bca->updated_at),
      ];
    }

    $remark = "WDOCASH 3{$disbursement->id}";

    $pipeline = new Pipeline();
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'bca_disbursement_id' => $disbursement->disbursement_vendor_reference_id,
      'corporate_id' => ApiManager::$CORPORATEID,
      'amount' => $disbursement->amount,
      'remark_1' => substr($remark, 0, 18),
      'remark_2' => strlen($remark > 18) ? substr($remark, 18, 18) : '',
      'transfer_from' => ApiManager::$ACCOUNTNUMBER[0],
      'transfer_to' => $disbursement->account_number,
      'transaction_key' => 'DISB2#' . $disbursement->disbursement_vendor_reference_id,
    ]));

    $pipeline->execute();

    $bca = $this->bcaDisbursementRepo->findById($disbursement->disbursement_vendor_reference_id);

    return [
      'disbursement_id' => $disbursement->id,
      'status' => intval($this->getDisbursementStatus($pipeline)),
      'disbursement_vendor' => VendorDisbursement::BCA,
      'disbursement_vendor_reference_id' => $bca->id,
      'disbursement_vendor_updated_at' => strtotime($bca->updated_at),
    ];
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return DisbursementStatus::COMPLETED;
    }

    switch ($pipeline->errorMessage['error_code'] ?? null) {
      case 'ESB-82-014':
      case 'ESB-99-156':
      case 'ESB-82-012':
        return DisbursementStatus::FAILED_WRONG_ACCOUNT_NUMBER;

      case 'ESB-82-020':
      case 'ESB-82-018':
      case 'ESB-82-022':
        return DisbursementStatus::FAILED_CLOSED_BANK_ACCOUNT;

      case 'ESB-99-157':
        return DisbursementStatus::FAILED_BANK_REJECTION;

      case 'TIMEOUT':
      default:
        return DisbursementStatus::SUSPECT;
    }
  }
}
