<?php

namespace Odeo\Domains\Disbursement\Adapter\Artajasa\Helper;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Vendor\ArtajasaDisbursement\BalanceUpdater;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Transfer;

class ExecuteArtajasaDisbursement {

  private $artajasaDisbursementRepo, $artajasaResponseMapper, $bankRepo, $redis;

  public function __construct() {
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
    $this->artajasaResponseMapper = app()->make(ArtajasaResponseMapper::class);
    $this->bankRepo = app()->make(BankRepository::class);
    $this->redis = Redis::connection();
  }

  public function exec($disbursement) {
    $user = $disbursement->user;
    $bank = $this->bankRepo->findById($disbursement->bank_id);

    $terminalId = preg_replace('/\\D/', '', $disbursement->user->telephone);
    $terminalId = substr($terminalId, strlen($terminalId) - 8, 8);
    $terminalId = empty($terminalId) ? '1' : $terminalId;

    $pipeline = new Pipeline();

    $pipeline->add(new Task(BalanceUpdater::class, 'updateBalance', [
      'type' => ArtajasaMutationDisbursement::TYPE_TRANSFER,
      'amount' => $disbursement->amount,
      'fee' => ArtajasaDisbursement::COST,
      'disbursement_artajasa_disbursement_id' => $disbursement->disbursement_vendor_reference_id,
    ]));
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'artajasa_disbursement_id' => $disbursement->disbursement_vendor_reference_id,
      'bank_code' => explode('-', $bank->aj_bank_code)[0],
      'regency_code' => $bank->aj_regency_code,
      'transfer_to' => $disbursement->account_number,
      'transfer_to_bank' => $bank->name,
      'amount' => $disbursement->amount,
      'purpose_desc' => $disbursement->description,
      'channel_type' => ArtajasaDisbursement::CHANNEL_INTERNET,
      'user_id' => $disbursement->user_id,
      'user_name' => $disbursement->customer_name ?? '',
      'terminal_id' => $terminalId,
      'sender_prefix' => $user->disbursementApiUser ? $user->disbursementApiUser->alias : ArtajasaDisbursement::ODEO_SENDER_PREFIX,
    ]));

    $pipeline->execute();

    if ($pipeline->success()) {
      $responseCode = '00';
    }
    else {
      $responseCode = $pipeline->errorMessage['response_code'] ?? null;
    }

    $status = $this->artajasaResponseMapper->map($responseCode);
    $ajDisbursement = $this->artajasaDisbursementRepo->findById($disbursement->disbursement_vendor_reference_id);

//    if ($status == ApiDisbursement::SUSPECT && $responseCode == 'EE') {
//      clog('api-disbursement', "DISB #{$disbursement->id}: dispatched auto retry aj");
//      dispatch((new AutoRetryDisbursement($disbursement->id))->delay(10));
//    }

    return [
      'disbursement_id' => $disbursement->id,
      'status' => intval($status),
      'disbursement_vendor' => VendorDisbursement::ARTAJASA,
      'disbursement_vendor_reference_id' => $ajDisbursement->id,
      'disbursement_vendor_updated_at' => strtotime($ajDisbursement->updated_at),
    ];
  }

}