<?php

namespace Odeo\Domains\Disbursement\Withdrawal\Artajasa\Helper;

use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\WithdrawCanceller;


class ArtajasaResponseHandler {

  public function handle($responseCode, $withdrawId, $userId) {
    if (!$responseCode) return;

    $pipeline = new Pipeline;

    switch ($responseCode) {
      case '14': // Invalid card number (no such number)
      case '76': // Invalid to account
      case '77': // Invalid from account
        $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
          'withdraw_id' => $withdrawId,
          'status' => Withdraw::CANCELLED_FOR_WRONG_ACCOUNT_NUMBER
        ]));
        break;
      case '78': // Account is closed
        $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
          'withdraw_id' => $withdrawId,
          'status' => Withdraw::CANCELLED_FOR_CLOSED_BANK_ACCOUNT
        ]));
        break;
      case '01': // Refer to card issuer
      case '03': // Invalid merchant
      case '05': // Do not honor
      case '12': // Invalid transaction
      case '13': // Invalid amount.
      case '15': // No such issuer
      case '20': // Invalid response
      case '30': // Format error
      case '31': // Bank not supported by switch
      case '39': // No credit account
      case '40': // Requested function not supported
      case '52': // No chequing account
      case '53': // No savings account
      case '54': // Expired card
      case '55': // Invalid PIN
      case '57': // Transaction not permitted to cardholder
      case '58': // Transaction not permitted to terminal
      case '61': // Exceeds withdrawal amount limit
      case '62': // Restricted card
      case '63': // Security violation
      case '65': // Exceeds withdrawal frequency limit
      case '75': // Allowable number of PIN tries exceeded
        $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
          'withdraw_id' => $withdrawId,
          'status' => Withdraw::CANCELLED_FOR_BANK_REJECTION
        ]));
        break;
      case '89': // Link to Host down
      case '91': // Issuer, Destination or switch is inoperative
      case '92': // Unable to route transaction
      case '94': // Duplicate transmission / request
      case '96': // System malfunction / system error
      case 'EE': // General error. Details of the error is in the description of the response.
      case 'LD': // Link problem between Gateway and ATM Bersama Network.
      case 'NF': // Transaction has not recorded on Remittance gateway.

      case '51': // Insufficient funds / over credit limit
      case '68': // Response received too late
      case 'TO': // Response time-out from ATM Bersama Network.
      case 'SG': // Signature error code
      case 'IF': // Insufficient deposit
//        $pipeline->add(new Task(ApiDisbursementSuspector::class, 'markAsSuspect'));
        break;
    }

    if ($pipeline->countTask()) {
      $pipeline->execute([
        'user_id' => $userId
      ]);
    }
  }

}