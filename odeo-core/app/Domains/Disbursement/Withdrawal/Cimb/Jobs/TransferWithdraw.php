<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/25/17
 * Time: 7:54 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Cimb\Jobs;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\CimbApi;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Transaction\WithdrawCanceller;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Domains\Vendor\Cimb\Transfer;
use Odeo\Jobs\Job;

class TransferWithdraw extends Job {

  private $data, $userWithdrawRepo, $redis;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->redis = Redis::connection();
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
  }

  public function handle() {

    $data = $this->data;

    if ($withdraw = $this->userWithdrawRepo->findById($data['withdraw_id'])) {

      if ($withdraw->status != Withdraw::ON_PROGRESS_AUTO_TRANSFER) {
        return;
      }
    }

    $pipeline = new Pipeline;
    $namespace = 'odeo_core:auto_wd_lock';
    $withdrawId = $data['withdraw_id'];

    if (!$this->redis->hsetnx($namespace, 'wd_id_' . $withdrawId, 1)) {
      throw new \Exception('trying to execute duplicate withdraw');
    }

    $requestId = CimbApi::WITHDRAW_OCASH_PREFIX . $withdrawId;

    $pipeline->add(new Task(Transfer::class, 'exec', [
      'withdraw_id' => $withdrawId,
      'memo' => $withdraw->is_bank_transfer ? $withdraw->description ?? '' : 'WDOCASH ' . $withdrawId,
      'transaction_key' => 'WD-OCASH#' . $withdrawId,
      'account_number' => $withdraw->account_number,
      'account_name' => $withdraw->account_name,
      'request_id' => $requestId,
      'cimb_disbursement_id' => $data['cimb_disbursement_id']
    ]));

    $pipeline->add(new Task(WithdrawRequester::class, 'complete'));

    $pipeline->execute($data);

    if ($pipeline->success()) {
      $this->redis->hdel($namespace, 'wd_id_' . $withdrawId);
    } else {

      $withdraw->status = Withdraw::FAIL_PROCESS_AUTO_TRANSFER;
      $this->userWithdrawRepo->save($withdraw);

      $errorCode = $pipeline->errorMessage['error_code'] ?? null;

      if (!$errorCode) return;

      $pipeline = $pipeline->clear();

      switch ($errorCode) {
        case '001':
          $pipeline->add(new Task(WithdrawCanceller::class, 'cancelForWrongAccountNumber', [
            'withdraw_id' => $withdrawId
          ]));
          break;
        case '037':
        case '045':
        case '22':
        case '28':
          $pipeline->add(new Task(WithdrawCanceller::class, 'cancelForBankRejection', [
            'withdraw_id' => $withdrawId
          ]));
          break;
      }

      $pipeline->countTask() && $pipeline->execute([
        'user_id' => $withdraw->user_id
      ]);

    }
  }

}
