<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 26/03/19
 * Time: 14.27
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Mandiri\Job;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Transaction\WithdrawCanceller;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Domains\Vendor\Mandiri\Transfer;
use Odeo\Jobs\Job;

class TransferWithdraw extends Job {

  /**
   * @var UserWithdrawRepository
   */
  private $userWithdrawRepo;

  /**
   * @var Redis
   */
  private $redis;

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  private function init() {
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
    $this->redis = Redis::connection();
  }

  public function handle() {
    $this->init();
    $data = $this->data;

    if ($withdraw = $this->userWithdrawRepo->findById($data['withdraw_id'])) {
      if ($withdraw->status != Withdraw::ON_PROGRESS_AUTO_TRANSFER) {
        return;
      }
    }

    $pipeline = new Pipeline;
    $namespace = 'odeo_core:auto_wd_lock';
    $withdrawId = $data['withdraw_id'];

    if (!$this->redis->hsetnx($namespace, 'wd_id_' . $withdrawId, 1)) {
      throw new \Exception('trying to execute duplicate withdraw');
    }

    $pipeline->add(new Task(Transfer::class, 'exec'));
    $pipeline->add(new Task(WithdrawRequester::class, 'complete'));

    $pipeline->execute([
      'withdraw_id' => $withdrawId,
      'remark' => $withdraw->description ?? 'WDOCASH ' . ($withdraw->is_bank_transfer ? '3' : '1') . $withdrawId,
      'transaction_key' => 'WD-OCASH#' . $withdrawId,
      'account_number' => $withdraw->account_number,
      'account_name' => $withdraw->account_name,
      'mandiri_disbursement_id' => $data['mandiri_disbursement_id']
    ]);

    if ($pipeline->success()) {
      $this->redis->hdel($namespace, 'wd_id_' . $withdrawId);
    } else {

      $withdraw->status = Withdraw::FAIL_PROCESS_AUTO_TRANSFER;
      $this->userWithdrawRepo->save($withdraw);

      $errorCode = $pipeline->errorMessage['response_code'] ?? null;

      $pipeline = $pipeline->clear();
      if (!$errorCode) return;

      switch ($errorCode) {
        case '2': // bank rejection
          $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
            'withdraw_id' => $withdrawId,
            'withdraw_status' => Withdraw::CANCELLED_FOR_BANK_REJECTION
          ]));
          break;
        case '3': // no response
        case '8': // on progress
          $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
            'withdraw_id' => $withdrawId,
            'withdraw_status' => Withdraw::SUSPECT_CON
          ]));
          break;
        case '1': // success but invalid signature
      }
      $pipeline->countTask() && $pipeline->execute([
        'user_id' => $withdraw->user_id
      ]);
    }
  }

}