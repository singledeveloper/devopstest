<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/10/17
 * Time: 10:41 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Bca\Helper;

use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Disbursement\Withdrawal\Bca\Jobs\TransferWithdraw;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;

class ExecuteTransfer {

  private $userWithdraws, $bcaDisbursements;

  public function __construct() {
    $this->userWithdraws = app()->make(UserWithdrawRepository::class);
    $this->bcaDisbursements = app()->make(DisbursementBcaDisbursementRepository::class);
  }

  public function execute($withdraw) {
    $bcaId = null;

    if ($withdraw->auto_disbursement_reference_id) {
      $bcaId = $withdraw->auto_disbursement_reference_id;

    } else {
      $bca = $this->bcaDisbursements->getNew();
      $bca->amount = $withdraw->amount - $withdraw->fee;
      $bca->disbursement_type = Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH;
      $bca->disbursement_reference_id = $withdraw->id;
      $bca->status = BcaDisbursement::PENDING;
      $this->bcaDisbursements->save($bca);

      $bcaId = $bca->id;
      $withdraw->auto_disbursement_reference_id = $bca->id;
      $withdraw->auto_disbursement_vendor = Withdraw::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT;
    }

    $withdraw->status = Withdraw::ON_PROGRESS_AUTO_TRANSFER;

    $this->userWithdraws->save($withdraw);

    dispatch(new TransferWithdraw([
      'withdraw_id' => $withdraw->id,
      'bca_disbursement_id' => $bcaId
    ]));
  }

}
