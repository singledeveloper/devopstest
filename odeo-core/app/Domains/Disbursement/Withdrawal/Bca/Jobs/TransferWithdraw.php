<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/25/17
 * Time: 9:51 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Bca\Jobs;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\WithdrawCanceller;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager;
use Odeo\Domains\Vendor\Bca\Transfer;
use Odeo\Jobs\Job;

class TransferWithdraw extends Job  {

  private $data;

  private $userWithdraws;

  private $redis;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;

    $this->redis = Redis::connection();
    $this->userWithdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
  }

  public function handle() {
    if ($withdraw = $this->userWithdraws->findById($this->data['withdraw_id'])) {
      if ($withdraw->status != Withdraw::ON_PROGRESS_AUTO_TRANSFER) {
        return;
      }
    }

    $pipeline = new Pipeline;
    $namespace = 'odeo_core:auto_wd_lock';

    if ($this->redis->hget($namespace, 'wd_id_' . $this->data['withdraw_id'])) {
      return;
    }

    $this->redis->hset($namespace, 'wd_id_' . $withdraw->id, 1);

    $remark = $withdraw->is_bank_transfer ? $withdraw->description ?? '' : 'WDOCASH 1' . $withdraw->id;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'corporate_id' => ApiManager::$CORPORATEID,
      'remark_1' => str_replace(["\t", "\r", "\n"], "", substr($remark, 0, 18)),
      'remark_2' => str_replace(["\t", "\r", "\n"], "", strlen($remark) > 18 ? substr($remark, 18, 18) : ''),
      'transfer_from' => ApiManager::$ACCOUNTNUMBER[0],
      'transfer_to' => $withdraw->account_number,
      'transaction_key' => 'WD-OCASH#' . $this->data['withdraw_id']
    ]));

    $pipeline->add(new Task(WithdrawRequester::class, 'complete'));

    $pipeline->execute($this->data);

    if ($pipeline->success()) {
      $this->redis->hdel($namespace, 'wd_id_' . $withdraw->id);
      return;
    } else {
      $withdraw->status = Withdraw::FAIL_PROCESS_AUTO_TRANSFER;
      $this->userWithdraws->save($withdraw);

      $errorCode = $pipeline->errorMessage['error_code'] ?? null;
      if (!$errorCode) return;

      $pipeline = $pipeline->clear();

      switch ($errorCode) {
        case 'ESB-82-014':
        case 'ESB-99-156':
        case 'ESB-82-012':
          $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
            'withdraw_id' => $withdraw->id,
            'withdraw_status' => Withdraw::CANCELLED_FOR_WRONG_ACCOUNT_NUMBER
          ]));
          break;
        case 'ESB-82-020':
        case 'ESB-82-018':
        case 'ESB-82-022':
          $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
            'withdraw_id' => $withdraw->id,
            'withdraw_status' => Withdraw::CANCELLED_FOR_CLOSED_BANK_ACCOUNT
          ]));
          break;
        case 'ESB-99-157':
          $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
            'withdraw_id' => $withdraw->id,
            'withdraw_status' => Withdraw::CANCELLED_FOR_BANK_REJECTION
          ]));
          break;
      }

      $pipeline->countTask() && $pipeline->execute([
        'user_id' => $withdraw->user_id
      ]);

    }
  }

}
