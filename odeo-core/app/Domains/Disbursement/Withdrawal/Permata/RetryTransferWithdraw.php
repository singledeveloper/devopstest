<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 26/02/19
 * Time: 19.01
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Permata;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\PermataDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Permata\Repository\DisbursementPermataDisbursementRepository;

class RetryTransferWithdraw {

  /**
   * @var UserWithdrawRepository,
   * @var DisbursementPermataDisbursementRepository,
   * @var Redis
   */
  private $userWithdraws, $permataDisbursements, $redis;

  function __construct() {
    $this->userWithdraws = app()->make(UserWithdrawRepository::class);
    $this->permataDisbursements = app()->make(DisbursementPermataDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if (!($withdraw = $this->userWithdraws->findById($data['withdraw_id']))) return $listener->response(400);
    if ($withdraw->status == Withdraw::COMPLETED) return $listener->response(400);

    if (!($permataDisbursement = $this->permataDisbursements->findById($data['permata_disbursement_id']))) return $listener->response(400);
    if ($permataDisbursement->status == PermataDisbursement::STATUS_SUCCESS) return $listener->response(400);

    $this->redis->hdel('odeo_core:permata_disbursement_lock', 'id_' . $data['permata_disbursement_id']);
    $this->redis->hdel('odeo_core:auto_wd_lock', 'wd_id_' . $data['withdraw_id']);

    $permataDisbursement->cust_reff_id = null;
    $permataDisbursement->status = PermataDisbursement::STATUS_PENDING;
    $permataDisbursement->status_code = null;
    $permataDisbursement->status_desc = null;
    $permataDisbursement->request_timestamp = null;
    $permataDisbursement->response_timestamp = null;
    $permataDisbursement->response_datetime = null;
    $permataDisbursement->ex_error_message = null;
    $permataDisbursement->error_log_response = null;

    $this->permataDisbursements->save($permataDisbursement);

    $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER;
    $this->userWithdraws->save($withdraw);

    return $listener->response(200);
  }

}
