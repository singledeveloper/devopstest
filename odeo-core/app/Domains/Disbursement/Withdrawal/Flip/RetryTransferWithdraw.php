<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/8/17
 * Time: 11:07 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Flip;


use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\FlipDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\PipelineListener;
use Illuminate\Support\Facades\Redis;

class RetryTransferWithdraw {

  private $userWithdraws, $flipDisbursements, $redis;

  public function __construct() {

    $this->userWithdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);

    $this->flipDisbursements = app()->make(\Odeo\Domains\Vendor\Flip\Repository\DisbursementFlipDisbursementRepository::class);

    $this->redis = Redis::connection();

  }


  public function retry(PipelineListener $listener, $data) {

    if (!($withdraw = $this->userWithdraws->findById($data['withdraw_id']))) return $listener->response(400);
    if ($withdraw->status == Withdraw::COMPLETED) return $listener->response(400);

    if (!($flipDisbursement = $this->flipDisbursements->findById($data['flip_disbursement_id']))) return $listener->response(400);
    if ($flipDisbursement->status == FlipDisbursement::SUCCESS) return $listener->response(400);

    $this->redis->hdel('odeo_core:flip_disbursement_lock', 'id_' . $data['flip_disbursement_id']);
    $this->redis->hdel('odeo_core:auto_wd_lock', 'wd_id_' . $data['withdraw_id']);

    if (strlen($flipDisbursement->transaction_key) || strlen($flipDisbursement->bca_status) || strlen($flipDisbursement->response_datetime)) {
      $flipDisbursement->transaction_key = null;
      $flipDisbursement->flip_status = null;
      $flipDisbursement->response_datetime = null;
      $this->flipDisbursements->save($flipDisbursement);
    }

    if ($withdraw->status != Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
      $withdraw->status = Withdraw::PENDING_PROGRESS_AUTO_TRANSFER;
      $this->userWithdraws->save($withdraw);
    }

    return $listener->response(200);
  }

}