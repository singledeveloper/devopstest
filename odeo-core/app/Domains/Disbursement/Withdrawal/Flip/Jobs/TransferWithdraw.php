<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/27/17
 * Time: 4:58 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Flip\Jobs;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\FlipDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\WithdrawCanceller;
use Odeo\Domains\Vendor\Flip\Jobs\SendFlipIsOnMaintenance;
use Odeo\Domains\Vendor\Flip\Jobs\SendFlipNotEnoughBalanceAlert;
use Odeo\Domains\Vendor\Flip\Transfer;
use Odeo\Jobs\Job;

class TransferWithdraw extends Job {

  private $data;

  private $userWithdraws;

  private $redis;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;

    $this->redis = Redis::connection();
    $this->userWithdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
  }

  public function handle() {

    if ($withdraw = $this->userWithdraws->findById($this->data['withdraw_id'])) {

      if ($withdraw->status != Withdraw::ON_PROGRESS_AUTO_TRANSFER) {
        return;
      }
    }

    $pipeline = new Pipeline;

    $namespace = 'odeo_core:auto_wd_lock';

    if ($this->redis->hget($namespace, 'wd_id_' . $withdraw->id)) {
      return;
    }

    $this->redis->hset($namespace, 'wd_id_' . $withdraw->id, 1);

    $pipeline->add(new Task(Transfer::class, 'exec', [
      'account_number' => $withdraw->account_number,
      'account_bank' => FlipDisbursement::getFlipDisbursementBankName($this->data['bank_id']),
      'note' => 'WDOCASH ' . $withdraw->id,
      'user_id' => $withdraw->user_id,
      'reference_id' => $withdraw->id,
      'reference_type' => 'withdraw',
      'transaction_key' => 'WD-OCASH#' . $withdraw->id,
      'fee' => $this->data['fee']
    ]));

    $pipeline->execute($this->data);

    if ($pipeline->success()) {

      $this->redis->hdel($namespace, 'wd_id_' . $withdraw->id);
      $withdraw->status = Withdraw::WAIT_FOR_NOTIFY;
      $this->userWithdraws->save($withdraw);

      return;
    } else {

      $withdraw->status = Withdraw::FAIL_PROCESS_AUTO_TRANSFER;
      $this->userWithdraws->save($withdraw);

      $errorCode = $pipeline->errorMessage['error_code'] ?? null;

      if (!$errorCode) return;

      $pipeline = $pipeline->clear();

      switch ($errorCode) {

        case 'BALANCE_INSUFFICIENT':

          dispatch(new SendFlipNotEnoughBalanceAlert([
            'user_id' => $withdraw->user_id,
            'reference_id' => $withdraw->id,
            'reference_type' => 'withdraw',
            'requested_amount' => $withdraw->amount - $withdraw->fee
          ]));

          break;

        case 'FLIP_ON_MAINTENANCE':

          dispatch(new SendFlipIsOnMaintenance([
            'user_id' => $withdraw->user_id,
            'reference_id' => $withdraw->id,
            'reference_type' => 'withdraw',
            'requested_amount' => $withdraw->amount - $withdraw->fee
          ]));

          break;

        case 'VALIDATION_ERROR':
          $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
            'withdraw_id' => $withdraw->id,
            'withdraw_status' => Withdraw::CANCELLED_FOR_WRONG_ACCOUNT_NUMBER
          ]));
          break;

      }

      $pipeline->countTask() && $pipeline->execute([
        'withdraw_id' => $withdraw->id
      ]);

    }


  }

}
