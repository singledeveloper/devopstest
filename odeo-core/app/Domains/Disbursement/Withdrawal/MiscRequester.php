<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/11/17
 * Time: 3:42 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Core\PipelineListener;

class MiscRequester {

  private $redis;

  public function __construct() {

    $this->redis = Redis::connection();

  }

  public function removeRedisLock(PipelineListener $listener, $data) {
    $this->redis->hdel('odeo_core:auto_wd_lock', 'wd_id_' . $data['withdraw_id']);
    return $listener->response(200);
  }

}