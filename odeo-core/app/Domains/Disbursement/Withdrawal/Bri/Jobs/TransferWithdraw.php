<?php

namespace Odeo\Domains\Disbursement\Withdrawal\Bri\Jobs;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Transaction\WithdrawCanceller;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Domains\Vendor\Bri\Helper\ApiManager;
use Odeo\Domains\Vendor\Bri\Helper\BriResponse;
use Odeo\Domains\Vendor\Bri\Transfer;
use Odeo\Jobs\Job;

class TransferWithdraw extends Job {

  private $data;

  private $userWithdrawRepo, $redis;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->redis = Redis::connection();
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
  }

  public function handle() {

    if ($withdraw = $this->userWithdrawRepo->findById($this->data['withdraw_id'])) {

      if ($withdraw->status != Withdraw::ON_PROGRESS_AUTO_TRANSFER) {
        return;
      }
    }

    $namespace = 'odeo_core:auto_wd_lock';

    if (!$this->redis->hsetnx($namespace, 'wd_id_' . $this->data['withdraw_id'], 1)) {
      return;
    }

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'remark' => $withdraw->description ?? 'WDOCASH ' . ($withdraw->is_bank_transfer ? '3' : '1') . $withdraw->id,
      'transfer_from' => ApiManager::$accountNumber,
      'transfer_to' => $withdraw->account_number
    ]));
    $pipeline->add(new Task(WithdrawRequester::class, 'complete'));
    $pipeline->execute($this->data);

    if ($pipeline->success()) {
      $this->redis->hdel($namespace, 'wd_id_' . $withdraw->id);
      return;
    } else {

      $withdraw->status = Withdraw::FAIL_PROCESS_AUTO_TRANSFER;
      $this->userWithdrawRepo->save($withdraw);

      $errorCode = $pipeline->errorMessage['response_code'] ?? null;
      $errorDesc = $pipeline->errorMessage['error_desc'] ?? null;

      if (!$errorCode) return;

      $pipeline = $pipeline->clear();

      if (
        stripos($errorDesc, BriResponse::DUPLICATE_JOURNAL) !== false ||
        stripos($errorDesc, BriResponse::INSUFFICIENT_AMOUNT) !== false ||
        stripos($errorDesc, BriResponse::VA_NOT_FOUND) !== false ||
        stripos($errorDesc, BriResponse::REFERENCE_USED) !== false
      ) {
        $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
          'withdraw_id' => $withdraw->id,
          'withdraw_status' => Withdraw::CANCELLED_FOR_BANK_REJECTION
        ]));
      }

      $pipeline->countTask() && $pipeline->execute([
        'user_id' => $withdraw->user_id
      ]);
    }
  }

}
