<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;

class BatchDisbursementGuard {

  private $batchDisbursementRepo;
  private $apiDisbursementRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
  }

  public function guard(PipelineListener $listener, $data) {
    $batch = $this->batchDisbursementRepo->findById($data['batch_disbursement_id']);
    if (!$batch || $batch->user_id != $data['auth']['user_id']) {
      return $listener->response(400, trans('errors.data_not_exists'));
    }

    $listener->appendData([
      'batch_disbursement' => $batch,
    ]);
    return $listener->response(200);
  }

  public function guardDetail(PipelineListener $listener, $data) {
    $disb = $this->apiDisbursementRepo->findById($data['disbursement_id']);
    if (!$disb || $disb->batch_disbursement_id != $data['batch_disbursement_id'] || $disb->user_id != $data['auth']['user_id']) {
      return $listener->response(400, trans('errors.data_not_exists'));
    }

    $listener->appendData([
      'api_disbursement' => $disb,
    ]);
    return $listener->response(200);
  }
}