<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Artajasa\Helper;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\AutoRetryDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\BalanceUpdater;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Transfer;

class ExecuteArtajasaDisbursement {

  private $apiDisbursementRepo, $artajasaDisbursementRepo, $artajasaResponseMapper, $bankRepo, $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
    $this->artajasaResponseMapper = app()->make(ArtajasaResponseMapper::class);
    $this->bankRepo = app()->make(BankRepository::class);
    $this->redis = Redis::connection();
  }

  public function exec($disbursement) {
    $this->init($disbursement);

    $user = $disbursement->user;
    $bank = $this->bankRepo->findById($disbursement->bank_id);

    $terminalId = preg_replace('/\\D/', '', $disbursement->user->telephone);
    $terminalId = substr($terminalId, strlen($terminalId) - 8, 8);
    $terminalId = empty($terminalId) ? '1' : $terminalId;

    $pipeline = new Pipeline();

    $pipeline->add(new Task(BalanceUpdater::class, 'updateBalance', [
      'type' => ArtajasaMutationDisbursement::TYPE_TRANSFER,
      'amount' => $disbursement->amount,
      'fee' => ArtajasaDisbursement::COST,
      'disbursement_artajasa_disbursement_id' => $disbursement->auto_disbursement_reference_id,
    ]));
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'artajasa_disbursement_id' => $disbursement->auto_disbursement_reference_id,
      'bank_code' => explode('-', $bank->aj_bank_code)[0],
      'regency_code' => $bank->aj_regency_code,
      'transfer_to' => $disbursement->account_number,
      'transfer_to_bank' => $bank->name,
      'amount' => $disbursement->amount,
      'purpose_desc' => $disbursement->description,
      'channel_type' => ArtajasaDisbursement::CHANNEL_INTERNET,
      'user_id' => $disbursement->user_id,
      'user_name' => $disbursement->customer_name ?? '',
      'terminal_id' => $terminalId,
      'sender_prefix' => $user->disbursementApiUser->alias,
    ]));

    $pipeline->execute([
      'disbursement_id' => $disbursement->id
    ]);

    $transferred = true;

    if ($pipeline->success()) {
      $responseCode = '00';
      $this->updateAccountName($disbursement);
    } else {
      $responseCode = $pipeline->errorMessage['response_code'] ?? null;
      $transferred = isset($pipeline->errorMessage['transferred']) ? $pipeline->errorMessage['transferred'] : true;
    }

    $status = $this->artajasaResponseMapper->map($responseCode);

    if (!$transferred && $status == ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS) {
      $status = ApiDisbursement::SUSPECT;
    }

    if ($status == ApiDisbursement::SUSPECT && !$transferred) {
      clog('api-disbursement', "DISB #{$disbursement->id}: dispatched auto retry aj");
      dispatch((new AutoRetryDisbursement($disbursement->id))->delay(10));
    }

    return $status;
  }

  private function init($disbursement) {
    if (!$disbursement->auto_disbursement_reference_id
      || $disbursement->auto_disbursement_vendor != ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT) {
      $artajasa = $this->artajasaDisbursementRepo->getNew();
      $artajasa->disbursement_type = Disbursement::API_DISBURSEMENT;
      $artajasa->disbursement_reference_id = $disbursement->id;
      $artajasa->status = ArtajasaDisbursement::PENDING;

      $this->artajasaDisbursementRepo->save($artajasa);

      $disbursement->auto_disbursement_reference_id = $artajasa->id;
      $disbursement->auto_disbursement_vendor = ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT;
    }

    $disbursement->cost = ArtajasaDisbursement::COST;
    $disbursement->status = ApiDisbursement::ON_PROGRESS;
    $this->apiDisbursementRepo->save($disbursement);
  }

  private function updateAccountName($disbursement) {
    $ajDisbursement = $this->artajasaDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
    $disbursement->account_name = $ajDisbursement->transfer_to_name;
    $this->apiDisbursementRepo->save($disbursement);
  }

}