<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Helper\BankAccountParser;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDisbursementInquiryReport;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementInquiryRepository;

class ApiDisbursementInquirySelector {

  private $apiDisbursementInquiryRepo;

  public function __construct() {
    $this->apiDisbursementInquiryRepo = app()->make(ApiDisbursementInquiryRepository::class);
    $this->bankAccountParser = app()->make(BankAccountParser::class);
  }

  public function transforms($item, Repository $repository) {
    $inquiry = [
      'id' => $item->id,
      'bank_id' => $item->bank_id,
      'account_number' => $item->account_number,
      'account_name' => $item->account_name,
      'customer_name' => $item->customer_name,
      'fee' => $item->fee,
      'status' => $item->status,
      'message' => ApiDisbursement::getStatusMessage($item->status),
      'created_at' => $item->created_at->timestamp,
      'created_at_date' => $item->created_at->format('Y-m-d H:i:s')
    ];

    if ($repository->hasExpand('bank')) {
      $inquiry['bank'] = [
        'name' => $item->bank->name,
      ];
    }

    if ($repository->hasExpand('validation')) {
      $inquiry['validity'] = $this->bankAccountParser->getValidity($item->account_name, $item->customer_name);
    }
    
    return $inquiry;
  }

  public function get(PipelineListener $listener, $data) {
    $this->apiDisbursementInquiryRepo->normalizeFilters($data);

    $inquiries = $this->apiDisbursementInquiryRepo->getAllByUserId($data['auth']['user_id']);
    $result = [];

    foreach ($inquiries as $item) {
      $temp = $this->transforms($item, $this->apiDisbursementInquiryRepo);
      unset($temp['account_name']);
      $result[] = $temp;
    }

    if (sizeof($result) > 0) {
      return $listener->response(200, array_merge(
        ['inquiries' => $result],
        $this->apiDisbursementInquiryRepo->getPagination()
      ));
    }

    return $listener->response(204, ['inquiries' => []]);
  }

  public function export(PipelineListener $listener, $data) {
    $userId = getUserId();

    if (Redis::get("disbursement_inquiry_export_" . $userId)) {
      return $listener->response(400, trans('user.order_report_already_exported'));
    }

    $filters = [
      'start_date' => $data['start_date'],
      'end_date' => $data['end_date'],
      'file_type' => $data['file_type'] ?? 'csv',
      'user_id' => $userId
    ];

    Redis::setex("disbursement_inquiry_export_" . $userId, 3600, 1);

    $exportId = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class)->create($data);

    $listener->pushQueue(new SendDisbursementInquiryReport($filters, $data['email'], $exportId));

    return $listener->response(200, [
      'export_id' => $exportId
    ]);
  }
}