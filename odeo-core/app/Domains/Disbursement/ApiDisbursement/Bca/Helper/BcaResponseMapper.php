<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/04/19
 * Time: 16.53
 */

namespace Odeo\Domains\Disbursement\ApiDisbursement\Bca\Helper;


use Odeo\Domains\Constant\ApiDisbursement;

class BcaResponseMapper {

  public function map($responseCode) {
    if ($responseCode == '00') {
      return ApiDisbursement::COMPLETED;
    }

    switch ($responseCode) {
      case 'ESB-82-014':
      case 'ESB-99-156':
      case 'ESB-82-012':
        return ApiDisbursement::FAILED_WRONG_ACCOUNT_NUMBER;

      case 'ESB-82-020':
      case 'ESB-82-018':
      case 'ESB-82-022':
        return ApiDisbursement::FAILED_CLOSED_BANK_ACCOUNT;

      case 'ESB-82-019':
      case 'ESB-99-157':
        return ApiDisbursement::FAILED_BANK_REJECTION;

      case 'TIMEOUT':
      default:
        return ApiDisbursement::SUSPECT;
    }
  }

}