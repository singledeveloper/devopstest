<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Redig;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\RedigDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\Redig\BalanceUpdater;
use Odeo\Domains\Vendor\Redig\Repository\DisbursementRedigDisbursementRepository;

class RetryRedigDisbursement {

  private $apiDisbursementRepo, $redigDisbursementRepo, $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->redigDisbursementRepo = app()->make(DisbursementRedigDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if (!($disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']))) {
      return $listener->response(400, 'Disbursement not found');
    }

    if ($disbursement->status != ApiDisbursement::SUSPECT) {
      return $listener->response(400, 'Disbursement status is not suspect');
    }

    if (!($redigDisbursement = $this->redigDisbursementRepo->findById($disbursement->auto_disbursement_reference_id))) {
      return $listener->response(400, 'Redig Disbursement not found');
    }

    if ($redigDisbursement->status == RedigDisbursement::SUCCESS) {
      return $listener->response(400, 'Redig Disbursement status is success');
    }

    $this->redis->hdel('odeo_core:api_disbursement_lock', 'id_' . $data['disbursement_id']);

    if ($redigDisbursement->status != RedigDisbursement::PENDING) {
      $listener->addNext(new Task(BalanceUpdater::class, 'refundDisbursement', [
        'redig_disbursement_id' => $redigDisbursement->id,
        'amount' => $disbursement->amount,
        'cost' => RedigDisbursement::COST,
      ]));
    }

    if ($disbursement->status != ApiDisbursement::PENDING) {
      $disbursement->status = ApiDisbursement::PENDING;
      $this->apiDisbursementRepo->save($disbursement);
    }

    $listener->pushQueue(new ExecuteDisbursement($disbursement->id));

    return $listener->response(200);
  }

}
