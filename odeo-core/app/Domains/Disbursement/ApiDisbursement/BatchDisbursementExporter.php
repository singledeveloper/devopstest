<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Carbon\Carbon;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BatchDisbursementStatus;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteBatchDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;

class BatchDisbursementExporter {

  private $batchDisbursementRepo;
  private $apiDisbursementRepo;
  private $cashInserter;
  private $disbursementApiUserRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
  }

  public function export(PipelineListener $listener, $data) {
    return $listener->response(200);
  }

  public function exportFailed(PipelineListener $listener, $data) {

  }
}