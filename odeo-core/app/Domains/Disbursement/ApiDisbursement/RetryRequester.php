<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\Artajasa\RetryArtajasaDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bca\RetryBcaDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bni\RetryBniDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bri\RetryBriDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Cimb\RetryCimbDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteRetryDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Mandiri\RetryMandiriDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Permata\RetryPermataDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Redig\RetryRedigDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;

class RetryRequester {

  private $apiDisbursementRepo, $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if ($data['password'] != env('API_DISBURSEMENT_RETRY_PASSWORD')) {
      return $listener->response(400, trans('errors.invalid_password'));
    }

    $disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']);

    if (!$disbursement) {
      return $listener->response(400, trans('errors.data_not_exists'));
    }

    if ($disbursement->status == ApiDisbursement::COMPLETED && $disbursement->delayed_at == null) {
      return $listener->response(400, 'Disbursement status is completed and its not a delayed success');
    }

    if ($disbursement->status == ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS) {
      return $listener->response(400, 'Status suspect, silakan info ke tim dev');
    }

    if (!$this->lockRetry($disbursement->id)) {
      return $listener->response(400, 'Silakan coba retry setelah menunggu 5 menit karena sudah pernah retry barusan');
    }

    if ($data['preferred_vendor'] != null) {
      $this->redis->hset('odeo_core:api_disbursement_preference', $data['disbursement_id'], $data['preferred_vendor']);
    } else {
      $this->redis->hdel('odeo_core:api_disbursement_preference', $data['disbursement_id']);
    }

    switch ($disbursement->auto_disbursement_vendor) {
      case ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT:
        $listener->addNext(new Task(RetryArtajasaDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT:
        $listener->addNext(new Task(RetryBcaDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT:
        $listener->addNext(new Task(RetryBniDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT:
        $listener->addNext(new Task(RetryBriDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_CIMB_API_DISBURSEMENT:
        $listener->addNext(new Task(RetryCimbDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT:
        $listener->addNext(new Task(RetryMandiriDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT:
        $listener->addNext(new Task(RetryPermataDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_REDIG_API_DISBURSEMENT:
        $listener->addNext(new Task(RetryRedigDisbursement::class, 'retry'));
        break;

      default:
        return $listener->response(400, 'Unknown disbursement vendor');
    }

    return $listener->response(200);
  }

  public function bulkRetry(PipelineListener $listener, $data) {
    if ($data['password'] != env('API_DISBURSEMENT_BULK_RETRY_PASSWORD')) {
      return $listener->response(400, trans('errors.invalid_password'));
    }

    $ids = explode(';', $data['disbursement_ids']);
    $invalidIds = '';

    if (count($ids) != count(array_filter($ids, 'is_numeric'))) {
      return $listener->response(400, 'Invalid Id Input');
    }

    if ($disbursements = $this->apiDisbursementRepo->getAllByIdList($ids)) {
      foreach ($disbursements as $disbursement) {
        if ($disbursement->status == ApiDisbursement::SUSPECT) {
          dispatch(new ExecuteRetryDisbursement($disbursement->id, $data['preferred_vendor']));
        } else {
          $invalidIds .= $disbursement->id . ';';
        }
      }

      $message = 'Processing retry, note your disbursement ids and check again later. ';
      if ($invalidIds != '') {
        $message .= $invalidIds . ' status are invalid when retrying';
        clog('bulk-retry-disbursement', $message);
      }
      return $listener->response(200);
    }

    return $listener->response(400, 'Unknown disbursement');
  }


  private function lockRetry($id) {
    $key = 'odeo_core:disbursement_retry_lock_' . $id;
    if (!$this->redis->setnx($key, 1)) {
      return false;
    }
    $this->redis->expire($key, 5 * 60);
    return true;
  }
}