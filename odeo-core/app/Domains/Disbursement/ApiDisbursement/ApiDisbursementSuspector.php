<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\NotifyApiDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDisbursementSuspectAlert;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;

class ApiDisbursementSuspector {

  private $apiDisbursementRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
  }

  public function markAsSuspect(PipelineListener $listener, $data) {
    $newStatus = $data['disbursement_status'];
    $disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']);

    if (!$disbursement) {
      return $listener->response(400, trans('errors.database'));
    }

    $previousStatus = $disbursement->status;
    $disbursement->status = $newStatus;

    if (!$this->apiDisbursementRepo->save($disbursement)) {
      return $listener->response(400, trans('errors.database'));
    }

    $listener->pushQueue(new SendDisbursementSuspectAlert());

    if ($previousStatus != ApiDisbursement::SUSPECT && $previousStatus != ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS) {
      $listener->pushQueue(new NotifyApiDisbursement($disbursement->id));
    }

    return $listener->response(200);
  }
}
