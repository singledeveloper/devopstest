<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 05-Apr-19
 * Time: 4:16 PM
 */

namespace Odeo\Domains\Disbursement\ApiDisbursement\Mandiri;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\MandiriDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;

class RetryMandiriDisbursement {
  private $apiDisbursementRepo, $mandiriDisbursementRepo, $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->mandiriDisbursementRepo = app()->make(DisbursementMandiriDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if (!($disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']))) {
      return $listener->response(400, 'Disbursement not found');
    }

    if ($disbursement->status == ApiDisbursement::COMPLETED) {
      return $listener->response(400, 'Disbursement status is completed');
    }

    if (!($mandiriDisbursement = $this->mandiriDisbursementRepo->findById($disbursement->auto_disbursement_reference_id))) {
      return $listener->response(400, 'Mandiri Disbursement not found');
    }

    if ($mandiriDisbursement->status == MandiriDisbursement::SUCCESS) {
      return $listener->response(400, 'Mandiri Disbursement status is success');
    }

    $this->redis->hdel('odeo_core:mandiri_disbursement_lock', 'id_' . $mandiriDisbursement->id);
    $this->redis->hdel('odeo_core:api_disbursement_lock', 'id_' . $data['disbursement_id']);

    $mandiriDisbursement->cust_reff_no = null;
    $mandiriDisbursement->status = MandiriDisbursement::PENDING;
    $mandiriDisbursement->status_code = null;
    $mandiriDisbursement->response_code = null;
    $mandiriDisbursement->response_message = null;
    $mandiriDisbursement->request_time = null;
    $mandiriDisbursement->response_timestamp = null;
    $mandiriDisbursement->response_remmittance_no = null;
    $mandiriDisbursement->is_sign_verified = false;
    $mandiriDisbursement->response_error_message = null;
    $mandiriDisbursement->response_error_number = null;
    $mandiriDisbursement->ex_error_message = null;

    $this->mandiriDisbursementRepo->save($mandiriDisbursement);

    $disbursement->status = ApiDisbursement::PENDING;
    $this->apiDisbursementRepo->save($disbursement);

    $listener->pushQueue(new ExecuteDisbursement($disbursement->id));

    return $listener->response(200);
  }
}