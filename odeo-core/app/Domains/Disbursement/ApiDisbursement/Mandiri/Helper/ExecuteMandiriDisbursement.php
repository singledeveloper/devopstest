<?php
namespace Odeo\Domains\Disbursement\ApiDisbursement\Mandiri\Helper;
use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\MandiriDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;
use Odeo\Domains\Vendor\Mandiri\Transfer;

/**
 * Created by PhpStorm.
 * User: William
 * Date: 05-Apr-19
 * Time: 3:53 PM
 */

class ExecuteMandiriDisbursement {
  private $apiDisbursementRepo, $mandiriDisbursementRepo, $bankAccountInquirer, $disbursementApiUserRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->mandiriDisbursementRepo = app()->make(DisbursementMandiriDisbursementRepository::class);
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
  }

  public function exec($disbursement) {
    $this->init($disbursement);

    $inquiryStatus = $this->inquiry($disbursement);
    if ($inquiryStatus != ApiDisbursement::COMPLETED) {
      $mandiri = $this->mandiriDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
      $mandiri->status = MandiriDisbursement::FAIL;
      $mandiri->status_code = MandiriDisbursement::RESPONSE_CODE_INQUIRY_FAILED;
      $this->mandiriDisbursementRepo->save($mandiri);

      return $inquiryStatus;
    }

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'amount' => $disbursement->amount,
      'mandiri_disbursement_id' => $disbursement->auto_disbursement_reference_id,
      'account_number' => $disbursement->account_number,
      'remark' => 'WDOCASH 2' . $disbursement->id,
      'account_name' => $disbursement->account_name,
      'transaction_key' => 'WDOCASH 2' . $disbursement->id
    ]));

    $pipeline->execute();

    return $this->getDisbursementStatus($pipeline);
  }

  private function init($disbursement) {
    if (!$disbursement->auto_disbursement_reference_id
      || $disbursement->auto_disbursement_vendor != ApiDisbursement::DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT) {
      $mandiri = $this->mandiriDisbursementRepo->getNew();
      $mandiri->amount = $disbursement->amount;
      $mandiri->disbursement_type = Disbursement::API_DISBURSEMENT;
      $mandiri->disbursement_reference_id = $disbursement->id;
      $mandiri->status = MandiriDisbursement::PENDING;
      $this->mandiriDisbursementRepo->save($mandiri);

      $disbursement->auto_disbursement_reference_id = $mandiri->id;
      $disbursement->auto_disbursement_vendor = ApiDisbursement::DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT;
    }

    $disbursement->cost = MandiriDisbursement::DISBURSEMENT_COST;
    $disbursement->status = ApiDisbursement::ON_PROGRESS;
    $this->apiDisbursementRepo->save($disbursement);
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return ApiDisbursement::COMPLETED;
    }

    switch ($pipeline->errorMessage['response_code'] ?? null) {
      case '3': // no response
      case '8': // in progress
      case '1':
      default:
        return ApiDisbursement::SUSPECT;
        break;
    }
  }

  private function inquiry($disbursement) {
    try {
      $apiUser = $this->disbursementApiUserRepo->findByUserId($disbursement->user_id);

      list($bankInquiry, $inquiryStatus) = $this->bankAccountInquirer
        ->inquire($disbursement->bank_id, $disbursement->account_number, $disbursement->user_id, [
          'timeout' => 50,
          'sender_prefix' => $apiUser->alias,
          'customer_name' => $disbursement->customer_name,
          'reference_id' => $disbursement->id,
          'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_API_DISBURSEMENT,
        ]);

      if ($inquiryStatus == BankAccountInquiryStatus::COMPLETED) {
        $disbursement->account_name = $bankInquiry->account_name;
        $this->apiDisbursementRepo->save($disbursement);
      }

      return BankAccountInquiryStatus::toApiDisbursementStatus($inquiryStatus);
    } catch (\Exception $e) {
      \Log::error($e);
      return ApiDisbursement::FAILED;
    }
  }
}