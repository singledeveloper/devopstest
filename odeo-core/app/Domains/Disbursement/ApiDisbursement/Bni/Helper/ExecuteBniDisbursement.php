<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Bni\Helper;

use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\BniDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\AutoRetryDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Vendor\Bni\Helper\ApiManager;
use Odeo\Domains\Vendor\Bni\Transfer;
use Odeo\Domains\Vendor\Bni\Repository\DisbursementBniDisbursementRepository;

class ExecuteBniDisbursement {

  private $apiDisbursementRepo, $bniDisbursementRepo, $bankAccountInquirer, $disbursementApiUserRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->bniDisbursementRepo = app()->make(DisbursementBniDisbursementRepository::class);
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
  }

  public function exec($disbursement) {
    $this->init($disbursement);

    $inquiryStatus = $this->inquiry($disbursement);
    if ($inquiryStatus != ApiDisbursement::COMPLETED) {
      $bni = $this->bniDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
      $bni->status = BniDisbursement::FAIL;
      $bni->response_code = BniDisbursement::RESPONSE_CODE_INQUIRY_FAILED;
      $this->bniDisbursementRepo->save($bni);

      return $inquiryStatus;
    }

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'bni_disbursement_id' => $disbursement->auto_disbursement_reference_id,
      'remark' => 'WDOCASH 2' . $disbursement->id,
      'transfer_from' => ApiManager::getConfig('account_number'),
      'transfer_to' => $disbursement->account_number,
    ]));

    $pipeline->execute();
    $status = $this->getDisbursementStatus($pipeline);

    if ($status == ApiDisbursement::SUSPECT) {
      clog('api-disbursement', "DISB #{$disbursement->id}: bni suspect, response_code " . $pipeline->errorMessage['response_code']);
    }

    if ($status == ApiDisbursement::SUSPECT && $this->canRetry($pipeline->errorMessage['response_code'])) {
      clog('api-disbursement', "DISB #{$disbursement->id}: dispatched auto retry bni");
      dispatch((new AutoRetryDisbursement($disbursement->id))->delay(5));
    }

    return $status;
  }

  private function init($disbursement) {
    if (!$disbursement->auto_disbursement_reference_id
      || $disbursement->auto_disbursement_vendor != ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT) {
      $bni = $this->bniDisbursementRepo->getNew();
      $bni->amount = $disbursement->amount;
      $bni->disbursement_type = Disbursement::API_DISBURSEMENT;
      $bni->disbursement_reference_id = $disbursement->id;
      $bni->status = BniDisbursement::PENDING;
      $this->bniDisbursementRepo->save($bni);

      $disbursement->auto_disbursement_reference_id = $bni->id;
      $disbursement->auto_disbursement_vendor = ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT;
    }

    $disbursement->cost = BniDisbursement::COST;
    $disbursement->status = ApiDisbursement::ON_PROGRESS;
    $this->apiDisbursementRepo->save($disbursement);
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return ApiDisbursement::COMPLETED;
    }

    if (isset($pipeline->errorMessage['response_code']) && $pipeline->errorMessage['response_code'] == '0110') {
      return ApiDisbursement::FAILED_CLOSED_BANK_ACCOUNT;
    }

    return ApiDisbursement::SUSPECT;
  }

  private function canRetry($responseCode)
  {
    switch ($responseCode) {
      case '0002': // Request has been rejected by Icons
      case '0007': // Request has been processed unsuccessfully
      case '0169': // Account number not found (tapi inquiry berhasil)
        return true;

      default:
        return false;
    }
  }

  private function inquiry($disbursement) {
    try {
      $apiUser = $this->disbursementApiUserRepo->findByUserId($disbursement->user_id);

      list($bankInquiry, $inquiryStatus) = $this->bankAccountInquirer
        ->inquire($disbursement->bank_id, $disbursement->account_number, $disbursement->user_id, [
          'timeout' => 50,
          'sender_prefix' => $apiUser->alias,
          'customer_name' => $disbursement->customer_name,
          'reference_id' => $disbursement->id,
          'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_API_DISBURSEMENT,
        ]);

      if ($inquiryStatus == BankAccountInquiryStatus::COMPLETED) {
        $disbursement->account_name = $bankInquiry->account_name;
        $this->apiDisbursementRepo->save($disbursement);
      }

      return BankAccountInquiryStatus::toApiDisbursementStatus($inquiryStatus);
    } catch (\Exception $e) {
      \Log::error($e);
      return ApiDisbursement::FAILED;
    }
  }
}