<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\CashManager;
use Odeo\Domains\Transaction\Helper\Currency;

class UserBalanceSelector {

  private $cashManager;

  public function __construct() {
    $this->cashManager = app()->make(CashManager::class);
  }

  public function get(PipelineListener $listener, $data) {
    $cash = $this->cashManager->getCashBalance($data['auth']['user_id'], $data['currency'] ?? Currency::IDR);

    return $listener->response(200, [
      'balance' => $cash[$data['auth']['user_id']],
    ]);
  }

}