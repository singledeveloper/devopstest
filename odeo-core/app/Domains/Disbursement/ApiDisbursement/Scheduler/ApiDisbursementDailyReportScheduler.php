<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Scheduler;

use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDisbursementUsageReport;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;

class ApiDisbursementDailyReportScheduler {

  private $disbursementApiUserRepo;

  public function __construct() {
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
  }

  public function run() {
    $users = $this->disbursementApiUserRepo->getAll();
    $date = date("Y-m-d", time() - 60 * 60 * 24);

    foreach ($users as $user) {
      $email = $user->user->email;

      if (empty($email)) {
        continue;
      }

      dispatch(new SendDisbursementUsageReport($user->user_id, $date, $email));
    }
  }
}