<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Transaction\Helper\CashManager;
use Odeo\Domains\Transaction\Helper\Currency;

class ApiDisbursementAdminSelector {

  private $apiDisbursementRepo;
  private $cashManager;
  private $currency;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->cashManager = app()->make(CashManager::class);
    $this->currency = app()->make(Currency::class);
  }

  public function transforms($item, Repository $repository, $cashMap = []) {
    $disbursement = [];

    if ($repository->hasExpand('user') && $user = $item->user) {
      $disbursement["user"] = [
        "id" => $user->id,
        "name" => $user->name,
        "email" => $user->email,
        "telephone" => $user->telephone
      ];
    }

    if ($repository->hasExpand('cash')) {
      $disbursement["cash"] = $cashMap[$item->user_id];
    }

    if ($repository->hasExpand('bank')) {
      $disbursement["cash"] = $cashMap[$user->id];
    }

    $disbursement['id'] = $item->id;
    $disbursement['total'] = $this->currency->formatPrice($item->amount + $item->fee);
    $disbursement['fee'] = $this->currency->formatPrice($item->fee);
    $disbursement["amount"] = $this->currency->formatPrice($item->amount);
    $disbursement["account_bank"] = $item->bank->name;
    $disbursement["account_number"] = $item->account_number;
    $disbursement["account_name"] = $item->account_name;
    $disbursement["customer_name"] = $item->customer_name;
    $disbursement["status"] = strtoupper(ApiDisbursement::getStatusMessage($item->status));
    $disbursement["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    $disbursement["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");
    $disbursement['reference_id'] = $item->reference_id;

    if (isAdmin()) {
      $disbursement['auto_disbursement_vendor'] = $item->auto_disbursement_vendor;
      $disbursement['auto_disbursement_reference_id'] = $item->auto_disbursement_reference_id;

      $disbursement["verified_at"] = $item->verified_at ? $item->verified_at->format('Y-m-d H:i:s') : null;
      $disbursement["cancelled_at"] = $item->cancelled_at ? $item->cancelled_at->format('Y-m-d H:i:s') : null;
      $disbursement["delayed_at"] = $item->delayed_at ? $item->delayed_at->format('Y-m-d H:i:s') : null;
      $disbursement["notified_at"] = $item->notified_at ? $item->notified_at->format('Y-m-d H:i:s') : null;

      if ($disbursement["status"] == "COMPLETED" && $disbursement['delayed_at'] != null) {
        $disbursement["status"] = "FAKE COMPLETED";
      }
    }

    return $disbursement;
  }

  public function get(PipelineListener $listener, $data) {

    $this->apiDisbursementRepo->normalizeFilters($data);
    $this->apiDisbursementRepo->setSimplePaginate(true);

    $disbursements = $this->apiDisbursementRepo->gets();
    $result = [];
    $cashMap = [];

    if ($this->apiDisbursementRepo->hasExpand('cash')) {
      $cashMap = $this->cashManager->getCashBalance($disbursements->pluck('user_id')->all(), Currency::IDR);
    }

    foreach ($disbursements as $item) {
      $result[] = $this->transforms($item, $this->apiDisbursementRepo, $cashMap);
    }

    if (sizeof($result) > 0) {
      return $listener->response(200, array_merge(
        ['disbursements' => $result],
        $this->apiDisbursementRepo->getPagination()
      ));
    }

    return $listener->response(204, ["disbursements" => []]);
  }

  public function getVendorRatio(PipelineListener $listener, $data) {
    $date = date('Y-m-d', time() - 7 * 24 * 60 * 60);
    $counts = $this->apiDisbursementRepo->getCountByVendor($date);
    $total = $counts->sum('count');
    $ratio = $counts->map(function ($row) use ($total) {
      return [
        'auto_disbursement_vendor' => $row->auto_disbursement_vendor,
        'count' => $row->count,
        'ratio' => round($row->count * 100 / $total, 2),
      ];
    });
    return $listener->response(200, ['ratio' => $ratio]);
  }

  public function getSuspectCount(PipelineListener $listener, $data) {
    $count = $this->apiDisbursementRepo->getSuspectNotRunning()->count();
    return $listener->response(200, ['count' => $count]);
  }
}
