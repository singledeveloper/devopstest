<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Cimb;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\CimbDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\Cimb\Repository\DisbursementCimbDisbursementRepository;

class RetryCimbDisbursement {

  private $apiDisbursementRepo, $cimbDisbursementRepo, $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->cimbDisbursementRepo = app()->make(DisbursementCimbDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if (!($disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']))) {
      return $listener->response(400, 'Disbursement not found');
    }

    if ($disbursement->status == ApiDisbursement::COMPLETED) {
      return $listener->response(400, 'Disbursement status is completed');
    }

    if (!($cimbDisbursement = $this->cimbDisbursementRepo->findById($disbursement->auto_disbursement_reference_id))) {
      return $listener->response(400, 'Cimb Disbursement not found');
    }

    if ($cimbDisbursement->status == CimbDisbursement::STATUS_SUCCESS) {
      return $listener->response(400, 'Cimb Disbursement status is success');
    }

    $this->redis->hdel('odeo_core:cimb_disbursement_lock', 'id_' . $cimbDisbursement->id);
    $this->redis->hdel('odeo_core:api_disbursement_lock', 'id_' . $data['disbursement_id']);

    $cimbDisbursement->error_log_response = null;
    $cimbDisbursement->status = null;
    $cimbDisbursement->status_code = null;
    $cimbDisbursement->status_message = null;
    $cimbDisbursement->response_datetime = null;
    $cimbDisbursement->error_code = null;
    $cimbDisbursement->error_message = null;

    $this->cimbDisbursementRepo->save($cimbDisbursement);

    if ($disbursement->status != ApiDisbursement::PENDING) {
      $disbursement->status = ApiDisbursement::PENDING;
      $this->apiDisbursementRepo->save($disbursement);
    }

    $listener->pushQueue(new ExecuteDisbursement($disbursement->id));

    return $listener->response(200);
  }

}
