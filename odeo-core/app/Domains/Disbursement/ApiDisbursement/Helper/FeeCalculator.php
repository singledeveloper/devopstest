<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement\Helper;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Domains\Disbursement\Repository\UserDisbursementFeeRepository;

class FeeCalculator {

  private $disbursementApiUserRepo, $userDisbursementFeeRepo;

  public function __construct() {
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->userDisbursementFeeRepo = app()->make(UserDisbursementFeeRepository::class);
  }

  public function calculateFee($userId, $bankId, $data) {
    $userFee = $this->userDisbursementFeeRepo->findByUserIdAndBankId($userId, $bankId);
    if ($userFee) {
      return $userFee->fee;
    }

    if ($bankId == Bank::ODEO) {
      return ApiDisbursement::FEE_ODEO;
    }

    $apiUser = null;

    if (isset($data['auth']['api_user'])) {
      $apiUser = $data['auth']['api_user'];
    } else {
      $apiUser = $this->disbursementApiUserRepo->findByUserId($userId);
    }

    if ($apiUser) {
      return intval($apiUser->fee);
    }

    return ApiDisbursement::FEE;
  }

}