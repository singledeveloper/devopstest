<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement\Helper;


use GuzzleHttp\Client;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementSelector;
use Odeo\Domains\Disbursement\Helper\DisbursementApiHelper;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\OdeoV2\Helper\ApiManager;

class ApiDisbursementNotifier {

  private $apiDisbursementRepo, $selector, $apiHelper, $odeoV2ApiManager;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->selector = app()->make(ApiDisbursementSelector::class);
    $this->apiHelper = app()->make(DisbursementApiHelper::class);
    $this->odeoV2ApiManager = app()->make(ApiManager::class);
  }

  public function notify($disbursement, $apiUser) {
    try {
      $response = $this->sendRequest($disbursement, $apiUser);
      $success = true;
      $statusCode = $response->getStatusCode();
      $content = $response->getBody()->getContents();
      $reason = $content;

      if ($statusCode != 200) {
        $success = false;
        $reason = 'Expected http status code is 200, but got %s' . $content;
      }

      if (!$this->isV1($disbursement->user_id) && strtoupper($content) != 'SUCCESS') {
        $success = false;
        $reason = "Expected response is SUCCESS, but got $content";
      }
    } catch (\Exception $exception) {
      $success = false;
      $statusCode = $exception->getCode();
      $reason = $exception->getMessage();
    }
    return [$success, $statusCode, $reason];
  }

  private function sendRequest($disbursement, $apiUser) {
    $payload = $this->selector->transforms($disbursement, $this->apiDisbursementRepo);
    $timestamp = time();

    if ($this->isV1($apiUser->user_id)) {
      $body = json_encode($payload);
      $signature = $this->apiHelper->generateSignature([
        'DisbursementCallback',
        $apiUser->notify_url,
        $payload['id'],
        $payload['bank_id'],
        $payload['account_number'],
        $payload['account_name'],
        $payload['amount'],
        $payload['customer_name'],
        $payload['customer_email'],
        $payload['fee'],
        $payload['description'],
        $payload['reference_id'],
        $payload['status'],
        $payload['message'],
        $payload['created_at']
      ], $timestamp, decrypt($apiUser->secret_access_key));
    } else {
      $path = parse_url($apiUser->notify_url, PHP_URL_PATH) ?? '/';
      $query = parse_url($apiUser->notify_url, PHP_URL_QUERY) ?? '';

      if (!starts_with($path, '/')) {
        $path = '/' . $path;
      }

      $body = json_encode([
        'account_number' => $payload['account_number'],
        'amount' => +$payload['amount'],
        'bank_id' => $payload['bank_id'],
        'created_at' => $payload['created_at'] . '',
        'customer_name' => $payload['customer_name'],
        'description' => $payload['description'],
        'disbursement_id' => $payload['id'] . '',
        'fee' => +$payload['fee'],
        'reference_id' => $payload['reference_id'],
        'status' => +$payload['status'],
      ]);
      $signature = $this->odeoV2ApiManager->getSignature($apiUser->signing_key, 'POST', $path, $query, '', $timestamp, $body);
    }

    $client = new Client();

    return $client->request('POST', $apiUser->notify_url, [
      'headers' => [
        'Content-Type' => 'application/json',
        DisbursementApiHelper::TIMESTAMP_HEADER => $timestamp,
        DisbursementApiHelper::SIGNATURE_HEADER => $signature,
      ],
      'body' => $body,
      'timeout' => 10,
    ]);
  }

  private function isV1($userId) {
    return $userId == 112342 || $userId == 16412;
  }
}