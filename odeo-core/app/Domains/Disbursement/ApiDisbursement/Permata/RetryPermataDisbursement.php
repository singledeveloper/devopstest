<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Permata;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\CimbDisbursement;
use Odeo\Domains\Constant\PermataDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\Permata\Repository\DisbursementPermataDisbursementRepository;

class RetryPermataDisbursement {

  private $apiDisbursementRepo, $permataDisbursementRepo, $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->permataDisbursementRepo = app()->make(DisbursementPermataDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if (!($disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']))) {
      return $listener->response(400, 'Disbursement not found');
    }

    if ($disbursement->status == ApiDisbursement::COMPLETED) {
      return $listener->response(400, 'Disbursement status is completed');
    }

    if (!($permataDisbursement = $this->permataDisbursementRepo->findById($disbursement->auto_disbursement_reference_id))) {
      return $listener->response(400, 'Permata Disbursement not found');
    }

    if ($permataDisbursement->status == CimbDisbursement::STATUS_SUCCESS) {
      return $listener->response(400, 'Permata Disbursement status is success');
    }

    $this->redis->hdel('odeo_core:permata_disbursement_lock', 'id_' . $permataDisbursement->id);
    $this->redis->hdel('odeo_core:api_disbursement_lock', 'id_' . $data['disbursement_id']);

    $permataDisbursement->cust_reff_id = null;
    $permataDisbursement->status = PermataDisbursement::STATUS_PENDING;
    $permataDisbursement->status_code = null;
    $permataDisbursement->status_desc = null;
    $permataDisbursement->request_timestamp = null;
    $permataDisbursement->response_timestamp = null;
    $permataDisbursement->response_datetime = null;
    $permataDisbursement->ex_error_message = null;
    $permataDisbursement->error_log_response = null;

    $this->permataDisbursementRepo->save($permataDisbursement);

    $disbursement->status = ApiDisbursement::PENDING;
    $this->apiDisbursementRepo->save($disbursement);

    $listener->pushQueue(new ExecuteDisbursement($disbursement->id));

    return $listener->response(200);
  }

}
