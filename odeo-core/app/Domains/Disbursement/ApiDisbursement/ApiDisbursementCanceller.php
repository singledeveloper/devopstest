<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Carbon\Carbon;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\RedigDisbursement;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\NotifyApiDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDisbursementFailedAlert;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\UpdateBatchDisbursementStatus;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ArtajasaBalanceUpdater;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;
use Odeo\Domains\Vendor\Redig\BalanceUpdater;
use Odeo\Domains\Vendor\Redig\Repository\DisbursementRedigDisbursementRepository;

class ApiDisbursementCanceller {

  private $apiDisbursementRepo, $cashInserter, $balanceUpdater, $ajDisbursementRepo, $redigDisbursementRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
    $this->balanceUpdater = app()->make(ArtajasaBalanceUpdater::class);
    $this->ajDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
    $this->redigDisbursementRepo = app()->make(DisbursementRedigDisbursementRepository::class);
  }

  public function cancel(PipelineListener $listener, $data) {
    $disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']);

    if (!$disbursement) {
      return $listener->response(400, trans('errors.database'));
    }

    switch ($disbursement->auto_disbursement_vendor) {
      case ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT:
        $ajDisbursement = $this->ajDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
        if ($ajDisbursement->status == ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS) {
          return $listener->response(400, 'disbursement suspect, silakan lapor tim dev');
        }
        break;
    }

    $disbursement->status = $data['disbursement_status'] ?? ApiDisbursement::FAILED;
    $disbursement->cancelled_at = Carbon::now()->toDateTimeString();

    if (!$this->apiDisbursementRepo->save($disbursement)) {
      return $listener->response(400, trans('errors.database'));
    }

    $this->cashInserter->add([
      'user_id' => $disbursement->user_id,
      'trx_type' => TransactionType::REFUND,
      'cash_type' => CashType::OCASH,
      'data' => json_encode([
        'type' => TransactionType::API_DISBURSEMENT,
        'id' => $disbursement->id,
        'notes' => 'Disbursement #' . $disbursement->id
      ]),
      'amount' => $disbursement->amount + $disbursement->fee,
      'reference_type' => TransactionType::API_DISBURSEMENT,
      'reference_id' => $disbursement->id
    ]);

    $this->cashInserter->run();
    $this->releaseDeposit($listener, $disbursement);
    $listener->pushQueue(new NotifyApiDisbursement($disbursement->id));

    if ($disbursement->batch_disbursement_id) {
      $listener->pushQueue(new UpdateBatchDisbursementStatus($disbursement->batch_disbursement_id));
    }

    if (in_array($disbursement->status, [ApiDisbursement::FAILED_VENDOR_DOWN, ApiDisbursement::FAILED])) {
      $listener->pushQueue(new SendDisbursementFailedAlert($disbursement->id));
    }

    return $listener->response(200);
  }

  private function releaseDeposit(PipelineListener $listener, $disbursement) {
    switch ($disbursement->auto_disbursement_vendor) {
      case ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT:
        $this->balanceUpdater->updateBalance([
          'amount' => $disbursement->amount,
          'fee' => ArtajasaDisbursement::COST,
          'type' => ArtajasaMutationDisbursement::TYPE_REFUND,
          'disbursement_artajasa_disbursement_id' => $disbursement->auto_disbursement_reference_id,
        ]);
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_REDIG_API_DISBURSEMENT:
        $redigDisb = $this->redigDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
        if (!$redigDisb->inquiry_request_id) {
          return;
        }

        $listener->addNext(new Task(BalanceUpdater::class, 'refundDisbursement', [
          'redig_disbursement_id' => $disbursement->auto_disbursement_reference_id,
          'amount' => $disbursement->amount,
          'cost' => RedigDisbursement::COST,
        ]));
        break;

      default:
        break;
    }
  }
}
