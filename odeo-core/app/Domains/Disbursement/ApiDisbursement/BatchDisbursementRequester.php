<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\BatchDisbursementStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Helper\FeeCalculator;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ValidateAccountNameByBatchDisbursementId;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;
use Odeo\Domains\Transaction\Helper\Currency;

class BatchDisbursementRequester {

  private $batchDisbursementRepo, $apiDisbursementRepo, $batchDisbursementSelector, $feeCalculator, $redis;

  const BATCH_REDIS = 'odeo-core:batch-day-accumulation-';

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
    $this->batchDisbursementSelector = app()->make(BatchDisbursementSelector::class);
    $this->feeCalculator = app()->make(FeeCalculator::class);
    $this->redis = Redis::connection();
  }

  public function request(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    $needSetExpire = (count($this->redis->hgetall(self::BATCH_REDIS . $userId)) <= 0);
    $rows = $data['rows'];
    $now = Carbon::now();
    $totalFee = 0;
    $totalAmount = 0;

    $validateAmounts = [];

    foreach ($rows as $key => $row) {
      $row['user_id'] = $userId;
      $row['fee'] = $this->feeCalculator->calculateFee($userId, $row['bank_id'], $data);
      $row['status'] = ApiDisbursement::VALIDATION_ON_PROGRESS;
      $row['created_at'] = $now;
      $row['updated_at'] = $now;
      $rows[$key] = $row;

      $totalFee += $row['fee'];
      $totalAmount += $row['amount'];

      if ($row['bank_id'] != Bank::ODEO) {
        if (!isset($validateAmounts[$row['bank_id']])) {
          $validateAmounts[$row['bank_id']] = 0;
        }
        $validateAmounts[$row['bank_id']] += $row['amount'];
      }
    }

    $bankNeedApproval = [];
    $currency = app()->make(Currency::class);
    $bankRepo = app()->make(BankRepository::class);
    foreach ($validateAmounts as $key => $item) {
      if (!$currentAmount = $this->redis->hget(self::BATCH_REDIS . $userId, $key))
        $currentAmount = 0;
      $currentAmount += $item;
      if ($currentAmount > 250000000) {
        $bank = $bankRepo->findById($key);
        $bankNeedApproval[] = $currency->formatPrice($item)['formatted_amount'] . ' ke ' . $bank->name;
      }
      $this->redis->hset(self::BATCH_REDIS . $userId, $key, $currentAmount);
    }

    $batch = $this->createBatchDisbursement($data, $totalFee, $totalAmount, count($rows), $bankNeedApproval);
    foreach ($rows as $key => $row) {
      $row['batch_disbursement_id'] = $batch->id;
      $rows[$key] = $row;
    }

    $this->apiDisbursementRepo->saveBulk($rows);
    $listener->pushQueue(new ValidateAccountNameByBatchDisbursementId($batch->id));

    if ($needSetExpire) {
      $this->redis->expire(self::BATCH_REDIS . $userId, strtotime(Carbon::now()->addDay()->format('Y-m-d 00:00:00')) - time());
    }

    return $listener->response(200, $this->batchDisbursementSelector->transforms($batch), true);
  }

  private function createBatchDisbursement($data, $totalFee, $totalAmount, $count, $bankNeedApproval = []) {
    $batch = $this->batchDisbursementRepo->getNew();
    $batch->name = $data['name'];
    $batch->user_id = $data['auth']['user_id'];
    $batch->status = BatchDisbursementStatus::PENDING;
    $batch->count = $count;
    $batch->total_fee = $totalFee;
    $batch->total_amount = $totalAmount;
    $batch->is_admin_approved = (count($bankNeedApproval) <= 0);

    $approvalUserRepo = app()->make(\Odeo\Domains\Approval\Repository\ApprovalUserRepository::class);
    if ($maker = $approvalUserRepo->findMakerPath($data['auth']['user_id'], Approval::PATH_BATCH_DISBURSEMENT))
      $batch->need_approval_from = $maker->approver_user_id;

    $batch->save();

    if (count($bankNeedApproval) > 0) {
      $userRepo = app()->make(UserRepository::class);
      $user = $userRepo->findById($batch->user_id);
      $internalNoticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
      $internalNoticer->saveMessage('Batch Disbursement dari ' . $user->name . ' sejumlah ' .
        implode(', ', $bankNeedApproval) . ' perlu di-approve oleh tim IT');
    }

    return $batch;
  }

}