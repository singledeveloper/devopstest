<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\RedigDisbursement;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementCanceller;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementRequester;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementSuspector;
use Odeo\Domains\Disbursement\ApiDisbursement\Artajasa\Helper\ExecuteArtajasaDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bca\Helper\ExecuteBcaDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bni\Helper\ExecuteBniDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bri\Helper\ExecuteBriDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Cimb\Helper\ExecuteCimbDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Odeo\Helper\ExecuteOdeoDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Mandiri\Helper\ExecuteMandiriDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Permata\Helper\ExecutePermataDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Redig\Helper\ExecuteRedigDisbursement;
use Odeo\Domains\Disbursement\Helper\VendorDisbursementBalanceHelper;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Jobs\Job;

class ExecuteDisbursement extends Job implements ShouldQueue {

  private $disbursementId, $apiDisbursementRepo, $redis, $vendorDisbursementBalanceHelper;
  private $vendorBalanceMap;

  public function __construct($disbursementId) {
    parent::__construct();
    $this->disbursementId = $disbursementId;
  }

  public function handle() {
    clog('api-disbursement', 'start disbursing ' . $this->disbursementId);
    $this->vendorBalanceMap = [];
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->vendorDisbursementBalanceHelper = app()->make(VendorDisbursementBalanceHelper::class);
    $this->redis = Redis::connection();

    $disbursement = $this->apiDisbursementRepo->findById($this->disbursementId);

    if (!$disbursement) {
      clog('api-disbursement', 'disbursement not found ' . $this->disbursementId);
      return;
    }

    if (!$this->lock($disbursement->id)) {
      clog('api-disbursement', 'disbursement locked ' . $this->disbursementId);
      return;
    }

    if ($disbursement->status == ApiDisbursement::COMPLETED && $disbursement->delayed_at == null) {
      clog('api-disbursement', 'disbursement delayed ' . $this->disbursementId);
      return;
    }

    $vendor = $this->getVendorDisbursement($disbursement);

    if (!$this->hasEnoughDeposit($disbursement, $vendor)) {
      $disbursement->status = ApiDisbursement::PENDING_NOT_ENOUGH_DEPOSIT;
      $this->apiDisbursementRepo->save($disbursement);

      $job = new ExecuteDisbursement($this->disbursementId);
      $job->delay(60);
      dispatch($job);

      $this->unlock($this->disbursementId);
      clog('api-disbursement', 'disbursement no balance ' . $this->disbursementId);
      return;
    }

    switch ($vendor) {
      case VendorDisbursement::BCA:
        $status = app()->make(ExecuteBcaDisbursement::class)->exec($disbursement);
        break;

      case VendorDisbursement::BNI:
        $status = app()->make(ExecuteBniDisbursement::class)->exec($disbursement);
        break;

      case VendorDisbursement::BRI:
        $status = app()->make(ExecuteBriDisbursement::class)->exec($disbursement);
        break;

      case VendorDisbursement::CIMB:
        $status = app()->make(ExecuteCimbDisbursement::class)->exec($disbursement);
        break;

      case VendorDisbursement::PERMATA:
        $status = app()->make(ExecutePermataDisbursement::class)->exec($disbursement);
        break;

     case VendorDisbursement::MANDIRI:
       $status = app()->make(ExecuteMandiriDisbursement::class)->exec($disbursement);
       break;

      case VendorDisbursement::REDIG:
        $status = app()->make(ExecuteRedigDisbursement::class)->exec($disbursement);
        break;

      case VendorDisbursement::ODEO:
        $status = app()->make(ExecuteOdeoDisbursement::class)->exec($disbursement);
        break;

      default:
        $status = app()->make(ExecuteArtajasaDisbursement::class)->exec($disbursement);
        break;
    }

    clog('api-disbursement', 'disbursement executed ' . $this->disbursementId . ' status is ' . $status);

    $pipeline = new Pipeline();

    switch ($status) {
      case ApiDisbursement::COMPLETED:
        $pipeline->add(new Task(ApiDisbursementRequester::class, 'complete'));
        break;

      case ApiDisbursement::SUSPECT:
      case ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS:
        $pipeline->add(new Task(ApiDisbursementSuspector::class, 'markAsSuspect'));
        break;

      case ApiDisbursement::FAILED:
      case ApiDisbursement::FAILED_WRONG_ACCOUNT_NUMBER:
      case ApiDisbursement::FAILED_CLOSED_BANK_ACCOUNT:
      case ApiDisbursement::FAILED_BANK_REJECTION:
      case ApiDisbursement::FAILED_VENDOR_DOWN:
      case ApiDisbursement::FAILED_DUPLICATE_REQUEST:
        $pipeline->add(new Task(ApiDisbursementCanceller::class, 'cancel'));
        break;

      default;
        break;
    }

    $pipeline->execute([
      'disbursement_id' => $this->disbursementId,
      'disbursement_status' => $status
    ]);
  }

  private function lock($id) {
    return $this->redis->hsetnx('odeo_core:api_disbursement_lock', 'id_' . $id, 1);
  }

  private function unlock($id) {
    $this->redis->hdel('odeo_core:api_disbursement_lock', 'id_' . $id);
  }

  private function getVendorDisbursement($disbursement) {
    if ($disbursement->bank_id == Bank::ODEO) {
      return VendorDisbursement::ODEO;
    }

    if (!isProduction()) {
      return VendorDisbursement::ARTAJASA;
    }

    $vendor = $this->getPreference($disbursement);
    if ($vendor) {
      return $vendor;
    }

    if ($disbursement->bank_id == Bank::BNI && $this->hasEnoughDeposit($disbursement, VendorDisbursement::BNI)) {
      $now = time();
      $maintenanceTime = Carbon::parse('2018-09-21 22:00:00')->getTimestamp();
      $maintenanceTimeEnd = Carbon::parse('2018-09-22 02:00:00')->getTimestamp();
      if ($now >= $maintenanceTime && $now < $maintenanceTimeEnd) {
        return VendorDisbursement::ARTAJASA;
      }

      return VendorDisbursement::BNI;
    }

    if ($disbursement->bank_id == Bank::BCA && $this->hasEnoughDeposit($disbursement, VendorDisbursement::BCA)) {
      return VendorDisbursement::BCA;
    }

    if ($disbursement->bank_id == Bank::CIMBNIAGA && $this->hasEnoughDeposit($disbursement, VendorDisbursement::CIMB)) {
      return VendorDisbursement::CIMB;
    }

    if ($disbursement->bank_id == Bank::PERMATA && $this->hasEnoughDeposit($disbursement, VendorDisbursement::PERMATA)) {
      return VendorDisbursement::PERMATA;
    }

    if ($disbursement->bank_id == Bank::BRI && $this->hasEnoughDeposit($disbursement, VendorDisbursement::BRI)) {
      return VendorDisbursement::BRI;
    }

    if ($disbursement->bank_id == Bank::MANDIRI && $this->hasEnoughDeposit($disbursement, VendorDisbursement::MANDIRI)) {
      return VendorDisbursement::MANDIRI;
    }
    
    if ($this->isRedig($disbursement) && $this->hasEnoughDeposit($disbursement, VendorDisbursement::REDIG)) {
      return VendorDisbursement::REDIG;
    }

    return VendorDisbursement::ARTAJASA;
  }

  private function isRedig($disbursement) {
    if ($this->redis->get(RedigDisbursement::DISABLE_DISBURSEMENT_REDIS_KEY)) {
      return false;
    }
    $pct = 10;
    return $disbursement->id % floor(100 / $pct) == 0;
  }

  private function getPreference($disbursement) {
    return $this->redis->hget('odeo_core:api_disbursement_preference', $disbursement->id);
  }

  private function hasEnoughDeposit($disbursement, $vendor) {
    $balance = $this->getBalance($vendor);
    $cost = 0;

    switch ($vendor) {
      case VendorDisbursement::ARTAJASA:
        $cost = ArtajasaDisbursement::COST;
        break;

      case VendorDisbursement::ODEO:
        return true;
    }

    return $balance >= $disbursement->amount + $cost;
  }

  private function getBalance($vendor) {
    if (isset($this->vendorBalanceMap[$vendor])) {
      return $this->vendorBalanceMap[$vendor];
    }

    return $this->vendorBalanceMap[$vendor] = $this->vendorDisbursementBalanceHelper->getBalance($vendor);
  }
}
