<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/7/17
 * Time: 5:35 PM
 */

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Helper\DisbursementExporter;
use Odeo\Jobs\Job;

class SendDisbursementInquiryReport extends Job  {
  protected $filters, $email, $exportId;

  public function __construct($filters, $email, $exportId) {
    parent::__construct();
    $this->filters = $filters;
    $this->email = $email;
    $this->exportId = $exportId;
  }

  public function handle() {
    $exporter = app()->make(DisbursementExporter::class);
    $exportQueueManager = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class);
    list($file, $hasData) = $exporter->exportDisbursementInquiry($this->filters);

    if (!$hasData && $this->exportId != '0') {
      $exportQueueManager->error($this->exportId, 'You don\'t have any data to be exported.');
      return;
    }

    $fileName = 'Laporan Disbursement Inquiry ' . $this->filters['start_date'] . ' - ' . $this->filters['end_date'] . '.' . $this->filters['file_type'];
    $exportQueueManager->update($this->exportId, $file, $fileName);

    Mail::send('emails.disbursement_inquiry_export_mail', [
      'data' => $this->filters
    ], function ($m) use ($file, $fileName) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->attachData($file, $fileName);
      $m->to($this->email)->subject('Laporan Disbursement Inquiry');
    });
  }
}
