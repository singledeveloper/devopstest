<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\Artajasa\RetryArtajasaDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bni\RetryBniDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaTransferRepository;
use Odeo\Domains\Vendor\Bni\Repository\DisbursementBniDisbursementRepository;
use Odeo\Jobs\Job;

class AutoRetryDisbursement extends Job implements ShouldQueue {

  private $disbursementId, $apiDisbursementRepo, $bniDisbursementRepo, $ajDisbursementRepo, $redis, $ajTransferRepo;

  public function __construct($disbursementId) {
    parent::__construct();
    $this->disbursementId = $disbursementId;
  }

  public function handle() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->bniDisbursementRepo = app()->make(DisbursementBniDisbursementRepository::class);
    $this->ajDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
    $this->ajTransferRepo = app()->make(DisbursementArtajasaTransferRepository::class);
    $this->redis = Redis::connection();

    $disbursement = $this->apiDisbursementRepo->findById($this->disbursementId);

    if (!$disbursement) {
      return;
    }

    if ($disbursement->status != ApiDisbursement::SUSPECT) {
      return;
    }

    if (!$this->lockAutoRetry($this->disbursementId)) {
      return;
    }

    if (!$this->lockRetry($this->disbursementId)) {
      return;
    }

    $pipeline = new Pipeline();

    switch ($disbursement->auto_disbursement_vendor) {
      case ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT:
        $bniDisbursement = $this->bniDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
        if ($bniDisbursement->response_code != '0169') {
          return;
        }

        $pipeline->add(new Task(RetryBniDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT:
        $ajDisbursement = $this->ajDisbursementRepo->findById($disbursement->auto_disbursement_reference_id);
        if (!$this->canRetryAJ($ajDisbursement)) {
          return;
        }

        $pipeline->add(new Task(RetryArtajasaDisbursement::class, 'retry'));
        break;

      default:
        return;
    }

    $pipeline->execute([
      'disbursement_id' => $this->disbursementId,
    ]);
  }

  private function lockAutoRetry($id) {
    return $this->redis->hsetnx('odeo_core:disbursement_auto_retry_lock', $id, 1);
  }

  private function lockRetry($id) {
    $key = 'odeo_core:disbursement_retry_lock_' . $id;
    if (!$this->redis->setnx($key, 1)) {
      return false;
    }
    $this->redis->expire($key, 5 * 60);
    return true;
  }

  private function canRetryAJ($ajDisbursement) {
    if ($ajDisbursement->disbursement_artajasa_transfer_id === null) {
      return true;
    }

    $ajTransfer = $this->ajTransferRepo->findById($ajDisbursement->disbursement_artajasa_transfer_id);
    return $ajTransfer->response_code == 'EE';
  }
}
