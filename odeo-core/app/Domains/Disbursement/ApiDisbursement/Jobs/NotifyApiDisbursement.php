<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;

use Datetime;
use Illuminate\Contracts\Queue\ShouldQueue;
use Odeo\Domains\Disbursement\ApiDisbursement\Helper\ApiDisbursementNotifier;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementNotifyLogRepository;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;
use Odeo\Jobs\Job;

class NotifyApiDisbursement extends Job implements ShouldQueue {

  private $disbursementId, $apiDisbursementRepo, $apiDisbursementNotifyLogRepo, $disbursementApiUserRepo, $apiDisbursementNotifier;

  public function __construct($disbursementId) {
    parent::__construct();
    $this->disbursementId = $disbursementId;
  }

  public function handle() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->apiDisbursementNotifyLogRepo = app()->make(ApiDisbursementNotifyLogRepository::class);
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
    $this->apiDisbursementNotifier = app()->make(ApiDisbursementNotifier::class);

    $disbursement = $this->apiDisbursementRepo->findById($this->disbursementId);
    $apiUser = $this->disbursementApiUserRepo->findByUserId($disbursement->user_id);

    if (empty($apiUser->notify_url)) {
      return;
    }

    if ($this->apiDisbursementNotifyLogRepo->hasSuccessfulNotify($disbursement->id, $disbursement->status)) {
      return;
    }

    list($success, $statusCode, $reason) = $this->apiDisbursementNotifier->notify($disbursement, $apiUser);

    $log = $this->apiDisbursementNotifyLogRepo->getNew();
    $log->is_success = $success;
    $log->user_id = $disbursement->user_id;
    $log->api_disbursement_id = $disbursement->id;
    $log->status = $disbursement->status;
    $log->response_status_code = $statusCode;
    $log->response_reason = $reason;
    $this->apiDisbursementNotifyLogRepo->save($log);

    if ($success) {
      $disbursement->notified_at = new Datetime();
      $this->apiDisbursementRepo->save($disbursement);
      return;
    }

    $notifyCount = $this->apiDisbursementNotifyLogRepo
      ->countByApiDisbursementIdAndStatus($disbursement->id, $disbursement->status);

    if ($notifyCount < 5) {
      dispatch(with(new NotifyApiDisbursement($this->disbursementId))
        ->delay(30));
    } else if ($notifyCount < 8) {
      dispatch(with(new NotifyApiDisbursement($this->disbursementId))
        ->delay(min(43200, $notifyCount * 1800)));
      dispatch(new SendDisbursementNotifyFailedAlert($this->disbursementId, $reason, $disbursement->user->email));
    }
  }

}
