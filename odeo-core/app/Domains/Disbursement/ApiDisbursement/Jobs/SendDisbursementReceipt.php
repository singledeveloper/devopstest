<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/7/17
 * Time: 5:35 PM
 */

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Odeo\Jobs\Job;

class SendDisbursementReceipt extends Job {
  protected $disbursementId;

  public function __construct($disbursementId) {
    parent::__construct();
    $this->disbursementId = $disbursementId;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

//    $currency = app()->make(Currency::class);
//    $selector = app()->make(ApiDisbursementSelector::class);
//    $disbursementRepo = app()->make(ApiDisbursementRepository::class);
//
//    $disbursementRepo->normalizeFilters([
//      'expand' => 'bank',
//    ]);
//
//    $disbursement = $disbursementRepo->findById($this->disbursementId);

//    if (empty($disbursement->customer_email)) {
//      return;
//    }

//    Mail::send('emails.disbursement_receipt', [
//      'data' => $selector->transforms($disbursement, $disbursementRepo),
//      'amount' => $currency->formatPrice($disbursement->amount),
//    ], function ($m) use ($disbursement) {
//      $m->from('noreply@odeo.co.id', 'odeo');
//      $m->to($disbursement->customer_email)
//        ->subject('Odeo Disbursement #' . $disbursement->id);
//    });
  }
}
