<?php

namespace Odeo\Domains\OAuth2;

use Odeo\Domains\Core\PipelineListener;
use Carbon\Carbon;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;

class OAuth2TokenChecker {

  private $scopeDataSelector, $oauth2ClientRepository, $oauth2TokenRepository;

  public function __construct() {
    $this->scopeDataSelector = app()->make(\Odeo\Domains\OAuth2\Helper\ScopeDataSelector::class);
    $this->oauth2ClientRepository = app()->make(\Odeo\Domains\OAuth2\Repository\OAuth2ClientRepository::class);
    $this->oauth2TokenRepository = app()->make(\Odeo\Domains\OAuth2\Repository\OAuth2TokenRepository::class);
  }
  
  public function check(PipelineListener $listener, $data) {
    try {
      $token = (new Parser())->parse($data["bearer"]);

      if (!$token->verify(new Sha256(), env('APP_KEY'))) {
        return $listener->response(401, trans('errors.invalid_authorization'));
      }

      $scopes = explode(' ', $token->getClaim('scp'));
      if (!in_array($data['allowed_scope'], $scopes)) {
        return $listener->response(400, "You don't have any credential to access this data");
      }

      if ($token->getClaim('exp') <= time()) {
        return $listener->response(400, "Session timeout, this token has expired");
      }

      if (!isset($data['headers']['x-odeo-timestamp']) || !isset($data['headers']['x-odeo-signature'])) {
        return $listener->response(400, "Invalid Headers");
      }
      $headerTimestamp = $data['headers']['x-odeo-timestamp'][0];
      $headerSignature = $data['headers']['x-odeo-signature'][0];
  
      $oauth2Client = $this->oauth2ClientRepository->findByClientId($token->getClaim('cid'));
      if (!$oauth2Client) {
        return $listener->response(400, "Client Not Found");
      }

      $data['body'] = sizeof($data['body']) === 0 ? '' : json_encode($data['body']);
      $hash = base64_encode(hash('sha256', $data['body'], true));
      $rawSign = "{$data['method']}:{$data['path']}::{$data['bearer']}:{$headerTimestamp}:{$hash}";
      $signature = base64_encode(hash_hmac('sha256', $rawSign, $oauth2Client->signing_key, true));

      if ($signature != $headerSignature) {
        return $listener->response(400, "Invalid Signature");
      }

      $response = $this->scopeDataSelector->getAdditionalData($token->getClaim('cid'), $data['allowed_scope']);
      return $listener->response(200, $response);
    } catch (\Exception $e) {
      if (app()->environment() == 'production') {
        return $listener->response(401, trans('errors.invalid_authorization'));
      }
      return $listener->response(401, $e->getMessage());
    }
  }

}
