<?php

namespace Odeo\Domains\OAuth2\Helper;

class ScopeDataSelector {
  
  public function __construct() {
    $this->paymentGatewayUsers = app()->make(\Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserRepository::class);
  }
  
  public function getAdditionalData($clientId, $scope) {
    $item = [];
    switch ($scope) {
      case 'pg_get_payment:read':
        $paymentGatewayUser = $this->paymentGatewayUsers->findByClientId($clientId);
        $item['payment_gateway_user_id'] = $paymentGatewayUser->id;
        break;
    }

    return $item;
  }
}
