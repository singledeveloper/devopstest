<?php

namespace Odeo\Domains\OAuth2\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\OAuth2\Model\OAuth2Client;

class OAuth2ClientRepository extends Repository
{

  public function __construct(OAuth2Client $client)
  {
    $this->model = $client;
  }

  public function findByUserId($userId) {
    return $this->model->where("user_id", $userId)->first();
  }

  public function findByClientId($clientId) {
    return $this->model->where("client_id", $clientId)->first();
  }

  public function findByClientIdAndSecret($clientId, $secret) {
    return $this->model->where("client_id", $clientId)->where("secret", $secret)->first();
  }

}
