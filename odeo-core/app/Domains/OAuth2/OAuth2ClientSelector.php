<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/07/19
 * Time: 15.41
 */

namespace Odeo\Domains\OAuth2;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\OAuth2\Repository\OAuth2ClientRepository;

class OAuth2ClientSelector {

  private $oauthClientRepo;

  public function __construct() {
    $this->oauthClientRepo = app()->make(OAuth2ClientRepository::class);
  }

  public function checkUserOAuth2Client(PipelineListener $listener, $data) {
    if (!$client = $this->oauthClientRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(200, [
        'is_created' => false
      ]);
    }
    return $listener->response(200, [
      'is_created' => true
    ]);
  }

  public function getUserOAuth2Client(PipelineListener $listener, $data) {
    if (!$client = $this->oauthClientRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    return $listener->response(200, [
      'client_id' => $client->client_id,
      'secret' => $client->secret,
      'signing_key' => $client->signing_key,
      'allowed_scope' => $client->allowed_scope
    ]);
  }


}