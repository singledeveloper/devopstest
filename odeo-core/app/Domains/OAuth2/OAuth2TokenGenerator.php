<?php

namespace Odeo\Domains\OAuth2;

use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;
use Carbon\Carbon;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class OAuth2TokenGenerator {

  private $builder, $oauth2TokenRepository;

  public function __construct() {
    $this->builder = app()->make(\Lcobucci\JWT\Builder::class);
    $this->oauth2TokenRepository = app()->make(\Odeo\Domains\OAuth2\Repository\OAuth2TokenRepository::class);
  }
  
  public function generate(PipelineListener $listener, $data) {
    if (!isset($data['grant_type']) || $data['grant_type'] !== 'client_credentials') {
      return $listener->response(400, trans('oauth2.unsupported_grant_type'));
    }

    $expiredDuration = 1800;
    $builder = $this->builder->unsign()
      ->setIssuedAt(time())
      ->setExpiration(time() + $expiredDuration)
      ->set('cid', $data["client_id"])
      ->set('uid', $data['user_id'])
      ->set('upf', Platform::H2H)
      ->set('scp', $data['scope']);
    $token = $builder->sign(new Sha256(), env('APP_KEY'))->getToken();

    if (!$oauth2Token = $this->oauth2TokenRepository->findByUserId($data['user_id'])) {
      $oauth2Token = $this->oauth2TokenRepository->getNew();
      $oauth2Token->user_id = $data['user_id'];
      $oauth2Token->client_id = $data['client_id'];
      $oauth2Token->redirect_uri = '';
      $oauth2Token->code = '';
      $oauth2Token->code_create_at = Carbon::now()->toDateTimeString();
      $oauth2Token->code_expires_in = 0;
      $oauth2Token->refresh = '';
      $oauth2Token->refresh_create_at = Carbon::now()->toDateTimeString();
      $oauth2Token->refresh_expires_in = 0;
    }

    $oauth2Token->scope = $data['scope'];
    $oauth2Token->access = (string)$token;
    $oauth2Token->access_create_at = Carbon::now()->toDateTimeString();
    $oauth2Token->access_expires_in = $expiredDuration;

    $this->oauth2TokenRepository->save($oauth2Token);

    return $listener->response(200, [
      'access_token' => (string)$token,
      'expires_in' => $expiredDuration,
      'scope' => $data['scope'],
      'token_type' => 'Bearer'
    ]);
  }

}
