<?php

namespace Odeo\Domains\Campaign\Repository;

use Odeo\Domains\Campaign\Model\CampaignMember;
use Odeo\Domains\Core\Repository;

class CampaignMemberRepository extends Repository {

  public function __construct(CampaignMember $member) {
    $this->model = $member;
  }

  public function getSeptOct2017members($limit, $offset) {
    return $this->model
      ->join('users', 'users.id', '=', 'campaign_members.user_id')
      ->where('campaign', 'cashback_sept_oct_2017')
      ->whereNotNull('users.email')
      ->orderBy('user_id')
      ->offset($offset)
      ->limit($limit)
      ->select(
        'campaign_members.user_id',
        'users.name',
        'users.email'
      )
      ->get();
  }
}
