<?php

namespace Odeo\Domains\Campaign\WeeklyTarget\Repository;


use Odeo\Domains\Campaign\WeeklyTarget\Model\WeeklyTarget;
use Odeo\Domains\Core\Repository;

class WeeklyTargetRepository extends Repository {

  public function __construct(WeeklyTarget $weeklyTarget) {
    $this->model = $weeklyTarget;
  }

  public function getTarget($storeId) {
    $query = $this->getCloneModel();

    $query = $query->where('store_id', $storeId);

    return $query->first();
  }

}