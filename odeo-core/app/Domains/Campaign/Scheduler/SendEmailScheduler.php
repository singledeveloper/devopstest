<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/16/17
 * Time: 18:11
 */

namespace Odeo\Domains\Campaign\Scheduler;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Helper\CustomerEmailManager;
use Odeo\Domains\Campaign\Jobs\SendDailySummaryEmail;
use Odeo\Domains\Campaign\Repository\CampaignMemberRepository;
use Odeo\Domains\Campaign\TransactionCounter;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;


class SendEmailScheduler {

  private $redis, $counter, $members;

  private $redisNamespace = 'odeo_core:campaign';
  private $redisKey = 'sept_oct_2017_members_cursor';

  public function __construct() {
    $this->redis = Redis::connection();
    $this->counter = app()->make(TransactionCounter::class);
    $this->members = app()->make(CampaignMemberRepository::class);
  }

  public function run() {

    $limit = 16;

    if (($cursor = $this->redis->hget($this->redisNamespace, $this->redisKey))) ;
    else {
      $cursor = 0;
      $this->initializeRedis();
    }

    if ($cursor == -1) return;

    $campaignMembers = $this->members->getSeptOct2017members($limit, $cursor);

    for ($i = 0; $i < $limit; $i++, $cursor++) {

      if (!isset($campaignMembers[$i])) {
        $cursor = -1;
        break;
      }

      $this->sendEmail($campaignMembers[$i]);

    }

    $this->redis->hset($this->redisNamespace, $this->redisKey, $cursor);

  }


  private function initializeRedis() {

    $date = Carbon::now();
    $this->redis->hset($this->redisNamespace, $this->redisKey, 0);
    $this->redis->expireAt($this->redisNamespace, Carbon::create($date->year, $date->month, $date->day + 1, 0, 0, 0)->timestamp);
  }

  public function sendEmail($member) {

    $result = $this->counter->septOct2017CampaignCounter($member->user_id);
    
    $pipeline = new Pipeline();
    $pipeline->add(new Task(CustomerEmailManager::class, 'pushJobIfEmailVerified', [
      'email' => $member->email,
      'job' => new SendDailySummaryEmail(array_merge($result, [
        'member' => [
          'email' => $member->email,
          'name' => $member->name
        ]
      ]))
    ]));
    $pipeline->execute();
  }

}