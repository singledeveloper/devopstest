<?php

namespace Odeo\Domains\Network;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;

class SponsorRequester {

  public function __construct() {
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->storeManager = app()->make(\Odeo\Domains\Subscription\Helper\StoreManager::class);
    $this->plan = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class);
    $this->sponsor = app()->make(\Odeo\Domains\Network\Repository\SponsorRepository::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->revenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
  }

  public function create(PipelineListener $listener, $data) {

    $store = $this->store->findById($data["store_id"]);
    if (isset($data["referred_store_id"])) $referredStore = $this->store->findById($data["referred_store_id"]);
    else $referredStore = $store->network->referredStore;

    if (isset($data["previous_plan_id"]) && $referredStore) {
      $listener->pushQueue($this->createBonus($referredStore, $store, "upgrade", $data["previous_plan_id"]));
    } else if (isset($data["referred_store_id"])) {
      $listener->pushQueue($this->createBonus($referredStore, $store, "assign"));
    }

    if (isset($data["upgrade_request_datetime"]) && $left = $this->checkLeftover($store, $data["upgrade_request_datetime"])) {
      $listener->pushQueue($left);
    }

    return $listener->response(200);
  }

  public function createBonus($item, $child, $type, $previous_plan_id = "") {

    $expired_date = date("Y-m-d 23:59:59");
    $day_limit = date("Y-m-d");
    $rules = $child->plan;
    $sponsor_par = "max_" . $type . "_sponsor_bonus";

    $bonus = $rules->$sponsor_par;
    if ($previous_plan_id != "") $bonus -= $this->plan->findById($previous_plan_id)->$sponsor_par;

    $max_bonus = $item->plan->$sponsor_par;
    $amount_get = ($bonus < $max_bonus ? $bonus : $max_bonus);

    $minimal_required_plan_id = ($bonus != $amount_get) ? $rules->id : NULL;
    if ($minimal_required_plan_id != NULL) {
      $leftover = $this->sponsor->getLeftover($item->id, $minimal_required_plan_id, $day_limit);
      $leftover_total = ($leftover->total ? $leftover->total : 0);
    }

    $sponsor = $this->sponsor->getNew();
    $sponsor->store_id = $child->id;
    $sponsor->sponsor_store_id = $item->id;
    $sponsor->amount = $bonus;
    $sponsor->claimed_amount = $amount_get;
    $sponsor->minimal_required_plan_id = $minimal_required_plan_id;
    $sponsor->created_cause = $type;
    $sponsor->day_limit = $day_limit;
    $this->sponsor->save($sponsor);

    $owners = $this->storeManager->getOwner([$item->id, $child->id]);

    $this->inserter->add([
      'user_id' => $owners[$item->id]["id"],
      'trx_type' => TransactionType::SPONSOR_BONUS,
      'cash_type' => CashType::OCASH,
      'amount' => $amount_get,
      'data' => json_encode(['recent_desc' => $item->name])
    ]);

    $this->revenue->save([
      'user_id' => $owners[$item->id]["id"],
      'store_id' => $item->id,
      'order_id' => null,
      'amount' => $amount_get,
      'trx_type' => TransactionType::SPONSOR_BONUS,
      'data' => json_encode(['recent_desc' => $item->name])
    ]);

    $this->inserter->run();

    if ($sponsor->minimal_required_plan_id != NULL) {
      $this->notification->setup($owners[$item->id]["id"], NotificationType::WARNING, NotificationGroup::TRANSACTION);
      $this->notification->sponsor_bonus_leftover($amount_get, ($leftover_total + $bonus - $amount_get), $item->id, $item->name, $child->name, $rules->id, $rules->name, $expired_date);
    } else {
      $this->notification->setup($owners[$item->id]["id"], NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
      $this->notification->sponsor_bonus($bonus, $item->name, $child->name, $type);
    }

    return $this->notification->queue();
  }

  public function checkLeftover($store, $upgrade_datetime) {
    $leftover = $this->sponsor->getAllLeftover($store->id, $upgrade_datetime);

    if (sizeof($leftover) > 0) {
      $leftovers_notif = [];
      $bonus_total = 0;
      $last_day_limit = "";
      $last_plan_id = "";

      $owner = $this->store->findOwner($store->id);

      foreach ($leftover as $item) {
        $leftover_amount = $item->amount - $item->claimed_amount;
        if ($store->plan->id >= $item->minimal_required_plan_id) {
          $item->claimed_amount = $item->amount;
          $item->minimal_required_plan_id = NULL;

          $this->inserter->add([
            'user_id' => $owner->user_id,
            'trx_type' => TransactionType::SPONSOR_BONUS,
            'cash_type' => CashType::OCASH,
            'amount' => $leftover_amount,
            'data' => json_encode(['recent_desc' => $store->name])
          ]);

          $this->revenue->save([
            'user_id' => $owner->user_id,
            'store_id' => $store->id,
            'order_id' => null,
            'amount' => $leftover_amount,
            'trx_type' => TransactionType::SPONSOR_BONUS,
            'data' => json_encode(['recent_desc' => $store->name])
          ]);

          $bonus_total += ($leftover_amount);
        } else {
          if (!isset($leftovers_notif[$item->minimal_required_plan_id])) {
            $leftovers_notif[$item->minimal_required_plan_id] = [
              "total_amount" => (isset($leftovers_notif[$last_plan_id]["total_amount"]) ? $leftovers_notif[$last_plan_id]["total_amount"] : 0),
              "last_day_limit" => "",
              "plan_name" => $item->plan->name
            ];
            $last_plan_id = $item->minimal_required_plan_id;
          }
          $leftovers_notif[$item->minimal_required_plan_id]["total_amount"] += ($leftover_amount);
          $leftovers_notif[$item->minimal_required_plan_id]["last_day_limit"] = $item->day_limit;
        }
      }

      if ($bonus_total > 0) {
        $this->notification->setup($owner->user_id, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
        $this->notification->sponsor_bonus_from_upgrade($bonus_total, $store->name);
      }

      if (sizeof($leftovers_notif) > 0) {
        $this->notification->setup($owner->user_id, NotificationType::WARNING, NotificationGroup::TRANSACTION);
        foreach ($leftovers_notif as $key => $item) {
          $this->notification->sponsor_bonus_leftover_still($item["total_amount"], $store->id, $store->name, $item["plan_name"], $item["last_day_limit"] . " 23:59:59");
        }
      }

      $this->inserter->run();

      return $this->notification->queue();
    }

    return false;
  }
}
