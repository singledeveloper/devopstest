<?php

namespace Odeo\Domains\Network;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\ReferralConfig;
use Odeo\Domains\Transaction\Helper\Currency;

class ReferralClaimRequester {

  public function __construct(Currency $currency) {
    $this->referral = app()->make(\Odeo\Domains\Network\Repository\ReferralRepository::class);
    $this->referralManager = app()->make(\Odeo\Domains\Network\Helper\ReferralManager::class);
    $this->tree = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->currency = $currency;
  }
  
  public function claim(PipelineListener $listener, $data) {
    $direct_referral = $this->referralManager->getCurrentReferral($data["store_id"])[$data["store_id"]];
    if (!$direct_referral["can_claim"])
      return $listener->response(400, "You don't have enough point(s) to claim. Need " . $direct_referral["points_need_to_claim"] . " more");
    else {
      $claim = $this->referral->getNew();
      $claim->referred_store_id = $data["store_id"];
      $claim->amount = ReferralConfig::AMOUNT_LIMIT;
      $claim->is_claimed = "1";
      
      $this->referral->save($claim);
      $network = $this->tree->findByStoreId($data["store_id"]);
      $owner = $this->store->findOwner($data["store_id"]);
      
      $currency = $this->currency->getCurrency(Currency::IDR);
      $this->inserter->add([
        'user_id' => $owner->user_id,
        'store_id' => $data["store_id"],
        'trx_type' => TransactionType::DIRECT_REFERRAL,
        'cash_type' => CashType::ODEPOSIT,
        'amount' => ReferralConfig::VALUE * $currency->buy_rate,
        'data' => json_encode(['recent_desc' => $network->store->name])
      ]);
      
      $this->inserter->run();
      
      return $listener->response(201);
    }
  }
}