<?php
namespace Odeo\Domains\Network\Helper;

use Odeo\Domains\Constant\StoreStatus;

class ParentManager {

  public function __construct() {
    $this->tree = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->treeParent = app()->make(\Odeo\Domains\Network\Repository\TreeParentRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
  }

  private $branch_data = [];

  private function recursive_get_parent($store_id) {
    if ($branch = $this->tree->findByStoreId($store_id)) {
      $this->branch_data[] = $branch->toArray();
      if ($branch->parent_store_id != NULL) {
        $this->recursive_get_parent($branch->parent_store_id);
      }
    }
  }

  public function get_me_and_parents($store_id) {
    $ids = [];
    $recursive = true;
    if ($parent = $this->treeParent->findByStoreId($store_id)) {
      $ids[] = $store_id;
      if ($parent->parent_store_ids != "") {
        $ids = array_merge($ids, explode(",", $parent->parent_store_ids));
        $recursive = false;
      }
    }
    if ($recursive) {
      $this->recursive_get_parent($store_id);
      $data = $this->branch_data;
      foreach ($data as $item) {
        if ($item["store_id"] != $store_id) {
          $ids[] = $item["store_id"];
        }
      }
      $parent = $this->treeParent->getNew();
      $parent->store_id = $store_id;
      $parent->parent_store_ids = implode(",", $ids);
      $this->treeParent->save($parent);
      $this->branch_data = [];
      $ids[] = $store_id;
    }
    return $this->tree->findByStoreIds($ids, ['store.plan']);
  }

  public function get_exact_parents($store_id, $returnArray = false) {
    $data = [
      "me" => false,
      "parent" => false,
      "grandparent" => false
    ];
    if ($branch = $this->tree->findByStoreId($store_id)) {
      $data["me"]["store_id"] = $branch->store_id;
      $data["me"]["store_name"] = $branch->store->name;
      $data["me"]["user_id"] = $this->store->findOwner($branch->store_id)->user_id;
      if ($branch->referred_store_id != NULL && $branch_parent = $this->tree->findByStoreId($branch->referred_store_id)) {
        if(StoreStatus::isActive($branch_parent->store->status)) {
          $data["parent"]["store_id"] = $branch_parent->store_id;
          $data["parent"]["store_name"] = $branch_parent->store->name;
          $data["parent"]["user_id"] = $this->store->findOwner($branch_parent->store_id)->user_id;
        }
        if ($branch_parent->referred_store_id != NULL && $branch_grand_parent = $this->tree->findByStoreId($branch_parent->referred_store_id)) {
          if(StoreStatus::isActive($branch_grand_parent->store->status)) {
            $data["grandparent"]["store_id"] = $branch_grand_parent->store_id;
            $data["grandparent"]["store_name"] = $branch_grand_parent->store->name;
            $data["grandparent"]["user_id"] = $this->store->findOwner($branch_grand_parent->store_id)->user_id;
          }
        }
      }
    }
    return $returnArray ? $data : json_decode(json_encode($data));
  }
}
