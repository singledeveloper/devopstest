<?php

namespace Odeo\Domains\Network\Helper;

class TreeManager {
  
  public function __construct() {
    $this->tree = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->treeChild = app()->make(\Odeo\Domains\Network\Repository\TreeChildRepository::class);
    $this->treeParent = app()->make(\Odeo\Domains\Network\Repository\TreeParentRepository::class);
  }
  
  public function check_position($position, $parent_store_id) {
    $branch = $this->tree->findByParent($parent_store_id);
    $auto_position = "";
    $error = "";
    if (sizeof($branch) >= 2) $error = "Can't assign in this branch. Branch is already full.";
    else if (sizeof($branch) == 1) {
      $auto_position = ($branch[0]->position == "1") ? "0" : "1";
      if ($position == $branch[0]->position) {
        $error = "Can't assign in this branch. " . ($branch[0]->position == "1" ? "Left" : "Right") . " branch is already filled.";
      }
    }
    else $auto_position = "0";
    
    return [$error, $auto_position];
  }

  public function refreshReferredList() {
    $trees = $this->tree->getAll();
    $hustlerStoreIds = array();
    $tempReferredStoreIds = array();
    $referredStoreIds = array();

    foreach($trees as $node) {
      if($node->referred_store_id) {
        $hustlerStoreIds[$node->referred_store_id][] = $node->store_id;
        $tempReferredStoreIds[$node->store_id] = $node->referred_store_id;
      }
    }

    foreach($tempReferredStoreIds as $storeId => $referredId) {
      $level = 1;
      while($level < 5) {
        $referredStoreIds[$storeId][] = $referredId;
        if(isset($tempReferredStoreIds[$referredId])) $referredId = $tempReferredStoreIds[$referredId];
        else break;
        $level++;
      };
    }
    foreach($hustlerStoreIds as $storeId => $hustlerIds) {
      $grandHustlerIds = array();
      $teamIds = array();
      foreach($hustlerIds as $hustlerId) {
        if(isset($hustlerStoreIds[$hustlerId])) $grandHustlerIds = array_merge($grandHustlerIds, $hustlerStoreIds[$hustlerId]);
      }
      $grandHustlerStoreIds = implode($grandHustlerIds, ',');
      $tempIds = array();
      $level = 1;
      while(($grandHustlerId = array_pop($grandHustlerIds)) && $level < 3) {
        if(isset($hustlerStoreIds[$grandHustlerId])) {
          $teamIds = array_merge($teamIds, $hustlerStoreIds[$grandHustlerId]);
          $tempIds = array_merge($tempIds, $hustlerStoreIds[$grandHustlerId]);
        }
        if(empty($grandHustlerIds)) {
          $grandHustlerIds = $tempIds;
          $tempIds = array();
          $level++;
        }
      }
      $updatedNode = $this->tree->findByAttributes(['store_id' => $storeId]);
      $updatedNode->hustler_store_ids = implode($hustlerIds, ',');
      $updatedNode->grand_hustler_store_ids = $grandHustlerStoreIds;
      $updatedNode->team_store_ids = implode($teamIds, ',');
      $this->tree->save($updatedNode);
    }

    foreach($referredStoreIds as $storeId => $referredIds) {
      $updatedNode = $this->tree->findByAttributes(['store_id' => $storeId]);
      $updatedNode->referred_store_ids = implode($referredIds, ',');
      $this->tree->save($updatedNode);
    }
  }
}
