<?php

namespace Odeo\Domains\Network\Helper;

class ChildInserter {
  
  public function __construct() {
    $this->treeChild = app()->make(\Odeo\Domains\Network\Repository\TreeChildRepository::class);
  }
  
  private $new_cache = [];
  
  private function recursive_reorder($data, $pid, $level_flag = 0) {
    if (isset($data[$pid])) {
      $branch = explode(",", $data[$pid]);
      $depth = "";
      if ($branch[0][0] == "D") {
        $depth = str_replace("D", "", $branch[0]);
      }
      if ($depth == "") {
        $branch = array_merge(["D" . $level_flag], $branch);
        $depth = $level_flag;
        $data[$pid] = implode(",", $branch);
      }
      $this->new_cache[$pid] = $data[$pid];
      foreach ($branch as $string) {
        $temp = explode("|", $string);
        if ($temp[0][0] != "D") {
          $this->recursive_reorder($data, $temp[0], ($depth+1));
        }
      }
    }
  }
  
  public function check($network) {
    $pid = $network->parent_store_id;
    $planId = $network->store->plan->id;
    $patterns = $this->treeChild->findLike($pid);
    
    foreach ($patterns as $item) {
      $data = json_decode($item->child_data, true);
      if (!isset($data[$pid])) $data[$pid] = "";
      $branch = explode(",", $data[$pid]);
      $new_branch = [];
      $overwrite = false;
      foreach ($branch as $string) {
        $temp = explode("|", $string);
        if ($temp[0] == $network->store_id) {
          $temp[1] = $planId;
          $temp[2] = (!$network->position ? "L" : "R");
          $overwrite = true;
        }
        $new_branch[] = implode("|", $temp);
      }
      
      if (!$overwrite) {
        $string = [];
        $string[] = $network->store_id;
        $string[] = $planId;
        $string[] = (!$network->position ? "L" : "R");
        $new_branch[] = implode("|", $string);
      }
      
      $data[$pid] = trim(implode(",", $new_branch), ",");
      $this->recursive_reorder($data, $item->store_id);
      
      $item->child_data = json_encode($this->new_cache);
      $this->treeChild->save($item);
      $this->new_cache = [];
    }
  }
}
