<?php

namespace Odeo\Domains\Network\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Plan;
use Odeo\Domains\Subscription\Model\Store;

class NetworkTree extends Entity
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    
    public $timestamps = true;
    
    public function store() {
        return $this->belongsTo(Store::class);
    }
    
    public function plan() {
        return $this->belongsTo(Plan::class);
    }
    
    public function referredStore() {
        return $this->belongsTo(Store::class, "referred_store_id");
    }
    
    public function parentStore() {
        return $this->belongsTo(Store::class, "parent_store_id");
    }
}
