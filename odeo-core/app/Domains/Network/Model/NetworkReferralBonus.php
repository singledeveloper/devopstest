<?php

namespace Odeo\Domains\Network\Model;

use Odeo\Domains\Core\Entity;

class NetworkReferralBonus extends Entity
{
    public $timestamps = false;
    
    public static function boot() {
      parent::boot();

      static::creating(function($model) {
        $timestamp = $model->freshTimestamp();
        $model->setCreatedAt($timestamp);
      });
    }
}
