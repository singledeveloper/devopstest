<?php

namespace Odeo\Domains\Network\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Plan;

class NetworkSponsorBonus extends Entity
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    
    public $timestamps = true;
    
    public function plan() {
        return $this->belongsTo(Plan::class, "minimal_required_plan_id");
    }

}
