<?php

namespace Odeo\Domains\Network;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;

class OrderBonusRequester {

  public function __construct() {
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->treeParentManager = app()->make(\Odeo\Domains\Network\Helper\ParentManager::class);
    $this->revenue = app()->make(\Odeo\Domains\Transaction\Helper\RevenueInserter::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
  }

  public function create(PipelineListener $listener, $data) {
    if (isset($data["order_bonus"]) && $data["order_bonus"] && isset($data['tree_data'])) {
      $order = isset($data["order_id"]) ? $this->order->findById($data["order_id"]) : false;
      if ($order && $order->seller_store_id != NULL)
        $storeId = $order->seller_store_id;

      if (isset($storeId)) {
        $exact = $data['tree_data'];

        $selfBonus = 0;
        $parentBonus = 0;
        $grandparentBonus = 0;
        if ($exact['me']) {
          $trxData = [
            'order_id' => $order->id,
            'recent_desc' => $exact['me']['store_name']
          ];
          if(isset($data['settlement_at'])) $trxData['settlement_at'] = $data['settlement_at'];
          $selfBonus = $data['order_bonus']['me'];
          $this->inserter->add([
            'user_id' => $exact['me']['user_id'],
            'trx_type' => TransactionType::ORDER_BONUS,
            'cash_type' => CashType::OCASH,
            'amount' => $selfBonus,
            'data' => json_encode($trxData),
            'settlement_at' => $data['settlement_at']
          ]);

          $this->notification->setup($exact['me']['user_id'], NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
          $this->notification->order_bonus($selfBonus, $exact['me']['store_name'], TransactionType::ORDER_BONUS);
        }

        if ($exact['parent']) {
          $trxData = [
            'order_id' => $order->id,
            'recent_desc' => $exact['me']['store_name']
          ];
          if(isset($data['settlement_at'])) $trxData['settlement_at'] = $data['settlement_at'];
          $parentBonus = $data['order_bonus']['parent'];
          $this->inserter->add([
            'user_id' => $exact['parent']['user_id'],
            'trx_type' => TransactionType::MENTOR_ORDER_BONUS,
            'cash_type' => CashType::OCASH,
            'amount' => $parentBonus,
            'data' => json_encode($trxData),
            'settlement_at' => $data['settlement_at']
          ]);

          $this->notification->setup($exact['parent']['user_id'], NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
          $this->notification->order_bonus($parentBonus, $exact['me']['store_name'], TransactionType::MENTOR_ORDER_BONUS);
        }

        if ($exact['grandparent']) {
          $trxData = [
            'order_id' => $order->id,
            'recent_desc' => $exact['me']['store_name']
          ];
          if(isset($data['settlement_at'])) $trxData['settlement_at'] = $data['settlement_at'];
          $grandparentBonus = $data['order_bonus']['grandparent'];
          $this->inserter->add([
            'user_id' => $exact['grandparent']['user_id'],
            'trx_type' => TransactionType::LEADER_ORDER_BONUS,
            'cash_type' => CashType::OCASH,
            'amount' => $grandparentBonus,
            'data' => json_encode($trxData),
            'settlement_at' => $data['settlement_at']
          ]);

          $this->notification->setup($exact['grandparent']['user_id'], NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
          $this->notification->order_bonus($grandparentBonus, $exact['me']['store_name'], TransactionType::LEADER_ORDER_BONUS);
        }

        $this->inserter->run();
        $this->notification->pushOnly();
        $listener->pushQueue($this->notification->queue());

        $this->revenue->insertRevenue($storeId, $order ? $order->id : 0, [
          'me' => $selfBonus,
          'parent' => $parentBonus,
          'grandparent' => $grandparentBonus
        ], true, true);
      }
    }
    return $listener->response(200);
  }
}
