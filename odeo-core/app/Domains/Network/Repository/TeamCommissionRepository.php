<?php

namespace Odeo\Domains\Network\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Network\Model\NetworkTeamCommission;

class TeamCommissionRepository extends Repository {

  public function __construct(NetworkTeamCommission $commission) {
    $this->model = $commission;
  }
  
  public function getRecordedSV($storeId) {
    return $this->model->where("store_id", $storeId)->count();
  }
  
  public function findTodaySV($storeId, $upgradeDatetime) {
    return $this->model->where("store_id", $storeId)->where("day_limit", date("Y-m-d", strtotime($upgradeDatetime)))->get();
  }
  
  public function getTodayClaimed($storeId) {
    return $this->model->where("store_id", $storeId)->whereNotNull("first_amount_claimed_at")
      ->whereNotNull("second_amount_claimed_at")->where("day_limit", date("Y-m-d"))->get();
  }
}
