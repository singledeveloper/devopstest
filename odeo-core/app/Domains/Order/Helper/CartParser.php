<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 5:22 PM
 */

namespace Odeo\Domains\Order\Helper;


use Odeo\Domains\Constant\Service;

class CartParser {


  public function __construct() {
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

  }

  public function parseDefault($cart, $expand = []) {

    $expand = array_flip($expand);

    if (!$cart->cart_data) {
      $cart->cart_data = '{"items":[],"charges":[],"subtotal":0,"total":0}';
    }

    if (!is_array($cart->cart_data)) $cartData = json_decode($cart->cart_data, true);
    else $cartData = $cart->cart_data;

    foreach ($cartData['items'] as &$item) {
      $item['type'] = strtolower(Service::getConstKeyByValue($item['service_id']));
      if (!isset($expand['base_price'])) {
        unset(
          $item['base_price'],
          $item['agent_base_price'],
          $item['merchant_price'],
          $item['margin'],
          $item['mentor_margin'],
          $item['leader_margin']
        );
      }
      if (!isset($expand['extended_detail'])) {
        unset($item['order_extended_detail']);
      }
      $item['quantity'] = 1;
    }


    return [
      'items' => $cartData['items'],
      'charges' => $cartData['charges'],
      'subtotal' => isset($cartData["subtotal"]) ? $cartData["subtotal"] : 0,
      'total' => isset($cartData["total"]) ? $cartData["total"] : 0,
    ];

  }

  public function parsePrice($cartData, $currencyCode = "IDR") {

    if ($currencyCode) {

      foreach ($cartData['items'] as &$item) {
        $item["price"] = $this->currencyHelper->formatPrice($item["price"], $currencyCode, true);
      }
      foreach ($cartData['charges'] as &$charge) {
        $charge["price"] = $this->currencyHelper->formatPrice($charge["price"], $currencyCode, true);
      }

      $cartData['subtotal'] = $this->currencyHelper->formatPrice($cartData['subtotal'], $currencyCode, true);
      $cartData['total'] = $this->currencyHelper->formatPrice(max(0, $cartData['total']), $currencyCode, true);
    }

    return [
      'items' => $cartData['items'],
      'charges' => $cartData['charges'],
      'subtotal' => $cartData['subtotal'],
      'total' => $cartData['total'],
    ];

  }

}
