<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/16/17
 * Time: 10:39 PM
 */

namespace Odeo\Domains\Order\Helper;

use Odeo\Domains\Affiliate\Jobs\AffiliateNotifier;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Vendor\Jabber\Jobs\ReplySocket;

class GatewayCallbacker {

  private $telegramOrder, $telegramManager, $jabberOrder, $smsCenterHistories, $smsCenterManager,
    $jabberManager, $cashManager, $cashTransactions, $currencyHelper, $pulsaRecurringHistories, $smsCenters;

  public function __construct() {
    $this->telegramOrder = app()->make(\Odeo\Domains\Vendor\Telegram\Repository\TelegramOrderRepository::class);
    $this->telegramManager = app()->make(\Odeo\Domains\Vendor\Telegram\TelegramManager::class);
    $this->jabberOrder = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberOrderRepository::class);
    $this->jabberManager = app()->make(\Odeo\Domains\Vendor\Jabber\JabberManager::class);
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->cashTransactions = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->currencyHelper = app()->make(Currency::class);
    $this->orderStatus = app()->make(StatusParser::class);
    $this->smsCenterHistories = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterHistoryRepository::class);
    $this->smsCenterManager = app()->make(\Odeo\Domains\Vendor\SMS\CenterManager::class);
    $this->pulsaRecurringHistories = app()->make(\Odeo\Domains\Order\PulsaRecurring\Repository\PulsaRecurringHistoryRepository::class);
    $this->smsCenters = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterRepository::class);
  }

  public function callback(PipelineListener $listener, $order, $message, $responseCode = Supplier::RC_OK, $additionalData = []) {
    switch ($order->gateway_id) {
      case Payment::AFFILIATE:
        $listener->pushQueue(new AffiliateNotifier([
          'order_id' => $order->id,
          'user_id' => $order->user_id,
          'status' => $this->orderStatus->parse($order)['status'],
          'message' => $message,
          'response_code' => $responseCode,
          'additional_data' => $additionalData
        ]));
        break;
      case Payment::TELEGRAM:
        $telegramOrder = $this->telegramOrder->findByOrderId($order->id);

        $this->telegramManager->initialize($telegramOrder->chat_id, $telegramOrder->telegramStore->token);
        $this->telegramManager->reply($message);
        break;
      case Payment::JABBER:
        $jabberOrder = $this->jabberOrder->findByOrderId($order->id);
        $listener->pushQueue(new ReplySocket(InlineJabber::JAXL_CHAT, [
          'to' => $jabberOrder->jabberUser->email,
          'message' => $message
        ], $jabberOrder->jabberStore ? $jabberOrder->jabberStore->socket_path : ''));
        break;
      case Payment::SMS_CENTER:
        $smsCenterHistories = $this->smsCenterHistories->findByOrderId($order->id);
        $this->smsCenterManager->reply($message, $smsCenterHistories->centerUser->telephone);
        break;
      case Payment::AUTO_RECURRING:
        $pulsaRecurringHistories = $this->pulsaRecurringHistories->findByOrderId($order->id);
        if ($order->status == OrderStatus::COMPLETED &&
          $smsCenter = $this->smsCenters->findByPulsaRecurringId($pulsaRecurringHistories->pulsa_recurring_id)) {
          $json = json_decode($smsCenter->additional_data);
          if ($json && isset($json->on_success_recurring) && isset($json->on_success_recurring->check_saldo_command)) {
            dispatch(new ReplySocket(InlineJabber::JAXL_CHAT, [
              'to' => $smsCenter->jabber_account,
              'message' => $json->on_success_recurring->check_saldo_command
            ]));
          }
        }
        break;
    }
  }

}
