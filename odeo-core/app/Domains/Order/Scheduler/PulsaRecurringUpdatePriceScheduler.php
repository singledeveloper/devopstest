<?php
namespace Odeo\Domains\Order\Scheduler;
use Odeo\Domains\Order\PulsaRecurring\Jobs\PulsaRecurringUpdatePrice;

class PulsaRecurringUpdatePriceScheduler {

  public function run() {
    dispatch(new PulsaRecurringUpdatePrice());
  }

}