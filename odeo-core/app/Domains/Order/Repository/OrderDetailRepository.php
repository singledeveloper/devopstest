<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 11:09 PM
 */

namespace Odeo\Domains\Order\Repository;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Model\OrderDetail;
use Odeo\Domains\Constant\OrderStatus;

class OrderDetailRepository extends Repository {

  public function __construct(OrderDetail $orderDetail) {
    $this->model = $orderDetail;
  }

  public function findSalesComponentTodayByServiceId($serviceId) {
    return $this->findSalesComponentLastNDayByServiceId(0, $serviceId);
  }

  public function findSalesComponentLastNDayByServiceId($day, $serviceId) {

    $dateFrom = Carbon::today()->subDays($day);
    $dateTo = $dateFrom->copy()->endOfDay();

    return $this->model->select(DB::raw('
      SUM(sale_price) as total_sale_amount, 
      SUM(base_price) as total_base_amount, 
      SUM(base_price - odps.current_base_price) as total_profit, 
      count(*) as total
    '))
      ->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->join('service_details', 'service_details.id', '=', 'order_details.service_detail_id')
      ->leftJoin('order_detail_pulsa_switchers as odps', 'odps.order_detail_id', '=', 'order_details.id')
      ->where('orders.status', OrderStatus::COMPLETED)
      ->whereDate('orders.closed_at', '>=', $dateFrom)
      ->whereDate('orders.closed_at', '<=', $dateTo)
      ->where('service_details.service_id', $serviceId)
      ->first();
  }

  public function findSalesComponentByServiceId($serviceId, $dateFrom, $dateTo) {
    $query = $this->getCloneModel();

    $query = $query->select(DB::raw('
      orders.closed_at::date as date,
      SUM(sale_price) as total_sale_amount, 
      SUM(base_price) as total_base_amount, 
      SUM(base_price - odps.current_base_price) as total_profit, 
      count(*) as total
    '))
      ->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->join('service_details', 'service_details.id', '=', 'order_details.service_detail_id')
      ->leftJoin('order_detail_pulsa_switchers as odps', 'odps.order_detail_id', '=', 'order_details.id')
      ->where('orders.status', OrderStatus::COMPLETED)
      ->whereDate('orders.closed_at', '>=', $dateFrom)
      ->whereDate('orders.closed_at', '<=', $dateTo)
      ->where('service_details.service_id', $serviceId)
      ->groupBy('date');

    return $query->get();
  }

  public function salesTodayByPlanId($planId) {
    return $this->salesLastNDayByPlanId(0, $planId);
  }

  public function salesLastNDayByPlanId($day, $planId) {
    $dateFrom = Carbon::today()->subDays($day);
    $dateTo = $dateFrom->copy()->endOfDay();

    $serviceId = Service::PLAN;

    return $this->model
      ->select(DB::raw('
        count(order_details.id) as total_count,
        sum(order_details.base_price) as total_order
      '))
      ->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->join('service_details', 'service_details.id', '=', 'order_details.service_detail_id')
      ->join('order_detail_plans', 'order_detail_plans.order_detail_id', '=', 'order_details.id')
      ->whereDate('orders.closed_at', '>=', $dateFrom)
      ->whereDate('orders.closed_at', '<=', $dateTo)
      ->where('service_details.service_id', $serviceId)
      ->where('order_detail_plans.plan_id', $planId)
      ->first();
  }

  public function getByOrderId($orderId) {
    return $this->model->where('order_id', $orderId)->get();
  }

  public function findByOrderId($orderId) {
    return $this->model->where('order_id', $orderId)->first();
  }

  public function findOrderFromSpecificService($userId, $serviceDetailId, $opcGroup) {
    return $this->model->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->join('payments', 'orders.id', '=', 'payments.order_id')
      ->where('info_id', $opcGroup)
      ->where('service_detail_id', $serviceDetailId)
      ->where('orders.status', OrderStatus::COMPLETED)
      ->where('orders.user_id', $userId)
      ->where('orders.closed_at', '>', Carbon::now()->subDays(5)->toDateTimeString())
      ->first();
  }

}
