<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 11:47 PM
 */

namespace Odeo\Domains\Order\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Model\AgentMasterSale;
use Odeo\Domains\Order\Model\UserPurchase;
use Odeo\Domains\Order\Salesreport\Helper\Initializer;

class AgentMasterSaleRepository extends Repository {

  public function __construct(AgentMasterSale $agentMasterSale) {
    $this->model = $agentMasterSale;
  }

  public function getExisting($data) {
    return $this->model
      ->where('date', $data['date'])
      ->where('user_id', $data['user_id'])
      ->where('service_id', $data['service_id'])
      ->where('store_id', $data['store_id'])
      ->first();
  }


}