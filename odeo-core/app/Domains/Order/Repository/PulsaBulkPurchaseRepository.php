<?php

namespace Odeo\Domains\Order\Repository;

use Odeo\Domains\Constant\BulkPurchase;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Model\PulsaBulkPurchase;

class PulsaBulkPurchaseRepository extends Repository {

  public function __construct(PulsaBulkPurchase $pulsaBulkPurchase) {
    $this->model = $pulsaBulkPurchase;
  }

  public function getLastPurchases($id, $take = 15) {
    $query = $this->model
      ->join('pulsa_bulks', 'pulsa_bulks.id', '=', 'pulsa_bulk_purchases.pulsa_bulk_id')
      ->where('pulsa_bulk_purchases.is_bulk_processed', false)
      ->where('pulsa_bulk_purchases.is_active', true)
      ->where('pulsa_bulks.status', BulkPurchase::APPROVED);
    if ($id != 0) $query = $query->where('pulsa_bulk_purchases.id', '>', $id);
    return $query->take($take)->orderBy('pulsa_bulk_purchases.id', 'asc')
      ->select(
        'pulsa_bulk_purchases.id',
        'pulsa_bulk_purchases.user_id',
        'pulsa_bulk_purchases.request_message'
      )->get();
  }

  public function gets($all = false) {
    $filters = $this->getFilters();

    $query = $this->model->join('users', 'users.id', '=', 'pulsa_bulk_purchases.user_id')
      ->leftJoin('orders', 'orders.id', '=', 'pulsa_bulk_purchases.order_id')
      ->leftJoin('order_details', 'order_details.order_id', '=', 'orders.id')
      ->leftJoin('order_detail_pulsa_switchers', 'order_detail_pulsa_switchers.order_detail_id', '=', 'order_details.id')
      ->where('pulsa_bulk_id', $filters['pulsa_bulk_id'])
      ->where('pulsa_bulk_purchases.is_active', true)
      ->select(
        'pulsa_bulk_purchases.id',
        'pulsa_bulk_purchases.order_id',
        'pulsa_bulk_purchases.user_id',
        'users.name as user_name',
        'pulsa_bulk_purchases.number',
        'order_detail_pulsa_switchers.requested_at',
        'pulsa_bulk_purchases.response_message',
        'pulsa_bulk_purchases.nominal',
        'orders.total',
        'order_detail_pulsa_switchers.serial_number',
        'order_detail_pulsa_switchers.status',
        'pulsa_bulk_purchases.is_bulk_processed'
      )
      ->orderBy('pulsa_bulk_purchases.id', 'asc');

    if ($all) return $query->get();
    return $this->getResult($query);
  }

  public function getsLite($pulsaBulkId) {
    return $this->getCloneModel()->where('pulsa_bulk_id', $pulsaBulkId)
      ->where('pulsa_bulk_purchases.is_active', true)->get();
  }

  public function findLastUnprocessed($pulsaBulkId, $strict = false) {
    if (!$strict)
      return $this->getCloneModel()
        ->join('pulsa_bulks', 'pulsa_bulks.id', '=', 'pulsa_bulk_purchases.pulsa_bulk_id')
        ->where('pulsa_bulk_purchases.is_bulk_processed', false)
        ->where(function($query) use ($pulsaBulkId) {
          $query->where('pulsa_bulks.status', BulkPurchase::APPROVED)
            ->orWhere('pulsa_bulk_id', $pulsaBulkId);
        })
        ->where('pulsa_bulk_purchases.is_active', true)
        ->orderBy('pulsa_bulk_purchases.id', 'desc')
        ->select('pulsa_bulk_purchases.*')
        ->first();
    else return $this->getCloneModel()
      ->where('is_bulk_processed', false)
      ->where('is_active', true)
      ->where('pulsa_bulk_id', $pulsaBulkId)
      ->orderBy('id', 'desc')->first();
  }

  public function findProcessedInGroup($pulsaBulkId) {
    return $this->getCloneModel()->where('is_bulk_processed', true)
      ->where('is_active', true)
      ->where('pulsa_bulk_id', $pulsaBulkId)->first();
  }

}