<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/12/17
 * Time: 2:41 PM
 */

namespace Odeo\Domains\Order;


use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class OrderAccumulator {

  private $orders, $switcherOrder, $store, $currencyHelper, $parser;

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->parser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function run() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(__CLASS__, 'accumulateSalesPerStoreFromDayOne', [
      'cli' => true
    ]));
    $pipeline->execute([]);
  }

  public function accumulateSalesPerStoreFromDayOne(PipelineListener $listener, $data) {

    $stores = $this->store->getAllActiveStore();
    $sales_with_value = [];
    $sales_with_count = [];
    $longest_day = 0;

    foreach ($stores as $item) {
      $orders = $this->switcherOrder->getOrderByStorePerDay($item->id);

      $temp = [];

      foreach($orders as $output) {
        $temp[$output->closed_at] = [
          'total_sales' => $output->total_sales_per_day,
          'total_count' => $output->total_count_per_day
        ];
      }
      $sales_with_value[$item->id] = [
        'name' => $item->name,
        'domain_name' => $this->parser->domainName($item)
      ];
      $sales_with_count[$item->id] = [
        'name' => $item->name,
        'domain_name' => $this->parser->domainName($item)
      ];

      $detail_sales = [];
      $detail_count = [];
      $start = date('Y-m-d', time() - (24 * 3600));

      while(true) {
        if ($start < date('Y-m-d', strtotime($item->created_at))) break;

        if (isset($temp[$start])) {
          $detail_sales[] = $temp[$start]['total_sales'];
          $detail_count[] = $temp[$start]['total_count'];
        }
        else {
          $detail_count[] = 0;
          $detail_sales[] = 0;
        }

        $start = date('Y-m-d', strtotime($start) - (24 * 3600));
      }

      if ($longest_day < count($detail_sales)) $longest_day = count($detail_sales);
      $sales_with_value[$item->id]['details'] = $detail_sales;
      $sales_with_count[$item->id]['details'] = $detail_count;
    }

    $new_sales_value = [];
    foreach ($sales_with_value as $key => $item) {
      $new_sales_value[$key] = $item['details'][0];
    }
    arsort($new_sales_value);

    foreach ($new_sales_value as $key => $item) {
      $new_sales_value[$key] = $sales_with_value[$key];
    }

    $new_sales_count = [];
    foreach ($sales_with_count as $key => $item) {
      $new_sales_count[$key] = $item['details'][0];
    }
    arsort($new_sales_count);
    foreach ($new_sales_count as $key => $item) {
      $new_sales_count[$key] = $sales_with_count[$key];
    }

    if (isset($data['cli']))
      Mail::send('order-sales-per-store-per-day', [
        'sales_value' => $new_sales_value,
        'sales_count' => $new_sales_count,
        'longest_days' => $longest_day
      ], function ($m) {
        $m->from('noreply@odeo.co.id', 'odeo');
        $m->to('report@odeo.co.id')->subject('Sales Revenue - ' . date('Y-m-d'));
      });
    else 
      return $listener->response(200, [
        'view_page' => 'order-sales-per-store-per-day',
        'view_data' => [
          'sales_value' => $new_sales_value,
          'sales_count' => $new_sales_count,
          'longest_days' => $longest_day
        ]
      ]);

    return $listener->response(200);
  }

}