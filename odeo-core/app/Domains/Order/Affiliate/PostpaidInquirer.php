<?php

namespace Odeo\Domains\Order\Affiliate;

use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\BPJS\BPJSManager;
use Odeo\Domains\Inventory\Broadband\BroadbandManager;
use Odeo\Domains\Inventory\Landline\LandlineManager;
use Odeo\Domains\Inventory\MultiFinance\MultiFinanceManager;
use Odeo\Domains\Inventory\PDAM\PDAMManager;
use Odeo\Domains\Inventory\PGN\PGNManager;
use Odeo\Domains\Inventory\Pln\PlnManager;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;

class PostpaidInquirer {

  private $pulsaOdeo;

  public function __construct() {
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $pulsaOdeo = $this->pulsaOdeo->findByInventoryCode(strtoupper($data['denom']));

    if ($pulsaOdeo) {

      $pipeline = new Pipeline();

      $params = [
        'service_detail_id' => $pulsaOdeo->service_detail_id,
        'for_affiliate' => true,
        'item_id' => $pulsaOdeo->id
      ];

      if ($pulsaOdeo->service_detail_id == ServiceDetail::BPJS_KES_ODEO)
        $pipeline->add(new Task(BPJSManager::class, 'validatePostpaidInventory', $params));
      else if ($pulsaOdeo->service_detail_id == ServiceDetail::PLN_POSTPAID_ODEO)
        $pipeline->add(new Task(PlnManager::class, 'validatePostpaidInventory', $params));
      else if ($pulsaOdeo->service_detail_id == ServiceDetail::PULSA_POSTPAID_ODEO)
        $pipeline->add(new Task(PulsaManager::class, 'validatePostpaidInventory', array_merge($params, [
          'postpaid_type' => PostpaidType::getConstKeyByValue($pulsaOdeo->category)
        ])));
      else if ($pulsaOdeo->service_detail_id == ServiceDetail::PDAM_ODEO)
        $pipeline->add(new Task(PDAMManager::class, 'validatePostpaidInventory', $params));
      else if ($pulsaOdeo->service_detail_id == ServiceDetail::PGN_ODEO)
        $pipeline->add(new Task(PGNManager::class, 'validatePostpaidInventory', $params));
      else if ($pulsaOdeo->service_detail_id == ServiceDetail::MULTI_FINANCE_ODEO)
        $pipeline->add(new Task(MultiFinanceManager::class, 'validatePostpaidInventory', $params));
      else if ($pulsaOdeo->service_detail_id == ServiceDetail::LANDLINE_ODEO)
        $pipeline->add(new Task(LandlineManager::class, 'validatePostpaidInventory', $params));
      else if ($pulsaOdeo->service_detail_id == ServiceDetail::BROADBAND_ODEO)
        $pipeline->add(new Task(BroadbandManager::class, 'validatePostpaidInventory', $params));
      else return $listener->response(400, 'Kode ' . $data['denom'] . ' tidak valid');

      $pipeline->execute($data);

      if ($pipeline->fail()) return $listener->response(400, $pipeline->errorMessage);

      try {
        return $listener->response(200, array_merge($pipeline->data['item_detail'], [
          'subtotal' => $pipeline->data['subtotal'],
          'discount' => $pipeline->data['discount'],
          'total' => $pipeline->data['total']
        ]));
      }
      catch(\Exception $e) {
        clog('test_inq', json_encode($pipeline->data));
        return $listener->response(400, 'Inquiry sedang gangguan');
      }
    }

    return $listener->response(400, 'Kode ' . $data['denom'] . ' tidak valid');
  }

}