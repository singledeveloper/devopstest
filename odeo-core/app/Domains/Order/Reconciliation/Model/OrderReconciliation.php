<?php

namespace Odeo\Domains\Order\Reconciliation\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Order\Model\Order;
use Odeo\Domains\Inventory\Model\Service;

class OrderReconciliation extends Entity {

  protected $dates = ['settlement_at'];

  public function order() {
    return $this->belongsTo(Order::class);
  }
 
  public function service() {
    return $this->belongsTo(Service::class);
  }
}