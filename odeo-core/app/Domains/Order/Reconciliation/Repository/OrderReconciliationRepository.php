<?php

namespace Odeo\Domains\Order\Reconciliation\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Reconciliation\Model\OrderReconciliation;

class OrderReconciliationRepository extends Repository {

  public function __construct(OrderReconciliation $orderReconciliation) {
    $this->model = $orderReconciliation;
  }

  public function gets() {

    $filters = $this->getFilters();

    $query = $this->getCloneModel()->whereNotNull('payment_name');

    $query = $query->with('service');

    if (isset($filters['search'])) {
      if (isset($filters['search']['is_reconciled'])) {
        $query = $query->where('is_reconciled', $filters['search']['is_reconciled']);
      }
      if (isset($filters['search']['diff_reason'])) {
        $query = $query->where('diff_reason', $filters['search']['diff_reason']);
      }
      if (isset($filters['search']['start_date'])) {
        $query = $query->whereDate('transaction_date', '>=', $filters['search']['start_date']);
      }
      if (isset($filters['search']['end_date'])) {
        $query = $query->whereDate('transaction_date', '<=', $filters['search']['end_date']);
      }
      if (isset($filters['search']['order_id'])) {
        $query = $query->where('order_id', $filters['search']['order_id']);
      }
    }

    return $this->getResult($query->orderBy($filters['sort_by'], $filters['sort_type']));
  }

  public function getSummaryByDate($date) {
    return $this->getCloneModel()
      ->select(\DB::raw('diff_reason, sum(total_diff) as total_diff'))
      ->whereDate('transaction_date', '=', $date)
      ->where('is_reconciled', true)
      ->whereNotNull('diff_reason')
      ->groupBy('diff_reason')->get();
  }

  public function updateData($orderId) {
    return \DB::update("
      update order_reconciliations set (
        total_cash_other, 
        total_cash, 
        total_deposit, 
        total_marketing, 
        payment_name,
        is_reconciled, 
        updated_at, 
        trx_count
      ) =
      (select 
        sum(case when user_id is null and store_id is null then amount else 0 end),
        sum(case when user_id is not null and cash_type = 'cash' then amount else 0 end),
        sum(case when cash_type = 'deposit' then amount else 0 end),
        sum(case when cash_type = 'ads' or cash_type = 'rush' or cash_type = 'cashback' then amount else 0 end),
        null,
        false,
        now(),
        count(id)
      from cash_transactions where cash_transactions.order_id = order_reconciliations.order_id
      group by order_id
      ) where order_id = :order_id
    ", ['order_id' => $orderId]);
  }
}