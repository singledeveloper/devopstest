<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/2/17
 * Time: 1:17 PM
 */

namespace Odeo\Domains\Order\Salesreport\Ocommerce;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;

class SalesOcommerceReportSelector {

  private $salesOcommerces, $currencyHelper;

  public function __construct() {

    $this->salesOcommerces = app()->make(\Odeo\Domains\Order\Salesreport\Ocommerce\Repository\SalesOcommerceRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

  }

  public function _transforms($data, Repository $repository) {

    $output = [];

    $output['sales'] = $data->sales;
    $output['sales_target'] = $data->sales_target;
    $output['total_sale_amount'] = $this->currencyHelper->formatPrice($data->total_sale_amount);
    $output['total_base_amount'] = $this->currencyHelper->formatPrice($data->total_base_amount);

    return $output;

  }

  public function getSalesOcommerceReport(PipelineListener $listener, $data) {

    $this->salesOcommerces->normalizeFilters($data);

    $dayNum = $data['day_num'];

    $response = [];

    foreach ($this->salesOcommerces->getLastNDay($dayNum) as $salesOcommerce) {
      $date = $salesOcommerce->date;
      $type = $salesOcommerce->service->displayed_name;
      
      $response[$date]['sales'][$type] = $salesOcommerce->sales;
      $response[$date]['sales_target'][$type] = $salesOcommerce->sales_target;
      $response[$date]['total_sale_amount'][$type] = $this->currencyHelper->formatPrice($salesOcommerce->total_sale_amount);
      $response[$date]['total_base_amount'][$type] = $this->currencyHelper->formatPrice($salesOcommerce->total_base_amount);
      $response[$date]['total_profit'][$type] = $this->currencyHelper->formatPrice($salesOcommerce->total_profit);
    }

    return $listener->response(200, ['ocommerce' => $response]);

  }

}