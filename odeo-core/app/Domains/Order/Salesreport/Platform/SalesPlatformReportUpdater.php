<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/2/17
 * Time: 1:45 PM
 */

namespace Odeo\Domains\Order\Salesreport\Platform;

use Carbon\Carbon;
use Odeo\Domains\Order\Salesreport\Helper\Initializer;

class SalesPlatformReportUpdater {

  private $platformReport, $orderDetails;

  public function __construct() {

    $this->platformReport = app()->make(\Odeo\Domains\Order\Salesreport\Platform\Repository\SalesPlatformRepository::class);
    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);

  }

  public function updatePlatformReport($plan, $day = Initializer::YESTERDAY) {

    $salesLastNDayPayment = $this->platformReport->findSalesLastNDayByPlanId($day, $plan['id']);
    if (!$salesLastNDayPayment) {

      $sales = $this->orderDetails->salesLastNDayByPlanId($day, $plan['id']);
      $salesPlatformReport = $this->platformReport->getNew();
      $salesPlatformReport->plan_id = $plan['id'];
      $salesPlatformReport->date = Carbon::now()->subDays($day)->format('Y-m-d');
      $salesPlatformReport->sales = $sales->total_count;
      $salesPlatformReport->sales_value = $sales->total_order ? $sales->total_order : 0;
      $salesPlatformReport->is_locked = FALSE;
      $salesPlatformReport->sales_target = Initializer::DEFAULT_TARGET_OCOMMERCE;

      $this->platformReport->save($salesPlatformReport);

    } else if (!$salesLastNDayPayment['is_locked']) {
      $sales = $this->orderDetails->salesLastNDayByPlanId($day, $plan['id']);
      $salesLastNDayPayment->sales = $sales->total_count;
      $salesLastNDayPayment->sales_value = $sales->total_order ? $sales->total_order : 0;
      $salesLastNDayPayment->is_locked = TRUE;

      $this->platformReport->save($salesLastNDayPayment);
    }

    $salesPlatformReport = $this->platformReport->findSalesTodayByPlanId($plan['id']);
    if (!$salesPlatformReport) {
      $salesPlatformReport = $this->platformReport->getNew();
      $salesPlatformReport->plan_id = $plan['id'];
      $salesPlatformReport->date = Carbon::now()->format('Y-m-d');
      $salesPlatformReport->sales_target = Initializer::DEFAULT_TARGET_PLATFORM;
    }

    $sales = $this->orderDetails->salesTodayByPlanId($plan['id']);
    $salesPlatformReport->sales = $sales->total_count;
    $salesPlatformReport->sales_value = $sales->total_order ? $sales->total_order : 0;

    $this->platformReport->save($salesPlatformReport);

  }

}