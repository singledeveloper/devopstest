<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/8/17
 * Time: 8:24 PM
 */

namespace Odeo\Domains\Order\Receipt\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class UserReceiptConfig extends Entity {

  public function user() {
    return $this->belongsTo(User::class);
  }
}