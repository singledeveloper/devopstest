<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/8/17
 * Time: 8:20 PM
 */

namespace Odeo\Domains\Order\Receipt;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Receipt\Helper\PaperCalculator;

class ReceiptConfigSelector {

  private $receiptConfigs, $paperCalculator;

  public function __construct() {
    $this->receiptConfigs = app()->make(\Odeo\Domains\Order\Receipt\Repository\UserReceiptConfigRepository::class);
    $this->paperCalculator = app()->make(PaperCalculator::class);
  }

  public function getReceiptConfig(PipelineListener $listener, $data) {

    $receiptConfig = $this->receiptConfigs->findByUserId($data['auth']['user_id']);

    if (!$receiptConfig) {
      return $listener->response(204);
    }

    $selectedPaperWidth = $receiptConfig->paper_width
      ? $receiptConfig->paper_width
      : PaperCalculator::defaultWidth;

    return $listener->response(200, [
      'title' => $receiptConfig->title,
      'slogan' => $receiptConfig->slogan,
      'address' => $receiptConfig->address,
      'footer' => $receiptConfig->footer,
      'phone_number' => $receiptConfig->phone_number,
      'selected_paper_width' => $selectedPaperWidth,
      'paper_width_options' => $this->paperCalculator->getPossibleWidth()
    ]);
  }

}