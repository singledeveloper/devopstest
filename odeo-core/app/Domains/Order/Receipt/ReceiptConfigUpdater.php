<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/9/17
 * Time: 3:19 PM
 */

namespace Odeo\Domains\Order\Receipt;


use Odeo\Domains\Core\PipelineListener;

class ReceiptConfigUpdater {

  private $receiptConfigs;

  public function __construct() {
    $this->receiptConfigs = app()->make(\Odeo\Domains\Order\Receipt\Repository\UserReceiptConfigRepository::class);
  }

  public function updateReceiptConfig(PipelineListener $listener, $data) {

    $receiptConfig = $this->receiptConfigs->findByUserId($data['auth']['user_id']);

    if (!$receiptConfig) {
      $receiptConfig = $this->receiptConfigs->getNew();
      $receiptConfig->user_id = $data['auth']['user_id'];
    }

    $receiptConfig->title = $data['title'] ?? $receiptConfig->title;
    $receiptConfig->slogan = $data['slogan'] ?? $receiptConfig->slogan;
    $receiptConfig->address = $data['address'] ?? $receiptConfig->address;
    $receiptConfig->footer = $data['footer'] ?? $receiptConfig->footer;
    $receiptConfig->paper_width = $data['paper_width'] ?? $receiptConfig->paperWidth;
    $receiptConfig->phone_number = $data['phone_number'] ?? $receiptConfig->phone_number;

    $this->receiptConfigs->save($receiptConfig);

    return $listener->response(200);
  }
}