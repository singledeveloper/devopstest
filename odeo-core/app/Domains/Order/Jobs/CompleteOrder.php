<?php

namespace Odeo\Domains\Order\Jobs;

use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\BudgetConverter;
use Odeo\Domains\Marketing\RevenueUpdater;
use Odeo\Domains\Order\OrderAgentSalesProcessor;
use Odeo\Domains\Order\OrderSalesProcessor;
use Odeo\Domains\Order\OrderVerificator;
use Odeo\Domains\Subscription\MdPlus\ReferralCashbackRequester;
use Odeo\Jobs\Job;
use Illuminate\Support\Facades\Redis;

class CompleteOrder extends Job {

  use SerializesModels;

  private $orderId;
  private $redis;

  public function __construct($orderId) {
    parent::__construct();
    $this->orderId = $orderId;
  }

  public function handle() {
    $pipeline = new Pipeline;
    $this->redis = Redis::connection();

    $this->lockOrder();

    $pipeline->add(new Task(OrderVerificator::class, 'verifyAndCalculateData'));
    $pipeline->add(new Task(OrderAgentSalesProcessor::class, 'convertDeposit'));
    $pipeline->add(new Task(OrderSalesProcessor::class, 'convertDeposit'));
    $pipeline->add(new Task(BudgetConverter::class, 'rush'));
    $pipeline->add(new Task(ReferralCashbackRequester::class, 'insertCashback'));
    $pipeline->add(new Task(RevenueUpdater::class, 'updateRevenue'));
    $pipeline->execute([
      'order_id' => $this->orderId,
      'auth' => [
        'type' => 'admin'
      ]
    ]);

    $this->removeLock();
  }

  private function lockOrder() {
    if (!$this->redis->setnx($this->getRedisKey(), 1)) {
      throw new \Exception('recall complete order');
    }
    $this->redis->expire($this->getRedisKey(), 60);
  }

  private function removeLock() {
    $this->redis->del($this->getRedisKey());
  }

  private function getRedisKey() {
    return 'complete_order_lock_' . $this->orderId;
  }

}
