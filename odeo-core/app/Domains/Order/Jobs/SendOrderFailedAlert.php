<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Order\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendOrderFailedAlert extends Job {

  use SerializesModels;

  public function __construct() {
    parent::__construct();
  }

  public function handle() {
    $orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class)->getFailedOrderAlert();

    if (!($count = $orders->count())) {
      return;
    }

    if (app()->environment() == 'production') {

      Mail::send('emails.order_fail_alert', [
        'data' => $orders,
        'count' => $count
      ], function ($m) {

        $m->from('noreply@odeo.co.id', 'odeo');

        $m->to('ops@odeo.co.id')->subject('Order Fail Alert - ' . time());
      });

    }


  }


}
