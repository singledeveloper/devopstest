<?php

namespace Odeo\Domains\Order;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\MarketingConfig;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\Jobs\SendOrderComplete;
use Odeo\Domains\Transaction\CashUpdater;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;

class OrderSalesProcessor {

  public function __construct() {
    $this->mdr = app()->make(\Odeo\Domains\Channel\Repository\StoreMdrRepository::class);
    $this->treeParentManager = app()->make(\Odeo\Domains\Network\Helper\ParentManager::class);
    $this->revenueInserter = app()->make(\Odeo\Domains\Transaction\Helper\RevenueInserter::class);
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->depositConverter = app()->make(\Odeo\Domains\Transaction\Helper\DepositConverter::class);
    $this->tempDeposit = app()->make(\Odeo\Domains\Transaction\Repository\StoreTempDepositRepository::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->queue = app()->make(\Odeo\Domains\Marketing\Helper\QueueManager::class);
  }

  public function convertDeposit(PipelineListener $listener, $data) {
    $order = $this->orders->findById($data['order_id']);
    $this->tempDeposit->lock();
    $tempDeposit = $this->tempDeposit->findByOrderId($data['order_id']);
    $switchExcludeStoreIds = $data['agent_store_ids'] ?? [];
    $serviceId = $data['service_id'];

    if ($order->seller_store_id) {
      $treeData = $this->treeParentManager->get_exact_parents($order->seller_store_id, true);
      $treeData['additionals'] = [];
    } else return $listener->response(200);

    if ($order->status == OrderStatus::COMPLETED && $order->seller_store_id && $tempDeposit) {
      $depositData = array_merge([
        'order_id' => $order->id,
        'recent_desc' => isset($data['channel_name']) ? $data['channel_name'] : $treeData['me']['store_name']
      ], (isset($data['channel_id']) ? ['channel_id' => $data['channel_id']] : [])
        , (isset($data['order']['settlement_at']) ? ['settlement_at' => $data['order']['settlement_at']] : []));

      $firstStoreId = $treeData['me']['store_id'];
      $firstStoreName = $treeData['me']['store_name'];
      $firstParentStoreId = $treeData['parent'] ? $treeData['parent']['store_id'] : 0;
      $firstGrandParentStoreId = $treeData['grandparent'] ? $treeData['grandparent']['store_id'] : 0;

      $failedIds = array();

      //temp
      $isAgent = isset($data['order']['is_agent']) && isset($data['disable_mentor_leader']);

      $h2hManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaH2HManager::class);
      $isH2H = $h2hManager->isUserH2H($order->user_id);

      // hustler
      $paidAmount = $tempDeposit->amount + $data['order_bonus']['me'];
      $depositAmount = $tempDeposit->amount;
      $userId = $treeData['me']['user_id'];
      if ($order->payment->info_id == Payment::OPC_GROUP_CASH_ON_DELIVERY) {
        $listener->addNext(new Task(CashUpdater::class, 'cod'));
        $treeData['me'] = false;
      } else if ($data['order']['is_agent'] || $isH2H) {
        $treeData['me'] = false;
      } else {
        $switched = false;
        $userId = $treeData['me']['user_id'];
        $storeId = $treeData['me']['store_id'];
        $storeName = $treeData['me']['store_name'];
        do {
          $trxData = array_merge($depositData, ['is_community' => true]);
          try {
            $this->depositConverter->convert([
              'order_id' => $order->id,
              'user_id' => $userId,
              'store_id' => $storeId,
              'paid_amount' => $paidAmount,
              'deposit_amount' => $depositAmount,
              'revenue_type' => ''
            ], $trxData);
            $switched = false;
          } catch (InsufficientFundException $e) {
            if (!$switched) {
              $failedIds[] = $storeId;
              $this->notification->setup($userId, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
              $this->notification->order_fail($treeData['me']['store_name'], $treeData['me']['store_id']);

              $switched = true;
              $nextStores = $this->queue->getByDeposit($serviceId, $depositAmount, 'hustler');
            }
            $nextStore = array_pop($nextStores);
            if (!$nextStore) {
              $this->depositConverter->default($paidAmount, $depositAmount, ['order_id' => $order->id]);
              $storeId = NULL;
              break;
            }
            $userId = $nextStore['user_id'];
            $storeId = $nextStore['store_id'];
            $storeName = $nextStore['store_name'];
            $depositData['recent_desc'] = $nextStore['store_name'];
          }
        } while ($switched);
        $order->actual_store_id = is_null($storeId) ? StoreStatus::getStoreDefault() : $storeId;
        $this->orders->save($order);
        if (isset($treeData['me']['store_id'])) $treeData['me']['is_community'] = true;
        if ($storeId != $firstStoreId) {
          $treeData = $this->treeParentManager->get_exact_parents($storeId, true);
          if ($treeData['me']) {
            $treeData['me']['is_community'] = true;
            $treeData['additionals'] = [];
            $firstStoreId = $treeData['me']['store_id'];
            $firstStoreName = $treeData['me']['store_name'];
            $firstParentStoreId = $treeData['parent'] ? $treeData['parent']['store_id'] : 0;
            $firstGrandParentStoreId = $treeData['grandparent'] ? $treeData['grandparent']['store_id'] : 0;
          } else {
            $treeData['parent'] = false;
            $treeData['grandparent'] = false;
            $treeData['additionals'] = [];
          }
        }
      }
      $this->tempDeposit->delete($tempDeposit);
      isset($treeData['me']['store_id']) && ($switchExcludeStoreIds[] = $treeData['me']['store_id']);

      // mentor
      $depositAmount = $data['total_deposit'] + $data['order_bonus']['grandparent'];
      $paidAmount = $tempDeposit->amount;
      if ($order->payment->info_id == Payment::OPC_GROUP_CASH_ON_DELIVERY || $isAgent || $isH2H) {
        $treeData['parent'] = false;
      } else {
        $skipParent = false;
        if ($treeData['parent']) {
          $switched = false;
          $storeId = $treeData['parent']['store_id'];
          $storeName = $treeData['parent']['store_name'];
          $userId = $treeData['parent']['user_id'];
          $nextStore = NULL;
        } else {
          $switched = true;
          $nextStores = $this->queue->getByDeposit($serviceId, $depositAmount, 'mentor', $switchExcludeStoreIds);
          $nextStore = array_pop($nextStores);
          if (!$nextStore) {
            $this->depositConverter->default($paidAmount, $depositAmount, ['order_id' => $order->id]);
            $treeData['parent'] = false;
            $skipParent = true;
          } else {
            $userId = $nextStore['user_id'];
            $storeId = $nextStore['store_id'];
            $storeName = $nextStore['store_name'];
            $treeData['parent'] = $nextStore;
          }
        }
        if (!$skipParent) {
          do {
            $trxData = array_merge($depositData, ($storeId != $firstParentStoreId || !$data['order']['is_agent'] ? ['is_community' => true] : []));
            $trxData['recent_desc'] = $storeName;
            $trxData['from'] = $firstStoreName;
            $trxData['role'] = 'mentor';
            try {

              $this->depositConverter->convert([
                'order_id' => $order->id,
                'user_id' => $userId,
                'store_id' => $storeId,
                'paid_amount' => $paidAmount,
                'deposit_amount' => $depositAmount,
                'revenue_type' => 'mentor'
              ], $trxData);

              $switched = false;
            } catch (InsufficientFundException $e) {
              if (!$switched) {
                $failedIds[] = $storeId;
                $this->notification->setup($userId, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
                $this->notification->order_fail($treeData['parent']['store_name'], $treeData['parent']['store_id']);
                $switched = true;
                $nextStores = $this->queue->getByDeposit($serviceId, $depositAmount, 'mentor', $switchExcludeStoreIds);
              }
              $nextStore = array_pop($nextStores);
              if (!$nextStore) {
                $this->depositConverter->default($paidAmount, $depositAmount, ['order_id' => $order->id]);
                $treeData['parent'] = false;
                $treeData['grandparent'] = false;
                break;
              }
              $userId = $nextStore['user_id'];
              $storeId = $nextStore['store_id'];
              $storeName = $nextStore['store_name'];
            }

          } while ($switched);
          if ($treeData['parent']) {
            if ($storeId != $firstParentStoreId || !$data['order']['is_agent']) $treeData['parent']['is_community'] = true;
            if ($storeId != $firstParentStoreId && isset($nextStore)) {

              $newTree = $this->treeParentManager->get_exact_parents($storeId, true);
              $treeData['parent'] = $nextStore;
              $treeData['parent']['is_community'] = true;
              $treeData['grandparent'] = $newTree['parent'];
            }
          }
        }
      }
      isset($treeData['parent']['store_id']) && ($switchExcludeStoreIds[] = $treeData['parent']['store_id']);

      // leader
      $depositAmount = $data['total_deposit'];
      $paidAmount = $depositAmount + $data['order_bonus']['grandparent'];
      if ($order->payment->info_id == Payment::OPC_GROUP_CASH_ON_DELIVERY || $isAgent || $isH2H)
        $treeData['grandparent'] = false;
      else {
        $skipGrandParent = false;
        if ($treeData['grandparent'] && $treeData['grandparent']['store_id'] != $treeData['me']['store_id']) {
          $switched = false;
          $storeId = $treeData['grandparent']['store_id'];
          $storeName = $treeData['grandparent']['store_name'];
          $userId = $treeData['grandparent']['user_id'];
          $nextStore = NULL;
        } else {
          $switched = true;
          $nextStores = $this->queue->getByDeposit($serviceId, $depositAmount, 'leader', $switchExcludeStoreIds);
          $nextStore = array_pop($nextStores);
          if (!$nextStore) {
            $this->depositConverter->default($paidAmount, $depositAmount, ['order_id' => $order->id]);
            $treeData['grandparent'] = false;
            $skipGrandParent = true;
          } else {
            $userId = $nextStore['user_id'];
            $storeId = $nextStore['store_id'];
            $storeName = $nextStore['store_name'];
            $treeData['grandparent'] = $nextStore;
          }
        }
        if (!$skipGrandParent) {
          do {
            $trxData = array_merge($depositData, ($storeId != $firstGrandParentStoreId || !$data['order']['is_agent'] ? ['is_community' => true] : []));
            $trxData['recent_desc'] = $storeName;
            $trxData['from'] = $firstStoreName;
            $trxData['role'] = 'leader';
            try {

              $this->depositConverter->convert([
                'order_id' => $order->id,
                'user_id' => $userId,
                'store_id' => $storeId,
                'paid_amount' => $paidAmount,
                'deposit_amount' => $depositAmount,
                'revenue_type' => 'leader'
              ], $trxData);

              $switched = false;
            } catch (InsufficientFundException $e) {
              if (!$switched) {
                $failedIds[] = $storeId;
                $this->notification->setup($userId, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
                $this->notification->order_fail($treeData['grandparent']['store_name'], $treeData['grandparent']['store_id']);
                $switched = true;
                $nextStores = $this->queue->getByDeposit($serviceId, $depositAmount, 'leader', $switchExcludeStoreIds);
              }
              $nextStore = array_pop($nextStores);
              if (!$nextStore) {
                $this->depositConverter->default($paidAmount, $depositAmount, ['order_id' => $order->id]);
                $treeData['grandparent'] = false;
                break;
              }
              $userId = $nextStore['user_id'];
              $storeId = $nextStore['store_id'];
              $storeName = $nextStore['store_name'];
            }

          } while ($switched);

          if ($treeData['grandparent']) {
            if ($storeId != $firstGrandParentStoreId || !$data['order']['is_agent']) $treeData['grandparent']['is_community'] = true;
            if ($storeId != $firstGrandParentStoreId && isset($nextStore)) {

              $treeData['grandparent'] = $nextStore;
              $treeData['grandparent']['is_community'] = true;
            }
          }
        }
      }
      isset($treeData['grandparent']['store_id']) && ($switchExcludeStoreIds[] = $treeData['grandparent']['store_id']);

      // order from marketing budget
      $count = 0;
      $paidAmount = $tempDeposit->amount + $data['order_bonus']['default'];
      $depositAmount = $tempDeposit->amount;

      $nextStoresByProfit = $this->queue->getByRush($serviceId, $data['order_bonus']['default'], $depositAmount,
        $switchExcludeStoreIds, 'profit');
      $nextStoresByRush = $this->queue->getByRush($serviceId, $data['order_bonus']['default'], $depositAmount,
        $switchExcludeStoreIds, 'rush');
      if ($count % 2 || (Carbon::now()->lt(Carbon::createFromTime(MarketingConfig::MIN_RUSH_HOUR, 0, 0)) &&
          Carbon::now()->gt(Carbon::createFromTime(MarketingConfig::MAX_RUSH_HOUR, 0, 0)))) {

        $nextStore = array_pop($nextStoresByProfit);
      } else {
        $nextStore = array_pop($nextStoresByRush);
      }
      $additionalStoreIds = [];

      $trxData = array_merge($depositData, ['is_community' => true]);
      while ($count < MarketingConfig::MAX_ORDER_FROM_MARKETING_BUDGET && $nextStore) {
        if (!in_array($nextStore['store_id'], $additionalStoreIds)) {
          $userId = $nextStore['user_id'];
          $storeId = $nextStore['store_id'];
          $trxData['recent_desc'] = $nextStore['store_name'];
          try {

            $this->depositConverter->convert([
              'order_id' => $order->id,
              'user_id' => $userId,
              'store_id' => $storeId,
              'paid_amount' => $paidAmount,
              'deposit_amount' => $depositAmount,
              'revenue_type' => 'rush',
              'with_marketing_budget' => true
            ], $trxData);

            $count++;
            $nextStore['is_community'] = true;
            $treeData['additionals'][] = $nextStore;
            $additionalStoreIds[] = $nextStore['store_id'];
          } catch (InsufficientFundException $e) {
          }
        }
        if ($count % 2 || (Carbon::now()->lt(Carbon::createFromTime(MarketingConfig::MIN_RUSH_HOUR, 0, 0)) &&
            Carbon::now()->gt(Carbon::createFromTime(MarketingConfig::MAX_RUSH_HOUR, 0, 0)))
        ) {
          $nextStore = array_pop($nextStoresByProfit);
        } else {
          $nextStore = array_pop($nextStoresByRush);
        }
      }
      Redis::pipeline(function ($pipe) use ($failedIds) {
        foreach ($failedIds as $failedId) {
          $pipe->hincrby('odeo_core:store_failed_order_count', $failedId, 1);
        }
      });
      $listener->pushQueue($this->notification->queue());
    }

    return $listener->response(200, ['tree_data' => $treeData]);
  }

  public function sendEmailComplete(PipelineListener $listener, $data) {
    $order = $this->orders->findById($data['order_id']);
    if ($order->status != OrderStatus::COMPLETED)
      return $listener->response(400, "Can't send order email, not completed yet.");
    $listener->pushQueue(new SendOrderComplete($order, $data));

    return $listener->response(200);
  }
}
