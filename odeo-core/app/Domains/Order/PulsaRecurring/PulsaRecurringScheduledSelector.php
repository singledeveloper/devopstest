<?php

namespace Odeo\Domains\Order\PulsaRecurring;

use Carbon\Carbon;

use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Order\PulsaRecurring\Helper\PulsaRecurringHelper;
use Odeo\Domains\Order\PulsaRecurring\Repository\PulsaRecurringRepository;
use Odeo\Domains\Transaction\Helper\Currency;

class PulsaRecurringScheduledSelector implements SelectorListener {

  private $currencyHelper, $pulsaRecurring, $pulsaRecurringHelper;

  public function __construct() {
    $this->currencyHelper = app()->make(Currency::class);
    $this->pulsaRecurring = app()->make(PulsaRecurringRepository::class);
    $this->pulsaRecurringHelper = app()->make(PulsaRecurringHelper::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $output['pulsa_recurring_id'] = $item->id;
    $output['number'] = revertTelephone($item->destination_number);
    $output['inventory_id'] = $item->pulsa_odeos_id;
    $output['name'] = $item->pulsa_odeos_name;
    $output['price'] = $this->currencyHelper->formatPrice($item->price);
    $output['service_detail_id'] = $item->pulsa_odeos_service_detail_id;
    $output['type'] = Recurring::getTypeName($item->type);
    $output['store_id'] = $item->stores_id;
    $output['store_name'] = isset($item->stores_name) ? $item->stores_name : 'Marketplace';
    $output['next_payment'] = $item->next_payment;
    $output['created_at'] = Carbon::parse($item->created_at)->toDateTimeString();
    $output['last_executed_at'] = isset($item->last_executed_at) ? Carbon::parse($item->last_executed_at)->toDateTimeString() : null;
    $output['expired_at'] = isset($item->expired_at) ? Carbon::parse($item->expired_at)->toDateString() : null;

    return $output;
  }

  public function get(PipelineListener $listener, $data) {

    $this->pulsaRecurring->normalizeFilters($data);

    $recurrings = [];

    foreach ($this->pulsaRecurring->getScheduledByUserId($data['auth']['user_id']) as $recurring) {
      $recurrings[] = $this->_transforms($recurring, $this->pulsaRecurring);
    }
    if (sizeof($recurrings) > 0)
      return $listener->response(200, array_merge([
        "pulsa_recurrings" => $this->_extends($recurrings, $this->pulsaRecurring)
      ], $this->pulsaRecurring->getPagination()));
    return $listener->response(204);

  }

}
