<?php

namespace Odeo\Domains\Order\PulsaRecurring;

use Carbon\Carbon;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository;
use Odeo\Domains\Order\PulsaRecurring\Helper\PulsaRecurringHelper;
use Odeo\Domains\Order\PulsaRecurring\Repository\PulsaRecurringRepository;
use Odeo\Domains\Transaction\Helper\Currency;

class Configurator {

  private $pulsaRecurring, $pulsaRecurringHelper, $currencyHelper;

  public function __construct() {
    $this->pulsaRecurring = app()->make(PulsaRecurringRepository::class);
    $this->pulsaRecurringHelper = app()->make(PulsaRecurringHelper::class);
    $this->currencyHelper = app()->make(Currency::class);
  }

  public function getTypes(PipelineListener $listener, $data) {
    $recurringTypes = [];
    foreach (Recurring::TYPES_FOR_PULSA as $type) {
      $recurringTypes[] = [
        "type" => $type,
        "name" => trans('recurring.' . $type),
        "show_date" => ($type == Recurring::TYPE_ONCE || $type == Recurring::TYPE_MONTH),
        "show_expired" => ($type !== Recurring::TYPE_ONCE)
      ];
    }
    return $listener->response(200, $recurringTypes);
  }

  public function create(PipelineListener $listener, $data) {
    if (isset($data['recurring_date']) && strtotime($data['recurring_date']) < time())
      return $listener->response(400, 'The date must be at least tomorrow.');

    if ($data['recurring_type'] == Recurring::TYPE_MONTH) {
      $day = intval(date('j', strtotime($data['recurring_date'])));
      if ($day <= 0 || $day > 28) return $listener->response(400, 'Please choose valid date between 1 and 28');
      $data['recurring_type'] .= '_' . $day;
    }
    else if ($data['recurring_type'] == 'once') $data['recurring_type'] .= '_' . $data['recurring_date'];

    $pulsaOdeoRepo = app()->make(PulsaOdeoRepository::class);
    $pulsaOdeo = $pulsaOdeoRepo->findById($data['item_id']);
    $isPostpaid = $pulsaOdeo && (PostpaidType::getConstKeyByValue($pulsaOdeo->category) != '');

    $pulsaRecurring = $this->pulsaRecurring->getNew();
    $pulsaRecurring->user_id = $data['auth']['user_id'];
    $pulsaRecurring->pulsa_odeo_id = $data['item_id'];
    $pulsaRecurring->destination_number = isset($data['number']) ?
      ($isPostpaid ? $data['number'] : purifyTelephone($data['number'])) : null;
    $pulsaRecurring->type = $data['recurring_type'];
    $pulsaRecurring->store_id = $data['store_id'] ?? null;
    $pulsaRecurring->expired_at = $data['recurring_expired'] ?? null;
    $pulsaRecurring->next_payment = $this->pulsaRecurringHelper->getNextPayment($data['recurring_type'], Carbon::now()->toDateString());
    $pulsaRecurring->price = $this->pulsaRecurringHelper->getEstimatedPrice($data['item_id'], $data['store_id'] ?? null, $data['auth']['user_id']);
    $this->pulsaRecurring->save($pulsaRecurring);

    return $listener->response(201);
  }

  public function updatePrice(PipelineListener $listener, $data) {
    $price = $this->pulsaRecurringHelper->getEstimatedPrice($data['inventory_id'], $data['store_id'] ?? null, $data['auth']['user_id']);
    $pulsaRecurring = $this->pulsaRecurring->findById($data['pulsa_recurring_id']);
    $pulsaRecurring->price = $price;
    $this->pulsaRecurring->save($pulsaRecurring);

    return $listener->response(200, [
      'price' => $this->currencyHelper->formatPrice($price)
    ]);
  }

  public function toggle(PipelineListener $listener, $data) {

    if ($pulsaRecurring = $this->pulsaRecurring->findById($data['pulsa_recurring_id'])) {
      if (isAdmin() || $pulsaRecurring->user_id == $data['auth']['user_id']) {
        $pulsaRecurring->is_enabled = !($pulsaRecurring->is_enabled);
        if ($pulsaRecurring->is_enabled) {
          $pulsaRecurring->next_payment = $this->pulsaRecurringHelper->getNextPayment($pulsaRecurring->type, $pulsaRecurring->last_executed_at ?? $pulsaRecurring->created_at);
          $pulsaRecurring->price = $this->pulsaRecurringHelper->getEstimatedPrice($pulsaRecurring->pulsa_odeo_id, $pulsaRecurring->store_id, $data['auth']['user_id']);
        }
        $this->pulsaRecurring->save($pulsaRecurring);

        return $listener->response(200, ['is_enabled' => $pulsaRecurring->is_enabled]);
      }
      else return $listener->response(400, "You don't have access to this data.");
    }

    return $listener->response(400, "No data.");
  }

  public function remove(PipelineListener $listener, $data) {

    if ($pulsaRecurring = $this->pulsaRecurring->findById($data['pulsa_recurring_id'])) {
      if (isAdmin() || $pulsaRecurring->user_id == $data['auth']['user_id']) {
        $pulsaRecurring->is_active = false;
        $this->pulsaRecurring->save($pulsaRecurring);

        return $listener->response(200);
      }
      else return $listener->response(400, "You don't have access to this data.");
    }

    return $listener->response(400, "No data.");
  }

}
