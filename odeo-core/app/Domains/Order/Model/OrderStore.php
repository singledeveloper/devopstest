<?php
namespace Odeo\Domains\Order\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;

class OrderStore extends Entity {

  public $timestamps = false;

  public function order() {
    return $this->belongsTo(Order::class);
  }

  public function store() {
    return $this->belongsTo(Store::class);
  }

}
