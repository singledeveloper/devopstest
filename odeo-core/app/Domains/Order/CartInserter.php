<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:27 PM
 */

namespace Odeo\Domains\Order;


use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\WarrantyStatus;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\DDLending\LoanPaymentValidator;
use Odeo\Domains\Inventory\Bolt\BoltManager;
use Odeo\Domains\Inventory\BPJS\BPJSManager;
use Odeo\Domains\Inventory\Broadband\BroadbandManager;
use Odeo\Domains\Inventory\EMoney\EMoneyManager;
use Odeo\Domains\Inventory\MultiFinance\MultiFinanceManager;
use Odeo\Domains\Inventory\PDAM\PDAMManager;
use Odeo\Domains\Inventory\Creditbill\CreditBillManager;
use Odeo\Domains\Inventory\GameVoucher\GameVoucherManager;
use Odeo\Domains\Inventory\GooglePlay\GooglePlayManager;
use Odeo\Domains\Inventory\PGN\PGNManager;
use Odeo\Domains\Inventory\Transportation\TransportationManager;
use Odeo\Domains\Inventory\UserInvoice\UserInvoiceManager;
use Odeo\Domains\Inventory\Flight\FlightManager;
use Odeo\Domains\Inventory\Hotel\HotelManager;
use Odeo\Domains\Inventory\Landline\LandlineManager;
use Odeo\Domains\Inventory\Pln\PlnManager;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;
use Odeo\Domains\Inventory\Repository\ServiceDetailRepository;
use Odeo\Domains\Payment\Pax\EdcTransactionRequester;
use Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter;
use Validator;

class CartInserter {


  private $carts, $cartParser, $cartChecker, $currencyHelper;

  public function __construct() {
    $this->carts = app()->make(\Odeo\Domains\Order\Repository\CartRepository::class);
    $this->cartParser = app()->make(\Odeo\Domains\Order\Helper\CartParser::class);
    $this->cartChecker = app()->make(\Odeo\Domains\Order\Helper\CartChecker::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->serviceDetails = app()->make(ServiceDetailRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
  }

  public function addToCart(PipelineListener $listener, $data) {
    $serviceDetail = $this->serviceDetails->findById($data['service_detail_id']);
    $data['service_id'] = $serviceDetail->service_id;

    $validator = Validator::make($data, Service::getValidatorVariables($data['service_id']), [
      'item_detail.number' => 'Please input a valid number'
    ]);

    if ($validator->fails()) return $listener->response(400, $validator->errors()->all());

    if (!$cart = $this->carts->getUserCart($data['auth']['user_id'])) {
      $cart = $this->carts->getNew();
      $cart->user_id = $data['auth']['user_id'];
    }

    switch ($data['service_id']) {

      case Service::PLAN: {
        $listener->addNext(new Task(\Odeo\Domains\Subscription\StoreVerificator::class, 'guard'));

        if ($store = $this->store->findById($data['store_id'])) {

          if ($store->status == StoreStatus::OK || $store->status == StoreStatus::CANCELLED)
            return $listener->response(400, "Can't add this store to cart.");

          $store->load('plan');
          $plan = $store->plan;
          $price = $plan->minimal_months * $plan->cost_per_month;

          if ($store->status == StoreStatus::WAIT_FOR_UPGRADE_VERIFY) {
            $plan = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class)->findById($data['plan_id']);
            $discount = 0;
            if ($store->renewal_at != null) {
              $discount = ceil(floor((strtotime($store->renewal_at) - time()) / (60 * 60 * 24)) /
                (30 * $store->plan->minimal_months) * $store->plan->cost_per_month);
            }
            $price = ($plan->minimal_months * $plan->cost_per_month > $price) ?
              ($plan->minimal_months * ($plan->cost_per_month - $discount)) : $plan->minimal_months * $plan->cost_per_month;
          }

          $listener->addNext(new Task(__CLASS__, 'addItem', [
            'cart' => $cart,
            'service_detail_id' => $data['service_detail_id'],
            'service_id' => $data['service_id'],
            'sale_price' => $price,
            'base_price' => $price,
            'margin' => 0,
            'item_detail' => [
              'store_id' => $store->id,
              'subscription_months' => $plan->minimal_months,
              'store_subdomain' => $store->subdomain_name,
              'store_name' => $store->name
            ],
            'item_id' => $plan->id,
            'name' => $plan->name
          ]));
        }

        break;
      }

      case Service::OCASH:
        $topupRepo = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupRepository::class);
        if (!$topup = $topupRepo->findLastTopup($data['auth']['user_id']))
          return $listener->response(400, 'Bad Request.');

        $listener->appendData([
          'name' => $serviceDetail->service->displayed_name,
        ]);
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
          'sale_price' => $topup->amount,
          'base_price' => $topup->amount,
          'margin' => 0,
          'item_detail' => ['topup_id' => $topup->id],
          'item_id' => $topup->id,
        ]));
        break;

      case Service::CREDIT_BILL:
        $listener->addNext(new Task(CreditBillManager::class, 'validateInventory', [
          'credit_bill_id' => $data['credit_bill']['id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
          'item_detail' => [
            'credit_bill_id' => $data['credit_bill']['id']
          ],
          'item_id' => $data['credit_bill']['id'],
        ]));
        break;

      case Service::USER_INVOICE:
        $listener->addNext(new Task(UserInvoiceManager::class, 'validateInventory', [
          'invoice_number' => $data['item_id'],
          'store_id' => $data['store_id'] ?? NULL,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
          'item_detail' => [
            'invoice_number' => $data['item_id']
          ],
          'item_id' => $data['item_id'],
        ]));
        break;

      case Service::ODEPOSIT:
        $depositRepo = app()->make(\Odeo\Domains\Transaction\Repository\StoreDepositRepository::class);
        if (!$deposit = $depositRepo->findLastStoreDeposit($data['store_id']))
          return $listener->response(400, 'Bad Request.');

        $newItem = [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
          'sale_price' => $deposit->amount,
          'base_price' => $deposit->amount,
          'margin' => 0,
          'item_detail' => [
            'store_id' => $data['store_id'],
            'store_deposit_id' => $deposit->id
          ],
          'item_id' => $deposit->id,
          'name' => $serviceDetail->service->displayed_name,
        ];

        if (isset($data['bonus'])) {
          $newItem['bonus'] = $data['bonus'];
          $newItem['topup_amount'] = $data['topup_amount'];
          $newItem['preset'] = $data['preset'];
        }
       
        $listener->addNext(new Task(__CLASS__, 'addItem', $newItem));
        break;

      case Service::WARRANTY:

        $listener->addNext(new Task(\Odeo\Domains\Subscription\StoreVerificator::class, 'guard'));

        $warranty = app()->make(\Odeo\Domains\Subscription\Repository\WarrantyRepository::class)->findById($data['warranty_id']);
        if (!$warranty || ($warranty && $warranty->status != WarrantyStatus::PENDING))
          return $listener->response(400, "Can't add this warranty to cart.");

        $store = $warranty->store;
        $plan = $store->plan;

        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
          'sale_price' => $plan->warranty * $plan->minimal_months,
          'base_price' => $plan->warranty * $plan->minimal_months,
          'margin' => 0,
          'item_detail' => [
            'warranty_id' => $data['warranty_id'],
            'store_id' => $store->id
          ],
          'item_id' => $data['warranty_id'],
          'name' => 'Odeo Safe+'
        ]));
        break;

      case Service::MD_PLUS:

        $mdPlusPlan = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\MdPlusPlanRepository::class)->findById($data['md_plus_plan_id']);
        
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
          'sale_price' => $mdPlusPlan->discounted_price,
          'base_price' => $mdPlusPlan->discounted_price,
          'margin' => 0,
          'item_detail' => [
            'md_plus_plan_id' => $data['md_plus_plan_id'],
            'store_id' => $data['store_id'],
            'is_auto_renew' => $data['is_auto_renew']
          ],
          'item_id' => $data['md_plus_plan_id'],
          'name' => 'MD Plus ' . $mdPlusPlan->name
        ]));

        break;

      case Service::FLIGHT:
        $listener->addNext(new Task(FlightManager::class, 'addToCart', [
          'service_detail_id' => $data['service_detail_id'],
          'store_id' => $data['store_id'],
          'cart' => $cart,
          'service_id' => $data['service_id'],
        ]));
        break;

      case Service::HOTEL:
        $listener->addNext(new Task(HotelManager::class, 'addToCart', [
          'service_detail_id' => $data['service_detail_id'],
          'store_id' => $data['store_id'],
          'cart' => $cart,
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::PULSA:
      case Service::PAKET_DATA:
        $listener->addNext(new Task(PulsaManager::class, 'validateInventory', [
          'item_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id'],
          'number' => $data['item_detail']['number']
        ]));
        isset($data['store_id']) && $listener->addNext(new Task(PriceGetter::class, 'applyAgentPrice', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::GAME_VOUCHER:
        $listener->addNext(new Task(GameVoucherManager::class, 'validateInventory', [
          'item_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id'],
          'number' => $data['item_detail']['number']
        ]));
        isset($data['store_id']) && $listener->addNext(new Task(PriceGetter::class, 'applyAgentPrice', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id'],
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::GOOGLE_PLAY:
        $listener->addNext(new Task(GooglePlayManager::class, 'validateInventory', [
          'item_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        isset($data['store_id']) && $listener->addNext(new Task(PriceGetter::class, 'applyAgentPrice', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::BOLT:
        $listener->addNext(new Task(BoltManager::class, 'validateInventory', [
          'item_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        isset($data['store_id']) && $listener->addNext(new Task(PriceGetter::class, 'applyAgentPrice', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::PLN:
        $listener->addNext(new Task(PlnManager::class, 'validateInventory', [
          'item_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        isset($data['store_id']) && $listener->addNext(new Task(PriceGetter::class, 'applyAgentPrice', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::TRANSPORTATION:
        $listener->addNext(new Task(TransportationManager::class, 'validateInventory', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        isset($data['store_id']) && $listener->addNext(new Task(PriceGetter::class, 'applyAgentPrice', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::EMONEY:
        $listener->addNext(new Task(EMoneyManager::class, 'validateInventory', [
          'item_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        isset($data['store_id']) && $listener->addNext(new Task(PriceGetter::class, 'applyAgentPrice', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::PLN_POSTPAID:
        $listener->addNext(new Task(PlnManager::class, 'validatePostpaidInventory', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
          'debug' => true
        ]));
        break;

      case Service::PULSA_POSTPAID:
        $listener->addNext(new Task(PulsaManager::class, 'validatePostpaidInventory', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::BROADBAND:
        $listener->addNext(new Task(BroadbandManager::class, 'validatePostpaidInventory', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::LANDLINE:
        $listener->addNext(new Task(LandlineManager::class, 'validatePostpaidInventory', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::PDAM:
        $listener->addNext(new Task(PDAMManager::class, 'validatePostpaidInventory', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::PGN:
        $listener->addNext(new Task(PGNManager::class, 'validatePostpaidInventory', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::BPJS_KES:
        $listener->addNext(new Task(BPJSManager::class, 'validatePostpaidInventory', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::MULTI_FINANCE:
        $listener->addNext(new Task(MultiFinanceManager::class, 'validatePostpaidInventory', [
          'inventory_id' => $data['item_id'],
          'store_id' => $data['store_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id']
        ]));
        break;

      case Service::DDLENDING_PAYMENT:
        $listener->addNext(new Task(LoanPaymentValidator::class, 'validate', [
          'service_detail' => $serviceDetail,
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'name' => $serviceDetail->service->displayed_name,
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
        ]));
        break;

      case Service::EDC_TRANSACTION:
        $listener->addNext(new Task(EdcTransactionRequester::class, 'validate', [
          'service_detail' => $serviceDetail,
        ]));
        $listener->addNext(new Task(__CLASS__, 'addItem', [
          'name' => $serviceDetail->service->displayed_name,
          'cart' => $cart,
          'service_detail_id' => $data['service_detail_id'],
          'service_id' => $data['service_id'],
        ]));
        break;
    }

    return $listener->response(201);
  }


  public function addItem(PipelineListener $listener, $data) {

    $uid = uniqid();

    if (!isset($data["quantity"])) $data["quantity"] = 1;

    if (isset($data['cart'])) {
      $cart = $data['cart'];
    } else {

      $cart = $this->carts->getUserCart($data['auth']['user_id']);

      if (!$cart) {
        $cart = $this->carts->getNew();
        $cart->user_id = $data['auth']['user_id'];
      }

    }


    $cartData = $this->cartParser->parseDefault($cart, ['base_price']);
    $cartData['subtotal'] += $data["sale_price"];
    $cartData['total'] += $data["sale_price"];

    $item["uid"] = $uid;
    $item["price"] = $data["sale_price"];
    $item['inventory_id'] = isset($data['inventory_id']) ? $data['inventory_id'] : $data['item_id'];
    $item['base_price'] = $data['base_price'];

    if (isset($data['agent_base_price'])) $item['agent_base_price'] = $data['agent_base_price'];
    if (isset($data['order_extended_detail'])) $item['order_extended_detail'] = json_encode($data['order_extended_detail']);

    if (isset($data['bonus'])) {
      $item['bonus'] = $data['bonus'];
      $item['preset'] = $data['preset'];
      $item['topup_amount'] = $data['topup_amount'];
    }

    $item['merchant_price'] = isset($data['merchant_price']) ? $data['merchant_price'] : $data['base_price'];
    $item['margin'] = $data['margin'];
    $item['mentor_margin'] = isset($data['mentor_margin']) ? $data['mentor_margin'] : 0;
    $item['leader_margin'] = isset($data['leader_margin']) ? $data['leader_margin'] : 0;

    $item['referral_cashback'] = $data['referral_cashback'] ?? 0;

    if (isset($data['item_detail'])) ksort_recursive($data['item_detail']);
    else $data['item_detail'] = [];

    $item['item_detail'] = $data['item_detail'];
    $item['service_id'] = $data['service_id'];
    $item['service_detail_id'] = $data['service_detail_id'];
    $item['store_inventory_detail_id'] = $data['store_inventory_detail_id'] ?? null;
    $item['quantity'] = $data['quantity'];
    $item['name'] = $data['name'];

    $cartData['items'][] = $item;

    $cart->cart_data = json_encode($cartData);
    $this->carts->save($cart);

    return $listener->response(201, ["cart_id" => $cart->id]);
  }

  public function addCharge(PipelineListener $listener, $data) {

    $validator = Validator::make($data, [
      'type' => 'required|in:payment_code,voucher,ocash,credit,admin_fee',
      'price.amount' => 'required',
      'price.currency' => 'required'
    ]);

    if ($validator->fails()) return $listener->response(400, $validator->errors()->all());


    $cart = $this->carts->getUserCart($data['auth']['user_id']);

    if (!$cart) return $listener->response(400, trans('errors.cart.not_found'));

    $cartData = $this->cartParser->parseDefault($cart, ['base_price']);


    $currency = $this->currencyHelper->getCurrency($data["price"]["currency"]);

    $item = [];
    $item['type'] = $data['type'];
    $item["price"] = $data["price"]["amount"] * $currency->sell_rate;

    $cartData['total'] += $item["price"];


    $cartData['charges'][] = $item;
    $cart->cart_data = json_encode($cartData);

    $this->carts->save($cart);

    return $listener->response(200);

  }

  public function useOCash(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    $cart = $this->carts->getUserCart($userId);
    $cartData = $this->cartParser->parseDefault($cart, ['base_price']);

    $userCash = $this->cashManager->getCashBalance($userId)[$userId];
    $cashAmount = $cartData['subtotal'];

    if ($cashAmount < $userCash) {
      $price = $this->currencyHelper->formatPrice($cashAmount);

      $item = [];
      $item['type'] = 'ocash';
      $item['price'] = -$price['amount'];

      $cartData['total'] += $item['price'];

      $cartData['charges'][] = $item;
      $cart->cart_data = json_encode($cartData);

      $this->carts->save($cart);

      return $listener->response(200, ['amount' => $price['amount'], 'currency' => $price['currency']]);
    } else {
      return $listener->response(400, trans('errors.cash.insufficient_cash', ['cash' => 'oCash']));
    }
  }

}
