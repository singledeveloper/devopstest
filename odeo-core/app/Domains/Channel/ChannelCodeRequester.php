<?php

namespace Odeo\Domains\Channel;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\ChannelConfig;
use Odeo\Domains\Constant\ChannelStatus;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Channel\Jobs\GenerateQRCodeImage;
use Elibyy\TCPDF\Facades\TCPDF;

class ChannelCodeRequester {

  public function __construct() {
    $this->channelCodes = app()->make(\Odeo\Domains\Channel\Repository\ChannelCodeRepository::class);
  }

  public function create(PipelineListener $listener, $data) {
    // dispatch job to generate url image
    $listener->pushQueue(new GenerateQRCodeImage($data['amount']));
    return $listener->response(200);
  }

  public function stat(PipelineListener $listener, $data) {
    $all = $this->channelCodes->getModel()->withTrashed()->count();
    $available = $this->channelCodes->getModel()->count();
    $channelCodes = $this->channelCodes->getModel()->orderBy('code', 'ASC')->get();
    $codes = [];
    foreach($channelCodes as $code) {
      $codes[] = $code->code;
    }
    return $listener->response(200, ["used" => $all - $available, "available" => $available, "codes" => $codes]);
  }

  public function printCode($data) {
    set_time_limit($data['amount'] * 3);
    if(isset($data['id'])) {
      $channelCodes = $this->channelCodes->getChannelCodes($data['amount'], $data['id']);
    } else {
      $channelCodes = $this->channelCodes->getChannelCodes($data['amount']);
    }

    $pdf = new TCPDF();
    $pageWidth = 420;
    $pageHeight = 297;
    $margin = 5;
    $itemWidth = $itemHeight = 25;
    $originX = ($margin + ($pageWidth - floor(($pageWidth - 2 * $margin) / $itemWidth) * $itemWidth)) / 2;
    $originY = ($margin + ($pageHeight - floor(($pageHeight - 2 * $margin) / $itemHeight) * $itemHeight)) / 2;
    $x = $originX;
    $y = $originY;

    $pdf::SetAuthor("odeo");
    $pdf::SetCreator("odeo");
    $pdf::SetTitle("odeoQR");
    $pdf::AddFont('AvenirLTStd-Book','regular',app()->basePath('resources/fonts/AvenirLTStdBook.php'));
    $pdf::SetFont('AvenirLTStd-Book', '', 6);
    $pdf::SetTextColor(10,37,40);
    $pdf::SetAutoPageBreak(false);

    foreach ($channelCodes as $channelCode) {
      if($x == $originX && $y == $originY) {
        $pdf::AddPage('L', array(420,297));
      }
      if($channelCode->qr_url) {
        if(substr($channelCode->qr_url,-3) == 'png') {
          $pdf::Image(ChannelConfig::CODE_BASE_URL . $channelCode->qr_url, $x + 3.25, $y + 2.75, 13.5, 13.5);
        } else {
          $pdf::ImageSvg(ChannelConfig::CODE_BASE_URL . $channelCode->qr_url, $x + 1, $y + 0.5, $w=18, $h=18);
        }
      }
      $pdf::MultiCell(18,5,$channelCode->code,0,'C',false,0,$x + 1,$y + 16.5);
      $pdf::Circle($x+10, $y+10, 10, 0, 360, 'D', ['color'=>[223, 223, 223], 'width'=>0.1]);

      $x += $itemWidth;
      if($x + $itemWidth >= $pageWidth) {
        $x = $originX;
        $y += $itemHeight;

        if($y + $itemHeight >= $pageHeight) {
          $x = $originX;
          $y = $originY;
        }
      }
    }
    $pdf::Output('odeoQR_' . (isset($data['id']) ? $data['id'] : $data['amount']) . '_' . date('Ymd'), 'I');
  }
}
