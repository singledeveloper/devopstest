<?php

namespace Odeo\Domains\Channel\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;
use Odeo\Domains\Account\Model\User;

class StoreChannel extends Entity {

  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = ['created_at', 'updated_at'];

  public $incrementing = false;

  public function user() {
    return $this->belongsTo(User::class);
  }

  public function store() {
    return $this->belongsTo(Store::class);
  }
}
