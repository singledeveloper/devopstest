<?php

namespace Odeo\Domains\Channel;

use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\ChannelConfig;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;

class ChannelPaymentProcessor {

  public function __construct() {
    $this->mdr = app()->make(\Odeo\Domains\Channel\Repository\StoreMdrRepository::class);
    $this->network = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->channel = app()->make(\Odeo\Domains\Channel\Repository\StoreChannelRepository::class);
    $this->userCash = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->userPoints = app()->make(\Odeo\Domains\Account\Repository\UserPointsRepository::class);
    $this->cashTransaction = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->budgetInserter = app()->make(\Odeo\Domains\Marketing\Helper\BudgetInserter::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
  }

  public function newUserCashback(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    $returningUser = $this->cashTransaction->findNewUserCashback($userId, $data['channel_id']);

    if(!$returningUser) {
      $userPoints = $this->userPoints->findById($userId);
      if(!$userPoints) {
        $userPoints = $this->userPoints->getNew();
        $userPoints->user_id = $userId;
        $userPoints->point = 0;
        $this->userPoints->save($userPoints);
      }
      $points = $userPoints->point;

      $maxCashbackAmount = 10000;
      $p = min(4, floor(-0.5 + sqrt(1 + 8 * ($points / 100)) / 2));
      if($p > 0) $maxCashbackAmount = 25000 * $p;

      $cashbackAmount = min(
        min(CashConfig::CASHBACK_MAX, max(CashConfig::CASHBACK_MIN, floor($points / 100) * CashConfig::CASHBACK_MULTIPLIER)) * $data['amount']['amount'],
        $maxCashbackAmount
      );

      $insertAmount = $this->budgetInserter->convertBudget(
        $userId, NULL,
        TransactionType::NEW_CASHBACK, CashType::OCASHBACK, $cashbackAmount,
        [
          'channel_id' => $data['channel_id'],
          'recent_desc' => $data['channel_name'],
        ]);

      if($insertAmount) {
        $this->notification->setup($userId, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
        $this->notification->cashback($insertAmount, $data['channel_name']);
        $listener->pushQueue($this->notification->queue());
      }
    }

    return $listener->response(200);
  }

  public function MDR(PipelineListener $listener, $data) {
    $mdr = $this->mdr->findByAttributes('store_id', $data['store_id']);

    if($mdr) {
      $mdrAmount = round($data['amount']['amount'] * $mdr->base_mdr);
      $this->cashInserter->add([
        'user_id' => $data['user_id'],
        'trx_type' => TransactionType::MDR,
        'cash_type' => CashType::OCASH,
        'amount' => -$mdrAmount,
        'data' => json_encode([
          'channel_id' => $data['channel_id'],
          'recent_desc' => $data['channel_name'],
        ])
      ]);

      $referrer = $this->network->findByAttributes('store_id', $data['store_id'])->referred;
      $referrerMdrBonus = round(ChannelConfig::MDR_REFERRER_MULTIPLIER * $mdrAmount);
      $userCashbackBonus = $mdrAmount - $referrerMdrBonus;

      if($referrerMdrBonus > 0) {
        $this->cashInserter->add([
          'user_id' => ((time() - strtotime($mdr->created_at)) > 365 * 86400 || !$referrer) ? NULL : $referrer->user_id,
          'trx_type' => TransactionType::MDR_BONUS,
          'cash_type' => CashType::OCASH,
          'amount' => $referrerMdrBonus,
          'data' => json_encode([
            'channel_id' => $data['channel_id'],
            'recent_desc' => $data['channel_name'],
          ])
        ]);
      }

      $this->cashInserter->add([
        'user_id' => $data['auth']['user_id'],
        'trx_type' => TransactionType::CASHBACK,
        'cash_type' => CashType::OCASH,
        'amount' => $userCashbackBonus,
        'data' => json_encode([
          'channel_id' => $data['channel_id'],
          'recent_desc' => $data['channel_name'],
        ])
      ]);
    }

    $this->cashInserter->run();

    return $listener->response(200);
  }
}
