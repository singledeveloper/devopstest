<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/12/18
 * Time: 17.41
 */

namespace Odeo\Domains\Vendor\Permata\Scheduler;


use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Vendor\Permata\Helper\ApiManager;

class CheckBalanceScheduler {

  /**
   * @var VendorDisbursementRepository
   */
  private $vendorDisbursementRepo;

  /**
   * @var ApiManager
   */
  private $apiManager;

  private function initialize() {
    $this->vendorDisbursementRepo = app()->make(VendorDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
  }

  public function run() {
    $this->initialize();

    $vendor = $this->vendorDisbursementRepo->findById(VendorDisbursement::PERMATA);
    $res = $this->apiManager->balanceInquiry();
    $vendor->balance = $res['BalInqRs']['InqInfo']['AccountBalanceAmount'];

    $this->vendorDisbursementRepo->save($vendor);
  }
}
