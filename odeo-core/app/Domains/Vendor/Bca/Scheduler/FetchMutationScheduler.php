<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/11/17
 * Time: 4:20 PM
 */

namespace Odeo\Domains\Vendor\Bca\Scheduler;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Notification\Helper\InternalNoticer;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager;
use Odeo\Domains\Vendor\Bca\Jobs\SendBcaPendingSuspect;

class FetchMutationScheduler {

  private $apiManager, $accountNumberRepo, $bcaInquiryRepo, $redis;

  const REDIS_RECON_ERROR_COUNT = 'odeo_core::bca_recon_error_count';

  public function __construct() {
    $this->apiManager = app()->make(ApiManager::class);
    $this->accountNumberRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->bcaInquiryRepo = app()->make(BankBcaInquiryRepository::class);
    $this->redis = Redis::connection();
  }

  public function run() {
    $accountNumber = $this->accountNumberRepo->findById(2);

    $pendingInquiries = $this->bcaInquiryRepo->getPendingInquiry()->all();
    $totalPendingInquiries = count($pendingInquiries);

    $lastScrappedDate = $this->getLastScraperDate($pendingInquiries, $accountNumber);
    $maxScrapedDate = $this->maxScrapedDate();

    $errorCount = $this->redis->get(self::REDIS_RECON_ERROR_COUNT) ?? 0;

    if (isProduction() && $maxScrapedDate->diffInDays($lastScrappedDate) > 10) {
      dispatch(new SendBcaPendingSuspect());
    }

    $response = $this->apiManager->getMutation(ApiManager::$ACCOUNTNUMBER[0],
      $lastScrappedDate->toDateString(), $maxScrapedDate->toDateString()
    );

    if ($response['status_code'] != 200 || !isset($response['data']['StartBalance'])) {

      if ($response['status_code'] == 404) {
        $this->updateLastScrapedDate($accountNumber, $maxScrapedDate);
      }

      clog('fetch_bca_inquiries', \json_encode($response));
      clog('fetch_bca_inquiries', $lastScrappedDate->toDateString() . ' - ' . $maxScrapedDate->toDateString());

      $this->handleInternalNotice($errorCount);

      return;
    }

    $existingInquiries = $this->bcaInquiryRepo->getInquriesFrom($lastScrappedDate)
      ->keyBy(function ($inquiry) {
        return $this->getInquiryKey($inquiry);
      })->all();

    $scrapedData = [];

    $balance = $response['data']['StartBalance'];

    foreach ($response['data']['Data'] as $result) {

      $inquiryDate = $result['TransactionDate'] == 'PEND'
        ? null
        : Carbon::createFromFormat('d/m', $result['TransactionDate']);

      // handle scrape on year change
      if (isset($inquiryDate)) {
        if($inquiryDate->greaterThan(Carbon::today())) {
          $inquiryDate->subYear();
        }

        $inquiryDate = $inquiryDate->toDateString();
      }

      $debit = $this->getAmount($result, 'D');
      $credit = $this->getAmount($result, 'C');
      $balance += $debit == 0 ? $credit : -$debit;

      $inquiryData = $this->transformInquiry($accountNumber, $inquiryDate,
        $maxScrapedDate, $debit, $credit, $balance, $result);

      if ($totalPendingInquiries) {

        $matchedPending = array_filter($pendingInquiries, function ($item) use ($inquiryData) {
          $pendingInquiryKey = $this->getInquiryKey($item, false);
          $inquiryKey = $this->getInquiryKey($inquiryData, false);
          return $pendingInquiryKey == $inquiryKey;
        });


        if (!empty($matchedPending) && $inquiryDate) {
          foreach ($matchedPending as $key => $val) {

            $val->date = $inquiryDate;
            $val->pending_scrapped_date = null;
            $this->bcaInquiryRepo->save($val);

            unset($pendingInquiries[$key]);
            $totalPendingInquiries--;
          }

          continue;
        }

      }

      if (isset($existingInquiries[$this->getInquiryKey($inquiryData)])) {
        continue;
      }

      $scrapedData[] = $inquiryData;

    }

    if (count($scrapedData)) {
      $this->bcaInquiryRepo->saveBulk($scrapedData);
    } else {
      clog('fetch_bca_inquiries', 'no data for date ' . $lastScrappedDate->toDateString() . 'to ' . $maxScrapedDate->toDateString());
    }

    $this->updateLastScrapedDate($accountNumber, $maxScrapedDate);

    $errorCount && $this->resetErrorCount();
  }

  private function updateLastScrapedDate($accountNumber, $maxScrapedDate) {
    $accountNumber->last_scrapped_date = $maxScrapedDate;
    $accountNumber->last_time_scrapped = $maxScrapedDate;

    $this->accountNumberRepo->save($accountNumber);
  }

  private function getAmount($result, $comparator) {
    return $result['TransactionType'] == $comparator ? $result['TransactionAmount'] : 0;
  }


  private function transformInquiry($accountNumber, $inquiryDate, $maxScrapedDate, $debit, $credit, $balance, $result) {
    return [
      'bank_scrape_account_number_id' => $accountNumber->id,
      'date' => $inquiryDate,
      'pending_scrapped_date' => $inquiryDate ? null : $maxScrapedDate->toDateString(),
      'description' => $this->transfromDesc($result['Trailer']),
      'branch' => $result['BranchCode'],
      'debit' => $debit,
      'credit' => $credit,
      'balance' => $balance,
      'name' => $result['TransactionName'],
      "created_at" => $maxScrapedDate,
      "updated_at" => $maxScrapedDate,
    ];
  }

  private function getLastScraperDate($pendingInquiries, $accountNumber) {
    if (isStaging()) {
      return Carbon::createFromDate(2016, 8, 29);
    }

    return count($pendingInquiries)
      ? Carbon::parse($pendingInquiries[0]->pending_scrapped_date)
      : Carbon::parse($accountNumber->last_scrapped_date);
  }

  private function maxScrapedDate() {
    return isStaging()
      ? Carbon::createFromDate(2016, 9, 1)
      : Carbon::now();
  }

  private function getInquiryKey($inquiry, $includeDate = true) {

    $temp = [
      $this->transfromDesc(trim($inquiry['description'])),
      $inquiry['branch'],
      number_format($inquiry['debit']),
      number_format($inquiry['credit']),
      number_format($inquiry['balance']),
      trim($inquiry['name'])
    ];

    if ($includeDate) {
      array_unshift($temp, $inquiry['date'] ?? 'PEND');
    }

    return join('#', $temp);
  }

  private function transfromDesc($desc) {
    return preg_replace('!\s+!', ' ', $desc);
  }

  private function resetErrorCount($count = 0) {
    $this->redis->set(self::REDIS_RECON_ERROR_COUNT, $count);
  }

  private function handleInternalNotice($count) {
    if ($count == 14) {
      $noticer = app()->make(InternalNoticer::class);
      $noticer->saveMessage('BCA Scrape Failed: ' . ApiManager::$CORPORATEID . ' failed to scrape in the last 15 minutes.');
      $this->resetErrorCount();
    } else {
      $count += 1;
      $this->resetErrorCount($count);
    }
  }

}
