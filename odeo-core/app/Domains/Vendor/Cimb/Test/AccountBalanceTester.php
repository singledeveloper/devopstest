<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/14/17
 * Time: 6:00 PM
 */

namespace Odeo\Domains\Vendor\Cimb\Test;


use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Cimb\Helper\ApiManager;

class AccountBalanceTester {

  public function get(PipelineListener $listener, $data) {
    $dom = new \DOMDocument();
    $dateTime = Carbon::now();
    $client = ApiManager::getClient();

    $data = [
      'tokenAuth' => ApiManager::generateAuthToken($dateTime->format('YmdHis'), $dateTime->format('YmdHis'), ApiManager::SERVICE_TRX_INQUIRY),
      'txnData' => ApiManager::generateAccountBalanceData(),
      'serviceCode' => ApiManager::SERVICE_ACCOUNT_BALANCE,
      'corpID' => ApiManager::getCorpId(),
      'requestID' => $dateTime->format('YmdHis'),
      'txnRequestDateTime' => $dateTime->format('YmdHis')
    ];

    $response = $client->call(ApiManager::$SOAP, [$data]);
    $dom->loadXML($response['txnData']);
    $data = [];

    foreach ($dom->getElementsByTagName('balance') as $node) {
      foreach ($node->childNodes as $childNode) {
        if($childNode->nodeName == '#text') continue;
        $temp[$childNode->nodeName] = $childNode->nodeValue;
      }

      $data[] = $temp;

    }

    return $listener->response(200, $data);
  }
}
