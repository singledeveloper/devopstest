<?php

namespace Odeo\Domains\Vendor\Cimb\Helper;


class ApiManager {

  static $SOAP = 'HostCustomer';
  const SERVICE_ACCOUNT_BALANCE = 'ACCOUNT_BALANCE';
  const SERVICE_TRX_INQUIRY = 'TRANSACTION_INQUIRY';
  const SERVICE_VA_HISTORY = 'DOWNLOAD_VA_HISTORY';
  const SERVICE_DC_NOTIFICATION = 'DEBIT_CREDIT_NOTIFICATION';
  const SERVICE_TRANSFER_FUND = 'ACCOUNT_TRANSFER';
  const SERVICE_TRANSFER_PHONE = 'REKENING_PONSEL';

  static $CONFIG = [
    'production' => [
      'url' => 'https://directchannel.cimbniaga.co.id:8004/PrismaGateway/services/HostCustomer',
      'debit_account_number' => '800146081300',
      'cop_id' => '',
      'secret_word' => ''
    ],
    'staging' => [
      'url' => 'https://pguat.cimbniaga.co.id:8005/PrismaGateway/services/HostCustomer',
      'debit_account_number' => '800099839000',
      'cop_id' => 'IDSALAM',
      'secret_word' => 'test1111'
    ]
  ];

  public static function init() {
    self::$CONFIG['production']['cop_id'] = env('API_CIMB_COP_ID');
    self::$CONFIG['production']['secret_word'] = env('API_CIMB_SECRET_WORD');
  }

  public static function getConfig($name) {

    $env = app()->environment();

    if (is_array($name)) {
      return array_map(function ($n) use ($env) {
        return self::$CONFIG[$env][$n];
      }, $name);
    }
    return self::$CONFIG[$env][$name];
  }

  static function generateAuthToken($dateTime, $reqId, $serviceCode) {

    list($copId, $secretWord) = self::getConfig(['cop_id', 'secret_word']);

    $words = join(':', [
      $copId,
      hash('sha256', $secretWord),
      $dateTime,
      $reqId,
      $serviceCode
    ]);

    return hash('sha256', $words);
  }

  static function getClient() {
    $url = self::getConfig('url');
    return new \nusoap_client($url,
      false, false, false,
      false, false, 60
    );
  }

  static function generateAccountBalanceData() {

    $debitAccountNumber = self::getConfig('debit_account_number');

    return '<balanceRequest>
                <balance>
                    <accountNo>' . $debitAccountNumber . '</accountNo>
                </balance>
            </balanceRequest>';
  }

  static function generateTransactionInquiryData($direction, $dateFrom, $dateTo) {

    $debitAccountNumber = self::getConfig('debit_account_number');

    return '<trxInqRequest>
              <account>
                <accountNo>' . $debitAccountNumber . '</accountNo>
                <fromDate>' . $dateFrom . '</fromDate>
                <toDate>' . $dateTo . '</toDate>
                <direction>' . $direction . '</direction>
              </account>
            </trxInqRequest>';
  }

  static function generateDebitCreditNotificationData() {

    $debitAccountNumber = self::getConfig('debit_account_number');

    return '<dcRequest><account><accountNo>' . $debitAccountNumber . '</accountNo></account></dcRequest>';
  }

  static function generateFundTransferXml($data) {

    list($copId) = self::getConfig(['cop_id']);

    return
      simplexml_load_string(
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://10.25.143.133" xmlns:java="java:prismagateway.service.HostCustomer">
          <soapenv:Header/>
          <soapenv:Body>
            <ns:HostCustomer>
              <ns:input>
                <java:tokenAuth>' . $data['tokenAuth'] . '</java:tokenAuth>
                <java:txnData>
                  <![CDATA[
                  <transferRequest>
                    <transfer>
                      <transferId>' . $data['requestID'] . '</transferId>
                      <txnDate>' . $data['txnDate'] . '</txnDate>
                      <debitAcctNo>' . $data['debitAcctNo'] . '</debitAcctNo>
                      <benAcctNo>' . $data['benAcctNo'] . '</benAcctNo>
                      <benName>' . $data['benName'] . '</benName>
                      <benBankName>CIMB Niaga</benBankName>
                      <benBankAddr1></benBankAddr1>
                      <benBankAddr2></benBankAddr2>
                      <benBankAddr3></benBankAddr3>
                      <benBankBranch></benBankBranch>
                      <benBankCode>022</benBankCode>
                      <benBankSWIFT></benBankSWIFT>
                      <currCd>IDR</currCd>
                      <amount>' . $data['amount'] . '</amount>
                      <memo>' . $data['memo'] . '</memo>
                    </transfer>
                  </transferRequest>
                  ]]>
                </java:txnData>
                <java:serviceCode>ACCOUNT_TRANSFER</java:serviceCode>
                <java:corpID>' . $copId . '</java:corpID>
                <java:requestID>' . $data['requestID'] . '</java:requestID>
                <java:txnRequestDateTime>' . $data['txnRequestDateTime'] . '</java:txnRequestDateTime>
              </ns:input>
            </ns:HostCustomer>
          </soapenv:Body>
        </soapenv:Envelope>'
      )->asXML();
  }

}
