<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/12/17
 * Time: 3:01 PM
 */

namespace Odeo\Domains\Vendor\Cimb\Jobs;


use Carbon\Carbon;
use Odeo\Domains\Vendor\Cimb\Helper\ApiManager;
use Odeo\Jobs\Job;

class TestCustomerTransfer extends Job{

  private $amount;
  public function __construct($amount) {
    parent::__construct();
    $this->amount = $amount;
  }

  public function handle() {
    $dateTime = Carbon::now();
    $client = ApiManager::getClient();

    $tokenAuth = ApiManager::generateAuthToken($dateTime->format('YmdHis'), $dateTime->format('YmdHis'), ApiManager::SERVICE_TRANSFER_FUND);
    $fundTransferXml = ApiManager::generateFundTransferXml([
      'tokenAuth' => $tokenAuth,
      'requestID' => $dateTime->format('YmdHis'),
      'txnRequestDateTime' => $dateTime->format('YmdHis'),
      'txnDate' => $dateTime->format('Ymd'),
      'debitAcctNo' => ApiManager::$AccountCustomerDev,
      'benAcctNo' => ApiManager::$AccountOdeoDev,
      'benName' => 'TEST',
      'amount' => $this->amount,
      'memo' => 'CUST TRANSFER'
    ]);

    $client->operation = ApiManager::$SOAP;
    $response = $client->send($fundTransferXml, ApiManager::getUrl());
  }
}
