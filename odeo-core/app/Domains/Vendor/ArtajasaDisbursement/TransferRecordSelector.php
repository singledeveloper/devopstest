<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;

class TransferRecordSelector implements SelectorListener {

  private $artajasaDisbursementRepo;

  public function __construct() {
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $result = [
      'artajasa_disbursement_id' => $item->id,
      'transfer_to' => $item->transfer_to,
      'transfer_to_name' => $item->transfer_to_name,
      'transfer_to_bank' => $item->transfer_to_bank,
      'purpose_desc' => $item->purpose_desc,
      'amount' => $item->amount,
      'disbursement_type' => $item->disbursement_type,
      'disbursement_reference_id' => $item->disbursement_reference_id,
      'status' => $item->status,
      'error_exception_message' => $item->error_exception_message,
      'error_exception_code' => $item->error_exception_code,
      'last_inquire_date' => $item->last_inquire_date ? $item->last_inquire_date->format('Y-m-d H:i:s') : '',
      'status_error_exception_message' => $item->status_error_exception_message,
      'status_error_exception_code' => $item->status_error_exception_code,
      'status_response_code' => $item->status_response_code,
      'status_response_description' => $item->status_response_description,
      'query_response_code' => $item->query_response_code,
      'query_response_description' => $item->query_response_description,
    ];

    if ($item->transferInquiry) {
      $result['inquiry_transaction_id'] = $item->transferInquiry->transaction_id;
      $result['inquiry_transaction_datetime'] = $item->transferInquiry->transaction_datetime;
      $result['inquiry_response_datetime'] = $item->transferInquiry->response_datetime;
      $result['inquiry_response_code'] = $item->transferInquiry->response_code;
      $result['inquiry_response_description'] = $item->transferInquiry->response_description;
    }

    if ($item->transfer) {
      $result['transaction_id'] = $item->transfer->transaction_id;
      $result['transaction_datetime'] = $item->transfer->transaction_datetime;
      $result['response_code'] = $item->transfer->response_code;
      $result['response_description'] = $item->transfer->response_description;
      $result['response_datetime'] = $item->transfer->response_datetime;
      $result['transfer_error_exception_message'] = $item->transfer->error_exception_message;
      $result['transfer_error_exception_code'] = $item->transfer->error_exception_code;
    }

    return $result;
  }

  public function get(PipelineListener $listener, $data) {
    $this->artajasaDisbursementRepo->normalizeFilters($data);
    $this->artajasaDisbursementRepo->setSimplePaginate(true);

    $result = [];

    foreach ($this->artajasaDisbursementRepo->get() as $disbursement) {
      $result[] = $this->_transforms($disbursement, $this->artajasaDisbursementRepo);
    }

    if (sizeof($result) > 0) {
      return $listener->response(200, array_merge(
        ["artajasa_disbursement_record" => $this->_extends($result, $this->artajasaDisbursementRepo)],
        $this->artajasaDisbursementRepo->getPagination()
      ));
    }
    return $listener->response(204, ["artajasa_disbursement_record" => []]);

  }

  public function getSuspectDisbursements(PipelineListener $listener, $data) {
    if ($vendorDisbursements = $this->artajasaDisbursementRepo->getSuspectDisbursements()) {
      return $listener->response(200, ['suspect_disbursement_record' => $vendorDisbursements]);
    }

    return $listener->response(204, ['suspect_disbursement_record' => []]);
  }

}
