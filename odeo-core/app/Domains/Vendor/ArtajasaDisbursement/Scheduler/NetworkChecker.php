<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Scheduler;

use Odeo\Domains\Vendor\ArtajasaDisbursement\Jobs\CheckNetwork;

class NetworkChecker {

  public function run() {
    $env = app()->environment();

    if ($env !== 'production') {
      return;
    }

    dispatch(new CheckNetwork());
  }

}
