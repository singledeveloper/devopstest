<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Jobs\InquireTransferStatus;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;

class TimeoutTransferStatusInquiryScheduler {

  const INQUIRE_MIN_SEC = 60 * 5;

  private $artajasaDisbursementRepo;

  public function __construct() {
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
  }

  public function run() {
    $date = Carbon::now()->subSeconds(self::INQUIRE_MIN_SEC);
    $disbursementList = $this->artajasaDisbursementRepo
      ->getTimeoutList([Disbursement::API_DISBURSEMENT, Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH], $date)
      ->pluck('id');

    foreach ($disbursementList as $id) {
      dispatch(new InquireTransferStatus([
        'id' => $id,
      ]));
    }
  }
}
