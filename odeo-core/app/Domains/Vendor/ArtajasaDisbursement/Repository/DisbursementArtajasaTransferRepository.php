<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Model\DisbursementArtajasaTransfer;

class DisbursementArtajasaTransferRepository extends Repository {

  private $lock = false;

  public function __construct(DisbursementArtajasaTransfer $model) {
    $this->model = $model;
  }

  public function lock() {
    $this->lock = true;
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }

  public function findLastByDate($date) {
    $model = $this->getCloneModel();
    
    if ($this->lock) $model = $this->model->lockForUpdate();

    return $model
      ->whereDate('transaction_datetime', '>=', $date)
      ->orderBy('transaction_id', 'desc')
      ->first();
  }

}