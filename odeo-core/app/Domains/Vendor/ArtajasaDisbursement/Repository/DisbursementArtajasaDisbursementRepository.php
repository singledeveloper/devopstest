<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Repository;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Model\DisbursementArtajasaDisbursement;

class DisbursementArtajasaDisbursementRepository extends Repository {

  private $lock = false;

  public function __construct(DisbursementArtajasaDisbursement $artajasaDisbursement) {
    $this->model = $artajasaDisbursement;
  }

  public function lock() {
    $this->lock = true;
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }

  public function findLastByDate($date) {
    $model = $this->getCloneModel();
    
    if ($this->lock) $model = $this->model->lockForUpdate();

    return $model
      ->whereDate('transaction_datetime', '>=', $date)
      ->orderBy('transaction_id', 'desc')
      ->first();
  }

  public function findLastByInquiryDate($date) {
    $model = $this->getCloneModel();
    
    if ($this->lock) $model = $this->model->lockForUpdate();

    return $model
      ->whereDate('inquiry_transaction_datetime', '>=', $date)
      ->orderBy('inquiry_transaction_id', 'desc')
      ->first();
  }

  public function get() {
    $filters = $this->getFilters();

    $latestStatusQuery = <<<SQL
select distinct on (disbursement_artajasa_disbursement_id) 
  disbursement_artajasa_disbursement_id as aj_disbursement_id, 
  error_exception_message as status_error_exception_message,
  error_exception_code as status_error_exception_code,
  response_code as status_response_code,
  response_description as status_response_description,
  query_response_code,
  query_response_description
from disbursement_artajasa_status_inquiries
order by disbursement_artajasa_disbursement_id, id desc
SQL;

    $query = $this->getCloneModel()
      ->with('transfer')
      ->with('transferInquiry')
      ->leftJoin(\DB::raw('(' . $latestStatusQuery . ') as statuses'), function($query) {
        $query->on('disbursement_artajasa_disbursements.id', '=', 'statuses.aj_disbursement_id');
      });

    if (isset($filters['search'])) {
      if (isset($filters['search']['artajasa_disbursement_id'])) {
        $query = $query->where('id', $filters['search']['artajasa_disbursement_id']);
      }
    }

    $sortBy = $filters['sort_by'] ?? 'id';
    return $this->getResult($query->orderBy($sortBy == 'artajasa_disbursement_id' ? 'id' : $sortBy, $filters['sort_type']));
  }


  public function getTimeoutList($types, $date) {
    return $this->getCloneModel()
      ->where('status', '=', ArtajasaDisbursement::TIMEOUT)
      ->whereIn('disbursement_type', $types)
      ->where(function($query) use ($date) {
        $query->whereNull('last_inquire_date')
          ->orWhere('last_inquire_date', '<', $date);
      })
      ->get();
  }

  public function getSuspectDisbursements() {
    return $this->getCloneModel()
      ->join('api_disbursements', 'api_disbursements.id', '=', 'disbursement_artajasa_disbursements.disbursement_reference_id')
      ->where('api_disbursements.status', ApiDisbursement::SUSPECT)
      ->select('disbursement_artajasa_disbursements.*')
      ->get();
  }

}