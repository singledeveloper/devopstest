<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Helper;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class ArtajasaStanGenerator
{
  private $redis;

  public function __construct()
  {
    $this->redis = Redis::connection();
  }

  /**
   * Transaction should be managed by method caller
   * @param Carbon $date
   * @return int
   */
  public function generate(Carbon $date)
  {
    $stan = $this->redis->hincrby('odeo_core:aj_disbursement_stan', 'id', 1);
    $transDate = $this->redis->hget('odeo_core:aj_disbursement_stan', 'date');

    if ($transDate != $date->toDateString()) {
      $stan = 1;
      $this->redis->hmset('odeo_core:aj_disbursement_stan', [
        'id' => 1,
        'date' => $date->toDateString()
      ]);
    }

    return $stan;
  }

}