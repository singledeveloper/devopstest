<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Helper;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\Mock\MockApiManager;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaStatusInquiryRepository;

class StatusInquiry
{

  private $artajasaStatusInquiryRepo, $redis, $stanGenerator, $apiManager;

  public function __construct()
  {
    $this->stanGenerator = app()->make(ArtajasaStanGenerator::class);
    $this->artajasaStatusInquiryRepo = app()->make(DisbursementArtajasaStatusInquiryRepository::class);
    $this->redis = Redis::connection();

    if (app()->environment() == 'production') {
      $this->apiManager = app()->make(ApiManager::class);
    }
    else {
      $this->apiManager = app()->make(MockApiManager::class);
    }
  }

  public function exec($ajDisbursementId, $transactionId, $transactionDatetime)
  {
    $statusInquiry = $this->initStatusInquiry($ajDisbursementId, $transactionId, $transactionDatetime);
    $this->doStatusInquiry($statusInquiry);
    return $statusInquiry;
  }

  private function initStatusInquiry($ajDisbursementId, $queryTransactionId, $queryTransactionDatetime)
  {
    $statusInquiry = $this->artajasaStatusInquiryRepo->getNew();
    $statusInquiry->disbursement_artajasa_disbursement_id = $ajDisbursementId;
    $statusInquiry->transaction_datetime = Carbon::now();
    $statusInquiry->query_transaction_id = $queryTransactionId;
    $statusInquiry->query_transaction_datetime = $queryTransactionDatetime;

    DB::transaction(function () use ($statusInquiry) {
      $this->artajasaStatusInquiryRepo->lock();
      $statusInquiry->transaction_id = $this->stanGenerator->generate($statusInquiry->transaction_datetime);
      $this->artajasaStatusInquiryRepo->save($statusInquiry);
    });

    return $statusInquiry;
  }

  private function doStatusInquiry($statusInquiry)
  {
    try {
      $result = $this->apiManager->statusInquiry(
        $statusInquiry->transaction_id,
        $statusInquiry->transaction_datetime,
        $statusInquiry->query_transaction_id,
        $statusInquiry->query_transaction_datetime
      );

      $statusInquiry->response_code = $result['Response']['Code'];
      $statusInquiry->response_description = $result['Response']['Description'];

      if ($result['Response']['Code'] == '00') {
        $statusInquiry->status = ArtajasaDisbursement::SUCCESS;
        $statusInquiry->query_response_code = $result['TransferData']['Response']['Code'];
        $statusInquiry->query_response_description = $result['TransferData']['Response']['Description'];
      }
      else {
        $statusInquiry->status = ArtajasaDisbursement::FAIL;
      }
      
      $statusInquiry->response_datetime = Carbon::now();
      $this->artajasaStatusInquiryRepo->save($statusInquiry);

      return $statusInquiry->status == ArtajasaDisbursement::SUCCESS;
    } catch (Exception $e) {
      \Log::error($e);

      $statusInquiry->status = ArtajasaDisbursement::FAIL;
      $statusInquiry->error_exception_message = $e->getMessage();
      $statusInquiry->error_exception_code = $e->getCode();
      $this->artajasaStatusInquiryRepo->save($statusInquiry);

      return false;
    }
  }

}