<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Model;


use Odeo\Domains\Core\Entity;

class DisbursementArtajasaDisbursement extends Entity {

  protected $dates = ['last_inquire_date'];

  public function transfer() {
    return $this->belongsTo(DisbursementArtajasaTransfer::class, 'disbursement_artajasa_transfer_id');
  }

  public function transferInquiry() {
    return $this->belongsTo(DisbursementArtajasaTransferInquiry::class,
      'disbursement_artajasa_transfer_inquiry_id');
  }

  public function statusInquiries() {
    return $this->hasMany(DisbursementArtajasaStatusInquiry::class);
  }

}