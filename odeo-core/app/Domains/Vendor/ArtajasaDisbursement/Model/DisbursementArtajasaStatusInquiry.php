<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Model;


use Odeo\Domains\Core\Entity;

class DisbursementArtajasaStatusInquiry extends Entity {

  protected $dates = ['transaction_datetime', 'query_transaction_datetime'];

}