<?php

namespace Odeo\Domains\Vendor\Bri\Repository;

use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Bri\Model\DisbursementBriDisbursement;

class DisbursementBriDisbursementRepository extends Repository {

  private $lock = false;

  public function __construct(DisbursementBriDisbursement $briDisbursement) {
    $this->model = $briDisbursement;
  }

  public function lock() {
    $this->lock = true;
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }

  public function get() {
    $filters = $this->getFilters();

    $query = $this->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['bri_disbursement_id'])) {
        $query = $query->where('id', $filters['search']['bri_disbursement_id']);
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function getSuspectDisbursements() {
    return $this->getCloneModel()
      ->join('api_disbursements', 'disbursement_bri_disbursements.disbursement_reference_id', '=', 'api_disbursements.id')
      ->where('api_disbursements.status', ApiDisbursement::SUSPECT)
      ->select('disbursement_bri_disbursements.*')
      ->get();

  }

}