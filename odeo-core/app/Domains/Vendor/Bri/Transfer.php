<?php

namespace Odeo\Domains\Vendor\Bri;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BriDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Bri\Helper\ApiManager;
use Odeo\Domains\Vendor\Bri\Repository\DisbursementBriDisbursementRepository;

class Transfer {

  private $briDisbursementRepo, $apiManager, $redis;

  public function __construct() {
    $this->briDisbursementRepo = app()->make(DisbursementBriDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
    $this->redis = Redis::connection();
  }

  public function exec(PipelineListener $listener, $data) {

    if (!$this->redis->hsetnx('odeo_core:bri_disbursement_lock', 'id_' . $data['bri_disbursement_id'], 1)) {
      return $listener->response(400, 'duplicate transaction');
    }

    $referralNumber = $this->generateRefNumber();

    $beforeSend = Carbon::now();

    try {

      \DB::beginTransaction();

      $this->briDisbursementRepo->lock();

      $briDisbursement = $this->briDisbursementRepo->findById($data['bri_disbursement_id']);

      if ($briDisbursement->response_datetime) {
        throw new \Exception('duplicate transaction');
      }

      $briDisbursement->remark = $data['remark'];
      $briDisbursement->transfer_from = $data['transfer_from'];
      $briDisbursement->transfer_to = $data['transfer_to'];
      $briDisbursement->transfer_datetime = $beforeSend->toDateTimeString();
      $briDisbursement->referral_number = $referralNumber;

      $this->briDisbursementRepo->save($briDisbursement);
      \DB::commit();

    } catch (\Exception $exception) {
      \DB::rollback();
      return $listener->response(400);
    }

    $response = null;

    try {

      $response = $this->apiManager->transfer(
        $briDisbursement->transfer_from,
        $briDisbursement->transfer_to,
        $briDisbursement->amount,
        $briDisbursement->remark,
        $briDisbursement->referral_number
      );
      if ($response->responseCode == "0200") {
        $briDisbursement->status = BriDisbursement::SUCCESS;
      } else {
        throw new \Exception(json_encode($response));
      }

    } catch (\Exception $exception) {
      $briDisbursement->status = BriDisbursement::FAIL;
      $briDisbursement->error_description = $response->errorDescription ?? null;
    } finally {
      if ($response) {
        $briDisbursement->response_code = $response->responseCode ?? null;
        $briDisbursement->response_description = $response->responseDescription ?? null;
        $briDisbursement->response_log = \json_encode($response);
      }
    }

    $briDisbursement->response_datetime = Carbon::now()->toDateTimeString();

    $this->briDisbursementRepo->save($briDisbursement);

    if ($briDisbursement->status != BriDisbursement::SUCCESS) {
      return $listener->response(400, [
        'response_code' => $briDisbursement->response_code,
        'error_desc' => $response->errorDescription
      ]);
    }

    return $listener->response(200);
  }

  private function generateRefNumber() {
    $referralNumber = str_replace('.', '', microtime(true)) . mt_rand(10000, 99999);
    return $referralNumber;
  }

}
