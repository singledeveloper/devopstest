<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 26/03/19
 * Time: 15.11
 */

namespace Odeo\Domains\Vendor\Mandiri;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\MandiriDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Mandiri\Helper\ApiManager;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;

class Transfer {

  /**
   * @var DisbursementMandiriDisbursementRepository
   */
  private $mandiriDisbursementRepo;

  /**
   * @var ApiManager
   */
  private $apiManager;

  /**
   * @var Redis
   */
  private $redis;

  private function init() {
    $this->mandiriDisbursementRepo = app()->make(DisbursementMandiriDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
    $this->redis = Redis::connection();
  }

  public function exec(PipelineListener $listener, $data) {
    $this->init();
    if (!$this->redis->hsetnx('odeo_core:mandiri_disbursement_lock', 'id_' . $data['mandiri_disbursement_id'], 1)) {
      return $listener->response(400, 'duplicate transaction');
    }

    \DB::beginTransaction();

    $disbursement = $this->mandiriDisbursementRepo->findById($data['mandiri_disbursement_id']);

    try {
      if ($disbursement->status_code != null || $disbursement->status_code != '') {
        throw new \Exception('duplicate transaction');
      }

      $this->mandiriDisbursementRepo->lock();

      $now = Carbon::now();

      if (isset($data['amount']) && $disbursement->amount == 0) {
        $disbursement->amount = $data['amount'];
      }

      $disbursement->to_account_number = $data['account_number'];
      $disbursement->to_account_name = $data['account_name'];
      $disbursement->cust_reff_no = $data['mandiri_disbursement_id'];
      $disbursement->remark = $data['remark'];
      $disbursement->request_time = $now;
      $disbursement->transaction_key = $data['transaction_key'];
      $disbursement->bank_code = $data['bank_code'] ?? '';

      list($response, $isVerified) = $this->apiManager->createPaymentTransfer(
        $disbursement->to_account_number,
        $disbursement->amount,
        $disbursement->remark,
        $disbursement->id,
        $disbursement->to_account_name,
        $disbursement->bank_code
      );

      $this->mandiriDisbursementRepo->save($disbursement);

      \DB::commit();

    } catch (\Exception $e) {
      \DB::rollback();
      return $listener->response(400);
    }

    try {
      $header = $response['responseHeader'];
      $resBody = $response['body'];

      $disbursement->response_code = $header['responseCode'];
      $disbursement->response_message = $header['responseMessage'];
      $disbursement->response_timestamp = $header['responseTimestamp'];
      $disbursement->response_error_message = $header['errorMessage'];
      $disbursement->response_error_number = $header['errorNumber'];
      $disbursement->response_remmittance_no = $resBody['remmittanceNumber'];
      $disbursement->is_sign_verified = $isVerified;

      $this->mandiriDisbursementRepo->save($disbursement);

      switch ($disbursement->response_code) {
        case '3': // no response
        case '8': // in progress
          $disbursement->status = MandiriDisbursement::SUSPECT;
          break;
        case '1':
          $disbursement->status = $isVerified ? MandiriDisbursement::SUCCESS : MandiriDisbursement::SUSPECT;
          break;
        default:
          $disbursement->status = MandiriDisbursement::FAIL;
      }

    } catch (\Exception $e) {
      $disbursement->status = MandiriDisbursement::FAIL;
      $disbursement->ex_error_message = $e->getMessage();
    }

    $this->mandiriDisbursementRepo->save($disbursement);

    $this->redis->hdel('odeo_core:mandiri_disbursement_lock', 'id_' . $data['mandiri_disbursement_id']);

    if ($disbursement->status != MandiriDisbursement::SUCCESS) {
      return $listener->response(400, [
        'response_code' => $disbursement->response_code,
      ]);
    }

    return $listener->response(200);
  }

}