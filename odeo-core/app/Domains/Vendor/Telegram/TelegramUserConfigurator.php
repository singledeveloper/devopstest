<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 6/3/17
 * Time: 2:32 PM
 */

namespace Odeo\Domains\Vendor\Telegram;


use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Core\PipelineListener;

class TelegramUserConfigurator {

  private $telegramUsers;

  public function __construct() {
    $this->telegramUsers = app()->make(\Odeo\Domains\Vendor\Telegram\Repository\TelegramUserRepository::class);
  }

  public function getConfig(PipelineListener $listener, $data) {

    if (!($telegramUser = $this->telegramUsers->findByUserId($data['auth']['user_id']))) {
      return $listener->response(204);
    }

    return $listener->response(200, [
      'has_set_transaction_pin' => $telegramUser->pin ? true : false
    ]);

  }


  public function createTransactionPin(PipelineListener $listener, $data) {

    if (!($telegramUser = $this->telegramUsers->findByUserId($data['auth']['user_id']))) {
      $telegramUser = $this->telegramUsers->getNew();
      $telegramUser->user_id = $data['auth']['user_id'];
    }

    list($isValid, $message) = checkPinSecure($data['transaction_pin']);
    if (!$isValid) return $listener->response(400, $message);

    $telegramUser->pin = Hash::make($data['transaction_pin']);

    $this->telegramUsers->save($telegramUser);

    return $listener->response(200);
  }


  public function changeTransactionPin(PipelineListener $listener, $data) {

    if (!($telegramUser = $this->telegramUsers->findByUserId($data['auth']['user_id'])) || strlen($telegramUser->pin) === 0) {
      return $listener->response(400);
    }

    list($isValid, $message) = checkPinSecure($data['new_transaction_pin']);
    if (!$isValid) return $listener->response(400, $message);

    $telegramUser->pin = Hash::make($data['new_transaction_pin']);
    $telegramUser->pin_try_counts = 0;

    $this->telegramUsers->save($telegramUser);

    return $listener->response(200);
  }
}
