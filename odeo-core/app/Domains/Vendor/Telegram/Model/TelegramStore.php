<?php

namespace Odeo\Domains\Vendor\Telegram\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Subscription\Model\Store;

class TelegramStore extends Entity
{
  public function store() {
    return $this->belongsTo(Store::class);
  }
}
