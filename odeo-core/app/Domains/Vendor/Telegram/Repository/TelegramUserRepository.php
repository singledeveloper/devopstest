<?php

namespace Odeo\Domains\Vendor\Telegram\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Telegram\Model\TelegramUser;

class TelegramUserRepository extends Repository {

  public function __construct(TelegramUser $telegramUser) {
    $this->model = $telegramUser;
  }

  public function findByChatId($id) {
    return $this->model->where('chat_id', $id)->first();
  }

  public function findByPhoneNumber($number) {
    return $this->model->where('phone_number', $number)->first();
  }

  public function findByUserId($userId) {
    return $this->model->where('user_id', $userId)->first();
  }

  public function getByUserId($userIds) {
    return $this->model->whereIn('user_id', $userIds)->get();
  }

}
