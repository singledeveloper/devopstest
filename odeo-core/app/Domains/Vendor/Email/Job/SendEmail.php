<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/8/17
 * Time: 4:36 PM
 */

namespace Odeo\Domains\Vendor\Email\Job;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendEmail extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    Mail::send($this->data['filename'], $this->data['data'], function ($m) {
      $m->subject($this->data['subject'])
        ->from($this->data['from']['email'], $this->data['from']['name']);

      foreach ($this->data['to'] as $to) {
        $m->to($to['email'], $to['name']);
      }
    });
  }

}
