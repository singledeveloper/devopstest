<?php

namespace Odeo\Domains\Vendor\Jabber\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

class SendErrorReport extends Job {

  use InteractsWithQueue, SerializesModels;

  protected $errorMessage, $ignoreCreateLog;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($errorMessage, $ignoreCreateLog = false) {
    parent::__construct();
    $this->errorMessage = $errorMessage;
    $this->ignoreCreateLog = $ignoreCreateLog;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle() {
    $jabberManager = app()->make(\Odeo\Domains\Vendor\Jabber\JabberManager::class);
    if ($jabberManager->createLog('jabber_alert', InlineJabber::TYPE_ERROR_REPORT, $this->errorMessage) || $this->ignoreCreateLog) {
      Mail::send('emails.jabber_alert', [
        'data' => ['error_message' => $this->errorMessage]
      ], function ($m) {

        $m->from('noreply@odeo.co.id', 'odeo');
        $m->to('paulus.cuang@gmail.com');
        $m->cc('channel@odeo.co.id')->subject('Jabber Error - ' . time());

      });
    }
  }

}
