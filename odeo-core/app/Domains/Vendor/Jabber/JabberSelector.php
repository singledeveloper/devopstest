<?php

namespace Odeo\Domains\Vendor\Jabber;

use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class JabberSelector implements SelectorListener {

  private $jabberUser, $user;

  public function __construct() {
    $this->jabberUser = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberUserRepository::class);
    $this->user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }

  public function getDetail(PipelineListener $listener, $data) {
    if ($jabberUser = $this->jabberUser->findByUserId(getUserId())) {
      if ($jabberUser->is_active) {
        return $listener->response(200, [
          'jabber_email' => $jabberUser->email,
          'is_active' => true
        ]);
      }
      return $listener->response(204);
    }
    return $listener->response(204);
  }

  public function getCommandExamples(PipelineListener $listener) {
    return $listener->response(200, InlineJabber::getCommandDescriptions());
  }

  public function getReplyExamples(PipelineListener $listener) {
    return $listener->response(200, InlineJabber::getReplyExamples());
  }

  public function getList(PipelineListener $listener, $data) {
    return $listener->response(200, [[
      'email' => 'odeo@jabber.id'
    ], [
      'email' => 'odeo@jabbr.id'
    ]]);
  }

}
