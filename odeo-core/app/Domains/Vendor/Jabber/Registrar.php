<?php

namespace Odeo\Domains\Vendor\Jabber;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Core\PipelineListener;

class Registrar {

  private $jabberUser, $user;

  public function __construct() {
    $this->jabberUser = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberUserRepository::class);
    $this->user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function register(PipelineListener $listener, $data) {

    $jabberUser = $this->jabberUser->findByUserId(getUserId());

    if ($jabberUser && $jabberUser->is_active) {
      return $listener->response(400, 'You have registered this account to Jabber.');
    }

    $user = $this->user->findById(getUserId());

    if ($user->telephone != null) {

      list($isValid, $message) = checkPinSecure($data['transaction_pin']);
      if (!$isValid) return $listener->response(400, $message);

      $client = new \GuzzleHttp\Client();

      try {
        $response = $client->request('POST', InlineJabber::getServer() . '/register', [
          'json' => [
            'user' => revertTelephone($user->telephone),
            'host' => InlineJabber::getHost(),
            'password' => $data['password']
          ]
        ]);

        if ($response->getStatusCode() != 200) {
          return $listener->response(400, $response->getBody()->getContents());
        }

        if (!$jabberUser) $jabberUser = $this->jabberUser->getNew();
        $jabberUser->user_id = getUserId();
        $jabberUser->email = revertTelephone($user->telephone) . '@' . InlineJabber::getHost();
        $jabberUser->pin = Hash::make($data['transaction_pin']);
        $jabberUser->is_active = true;
        $this->jabberUser->save($jabberUser);

        return $listener->response(200, [
          'jabber_email' => $jabberUser->email
        ]);
      } catch (\GuzzleHttp\Exception\ClientException $e) {
        return $listener->response(400, $e->getResponse()->getBody()->getContents());
      } catch (\GuzzleHttp\Exception\ServerException $e) {
        return $listener->response(400, 'Gagal. Silakan coba lagi.');
      }
    }

    return $listener->response(400, 'Anda harus mendaftarkan nomor telepon Anda terlebih dahulu.');
  }

  public function remove(PipelineListener $listener, $data) {

    $jabberUser = $this->jabberUser->findByUserId(getUserId());

    if (!$jabberUser || ($jabberUser && !$jabberUser->is_active)) {
      return $listener->response(400, 'You haven\'t registered this account to Jabber yet.');
    }

    if (strpos($jabberUser->email, InlineJabber::getHost()) !== false) {

      $client = new \GuzzleHttp\Client();

      try {

        list($userName, $host) = explode('@', $jabberUser->email);
        $client->request('POST', InlineJabber::getServer() . '/unregister', [
          'json' => [
            'user' => $userName,
            'host' => $host
          ]
        ]);
      } catch (\GuzzleHttp\Exception\ClientException $e) {
        return $listener->response(400, $e->getResponse()->getBody()->getContents());
      } catch (\GuzzleHttp\Exception\ServerException $e) {
        return $listener->response(400, 'Gagal. Silakan coba lagi.');
      }
    }

    $jabberUser->is_active = false;
    $this->jabberUser->save($jabberUser);

    return $listener->response(200);
  }

  public function changeTransactionPin(PipelineListener $listener, $data) {

    $jabberUser = $this->jabberUser->findByUserId(getUserId());

    if (!$jabberUser || ($jabberUser && !$jabberUser->is_active)) {
      return $listener->response(400, 'You haven\'t registered this account to Jabber yet.');
    }

    list($isValid, $message) = checkPinSecure($data['new_transaction_pin']);
    if (!$isValid) return $listener->response(400, $message);

    $jabberUser->pin = Hash::make($data['new_transaction_pin']);
    $jabberUser->pin_try_counts = 0;
    $this->jabberUser->save($jabberUser);

    return $listener->response(200);
  }

  public function changePassword(PipelineListener $listener, $data) {

    $jabberUser = $this->jabberUser->findByUserId(getUserId());

    if (!$jabberUser || ($jabberUser && !$jabberUser->is_active)) {
      return $listener->response(400, 'You haven\'t registered this account to Jabber yet.');
    }
    if (strpos($jabberUser->email, InlineJabber::getHost()) !== false) {

      $client = new \GuzzleHttp\Client();

      try {
        list($userName, $host) = explode('@', $jabberUser->email);
        $client->request('POST', InlineJabber::getServer() . '/change_password', [
          'json' => [
            'user' => $userName,
            'host' => $host,
            'newpass' => $data['new_password']
          ]
        ]);
      } catch (\GuzzleHttp\Exception\ClientException $e) {
        return $listener->response(400, $e->getResponse()->getBody()->getContents());
      } catch (\GuzzleHttp\Exception\ServerException $e) {
        return $listener->response(400, 'Gagal. Silakan coba lagi.');
      }
      return $listener->response(200);
    }
    return $listener->response(400, 'We don\'t have access to change password of this account. Please contact your respective Jabber provider.');
  }

}
