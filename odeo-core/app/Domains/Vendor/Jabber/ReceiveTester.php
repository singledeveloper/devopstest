<?php

namespace Odeo\Domains\Vendor\Jabber;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class ReceiveTester {

  const EMAILS = [
    'voltron1@jabber-im.com'
  ];

  public function notify($message) {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute(['jabber_message' => $message]);
  }

  public function check(PipelineListener $listener, $data) {
    return $listener->response(200);
  }

}
