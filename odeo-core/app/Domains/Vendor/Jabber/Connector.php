<?php

namespace Odeo\Domains\Vendor\Jabber;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Vendor\Jabber\Jobs\BridgeNotify;
use Odeo\Domains\Vendor\Jabber\Jobs\BridgeSupplyResponse;

class Connector extends JabberManager {

  private $client, $redis;

  public function __construct() {
    parent::__construct();
    $this->redis = Redis::connection();
  }

  public function run() {
    if (!isProduction()) {
      if (posix_getpwuid(posix_geteuid())['name'] != 'laradock')
        echo "User denied, restart ignored.\n";
      $sockDir = '/var/tmp/.jaxl';

      if ($jabberPid = $this->redis->hget('odeo_core:jabber', 'current_pid')) {
        try {posix_kill($jabberPid, SIGINT);}
        catch(\Exception $e) {}
      }
      $this->redis->hset('odeo_core:jabber', 'current_pid', getmypid());
    }
    else {
      $sockDir = env('SOCKDIR');
      $this->clearPreviousSockets($sockDir);
    }

    foreach ($this->jabberStores->getAllActive() as $jabberStore) {

      if (isProduction()) $password = Crypt::decrypt($jabberStore->password);
      else {
        try {$password = Crypt::decrypt($jabberStore->password);}
        catch (\Exception $e) {$password = $jabberStore->password;}
      }
      $this->client = $this->setClient($jabberStore->id, $jabberStore->email, $password, $sockDir);

      $this->client->add_cb('on_auth_success', function ($client) use ($jabberStore) {
        $jabberStore->socket_path = $client->get_sock_file_path();
        $this->jabberStores->save($jabberStore);
        $client->set_status($jabberStore->status_message, null);
      });

      $this->client->add_cb('on_auth_failure', function($client, $reason) use ($jabberStore) {
        //clog('jabber_jaxl', "JABBER FAILURE ID " . $jabberStore->id . '. Reason: ' . $reason);
        $jabberStore->socket_path = null;
        $this->jabberStores->save($jabberStore);
        $client->send_end_stream();
      });

      $this->client->add_cb('on_chat_message', function ($client, $msg) use ($jabberStore) {
        try {
          list($from, $hash) = explode('/', $msg->from);
        } catch (\Exception $e) {
          //clog('jabber_jaxl', 'Unidentified message from: ' . $msg->from . ', body: ' . $msg->body);
        }

        if (isset($from)) {
          try {
            if ($jabberUser = $this->jabberUser->findByEmail($from, $jabberStore->store_id)) {
              if ($jabberUser->account_type == InlineJabber::ACCOUNT_TYPE_BILLER) {
                dispatch(new BridgeSupplyResponse([
                  'jabber_from' => $from,
                  'jabber_message' => $msg->body,
                  'jabber_store_id' => $jabberStore->store_id != null ? $jabberStore->store_id : ''
                ]));
                return;
              } else if ($jabberUser->user_id == null) {
                $replies[] = 'GAGAL Akun Jabber Anda belum terdaftar di ODEO. ' .
                  'Hubungi admin terkait untuk melakukan registrasi.';
              } else {
                $msgs = explode("\n", $msg->body);
                $replies = [];

                foreach ($msgs as $item) {
                  if (!$temp = $this->format(trim($item))) {
                    $replies[] = 'GAGAL Format salah. Silakan coba lagi. Ketik hi/help';
                  } else {
                    list ($isValid, $auth) = $this->auth($jabberUser);
                    if ($isValid) {
                      if (Inline::commandHistory($this->getCommand())) {
                        if (!Inline::commandNoAuth($this->getCommand())) $temp[count($temp) - 1] = 'XXXXXX';
                      }
                      $parser = InlineJabber::getParserPath($this->getCommand());
                      if ($parser == '') $replies[] = 'GAGAL Format salah. Silakan coba lagi. Ketik hi/help';
                      else {
                        $pipeline = new Pipeline();
                        $pipeline->add(new Task($parser, 'parse'));
                        $pipeline->execute([
                          'store_id' => $jabberUser->owner_store_id,
                          'auth' => [
                            'user_id' => $jabberUser->user_id,
                            'type' => UserType::SELLER,
                            'platform_id' => Platform::JABBER
                          ],
                          'jabber_user_id' => $jabberUser->id,
                          'jabber_store_id' => $jabberStore->id,
                          'jabber_message' => implode('.', $temp),
                          'jabber_from' => $from
                        ]);
                        if ($pipeline->getResponseMessage(InlineJabber::RES_PARAM) != '')
                          $replies[] = htmlspecialchars(strip_tags($pipeline->getResponseMessage(InlineJabber::RES_PARAM)));
                      }
                    } else $replies[] = $auth;
                  }
                }
              }

              if (count($replies) > 0) {
                $client->send_chat_msg($from, implode("\n", $replies));
              }
            }
            else {
              $path = InlineJabber::getNotifyPath($from);
              if ($path != '') dispatch(new BridgeNotify($from, $msg->body));
              else if (strpos($msg->body, 'ping: ') === false) {
                $this->createLog('jabber_jaxl', InlineJabber::TYPE_UNMATCH_ROUTE, 'from: ' . $msg->from . ', body: ' . $msg->body);
              }
            }
          } catch (\Exception $e) {
            $err = '';
            if (!isProduction()) $err = ' Error: ' . $e->getMessage();
            $client->send_chat_msg($from, 'Koneksi dalam gangguan. Silakan ulangi pesan Anda.' . $err);
          }
        }

      });

      $this->client->add_cb('on_disconnect', function () use ($jabberStore) {
        //clog('jabber_jaxl', "JABBER DISCONNECT: " . $jabberStore->email);
      });

      $this->client->manage_subscribe = 'mutual';

      $this->client->connect($this->client->get_socket_path());
      $this->client->start_stream();
      $this->client->enable_unix_sock();

    }

    try {
      \JAXLLoop::run();
    }
    catch (\Exception $e) {
      //clog('jabber_jaxl', $e->getMessage());
      $this->client->ev->emit('on_disconnect');
    }
  }

  private function clearPreviousSockets($dir) {
    if (ends_with($dir, '/')) {
      $dir = substr($dir, 0, strlen($dir) - 1);
    }

    $resp = shell_exec("rm -rf $dir/* && mkdir -p $dir");
    if (!empty($resp)) {
      //clog('jabber_jaxl', "Clear socket output: $resp");
    }
  }

}
