<?php

namespace Odeo\Domains\Vendor\Bni;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BniDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Bni\Helper\ApiManager;
use Odeo\Domains\Vendor\Bni\Repository\DisbursementBniDisbursementRepository;

class Transfer {

  private $bniDisbursements, $apiManager, $redis;

  public function __construct() {
    $this->bniDisbursements = app()->make(DisbursementBniDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
    $this->redis = Redis::connection();
  }

  public function exec(PipelineListener $listener, $data) {
    if (!$this->redis->hsetnx('odeo_core:bni_disbursement_lock', 'id_' . $data['bni_disbursement_id'], 1)) {
      return $listener->response(400, 'duplicate transaction');
    }

    list($customerRef, $valueDate) = $this->apiManager->generateCustomerRefAndValueDate();

    $beforeSend = Carbon::now();

    try {

      \DB::beginTransaction();

      $this->bniDisbursements->lock();

      $bniDisbursement = $this->bniDisbursements->findById($data['bni_disbursement_id']);

      if ($bniDisbursement->response_datetime) {
        throw new \Exception('duplicate transaction');
      }

      if ($bniDisbursement->amount == 0 && isset($data['amount'])) {
        $bniDisbursement->amount = $data['amount'];
      }

      $bniDisbursement->remark = $data['remark'];
      $bniDisbursement->transfer_from = $data['transfer_from'];
      $bniDisbursement->transfer_to = $data['transfer_to'];
      $bniDisbursement->transfer_datetime = $beforeSend->toDateTimeString();
      $bniDisbursement->customer_reference = $customerRef;

      $this->bniDisbursements->save($bniDisbursement);
      \DB::commit();

    } catch (\Exception $exception) {
      \DB::rollback();
      return $listener->response(400);
    }

    $response = null;

    try {
      $response = $this->apiManager->doPayment(
        $bniDisbursement->transfer_from,
        $bniDisbursement->transfer_to,
        $bniDisbursement->amount,
        $bniDisbursement->remark,
        $customerRef,
        $valueDate
      );

      if ($response->responseCode == "0001") {
        if ($customerRef != $response->customerReference) {
          throw new \Exception(
            'request customerRef not same with response: ' .
            json_encode($response)
          );
        }

        $bniDisbursement->status = BniDisbursement::SUCCESS;
        $bniDisbursement->currency = $response->valueCurrency;
        $bniDisbursement->bank_reference = $response->bankReference;
        $bniDisbursement->customer_reference = $response->customerReference;
      } else {
        throw new \Exception(json_encode($response));
      }

    } catch (\Exception $exception) {
      $bniDisbursement->status = BniDisbursement::FAIL;
      $bniDisbursement->response_log = $exception->getMessage();

      if ($response) {
        $bniDisbursement->error_message = $response->responseMessage;
      }
    } finally {
      if ($response) {
        $bniDisbursement->response_code = $response->responseCode;
        $bniDisbursement->response_message = $response->responseMessage;
      }
    }

    $bniDisbursement->response_datetime = Carbon::now()->toDateTimeString();

    $this->bniDisbursements->save($bniDisbursement);

    if ($bniDisbursement->status != BniDisbursement::SUCCESS) {
      return $listener->response(400, [
        'response_code' => $bniDisbursement->response_code
      ]);
    }

    return $listener->response(200);
  }

}
