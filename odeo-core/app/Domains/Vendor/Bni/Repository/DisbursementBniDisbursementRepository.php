<?php

namespace Odeo\Domains\Vendor\Bni\Repository;

use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Bni\Model\DisbursementBniDisbursement;

class DisbursementBniDisbursementRepository extends Repository {

  private $lock = false;

  public function __construct(DisbursementBniDisbursement $bniDisbursement) {
    $this->model = $bniDisbursement;
  }

  public function lock() {
    $this->lock = true;
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }

  public function get() {
    $filters = $this->getFilters();

    $query = $this->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['bni_disbursement_id'])) {
        $query = $query->where('id', $filters['search']['bni_disbursement_id']);
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function getSuspectDisbursements() {
    return $this->model
      ->join('api_disbursements', 'api_disbursements.id', '=','disbursement_bni_disbursements.disbursement_reference_id')
      ->where('api_disbursements.status', ApiDisbursement::SUSPECT)
      ->select('disbursement_bni_disbursements.*')
      ->get();

  }

}