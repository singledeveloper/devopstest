<?php

namespace Odeo\Domains\Vendor\Bni;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Vendor\Bni\Repository\DisbursementBniDisbursementRepository;

class TransferRecordSelector implements SelectorListener {

  private $bniDisbursementRepo;

  public function __construct() {
    $this->bniDisbursementRepo = app()->make(DisbursementBniDisbursementRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {

    $response['bni_disbursement_id'] = $item->id;
    $response['transfer_from'] = $item->transfer_from;
    $response['transfer_to'] = $item->transfer_to;
    $response['remark'] = $item->remark;
    $response['transfer_datetime'] = $item->transfer_datetime;
    $response['response_datetime'] = $item->response_datetime;
    $response['amount'] = $item->amount;
    $response['currency'] = $item->currency;
    $response['bank_reference'] = $item->bank_reference;
    $response['customer_reference'] = $item->customer_reference;
    $response['disbursement_type'] = $item->disbursement_type;
    $response['disbursement_reference_id'] = $item->disbursement_reference_id;
    $response['status'] = $item->status;
    $response['response_code'] = $item->response_code;
    $response['response_message'] = $item->response_message;
    $response['error_message'] = $item->error_message;

    return $response;
  }

  public function get(PipelineListener $listener, $data) {

    $this->bniDisbursementRepo->normalizeFilters($data);
    $this->bniDisbursementRepo->setSimplePaginate(true);

    $bniDisbursementRecords = [];

    foreach ($this->bniDisbursementRepo->get() as $bniDisbursement) {
      $bniDisbursementRecords[] = $this->_transforms($bniDisbursement, $this->bniDisbursementRepo);
    }

    if (sizeof($bniDisbursementRecords) > 0) {
      return $listener->response(200, array_merge(
        ["bni_disbursement_record" => $this->_extends($bniDisbursementRecords, $this->bniDisbursementRepo)],
        $this->bniDisbursementRepo->getPagination()
      ));
    }
    return $listener->response(204, ["bni_disbursement_record" => []]);

  }

  public function getSuspectDisbursements(PipelineListener $listener, $data) {
    if ($vendorDisbursements = $this->bniDisbursementRepo->getSuspectDisbursements()) {
      return $listener->response(200, ['suspect_disbursement_record' => $vendorDisbursements]);
    }

    return $listener->response(204, ['suspect_disbursement_record' => []]);
  }

}
