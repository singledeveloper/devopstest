<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/2/17
 * Time: 10:32 PM
 */

namespace Odeo\Domains\Vendor\Flip\Helper;


use GuzzleHttp\Client;

class FlipApi {

  public static $BASE_URL, $CREATE_DISBURSEMENT_URL,
    $GET_BALANCE_URL, $CHECK_IS_OPERATIONAL_URL,
    $SECRET, $GET_DISBURSEMENT, $TOKEN;

  private $client;

  public function __construct() {
    $this->client = new Client([
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Authorization' => 'Basic ' . base64_encode(self::$SECRET . ':'),
      ],
      'timeout' => 60,
      'http_errors' => false
    ]);
  }

  public static function init() {
    self::$BASE_URL = 'https://sandbox.flip.id/api/v1';
    self::$SECRET = 'JDJ5JDEzJGFkL3paQmJDOHd1bkZQTlE4eDNDTE9wYUxsaU15ZU1JdmRQNXJzS2V5RlNSLjhQZW1sZ0Uu';
    self::$TOKEN = '$2y$13$cIk0hN0ckNJWk5gjVRZKz.F0smJAsP1cOjG9guBsJUrmDMA2yZMci';

    if (app()->environment() == 'production') {
      self::$BASE_URL = 'https://big.flip.id/api/v1';
      self::$SECRET = 'JDJ5JDEzJHVQUFAvMm5XVzRhUUd5azljaHFTVWVFRWF0QjFFVjY2WXF0OGdFZVIvc29IVUdrbFV3U3NX';
      self::$TOKEN = '$2y$13$zdrB/qJMnBZYZbSNT67RSue8a7Kvyif5Cb/kVFylZyRioGvsBM.TK';
    }
    self::$CREATE_DISBURSEMENT_URL = self::$BASE_URL . '/disbursement';
    self::$CHECK_IS_OPERATIONAL_URL = self::$BASE_URL . '/general/is-operational';
    self::$GET_BALANCE_URL = self::$BASE_URL . '/general/balance';
    self::$GET_DISBURSEMENT = self::$BASE_URL . '/disbursement';


  }

  public function post($url, $data) {

    $response = $this->client->post($url, [
      'form_params' => $data,
    ]);

    return [
      'status' => $response->getStatusCode(),
      'data' => \json_decode($response->getBody()->getContents(), true)
    ];
  }

  public function getDisbursement($page = 1) {
    $response = $this->client->get(FlipApi::$GET_DISBURSEMENT, [
      'query' => [
        'pagination' => 20,
        'page' => $page,
      ],
    ]);

    return [
      'status' => $response->getStatusCode(),
      'data' => \json_decode($response->getBody()->getContents(), true)
    ];

  }

  public function getBalance() {
    $response = $this->client->get(FlipApi::$GET_BALANCE_URL);
    return [
      'status' => $response->getStatusCode(),
      'data' => \json_decode($response->getBody()->getContents(), true)
    ];
  }

  public function isOperational() {
    $response =  $this->client->get(FlipApi::$CHECK_IS_OPERATIONAL_URL);
    return [
      'status' => $response->getStatusCode(),
      'data' => \json_decode($response->getBody()->getContents(), true)
    ];
  }


}