<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/4/17
 * Time: 5:17 PM
 */

namespace Odeo\Domains\Vendor\Flip;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Flip\Helper\FlipApi;

class MiscRequester {

  private $api;

  public function __construct() {
    $this->api = app()->make(\Odeo\Domains\Vendor\Flip\Helper\FlipApi::class);
  }

  public function getBalance(PipelineListener $listener, $data) {

    $response = $this->api->getBalance();

    $result = $response->getBody()->getContent();

    $listener->response(200, [
      'balance' => $result['balance']
    ]);

  }

  public function isOperational(PipelineListener $listener, $data) {

    $response = $this->api->isOperational();

    $result = $response->getBody()->getContent();

    $listener->response(200, [
      'is_operational' => $result['operational']
    ]);
  }

}