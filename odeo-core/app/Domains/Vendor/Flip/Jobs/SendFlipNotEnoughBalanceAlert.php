<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Vendor\Flip\Jobs;


use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Vendor\Flip\MiscRequester;
use Odeo\Jobs\Job;

class SendFlipNotEnoughBalanceAlert extends Job  {

  use SerializesModels;

  private $data, $currencyHelper;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;

    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function handle() {

    if (app()->environment() != 'production') {
      return;
    }

    $pipeline = new Pipeline;

    $pipeline->add(new Task(MiscRequester::class, 'getBalance'));

    $pipeline->execute();

    $balance = $pipeline->data['balance'];

    Mail::send('emails.flip_not_enough_deposit_alert', [
      'data' => [
        'balance' => $this->currencyHelper->formatPrice($balance),
        'user_id' => $this->data['user_id'],
        'reference_id' => $this->data['reference_id'],
        'reference_type' => $this->data['reference_type'],
        'requested_amount' => $this->currencyHelper->formatPrice($this->data['requested_amount']),
      ]
    ], function ($m) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->to('pg@odeo.co.id')->subject('Flip not enough balance - ' . time());

    });


  }

}
