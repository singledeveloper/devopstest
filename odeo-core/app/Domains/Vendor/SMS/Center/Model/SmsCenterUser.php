<?php

namespace Odeo\Domains\Vendor\SMS\Center\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class SmsCenterUser extends Entity
{
  public function user() {
    return $this->belongsTo(User::class);
  }
}
