<?php

namespace Odeo\Domains\Vendor\SMS\Center\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\SMS\Center\Model\SmsCenter;

class SmsCenterRepository extends Repository {

  public function __construct(SmsCenter $smsCenter) {
    $this->model = $smsCenter;
  }

  public function findByActiveJabberAccount($jabberName) {
    return $this->model->where('jabber_account', $jabberName)
      ->where(function($query){
        $query->whereNull('last_error_reported_at')->orWhere('last_error_reported_at', '<', Carbon::now()->subMinutes(2)->toDateTimeString());
      })->where('last_pinged_at', '>', Carbon::now()->subMinutes(5)->toDateTimeString())->first();
  }

  public function findByJabberAccount($jabberName) {
    return $this->model->where('jabber_account', $jabberName)->first();
  }

  public function getPublished() {
    return $this->model->where('is_published', true)->get();
  }

  public function getActive() {
    return $this->model->where('is_active', true)->get();
  }

  public function findByPulsaRecurringId($pulsaRecurringId) {
    return $this->model->where('pulsa_recurring_id', $pulsaRecurringId)->first();
  }

  public function findByPhoneNumber($telephone) {
    return $this->model->whereRaw("replace(number, '-', '') = ? ", [$telephone])->first();
  }
}
