<?php

namespace Odeo\Domains\Vendor\SMS\Center\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\SMS\Center\Model\SmsCenterHistory;

class SmsCenterHistoryRepository extends Repository {

  public function __construct(SmsCenterHistory $smsCenterHistory) {
    $this->model = $smsCenterHistory;
  }

  public function findByOrderId($orderId) {
    return $this->model->where('order_id', $orderId)->first();
  }

  public function findSameMessage($message) {
    return $this->model->where('message', $message)
      ->where('created_at', '>', Carbon::now()->subHours(12)->toDateTimeString())->first();
  }

}
