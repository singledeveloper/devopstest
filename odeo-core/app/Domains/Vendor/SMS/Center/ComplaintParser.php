<?php

namespace  Odeo\Domains\Vendor\SMS\Center;

use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\SMS\CenterManager;

class ComplaintParser extends CenterManager {

  public function __construct() {
    parent::__construct();
  }

  public function parse(PipelineListener $listener, $data) {

    $this->saveHistory($data);

    list($command, $message) = explode('.', $data['sms_message']);

    Mail::raw('New complaint from user: ' . $message . '. Please sms/call them at ' . $data['sms_to'], function($d){
      $d->to('cs@odeo.co.id');
      $d->subject('[SMS]New Complaint - ' . time());
      $d->from('noreply@odeo.co.id');
      $d->cc('replenishment@odeo.co.id');
    });

    $this->reply('Komplain sudah kami terima. Akan kami proses paling lambat 1x24 jam.', $data['sms_to']);

    return $listener->response(200);
  }

}
