<?php


namespace Odeo\Domains\Vendor\Redig\Scheduler;


use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\RedigDisbursement;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Vendor\Redig\BalanceUpdater;
use Odeo\Domains\Vendor\Redig\Helper\ApiManager;
use Odeo\Domains\Vendor\Redig\Jobs\SendRedigImbalanceAlert;
use Odeo\Domains\Vendor\Redig\Jobs\SendRedigTopupAlert;

class CheckRedigBalance {

  private $vendorDisbursementRepo;
  private $apiManager;
  private $vendorSwitcherReplenishmentRepo;
  private $balanceUpdater;
  private $redis;

  public function __construct() {
    $this->vendorDisbursementRepo = app()->make(VendorDisbursementRepository::class);
    $this->apiManager = app()->make(ApiManager::class);
    $this->vendorSwitcherReplenishmentRepo = app()->make(VendorSwitcherReplenishmentRepository::class);
    $this->balanceUpdater = app()->make(BalanceUpdater::class);
    $this->redis = Redis::connection();
  }

  public function run() {
    $res = $this->apiManager->deposit();
    if ($res['error'] || Arr::get($res, 'data.status') !== '0') {
      return;
    }

    $vendor = $this->vendorDisbursementRepo->findById(VendorDisbursement::REDIG);
    $remoteBalance = Arr::get($res, 'data.data2', 0);
    $diff = $remoteBalance - $vendor->balance;

    if ($this->isTopup($diff)) {
      $pipeline = new Pipeline();
      $pipeline->add(new Task(BalanceUpdater::class, 'topup', [
        'amount' => $diff,
      ]));
      /*$pipeline->pushQueue(new SendRedigTopupAlert([
        'redig_balance' => $remoteBalance,
        'recorded_balance' => $vendor->balance,
        'topup_amount' => $diff,
        'final_balance' => $diff + $vendor->balance,
        'datetime' => Carbon::now()->toDateTimeString()
      ]));*/
      $pipeline->execute();
      return;
    }

    if (!$this->shouldAlert($diff)) {
      return;
    }

    /*dispatch(new SendRedigImbalanceAlert([
      'redig_balance' => $remoteBalance,
      'recorded_balance' => $vendor->balance,
      'diff_balance' => $diff,
      'datetime' => Carbon::now()->toDateTimeString(),
    ]));*/
  }

  public function isTopup($diff) {
    $repl = $this->vendorSwitcherReplenishmentRepo->findLastReplenishment(RedigDisbursement::VENDOR_SWITCHER_ID);
    if (!$repl) {
      return false;
    }
    return $repl->amount == $diff;
  }

  public function shouldAlert($diff) {
    if ($diff == 0) {
      $this->redis->hset('odeo_core:redig', 'balance_imbalance_count', 0);
      return false;
    }

    if (!($imbalanceCount = $this->redis->hget('odeo_core:redig', 'balance_imbalance_count'))) {
      $imbalanceCount = 0;
    }

    $imbalanceCount = ($imbalanceCount + 1) % 3;
    $this->redis->hset('odeo_core:redig', 'balance_imbalance_count', $imbalanceCount);

    return $imbalanceCount == 0;
  }
}