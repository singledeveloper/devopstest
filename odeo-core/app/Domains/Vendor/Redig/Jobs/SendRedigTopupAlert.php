<?php

namespace Odeo\Domains\Vendor\Redig\Jobs;


use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendRedigTopupAlert extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    Mail::send('emails.redig_topup_detected', [
      'data' => $this->data,
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id')
        ->to('disbursement@odeo.co.id')
        ->subject('Redig Disbursement Topup Alert - ' . date('Y-m-d'));
    });
  }
}