<?php


namespace Odeo\Domains\Vendor\Redig;


use Odeo\Domains\Constant\RedigDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Redig\Repository\DisbursementRedigDisbursementRepository;

class RedigDisbursementSelector {

  private $disbursementRedigDisbursementRepo;

  public function __construct() {
    $this->disbursementRedigDisbursementRepo = app()->make(DisbursementRedigDisbursementRepository::class);
  }

  public function transforms($item) {
    $row = [
      'redig_disbursement_id' => $item->id,
      'bank_name' => $item->bank->name,
      'account_no' => $item->account_no,
      'account_name' => $item->account_name,
      'amount' => $item->amount,
      'description' => $item->description,
      'status' => $item->status,
      'message' => RedigDisbursement::toStatusMessage($item->status),
      'disbursement_type' => $item->disbursement_type,
      'disbursement_reference_id' => $item->disbursement_reference_id,
      'inquiry_request_id' => $item->inquiry_request_id,
      'inquiry_status_code' => $item->inquiry_status_code,
      'inquiry_error' => '',
      'transfer_request_id' => $item->transfer_request_id,
      'transfer_status_code' => $item->transfer_status_code,
      'transfer_error' => '',
    ];

    if ($item->inquiry_request_id) {
      $row['inquiry_error'] = $item->inquiryRequest->error;
    }

    if ($item->transfer_request_id) {
      $row['transfer_error'] = $item->transferRequest->error;
    }

    return $row;
  }

  public function get(PipelineListener $listener, $data) {
    $this->disbursementRedigDisbursementRepo->normalizeFilters($data);
    $this->disbursementRedigDisbursementRepo->setSimplePaginate(true);

    $result = $this->disbursementRedigDisbursementRepo
      ->get()
      ->map([$this, 'transforms']);

    if (sizeof($result) > 0) {
      return $listener->response(200, array_merge(
        ["redig_disbursement_record" => $result],
        $this->disbursementRedigDisbursementRepo->getPagination()
      ));
    }
    return $listener->response(204, ["redig_disbursement_record" => []]);
  }

}