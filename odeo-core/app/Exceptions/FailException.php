<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 15/01/18
 * Time: 16.01
 */

namespace Odeo\Exceptions;


class FailException extends \Exception {
  private $errorStatus;

  public function __construct($message, $errorStatus = null) {
    parent::__construct($message);
    $this->errorStatus = $errorStatus;
  }
  
  public function getErrorStatus() {
    return $this->errorStatus;
  }
}
