<?php

namespace Odeo\Http\Middleware;

use Closure;
use Odeo\Domains\Constant\Supplier;
use Validator;
use Odeo\Domains\Affiliate\AffiliateValidator;
use Odeo\Domains\Affiliate\Formatter\ApiTypeManager;
use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Account\TokenManager;

class AuthenticateAffiliate {

  use \Odeo\Domains\Core\IO;

  public function handle($request, Closure $next) {

    $route = $request->route();
    $apiType = is_array($route) ? (isset($route[2]['api_type']) ? $route[2]['api_type'] : false) : $route->parameter('api_type');
    if (!$apiType) $apiType = $request->getMethod() == 'GET' ? Affiliate::API_TYPE_GET : Affiliate::API_TYPE_JSON;

    $data = [];

    if (!$request->header('Authorization')) {
      if ($apiType == Affiliate::API_TYPE_JSON)
        return $this->buildErrorsResponse(authDebug('Authorization header is not there.'), 401);
    } else {
      $auth = explode(", ", $request->header('Authorization'));
      if (count($auth) > 1) {
        return $this->buildErrorsResponse(authDebug('Authorization header is not valid.'), 401);
      }

      if (!($auth = explode(" ", $auth[0])) || count($auth) != 2) {
        return $this->buildErrorsResponse(authDebug('Bearer format is not valid.'), 401);
      }

      $credential = explode(':', base64_decode($auth[1]));

      if ($auth[0] != 'Bearer' || count($credential) != 2) {
        return $this->buildErrorsResponse(authDebug('Bearer value is not valid.'), 401);
      }

      $data['mid'] = trim($credential[0]);
      $data['secret_key'] = trim($credential[1]);
    }

    if (!isset($data['mid']) || $apiType != Affiliate::API_TYPE_JSON) {
      $apiTypeManager = app()->make(ApiTypeManager::class)->setPath($apiType);
      if ($apiTypeManager == '') return response('Not valid url', 400);

      $typeRequest = $apiTypeManager->deconstructRequest();
      if ($typeRequest === false)
        return $apiTypeManager->constructResponse([
          'message' => 'Data yang dikirimkan tidak valid. Mohon cek kembali struktur ' . $apiType . ' yang Anda kirim'
        ], Supplier::RC_FAIL);

      $data = array_merge($data, $typeRequest);

      if (!isset($data['mid'])) {
        $validator = Validator::make($data, [
          'mid' => 'required|integer',
          'secret_key' => 'required'
        ]);

        if ($validator->fails()) return $apiTypeManager->constructResponse([
          'message' => $validator->errors()->all()
        ], Supplier::RC_FAIL);
      }
    }

    $pipeline = new Pipeline;
    $pipeline->add(new Task(AffiliateValidator::class, 'guard', [
      'api_type' => $apiType
    ]));
    $pipeline->add(new Task(TokenManager::class, 'setUserFromAffiliate'));
    $pipeline->execute([
      'mid' => $data['mid'],
      'secret_key' => $data['secret_key'],
      'ip_address' => getClientIP()
    ]);

    if ($pipeline->fail()) {
      if ($apiType != Affiliate::API_TYPE_JSON)
        return app()->make(ApiTypeManager::class)->setPath($apiType)->constructResponse([
          'message' => $pipeline->errorMessage
        ], Supplier::RC_FAIL);
      return $this->buildErrorsResponse($pipeline->errorMessage, $pipeline->statusCode);
    }

    $data['auth'] = $pipeline->data['auth'];
    $apiType = $pipeline->data['api_type'];

    $request->merge($data);

    $response = $next($request);

    if ($apiType != Affiliate::API_TYPE_JSON) {
      $response = json_decode($response->getOriginalContent(), true)['data'];
      $rc = Supplier::RC_OK;
      if (isset($response['errors'])) {
        $error = $response['errors'][0];
        if (strpos($error, 'Trx sdh pernah') === false) $rc = Supplier::RC_FAIL;
        else $rc = Supplier::RC_EVEN;
        $response['message'] = $error;
        unset($response['errors']);
      }
      else if (sizeof($response) == 1) {
        $oldResponse = $response;
        reset($response);
        $response = $response[key($response)];
        if (!is_array($response)) $response = $oldResponse;
      }
      if (isset($response['rc'])) {
        $rc = $response['rc'];
        unset($response['rc']);
      }

      return app()->make(ApiTypeManager::class)->setPath($apiType)->constructResponse($response, $rc);
    }

    return $response;
  }
}
