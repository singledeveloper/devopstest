<?php

namespace Odeo\Http\Middleware;

use Closure;
use Odeo\Domains\Account\TokenManager;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementGuard;
use Odeo\Domains\Disbursement\Helper\DisbursementApiHelper;

class AuthenticateDisbursement {

  public function handle($request, Closure $next) {
    if (!$request->header('Authorization')) {
      return response()->json(
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_UNAUTHORIZED), 401);
    }

    list($scheme, $auth) = array_pad(explode(' ', $request->header('Authorization'), 2), 2, null);
    list($accessKeyId, $signature) = array_pad(explode(':', $auth, 2), 2, null);

    if (strtoupper($scheme) != 'ODEO' || !$accessKeyId || !$signature) {
      return response()->json(
        ApiDisbursement::getErrorObject(ApiDisbursement::ERROR_UNAUTHORIZED), 401);
    }

    $pipeline = new Pipeline;
    $pipeline->add(new Task(ApiDisbursementGuard::class, 'guard'));
    $pipeline->add(new Task(TokenManager::class, 'setUserFromDisbursement'));
    $pipeline->execute([
      'access_key_id' => $accessKeyId,
      'signature' => $signature,
      'ip_address' => getClientIP()
    ]);

    if ($pipeline->fail()) {
      return response()->json($pipeline->errorMessage, $pipeline->statusCode);
    }

    $request->merge([
      "auth" => $pipeline->data,
      "api_data" => [
        "timestamp" => $request->header(DisbursementApiHelper::TIMESTAMP_HEADER),
        "signature" => $signature,
        "access_key_id" => $accessKeyId
      ]
    ]);

    return $next($request);
  }
}
