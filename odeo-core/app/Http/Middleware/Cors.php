<?php

namespace Odeo\Http\Middleware;

use Closure;
use Odeo\Domains\Constant\Header;

class Cors {

  private $acceptable_lang = ["id", "en", "zh"];

  public function handle($request, Closure $next) {
    $headers = [
      'Access-Control-Allow-Origin' => '*',
    ];

    if (app()->environment() == 'production') {
      if (versioning($request, Header::ANDROID_HEADER_VER, env('ANDROID_MINIMAL_REQ_VERSION')) == "not-same") {
        return response("", 410);
      }
      if (versioning($request, Header::WEB_APP_HEADER_VER, env('WEB_APP_LATEST_VERSION')) == "not-same") {
        return response("", 410, $headers);
      }
      if (versioning($request, Header::IOS_HEADER_VER, env('IOS_LATEST_VERSION')) == "not-same") {
        return response("", 410, $headers);
      }
    }

    if ($lang = checkHeaderLanguage($this->acceptable_lang)) {
      app('translator')->setLocale($lang);
    }

    $response = $next($request);

    foreach ($headers as $key => $value) {
      $response->header($key, $value);
    }

    return $response;

  }
}
