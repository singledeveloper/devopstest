<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/weekdays-restock-report', 'CampaignController@weekdayRestockReport');
$app->get('/interest-report', 'CampaignController@interestReport');

$app->get('/accounting-report', 'AccountingController@report');

$app->post('competitor-price', 'CompetitorPriceController@create');
$app->get('competitor-price', 'CompetitorPriceController@get');
$app->get('competitor-price/names', 'CompetitorPriceController@getNames');
$app->get('competitor-price/compare', 'CompetitorPriceController@getPriceComparison');

$app->get('/user', 'UserController@getAll');
$app->get('/user/affiliates', 'UserController@getAllAffiliates');
$app->post('/user/affiliates/edit', 'UserController@editAffiliate');
$app->get('/user/{user_id}/summary', 'UserController@summariesUser');
$app->post('/user/{user_id}/toggle-block', 'UserController@toggleUserBlock');
$app->post('/user/{user_id}/update-status', 'UserController@updateStatus');
$app->post('/user/{user_id}/send-otp', 'UserController@sendOtpEmail');

$app->post('/user/withdraw/complete', 'WithdrawController@completeWithdraw');
$app->post('/user/create', 'UserController@createUser');
$app->post('/user/create-business', 'UserController@createBusinessUser');

$app->get('/user/banned-bank-account', 'UserBannedBankAccountController@get');
$app->put('/user/banned-bank-account/{id}', 'UserBannedBankAccountController@update');

$app->get('/user/topup-bank-transfers', 'TopupController@getTopupBankTransfers');
$app->post('/user/topup-bank-transfers/toggle', 'TopupController@toggleTopupBankTransfers');

$app->get('/user/otp-attempt', 'UserOtpAttemptController@get');
$app->put('/user/otp-attempt/{id}', 'UserOtpAttemptController@update');

$app->get('/user/business-kyc/e/{kyc_id}', 'UserBusinessKycController@checkById');
$app->get('/user/business-kyc/list', 'UserBusinessKycController@getUsers');
$app->get('/user/business-kyc/detail/{kyc_id}', 'UserBusinessKycController@getById');
$app->post('/user/business-kyc/approval', 'UserBusinessKycController@manageApproval');

$app->get('/business-reseller/options', 'BusinessResellerController@getOptions');
$app->get('/business-reseller/list', 'BusinessResellerController@getList');
$app->get('/business-reseller/templates', 'BusinessResellerController@getTemplates');
$app->get('/business-reseller/{reseller_id}/details', 'BusinessResellerController@getDetail');
$app->get('/business-reseller/template/{template_id}/details', 'BusinessResellerController@getDetailTemplate');
$app->post('/business-reseller/change-template', 'BusinessResellerController@changeResellerTemplate');
$app->post('/business-reseller/add-group', 'BusinessResellerController@addGroup');
$app->post('/business-reseller/remove/{group_id}', 'BusinessResellerController@removeGroup');
$app->post('/business-reseller/add-group-detail', 'BusinessResellerController@addGroupDetail');
$app->post('/business-reseller/remove-detail/{detail_id}', 'BusinessResellerController@removeGroupDetail');
$app->post('/business-reseller/approve-bonus', 'BusinessResellerController@approveBonus');
$app->get('/business-reseller/pg-transactions', 'BusinessResellerController@getPgTransactions');

$app->get('/store', 'StoreController@index');
$app->post('/store/domainSetup', 'StoreController@domainSetup');
$app->post('/store/createFree', 'SubscriptionController@createFree');

$app->get('/store/deposit', 'SubscriptionController@getDeposit');
$app->get('/store/deposit/locked', 'SubscriptionController@getLockedDeposit');

$app->get('/store/users-have-no-store', 'StoreController@getUsersHaveNoStore');

$app->get('/store/suppliers', 'StoreController@getSupplierStores');

$app->get('/channel/list', 'ChannelController@listChannel');
$app->post('/channel/updateDeviceId', 'ChannelController@updateDeviceId');
$app->get('/channel/code', 'ChannelController@code');
$app->post('/channel/assignCode', 'ChannelController@assignCode');
$app->post('/channel/generateCode', 'ChannelController@generateCode');
$app->post('/channel/printCode', 'ChannelController@printCode');
$app->post('/channel/setMDR', 'ChannelController@setMDR');

$app->get('/order/accumulate-day-one', 'OrderController@accumulateSalesPerStoreFromDayOne');
$app->get('/order', 'OrderController@getAll');

$app->get('/order/{order_id}/confirmation-detail', 'PaymentController@getConfirmationDetail');

$app->post('/order/{order_id}/verify', 'OrderController@verify');
$app->post('/order/{order_id}/complete', 'OrderController@complete');
$app->put('/order/{order_id}/update-status', 'OrderController@updateStatus');
$app->post('/order/{order_id}/refund', 'OrderController@refund');
$app->post('/order/{order_id}/void', 'OrderController@void');
$app->post('/order/{order_id}/fault-refund', 'OrderController@faultRefund');
$app->post('/order/{order_id}/send-email-complete', 'OrderController@sendEmailComplete');
$app->get('/order/reconciliation', 'OrderReconciliationController@reconciliation');
$app->post('/order/reconciliation/reconcile', 'OrderReconciliationController@reconcileOrder');
$app->post('/order/reconciliation/batch-reconcile', 'OrderReconciliationController@reconcileOrderBatch');
$app->post('/order/reconciliation/recalculate', 'OrderReconciliationController@recalculateReconciliation');
$app->get('/order/reconciliation/summary', 'OrderReconciliationController@reconciliationSUmmary');

$app->get('/transaction', 'TransactionController@get');
$app->post('/transaction/patch', 'TransactionController@patch');

$app->post('/pulsa/add', 'PulsaSwitcherController@addPulsa');
$app->post('/pulsa/replicate', 'PulsaSwitcherController@replicatePulsa');
$app->get('/pulsa/operators', 'PulsaSwitcherController@getOperators');
$app->get('/pulsa/categories', 'PulsaSwitcherController@getCategories');
$app->get('/pulsa/data', 'PulsaSwitcherController@get');
$app->post('/pulsa/inventories/add', 'PulsaSwitcherController@addInventories');
$app->post('/pulsa/inventories/edit', 'PulsaSwitcherController@editInventories');
$app->get('/pulsa/inventories', 'PulsaSwitcherController@getInventories');
$app->get('/pulsa/inventories/suppliers', 'PulsaSwitcherController@getSupplierInventories');
$app->post('/pulsa/order/edit', 'PulsaSwitcherController@editPulsaOrder');
$app->get('/pulsa/order', 'PulsaSwitcherController@getOrder');
$app->get('/pulsa/log/{switcher_id}', 'PulsaSwitcherController@getOrderLog');
$app->get('/pulsa/sla', 'PulsaSwitcherController@getSLA');
$app->get('/pulsa/lead-time', 'PulsaSwitcherController@getLeadTime');
$app->get('/pulsa/report', 'PulsaSwitcherController@getReport');
$app->get('/pulsa/channel-logs', 'ChannelLogController@get');
$app->delete('/pulsa/channel-logs', 'ChannelLogController@remove');
$app->put('/pulsa/channel-logs/{id}', 'ChannelLogController@update');

$app->get('/pulsa/h2h/groups', 'H2HController@getGroupList');
$app->get('/pulsa/h2h/group/{group_id}/details', 'H2HController@getGroupDetails');
$app->post('/pulsa/h2h/group/add', 'H2HController@addGroup');
$app->post('/pulsa/h2h/group/{group_id}/edit', 'H2HController@editGroup');
$app->post('/pulsa/h2h/group/{group_id}/user/add', 'H2HController@addUser');
$app->delete('/pulsa/h2h/group/{group_id}/user/remove', 'H2HController@removeUser');
$app->post('/pulsa/h2h/group/{group_id}/detail/add', 'H2HController@addGroupDetail');
$app->delete('/pulsa/h2h/group/{group_id}/detail/remove', 'H2HController@removeGroupDetail');
$app->post('/pulsa/h2h/group/{group_id}/broadcast', 'H2HController@broadcastFromGroup');

$app->get('/pulsa/bulk-purchase/groups', 'PulsaBulkPurchaseController@getGroups');
$app->post('/pulsa/bulk-purchase/group/create', 'PulsaBulkPurchaseController@bulkCreateGroup');
$app->post('/pulsa/bulk-purchase/group/remove', 'PulsaBulkPurchaseController@removeGroup');
$app->post('/pulsa/bulk-purchase/group/approve', 'PulsaBulkPurchaseController@bulkChangeApproval');
$app->post('/pulsa/bulk-purchase/generate', 'PulsaBulkPurchaseController@bulkPurchase');
$app->post('/pulsa/bulk-purchase/toggle-detail', 'PulsaBulkPurchaseController@toggleDetail');

$app->get('/feedback', 'FeedbackController@getAll');

$app->get('/withdrawal', 'WithdrawController@withdraw');
$app->get('/withdrawal/user-withdraws', 'WithdrawController@userWithdrawReport');

$app->get('/api-disbursement', 'DisbursementController@getDisbursementsForAdmin');
$app->get('/api-disbursement/vendor-ratio', 'DisbursementController@getVendorRatio');
$app->get('/api-disbursement/suspect-count', 'DisbursementController@getSuspectCount');
$app->post('/api-disbursement/retry', 'DisbursementController@retry');
$app->post('/api-disbursement/bulk-retry', 'DisbursementController@bulkRetry');
$app->post('/api-disbursement/notify', 'DisbursementController@notify');
$app->post('/api-disbursement/mark-as-success', 'DisbursementController@markAsSuccess');
$app->post('/api-disbursement/mark-as-cancel', 'DisbursementController@markAsCancel');
$app->get('/api-disbursement-inquiry', 'DisbursementController@getDisbursementInquiriesForAdmin');

$app->get('/payment', 'PaymentController@getAll');

$app->get('/financial/order', 'OrderController@getPaginatedFinancialOrder');
$app->get('/financial/order/all', 'OrderController@getAllFinancialOrder');

$app->get('/financial/company-cash-account', 'CompanyCashController@getAccount');
$app->get('/financial/company-cash-account/{id}/transaction', 'CompanyCashController@getTransactions');
$app->put('/financial/company-cash-account/{id}/transaction', 'CompanyCashController@updateReference');
$app->get('/financial/company-cash-account/{id}/export-transaction', 'CompanyCashController@exportTransaction');
$app->delete('/financial/cash-transaction-information/detail/{cash_transaction_information_id}', 'CompanyCashController@removeDetail');

$app->get('/payment/channel/group-list', 'PaymentGatewayController@paymentChannelGroupList');
$app->put('/payment/channel/toggle', 'PaymentController@togglePaymentChannel');

$app->get('/payment/doku/creditcard', 'DokuController@getAllCreditCard');

$app->get('/payment/doku/alfagroup', 'DokuController@getAllAlfaGroup');

$app->get('/payment/doku/dokuwallet', 'DokuController@getAllDokuWallet');

$app->get('/payment/kredivo', 'KredivoController@getAllKredivo');

$app->get('/payment/bank-transfer', 'BankTransferController@getAccountNumber');
$app->get('/payment/bank-transfer/{bank}/account/{id}/inquiry', 'BankTransferController@getInquiry');
$app->get('/payment/bank-transfer/{bank}/account/{id}/export-inquiry', 'BankTransferController@exportInquiry');
$app->put('/payment/bank-transfer/{bank}/account/{id}/inquiry', 'BankTransferController@updateInquiry');
$app->put('/payment/bank-transfer/{bank}/account/{id}/create-topup', 'BankTransferController@manualCreateTopupFromBankTransfer');
$app->post('/payment/bank-transfer/{bank}/account/{id}/create-inquiry-information', 'BankTransferController@createInquiryInformation');

$app->get('/payment/inquiry-information', 'InquiryInformationController@getInquiryInformations');
$app->get('/payment/inquiry-information/{bank}/{reference_id}', 'InquiryInformationController@getInquiryInformationDetails');
$app->get('/payment/inquiry-information/file-images', 'InquiryInformationController@getInquiryInformationFileImages');
$app->delete('/payment/inquiry-information/file-images/{id}', 'InquiryInformationController@removeDetail');

$app->get('/payment/settlement', 'SettlementController@get');
$app->get('/payment/settlement/detail', 'SettlementController@getDetail');

$app->get('/activity', 'ActivityController@activity');

$app->get('/income', 'IncomeController@income');

$app->get('/sales-ocommerce-report', 'SalesReportController@getSalesOcommerceReport');
$app->get('/sales-platform-report', 'SalesReportController@getSalesPlatformReport');
$app->get('/sales-bypayment-report', 'SalesReportController@getSalesByPaymentReport');

$app->get('/store-merchant-success', 'StoreMerchantSuccessController@index');

$app->get('/announcement/list', 'AnnouncementController@get');
$app->post('/announcements', 'AnnouncementController@create');
$app->delete('/announcements', 'AnnouncementController@delete');

$app->get('/faqs', 'FaqController@get');
$app->post('/faqs', 'FaqController@insert');
$app->post('/faqs/{id}/priority', 'FaqController@updatePriority');
$app->put('/faqs/{id}', 'FaqController@update');
$app->delete('/faqs', 'FaqController@remove');

$app->get('/webstore/banner', 'WebStoreBannerController@get');
$app->post('/webstore/banner', 'WebStoreBannerController@insert');
$app->put('/webstore/banner/activate/{banner_id}', 'WebStoreBannerController@activate');
$app->delete('/webstore/banner', 'WebStoreBannerController@remove');

$app->get('/mobile/banner/all', 'MobileBannerController@getByAdmin');
$app->post('/mobile/banner', 'MobileBannerController@insert');
$app->put('/mobile/banner/activate/{banner_id}', 'MobileBannerController@activate');
$app->delete('/mobile/banner', 'MobileBannerController@remove');

$app->get('/user/ktp', 'UserKtpController@getAll');
$app->put('/user/ktp/{ktp_id}', 'UserKtpController@updateStatus');
$app->get('/user/{user_id}/ktp/history', 'UserKtpController@getUserKtpHistory');

$app->get('/registered-user-report', 'RegisteredUserController@getRegisteredUserReport');

$app->post('/blast-notification/mobile/user', 'BlastNotificationController@blastToUser');
$app->post('/blast-notification/mobile/all', 'BlastNotificationController@blastToAll');

$app->get('/test-daily-report', 'OrderController@testDailyReport');
$app->post('/test-bakoel-status', 'BillerBakoelController@checkStatus');

$app->get('/vendor/bca/record', 'VendorBcaController@getBcaRecord');
$app->get('/vendor/bca/information', 'VendorBcaController@getInformation');
$app->post('/vendor/bca/internal-refund', 'VendorBcaController@internalRefund');
$app->get('/vendor/bca/suspect-disbursement', 'VendorBcaController@getSuspectDisbursements');

$app->get('/vendor/redig/record', 'VendorRedigController@getRedigRecord');
$app->get('/vendor/redig/information', 'VendorRedigController@getInformation');

$app->get('/vendor/bni/record', 'VendorBniController@getBniRecord');
$app->get('/vendor/bni/information', 'VendorBniController@getInformation');
$app->get('/vendor/bni/suspect-disbursement', 'VendorBniController@getSuspectDisbursements');
$app->get('/vendor/flip/record', 'VendorFlipController@getFlipRecord');
$app->get('/vendor/flip/information', 'VendorFlipController@getInformation');
$app->get('/vendor/cimb/record', 'VendorCimbController@getDisbursementRecord');
$app->get('/vendor/cimb/information', 'VendorCimbController@getInformation');
$app->get('/vendor/cimb/suspect-disbursement', 'VendorCimbController@getSuspectDisbursements');
$app->get('/vendor/permata/record', 'VendorPermataController@getPermataRecord');
$app->get('/vendor/permata/information', 'VendorPermataController@getInformation');
$app->get('/vendor/permata/suspect-disbursement', 'VendorPermataController@getSuspectDisbursements');
$app->get('/vendor/mandiri/record', 'VendorMandiriController@getMandiriRecord');
$app->get('/vendor/mandiri/information', 'VendorMandiriController@getInformation');
$app->post('/vendor/mandiri/internal-refund', 'VendorMandiriController@internalRefund');
$app->get('/vendor/mandiri/suspect-disbursement', 'VendorMandiriController@getSuspectDisbursements');
$app->get('/vendor/bri/information', 'VendorBriController@getInformation');
$app->get('/vendor/bri/record', 'VendorBriController@getBriRecord');
$app->get('/vendor/bri/suspect-disbursement', 'VendorBriController@getSuspectDisbursements');

$app->post('/user/withdraw/auto-transfer/retry', 'WithdrawController@retryAutoWithdraw');
$app->post('/user/withdraw/auto-transfer/mark-as-success', 'WithdrawController@markAsSuccess');
$app->post('/user/withdraw/auto-transfer/mark-as-cancel', 'WithdrawController@markAsCancel');

$app->get('/revenue/recalculate', 'MarketingController@recalculateRevenue');

$app->get('/vendor/artajasa-disbursement/record', 'VendorArtajasaDisbursementController@getArtajasaRecord');
$app->get('/vendor/artajasa-disbursement/information', 'VendorArtajasaDisbursementController@getInformation');
$app->post('/vendor/artajasa-disbursement/internal-refund', 'VendorArtajasaDisbursementController@internalRefund');
$app->get('/vendor/artajasa-disbursement/suspect-disbursement', 'VendorArtajasaDisbursementController@getSuspectDisbursements');

$app->get('/billers', 'BillerController@get');
$app->get('/biller/reconciliations', 'BillerController@recon');
$app->get('/biller/replenishments', 'BillerController@replenish');
$app->post('/biller/replenishments/create', 'BillerController@replenishCreate');
$app->post('/biller/replenishments/verify', 'BillerController@replenishVerify');
$app->post('/biller/replenishments/force-fail', 'BillerController@replenishFail');
$app->post('/biller/replenishments/cancel', 'BillerController@replenishCancel');
$app->post('/biller/replenishments/force-complete', 'BillerController@replenishComplete');

$app->get('/biller/last-mutations', 'BillerController@lastMutation');
$app->get('/biller/{biller_id}/mutations', 'BillerController@getMutations');
$app->get('/biller/{biller_id}/replenishment-setting', 'BillerController@getReplenishmentSetting');
$app->post('/biller/replenishment-setting/edit', 'BillerController@editReplenishmentSetting');
$app->get('/biller/replenishment-setting/condition/{setting_id}', 'BillerController@getReplenishmentConditionDetail');
$app->post('/biller/replenishment-setting/condition/add', 'BillerController@addReplenishmentCondition');
$app->post('/biller/replenishment-setting/condition/edit', 'BillerController@editReplenishmentCondition');
$app->post('/biller/replenishment-setting/condition/remove', 'BillerController@removeReplenishmentCondition');
$app->post('/biller/mutation/insert', 'PulsaSwitcherController@insertManualMutation');
$app->post('/biller/mutation/balance-all', 'PulsaSwitcherController@insertBalanceMutationAll');

//$app->get('/pulsa/balance-information', ['as' => 'test_named_route_7788', 'uses' => 'BillerController@getBalanceInformations']);
$app->get('/pulsa/balance-information', 'BillerController@getBalanceInformations');
$app->post('/pulsa/balance-information/read-notice/{notification_id}', 'NotificationController@readInternal');
$app->get('/sms/logs', 'SMSController@getLogs');

$app->get('/migrate-pin', 'UserController@migratePin');
$app->get('/report/resend-core', function(){(new \Odeo\Domains\Activity\Scheduler\AutoSendReportScheduler)->run();});

$app->get('/affiliate/invoice/{biller_id}/init-user', 'AffiliateInvoiceController@initUser');
$app->post('/affiliate/invoice/resend-notification', 'AffiliateInvoiceController@resendNotification');
$app->get('/pg/payment/{payment_id}/notify-payment', 'PaymentGatewayController@resendPaymentNotification');

$app->get('/pg/payment', 'PaymentGatewayController@getAllPayment');
$app->get('/pg/payment/logs', 'PaymentGatewayController@getPgNotifyLog');
$app->get('/pg/payment/export', 'PaymentGatewayController@export');

$app->post('/pg/payment/mark-as-accepted', 'PaymentGatewayController@markAsAccepted');
$app->post('/pg/payment/notify-payment', 'PaymentGatewayController@notifyPayment');

$app->get('/payment/prismalink/va/payment', 'PrismalinkPaymentController@getAllVaPayment');
$app->get('/payment/prismalink/va/inquiry', 'PrismalinkPaymentController@getAllVaInquiry');
$app->get('/payment/doku/va/payment', 'DokuPaymentController@getAllVaPayment');
$app->get('/payment/doku/va/inquiry', 'DokuPaymentController@getAllVaInquiry');
$app->get('/payment/aj/va/payment', 'ArtajasaPaymentController@getAllVaPayment');
$app->get('/payment/aj/va/inquiry', 'ArtajasaPaymentController@getAllVaInquiry');
$app->get('/payment/permata/va/payment', 'PermataPaymentController@getAllVaPayment');
$app->get('/payment/permata/va/inquiry', 'PermataPaymentController@getAllVaInquiry');

$app->get('/payment/prismalink/va-order-payment', 'PrismalinkPaymentController@getOrderPayments');
$app->get('/payment/prismalink/va-order-payment/export', 'PrismalinkPaymentController@exportData');
$app->post('/payment/prismalink/va-order-summary', 'PrismalinkPaymentController@getVaTransactionCount');

$app->get('/approval/{id}/raw-data', 'ApprovalController@findRawData');
$app->post('/approval/submit-data', 'ApprovalController@submitData');
$app->post('/approval/{id}/approve', 'ApprovalController@approveData');
$app->post('/approval/{id}/cancel', 'ApprovalController@cancelData');

$app->post('/check-template/{template_id}/{biller_id}', function($templateId, $billerId){
  $response = app()->make(\Illuminate\Http\Request::class)->getContent();
  $details = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseDetailRepository::class);
  $updater = app()->make(\Odeo\Domains\Supply\Helper\BillerStatusUpdater::class);

  $data = [
    'match' => false,
    'result' => [],
    'tries' => [],
  ];

  foreach ($details->getResponseList($templateId, $billerId, \Odeo\Domains\Constant\Supplier::ROUTE_RESPONSE) as $item) {
    if ($updater->translateResponse($item, $response)) {
      $data['result'] = $updater->translateResult;
      $data['match'] = true;
    }
    $responseConditions = $item->format_type == \Odeo\Domains\Constant\Supplier::TYPE_TEXT ? $item->response_details : json_decode($item->response_details, true);
    $data['tries'][] = $responseConditions;
  }

  return $data;
});

$app->get('/patch-cash', function(){
  $inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
  /*$switcherRepo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  $reconRepo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);

  $results = [];
  $count = 0;
  $result = \DB::select(\DB::raw("select vsr.id, od.order_id, vsr.order_detail_pulsa_switcher_id, odps.first_base_price, vsr.sale_amount, poi.supply_pulsa_id, po.price, us.user_id
from vendor_switcher_reconciliations vsr
join order_detail_pulsa_switchers odps on vsr.order_detail_pulsa_switcher_id = odps.id
join pulsa_odeo_inventories poi on odps.current_inventory_id = poi.id
join order_details od on odps.order_detail_id = od.id
join pulsa_odeos po on po.id = poi.supply_pulsa_id
join user_stores us on us.store_id = po.owner_store_id
where poi.supply_pulsa_id is not null and odps.status = '50000' and vsr.status = '20000'
  and vsr.sale_amount - odps.first_base_price + odps.subsidy_amount > 0
  and date(requested_at) = '2019-02-21'
order by odps.id desc"));

  foreach ($result as $item) {
    if ($item->price < 40000) $baseMultiplier = 40000;
    else if ($item->price > 200000) $baseMultiplier = 200000;
    else $baseMultiplier = $item->price;
    $priceSupposed = $item->price + ceil(0.002 * $baseMultiplier) + 400;

    $switcher = $switcherRepo->findById($item->order_detail_pulsa_switcher_id);
    $switcher->current_base_price = $priceSupposed;
    if ($item->first_base_price != $priceSupposed) {
      $switcher->first_base_price = $priceSupposed;
      $switcher->subsidy_amount = $item->price < 100000 ? ($priceSupposed - $item->price + 600) : 0;
    }
    $switcherRepo->save($switcher);

    $recon = $reconRepo->findById($item->id);
    $recon->sale_amount = $priceSupposed;
    $recon->base_price = $item->price + 400;
    $reconRepo->save($recon);

    $haha = [
      'user_id' => $item->user_id, //83488
      'trx_type' => \Odeo\Domains\Constant\TransactionType::MANUAL_VOID,
      'cash_type' => \Odeo\Domains\Constant\CashType::OCASH,
      'amount' => $priceSupposed - $item->sale_amount,
      'data' => json_encode([
        "order_id" => $item->order_id
      ])
    ];

    $results[] = $haha;
    $inserter->add($haha);
    $count++;
  }*/

  /*$inserter->add([
    'user_id' => 811,
    'trx_type' => \Odeo\Domains\Constant\TransactionType::TRANSFER,
    'cash_type' => \Odeo\Domains\Constant\CashType::OCASH,
    'amount' => -500000,
    'data' => json_encode([
      'notes' => 'Cashback SGS 1 [2020-01-06]',
      'to' => 'PT Sinergi Global Servis',
      'to_user_id' => 189748
    ])
  ]);

  $inserter->add([
    'user_id' => 189748,
    'trx_type' => \Odeo\Domains\Constant\TransactionType::TRANSFER,
    'cash_type' => \Odeo\Domains\Constant\CashType::OCASH,
    'amount' => 500000,
    'data' => json_encode([
      'notes' => 'Cashback SGS 1 [2020-01-06]',
      'from' => 'ODEO Marketing',
      'from_user_id' => 811
    ])
  ]);*/

  $inserter->run();
  return 'OK28';
  /*return [
    'status' => 'OK3',
    'count' => $count,
    'result' => $results
  ];*/
});

$app->get('/patch-scheduler', function(){
  (new \Odeo\Domains\Biller\Gojek\Scheduler\GojekSalesChecker)->run();
  //(new \Odeo\Domains\Biller\TCash\Scheduler\PurchaseResetter)->run();
});

$app->get('/sku-check', function(){
  //$pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
  //$allSKU = $pulsaOdeos->getAllSKU();
  //return count($allSKU);
  //$deadStockData = [];
  //foreach ($pulsaOdeos->getDeadStock() as $item) $deadStockData[] = $item;
  //return $deadStockData;
  (new \Odeo\Domains\Activity\Scheduler\AutoRecordSKUDataScheduler)->run();
  return 'OKHAI';
});

$app->get('dc/{token}', function($token){
  return \Illuminate\Support\Facades\Crypt::decrypt($token);
});

$app->get('/zenziva-topup', function(){
  dispatch(new \Odeo\Domains\Disbursement\BillerReplenishment\Jobs\BeginReplenishment([
    'vendor_switcher_id' => \Odeo\Domains\Constant\SwitcherConfig::ZENZIVA
  ]));
});

$app->get('/patch-aws', function(){
  $userKycRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycRepository::class);
  $userKycDetailRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycDetailRepository::class);
  $users = [];
  $details = [];
  foreach (scandir('./temp_02751') as $item) {
    if ($item == '.' || $item == '..') continue;

    list($userId, $optionId, ) = explode('_', $item);
    if (!isset($users[$userId])) {
      $users[$userId] = $userKycRepo->checkCurrentKyc($userId)->id;
    }

    $url = 'business-kyc/' . $item;

    \Illuminate\Support\Facades\Storage::disk('s3')->put($url, file_get_contents('./temp_02751/' . $item), 'private');

    $details[] = [
      'kyc_id' => $users[$userId],
      'option_id' => $optionId,
      'file_url' => $url,
      'kyc_status' => '50000',
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ];
  };

  $userKycDetailRepo->saveBulk($details);

  return 'OK';
});

$app->get('/test-union', function(){
  $data = app()->make(\Illuminate\Http\Request::class)->all();
  if (!isset($data['date'])) return;

  $userRepo = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  $result = (new Odeo\Domains\Marketing\Scheduler\BusinessBonusAccumulator)->run($data['date']);

  $final = [];
  $users = [];
  foreach ($result as $item) {
    if (!isset($users[$item->user_id])) {
      $users[$item->user_id] = $userRepo->findById($item->user_id)->name;
    }
    $final[] = [
      'user_id' => $item->user_id,
      'user_name' => $users[$item->user_id],
      'type' => $item->type,
      'total_amount' => round($item->total_amount, 0),
      'total_fee' => round($item->total_fee, 0),
      'total_cost' => round($item->total_cost, 0),
      'qty' => $item->qty
    ];
  }

  return $final;
});

$app->get('/test-bonus', function(){
  (new \Odeo\Domains\Marketing\Scheduler\BusinessBonusSender)->run();
});

$app->get('/patch-bb', function(){
  $result = \DB::select(\DB::raw("select * from (
  (select date(verified_at) as transaction_date, user_id, 0 as payment_group_id,
  'disbursement' as type, sum(amount) as total_amount,
  sum(fee) as total_fee, sum(cost) as total_cost, count(id) as qty
from api_disbursements
where status = '50000' and batch_disbursement_id is null and date(verified_at) > '2018-12-31' and date(verified_at) not in ('2019-10-02','2019-10-03')
group by date(verified_at), user_id having sum(fee) > 0)
UNION
(select date(verified_at) as transaction_date, user_id, 0 as payment_group_id,
  'disbursement_batch' as type, sum(amount) as total_amount,
  sum(fee) as total_fee, sum(cost) as total_cost, count(id) as qty
from api_disbursements
where status = '50000' and batch_disbursement_id is not null and date(verified_at) > '2018-12-31' and date(verified_at) not in ('2019-10-02','2019-10-03')
group by date(verified_at), user_id having sum(fee) > 0)
UNION
(select date(created_at) as transaction_date, user_id, 0 as payment_group_id,
  'disbursement_inquiry' as type, 0 as total_amount,
  sum(fee) as total_fee, sum(cost) as total_cost, count(id) as qty
from api_disbursement_inquiries
where status in ('50000', '90001', '90003') and date(created_at) > '2018-12-31' and date(created_at) not in ('2019-10-02','2019-10-03')
group by date(created_at), user_id having sum(fee) > 0)
UNION
(select date(verified_at) as transaction_date, user_id, 0 as payment_group_id,
  'withdraw' as type, sum(amount) as total_amount,
  sum(fee) as total_fee, sum(cost) as total_cost, count(id) as qty
from user_withdraws
where status = '50000' and date(verified_at) > '2018-12-31' and date(verified_at) not in ('2019-10-02','2019-10-03')
group by date(verified_at), user_id having sum(fee) > 0)
UNION
(select date(pgp.created_at) as transaction_date, user_id, payment_group_id,
  'payment_gateway' as type, sum(amount) as total_amount,
  sum(fee) as total_fee, sum(pgp.cost) as total_cost, count(pgp.id) as qty
from payment_gateway_payments pgp
join payment_gateway_users pgu on pgp.pg_user_id = pgu.id
where status = '50000' and reconciled_at is not null and date(reconciled_at) > '2018-12-31' 
and date(reconciled_at) not in ('2019-10-02','2019-10-03') and date(pgp.created_at) not in ('2019-10-02','2019-10-03')
group by date(pgp.created_at), user_id, payment_group_id having sum(fee) > 0)) x
order by x.transaction_date asc, x.type asc"));

  $rr = [];
  $businessResellerTransactionRepo = app()->make(\Odeo\Domains\Marketing\Repository\BusinessResellerTransactionRepository::class);
  foreach ($result as $item) {
    $rr[] = [
      'transaction_date' => $item->transaction_date,
      'user_id' => $item->user_id,
      'payment_group_id' => $item->payment_group_id ? $item->payment_group_id : null,
      'type' => $item->type,
      'total_amount' => round($item->total_amount, 0),
      'total_fee' => round($item->total_fee, 0),
      'total_cost' => round($item->total_cost, 0),
      'qty' => $item->qty,
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ];
  }
  $businessResellerTransactionRepo->saveBulk($rr);
});

$app->get('cashh151', function(){
  $userRepo = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);

  $user = $userRepo->findById(205155);
  $user->transaction_pin = \Illuminate\Support\Facades\Hash::make('840712');
  $userRepo->save($user);
});

$app->get('chhh3333', function(){
  $data['body'] = '';
  $headerTimestamp = '2010123456';
  $data['bearer'] = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NzM0NjA1OTMsImlhdCI6MTU3MzQ1ODc5MywidWlkIjoxOTA0NTIsInVwZiI6MTAwLCJjaWQiOiJQZ1d2dHhZbXY0T3BVc0ExaHFIamVhUzRZeHRmWmhtSiIsInNjcCI6InBnX2dldF9wYXltZW50OnJlYWQifQ.CslU55PRBzz5IM6A_QcMtsU1pfhucTgrxy5vljNud3I';
  $hash = base64_encode(hash('sha256', $data['body'], true));
  $data['method'] = 'GET';
  $data['path'] = '/pg/v1/payment/162831';
  $rawSign = "{$data['method']}:{$data['path']}::{$data['bearer']}:{$headerTimestamp}:{$hash}";
  $signingKey = '2PfQEQqNXOWgYmT0wEUxs9iXKkRB0goRXlUPRxzvAufyoHwr0U0kMnWzYzJTwZMX';
  return base64_encode(hash_hmac('sha256', $rawSign, $signingKey, true));
});