<?php

$app->post('internal/enqueue', 'InternalController@enqueue');
$app->post('internal/disbursement', 'InternalController@createDisbursement');
$app->post('internal/reserve-disbursement', 'InternalController@reserveDisbursement');
$app->post('internal/execute-reserved-disbursement', 'InternalController@executeReservedDisbursement');
$app->post('internal/bank-account-inquiry', 'InternalController@createBankAccountInquiry');
$app->put('internal/va', 'InternalController@getOrCreateVA');
$app->post('internal/test-complete-va-payment', 'InternalController@testCompleteVAPayment');
$app->get('internal/affiliate-invoice-inquiry', 'InternalController@affiliateInvoiceInquiry');
$app->post('/internal/cancel-order', 'InternalController@cancelOrder');
$app->post('internal/cancel-order', 'InternalController@cancelOrder');
