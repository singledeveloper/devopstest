<?php


$app->get('/terminal/order/{order_id}', 'OrderController@terminalFindOrder');
$app->get('/terminal/payment/list', 'PaymentController@terminalGetList');
$app->get('/terminal/payment/list-wo-order', 'PaymentController@terminalGetListWoOrder');
$app->get('/terminal/receipt-config', 'UserReceiptConfigController@getConfig');
$app->post('/terminal/payment/request-open', 'PaymentController@terminalRequestOpenPayment');
$app->post('/terminal/payment/log-bni-sale-result', 'PaymentController@logBniSaleResult');
$app->post('/terminal/log-pax-result', 'TerminalActivityController@logPaxAppCallResult');
