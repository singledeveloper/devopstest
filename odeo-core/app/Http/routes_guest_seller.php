<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/flight', 'FlightController@index');
$app->post('/flight/search', 'FlightController@searchFlight');
$app->get('/flight/order', 'FlightController@getBookingForm');

$app->get('/hotel/search/autocomplete', 'HotelController@searchAutocomplete');
$app->post('/hotel/search', 'HotelController@searchHotel');
$app->get('/hotel/view', 'HotelController@getRoomList');

$app->get('/pulsa', 'PulsaController@searchNominal');
$app->get('/pulsa-data', 'PulsaController@searchNominalData');
$app->get('/pulsa-sms-telp-packages', 'PulsaController@searchNominalSmsTelpPackages');
$app->get('/bolt', 'BoltController@get');
$app->get('/pln', 'PlnController@get');
$app->get('/pln-postpaid', 'PlnController@getPostpaid');
$app->get('/postpaid', 'PostpaidSwitcherController@getPostpaidList');
$app->get('/pulsa-postpaid', 'PulsaController@getPostpaid');
$app->get('/pdam-postpaid', 'PDAMController@getPostpaid');
$app->get('/pgn-postpaid', 'PGNController@getPostpaid');
$app->get('/google-play', 'GameVoucherController@getGooglePlayList');
$app->get('/bpjs-kes', 'BPJSController@getInquiryKes');
//$app->get('/bpjs-tk', 'BPJSController@getInquiryKtg');
$app->get('/user-invoice', 'UserInvoiceController@invoiceInquiry');
$app->get('/transportation', 'TransportationController@searchNominal');
$app->get('/transportation/categories', 'TransportationController@getCategories');
$app->get('/transportation/amounts', 'TransportationController@getAmounts');
$app->get('/multi-finance', 'MultiFinanceController@getPostpaid');
$app->get('/game-voucher', 'GameVoucherController@searchNominal');
$app->get('/game-voucher/categories', 'GameVoucherController@getCategories');
$app->get('/game-voucher/amounts', 'GameVoucherController@getAmounts');
$app->get('/e-money/categories', 'EMoneyController@getCategories');
$app->get('/e-money/amounts', 'EMoneyController@getAmounts');
$app->post('/credit-bill/create', 'CreditBillController@createCreditBill');

$app->get('/cart', 'CartController@getCart');
$app->post('/cart/add', 'CartController@addCart');
$app->post('/cart/remove', 'CartController@removeCart');
$app->post('/cart/clear', 'CartController@clearAll');
$app->post('/cart/checkout', 'CartController@checkout');
$app->post('/cart/instant-checkout', 'CartController@instantCheckout');

$app->get('/order/double-purchase', 'OrderController@getDetectedDoublePurchaseOrder');
$app->post('/order/{order_id}/cancel', 'OrderController@cancel');
$app->get('/order/{order_id}', 'OrderController@findOrder');

$app->get('/payment/list', 'PaymentController@getList');
$app->post('/payment/request', 'PaymentController@requestPayment');

$app->post('/payment/open', 'PaymentController@openPayment');
$app->post('/payment/confirm', 'PaymentController@confirmPayment');
$app->post('/payment/cancel_confirm', 'PaymentController@cancelConfirmPayment');
$app->post('/payment/doku/submit', 'PaymentController@dokuSubmit');

$app->get('/price-list/list[/{storeId}]', 'PriceListController@getPriceList');
$app->get('/price-list[/{storeId}]', 'PriceListController@sendPricelist');

$app->post('/user/logout', 'UserController@logout');
