<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 10-Dec-18
 * Time: 6:44 PM
 */

namespace Odeo\Http\Controllers;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Payment\Prismalink\Helper\PrismalinkLibrary;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserRepository;

class PrismalinkPaymentTestController extends Controller {

  private $paymentGatewayUserRepo, $redis;

  public function __construct() {
    parent::__construct();
    $this->paymentGatewayUserRepo = app()->make(PaymentGatewayUserRepository::class);
    $this->redis = Redis::connection();
  }

  public function getTestPage($encodedClientId) {
    if (isProduction()) {
      return $this->buildErrorsResponse('', 404);
    }

    $clientId = base64_decode($encodedClientId);
    $pgUser = $this->paymentGatewayUserRepo->findByClientId($clientId);
    $prefixes = $pgUser->channels ? $pgUser->channels->pluck('prefix') : [];

    return view('pg-test', [
      'prefixes' => $prefixes
    ]);
  }

  public function notifyInquiry() {
    list($isValid, $data) = $this->validateData([
      'virtual_account_number' => 'required|numeric',
    ]);

    if (!$isValid) {
      return $data;
    }

    $data = [
      'code' => '01',
      'accountNo' => $data['virtual_account_number'],
      "partnerId" => env("PRISMALINK_PARTNER_ID")
    ];
    $prismalinkLibrary = app()->make(PrismalinkLibrary::class);
    $data['signature'] = $prismalinkLibrary->generateSignature($data);

    list($ok, $response) = $this->request(url() . '/v1/payment/prismalink/inquiry', $data);
    if (!$ok) {
      return $this->buildErrorsResponse('timeout');
    }
    $response = json_decode($response->getBody());

    $isSuccess = $response->responseCode === '00';
    if ($isSuccess) {
      return $this->buildSuccessResponse($response);
    }

    return $this->buildErrorsResponse($response);
  }

  private function request($url, $data) {
    $response = null;
    try {
      $client = new Client();
      $response = $client->request('post', $url, [
        'json' => $data,
        'timeout' => 10,
      ]);
    } catch (\Exception $e) {
      \Log::info(json_encode($e->getMessage()));
      \Log::info(json_encode($e->getTraceAsString()));
      return [false, $e->getMessage()];
    }

    return [true, $response];
  }

  public function notifyPayment() {
    list($isValid, $data) = $this->validateData([
      'virtual_account_number' => 'required',
      'amount' => 'required',
      'traceNo' => 'required'
    ]);

    if (!$isValid) {
      return $data;
    }

    $traceNo = $data['traceNo'];
    $data = [
      "accountNo" => $data['virtual_account_number'],
      "partnerId" => env("PRISMALINK_PARTNER_ID"),
      "feeAmount" => 1000,
      "amount" => $data['amount'],
      "netAmount" => $data['amount'] - 1000,
      "transDate" => Carbon::now()->format('YmdHis'),
      "traceNo" => $traceNo,
    ];

    $prismalinkLibrary = app()->make(PrismalinkLibrary::class);
    $data['signature'] = $prismalinkLibrary->generateSignature($data);
    $data['code'] = '02';
    $data['codeCurrency'] = '360';

    list($ok, $response) = $this->request(url() . '/v1/payment/prismalink/notify', $data);
    if (!$ok) {
      return $this->buildErrorsResponse('timeout');
    }

    $response = json_decode($response->getBody());

    $isSuccess = $response->responseCode === '00';
    if ($isSuccess) {
      $this->redis->set('odeo_pg_test_' . $traceNo, 0);
      $response->traceNo = $traceNo;

      return $this->buildSuccessResponse($response);
    }

    return $this->buildErrorsResponse($response);
  }

  public function notifyPaymentStatus() {
    list($isValid, $data) = $this->validateData([
      'trace_no' => 'required',
      'status' => 'required'
    ]);

    if (!$isValid) {
      return $data;
    }

    $this->redis->set('odeo_pg_test_' . $data['trace_no'], $data['status']);

    return $this->buildSuccessResponse($this->redis->get('odeo_pg_test_' . $data['trace_no']));
  }


  public function checkStatus() {
    if (isProduction()) {
      return null;
    }

    $redis = Redis::connection();
    $data = $this->getRequestData();

    $traceNo = $data['traceNo'];
    $expectedStatus = $redis->get('odeo_pg_test_' . $traceNo);

    if ($expectedStatus) {
      $redis->del('odeo_pg_test_' . $traceNo);
    }

    $resp = $this->statusToResponse($expectedStatus);

    clog('pg_test', 'from check status: ' . $resp);

    return $resp;
  }


  private function statusToResponse($status) {
    switch ($status) {
      case 50000:
        return response()->json([
          "status" => 'APRVD',
          'rc' => '00'
        ]);
      case 80000:
        return response()->json([
          "status" => 'ODEO_TEST',
          'rc' => '997'
        ]);
      case 90000:
        return response()->json([
          "status" => 'ODEO_TEST',
          'rc' => '998'
        ]);
      case 0:
        return response()->json([
          "status" => 'ODEO_TEST',
          'rc' => '999'
        ]);
      default:
        return response()->json([
          "status" => 'APRVD',
          'rc' => '00'
        ]);
    }
  }
}
