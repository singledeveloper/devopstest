<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Affiliate\ChannelLogRemover;
use Odeo\Domains\Affiliate\ChannelLogSelector;
use Odeo\Domains\Affiliate\ChannelLogUpdater;
use Odeo\Domains\Core\Task;

class ChannelLogController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function get() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(ChannelLogSelector::class, 'getAllLogs'));

    return $this->executeAndResponse($data);
  }

  public function update($channelLogId) {
    list($isValid, $data) = $this->validateData([]);
    if (!$isValid) return $data;

    $data['channel_log_id'] = $channelLogId;
    $this->pipeline->add(new Task(ChannelLogUpdater::class, 'updateLog'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }


  public function remove() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(ChannelLogRemover::class, 'removeLog'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

}