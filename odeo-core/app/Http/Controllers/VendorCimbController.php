<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/8/17
 * Time: 5:35 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BalanceSelector;
use Odeo\Domains\Vendor\Cimb\TransferRecordSelector;

class VendorCimbController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getDisbursementRecord() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(TransferRecordSelector::class, 'get'));
    return $this->executeAndResponse($data);

  }

  public function getInformation() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BalanceSelector::class, 'getBalance', [
      'id' => VendorDisbursement::CIMB
    ]));
    return $this->executeAndResponse($data);
  }

  public function getSuspectDisbursements() {
    $this->pipeline->add(new Task(TransferRecordSelector::class, 'getSuspectDisbursements'));
    return $this->executeAndResponse();
  }
}
