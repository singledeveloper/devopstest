<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Inventory\MultiFinance\MultiFinanceManager;

class MultiFinanceController extends Controller {

  public function getPostpaid() {

    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:service_detail_id,store_id',
      'service_detail_id' => 'required_unless:default_price,true',
      'store_id' => 'required_unless:default_price,true|exists:stores,id',
      'number' => 'required'
    ]);

    if (!$isValid) return $data;

    if(isset($data['default_price']) && $data['default_price']) {
      $data['service_detail_id'] = ServiceDetail::MULTI_FINANCE_ODEO;
    }

    $this->pipeline->add(new Task(MultiFinanceManager::class, 'validatePostpaidInventory', ["for_bill" => true]));
    return $this->executeAndResponse($data);
  }

}
