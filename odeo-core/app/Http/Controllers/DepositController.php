<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/9/16
 * Time: 12:30 AM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Campaign\DepositCashback\Helper\PresetGenerator;
use Odeo\Domains\Campaign\DepositCashback\PresetSelector;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Subscription\StoreVerificator;
use Odeo\Domains\Transaction\DepositRequester;
use Odeo\Domains\Transaction\DepositSelector;

class DepositController extends Controller {
  private $presetGenerator;

  public function __construct() {
    parent::__construct();
    $this->presetGenerator = app()->make(PresetGenerator::class);
  }

  public function index($storeId) {

    $data = $this->getRequestData();

    $data["expand"] = "order";
    $data['store_id'] = $storeId;

    if (isset($data['has_pending_deposit'])) {

      $this->pipeline->add(new Task(DepositSelector::class, 'getDetail'));

      return $this->executeAndResponse($data);
    }

    return $this->buildErrorsResponse();
  }

  public function createDeposit($storeId) {
    list($isValid, $data) = $this->validateData([
      'deposit.amount' => 'required|numeric|min:10000|max:1000000000',
      'deposit.currency' => 'required',
    ]);

    if (!$isValid) {
      return $data;
    }

    $data['store_id'] = $storeId;

    if (isset($data['preset_index'])) {
      $bonus = $this->presetGenerator->presetAmount($storeId, $data['preset_index']);
      $data['preset'] = $data['preset_index'];
      $data['bonus'] = $bonus['get_amount'] - $bonus['topup_amount'];
      $data['topup_amount'] = $bonus['topup_amount'];
    }

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true,
      'not_available_for_free_store' => true
    ]));
    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(DepositRequester::class, 'request'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::DEPOSIT_ODEO]));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }
}
