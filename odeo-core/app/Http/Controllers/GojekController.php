<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Biller\Gojek\AccountRequester;
use Odeo\Domains\Biller\Gojek\AccountSelector;
use Odeo\Domains\Biller\Gojek\HistoryScrapper;
use Odeo\Domains\Core\Task;

class GojekController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function requestOtp() {
    list($isValid, $data) = $this->validateData([
      'telephone' => 'required',
      'store_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'requestOtp'));
    return $this->executeAndResponse($data);
  }

  public function generateToken() {
    list($isValid, $data) = $this->validateData([
      'gojek_user_id' => 'required',
      'otp' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'generateToken'));
    return $this->executeAndResponse($data);
  }

  public function setupPin() {
    list($isValid, $data) = $this->validateData([
      'gojek_user_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'setupPin'));
    return $this->executeAndResponse($data);
  }

  public function resetUniqueId() {
    list($isValid, $data) = $this->validateData([
      'gojek_user_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'resetUniqueId'));
    return $this->executeAndResponse($data);
  }

  public function checkBalance() {
    list($isValid, $data) = $this->validateData([
      'gojek_user_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'checkBalance'));
    return $this->executeAndResponse($data);
  }

  public function changeTelephone() {
    list($isValid, $data) = $this->validateData([
      'gojek_user_id' => 'required',
      'new_telephone' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'changeTelephone'));
    return $this->executeAndResponse($data);
  }

  public function verifyChangeTelephone() {
    list($isValid, $data) = $this->validateData([
      'gojek_user_id' => 'required',
      'new_telephone' => 'required',
      'vcode' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'verifyChangeTelephone'));
    return $this->executeAndResponse($data);
  }

  public function updatePin() {
    list($isValid, $data) = $this->validateData([
      'gojek_user_id' => 'required',
      'new_pin' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'updatePin'));
    return $this->executeAndResponse($data);
  }

  public function remove() {
    list($isValid, $data) = $this->validateData([
      'gojek_user_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'remove'));
    return $this->executeAndResponse($data);
  }

  public function getAccounts() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(AccountSelector::class, 'getAccounts'));
    return $this->executeAndResponse($data);
  }

  public function getHistories() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(HistoryScrapper::class, 'getHistories'));
    return $this->executeAndResponse($data);
  }

  public function getSummaries() {
    $this->pipeline->add(new Task(AccountSelector::class, 'getSummaries'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function test() {
    list($isValid, $data) = $this->validateData([
      'gojek_user_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'test'));
    return $this->executeAndResponse($data);
  }

}
