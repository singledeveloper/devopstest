<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/3/17
 * Time: 22:47
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\DistributionChannel\ChannelSelector;
use Odeo\Domains\Subscription\DistributionChannel\ChannelUpdater;
use Odeo\Domains\Subscription\StoreVerificator;

class DistributionChannelController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function get($storeId) {

    $data = $this->getRequestData();
    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(ChannelSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function toggle($storeId) {

    $data = $this->getRequestData();
    $data['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_must_active' => true
    ]));
    $this->pipeline->add(new Task(ChannelUpdater::class, 'updateStatus'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);


  }

}