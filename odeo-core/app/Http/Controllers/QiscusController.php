<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Support\Qiscus\QiscusAuthRequester;
use Odeo\Domains\Support\Qiscus\QiscusUserSelector;
use Odeo\Domains\Support\Qiscus\QiscusUserUpdater;
use Odeo\Domains\Core\Task;

class QiscusController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function qiscusJwtAuthentication() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(QiscusAuthRequester::class, 'getJwtToken'));
    $this->pipeline->execute($data);

    if($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    return $this->buildSuccessResponse($this->pipeline->data);
  }

  public function getCsOdeoUserIds() {
    $this->pipeline->add(new Task(QiscusUserSelector::class, 'getCsOdeoUserIds'));
    $this->pipeline->execute([]);

    if($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    return $this->buildSuccessResponse($this->pipeline->data);
  }
  
  public function recordQiscusAccount() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(QiscusUserUpdater::class, 'updateQiscusAccount'));
    $this->pipeline->execute($data);

    if($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    return $this->buildSuccessResponse($this->pipeline->data);
  }

  public function getChatNotification() {
    return $this->buildSuccessResponse(['message' => trans('misc.chat_notification')]);
  }

}
