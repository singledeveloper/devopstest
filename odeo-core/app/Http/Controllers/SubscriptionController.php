<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Account\UserVerificator;
use Odeo\Domains\Subscription\DepositSelector;
use Odeo\Domains\Subscription\StoreSelector;
use Odeo\Domains\Subscription\StoreRequester;
use Odeo\Domains\Subscription\StoreUpdater;
use Odeo\Domains\Subscription\DomainVerificator;
use Odeo\Domains\Subscription\StoreVerificator;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Network\TreeRequester;
use Odeo\Domains\Marketing\BudgetUpdater;
use Odeo\Domains\Network\ReferralRequester;
use Odeo\Domains\Network\SponsorRequester;
use Odeo\Domains\Network\CommissionRequester;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Transaction\TempDepositSelector;

class SubscriptionController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function checkSubdomain($subdomain) {
    $data["subdomain_name"] = $subdomain;

    list($isValid, $data) = $this->validateData([
      'subdomain_name' => 'regex:/[A-Za-z0-9](?:[A-Za-z0-9\-]{0,61}[A-Za-z0-9])?/|min:5|max:50'
    ], $data);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(DomainVerificator::class, 'checkSubdomain'));
    return $this->executeAndResponse($data);
  }

  public function create() {
    list($isValid, $data) = $this->validateData([
      'plan_id' => 'required|exists:plans,id',
      'name' => 'required',
      'subdomain_name' => 'required|regex:/[A-Za-z0-9](?:[A-Za-z0-9\-]{0,61}[A-Za-z0-9])?/|min:5|max:50'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(DomainVerificator::class, 'checkSubdomain'));
    $this->pipeline->add(new Task(DomainVerificator::class, 'checkInvitationCode'));
    $this->pipeline->add(new Task(StoreRequester::class, 'create'));
    $this->pipeline->add(new Task(TreeRequester::class, 'update'));
    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::PLAN_ODEO]));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function createFree() {
    list($isValid, $data) = $this->validateData([
      'store_name' => 'required',
      'phone_number' => 'required',
      'subdomain_name' => 'required|regex:/[A-Za-z0-9](?:[A-Za-z0-9\-]{0,61}[A-Za-z0-9])?/|min:5|max:50'
    ]);
    if (!$isValid) return $data;

    $data["plan_id"] = app()->make(\Odeo\Domains\Subscription\Repository\PlanRepository::class)->findByAttributes('name', 'FREE')->id;
    $data["name"] = $data["store_name"];

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(UserVerificator::class, 'guardTelephone'));
    $this->pipeline->add(new Task(DomainVerificator::class, 'checkSubdomain'));
    $this->pipeline->add(new Task(DomainVerificator::class, 'checkInvitationCode'));
    $this->pipeline->add(new Task(StoreRequester::class, 'create'));
    $this->pipeline->add(new Task(TreeRequester::class, 'update'));
    $this->pipeline->add(new Task(StoreVerificator::class, 'verify'));
    $this->pipeline->add(new Task(ReferralRequester::class, 'create'));
    $this->pipeline->add(new Task(SponsorRequester::class, 'create'));
    $this->pipeline->add(new Task(CommissionRequester::class, 'create'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function verify() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(StoreVerificator::class, 'verify'));
    $this->pipeline->add(new Task(BudgetUpdater::class, 'updateBudget'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function change() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'plan_id' => 'required|exists:plans,id'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', ['store_must_active' => true]));
    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(StoreRequester::class, 'changePlan'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::PLAN_ODEO]));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function renewal() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', ['must_status' => [StoreStatus::OK, StoreStatus::EXPIRED, StoreStatus::WAIT_FOR_RENEWAL_VERIFY, StoreStatus::WAIT_FOR_EXP_RENEWAL_VERIFY]]));
    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(StoreRequester::class, 'renew'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::PLAN_ODEO]));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function terminate() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(StoreRequester::class, 'terminate'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getSettings($storeId) {
    $data = $this->getRequestData();
    $data["fields"] = "id,logo_path,favicon_path";
    $data["store_id"] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(StoreSelector::class, 'getStoreDetail'));
    return $this->executeAndResponse($data);
  }

  public function updateSettings() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'service_detail_id' => 'exists:service_details,id',
      'active' => 'integer|in:0,1',
      'discount' => 'numeric|max:100|min:0',
      'phone_number' => 'regex:/^[1-9][0-9]*$/|min:8|max:16',
      'banner' => 'json'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(StoreUpdater::class, 'update'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }


  public function getDeposit() {
    $data = $this->getRequestData();
    $data['expand'] = 'total_deposit';

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(DepositSelector::class, 'get'));
    return $this->executeAndResponse($data);

  }

  public function getLockedDeposit() {
    $data = $this->getRequestData();
    $data['expand'] = 'total_deposit,order,user';

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(TempDepositSelector::class, 'getLockedDeposit'));

    return $this->executeAndResponse($data);

  }

}
