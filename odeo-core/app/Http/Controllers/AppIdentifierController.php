<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/2/17
 * Time: 4:46 PM
 */
namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Appidentifier\AppIdentifierRegistrar;
use Odeo\Domains\Core\Task;

class AppIdentifierController extends Controller {
    
  public function __construct() {
    parent::__construct();
  }

  public function register() {

    $this->pipeline->add(new Task(AppIdentifierRegistrar::class, 'register'));
    return $this->executeAndResponse($this->getRequestData());

  }

}