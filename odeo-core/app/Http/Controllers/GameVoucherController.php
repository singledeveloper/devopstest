<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Inventory\GameVoucher\GameVoucherManager;
use Odeo\Domains\Inventory\GooglePlay\GooglePlayManager;

class GameVoucherController extends Controller {

  public function searchNominal() {
    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:service_detail_id,store_id',
      'service_detail_id' => 'required_unless:default_price,true',
      'store_id' => 'required_unless:default_price,true|exists:stores,id'
    ]);

    if (!$isValid) return $data;

    if (isset($data['default_price']) && $data['default_price']) {
      $data['service_detail_id'] = ServiceDetail::GAME_VOUCHER_ODEO;
    }

    $this->pipeline->add(new Task(GameVoucherManager::class, 'searchNominal'));

    return $this->executeAndResponse($data);

  }

  public function getTnC() {
    $this->pipeline->add(new Task(GameVoucherManager::class, 'getTermAndCon'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getCategories() {
    $data = $this->getRequestData();
    $data['service_detail_id'] = ServiceDetail::GAME_VOUCHER_ODEO;
    $this->pipeline->add(new Task(GameVoucherManager::class, 'getAllCategoryByServiceDetailId'));
    return $this->executeAndResponse($data);
  }

  public function getAmounts() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required|exists:stores,id',
      'category' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['service_detail_id'] = ServiceDetail::GAME_VOUCHER_ODEO;

    $this->pipeline->add(new Task(GameVoucherManager::class, 'searchNominal'));

    return $this->executeAndResponse($data);
  }

  public function getGooglePlayList() {

    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:store_id',
      'store_id' => 'required_unless:default_price,true|exists:stores,id'
    ]);

    if (!$isValid) return $data;

    $data['service_detail_id'] = ServiceDetail::GOOGLE_PLAY_ODEO;
    $this->pipeline->add(new Task(GooglePlayManager::class, 'searchNominal'));
    $this->pipeline->add(new Task(GooglePlayManager::class, 'getMarketingCopyList'));
    return $this->executeAndResponse($data);
  }

  public function getGooglePlayTnC() {
    $this->pipeline->add(new Task(GooglePlayManager::class, 'getTermAndCon'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getGooglePlayRedeemInstructions() {
    $this->pipeline->add(new Task(GooglePlayManager::class, 'getRedeemInstructionList'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function test() {
    $this->pipeline->add(new Task(\Odeo\Domains\Biller\Unipin\UnipinManager::class, 'inquiry'));
    return $this->executeAndResponse();
  }

}
