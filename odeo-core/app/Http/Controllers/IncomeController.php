<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Marketing\IncomeSelector;
use Odeo\Domains\Core\Task;

class IncomeController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function income() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(IncomeSelector::class, 'get', [
      'day_num' => 10
    ]));
    return $this->executeAndResponse($data);
  }

}
