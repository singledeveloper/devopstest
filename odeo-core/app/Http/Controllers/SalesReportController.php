<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/27/16
 * Time: 11:36 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\Salesreport\Helper\Initializer;
use Odeo\Domains\Order\Salesreport\Ocommerce\SalesOcommerceReportSelector;
use Odeo\Domains\Order\Salesreport\Platform\SalesPlatformReportSelector;
use Odeo\Domains\Order\Salesreport\Bypayment\SalesByPaymentReportSelector;

class SalesReportController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getSalesOcommerceReport() {

    $data = $this->getRequestData();

    $this->parseQuerySearch($data);

    $data['is_paginate'] = false;

    $this->pipeline->add(new Task(SalesOcommerceReportSelector::class, 'getSalesOcommerceReport', [
      'day_num' => Initializer::DAY_NUM
    ]));

    return $this->executeAndResponse($data);

  }

  public function getSalesPlatformReport() {

    $data = $this->getRequestData();

    $this->parseQuerySearch($data);

    $data['is_paginate'] = false;

    $this->pipeline->add(new Task(SalesPlatformReportSelector::class, 'getSalesPlatformReport', [
      'day_num' => Initializer::DAY_NUM
    ]));

    return $this->executeAndResponse($data);

  }

  public function getSalesByPaymentReport() {

    $data = $this->getRequestData();

    $this->parseQuerySearch($data);

    $data['is_paginate'] = false;

    $this->pipeline->add(new Task(SalesByPaymentReportSelector::class, 'getSalesByPaymentReport', [
      'day_num' => Initializer::DAY_NUM
    ]));

    return $this->executeAndResponse($data);

  }

}