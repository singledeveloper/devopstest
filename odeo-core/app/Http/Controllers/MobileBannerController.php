<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/6/17
 * Time: 4:44 PM
 */
namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Banner\Mobile\AdminBannerSelector;
use Odeo\Domains\Banner\Mobile\BannerInserter;
use Odeo\Domains\Banner\Mobile\BannerRemover;
use Odeo\Domains\Banner\Mobile\BannerSelector;
use Odeo\Domains\Banner\Mobile\BannerUpdater;
use Odeo\Domains\Core\Task;

class MobileBannerController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getByAdmin() {

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(AdminBannerSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function get() {

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(BannerSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function insert() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(BannerInserter::class, 'insert'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

  public function remove() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(BannerRemover::class, 'remove'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

  public function activate($bannerId) {

    $data = $this->getRequestData();
    $data['banner_id'] = $bannerId;

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(BannerUpdater::class, 'activate'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

}