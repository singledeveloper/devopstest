<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/06/18
 * Time: 18.58
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Accounting\CompanyTransactionInformation\TransactionInformationRemover;
use Odeo\Domains\Accounting\CompanyTransactionInformation\TransactionInformationSelector;
use Odeo\Domains\Accounting\CompanyTransactionInformation\TransactionInformationUpdater;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Odeo\CompanyCash\CompanyCashAccountSelector;
use Odeo\Domains\Payment\Odeo\CompanyCash\CompanyCashTransactionSelector;
use Odeo\Domains\Payment\Odeo\CompanyCash\CompanyCashTransactionUpdater;

class CompanyCashController extends Controller {
  public function __construct() {
    parent::__construct();
  }

  public function getAccount() {

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(CompanyCashAccountSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function getTransactions($id) {

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $data['id'] = $id;

    $this->pipeline->add(new Task(CompanyCashTransactionSelector::class, 'getTransactions'));

    return $this->executeAndResponse($data);
  }

  public function updateReference($id) {
    $data =  $this->getRequestData();
    $data['id'] = $id;

    $this->pipeline->add(new Task(CompanyCashTransactionUpdater::class, 'updateTransactionReference'));

    return $this->executeAndResponse($data);
  }

  public function createTransactionInformation($id) {

    list($isValid, $data) = $this->validateData([
      'file' => 'max:102400'
    ]);
    if (!$isValid) return $data;

    $data['transaction_id'] = $id;

    $this->pipeline->add(new Task(TransactionInformationUpdater::class, 'updateTransactionInformation'));

    return $this->executeAndResponse($data);
  }

  public function exportTransaction($id) {

    $data = $this->getRequestData();

    $this->parseQuerySearch($data);
    $data['id'] = $id;

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse();
    }

    return (new CompanyCashTransactionSelector())->exportTransaction($data);
  }

  public function getTransactionInformation($cashTransactionId) {
    $data = $this->getRequestData();
    $data['cash_transaction_id'] = $cashTransactionId;
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(TransactionInformationSelector::class, 'getTransactionInformation'));
    return $this->executeAndResponse($data);
  }

  public function removeDetail($detailId) {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $data['id'] = $detailId;
    $this->pipeline->add(new Task(TransactionInformationRemover::class, 'remove'));
    return $this->executeAndResponse($data);
  }

}