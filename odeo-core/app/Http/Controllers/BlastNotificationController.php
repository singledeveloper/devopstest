<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/20/17
 * Time: 12:16 AM
 */
namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Notification\Blast\AllBlaster;
use Odeo\Domains\Notification\Blast\UserBlaster;

class BlastNotificationController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function blastToUser() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(UserBlaster::class, 'blast'));
    $this->executeAndResponse($data);
  }

  public function blastToAll() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(AllBlaster::class, 'blast'));
    $this->executeAndResponse($data);
  }

}