<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 03/07/19
 * Time: 16.54
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\DisbursementApiUserUpdater;
use Odeo\Domains\Invoice\BusinessInvoiceUserRequester;
use Odeo\Domains\OAuth2\Helper\OAuth2ClientHelper;
use Odeo\Domains\OAuth2\OAuth2ClientSelector;
use Odeo\Domains\OAuth2\OAuth2ClientUpdater;
use Odeo\Domains\PaymentGateway\PaymentGatewayUserUpdater;

class OAuth2ClientController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function checkOAuth2Client() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(OAuth2ClientSelector::class, 'checkUserOAuth2Client'));
    return $this->executeAndResponse($data);
  }

  public function getOAuth2Client() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(OAuth2ClientSelector::class, 'getUserOAuth2Client'));
    return $this->executeAndResponse($data);
  }

  public function createOAuth2Client() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(OAuth2ClientUpdater::class, 'createUserOAuth2Client'));
    return $this->executeAndResponse($data);
  }

  public function createPaymentGatewayUser() {
    $data = $this->getRequestData();
    $data['scope'] = OAuth2ClientHelper::SCOPE_PG;

    $this->pipeline->add(new Task(OAuth2ClientUpdater::class, 'updateScope'));
    $this->pipeline->add(new Task(PaymentGatewayUserUpdater::class, 'createPaymentGatewayUser'));
    return $this->executeAndResponse($data);
  }

  public function createDisbursementApiUser() {
    $data = $this->getRequestData();
    $data['scope'] = OAuth2ClientHelper::SCOPE_DG;

    $this->pipeline->add(new Task(OAuth2ClientUpdater::class, 'updateScope'));
    $this->pipeline->add(new Task(DisbursementApiUserUpdater::class, 'createDisbursementApiUser'));
    return $this->executeAndResponse($data);
  }

  public function generateSecret() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(OAuth2ClientUpdater::class, 'generateSecret'));
    return $this->executeAndResponse($data);
  }

  public function generateSigningKey() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(OAuth2ClientUpdater::class, 'generateSigningKey'));
    return $this->executeAndResponse($data);
  }

  public function createUserInvoiceUser() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BusinessInvoiceUserRequester::class, 'request'));
    return $this->executeAndResponse($data);
  }

}