<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\Competitor\CompetitorPriceInserter;
use Odeo\Domains\Marketing\Competitor\CompetitorPriceSelector;

class CompetitorPriceController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function create() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(CompetitorPriceInserter::class, 'bulkInsert'));
    return $this->executeAndResponse($data);
  }

  public function get() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(CompetitorPriceSelector::class, 'getAll'));

    return $this->executeAndResponse($data);
  }

  public function getNames() {
    $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(CompetitorPriceSelector::class, 'getAllNames'));

    return $this->executeAndResponse($data);
  }

  public function getPriceComparison() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(CompetitorPriceSelector::class, 'getPriceComparison'));

    return $this->executeAndResponse($data);
  }

}