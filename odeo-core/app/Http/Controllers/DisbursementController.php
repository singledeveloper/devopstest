<?php

namespace Odeo\Http\Controllers;

use Illuminate\Validation\Rule;
use Odeo\Domains\Account\Helper\CustomerEmailManager;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementAdminSelector;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementCallbackRequester;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementGuard;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementInquiryAdminSelector;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementInquirySelector;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementInquirySummarySelector;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementNotifyLogSelector;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementSelector;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementSummarySelector;
use Odeo\Domains\Disbursement\ApiDisbursement\DisbursementApiUserSelector;
use Odeo\Domains\Disbursement\ApiDisbursement\DisbursementApiUserUpdater;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\NotifyApiDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\MarkAsCancelledRequester;
use Odeo\Domains\Disbursement\ApiDisbursement\MarkAsSuccessRequester;
use Odeo\Domains\Disbursement\ApiDisbursement\RetryRequester;

class DisbursementController extends Controller {

  public function getDisbursements() {
    list($valid, $data) = $this->validateData([
      'id' => 'integer',
      'start_date' => 'date',
      'end_date' => 'date',
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(ApiDisbursementSelector::class, 'get', [
      'expand' => 'bank',
    ]));

    return $this->executeAndResponse($data);
  }

  public function exportDisbursement() {
    list($isValid, $data) = $this->validateData([
      'start_date' => 'required|date',
      'end_date' => 'required|date'
    ]);

    if (!$isValid) return $data;

    $data['sort_by'] = 'disbursement.id';
    $data['sort_type'] = 'desc';
    $this->pipeline->add(new Task(ApiDisbursementSelector::class, 'export'));

    return $this->executeAndResponse($data);
  }

  public function exportInvoice() {
    list($isValid, $data) = $this->validateData([
      'periode' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ApiDisbursementSelector::class, 'exportInvoice'));

    return $this->executeAndResponse($data);
  }

  public function getDisbursementInquiries() {
    $this->pipeline->add(new Task(ApiDisbursementInquirySelector::class, 'get'));

    return $this->executeAndResponse($this->getRequestData([
      'expand' => 'bank'
    ]));
  }

  public function exportDisbursementInquiry() {
    list($isValid, $data) = $this->validateData([
      'start_date' => 'required|date',
      'end_date' => 'required|date'
    ]);

    if (!$isValid) return $data;

    $data['sort_by'] = 'disbursement.id';
    $data['sort_type'] = 'desc';
    $this->pipeline->add(new Task(CustomerEmailManager::class, 'checkIsEmailVerified'));
    $this->pipeline->add(new Task(ApiDisbursementInquirySelector::class, 'export'));

    return $this->executeAndResponse($data);
  }

  public function getFilterContents() {
    $data['status'] = [
      ['code' => ApiDisbursement::PENDING, 'message' => 'Pending'],
      ['code' => ApiDisbursement::COMPLETED, 'message' => 'Completed'],
      ['code' => ApiDisbursement::SUSPECT, 'message' => 'Suspect'],
      ['code' => ApiDisbursement::FAILED, 'message' => 'Failed'],
      ['code' => ApiDisbursement::FAILED_WRONG_ACCOUNT_NUMBER, 'message' => 'Failed wrong account number'],
      ['code' => ApiDisbursement::FAILED_CLOSED_BANK_ACCOUNT, 'message' => 'Failed closed bank account'],
      ['code' => ApiDisbursement::FAILED_BANK_REJECTION, 'message' => 'Failed bank rejection'],
      ['code' => ApiDisbursement::FAILED_VENDOR_DOWN, 'message' => 'Failed vendor down'],
      ['code' => ApiDisbursement::FAILED_DUPLICATE_REQUEST, 'message' => 'Failed duplicate request'],
    ];

    return $this->buildResponse(200, $data);
  }

  public function getDisbursementsForAdmin() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data["expand"] = "user,cash,bank";

    $this->pipeline->add(new Task(ApiDisbursementAdminSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function getDisbursementInquiriesForAdmin() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data["expand"] = "user,cash,bank";

    $this->pipeline->add(new Task(ApiDisbursementInquiryAdminSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function retry() {
    list($isValid, $data) = $this->validateData([
      'password' => 'required',
      'disbursement_id' => 'required',
      'preferred_vendor' => 'nullable|numeric',
    ]);

    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(RetryRequester::class, 'retry'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function bulkRetry() {
    list($isValid, $data) = $this->validateData([
      'password' => 'required',
      'disbursement_ids' => 'required',
      'preferred_vendor' => 'nullable|numeric'
    ]);
    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(RetryRequester::class, 'bulkRetry'));

    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function markAsCancel() {
    list($isValid, $data) = $this->validateData([
      'password' => 'required',
      'disbursement_id' => 'required',
      'disbursement_status' => Rule::in(ApiDisbursement::FAILED_STATUS_LIST),
    ]);

    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(MarkAsCancelledRequester::class, 'markAsCancel'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function markAsSuccess() {
    list($isValid, $data) = $this->validateData([
      'password' => 'required',
      'disbursement_id' => 'required',
    ]);

    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(MarkAsSuccessRequester::class, 'markAsSuccess'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }


  public function notify() {
    list ($isValid, $data) = $this->validateData([
      'disbursement_id' => 'required',
    ]);

    if (!$isValid) {
      return $data;
    }

    $this->pipeline->pushQueue(new NotifyApiDisbursement($data['disbursement_id']));
    return $this->executeAndResponse($data);
  }

  public function getVendorRatio() {
    $this->pipeline->add(new Task(ApiDisbursementAdminSelector::class, 'getVendorRatio'));
    return $this->executeAndResponse();
  }

  public function getSuspectCount() {
    $this->pipeline->add(new Task(ApiDisbursementAdminSelector::class, 'getSuspectCount'));
    return $this->executeAndResponse();
  }

  public function getSummary() {
    list($valid, $data) = $this->validateData([
      'start_date' => 'date',
      'end_date' => 'date',
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(ApiDisbursementSummarySelector::class, 'getSummary'));
    return $this->executeAndResponse($data);
  }

  public function getInquirySummary() {
    list($valid, $data) = $this->validateData([
      'start_date' => 'date',
      'end_date' => 'date',
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(ApiDisbursementInquirySummarySelector::class, 'getInquirySummary'));
    return $this->executeAndResponse($data);
  }

  public function getSettings() {
    $this->pipeline->add(new Task(DisbursementApiUserSelector::class, 'get'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function generateSecretKey() {
    $this->pipeline->add(new Task(DisbursementApiUserUpdater::class, 'generateSecretKey'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function generateSigningKey() {
    $this->pipeline->add(new Task(DisbursementApiUserUpdater::class, 'generateSigningKey'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function updateWhitelist() {
    list($valid, $data) = $this->validateData([
      'whitelist.*' => 'filled|ip',
    ]);
    if (!$valid) {
      return $data;
    }
    $this->pipeline->add(new Task(DisbursementApiUserUpdater::class, 'updateWhitelist'));
    return $this->executeAndResponse($data);
  }

  public function getV2DisbursementUser() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(DisbursementApiUserSelector::class, 'getV2DisbursementUser'));
    return $this->executeAndResponse($data);
  }

  public function updateCallback() {
    list($valid, $data) = $this->validateData([
      'url' => 'url',
    ]);
    if (!$valid) {
      return $data;
    }
    $this->pipeline->add(new Task(DisbursementApiUserUpdater::class, 'updateCallback'));
    return $this->executeAndResponse($data);
  }

  public function updateEmail() {
    list($valid, $data) = $this->validateData([
      'email' => 'required',
    ]);
    if (!$valid) return $data;
    $this->pipeline->add(new Task(DisbursementApiUserUpdater::class, 'updateEmail'));
    return $this->executeAndResponse($data);
  }

  public function testCallback() {
    $this->pipeline->add(new Task(ApiDisbursementCallbackRequester::class, 'testCallback'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getDashboard() {
    $this->pipeline->add(new Task(ApiDisbursementSummarySelector::class, 'getSummary'));
    $this->pipeline->add(new Task(ApiDisbursementSummarySelector::class, 'getTopBanks'));
    $this->pipeline->add(new Task(ApiDisbursementSelector::class, 'get', [
      'limit' => 5,
      'expand' => 'bank',
      'hide_metadata' => true,
    ]));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getSummaryByPeriod($period) {
    list ($valid, $data) = $this->validateData([
      'period' => 'required|in:daily,weekly,monthly'
    ], [
      'period' => $period,
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(ApiDisbursementSummarySelector::class, 'getSummaryByPeriod'));
    return $this->executeAndResponse($data);
  }

  public function getDisbursementNotifyLogs($disbursementId) {
    $this->pipeline->add(new Task(ApiDisbursementGuard::class, 'guardByAuthUser'));
    $this->pipeline->add(new Task(ApiDisbursementNotifyLogSelector::class, 'listByDisbursementId'));
    return $this->executeAndResponse($this->getRequestData(['disbursement_id' => $disbursementId]));
  }

  public function getDisbursementById($id) {
    $data = $this->getRequestData();
    $data['disbursement_id'] = $id;
    $data['expand'] = 'bank';
    $this->pipeline->add(new Task(ApiDisbursementSelector::class, 'findById'));
    return $this->executeAndResponse($data);
  }
}
