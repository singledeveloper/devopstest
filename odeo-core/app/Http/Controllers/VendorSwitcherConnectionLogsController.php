<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Pulsa\VendorSwitcherConnectionLogs\VendorSwitcherConnectionLogsSelector;

class VendorSwitcherConnectionLogsController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function get() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(VendorSwitcherConnectionLogsSelector::class, 'getAll'));

    return $this->executeAndResponse($data);
  }

}