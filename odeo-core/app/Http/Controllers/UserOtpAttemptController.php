<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Account\UserOtpAttemptSelector;
use Odeo\Domains\Account\UserOtpAttemptUpdater;

class UserOtpAttemptController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function get() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(UserOtpAttemptSelector::class, 'getAll'));

    return $this->executeAndResponse($data);
  }

  public function update($otpAttemptId) {
    $data = $this->getRequestData();

    $data['otp_attempt_id'] = $otpAttemptId;
    $this->pipeline->add(new Task(UserOtpAttemptUpdater::class, 'update'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

}