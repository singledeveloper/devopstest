<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\BankAccountCreator;
use Odeo\Domains\Account\BankAccountTransformer;
use Odeo\Domains\Account\BankAccountUpdater;
use Odeo\Domains\Account\BankAccountRemover;
use Odeo\Domains\Account\BankAccountSelector;
use Odeo\Domains\Account\BankAccountVerificator;
use Odeo\Domains\Account\BankSelector;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\Task;

class BankController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function index() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BankSelector::class, 'get'));
    return $this->executeAndResponse($data)
      ->header('Cache-control', 'max-age=43200, public');
  }

  public function bankAccounts() {
    $data = $this->getRequestData();
    $data["fields"] = "id,account_name,account_number,bank_id,is_own_account,alias";
    $data['expand'] = 'bank';
    $this->pipeline->add(new Task(BankAccountSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function createBankAccount() {
    list($isValid, $data) = $this->validateData([
      'bank_id' => 'required|exists:banks,id',
      'account_number' => 'required',
      'account_name' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['account_number'] = sanitizeNumber($data['account_number']);

    //$this->pipeline->add(new Task(BankAccountTransformer::class, 'intercept'));
    if (!isVersionSatisfy(Platform::ANDROID, '3.1.1') && !isVersionSatisfy(Platform::IOS, '3.1.1')) {
      $this->pipeline->add(new Task(BankAccountVerificator::class, 'verify'));
    }
    $this->pipeline->add(new Task(BankAccountCreator::class, 'create'));
    return $this->executeAndResponse($data);
  }

  public function updateBankAccount() {
    list($isValid, $data) = $this->validateData([
      'bank_account_id' => 'required|exists:user_bank_accounts,id',
      'bank_id' => 'required|exists:banks,id',
      'account_number' => 'required',
      'account_name' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['account_number'] = sanitizeNumber($data['account_number']);

    //$this->pipeline->add(new Task(BankAccountTransformer::class, 'intercept'));
    $this->pipeline->add(new Task(BankAccountVerificator::class, 'verify'));
    $this->pipeline->add(new Task(BankAccountUpdater::class, 'update'));
    return $this->executeAndResponse($data);
  }

  public function deleteBankAccount() {
    list($isValid, $data) = $this->validateData([
      'bank_account_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BankAccountRemover::class, 'remove'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function checkBankAccount() {
    list($isValid, $data) = $this->validateData([
      'bank_id' => 'required|exists:banks,id',
      'account_number' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['account_number'] = sanitizeNumber($data['account_number']);

    $this->pipeline->add(new Task(BankAccountVerificator::class, 'verify'));
    return $this->executeAndResponse($data);
  }
}
