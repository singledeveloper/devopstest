<?php

return [
  'description_enable_md_plus_feature' => 'We need to verify your identity to enable this feature. If you have uploaded your KTP but see this page, please wait for admin verification in 2x24 hours.',
  'description_md_plus_feature' => 'Share referral code, invite friends, and get cashback for each transaction',
  'status_md_plus_active' => 'Active',
  'status_md_plus_inactive' => 'Inactive',

  'description_1' => 'Bagikan kode referal Anda dan ajak agen Anda untuk memasukkan kode referal Anda pada menu Kelola Master',
  'description_2' => 'Setiap kali agen yang Anda ajak melakukan transaksi, Anda akan mendapatkan cashback dalam bentuk oCash.',
  'description_3' => 'Besar cashback yang didapatkan tergantung dari produk yang dibeli yaitu:',
  'description_4' => 'Cashback akan langsung masuk ke oCash Anda 1x24 jam setelah transaksi sukses.',
  'description_5' => 'Apabila agen yang Anda ajak juga mendaftar menjadi MD Plus, Anda akan mendapatkan bonus langsung sesuai paket yang didaftarkan.',

  'bonus_header_1' => 'Jenis Produk',
  'bonus_header_2' => 'Jumlah Cashback',

  'plan_month' => ':month bulan'
];
