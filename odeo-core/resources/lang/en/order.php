<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 4:29 PM
 */
use Odeo\Domains\Constant\OrderStatus;

return [
  'order_charge_unique_code' => 'Unique Code',
  'order_charge_payment_service_cost' => 'Payment Service Cost',
  'order_charge_ocash' => 'oCash Payment',
  'order_charge_referral_cashback' => 'Referral Cashback',
  
  'order_status_' . OrderStatus::CREATED => 'Checkout',
  'order_status_' . OrderStatus::OPENED => 'Waiting Payment',
  'order_status_' . OrderStatus::WAITING_FOR_UPDATE => 'Waiting for Update',
  'order_status_' . OrderStatus::CONFIRMED => 'Verifying',
  'order_status_' . OrderStatus::WAITING_SUSPECT => 'Waiting Operator Response',
  'order_status_' . OrderStatus::PARTIAL_FULFILLED => 'Processing',
  'order_status_' . OrderStatus::COMPLETED => 'Complete',
  'order_status_' . OrderStatus::CANCELLED => 'Cancelled',
  'order_status_' . OrderStatus::REFUNDED => 'Refund',

  'product_name' => 'Product Name',
  'customer_number' => 'Customer Number',
  'store_name' => 'Store Name',
  'store_domain' => 'Store Domain',
  'name' => 'Name',
  'customer_id' => 'Customer ID',
  'customer_name' => 'Customer Name',
  'kwh' => 'KWH',
  'tariff' => 'Tariff/Power',
  'total_month' => 'Total Month',
  'reference_number' => 'Reference Number',
  'period' => 'Period',
  'player_id' => 'Player ID',
  'amount_due' => 'Amount Due',
  'admin_fee' => 'Admin Fee',
  'discount' => 'Discount',
  'fine' => 'Fine',
  'amount' => 'Amount',
  'product_price' => 'Product Price',
  'meter_change' => 'Meter Change',
  'serial_number' => 'Serial Number',
  'usage' => 'Usage',
  'due_date' => 'Due Date'
];
