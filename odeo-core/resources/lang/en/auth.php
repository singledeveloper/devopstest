<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'telkomsel_otp_register_fail' => "There's a problem with Telkomsel numbers right now. Please try to use other provider number or contact cs@odeo.co.id to provide you further.",
    'telkomsel_otp_reset_password_fail' => "There's a problem with Telkomsel numbers right now. Please contact cs@odeo.co.id for provide to reset your password."

];
