<?php
use \Odeo\Domains\PaymentGateway\Helper\PaymentGatewaySettings;
return [
  'unauthorized' => 'Unauthorized Access',
  'client_exists' => 'Client already registered',
  'client_registered' => 'Client registration successful',
  'scope_exists' => 'Client scope already exists',
  'invalid_email_format' => 'Invalid Email Format',
  'invalid_pg_channel' => 'Invalid Payment Channel',
  'invalid_settlement_type' => 'Invalid Settlement Type',
  'pg_settlement_' . PaymentGatewaySettings::SETTLEMENT_TYPE_REALTIME => 'Realtime Settlement',
  'pg_settlement_' . PaymentGatewaySettings::SETTLEMENT_TYPE_DEFAULT => 'Default Settlement',
  'pg_settlement_' . PaymentGatewaySettings::SETTLEMENT_TYPE_WEEKDAY => 'Weekday Settlement',
  'pg_already_notified' => 'Already Notified within the last 10 min, please try again later.'
];