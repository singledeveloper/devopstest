<?php


return [
  'withdraw' => '支出',
  'transfer' => '转账',
  'tnc_withdraw_1' => '所有银行的:process费为' . \Odeo\Domains\Constant\Withdraw::REAL_TIME_FEE . '印尼盾。 这是为了使用实时在线传输系统来实现更好，更快的处理。',
];
