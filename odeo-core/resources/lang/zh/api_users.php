<?php
use \Odeo\Domains\PaymentGateway\Helper\PaymentGatewaySettings;
return [
  'unauthorized' => '越权存取',
  'client_exists' => '客户已经注册',
  'client_registered' => '客户注册成功',
  'scope_exists' => '客户scope已存在',
  'invalid_email_format' => '无效的电子邮件格式',
  'invalid_pg_channel' => '无效的付款渠道',
  'invalid_settlement_type' => '无效的结算类型',
  'pg_settlement_' . PaymentGatewaySettings::SETTLEMENT_TYPE_REALTIME => '实时结算',
  'pg_settlement_' . PaymentGatewaySettings::SETTLEMENT_TYPE_DEFAULT => '预设结算',
  'pg_settlement_' . PaymentGatewaySettings::SETTLEMENT_TYPE_WEEKDAY => '平日结算',
  'pg_already_notified' => '在过去10分钟内已通知，请稍后再试'
];