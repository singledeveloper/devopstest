<?php
use Odeo\Domains\Constant\Recurring;

return [
  Recurring::TYPE_ONCE => '只有一次',
  Recurring::TYPE_DAY => '每天',
  Recurring::TYPE_DAY_WORK => '每天（星期一至星期五）',
  Recurring::TYPE_DAY_MON_SAT => '每天（星期一至星期六）',
  Recurring::TYPE_WEEK . '_1' => '每个星期一',
  Recurring::TYPE_WEEK . '_2' => '每个星期二',
  Recurring::TYPE_WEEK . '_3' => '每个星期三',
  Recurring::TYPE_WEEK . '_4' => '每个星期四',
  Recurring::TYPE_WEEK . '_5' => '每个星期五',
  Recurring::TYPE_WEEK . '_6' => '每个星期六',
  Recurring::TYPE_WEEK . '_7' => '每个星期天',
  Recurring::TYPE_MONTH => '每个月',
  'date' => '第',
  'time_format' => '- gA \G\M\T+7'
];