<?php

return [
  'update_price_on_' . 1 => '1 menit kemudian',
  'update_price_on_' . 5 => '5 menit kemudian',
  'update_price_on_' . 30 => '30 menit kemudian',
  'update_price_on_' . 60 => '1 jam kemudian',
  'update_price_on_' . 120 => '2 jam kemudian',
  'update_price_on_' . 240 => '4 jam kemudian',
  'update_price_on_' . 480 => '8 jam kemudian',
  'update_price_on_' . 720 => '12 jam kemudian',
  'update_price_on_' . 960 => '16 jam kemudian',
  'update_price_on_' . 1440 => '1 hari kemudian',
  'update_price_on_' . 2880 => '2 hari kemudian',
];

