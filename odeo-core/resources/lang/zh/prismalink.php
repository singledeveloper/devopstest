<?php

return [
  'invalid_code' => 'Invalid code',
  'invalid_partner_id' => 'Invalid Partner Id',
  'signature_not_match' => 'Signature Not Match',
  'request_cannot_be_processed' => 'Request cannot be processed'
];
