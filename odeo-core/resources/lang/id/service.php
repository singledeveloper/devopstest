<?php
use Odeo\Domains\Constant\Service;

return [
  'service_' . Service::PULSA => 'Pulsa',
  'service_' . Service::PLN => 'Token PLN',
  'service_' . Service::PAKET_DATA => 'Paket Data',
  'service_' . Service::BOLT => 'BOLT',
  'service_' . Service::PULSA_POSTPAID => 'Pulsa Pascabayar',
  'service_' . Service::PLN_POSTPAID => 'Tagihan PLN',
  'service_' . Service::BPJS_KES => 'BPJS Kesehatan',
  'service_' . Service::BROADBAND => 'Broadband',
  'service_' . Service::LANDLINE => 'Landline',
  'service_' . Service::GAME_VOUCHER => 'Voucher Game',
  'service_' . Service::TRANSPORTATION => 'Transportasi',
  'service_' . Service::GOOGLE_PLAY => 'Google Play',
  'service_' . Service::PDAM => 'PDAM',
  'service_' . Service::PGN => 'PGN',
  'service_' . Service::EMONEY => 'Uang Elektronik'
];
