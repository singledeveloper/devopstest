<?php

return [
  'sms_otp_shorten' => 'ODEO: :otp',
  'sms_otp' => '<#> ODEO: :otp. Demi keamanan akun anda, jangan pernah memberitahukan ke orang lain ataupun pihak ODEO. lPG3/3iJnyO',
  'sms_random_pattern' => 'Kode-K0de|k0de;konfirmasi-k0nfirmas1|konf1rmas1|k0nf1rmasi;Code-C0de|c0de;confirmation-c0nfirmat1on|conf1rmat1on|c0nf1rmation',
  'sms_pln_token' => 'Token untuk PLN ID :meter_number adalah :token',
  'order_not_found' => 'Order tidak ditemukan',
  'order_report_already_exported' => 'Ekspor sudah dilakukan dalam waktu 1 jam, silahkan coba lagi nanti.',
  'email_not_set' => 'Akun email Anda belum di diisi.',
  'email_not_verified' => 'Akun email Anda belum diverifikasi.',
  'nik_mismatch' => 'NIK tidak cocok',
  'name_mismatch' => 'Nama tidak cocok',
  'ktp_mismatch' => 'KTP tidak cocok',
  'ktp_unclear' => 'KTP tidak jelas',
  'ktp_selfie_mismatch' => 'KTP + Selfie tidak cocok',
  'ktp_selfie_unclear' => 'KTP + Selfie tidak jelas',
  'last_ocash_balance_mismatch' => 'Saldo Ocash Terakhir tidak cocok',
  'last_transaction_description_mismatch' => 'Deskripsi Transaksi Terakhir tidak cocok',
  'email_is_taken' => 'Email tersebut telah digunakan oleh akun lain',
  'email_is_verified' => 'Email Anda telah terverifikasi',
  'email_not_allowed' => 'Email ini tidak dapat digunakan',
  'max_otp_request_reached' => 'Permintaan OTP Anda telah mencapai batas maksimal (3 kali). Silahkan dicoba kembali 1 jam kemudian.',
  'email_otp_instruction' => 'Masukkan kode berikut ini ke aplikasi untuk memverifikasi email Anda',
  'new_login' => 'Kami mendeteksi adanya login ke aplikasi odeo dari perangkat baru. Jika ini bukan anda, ganti password anda sekarang untuk melindungi akun anda'
];
