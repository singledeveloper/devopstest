<?php
use \Odeo\Domains\PaymentGateway\Helper\PaymentGatewaySettings;
return [
  'unauthorized' => 'Unauthorized Access',
  'client_exists' => 'Client sudah terdaftar',
  'client_registered' => 'Client berhasil terdaftar',
  'scope_exists' => 'Scope sudah terdaftar',
  'invalid_email_format' => 'Format Email Salah',
  'invalid_pg_channel' => 'Payment Channel Salah',
  'invalid_settlement_type' => 'Settlement Type Salah',
  'pg_settlement_' . PaymentGatewaySettings::SETTLEMENT_TYPE_REALTIME => 'Realtime Settlement',
  'pg_settlement_' . PaymentGatewaySettings::SETTLEMENT_TYPE_DEFAULT => 'Default Settlement',
  'pg_settlement_' . PaymentGatewaySettings::SETTLEMENT_TYPE_WEEKDAY => 'Weekday Settlement',
  'pg_already_notified' => 'Sudah dinotifikasi dalam 10 menit terakhir, silahkan coba lagi nanti.'
];