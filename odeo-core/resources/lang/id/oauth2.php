<?php

return [
  'require_grant' => 'GrantType harus diisi',
  'invalid_grant' => 'Grant tidak valid',
  'invalid_request' => 'Request tidak valid',
  'unsupported_grant_type' => 'Grant type tidak didukung',
  'invalid_scope' => 'Scope tidak valid',
  'invalid_client' => 'Client tidak valid',
  'invalid_auth_header' => 'Authorization header tidak valid',
  'invalid_data' => 'Data tidak valid'
];
