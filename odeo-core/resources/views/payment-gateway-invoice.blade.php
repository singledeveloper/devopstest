<html>
<body style="margin: 0; padding: 0; font-size: 12px; font-family: Arial, sans-serif;">
<div><img src="https://api.odeo.co.id/images/odeo-logo-1.png" alt="" width="150"></div>

<div style="font-weight: bold; font-size: 32px; margin: 0; padding: 0;">Tagihan Payment Gateway</div>

<br>

<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <table>
        <tr>
          <td>Kepada Yth,<br>{{$data['user_name']}}</td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td width="30%">No.</td>
          <td>: {{$data['invoice_no']}}</td>
        </tr>
        <tr>
          <td>Tanggal</td>
          <td>: {{date('Y-m-d')}}</td>
        </tr>
        <tr>
          <td>ID Akun</td>
          <td>: {{$data['user_id']}}</td>
        </tr>
        <tr>
          <td colspan="2"><span style="font-size: 20px; font-weight: bold; color: red">LUNAS</span></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<p>Dengan ini kami informasikan tagihan Payment Gateway yang berlaku pada periode {{$data['period']}} adalah sebagai
  berikut:</p>

<table cellpadding="4" cellspacing="0" width="100%" class="content">
  <tr bgcolor="#6fd0e1" style="font-weight: bold">
    <td width="10%" style="font-weight: bold;">Periode</td>
    <td width="30%" style="font-weight: bold;">Produk</td>
    <td width="20%" style="font-weight: bold; text-align: right;">Jumlah</td>
    <td width="20%" style="font-weight: bold; text-align: right;">Harga (IDR)</td>
    <td width="20%" style="font-weight: bold; text-align: right;">Nominal (IDR)</td>
  </tr>
  @foreach($data['rows'] as $index => $row)
    <tr>
      <td>{{ $row["period"] }}</td>
      <td>{{ $row["name"] }}</td>
      <td style="text-align: right;">{{ $row["quantity"] }}</td>
      <td style="text-align: right;">{{ $row["price"] }}</td>
      <td style="text-align: right;">{{ $row["formatted_amount"] }}</td>
    </tr>
  @endforeach
  <tr>
    <td colspan="4" style="text-align: right; font-weight: bold">Total (IDR)</td>
    <td style="text-align: right; font-weight: bold">{{$data['total']}}</td>
  </tr>
</table>

<p>Demikian informasi dari kami, atas perhatiannya kami ucapkan terima kasih.</p>

<p>Hormat kami,</p>
<p>&nbsp;</p>
<p style="margin-top: 40px; padding-top: 40px;">PT Odeo Teknologi Indonesia<br>Surat ini resmi tanpa tanda tangan dan
  cap perusahaan</p>

<p>&nbsp;</p>
<p style="color: #273c40; padding: 15px 15px 15px 15px; text-align: center; font-size: 10px;">Untuk informasi lebih
  lanjut, silakan hubungi kami di <a href="mailto:cs@odeo.co.id">cs@odeo.co.id</a></p>
<p>&nbsp;</p>

<p style="color: #273c40; padding: 15px 15px 15px 15px; text-align: left; font-size: 10px;">
  <br>PT Odeo Teknologi Indonesia
  <br>Komplek Business Park Kebon Jeruk, Ruko AB-6 Lt. 6, Jalan Meruya Ilir No. 88 RT01/RW05, Meruya Utara, Kembangan, Jakarta Barat, 11620
  <br>Hak cipta {{ date('Y') }} PT ODEO Teknologi Indonesia - ODEO dan semua logonya merupakan properti kreatif milik PT
  ODEO Teknologi Indonesia
</p>
</body>

</html>
