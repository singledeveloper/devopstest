<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<div
  style="background-image: url(<?php echo baseUrl('images/email/email_bg.png'); ?>); position: fixed; top: 0; left: 0; background-size: cover; margin:0; padding: 0; width: 100%; height: 100%;">
  <div style="padding-top: 15px;">
      <img src="https://api.odeo.co.id/images/email/email_logo.png"
           style="display: block; margin:0 auto; background-position: center;">
  </div>
  <div style="max-width: 80%; height: 100%;  margin: 0 auto; padding: 0;">
    <div style="background-color: white; padding: 40px;">
        Hi, Odeo User<br/><br/>

Besok tanggal 23 November 2016 jam 13.00 - 19.00, Odeo akan melakukan maintenance keseluruhan di semua sistem (Android, WebStore, WebApp).<br/>
Maintenance ini akan membantu performa Odeo ke depannya agar semua transaksi dapat selesai dalam waktu yang lebih cepat.<br/>
Dimohon agar user tidak melakukan transaksi di jam tersebut untuk menghindari kesalahan data.<br/>
Terima kasih atas kerjasamanya.<br/><br/>

Best Regards, Administrator
    </div>
    <div>
      <p style="opacity: 0.7; color: white; text-align:center; margin-bottom: 20px;">Butuh Bantuan? Hubungi Kami
        <b><a style="color:white; text-decoration: none;" href="">cs@odeo.co.id</a></b></p>
    </div>
  </div>
</div>
</body>
</html>