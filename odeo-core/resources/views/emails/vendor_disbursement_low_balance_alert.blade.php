<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Email</title>
</head>
<body>
Vendor disbursement low balance alert:

<table>
  <tr>
    <th>Vendor</th>
    <th>Balance</th>
  </tr>
  @foreach($balances as $vendor => $balance)
    <tr>
      <td>{{$vendor}}</td>
      <td>{{$balance}}</td>
    </tr>
  @endforeach
</table>
</body>
</html>