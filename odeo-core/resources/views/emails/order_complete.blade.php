@extends('emails.base')

@section('styles')
  <style type="text/css">
    #detail-box {
      margin: 20px 0px;
      background-color: rgb(248,248,248);
      width: 550px;
    }
    #inner-detail-box {
      margin: 10px 20px;
    }
    .left-box-text {
      color: rgb(144,145,146); 
      margin: 10px 0px;
      font-size: 14px;
    }
    .middle-box-text {
      color: rgb(144,145,146); 
      margin: 10px 10px;
      font-size: 14px;
    }
    .right-box-text {
      margin: 10px 0px;
      font-size: 14px;
    }
    .detail-text {
      margin-bottom: 0px;
    }
  </style>
@endsection

@section('content')
  <table id="content-table" cellpadding="0" cellspacing="0" style="margin: 0px">
      <tr>
        <td>
          <p class="no-margin text-14">Dear <b>{{$data['customer_name'] or '-'}}</b>,</p>
        </td>
        <td width="45%">
          <p class="no-margin text-14 gray-text right-text">ID Transaksi</p>
        </td>
      </tr>
      <tr>
        <td rowspan="2">
          <p class="no-margin text-14">Transaksi <b>{{$data['cart_data']['items'][0]['name']}}</b> Anda telah berhasil, dengan rincian sebagai berikut:</p>
        </td>
        <td>
          <p class="text-18 green-text bold-text right-text no-margin">#{{$data['order_id'] or '-'}}</p>
        </td>
      </tr>
      <tr>
        <td>
          <p class="no-margin text-14 right-text">{{$data['purchase_at']}}</p>
        </td>
      </tr>
    </table>

    <table id="detail-box">
      <tr>
        <td width="520">
        <table id="inner-detail-box" cellpadding="0" cellspacing="0">
          @foreach($data['receipt_order_detail'] as $item)
            <tr>
              <td width="220">
                <p class="left-box-text">{{$item['label']}}</p>
              </td>
              <td>
                <p class="middle-box-text">:</p>
              </td>
              <td>
                <p class="right-box-text">{{$item['value'] or '-'}}</p>
              </td>
            </tr>
          @endforeach
        </table>
        </td>
      </tr>
    </table>

    @if(isset($data['cart_data']['items'][0]['item_detail']['inquiry']['monthly']))
      <h3 class="text-18">Detail</h3>
      <table id="detail-box">
        <tr>
          <td width="520">
            <table id="inner-detail-box" cellpadding="0" cellspacing="0" width="505">
              @foreach($data['cart_data']['items'][0]['item_detail']['inquiry'] as $key => $output)
                @if($key == 'monthly')
                  <tr>
                    <td class="left-box-text">Periode</td>
                    @if(isset($output[0]['meter_changes']))
                      <td class="left-box-text center-text">Stand Meter</td>
                    @endif
                    <td class="left-box-text center-text">Harga</td>
                    @if(isset($output[0]['fine']))
                      <td class="left-box-text center-text">Denda</td>
                    @endif
                    <td class="left-box-text center-text">Admin</td>
                    <td class="left-box-text right-text">Subtotal</td>
                  </tr>

                  @foreach($output as $suboutput)
                    <tr>
                      <td><p class="detail-text text-14">{{$suboutput['period']}}</p></td>
                      @if(isset($suboutput['meter_changes']))
                        <td><p class="detail-text text-14 center-text">{{$suboutput['meter_changes']}}</p></td>
                      @endif
                      <td><p class="detail-text text-14 center-text">{{$suboutput['price']['formatted_amount']}}</p></td>
                      @if(isset($suboutput['meter_changes']))
                        <td><p class="detail-text text-14 center-text">{{$suboutput['fine']['formatted_amount']}}</p></td>
                      @endif
                      <td><p class="detail-text text-14 center-text">{{$suboutput['admin_fee']['formatted_amount']}}</p></td>
                      <td><p class="detail-text text-14 right-text">{{$suboutput['subtotal']['formatted_amount']}}</p></td>
                    </tr>
                  @endforeach
                @endif
              @endforeach
            </table>
          </td>
        </tr>
      </table>
    @endif

    @if($data['cart_data']['items'][0]['type'] === 'pln postpaid')
      <a class="link" href="{{baseUrl() . 'v1/report/pln-postpaid?odps=' . $data['cart_data']['items'][0]['order_detail_pulsa_switcher_id'] . '&ref=' . $data['cart_data']['items'][0]['item_detail']['inquiry']['ref_number']}}">Lihat struk di sini</a>
      <br><br>
    @endif

    @if($data['cart_data']['items'][0]['type'] === 'bpjs kes')
      <a class="link" href="{{baseUrl() . 'v1/report/bpjs?odps=' . $data['cart_data']['items'][0]['order_detail_pulsa_switcher_id'] . '&ref=' . $data['cart_data']['items'][0]['item_detail']['inquiry']['ref_number']}}">Lihat struk di sini</a>
      <br><br>
    @endif

@endsection
