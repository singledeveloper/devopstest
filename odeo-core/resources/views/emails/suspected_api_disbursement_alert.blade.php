<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<h1>Please kindly check this suspected api disbursement</h1>
<table>
<tr>
  <td>Api Disbursement ID</td>
  <td>Inquiry ID</td>
  <td>Vendor</td>
  <td>Disbursement Status</td>
  <td>Disbursement Amount</td>
  <td>Inquiry Amount</td>
  <td>Inquiry Description</td>
</tr>
@foreach($data['suspects'] as $suspect)
  <tr>
    <td>{{$suspect['id']}}</td>
    <td>{{$suspect['inquiry_id']}}</td>
    <td>{{$suspect['vendor']}}</td>
    <td>{{$suspect['disbursement_status']}}</td>
    <td>{{$suspect['disbursement_amount']}}</td>
    <td>{{$suspect['inquiry_amount']}}</td>
    <td>{{$suspect['inquiry_description']}}</td>
  </tr>
@endforeach
</table>

</body>
</html>