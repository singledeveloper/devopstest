<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body style="font-family: 'Arial'; font-size: 14px">

Dear Mitra Odeo, <br><br>

Terlampir tagihan API Disbursement pada periode {{$data['period']}}.<br><br>

Best Regards, <br>
PT Odeo Teknologi Indonesia
</body>
</html>