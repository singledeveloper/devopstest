<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title>Total Transaksi Promo Cashback</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style type="text/css">
    @media only screen and (min-device-width: 601px) {
      .content {
        width: 600px !important;
      }
      .desktop-hide {
        display: none !important;;
      }
      .contact-us {
        width: 30%;
      }
    }

    @media only screen and (max-device-width: 601px) {
      .content {
        width: 320px !important;
      }
      .mobile-hide {
        display: none !important;;
      }
      .contact-us {
        width: 40%;
      }
    }


  </style>
</head>
<body style="margin: 0; padding: 0; font-family: Arial, sans-serif;">
<table align="center" cellpadding="0" cellspacing="0" width="100%" bgcolor="#273c40">
  <tr>
    <td>
      <table class="content" cellspacing="0" cellpadding="0"
             style="border-bottom-left-radius: 15px;border-bottom-right-radius: 15px; margin: 0 auto 20px auto; background: white;">
        <tr>
          <td width="150px;">
            <img src="https://api.odeo.co.id/images/odeo-logo-1.png" alt="" width="150">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table align="center" cellpadding="0" cellspacing="0" width="100%" style="margin: 70px auto; font-size: 15px; line-height: 1.5em;"  class="content">
  <tr>
    <td>
      {{$top_info}} <b>{{$count}}</b>

      <br/>
      <br/>
      
      {{$bottom_info}}

      <br/>
      <br/>

      <table align="center" border="1" cellpadding="5" cellspacing="0">
        <tr>
          <th>Transaksi</th>
          <th>Cashback</th>
        </tr>
        @foreach($cashback as $target => $cash)
          <tr @if($target == $current) style="background:#FFEE58" @endif>
            <td>
              {{$target}}
            </td>
            <td>
              {{$cash}}
            </td>
          </tr>
        @endforeach
      </table>

    </td>
  </tr>
  <tr></tr>
</table>
<table cellpadding="0" cellspacing="20" width="100%" bgcolor="#273c40">
  <tr>
    <td>
      <table cellpadding="0" cellspacing="0" class="content" style="margin: 0 auto 0 auto; text-align: center;">
        <tr>
          <td style="color:#1dbed8;" class="contact-us">
            <p>Hubungi Kami</p>
          </td>
          <td style="color:#1dbed8;">
           <p>Download Odeo</p>
          </td>
          <td class="mobile-hide" style="width: 10px;" rowspan="3"></td>
          <td style="color:#1dbed8; text-align: left; padding: 0px 5px 0px 5px;"  class="mobile-hide"></td>
        </tr>
        <tr>
          <td>
            <a href="https://www.facebook.com/odeo.id"><img src="https://api.odeo.co.id/images/email/fb.png" alt=""></a>
            <a href="https://twitter.com/odeo_id"><img src="https://api.odeo.co.id/images/email/twitter.png" alt=""></a>
            <a href="https://www.instagram.com/odeo.id"><img src="https://api.odeo.co.id/images/email/ig.png" alt=""></a>
          </td>
          <td>
            <img src="https://api.odeo.co.id/images/email/button_appstore.png" alt="" width="150px">
          </td>
          <td class="mobile-hide" bgcolor="" style="color: #bbb; padding: 0px 5px 0px 5px; text-align: justify" rowspan="2">
            Hak cipta 2016 PT ODEO Teknologi Indonesia - ODEO dan semua logonya merupakan properti kreatif milik PT ODEO Teknologi Indonesia
          </td>
        </tr>
        <tr>
          <td style="color: white;">
            <a href="mailto:cs@odeo.co.id" style="color: white;text-decoration: none;">cs@odeo.co.id</a>
            <p style="font-size: 12px;">Senin-Jumat
              <br>9:00-18:00</p>
          </td>
          <td><a href="https://play.google.com/store/apps/details?id=com.odeo.odeo"><img src="https://api.odeo.co.id/images/email/button_gplay.png" alt="" width="150px"></a></td>
        </tr>
      </table>
    </td>
  </tr>
  </tr>
</table>
<table cellpadding="0" cellspacing="0" width="100%" bgcolor="#273c40" class="desktop-hide">
  <tr>
    <td bgcolor="black" style="color: #747474; padding: 15px 15px 15px 15px; text-align: justify; font-size: 12px;" colspan="3">
      Hak cipta 2016 PT ODEO Teknologi Indonesia - ODEO dan semua logonya merupakan properti kreatif milik PT ODEO Teknologi Indonesia
    </td>
</table>
</body>
</html>