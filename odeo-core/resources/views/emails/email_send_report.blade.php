<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
  .text-center, .text-center td, .text-center th {
    text-align: left;
  }

  tr.total td {
    font-weight: bold;
  }
</style>

@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')

<body style="font-family: 'Arial'; font-size: 14px">

<h4>Report Date : {{$date}}</h4>

<h2>Report Sales oCommerce</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Type</th>
    <th>Sales</th>
    <th>Sales Target</th>
    <th>Total Sales Amount</th>
  </tr>
  <?php $sales = 0; $salesTarget = 0; $salesAmount = 0; ?>
  @foreach ($salesPlatformReports as $item)
    <tr>
      <td>{{ $item->plan->name }}</td>
      <td>{{ $item->sales }}<?php $sales += $item->sales; ?></td>
      <td>{{ $item->sales_target }}<?php $salesTarget += $item->sales_target; ?></td>
      <td>
        {{ $currencyHelper->formatPrice($item->sales_value)['formatted_amount'] }}
        <?php $salesAmount += $item->sales_value; ?>
      </td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td>{{ $sales }}</td>
    <td>{{ $salesTarget }}</td>
    <td>{{ $currencyHelper->formatPrice($salesAmount)['formatted_amount'] }}</td>
  </tr>
</table>

<h2>Agent Sales</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Agent Store Name</th>
    <th>Invited Store Count</th>
  </tr>
  @if (count($agentReport) > 0)
    <?php $sales = 0; ?>
    @foreach ($agentReport as $item)
      <tr>
        <td>{{ $item->referredStore->name }}</td>
        <td>{{ $item->referred_counts }}<?php $sales += $item->referred_counts; ?></td>
      </tr>
    @endforeach
    <tr class="total">
      <td>Total</td>
      <td>{{ $sales }}</td>
    </tr>
  @else
    <tr>
      <td colspan="2">No invitation today</td>
    </tr>
  @endif
</table>

<h2>Report Registered User</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>User Role</th>
    <th>Registered User</th>
    <th>Registration Target</th>
  </tr>

  <?php $registeredUser = 0; $registeredTarget = 0; ?>
  @foreach ($registeredUserReports as $item)
    @if ($item->user_type != 'admin')
      <tr>
        <td>
          @if ($item->user_type == 'seller') Android
          @elseif ($item->user_type == 'guest') Web Traffic
          @else {{ $item->user_type }}
          @endif
        </td>
        <td>{{ $item->registered_user }}<?php $registeredUser += $item->registered_user; ?></td>
        <td>{{ $item->registration_target }}<?php $registeredTarget += $item->registered_target; ?></td>
      </tr>
    @endif
  @endforeach
  <tr class="total">
    <td>Total</td>
    <td>{{ $registeredUser }}</td>
    <td>{{ $registeredTarget }}</td>
  </tr>
</table>

@inject('statusParser', 'Odeo\Domains\Order\Helper\StatusParser')

<h2>Order Failure Status</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Status</th>
    <th>Quantity</th>
  </tr>

  @if (count($notCompletedOrderCount) > 0)
    <?php $total = 0; ?>
    @foreach ($notCompletedOrderCount as $item)
      <tr>
        <td>{{ $statusParser->parse($item, '0')['status'] }}</td>
        <td>{{ $item->status_count }}<?php $total += $item->status_count; ?></td>
      </tr>
    @endforeach
    <tr class="total">
      <td>Total</td>
      <td>{{ $total }}</td>
    </tr>
  @else
    <tr>
      <td>Fail</td>
      <td>0</td>
    </tr>
  @endif
</table>

<h2>Lead Time Above 60 Seconds</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Biller Name</th>
    <th>Quantity</th>
  </tr>

  @if (count($leadTimeAbove60Count) > 0)
    <?php $total = 0; ?>
    @foreach ($leadTimeAbove60Count as $item)
      <tr>
        <td>{{ $item->name }}</td>
        <td>{{ $item->order_counts }}<?php $total += $item->order_counts; ?></td>
      </tr>
    @endforeach
    <tr class="total">
      <td>Total</td>
      <td>{{ $total }}</td>
    </tr>
  @else
    <tr>
      <td colspan="2">All is well</td>
    </tr>
  @endif
</table>

<h2>Biller SLA / Average Lead Time</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Biller Name</th>
    <th>SLA</th>
    <th>Average Lead Time</th>
    <th>Transaction Counts</th>
  </tr>

  <?php $totalSla = 0; $countSla = 0; $timestamps = 0; $successPurchases = 0; ?>
  @foreach ($billerReports as $item)
    <tr>
      <td>{{ $item->name }}</td>
      <td>{{ $item->sla }}<?php if ($item->sla != '-') {
          echo '%';
          $totalSla += $item->sla;
          $countSla++;
        } ?></td>
      <td>
        {{ $item->average_lead_time or '-' }}
        <?php
        if ($item->average_lead_time && $item->average_lead_time != '-') {
          list($h, $m, $s) = explode(':', $item->average_lead_time);
          $timestamps += intval($h) * 3600 + intval($m) * 60 + intval($s);
        }
        ?>
      </td>
      <td>{{ $item->success_purchase_count }}<?php $successPurchases += $item->success_purchase_count; ?></td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Overall</td>
    <td>
      @if ($countSla != 0)
        {{ round($totalSla / $countSla, 5) }}%
      @else -
      @endif
    </td>
    <td>
      @if ($countSla != 0)
        {{ gmdate('H:i:s', ceil($timestamps / $countSla)) }}
      @else -
      @endif
    </td>
    <td>{{ $successPurchases }}</td>
  </tr>
</table>

<h2>oCash & oDeposit</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Type</th>
    <th>Value</th>
    <th>No. of Transaction</th>
  </tr>
  <tr>
    <td>oCash Topup</td>
    <td>{{ $currencyHelper->formatPrice($topupTransactions->total)['formatted_amount'] }}</td>
    <td>{{ $topupTransactions->count }}</td>
  </tr>
  <tr>
    <td>oDeposit Topup</td>
    <td>{{ $currencyHelper->formatPrice($depositTransactions->total)['formatted_amount'] }}</td>
    <td>{{ $depositTransactions->count }}</td>
  </tr>
</table>

<h2>Completed Withdraw</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>No.</th>
    <th>User Name</th>
    <th>Account Bank</th>
    <th>Withdraw Amount</th>
  </tr>

  @if (count($withdrawTransactions) > 0)
    <?php $total = 0; $x = 1; ?>
    @foreach ($withdrawTransactions as $item)
      <tr>
        <td>{{ $x++ }}.</td>
        <td>{{ $item->user->name }}</td>
        <td>{{ $item->account_bank }}</td>
        <td>{{ $currencyHelper->formatPrice($item->amount)['formatted_amount'] }}<?php $total += $item->amount; ?></td>
      </tr>
    @endforeach
    <tr class="total">
      <td colspan="3">Total</td>
      <td>{{ $currencyHelper->formatPrice($total)['formatted_amount'] }}</td>
    </tr>
  @else
    <tr>
      <td colspan="6">No withdraw today</td>
    </tr>
  @endif
</table>

<h2>Current Recorded Balance</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <td>Bank Mandiri</td>
    <td>
      <?php
      $temp = explode('.', $mandiriBalance->balance);
      echo $currencyHelper->formatPrice(str_replace(',', '', $temp[0]))['formatted_amount'];
      ?>
    </td>
  </tr>
  <tr>
    <td>Bank BCA</td>
    <td>
      <?php
      $temp = explode('.', $bcaBalance->balance);
      echo $currencyHelper->formatPrice(str_replace(',', '', $temp[0]))['formatted_amount'];
      ?>
    </td>
  </tr>
  @foreach ($billerData as $item)
    <tr>
      <td>{{ $item->name }}</td>
      <td>{{ $currencyHelper->formatPrice($item->current_balance)['formatted_amount'] }}</td>
    </tr>
  @endforeach
</table>

<h2>User Rates</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>No.</th>
    <th>User Name</th>
    <th>Confirmation Name</th>
    <th>From Platform</th>
  </tr>

  @if (count($userRates) > 0)
    <?php $x = 1; ?>
    @foreach ($userRates as $item)
      <tr>
        <td>{{ $x++ }}.</td>
        <td>{{ $item->user->name }}</td>
        <td>{{ $item->confirmation_name }}</td>
        <td>Android</td>
      </tr>
    @endforeach
  @else
    <tr>
      <td colspan="4">No rate today</td>
    </tr>
  @endif
</table>

<h2>Admin Access Log</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>User Name</th>
    <th>Today First Login</th>
  </tr>

  @if (count($userAccessLogs) > 0)
    @foreach ($userAccessLogs as $item)
      <tr>
        <td>{{ $item->name }}</td>
        <td>{{ date('d/m/Y H:i', strtotime($item->first_created_at)) }}</td>
      </tr>
    @endforeach
  @else
    <tr>
      <td colspan="2">No access log today</td>
    </tr>
  @endif
</table>

</body>
</html>