@extends('emails.base')

@section('styles')
  <style type="text/css">
    #detail-box {
      margin: 0 auto;
      background-color: rgb(248,248,248);
    }
    #inner-detail-box {
      margin: 20px 30px;
      line-height: 1.3;
    }
    .image {
      margin-right: 20px;
    }
    .instruction {
      margin-left: 15px;
      margin-right: 15px;
    }
  </style>
@endsection

@section('content')
    <p class="center-text text-24 bold-text no-margin">New Device Alert</p>
    <p class="center-text text-14">Kami mendeteksi adanya login baru melalui perangkat baru:</p>

    <table id="detail-box">
      <tr>
        <td>
        <table id="inner-detail-box" cellpadding="0" cellspacing="0">
          <tr>
            <td rowspan="4">
              @if ($platform === 'Android')
                <img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/android.png" alt="" width="45" class="image"/>
              @elseif ($platform === 'IOS')
                <img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/ios.png" alt="" width="45" class="image"/>
              @else
                <img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/desktop.png" alt="" width="45" class="image"/>
              @endif
            </td>
            <td>
              <p class="text-16 bold-text no-margin">{{ $platform }}</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="text-14 gray-text no-margin bold-text" style="line-height: 1.5">{{ date('d F Y H:i') }} WIB</p>
            </td>
          </tr>
          @if(isset($city)) 
          <tr>
            <td>
              <p class="text-14 gray-text no-margin">{{ $city }}</p>
            </td>
          </tr>
          @endif
          <tr>
            <td>
              <p class="text-14 gray-text no-margin">IP {{ $ip_address }}</p>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>

    <p class="text-14 instruction">Jika Anda mengenali login tersebut, Anda dapat mengabaikan email ini.</p>
    <p class="text-14 instruction">Jika tidak, kemungkinan ada pihak tertentu yang ingin memanipulasi data Anda. Mohon segera ubah password di aplikasi ODEO perangkat Anda.</p>
@endsection
