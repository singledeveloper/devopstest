@extends('emails.base_payment')

@section('header')
  <img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/trx_menunggu.png" style="width: 100%" alt=""/>
@endsection

@section('styles')
    <style>
        #title {
            text-align: center;
            margin: 0;
            margin-top: 20px;
            font-weight: bold;
        }
        .time-description-text {
            text-align: center;
            margin: 5px;
            margin-top: 15px;
        }
        #time-text {
            text-align: center;
            margin: 5px;
            margin-bottom: 15px;
            font-size: 24px;
            font-weight: bold;
        }
        #pay-code {
            text-align: center;
            margin: 15px 0px;
        }
    </style>
@endsection

@section('content')
    <p id="title" class="text-24" >Transaksi Menunggu Pembayaran</p>
    <p class="center-text text-14" style="margin-top: 5px">Selesaikan pembelian Anda di <a href="{{$data['domain']}}" target="_blank" class="link">{{$data['name']}}</a></p>

    @include('emails.order_summary')

    <hr class="separator"/>

    <p class="time-description-text text-16">Mohon selesaikan pembayaran Anda sebelum</p>

    <p id="time-text">{{ date("l j F Y H:i", strtotime($data['date_expired'] ))}} WIB</p>

    <p class="text-24" id="pay-code">{{$data['pay_code']}}</p>

    @include('emails.order_detail_table')

@endsection
