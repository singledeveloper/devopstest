<!doctype html>
@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<h1>Recon BNI EDC Settlement Result - {{$date}}</h1>

<table>
    <tr>
        <th>BNI Bank Inquiry ID</th>
        <th>Date</th>
        <th>Amount</th>
        <th>Description</th>
        <th>Message</th>
    </tr>
    @foreach($results as $row)
        <tr>
            <td>{{$row['id']}}</td>
            <td>{{$row['date']}}</td>
            <td>{{$currencyHelper->formatPrice($row['amount'])['formatted_amount']}}</td>
            <td>{{$row['description']}}</td>
            <td>{{$row['message']}}</td>
        </tr>
    @endforeach
</table>

</body>
</html>