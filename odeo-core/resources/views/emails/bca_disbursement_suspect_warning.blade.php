<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Email</title>
</head>
<body>

reason: {{$data['additional']['reason']}}

<br><br>

<table border="1">
  <tr>
    <td>Bca Disbursement ID</td>
    <td>Status</td>
    <td>Bca Status</td>
    <td>Created At</td>
    <td>Transfer at</td>
    <td>Response at</td>
  </tr>
@foreach($data['bca_disbursement_suspect'] as $suspect)
  <tr>
    <td>{{$suspect['id']}}</td>
    <td>{{$suspect['status']}}</td>
    <td>{{$suspect['bca_status']}}</td>
    <td>{{$suspect['transfer_datetime']}}</td>
    <td>{{$suspect['response_datetime']}}</td>
  </tr>
@endforeach
</table>
</body>
</html>