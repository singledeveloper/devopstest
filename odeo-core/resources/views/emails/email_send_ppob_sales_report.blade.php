<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
  .text-center, .text-center td, .text-center th {
    text-align: left;
  }

  tr.total td {
    font-weight: bold;
  }
</style>

@inject('ch', 'Odeo\Domains\Transaction\Helper\Currency')

<body style="font-family: 'Arial'; font-size: 14px">

<h4>Sales Report Date : {{$date}}</h4>


<h2>GMV Overview</h2>
<table border="1" cellspacing="0" cellpadding="5">
  <tr>
    <th>Channel</th>
    <th>Product</th>
    <th>This Month Target</th>
    <th>Target MTD</th>
    <th>Achievement MTD</th>
    <th>Run Rate</th>
    <th>Prediction</th>
    <th>All Time High</th>
    <th>ATH Run Rate</th>
    <th>Loss Sales</th>
    <th>Last 1 Year</th>
  </tr>
  @foreach($keys as $c => $services)
    @foreach($services as $i => $s)
      <tr>
        <?php $key = $c . ' ' . $s; ?>
        @if($i == 0)
          <td rowspan="{{count($services)}}" style="font-weight: bold;">{{ $channelLabelMap[$c] }}</td>
        @endif
        <td style="font-weight: bold;">{{ $s }}</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($targetMap[$key]->gmv) }}</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($targetMtdMap[$key]->gmv) }}</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($mtdMap[$key]->gmv) }}</td>
        <td style="text-align: right;">{{ number_format($runRateMap[$key]->gmv, 0, ",", ".") }}%</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($predictionMap[$key]->gmv) }}</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($athMap[$key]->gmv) }}</td>
        <td style="text-align: right;">{{ number_format($athRunRateMap[$key]->gmv, 0, ",", ".") }}%</td>
        <td
          style="text-align: right;">{{ isset($refundedMtdMap[$key]) ? $ch->getFormattedOnly($refundedMtdMap[$key]->gmv) : '-' }}</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($ytdMap[$key]->gmv) }}</td>
      </tr>
    @endforeach
  @endforeach
  <tr>
    <td colspan="2" style="font-weight: bold;">Total (IDR)</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTarget->gmv) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTargetMtd->gmv) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesMtd->gmv) }}</td>
    <td style="text-align: right;">{{ number_format($salesRunRate->gmv, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesPrediction->gmv) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesAth->gmv) }}</td>
    <td style="text-align: right;">{{ number_format($salesAthRunRate->gmv, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesRefundedMtd->gmv) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesYtd->gmv) }}</td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight: bold;">USD Rate</td>
    <td colspan="9" style="text-align: center;">{{ $ch->getFormattedOnly($fxRate) }}</td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight: bold;">Total (USD)</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTarget->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTargetMtd->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesMtd->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ number_format($salesRunRate->gmv, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesPrediction->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesAth->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ number_format($salesAthRunRate->gmv, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesRefundedMtd->gmv / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesYtd->gmv / $fxRate, 'USD') }}</td>
  </tr>
</table>

<h2>Revenue Overview</h2>
<table border="1" cellspacing="0" cellpadding="5">
  <tr>
    <th>Channel</th>
    <th>Product</th>
    <th>This Month Target</th>
    <th>Target MTD</th>
    <th>Achievement MTD</th>
    <th>Run Rate</th>
    <th>Prediction</th>
    <th>All Time High</th>
    <th>ATH Run Rate</th>
    <th>Loss Sales</th>
    <th>Last 1 Year</th>
  </tr>
  @foreach($keys as $c => $services)
    @foreach($services as $i => $s)
      <tr>
        <?php $key = $c . ' ' . $s; ?>
        @if($i == 0)
          <td rowspan="{{count($services)}}" style="font-weight: bold;">{{ $channelLabelMap[$c] }}</td>
        @endif
        <td style="font-weight: bold;">{{ $s }}</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($targetMap[$key]->revenue) }}</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($targetMtdMap[$key]->revenue) }}</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($mtdMap[$key]->revenue) }}</td>
        <td style="text-align: right;">{{ number_format($runRateMap[$key]->revenue, 0, ",", ".") }}%</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($predictionMap[$key]->revenue) }}</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($athMap[$key]->revenue) }}</td>
        <td style="text-align: right;">{{ number_format($athRunRateMap[$key]->revenue, 0, ",", ".") }}%</td>
        <td
          style="text-align: right;">{{ isset($refundedMtdMap[$key]) ? $ch->getFormattedOnly($refundedMtdMap[$key]->revenue) : '-' }}</td>
        <td style="text-align: right;">{{ $ch->getFormattedOnly($ytdMap[$key]->revenue) }}</td>
      </tr>
    @endforeach
  @endforeach
  <tr>
    <td colspan="2" style="font-weight: bold;">Total (IDR)</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTarget->revenue) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTargetMtd->revenue) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesMtd->revenue) }}</td>
    <td style="text-align: right;">{{ number_format($salesRunRate->revenue, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesPrediction->revenue) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesAth->revenue) }}</td>
    <td style="text-align: right;">{{ number_format($salesAthRunRate->revenue, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesRefundedMtd->revenue) }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesYtd->revenue) }}</td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight: bold;">USD Rate</td>
    <td colspan="9" style="text-align: center;">{{ $ch->getFormattedOnly($fxRate) }}</td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight: bold;">Total (USD)</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTarget->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesTargetMtd->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesMtd->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ number_format($salesRunRate->revenue, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesPrediction->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesAth->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ number_format($salesAthRunRate->revenue, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesRefundedMtd->revenue / $fxRate, 'USD') }}</td>
    <td style="text-align: right;">{{ $ch->getFormattedOnly($salesYtd->revenue / $fxRate, 'USD') }}</td>
  </tr>
</table>

<h2>Transaction Count Overview</h2>
<table border="1" cellspacing="0" cellpadding="5">
  <tr>
    <th>Channel</th>
    <th>Product</th>
    <th>This Month Target</th>
    <th>Target MTD</th>
    <th>Achievement MTD</th>
    <th>Run Rate</th>
    <th>Prediction</th>
    <th>All Time High</th>
    <th>ATH Run Rate</th>
    <th>Loss Sales</th>
    <th>Last 1 Year</th>
  </tr>
  @foreach($keys as $c => $services)
    @foreach($services as $i => $s)
      <tr>
        <?php $key = $c . ' ' . $s; ?>
        @if($i == 0)
          <td rowspan="{{count($services)}}" style="font-weight: bold;">{{ $channelLabelMap[$c] }}</td>
        @endif
        <td style="font-weight: bold;">{{ $s }}</td>
        <td style="text-align: right;">{{ number_format($targetMap[$key]->trx, 0, ",", ".") }}</td>
        <td style="text-align: right;">{{ number_format($targetMtdMap[$key]->trx, 0, ",", ".") }}</td>
        <td style="text-align: right;">{{ number_format($mtdMap[$key]->trx, 0, ",", ".") }}</td>
        <td style="text-align: right;">{{ number_format($runRateMap[$key]->trx, 0, ",", ".") }}%</td>
        <td style="text-align: right;">{{ number_format($predictionMap[$key]->trx, 0, ",", ".") }}</td>
        <td style="text-align: right;">{{ number_format($athMap[$key]->trx, 0, ",", ".") }}</td>
        <td style="text-align: right;">{{ number_format($athRunRateMap[$key]->trx, 0, ",", ".") }}%</td>
        <td
          style="text-align: right;">{{ isset($refundedMtdMap[$key]) ? number_format($refundedMtdMap[$key]->trx, 0, ",", ".") : '-' }}</td>
        <td style="text-align: right;">{{ number_format($ytdMap[$key]->trx, 0, ",", ".") }}</td>
      </tr>
    @endforeach
  @endforeach
  <tr>
    <td colspan="2" style="font-weight: bold;">Total</td>
    <td style="text-align: right;">{{ number_format($salesTarget->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesTargetMtd->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesMtd->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesRunRate->trx, 0, ",", ".") }}%</td>
    <td style="text-align: right;">{{ number_format($salesPrediction->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesAth->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesAthRunRate->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesRefundedMtd->trx, 0, ",", ".") }}</td>
    <td style="text-align: right;">{{ number_format($salesYtd->trx, 0, ",", ".") }}</td>
  </tr>
</table>

<br>
<br>
<br>

<h2>Top 100 Customer App</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>No.</th>
    <th>User Name</th>
    <th>Sales Product Value</th>
    <th>Sales Product Quantity</th>
  </tr>

  <?php $x = 1; $orderTotal = 0; $orderCount = 0; ?>
  @foreach ($orderValueReportsApp as $item)
    <?php
    $orderTotal += $item->order_total;
    $orderCount += $item->order_counts;
    ?>
    @if ($x <= 100 || $x == count($orderValueReportsApp))
      <tr>
        <td style="text-align: right;">{{ $x++ }}.</td>
        <td>{{ $item->user->name != null && $item->user->name != '' ? $item->user->name : '*GUEST*' }}</td>
        <td style="text-align: right">{{ $ch->formatPrice($item->order_total)['formatted_amount'] }}</td>
        <td style="text-align: right">{{ $item->order_counts }}</td>
      </tr>
    @elseif ($x > 100 && $x == count($orderValueReportsApp) - 1)
      <tr><td colspan="4">...</td></tr>
      <?php $x++; ?>
    @else 
      <?php $x++; ?>
    @endif
  @endforeach
  <tr class="total">
    <td colspan="2">Total</td>
    <td style="text-align: right">{{ $ch->formatPrice($orderTotal)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $orderCount }}</td>
  </tr>
</table>

<h2>Top 100 Customer H2H</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>No.</th>
    <th>User Name</th>
    <th>Sales Product Value</th>
    <th>Sales Product Quantity</th>
  </tr>

  <?php $x = 1; $orderTotal = 0; $orderCount = 0; ?>
  @foreach ($orderValueReportsH2H as $item)
    <?php
    $orderTotal += $item->order_total;
    $orderCount += $item->order_counts;
    ?>
    @if ($x <= 100 || $x == count($orderValueReportsH2H))
      <tr>
        <td style="text-align: right;">{{ $x++ }}.</td>
        <td>{{ $item->user->name != null && $item->user->name != '' ? $item->user->name : '*GUEST*' }}</td>
        <td style="text-align: right">{{ $ch->formatPrice($item->order_total)['formatted_amount'] }}</td>
        <td style="text-align: right">{{ $item->order_counts }}</td>
      </tr>
    @elseif ($x > 100 && $x == count($orderValueReportsH2H) - 1)
      <tr><td colspan="4">...</td></tr>
      <?php $x++; ?>
    @else 
      <?php $x++; ?>
    @endif
  @endforeach
  <tr class="total">
    <td colspan="2">Total</td>
    <td style="text-align: right">{{ $ch->formatPrice($orderTotal)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $orderCount }}</td>
  </tr>
</table>

<h2>Top 100 Customer Others</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>No.</th>
    <th>User Name</th>
    <th>Sales Product Value</th>
    <th>Sales Product Quantity</th>
  </tr>

  <?php $x = 1; $orderTotal = 0; $orderCount = 0; ?>
  @foreach ($orderValueReportsOthers as $item)
    <?php
    $orderTotal += $item->order_total;
    $orderCount += $item->order_counts;
    ?>
    @if ($x <= 100 || $x == count($orderValueReportsOthers))
      <tr>
        <td style="text-align: right;">{{ $x++ }}.</td>
        <td>{{ $item->user->name != null && $item->user->name != '' ? $item->user->name : '*GUEST*' }}</td>
        <td style="text-align: right">{{ $ch->formatPrice($item->order_total)['formatted_amount'] }}</td>
        <td style="text-align: right">{{ $item->order_counts }}</td>
      </tr>
    @elseif ($x > 100 && $x == count($orderValueReportsOthers) - 1)
      <tr><td colspan="4">...</td></tr>
      <?php $x++; ?>
    @else 
      <?php $x++; ?>
    @endif
  @endforeach
  <tr class="total">
    <td colspan="2">Total</td>
    <td style="text-align: right">{{ $ch->formatPrice($orderTotal)['formatted_amount'] }}</td>
    <td style="text-align: right">{{ $orderCount }}</td>
  </tr>
</table>

<h2>Top 20 Product By Quantity</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Name</th>
    <th>Quantity</th>
    <th>Percentage</th>
  </tr>

  <?php $subtotal = 0; ?>
  @foreach ($pulsaSkuCount as $item)
    <tr>
      <td>{{ $item->name }}</td>
      <td style="text-align: right">{{ $item->order_number_count }}<?php $subtotal += $item->order_number_count; ?></td>
      <td style="text-align: right">{{ round($item->order_number_count / $pulsaSkuCountTotal * 100) }}%</td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Subtotal</td>
    <td style="text-align: right">{{ $subtotal }}</td>
    <td style="text-align: right">{{ $pulsaSkuCountTotal ? round($subtotal / $pulsaSkuCountTotal * 100) : 0 }}%</td>
  </tr>
  <tr class="total">
    <td>Total</td>
    <td colspan="2" style="text-align: right">{{ $pulsaSkuCountTotal }}</td>
  </tr>
</table>

<h2>Top 20 Product By Value</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Name</th>
    <th>Value</th>
    <th>Percentage</th>
  </tr>

  <?php $subtotal = 0; ?>
  @foreach ($pulsaSkuValue as $item)
    <tr>
      <td>{{ $item->name }}</td>
      <td style="text-align: right">
        {{ $ch->formatPrice($item->order_value)['formatted_amount'] }}
        <?php $subtotal += $item->order_value; ?>
      </td>
      <td style="text-align: right">{{ round($item->order_value / $pulsaSkuValueTotal->order_value * 100) }}%</td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Subtotal</td>
    <td style="text-align: right">{{ $ch->formatPrice($subtotal)['formatted_amount'] }}</td>
    <td
      style="text-align: right">{{ $pulsaSkuValueTotal->order_value ? round($subtotal / $pulsaSkuValueTotal->order_value * 100) : 0 }}
      %
    </td>
  </tr>
  <tr class="total">
    <td>Total</td>
    <td colspan="2"
        style="text-align: right">{{ $ch->formatPrice($pulsaSkuValueTotal->order_value)['formatted_amount'] }}</td>
  </tr>
</table>

<h2>Top 20 Failed Product</h2>
<table border="1" cellspacing="0" cellpadding="5" class="text-center">
  <tr>
    <th>Name</th>
    <th>Quantity</th>
    <th>Percentage</th>
  </tr>

  <?php $subtotal = 0; ?>
  @foreach ($pulsaFailed as $item)
    <tr>
      <td>{{ $item->name }}</td>
      <td style="text-align: right">{{ $item->order_number_count }}<?php $subtotal += $item->order_number_count; ?></td>
      <td style="text-align: right">{{ round($item->order_number_count / $pulsaFailedTotal * 100) }}%</td>
    </tr>
  @endforeach
  <tr class="total">
    <td>Subtotal</td>
    <td style="text-align: right">{{ $subtotal }}</td>
    <td style="text-align: right">{{ $pulsaFailedTotal ? round($subtotal / $pulsaFailedTotal * 100) : 0 }}%</td>
  </tr>
  <tr class="total">
    <td>Total</td>
    <td colspan="2" style="text-align: right">{{ $pulsaFailedTotal }}</td>
  </tr>
</table>

<h2>Inventory Counts</h2>
<table border="1" cellspacing="0" cellpadding="5">
  <tr>
    <th>Owner</th>
      <th>In Stock</th>
      <th>No Stock</th>
  </tr>
    @foreach($inventoryCounts as $owner => $c)
    <tr>
        <td>{{ $owner  }}</td>
        <td>{{ $c['instock'] }}</td>
        <td>{{ $c['nostock'] }}</td>
    </tr>
  @endforeach
</table>
</body>
</html>