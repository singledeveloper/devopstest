<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')

<h2>FLIP BALANCE IMBALANCE on date {{$data['datetime']}}</h2>

FLIP BALANCE: {{$currencyHelper->formatPrice($data['flip_balance'])['formatted_amount']}} <br>
RECORDED BALANCE: {{$currencyHelper->formatPrice($data['recorded_balance'])['formatted_amount']}} <br>

</body>
</html>