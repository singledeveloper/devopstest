<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<?php echo $data['user']['name']  . ' - ' . $data['user']['telephone'];?> has requested a withdrawal <br><br>
amount : <?php echo $data['withdraw_amount']['formatted_amount']; ?><br>
to : <?php echo $data['account_bank'] . ' - ' . $data['account_number']; ?>
<br/><br/>
<?php echo isset($data['additional_message']) ? $data['additional_message'] : ''; ?>
</body>
</html>