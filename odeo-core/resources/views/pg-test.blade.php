<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <style>
    .progress {
      border-radius: 0;
      height: 0.3rem;
      background-color: transparent;
    }
  </style>
</head>
<body>
<div class="progress">
  <div id="query-progress-bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
       aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
</div>
<div class="container">
  <br>
  <h3>ODEO Payment Gateway Callback Test</h3>
  <br>
  <ul class="nav nav-tabs d-none" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="inquiry-tab" data-toggle="tab" href="#inquiry" role="tab"
         aria-controls="home"
         aria-selected="true">Inquiry</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="profile"
         aria-selected="false">Payment</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="status-tab" data-toggle="tab" href="#status" role="tab" aria-controls="contact"
         aria-selected="false">Payment Status</a>
    </li>
  </ul>
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="inquiry" role="tabpanel" aria-labelledby="home-tab">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Input VA Code</h5>
          <div class="form-row align-items-center">
            <div class="col-auto">
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <select class="custom-select" id="va-prefix">
                    @if(count($payments) == 1)
                      <option value="{{$payments[0]->prefix}}" selected>{{$payments[0]->prefix}}</option>
                    @else
                      <option value="">-Select Prefix-</option>
                      @foreach($payments as $payment)
                        <option value="{{$payment->prefix}}">{{$payment->prefix}}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
                <input type="text" class="form-control" id="va-number" placeholder="VA Number" required>
              </div>
            </div>
            <div class="col-auto">
              <button class="btn btn-primary mb-2" id="add-va-button">Submit</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="profile-tab">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Payment Details</h5>
          <table class="table">
            <tbody>
            <tr>
              <th scope="row">VA Code</th>
              <td><label class="va-code-label"></label></td>
            </tr>
            <tr>
              <th scope="row">Amount</th>
              <td><label class="amount-label"></label></td>
            </tr>
            <tr>
              <th scope="row">Customer Name</th>
              <td><label class="customer-name-label"></label></td>
            </tr>
            <tr>
              <th scope="row">Item Name</th>
              <td><label class="item-name-label"></label></td>
            </tr>
            <tr>
              <th scope="row">Description</th>
              <td><label class="display-text-label"></label></td>
            </tr>
            </tbody>
          </table>
          <button class="btn btn-primary mb-2" id="pay-button">Pay</button>
        </div>
      </div>
    </div>

    <div class="tab-pane fade" id="status" role="tabpanel" aria-labelledby="contact-tab">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Payment Details</h5>
          <table class="table">
            <tbody>
            <tr>
              <th scope="row">VA Code</th>
              <td><label class="va-code-label"></label></td>
            </tr>
            <tr>
              <th scope="row">Amount</th>
              <td><label class="amount-label"></label></td>
            </tr>
            <tr>
              <th scope="row">Customer Name</th>
              <td><label class="customer-name-label"></label></td>
            </tr>
            <tr>
              <th scope="row">Item Name</th>
              <td><label class="item-name-label"></label></td>
            </tr>
            <tr>
              <th scope="row">Display Text</th>
              <td><label class="display-text-label"></label></td>
            </tr>
            <tr>
              <th scope="row">Status</th>
              <td>
                <select id="notify-status" class="custom-select">
                  <option value="50000">50000 | SUCCESS</option>
                  <option value="80000">80000 | SUSPECT</option>
                  <option value="90000">90000 | FAILED</option>
                </select>
              </td>
            </tr>
            </tbody>
          </table>
          <button class="btn btn-primary mb-2" id="notify-status-button">Notify Status</button>
          <button class="btn btn-primary mb-2" id="retry-test-button">Retry Test</button>
        </div>
      </div>
    </div>
  </div>

  <br>

  <div class="card" id="error-container" style="display: none;">
    <div class="card-body alert-danger">
      <h5 class="card-title">Error</h5>
      <label id="error-label"></label>
    </div>
  </div>

  <div class="card" id="success-container" style="display: none;">
    <div class="card-body alert-success">
      <h5 class="card-title">Success</h5>
      <label id="success-label">Notification sent!</label>
    </div>
  </div>

</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<script>
  var vaNumber, amount, traceNo, customerName, pgPaymentId;

  var domain = '{{isStaging() ? "http://api.staging.odeo.co.id" : "http://api.odeo.local"}}';

  function resetProgressBar() {
    $('#query-progress-bar').css('width', '100%');
    setTimeout(function () {
      $('#query-progress-bar').css('width', '0%');
    }, 500);
  }

  function startProgressBar() {
    $('#query-progress-bar').css('width', '50%');
  }

  function hideErrorText() {
    $('#error-container').css('display', 'none');
  }

  function setErrorText(xhr) {
    $('#error-container').css('display', 'block');
    $('#error-label').text(xhr.responseJSON.responseMessage);
  }

  function showSuccessText(xhr) {
    $('#success-container').css('display', 'block');
  }

  function hideSuccessText(xhr) {
    $('#success-container').css('display', 'none');
  }

  $('#add-va-button').click(function () {
    prefix = $('#va-prefix').val();
    if (prefix == "") {
      alert('Select your VA prefix');
      return;
    }

    number = $('#va-number').val();
    if (number == "") {
      alert('Input your VA Number');
      return;
    }
    hideErrorText();
    startProgressBar();
    vaNumber = prefix + number;
    $.post(domain + '/v1/test/pg/notify-inquiry',
      {
        virtual_account_number: vaNumber
      },
      function (response) {
        amount = response.billingAmount;
        customerName = response.vaName;
        traceNo = response.traceNo;
        $('.va-code-label').text(vaNumber);
        $('.amount-label').text(response.billingAmount);
        $('.customer-name-label').text(customerName);
        $('.display-text-label').text(response.vaDesc);
        $('.item-name-label').text(response.vaItemName);
        $('#myTab #payment-tab').tab('show');
      },
      'json'
    ).done(hideErrorText)
      .fail(setErrorText)
      .always(resetProgressBar);
  });

  $('#pay-button').click(function () {
    startProgressBar();
    $.post(domain + '/v1/test/pg/notify-payment',
      {
        virtual_account_number: vaNumber,
        amount: amount,
        trace_no: traceNo,
      },
      function (response) {
        pgPaymentId = response.pgPaymentId;
        $('#myTab #status-tab').tab('show');
      },
      'json'
    ).done(hideErrorText)
      .fail(setErrorText)
      .always(resetProgressBar);
  });

  $('#notify-status-button').click(function () {
    startProgressBar();
    $.post(domain + '/v1/test/pg/notify-status',
      {
        pg_payment_id: pgPaymentId,
        pg_payment_status: $('#notify-status').val(),
        trace_no: traceNo
      },
      function (response) {
        console.log(response);
      },
      'json'
    ).done(function() {
      hideErrorText();
      showSuccessText();
    })
      .fail(setErrorText)
      .always(resetProgressBar);
  });

  $('#retry-test-button').click(function () {
    hideErrorText();
    hideSuccessText();
    $('#myTab #inquiry-tab').tab('show');
  });
</script>

</body>
</html>
