<style>
    table {
        font-family: monospace;
        margin: auto;
    }

    table td {padding: 5px 0; font-size: 1.2em;}

</style>
@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')
<table>
    <?php $detail = $so->bpjsKesDetail; ?>
    <tr>
        <td colspan="2">BANK MANDIRI</td>
    </tr>
    <tr>
        <td colspan="2">STRUK PEMBAYARAN IURAN BPJS KESEHATAN</td>
    </tr>
    <tr>
        <td>NO REFERENSI</td>
        <td>: {{ $so->serial_number }}</td>
    </tr>
    <tr>
        <td>TANGGAL</td>
        <td>: {{ $so->created_at }}</td>
    </tr>
    <tr>
        <td>NO VA</td>
        <td>: {{ $so->number }}</td>
    </tr>
    <tr>
        <td>NAMA PESERTA</td>
        <td>: {{ $detail->subscriber_name }}</td>
    </tr>
    <tr>
        <td>JUMLAH PESERTA</td>
        <td>: {{ $detail->participant_counts }} Orang</td>
    </tr>
    <tr>
        <td>SISA SEBELUMNYA</td>
        <td>: {{ $currencyHelper->formatPrice($detail->bill_rest)['formatted_amount'] }}</td>
    </tr>
    <tr>
        <td>PERIODE</td>
        <td>: {{ $detail->month_counts }} BULAN</td>
    </tr>
    <tr>
        <td>JUMLAH TAGIHAN</td>
        <td>: {{ $currencyHelper->formatPrice($detail->base_price)['formatted_amount'] }}</td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2">BPJS KESEHATAN MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH</td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td>BIAYA ADMIN</td>
        <td>: {{ $currencyHelper->formatPrice($detail->admin_fee)['formatted_amount'] }}</td>
    </tr>
    <tr>
        <td>TOTAL BAYAR</td>
        <td>: {{ $currencyHelper->formatPrice($detail->base_price + $detail->admin_fee)['formatted_amount'] }}</td>
    </tr>
    <tr>
        <td colspan="2">Rincian tagihan dapat diakses pada www.bpjs-kesehatan.go.id</td>
    </tr>
    <tr>
        <td colspan="2">Tanggal Cetak: {{ date('d-m-Y H:i:s') }} / CETAK ASLI</td>
    </tr>
</table>