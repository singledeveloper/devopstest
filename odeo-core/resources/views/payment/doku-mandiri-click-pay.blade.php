<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
  <meta name="apple-mobile-web-app-capable" content="yes">
   
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <title>DOKU Payment Page</title>

  <style>
    #payment-redirect {
      margin-top: 80px;
      padding: 80px;
      text-align: center;
      font-size: 21px;
    }
    .payment-wraper-error{
      margin-bottom: 10px;
    }
  </style>
  @if (!isProduction())
    <link href="http://staging.doku.com/doku-js/assets/css/doku.css" rel="stylesheet">
  @else
    <link href="https://pay.doku.com/doku-js/assets/css/doku.css" rel="stylesheet">
  @endif
  <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" rel="stylesheet">

</head>
<body>


{{--<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>--}}

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
@if (!isProduction())
  <script src="{{url()}}/js/doku-staging.js?version=<?php echo time(); ?>"></script>
@else
  <script src="{{url(null, [], true)}}/js/doku.js?version=<?php echo time(); ?>"></script>
@endif
<script>
    $(document).ready(function () {
        $(function () {
          /* hide show payment channel */
            $('#select-paymentchannel').change(function () {
                $('.channel').hide();
                $('#' + $(this).val()).show();
            });

            var data = new Object();
            data.req_cc_field = 'cc_number';
            data.req_challenge_field = 'CHALLENGE_CODE_1';

            dokuMandiriInitiate(data);

        });

        jQuery(function ($) {

            $('.cc-number').payment('formatCardNumber');
            $.fn.toggleInputError = function (erred) {
                this.parent('.form-group').toggleClass('has-error', erred);
                return this;
            };

            var challenge3 = Math.floor(Math.random() * 999999999);
            $("#challenge_div_3").text(challenge3);
            $("#CHALLENGE_CODE_3").val(challenge3);

        });
    });
</script>


<section class="default-width">

  <div class="head padd-default">
    <div class="left-head fleft">
      <img width="125" src="{{url('/images/odeo-logo-1.png', [], true)}}" alt=""/> 
    </div>
    <div class="right-head fright">
      <div class="text-totalpay color-two">Total Payment ( IDR )</div>
      <div class="amount color-one">{{number_format($amount,2,",",".")}}</div>
    </div>
    <div class="clear"></div>
  </div>

  <div class="select-payment-channel color-border padd-default">Mandiri Clickpay </div>

  <div class="content-payment-channel padd-default">
    <form method="post">

      <div id="mandiriclickpay" class="channel">
        <div class="logo-payment-channel right-paychan mandiriclickpay"></div>

        <input type="hidden" name="req_headers" value="{{$headers}}"> 
        <input type="hidden" name="doku_amount" value="{{$amount}}"> 
        <input type="hidden" name="req_payment_channel" value="{{$paymentChannel}}"> 
        <input type="hidden" name="opc" value="{{$opc}}">
        <input type="hidden" name="doku_currency" value="{{$currency}}">
        <input type="hidden" name="session" value="{{$sessionID}}">

        <div class="styled-input">
          <input type="text" id="cc_number" name="cc_number" class="cc-number" required/>
          <label>mandiri debit card number</label>
        </div>
        <div class="desc">
          Pastikan bahwa kartu Anda telah diaktivasi melalui layanan mandiri Internet Banking Bank Mandiri pada menu
          Authorized Payment agar dapat melakukan transaksi Internet Payment.
        </div>
        <div class="line"></div>
        <div class="token">
          <img src="http://staging.doku.com/doku-js/assets/images/token.png" alt="" class="fleft"/>
          <div class="text-token desc fright">
            Gunakan token pin mandiri untuk bertransaksi. Nilai yang dimasukkan pada token Anda (Metode APPLI 3)
          </div>
          <div class="clear"></div>
        </div>

        <div class="list-chacode">
          <ul>
            <li>
              <div class="text-chacode">Challange Code 1</div>
              <input type="text" id="CHALLENGE_CODE_1" name="CHALLENGE_CODE_1" readonly="true" required/>
              <div class="clear"></div>
            </li>
            <li>
              <div class="text-chacode">Challange Code 2</div>
              <div class="num-chacode"><?php echo sprintf("%d", $amount); ?></div>
              <input type="hidden" name="CHALLENGE_CODE_2" value="<?php echo sprintf("%d", $amount); ?>"/>
              <div class="clear"></div>
            </li>
            <li>
              <div class="text-chacode">Challange Code 3</div>
              <div class="num-chacode" id="challenge_div_3"></div>
              <input type="hidden" name="CHALLENGE_CODE_3" id="CHALLENGE_CODE_3"/>
              <div class="clear"></div>
            </li>
            <div class="clear"></div>
          </ul>
        </div>
        <div class="validasi">
          <div class="styled-input fleft width50">
            <input type="text" required="" maxlength="6" name="response_token">
            <label>Token Response</label>
          </div>
          <div class="clear"></div>
          <span title="Explenation Text" class="tooltip tolltips-wallet">
            <span title="More"><img src="http://staging.doku.com/doku-js/assets/images/icon-help.png" alt="" style="margin: 0 0 0 10px;"/>
            </span>
          </span>
        </div>
        <input type="hidden" name="invoice_no" value="{{$orderID}}">
        <input type="submit" id="processBtn" value="Process Payment" class="default-btn">
      </div>
    </form>
  </div>
</section>

<div class="footer"> 
  <img src="https://staging.doku.com/doku-js/assets/images/secure.png" alt=""/> 
  <br/><br/> 
  <div class="">Copyright DOKU 2016</div>
   
</div>

<script>

    jQuery(function ($) {
        $('form').submit(function (e) {
            e.preventDefault();

            var cardType = $.payment.cardType($('.cc-number').val());
            $('.cc-number').toggleInputError(!$.payment.validateCardNumber($('.cc-number').val()));

            var inputsArray = $('input').serializeArray();
            var input = {};
            for (var i = 0; i < inputsArray.length; i++) {
                input[inputsArray[i]['name']] = inputsArray[i]['value'];
            }

            $('#processBtn').val('Processsing...');
            $('#processBtn').prop('disabled', true);
            $.ajax({
                'type': 'POST',
                'url': '{{$submitURL}}',
                'headers': {
                    'Authorization': '{{$headers}}'
                },
                'data': input
            }).then(function (response) {

                $('#processBtn').prop('disabled', false);

                if (response['res_response_code'] == '0000') {
                    window.location.href = '{{$finish_redirect_url}}';
                } else {

                  $(".default-width").html('<div id="payment-redirect"><div class="payment-wraper-error"><label>Failed to process your payment.</label><br></div><div><label>You Will Redirect within 5 seconds...</label></div></div>');

                  setTimeout((function(){
                    window.location.href = '{{$unfinished_redirect_url}}';
                  }), 5000);

                }

            }, function () {
                $('#processBtn').prop('disabled', false);
                window.location.href = '{{$unfinished_redirect_url}}';
            });

            return false;

        });
    })

</script>

</body>
</html>
