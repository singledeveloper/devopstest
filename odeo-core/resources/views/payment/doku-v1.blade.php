<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Payment</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://pay.doku.com/doku-js/assets/css/doku.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" rel="stylesheet">
  <link rel="stylesheet" href="{{url('/css/doku-override.css', [], true)}}">
</head>
<body>
@if (isset($show_return_button))
  <div id="return-container">
    <a class="button" href="{{$unfinished_redirect_url}}">
      <img src="{{url('/images/previous-button.png', [], true)}}" class="icon-back"> kembali
    </a>
  </div>
@endif
<div id="header">
  <div class="row">
    <div class="col col-5">
      <img id="logo" src="{{url('/images/odeo-logo-1.png', [], true)}}" alt="">
    </div>
    @if (isset($order))
      <div class="col col-5">
        <div class="row">
          <b id="payment-type" class="pull-right">{{$order['payment']['name']}}</b>
        </div>
        <div class="row">
          <img id="payment-logo" class="pull-right" src="{{$order['payment']['logo']}}" alt="">
        </div>
      </div>
    @endif
  </div>
</div>

@if (isset($order))
  <div id="order-form">
    <h3 style="padding: 15px;">Pesanan Anda</h3>
    @foreach($order['cart_data']['items'] as $item)
      <div class="row item">
        <div class="col col-5 item-left">{{$item['name']}}</div>
        <div class="col col-5 item-right">{{$item['price']['formatted_amount']}}</div>
      </div>
      <hr>
    @endforeach
    <div class="row item">
      <div class="col col-5 item-left">Sub Total</div>
      <div class="col col-5 item-right">{{$order['cart_data']['subtotal']['formatted_amount']}}</div>
    </div>
    <div class="row item">
      <div class="col col-5 item-left">Admin Fee</div>
      <div class="col col-5 item-right">{{$order['payment']['detail']['fee']['formatted_amount']}}</div>
    </div>
    <hr>
    <div class="row item">
      <div class="col col-5 item-left" style="font-size:24px;">Total</div>
      <div class="col col-5 item-right"
           style="color: #1DBED8;font-size:24px;">{{$order['payment']['detail']['total']['formatted_amount']}}</div>
    </div>
  </div>
@endif


<div id="payment-form">
  <div doku-div='form-payment'>
    <input id="doku-token" name="doku-token" type="hidden"/>
    <input id="doku-pairing-code" name="doku-pairing-code" type="hidden"/>
  </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js"></script>
@if (!isProduction())
  <script src="{{url()}}/js/doku-staging.js?version=<?php echo time(); ?>"></script>
@else
  <script src="{{url(null, [], true)}}/js/doku.js?version=<?php echo time(); ?>"></script>
  <script src="https://cdn.ravenjs.com/3.14.1/raven.min.js"></script>
@endif
<script>

    $(document).ready(function () {

        var data = {};
        data.req_merchant_code = '{{$MALLID}}';
        data.req_chain_merchant = 'NA';
        data.req_payment_channel = '{{$paymentChannel}}';
        data.req_server_url = '{{$submitURL . '?opc=' . $opc}}';
        data.req_transaction_id = '{{$orderID}}';
        data.req_currency = '{{$currency}}';
        data.req_amount = '{{$amount}}';
        data.req_words = '{{$words}}';
        data.req_session_id = '{{$sessionID}}';
        data.req_form_type = 'full';
        data.req_headers = '{{$headers}}';
        data.finish_redirect_url = '{{$finish_redirect_url}}';
        data.error_redirect_url = '{{$error_redirect_url}}';
        data.unfinished_redirect_url = '{{$unfinished_redirect_url}}';

      @if(isset($customer_id))
          data.req_customer_id = '{{$customer_id}}';
      @endif
        @if(isset($token_payment))
          data.req_token_payment = '{{$token_payment}}';
        @endif
        getForm(data);

    });
</script>

</body>
</html>
